<?php

use Illuminate\Database\Seeder;
use Intervention\Image\ImageManagerStatic as Image;
use Carbon\Carbon;

class ProfileScirptSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $member = \DB::table('Member')->whereNotNull('profile_pic')->get();
        $i=0;
        foreach ($member as $memberDetail){
            $i++;
            $id = $memberDetail->id;
            $email = $memberDetail->username;
            $img = Image::make("https://s3-ap-southeast-1.amazonaws.com/trealcap/profile/".$email);
            $img->resize(500, 500);
            $b64 = base64_encode($img->encode()->encoded);
            DB::table('Member_Detail')->where('member_id',$id)->update(['profile_pic_64' => $b64]);

            echo $i."."."Email = ".$email."\n";

            DB::table('scirptlog')->insert([
                'member_id' => $id,
                'old_username' => $email,
                'scirpt' => 'ProfileScirpt',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
                
            ]);
            
            
        }
    }
}
