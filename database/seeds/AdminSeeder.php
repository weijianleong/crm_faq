<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = \Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Admin',
            'slug' => 'admin',
            'permissions' => [
                'admin' => true,
            ]
        ]);

        $user = \Sentinel::registerAndActivate([
            'email'   => 'supercs@trealcap.com',
            'username'  =>  'supercs',
            'password'  =>  'password1234',
            'password2'  =>  'password1234',
            'cs_type'  =>  '0',
            'permissions' =>  [
                'admin' => true,
            ]
        ]);

        $role = \Sentinel::findRoleByName('Admin');
        $role->users()->attach($user);

        $user2 = \Sentinel::registerAndActivate([
            'email'     =>  'fundcs@trealcap.com',
            'username'  =>  'fundcs',
            'password'  =>  'password1234',
            'password2'  =>  'password1234',
            'cs_type'  =>  '1',
            'permissions' =>  [
                'admin' => true,
            ]
        ]);
        $role->users()->attach($user2);

        $user3 = \Sentinel::registerAndActivate([
            'email'     =>  'cashcs@trealcap.com',
            'username'  =>  'cashcs',
            'password'  =>  'password1234',
            'password2'  =>  'password1234',
            'cs_type'  =>  '2',
            'permissions' =>  [
                'admin' => true,
            ]
        ]);
        $role->users()->attach($user3);

        $user4 = \Sentinel::registerAndActivate([
            'email'     =>  'genaralcs@trealcap.com',
            'username'  =>  'genaralcs',
            'password'  =>  'password1234',
            'password2'  =>  'password1234',
            'cs_type'  =>  '3',
            'permissions' =>  [
                'admin' => true,
            ]
        ]);
        $role->users()->attach($user4);

        
    }
}
