<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AutoTicketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $helpdesk = \DB::table('helpdesk')->where('is_reply',0)->where('updated_at','>=','2018-12-04')->get();
        $getDate = Carbon::now()->format('ymd');
        //echo $getDate."\n";
        $shift = 1;
        $i=0;
        foreach ($helpdesk as $helpdesks ) {
            $i++;
            echo $i.":".$helpdesks->id."\n";
            $helpdesk_cs_id = \DB::table('helpdesk')->where('id',$helpdesks->id)->value('cs_id');

            if($helpdesk_cs_id){

                if($cs_model = \DB::table('helpdesk_schedule')->where('day',$getDate)->where('cs_id',$helpdesk_cs_id)->first()){
                    if($cs_model->shift != $shift || $cs_model->work ==0){

                        if($cs_id = \DB::table('helpdesk_schedule')->where('day',$getDate)->where('work',1)->where('shift',$shift)->orderBy('ticket_num')->value('cs_id')){
                            \DB::table('helpdesk_schedule')->where('cs_id',$cs_id)->where('day',$getDate)->increment('ticket_num');
                            \DB::table('helpdesk')->where('id', $helpdesks->id)->update(['cs_id' => $cs_id ]);

                            $email = \DB::table('users')->where('id',$cs_id)->value('email');

                            \DB::table('helpdesk_log')->insert(
                                ['helpdesk_id' => $helpdesks->id,'helpdesk_type' => "99", 'cs_id' => '1','cs_type'=>'1','cs_username'=>'System','action'=>"T",'to_cs_id'=>$cs_id,'to_cs_username'=>$email,'created_at'=>Carbon::now()]
                            );
                            echo $i.":".$helpdesks->id."asign to ".$cs_id."\n";
                        }
                    }
                }

            }else{

                if($cs_id = \DB::table('helpdesk_schedule')->where('day',$getDate)->where('work',1)->where('shift',$shift)->orderBy('ticket_num')->value('cs_id')){
                    \DB::table('helpdesk_schedule')->where('cs_id',$cs_id)->where('day',$getDate)->increment('ticket_num');
                    \DB::table('helpdesk')->where('id', $helpdesks->id)->update(['cs_id' => $cs_id ]);

                    $email = \DB::table('users')->where('id',$cs_id)->value('email');

                    \DB::table('helpdesk_log')->insert(
                        ['helpdesk_id' => $helpdesks->id,'helpdesk_type' => "99", 'cs_id' => '1','cs_type'=>'1','cs_username'=>'System','action'=>"T",'to_cs_id'=>$cs_id,'to_cs_username'=>$email,'created_at'=>Carbon::now()]
                    );
                    echo $i.":".$helpdesks->id."asign to ".$cs_id."\n";
                }
            }


        }

        

    }
}
