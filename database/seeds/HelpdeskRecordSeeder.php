<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class HelpdeskRecordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \DB::table('helpdesk_record')->insert([
            'email' => "supercs@trealcap.com",
            'reply_sec' => "1000",
            'created_at' => "2018-07-25 09:30:00",
            'updated_at' => Carbon::now()
            
        ]);
    }
}
