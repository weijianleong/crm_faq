<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class BankListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \DB::table('bank_list')->insert([
            'bank_code' => 3002,
            'bank_type' => 4,
            'bank_value' => "ICBC",
            'bank_name' => "中国工商银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 3005,
            'bank_type' => 4,
            'bank_value' => "ABC",
            'bank_name' => "中国农业银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 3003,
            'bank_type' => 4,
            'bank_value' => "CCB",
            'bank_name' => "中国建设银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 3020,
            'bank_type' => 4,
            'bank_value' => "BCOM",
            'bank_name' => "中国交通银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 3001,
            'bank_type' => 4,
            'bank_value' => "CMB",
            'bank_name' => "招商银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4002,
            'bank_type' => 4,
            'bank_value' => "BOHA",
            'bank_name' => "渤海银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 3022,
            'bank_type' => 4,
            'bank_value' => "CEB",
            'bank_name' => "中国光大银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 3009,
            'bank_type' => 4,
            'bank_value' => "CIB",
            'bank_name' => "兴业银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 3039,
            'bank_type' => 4,
            'bank_value' => "CITIC",
            'bank_name' => "中信银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 3006,
            'bank_type' => 4,
            'bank_value' => "CMBC",
            'bank_name' => "中国民生银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 3036,
            'bank_type' => 4,
            'bank_value' => "GDB",
            'bank_name' => "广东发展银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 3004,
            'bank_type' => 4,
            'bank_value' => "SPDB",
            'bank_name' => "上海浦东发展银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 3035,
            'bank_type' => 4,
            'bank_value' => "PAB",
            'bank_name' => "平安银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 3038,
            'bank_type' => 4,
            'bank_value' => "PSBC",
            'bank_name' => "中国邮政储蓄银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 3059,
            'bank_type' => 4,
            'bank_value' => "SHB",
            'bank_name' => "上海银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 3032,
            'bank_type' => 4,
            'bank_value' => "BOB",
            'bank_name' => "北京银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 3033,
            'bank_type' => 4,
            'bank_value' => "SRCB",
            'bank_name' => "上海农村商业银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4008,
            'bank_type' => 4,
            'bank_value' => "HXB",
            'bank_name' => "华夏银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4005,
            'bank_type' => 4,
            'bank_value' => "CONT",
            'bank_name' => "包商银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4010,
            'bank_type' => 4,
            'bank_value' => "CRCB",
            'bank_name' => "重庆农商行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4011,
            'bank_type' => 4,
            'bank_value' => "CTG",
            'bank_name' => "重庆三峡银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4011,
            'bank_type' => 4,
            'bank_value' => "CTG",
            'bank_name' => "大连银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4012,
            'bank_type' => 4,
            'bank_value' => "DLB",
            'bank_name' => "大连银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4013,
            'bank_type' => 4,
            'bank_value' => "FUDI",
            'bank_name' => "富滇银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4014,
            'bank_type' => 4,
            'bank_value' => "FUJI",
            'bank_name' => "福建海峡银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4015,
            'bank_type' => 4,
            'bank_value' => "GNAB",
            'bank_name' => "广东南海农商行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4016,
            'bank_type' => 4,
            'bank_value' => "GNB",
            'bank_name' => "广东南粤银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4017,
            'bank_type' => 4,
            'bank_value' => "GRC",
            'bank_name' => "广州农商行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4018,
            'bank_type' => 4,
            'bank_value' => "GSRC",
            'bank_name' => "广东顺德农商行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4055,
            'bank_type' => 4,
            'bank_value' => "GUIL",
            'bank_name' => "桂林银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4019,
            'bank_type' => 4,
            'bank_value' => "GZCB",
            'bank_name' => "广州银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4020,
            'bank_type' => 4,
            'bank_value' => "HARB",
            'bank_name' => "哈尔滨银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4021,
            'bank_type' => 4,
            'bank_value' => "HENG",
            'bank_name' => "恒丰银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4022,
            'bank_type' => 4,
            'bank_value' => "HKB",
            'bank_name' => "汉口银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4045,
            'bank_type' => 4,
            'bank_value' => "ZHEJ",
            'bank_name' => "浙商银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4023,
            'bank_type' => 4,
            'bank_value' => "HUIS",
            'bank_name' => "徽商银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4024,
            'bank_type' => 4,
            'bank_value' => "HUZ",
            'bank_name' => "湖州银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4025,
            'bank_type' => 4,
            'bank_value' => "HZB",
            'bank_name' => "杭州银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4026,
            'bank_type' => 4,
            'bank_value' => "IBN",
            'bank_name' => "宁波通商银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4027,
            'bank_type' => 4,
            'bank_value' => "JAB",
            'bank_name' => "江阴农商行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4028,
            'bank_type' => 4,
            'bank_value' => "JIAN",
            'bank_name' => "江西银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4029,
            'bank_type' => 4,
            'bank_value' => "JIAX",
            'bank_name' => "嘉兴银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4030,
            'bank_type' => 4,
            'bank_value' => "JSB",
            'bank_name' => "江苏银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4031,
            'bank_type' => 4,
            'bank_value' => "JXNG",
            'bank_name' => "晋中银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4032,
            'bank_type' => 4,
            'bank_value' => "JZOU",
            'bank_name' => "锦州银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4033,
            'bank_type' => 4,
            'bank_value' => "KRCC",
            'bank_name' => "昆山农信社",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4034,
            'bank_type' => 4,
            'bank_value' => "LANG",
            'bank_name' => "廊坊银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4035,
            'bank_type' => 4,
            'bank_value' => "LANZ",
            'bank_name' => "兰州银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4036,
            'bank_type' => 4,
            'bank_value' => "MINT",
            'bank_name' => "民泰商行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4037,
            'bank_type' => 4,
            'bank_value' => "NBCB",
            'bank_name' => "宁波银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4038,
            'bank_type' => 4,
            'bank_value' => "NJCB",
            'bank_name' => "南京银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);


        \DB::table('bank_list')->insert([
            'bank_code' => 4039,
            'bank_type' => 4,
            'bank_value' => "TAI",
            'bank_name' => "台州银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4040,
            'bank_type' => 4,
            'bank_value' => "WUIJ",
            'bank_name' => "吴江农商行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4041,
            'bank_type' => 4,
            'bank_value' => "WUXI",
            'bank_name' => "无锡农商行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4042,
            'bank_type' => 4,
            'bank_value' => "XIA",
            'bank_name' => "厦门银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4043,
            'bank_type' => 4,
            'bank_value' => "ZAB",
            'bank_name' => "张家港农商行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4044,
            'bank_type' => 4,
            'bank_value' => "YNRC",
            'bank_name' => "宁波鄞州农村合作银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('bank_list')->insert([
            'bank_code' => 4046,
            'bank_type' => 4,
            'bank_value' => "ZTCB",
            'bank_name' => "浙江泰隆商业银行",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

    }
}
