<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MemberBlackSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $member = \DB::table('Member_Blacklist')->where('is_due_to_ban',0)->where('is_special_group',0)->orderBy('member_id')->get();
        $i=0;
        foreach ($member as $memberDetail){
            $i++;
            $id = $memberDetail->member_id;
            $username = $memberDetail->username;
            $new = $memberDetail->username."_".Carbon::now()->format('Ymd')."N".rand(5, 15);

            $ic = \DB::table('Member_Detail')->where('member_id',$id)->first(['identification_number']);
            $newIC = $ic->identification_number."_".Carbon::now()->format('Ymd')."N".rand(5, 15);

            
            \DB::table('Member')->where('id',$id)->where('username',$username)->update(['username' => $new,'void'=>1]);
            \DB::table('Member_Detail')->where('member_id',$id)->update(['identification_number' => $newIC]);
            \DB::table('Admin_Fees')->where('username',$username)->update(['username' => $new]);
            \DB::table('Bonus_Direct')->where('username',$username)->update(['username' => $new]);
            \DB::table('Bonus_Direct')->where('from_username',$username)->update(['from_username' => $new]);
            \DB::table('Bonus_Group')->where('username',$username)->update(['username' => $new]);
            \DB::table('Bonus_Month')->where('username',$username)->update(['username' => $new]);
            \DB::table('Bonus_Month')->where('from_username',$username)->update(['from_username' => $new]);
            \DB::table('Bonus_Override')->where('username',$username)->update(['username' => $new]);
            \DB::table('Bonus_Override')->where('from_username',$username)->update(['from_username' => $new]);
            \DB::table('Bonus_Pairing')->where('username',$username)->update(['username' => $new]);
            \DB::table('helpdesk')->where('username',$username)->update(['username' => $new]);
            \DB::table('helpdesk_comment')->where('client_username',$username)->update(['client_username' => $new]);
            \DB::table('Member_Account_Withdrawal')->where('username',$username)->update(['username' => $new]);
            \DB::table('member_commission_report')->where('username',$username)->update(['username' => $new]);
            \DB::table('Member_Compensate')->where('username',$username)->update(['username' => $new]);
            \DB::table('Member_Inactive_Account')->where('username',$username)->update(['username' => $new]);
            \DB::table('Member_Network')->where('username',$username)->update(['username' => $new]);
            \DB::table('Member_Sales')->where('username',$username)->update(['username' => $new]);
            \DB::table('Member_x')->where('username',$username)->update(['username' => $new]);
            \DB::table('Orders')->where('member_username',$username)->update(['member_username' => $new]);
            \DB::table('Orders_Fund')->where('member_username',$username)->update(['member_username' => $new]);
            \DB::table('payid_info')->where('rec_name',$username)->update(['rec_name' => $new]);
            \DB::table('temp_leader')->where('Email',$username)->update(['Email' => $new]);
            \DB::table('temp_member')->where('Emails',$username)->update(['Emails' => $new,'Ic number' =>$newIC]);
            \DB::table('temp_orders')->where('member_username',$username)->update(['member_username' => $new]);
            \DB::table('temp_ranking')->where('username',$username)->update(['username' => $new]);
            \DB::table('temp_rwallet')->where('member_username',$username)->update(['member_username' => $new]);
            \DB::table('Transfer')->where('from_username',$username)->update(['from_username' => $new]);
            \DB::table('Transfer')->where('to_username',$username)->update(['to_username' => $new]);
            \DB::table('Tre_Gateway_login')->where('username',$username)->update(['username' => $new]);
            \DB::table('users')->where('username',$username)->update(['username' => $new]);
            \DB::table('users')->where('email',$username)->update(['email' => $new]);
            \DB::table('Withdraw')->where('username',$username)->update(['username' => $new]);
            \DB::table('wwallet_reimburse')->where('username',$username)->update(['username' => $new]);
            \DB::table('temp_leader')->where('Email',$username)->update(['Email' => $new]);
            //\DB::table('temp_account_status')->where('Email',$username)->update(['Email' => $new]);
            //\DB::table('temp_allow_transfer')->where('Email',$username)->update(['Email' => $new]);
            //\DB::table('temp_bonus')->where('member_username',$username)->update(['member_username' => $new]);

            \DB::table('scirptlog')->insert([
                'member_id' => $id,
                'scirpt' => 'MemberBlackScirpt',
                'old_username' => $username,
                'new_username' => $new,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
                
            ]);
            
            \DB::table('Member_Blacklist')->where('member_id',$id)->update(['is_due_to_ban' => 99]);
            echo $i."."."old Email = ".$username."    new Email = ".$new."\n";
        }
        echo "Done";

    }
}
