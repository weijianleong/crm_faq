<?php

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    public function run() {
        $this->call(AdminSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(MT5TableSeeder::class);
        $this->call(AdminRightTableSeeder::class);
        $this->call(AdminSideBarSeeder::class);
        //$this->call(PackageSeeder::class);


    }
}
