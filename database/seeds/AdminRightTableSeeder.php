<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AdminRightTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \DB::table('admin_right')->insert([
            'user_id' => "1",
            'level' => "superadmin",
            'right' => json_encode(["home"=>true,"profile"=>true,"right"=>true,"member" => true,"memberP" => true,"lpoa" => true,"processT" => true,"helpdesk" => true,"announcement" => true] ),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);
    }
}
