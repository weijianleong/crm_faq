<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AnnouncementAddRefrenceIdSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $announcement = \DB::table('Announcement')->orderBy('id')->get()->groupBy(function($d) {
             return Carbon::parse($d->created_at)->format('Y-m-d');
         });
        $i=0;

        //dd($announcement);
        foreach($announcement as $announcementDetail =>$value){
            $i++;
            $date[$i] = $announcementDetail;
            //\DB::table('announcement')->where('created_at', 'like', $announcementDetail.'%')->orderBy('created_at')->first();
        }
        for($x=1 ; $x<=$i ; $x++){

            $announcement = \DB::table('Announcement')->where('created_at', 'like', $date[$x].'%')->orderBy('created_at')->get();
            
            foreach($announcement as $announcementDetail){
                echo $x." : ". $announcementDetail->created_at.",";

                if(count($announcement) <= 1){
                    $refrence_id = substr($announcementDetail->created_at,2,2)."-".substr($announcementDetail->created_at,5,2).substr($announcementDetail->created_at,8,2)."01";
                    echo "  refrence_id : ". $refrence_id."\n";
                    //$data['refrence_id'] = Carbon::now()->format('y').'-'.Carbon::now()->format('m').Carbon::now()->format('d').'01';   
                
                }else{
                    $lastRefrence_id = \DB::table('Announcement')->where('created_at', 'like', $date[$x].'%')->orderBy('refrence_id','DESC')->first();

                    $last = substr($lastRefrence_id->refrence_id,7);
                    if( ($last + 1) <= 9){
                        $refrence_id = substr($announcementDetail->created_at,2,2)."-".substr($announcementDetail->created_at,5,2).substr($announcementDetail->created_at,8,2).'0'.($last + 1);
                        echo "  refrence_id : ". $refrence_id."\n";
                    }else{
                        $refrence_id = substr($announcementDetail->created_at,2,2)."-".substr($announcementDetail->created_at,5,2).substr($announcementDetail->created_at,8,2).($last + 1);
                        echo "  refrence_id : ". $refrence_id."\n";

                    }

                }

                \DB::table('Announcement')
                ->where('id', $announcementDetail->id)
                ->update(['refrence_id' => $refrence_id]);
            }
        }
         
    }
}
