<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class AdminSideBarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //home
        \DB::table('admin_sbar')->insert([
            'name' => "Home",
            'right_name' => "home",
            'routes' => "admin.home",
            'icon' => "md md-blur-on",
            'class' => "md md-blur-on",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);
        //profile
        \DB::table('admin_sbar')->insert([
            'name' => "Profile",
            'right_name' => "profile",
            'routes' => "admin.settings.account",
            'icon' => "md md-settings",
            'class' => "md md-settings",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);
        // \DB::table('admin_sbar')->insert([
        //     'name' => "Personal Rating",
        //     'right_name' => "profile",
        //     'routes' => "admin.settings.personalRating",
        //     'icon' => "md md-settings",
        //     'class' => "md md-settings",
        //     'parent_id' => "2",
        //     'created_at' => Carbon::now(),
        //     'updated_at' => Carbon::now()
            
        // ]);
        //right
        \DB::table('admin_sbar')->insert([
            'name' => "Edit Right",
            'right_name' => "right",
            'routes' => "admin.right",
            'icon' => "glyphicon glyphicon-pencil",
            'class' => "glyphicon glyphicon-pencil",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);
        //member
        \DB::table('admin_sbar')->insert([
            'name' => "Members",
            'right_name' => "member",
            'routes' => "admin.member.list",
            'icon' => "md md-accessibility",
            'class' => "md md-accessibility",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);
        //member pending
        \DB::table('admin_sbar')->insert([
            'name' => "Members Pending Verification",
            'right_name' => "memberP",
            'routes' => "admin.member.list2",
            'icon' => "md md-accessibility",
            'class' => "md md-accessibility",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);
        //lpoa
        \DB::table('admin_sbar')->insert([
            'name' => "LPOA",
            'right_name' => "lpoa",
            'routes' => "admin.settings.declare",
            'icon' => "md md-wallet-giftcard",
            'class' => "md md-wallet-giftcard",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);
        //process
        \DB::table('admin_sbar')->insert([
            'name' => "Process Trade",
            'right_name' => "processT",
            'routes' => "admin.settings.trade",
            'icon' => "md md-wallet-giftcard",
            'class' => "md md-wallet-giftcard",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);
        //helpdesk
        \DB::table('admin_sbar')->insert([
            'name' => "Helpdesk",
            'right_name' => "helpdesk",
            'icon' => "md md-local-attraction",
            'class' => "md md-local-attraction",
            'data_target' => "helpdesk",
            'child' => "2",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('admin_sbar')->insert([
            'name' => "Pending Helpdesk",
            'right_name' => "helpdesk",
            'routes' => "admin.helpdesk.Plist",
            'icon' => "md md-accessibility",
            'class' => "gmd md-accessibility",
            'parent_id' => "8",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('admin_sbar')->insert([
            'name' => "Multi Send",
            'right_name' => "helpdesk",
            'routes' => "admin.helpdesk.multiSend",
            'icon' => "md md-accessibility",
            'class' => "gmd md-accessibility",
            'parent_id' => "8",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);
        \DB::table('admin_sbar')->insert([
            'name' => "Personal Rating",
            'right_name' => "helpdesk",
            'routes' => "admin.helpdesk.personalRating",
            'icon' => "md md-accessibility",
            'class' => "gmd md-accessibility",
            'parent_id' => "8",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);
        \DB::table('admin_sbar')->insert([
            'name' => "Helpdesk Reply Detail",
            'right_name' => "helpdesk_record",
            'routes' => "admin.helpdesk.Record",
            'icon' => "md md-accessibility",
            'class' => "gmd md-accessibility",
            'parent_id' => "8",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);
        \DB::table('admin_sbar')->insert([
            'name' => "Helpdesk Schedule",
            'right_name' => "helpdesk_schedule",
            'routes' => "admin.helpdesk.schedule",
            'icon' => "md md-accessibility",
            'class' => "gmd md-accessibility",
            'parent_id' => "8",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);
        \DB::table('admin_sbar')->insert([
            'name' => "Helpdesk Black List",
            'right_name' => "helpdesk_black",
            'routes' => "admin.helpdesk.blacklist",
            'icon' => "md md-accessibility",
            'class' => "gmd md-accessibility",
            'parent_id' => "8",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);
        // anounment
        \DB::table('admin_sbar')->insert([
            'name' => "Announcement",
            'right_name' => "announcement",
            'icon' => "md md-new-releases",
            'class' => "md md-new-releases",
            'data_target' => "MDAnnouncement",
            'child' => "2",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('admin_sbar')->insert([
            'name' => "Create",
            'right_name' => "announcement",
            'routes' => "admin.announcement.create",
            'icon' => "",
            'class' => "",
            'parent_id' => "15",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);

        \DB::table('admin_sbar')->insert([
            'name' => "list",
            'right_name' => "announcement",
            'routes' => "admin.announcement.list",
            'icon' => "",
            'class' => "",
            'parent_id' => "15",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);
        //withdrawal
        \DB::table('admin_sbar')->insert([
            'name' => "Withdraw Statement",
            'right_name' => "withdrawal",
            'routes' => "admin.withdraw.all",
            'icon' => "md md-accessibility",
            'class' => "md md-accessibility",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);
        \DB::table('admin_sbar')->insert([
            'name' => "Withdrawal Pending Approval",
            'right_name' => "withdrawal",
            'routes' => "admin.withdraw.pending",
            'icon' => "md md-accessibility",
            'class' => "md md-accessibility",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);
        //Manually Deposit
        \DB::table('admin_sbar')->insert([
            'name' => "Manually Deposit",
            'right_name' => "manual_deposit",
            'routes' => "admin.manualDeposit",
            'icon' => "md md-border-color",
            'class' => "md md-border-color",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            
        ]);
        
    }
}
