<?php

use App\Repositories\MemberRepository;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userRole = \Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'User',
            'slug' => 'user',
            'permissions' => [
                'member' => true,
            ]
        ]);

        $user = \Sentinel::registerAndActivate([
            'email'   => 'user@user.com',
            'username'  =>  'user',
            'password'  =>  'password',
            'password2'  =>  'password2',
            'permissions' =>  [
                'member' => true,
            ]
        ]);

        $role = \Sentinel::findRoleByName('User');
        $role->users()->attach($user);

        $repo = new MemberRepository(new \App\Models\Member);
        $repo->store([
        	'user_id'	=>	$user->id,
            'package_id'    =>  1,
            'username'  =>  $user->username,
            'level' => 1
        	// 'package_amount'	=>	100
        ]);
    }
}
