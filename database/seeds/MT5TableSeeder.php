<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MT5TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \DB::table('Shares_Centre')->insert([
            'minimum_price' => 0.200,
            'current_price' => 0.200,
            'raise_by' => 0.001,
            'raise_limit' => 20000,
            'always_company' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        \DB::table('MT5')->insert([
            'bookA' => "1111",
            'bookB' => "2222",
        ]);
    }
}
