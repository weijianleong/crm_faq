<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsdcxWalletToMemberWalletTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('member_wallet', function (Blueprint $table) {
            //
            $table->decimal('usdcx_wallet',16,2)->nullable()->default('0')->after('t_wallet');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('member_wallet', function (Blueprint $table) {
            //
            $table->dropColumn('usdcx_wallet');
        });
    }
}
