<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TempBonus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_bonus', function (Blueprint $table) {
            $table->integer('member_id')->nullable();
            $table->string('member_username',200)->nullable();
            $table->string('mber ID',16)->nullable();
            $table->decimal('Total',7,2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_bonus');
    }
}
