<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbHelpdeskSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('helpdesk_schedule', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cs_id')->nullable(false)->index();
            $table->integer('ticket_num')->nullable(false)->default('0');
            $table->integer('work')->nullable(false)->default('0')->comment('0 - offday , 1-working');
            $table->integer('day')->nullable(false)->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('helpdesk_schedule');
    }
}
