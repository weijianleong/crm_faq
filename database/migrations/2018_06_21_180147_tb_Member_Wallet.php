<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbMemberWallet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Member_Wallet', function (Blueprint $table) {
            $table->increments('member_id');

            
            $table->decimal('register_point', 16, 2)->nullable(false)->default(0);
            $table->decimal('purchase_point', 16, 2)->nullable(false)->default(0);
            $table->decimal('promotion_point', 16, 2)->nullable(false)->default(0);
            $table->decimal('cash_point', 16, 2)->nullable(false)->default(0);
            $table->decimal('md_point', 16, 2)->nullable(false)->default(0);
            $table->tinyInteger('lock_cash')->nullable(false)->default(0);
            $table->tinyInteger('lock_promotion')->nullable(false)->default(0);
            $table->tinyInteger('lock_register')->nullable(false)->default(0);
            $table->decimal('w_wallet', 16, 2)->nullable()->default(0);
            $table->decimal('b_wallet', 16, 2)->nullable()->default(0);
            $table->decimal('wd_wallet', 16, 2)->nullable()->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Member_Wallet');
    }
}
