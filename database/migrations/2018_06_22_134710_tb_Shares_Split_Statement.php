<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbSharesSplitStatement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Shares_Split_Statement', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('member_id')->nullable(false)->default('0')->index()->unsigned();
            $table->integer('amount')->nullable(false)->default('0')->unsigned();
            $table->string('username',255)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Shares_Split_Statement');
    }
}
