<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbHelpdeskBlacklist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('helpdesk_blacklist', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_id')->nullable(false)->index();
            $table->integer('is_ban')->nullable(false)->default('1')->comment('0 - cancel ban , 1- ban');
            $table->integer('day')->nullable();
            $table->integer('cr8_by')->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('helpdesk_blacklist');
    }
}
