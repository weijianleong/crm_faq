<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbOrdersFund extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Orders_Fund', function (Blueprint $table) {
            $table->increments('orderid');

                     
            $table->integer('member_id')->nullable();
            $table->string('member_username',200)->nullable();
            $table->integer('bookA')->nullable();
            $table->integer('bookAorderid')->nullable();
            $table->char('status',2)->default('N');
            $table->decimal('balancea', 16, 2)->nullable()->default('0');
            $table->decimal('balanceb', 16, 2)->nullable()->default('0');
            $table->decimal('profitA', 16, 2)->nullable()->default('0');
            $table->decimal('swapA', 16, 2)->nullable()->default('0');
            $table->decimal('slippage', 16, 2)->nullable()->default('0');
            $table->dateTime('opentime')->nullable();
            $table->dateTime('closetime')->nullable();
            $table->decimal('openprice', 16, 5)->nullable();
            $table->decimal('closeprice', 16, 5)->nullable();
            $table->char('tradecommand',5)->nullable();
            $table->string('symbol',15)->nullable();
            $table->decimal('lotsize', 6, 2)->nullable();
            $table->decimal('stoploss', 16, 5)->nullable();
            $table->decimal('takeprofit', 16, 5)->nullable();
            $table->decimal('transferb', 16, 2)->nullable()->default('0');
            $table->decimal('bwallet', 16, 2)->nullable()->default('0');
            $table->decimal('wwallet', 16, 2)->nullable()->default('0');
            $table->decimal('mwallet', 16, 2)->nullable()->default('0');
            $table->decimal('rwallet', 16, 2)->nullable()->default('0');
            $table->decimal('swallet', 16, 2)->nullable()->default('0');
            $table->integer('checkstatus')->nullable(false)->default('0');
            $table->string('checkby',200)->nullable();
            $table->integer('fundcompany')->nullable();
            $table->integer('mam_account')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Orders_Fund');
    }
}
