<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbPartner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Partner', function (Blueprint $table) {
            $table->increments('id');


            $table->string('partner_name',200)->nullable();
            $table->string('partner_name_cn',200)->nullable();
            $table->decimal('m_wallet', 16, 2)->nullable()->default('0');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Partner');
    }
}
