<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbMamUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mam_users', function (Blueprint $table) {
            $table->increments('id');

            $table->string('Email',191)->nullable();
            $table->string('Name',191)->nullable();
            $table->string('Group',100)->nullable();
            $table->string('ClientGroup',45)->nullable();
            $table->string('Country',45)->nullable();
            $table->string('State',45)->nullable();
            $table->string('Address',200)->nullable();
            $table->integer('CardType')->nullable();
            $table->string('CardNumber',100)->nullable();
            $table->string('ZipCode',45)->nullable();
            $table->string('PhoneNumber',45)->nullable();
            $table->string('Password',100)->nullable();
            $table->string('ConfirmPassword',100)->nullable();
            $table->integer('MAMAccount')->nullable();
            $table->decimal('MAMBalance',10,0)->nullable();
            $table->text('Comment')->nullable();
            $table->dateTime('RegisterTime')->nullable();
            $table->dateTime('UpdateTime')->nullable();
            $table->string('PartnerCode',45)->nullable();
            $table->string('TraderCode',45)->nullable();
            $table->string('ManagerCode',45)->nullable();
            $table->integer('status')->nullable();
            $table->decimal('rbalance',16,2)->nullable();
            $table->char('chgpass', 2)->default('N');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mam_users');
    }
}
