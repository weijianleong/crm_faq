<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbTempAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_accounts', function (Blueprint $table) {

            $table->integer('Login')->nullable();
            $table->string('Name',255)->nullable();
            $table->string('Group',255)->nullable();
            $table->string('Country',255)->nullable();
            $table->decimal('Balance', 16, 2)->nullable();
            $table->integer('member_id')->nullable();
            $table->decimal('TotalBalance', 16, 2)->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_accounts');
    }
}
