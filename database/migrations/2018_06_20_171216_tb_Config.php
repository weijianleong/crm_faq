<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Config', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('key',50)->nullable();
            $table->text('content')->nullable();
            $table->tinyInteger('is_active')->nullable(false)->default('1');

            $table->timestamps();
            $table->index('key');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Config');
    }
}
