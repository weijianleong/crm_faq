<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbMemberAccountSelf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Member_Account_Self', function (Blueprint $table) {
            $table->increments('id');


            $table->integer('member_id')->nullable();
            $table->integer('bookA')->nullable();
            $table->integer('bookB')->nullable();
            $table->char('system_manage', 2)->nullable();
            $table->integer('status')->nullable();
            $table->dateTime('lockdate')->nullable();
            $table->decimal('rwallet',16,2)->nullable();
            $table->integer('winB')->default('0');
            $table->string('updated_by',200)->nullable();
            $table->decimal('balanceA',16,2)->default('0');
            $table->decimal('balanceB',16,2)->default('0');
            $table->string('partner_code',32)->nullable();
            $table->string('manager_code',32)->nullable();
            $table->string('trader_code',32)->nullable();
            $table->tinyInteger('partner_status')->nullable(false)->default('0');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Member_Account_Self');
    }
}
