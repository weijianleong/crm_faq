<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbMemberCompensate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Member_Compensate', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_id')->nullable(false)->default('0');
            $table->string('username',255)->nullable();
            $table->string('first_name',191)->nullable();
            $table->integer('booka')->default('0');
            $table->decimal('w_wallet',10,2)->nullable(false)->default('0');
            $table->decimal('balancea',10,2)->nullable(false)->default('0');
            $table->integer('balanceb')->default('0');
            $table->decimal('compensated_amount',16,2)->nullable(false)->default('0');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Member_Compensate');
    }
}
