<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbBankInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_info', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('member_id')->nullable(false)->index();
            $table->integer('withdraval_type')->nullable(false)->comment('1=银联，2=电汇')->index();
            $table->string('bank_no',200)->nullable(false)->index();
            $table->string('bank_address',200)->nullable();
            $table->string('bank_name',200)->nullable();
            $table->string('countryName',200)->nullable();
            $table->string('stateName',200)->nullable();
            $table->string('cityName',200)->nullable();
            $table->string('sub_bank',200)->nullable();
            $table->string('swift',200)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_info');
    }
}
