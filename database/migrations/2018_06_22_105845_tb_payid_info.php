<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbPayidInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payid_info', function (Blueprint $table) {
            $table->increments('payid');
            $table->string('bank_name',255)->nullable();
            $table->string('zbank_name',255)->nullable();
            $table->string('rec_name',380)->nullable();
            $table->string('bank_address',255)->nullable();
            $table->string('bank_swift',128)->nullable();
            $table->string('bank_memo',255)->nullable();
            $table->string('card_no',50)->nullable();
            $table->string('Countries',255)->nullable();
            $table->string('Province',255)->nullable();
            $table->string('City',255)->nullable();
            $table->smallInteger('WithdrawalMethod')->nullable();
            $table->string('mobile',50)->nullable();
            $table->string('id_number',32)->nullable();
            $table->double('exchangeRate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payid_info');
    }
}
