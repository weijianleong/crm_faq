<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbWithdraw extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Withdraw', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('member_id')->nullable(false)->unique();
            $table->string('username',255)->nullable();
            $table->string('status',255)->nullable();
            $table->decimal('admin',16,4)->nullable(false)->default('0');
            $table->string('payment_type',255)->nullable();
            $table->string('bank_name',255)->nullable();
            $table->string('bank_account_number',255)->nullable();
            $table->string('bank_account_holder',255)->nullable();
            $table->string('bank_swiftcode',255)->nullable();
            $table->string('bank_country',255)->nullable();
            $table->string('bank_address',255)->nullable();
            $table->string('card_number',255)->nullable();
            $table->string('card_country',255)->nullable();
            $table->string('receipt',255)->nullable();
            $table->decimal('amount',16,4)->nullable(false)->default('0');
            $table->string('_token',255)->nullable();



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Withdraw');
    }
}
