<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbMemberCommissionReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_commission_report', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('member_id')->nullable(false)->default('0');
           
            $table->string('username',500)->nullable(false)->default('"');
            $table->string('unique_code',500)->nullable(false)->default('"');
            $table->string('detail_report_name',500)->nullable(false)->default('"');
            $table->string('summary_report_name',500)->nullable(false)->default('"');
            $table->decimal('income_wallet',16,2)->nullable(false)->default('0');
            $table->decimal('income_wallet_different',16,2)->nullable(false)->default('0');
            $table->char('status',2)->default('N');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_commission_report');
    }
}
