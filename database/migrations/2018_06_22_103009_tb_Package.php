<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbPackage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Package', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title',100)->nullable();
            $table->tinyinteger('insurance_percent')->default('0');
            $table->decimal('package_amount', 16, 2)->nullable()->default('0');;
            $table->decimal('cash_amount', 16, 2)->nullable()->default('0');;
            $table->decimal('b_amount', 16, 2)->nullable()->default('0');;
            $table->decimal('deposit_amount', 16, 2)->nullable()->default('0');;
            $table->decimal('lotsize', 16, 2)->nullable()->default('0');;
            $table->integer('status')->default('0');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Package');
    }
}
