<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbMember extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Member', function (Blueprint $table) {
            $table->increments('id');

            $table->string('username',255)->nullable();
            $table->string('secret_password',255)->nullable();
            $table->string('register_by',255)->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('package_id')->nullable();
            $table->integer('direct_id')->nullable();
            $table->integer('root_id')->nullable();
            $table->integer('level')->nullable(false)->unsigned()->default('1');
            $table->integer('direct_percent')->nullable(false)->unsigned()->default('0');
            $table->integer('month_percent')->nullable(false)->unsigned()->default('0');
            $table->integer('group_percent')->default('0');
            $table->integer('group_level')->nullable()->unsigned()->default('0');
            $table->integer('max_pair')->nullable()->unsigned()->default('0');
            $table->decimal('max_pairing_bonus',8,2)->nullable(false)->default('0');
            $table->decimal('original_amount',16,4)->nullable(false)->default('0');
            $table->decimal('left_total',8,2)->nullable(false)->default('0');
            $table->decimal('right_total',8,2)->nullable(false)->default('0');
            $table->enum('position', ['left', 'right', 'top'])->nullable();
            $table->integer('is_active')->nullable(false)->default('1');
            $table->integer('is_group_bonus')->nullable(false)->default('0');
            $table->text('left_children')->nullable();
            $table->text('right_children')->nullable();
            $table->string('id_front',200)->nullable();
            $table->string('id_back',200)->nullable();
            $table->string('declare_form',200)->nullable();
            $table->char('declare_status', 2)->nullable(false)->default('N');
            $table->char('process_status', 2)->nullable(false)->default('N');
            $table->string('lpoa',200)->nullable();
            $table->char('lpoa_status', 2)->default('N');
            $table->char('lpoa_status_2',2)->nullable();
            $table->string('declare_form2',200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Member');
    }
}
