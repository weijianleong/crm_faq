<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbMemberInactiveAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Member_Inactive_Account', function (Blueprint $table) {
            

            $table->increments('member_id');
            $table->string('first_name',255)->nullable();
            $table->string('username',255)->nullable();
            $table->string('secret_password',255)->nullable();
            $table->integer('direct_id')->unsigned()->nullable();
            $table->char('lpoa_status',2)->default('N');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Member_Inactive_Account');
    }
}
