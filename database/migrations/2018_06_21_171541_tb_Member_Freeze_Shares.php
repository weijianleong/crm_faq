<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbMemberFreezeShares extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Member_Freeze_Shares', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('member_id')->unsigned()->nullable(false)->index();
            $table->integer('amount')->unsigned()->nullable(false)->default('0');
            $table->tinyInteger('has_process')->nullable(false)->default('0');
            $table->dateTime('active_date')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Member_Freeze_Shares');
    }
}
