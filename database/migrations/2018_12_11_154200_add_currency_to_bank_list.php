<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrencyToBankList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bank_list', function (Blueprint $table) {
            //
            $table->string('currency',100)->nullable()->after('bank_name')->comment('bank_country');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bank_list', function (Blueprint $table) {
            //
            $table->dropColumn('currency');

        });
    }
}
