<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbTempTenfx extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_tenfx', function (Blueprint $table) {
            $table->string('date',255)->nullable();
            $table->integer('mam')->nullable();
            $table->integer('tradeno')->nullable();
            $table->integer('win')->nullable();
            $table->integer('lose')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_tenfx');
    }
}
