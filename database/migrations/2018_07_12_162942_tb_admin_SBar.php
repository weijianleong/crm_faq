<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbAdminSBar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_SBar', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',200)->nullable(false);
            $table->string('right_name',200)->nullable(false);
            $table->string('routes',200)->nullable();
            $table->string('icon',200)->nullable();
            $table->string('class',200)->nullable();
            $table->string('data_target',200)->nullable();
            $table->string('child',200)->nullable();
            $table->string('parent_id',200)->nullable();
            $table->string('order_by',200)->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_SBar');
    }
}
