<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbSharesCentre extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Shares_Centre', function (Blueprint $table) {
            $table->increments('id');

            $table->decimal('minimum_price', 16, 4)->nullable(false)->default('0');
            $table->decimal('current_price', 16, 4)->nullable(false)->default('0');
            $table->decimal('raise_by', 16, 4)->nullable(false)->default('0');
            $table->integer('current_accumulate')->nullable(false)->unsinged()->default('0');
            $table->integer('raise_limit')->nullable(false)->unsinged()->default('200000');
            $table->tinyInteger('always_company')->nullable(false)->default('0');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Shares_Centre');
    }
}
