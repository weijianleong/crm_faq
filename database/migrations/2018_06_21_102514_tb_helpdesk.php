<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbHelpdesk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('helpdesk', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('refrence_id')->unsigned()->nullable(false);
            $table->integer('user_id')->unsigned()->nullable(false);
            $table->string('username',255)->nullable();
            $table->string('tittle',255)->nullable();
            $table->integer('type')->nullable();
            $table->integer('for_cs_type')->nullable();
            $table->integer('cs_id')->nullable();
            $table->string('status',255)->nullable();
            $table->integer('is_reply')->nullable(false)->default('0');
            $table->integer('is_rate')->nullable(false)->default('0');
            $table->integer('rate_star')->nullable(false)->default('0');
            $table->text('content')->nullable();
            $table->string('pic_1',200)->nullable();
            $table->string('pic_2',200)->nullable();
            $table->string('close_by',255)->nullable();
            $table->string('remark',255)->nullable();
            $table->integer('read')->nullable(false)->default('0');
            $table->string('_token',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('helpdesk');
    }
}
