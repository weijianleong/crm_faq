<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbTempMemberAccountFundPerformance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Temp_Member_Account_Fund_Performance', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('account_no')->nullable();
            $table->integer('win_status')->nullable();
            $table->dateTime('transaction_date')->nullable();
            $table->integer('fund_company')->nullable();
            $table->decimal('m_wallet',10,2)->nullable()->default('0');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Temp_Member_Account_Fund_Performance');
    }
}
