<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbCity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city', function (Blueprint $table) {
            $table->increments('cityid');
          
            $table->string('citycode',5);
            $table->string('cityname',100);
            $table->string('statecode',10)->nullable();
            $table->string('languagecode',20);
            $table->char('countrycode',3)->nullable();

            $table->timestamps();

            
         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city');
    }
}
