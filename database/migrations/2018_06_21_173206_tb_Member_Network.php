<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbMemberNetwork extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Member_Network', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_id')->nullable(false);
            $table->string('username',255)->nullable();
            $table->integer('parent_id')->nullable();
            $table->string('parent_username',200)->nullable();
            $table->integer('parent_level')->nullable();
            $table->integer('my_level')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Member_Network');
    }
}
