<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbHelpdeskLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('helpdesk_log', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('helpdesk_id')->unsigned()->nullable(false);
            $table->integer('helpdesk_type')->unsigned()->nullable(false);     
            $table->string('cs_username',255)->nullable();
            $table->integer('cs_id')->nullable(false)->unsigned();
            $table->integer('cs_type')->nullable(false);
            $table->string('action',10)->nullable();
            $table->text('comment')->nullable();
            $table->integer('to_cs_id')->nullable();
            $table->string('to_cs_username',255)->nullable();
            $table->string('pic_1',255)->nullable();
            $table->string('pic_2',255)->nullable();
            $table->string('_token',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('helpdesk_log');
    }
}
