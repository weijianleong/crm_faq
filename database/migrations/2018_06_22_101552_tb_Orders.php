<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Orders', function (Blueprint $table) {
            $table->increments('orderid');
            $table->string('member_username',200)->nullable();
            $table->integer('bookB')->nullable(false);
            $table->integer('bookAorderid')->nullable();
            $table->char('status',2)->default('N');
            $table->decimal('balancea', 16, 2)->nullable();
            $table->decimal('profitA', 16, 2)->nullable();
            $table->decimal('balanceb', 16, 2)->nullable();
            $table->decimal('profitB', 16, 2)->nullable();
            $table->decimal('updatebalanceb', 16, 2)->nullable();
            $table->decimal('updatecreditb', 16, 2)->nullable();
            $table->decimal('rwallet', 16, 2)->nullable();
            $table->integer('checkstatus')->nullable();
            $table->char('checkby',200)->nullable();
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Orders');
    }
}
