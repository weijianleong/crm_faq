<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbMemberSales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Member_Sales', function (Blueprint $table) {
            $table->increments('member_id');

                        
            $table->string('username',255)->nullable();

            $table->decimal('totaltransfer', 16, 2)->default(0);
            $table->decimal('totallotsize', 16, 2)->default(0);
            $table->decimal('totaldeposit', 16, 2)->default(0);
            $table->decimal('balanceb', 16, 2)->default(0);
            $table->char('rank',5)->nullable();
            $table->integer('conditions')->nullable();
            $table->char('status',2)->nullable();
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Member_Sales');
    }
}
