<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbRwalletLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Rwallet_log', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('member_id')->nullable();
            $table->integer('orderid')->nullable();
            $table->integer('bookA')->nullable();
            $table->decimal('r_amount', 10, 2)->nullable();
            $table->char('type',3)->nullable();
            $table->TEXT('remark')->nullable();
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Rwallet_log');
    }
}
