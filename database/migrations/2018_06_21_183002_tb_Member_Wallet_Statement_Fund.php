<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbMemberWalletStatementFund extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Member_Wallet_Statement_Fund', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('member_id')->nullable(false)->unsinged()->index();
            $table->string('member_username',200)->nullable();
            $table->integer('admin_id')->nullable();
            $table->string('username',255)->nullable();
            $table->string('action_type',100)->nullable();
            $table->char('wallet_type',2)->nullable();
            $table->char('transaction_type',2)->nullable();
            
            $table->decimal('cash_amount', 16, 2)->nullable()->default(0);
            $table->decimal('register_amount', 16, 2)->nullable()->default(0);
            $table->decimal('promotion_amount', 16, 2)->nullable()->default(0);
            $table->decimal('b_amount', 16, 2)->nullable()->default(0);
            $table->decimal('w_amount', 16, 2)->nullable()->default(0);
            $table->decimal('wd_amount', 16, 2)->nullable()->default(0);
            $table->decimal('balance', 16, 2)->nullable();
            $table->text('remark')->nullable();
            $table->integer('bookA')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Member_Wallet_Statement_Fund');
    }
}
