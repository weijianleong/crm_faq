<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbTempChangeFundmanage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_change_fundmanage', function (Blueprint $table) {
           

            $table->integer('bookA')->nullable();
            $table->integer('fundcompany')->nullable();
            $table->integer('tradestatus')->nullable()->default('0');
            $table->integer('profit')->nullable()->default('0');
            $table->integer('batch')->nullable();
            $table->char('status',2)->nullable()->default('N');
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_change_fundmanage');
    }
}
