<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbNotificationTarget extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_target', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_id')->nullable(false)->index();
            $table->integer('type')->nullable(false)->index()->comment('0 -system notification');
            $table->string('subject_chs',30)->nullable();
            $table->string('subject_en',100)->nullable();
            $table->string('content_chs',500)->nullable();
            $table->string('content_en',1500)->nullable();
            $table->string('from',200)->nullable();
            $table->string('attachments',200)->nullable();
            $table->integer('read')->nullable(false)->default('0')->comment('0 -unread , 1-read');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_target');
    }
}
