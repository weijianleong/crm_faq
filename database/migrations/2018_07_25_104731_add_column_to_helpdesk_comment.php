<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToHelpdeskComment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('helpdesk_comment', function (Blueprint $table) {
            //
            $table->integer('reply_sec')->nullable()->after('pic_2');
        });
        Schema::table('helpdesk_log', function (Blueprint $table) {
            //
            $table->integer('reply_sec')->nullable()->after('pic_2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('helpdesk_comment', function (Blueprint $table) {
            //
            //Schema::dropIfExists('reply_sec');
            $table->dropColumn('reply_sec');

        });
        Schema::table('helpdesk_log', function (Blueprint $table) {
            //
            //Schema::dropIfExists('reply_sec');
            $table->dropColumn('reply_sec');
        });
    }
}
