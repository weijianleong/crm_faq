<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbMemberAccountWithdrawal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Member_Account_Withdrawal', function (Blueprint $table) {
            $table->increments('id');

            $table->string('username',100)->nullable();
            $table->string('name',100)->nullable();
            $table->integer('fund_account')->unsigned()->nullable();
            $table->integer('member_id')->nullable();

            $table->integer('status');
            $table->decimal('balanceA',16,2)->nullable();
            $table->decimal('balanceB',16,2)->nullable();
            $table->char('system_manage', 2)->nullable();
            $table->integer('mam_account')->nullable();
            $table->integer('withdrawal_type')->default('0');
            $table->integer('process_status')->default('0');
            $table->decimal('w_wallet',16,2)->default('0');
            $table->decimal('total_withdrawal',16,2)->default('0');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Member_Account_Withdrawal');
    }
}
