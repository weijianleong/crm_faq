<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbMwalletLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Mwallet_log', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('member_id')->nullable();
            $table->integer('orderid')->nullable()->index();
            $table->integer('partnerid')->nullable();
            $table->decimal('mwallet', 10, 2)->nullable();
            $table->dateTime('opentime')->nullable();
            $table->dateTime('closetime')->nullable();
            $table->string('remark',200)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Mwallet_log');
    }
}
