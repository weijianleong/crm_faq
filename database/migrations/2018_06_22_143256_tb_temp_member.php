<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbTempMember extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_member', function (Blueprint $table) {
            

            $table->string('id',5)->nullable();
            $table->string('Sponsor ID',18)->nullable();
            $table->string('Name',14)->nullable();
            $table->string('Ic number',19)->nullable();
            $table->string('Sex',1)->nullable();
            $table->string('Emails',23)->nullable();
            $table->string('Phone',15)->nullable();
            $table->string('Country',2)->nullable();
            $table->string('Address',72)->nullable();
            $table->string('User ID',18)->nullable();
            $table->string('Password',15)->nullable();
            $table->string('Secure',15)->nullable();
            $table->string('Image',32)->nullable();
            $table->string('Ac1',6)->nullable();
            $table->string('Ac2',6)->nullable();
            $table->string('Join Date',19)->nullable();
            $table->string('eCash',7)->nullable();
            $table->string('Rwallet',7)->nullable();
            $table->string('BookA',7)->nullable();
            $table->string('BookB',7)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_member');
    }
}
