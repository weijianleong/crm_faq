<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbDepositLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposit_log', function (Blueprint $table) {
            $table->increments('payid');
            $table->string('TransID',32)->nullable();
            $table->tinyInteger('type')->nullable();
            $table->decimal('OrderMoney',32,2)->nullable(false);
            $table->decimal('fundmoney',32,2)->nullable(false);
            $table->Integer('wid')->nullable(false);
            $table->Integer('login')->length(255)->nullable();
            $table->string('created',16)->nullable(false);
            $table->Integer('paybank')->length(255)->nullable(false);
            $table->Integer('paybankcode')->length(255)->nullable();
            $table->smallInteger('status')->nullable();
            $table->dateTime('deal_time')->nullable();
            $table->string('bfno',32)->nullable();
            $table->string('note',255)->nullable();
            $table->string('cname',255)->nullable();
            $table->string('memo',255)->nullable();
            $table->string('token',255)->nullable();
            $table->string('optIp',50)->nullable();
            $table->string('invoiceno',50)->nullable();
            
            $table->timestamps();

            
            $table->index(['type','TransID','status'],'tys_index');
            $table->unique(['TransID', 'type'],'TransID');
 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit_log');
    }
}
