<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbTempSwapChangeDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_swap_change_date', function (Blueprint $table) {
            $table->integer('OrderId')->nullable();
            $table->integer('Login')->nullable();
            $table->dateTime('OpenTime')->nullable();
            $table->dateTime('CloseTime')->nullable();
            $table->string('Symbol',255)->nullable();
            $table->decimal('Lotsize',16,2)->nullable();
            $table->decimal('Profit',16,2)->nullable();
            $table->decimal('Swap',16,2)->nullable();
            $table->char('Status',2)->nullable()->default("N");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_swap_change_date');
    }
}
