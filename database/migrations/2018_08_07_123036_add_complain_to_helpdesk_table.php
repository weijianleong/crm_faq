<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddComplainToHelpdeskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('helpdesk', function (Blueprint $table) {

            Schema::table('helpdesk', function (Blueprint $table) {
                //
                $table->integer('is_complain')->nullable(false)->default('0')->after('rate_star');
            });
            Schema::table('helpdesk', function (Blueprint $table) {
                //
                $table->string('complain',200)->nullable()->after('is_complain');
            });
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('helpdesk', function (Blueprint $table) {
            //
            $table->dropColumn('is_complain');
        });
        Schema::table('helpdesk', function (Blueprint $table) {
            //
            $table->dropColumn('complain');
        });
    }
}
