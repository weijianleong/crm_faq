<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbTransfer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Transfer', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('from_member_id')->nullable(false)->default('0')->unsinged();
            $table->integer('to_member_id')->nullable(false)->unsinged();
            $table->string('from_username',255)->nullable();
            $table->string('to_username',255)->nullable();
            $table->string('type',50)->nullable();
            $table->decimal('amount',16,2)->nullable(false)->default('0');
            $table->text('remark')->nullable();

            $table->index(['from_member_id','to_member_id'])->unsinged();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Transfer');
    }
}
