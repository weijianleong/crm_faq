<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbMemberAccountFund extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Member_Account_Fund', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('member_id')->nullable();
            $table->integer('bookA')->nullable()->index();
            $table->integer('bookB')->nullable()->index();
            $table->char('system_manage', 2)->default('N');
            $table->integer('status')->default('0');
            $table->dateTime('lockdate')->nullable();
            $table->decimal('bwallet',16,2)->nullable();
            $table->integer('winA')->default('0');
            $table->integer('winB')->default('0');
            $table->string('updated_by',200)->nullable();
            $table->decimal('balanceA',16,2)->default('0');
            $table->decimal('balanceB',16,2)->default('0');
            $table->integer('fundcompany')->nullable();
            $table->dateTime('subscribe_date')->nullable();
            $table->dateTime('unsubscribe_date')->nullable();

            $table->string('partner_code',32)->nullable();
            $table->string('manager_code',32)->nullable();
            $table->string('trader_code',32)->nullable();
            $table->tinyInteger('partner_status')->nullable();
            $table->bigInteger('mam_account')->nullable();
            $table->tinyInteger('account_type')->nullable();
            $table->char('chgpass', 2)->default('N');
            $table->char('chgpass2', 2)->default('N');
            $table->char('chggroup', 2)->default('N');
            $table->char('changefund', 2)->default('N');
            $table->decimal('rbalance',16,2);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Member_Account_Fund');
    }
}
