<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbMemberTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Member_Transaction', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('member_id')->nullable();
            $table->integer('account_id')->nullable();
            $table->integer('bookA')->nullable();
            $table->decimal('bookAamount', 16, 2)->nullable()->default(0);
            $table->decimal('bookAbalance', 16, 2)->nullable()->default(0);
            $table->decimal('bookAprofit', 16, 2)->nullable()->default(0);
            $table->integer('bookAorderid')->nullable();
            $table->integer('bookB')->nullable();
            $table->decimal('bookBamount', 16, 2)->nullable()->default(0);
            $table->decimal('bookBbalance', 16, 2)->nullable()->default(0);
            $table->decimal('rwallet', 16, 2)->nullable()->default(0);
            $table->char('action_type',10)->nullable();
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Member_Transaction');
    }
}
