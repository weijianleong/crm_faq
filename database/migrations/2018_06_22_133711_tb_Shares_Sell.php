<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbSharesSell extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Shares_Sell', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('member_id')->nullable(false)->default('0')->index()->unsigned();
            $table->integer('amount')->nullable(false)->default('0')->unsigned();
            $table->integer('amount_left')->nullable(false)->default('0')->unsigned();
            $table->decimal('share_price', 16, 4)->nullable(false)->default('0');
            $table->decimal('total', 16, 4)->nullable(false)->default('0');
            $table->decimal('cash_point', 16, 4)->nullable(false)->default('0');
            $table->decimal('md_point', 16, 4)->nullable(false)->default('0');
            $table->decimal('purchase_point', 16, 4)->nullable(false)->default('0');
            $table->decimal('admin_fee', 16, 4)->nullable(false)->default('0');
            $table->tinyInteger('has_process')->nullable(false)->default('0');
            $table->tinyInteger('is_admin')->nullable(false)->default('0');

            $table->index(['share_price','has_process']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Shares_Sell');
    }
}
