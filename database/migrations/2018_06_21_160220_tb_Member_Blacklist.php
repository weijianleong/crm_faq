<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbMemberBlacklist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Member_Blacklist', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('member_id')->nullable()->unsigned()->default('0');
            $table->string('first_name',191)->nullable();
            $table->string('username',255)->nullable();
            $table->string('secret_password',255)->nullable();
            $table->integer('level')->default('1');
            $table->integer('direct_id')->nullable()->unsigned();
            $table->integer('is_due_to_ban')->nullable(false)->unsigned()->default('0');
            $table->string('lpoa_status',2)->default('N');
            $table->integer('withdrawal_type')->default('0');
            $table->decimal('a_book',10,2)->default('0');
            $table->decimal('b_book',10,2)->default('0');
            $table->decimal('b_amount',10,2)->default('0');
            $table->integer('type')->default('0');
            $table->dateTime('blacklist_at');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Member_Blacklist');
    }
}
