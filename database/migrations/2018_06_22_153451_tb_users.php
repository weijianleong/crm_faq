<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email',191)->nullable(false);
            $table->string('username',191)->nullable(false)->unique();
            $table->string('phone',50)->nullable();
            $table->string('password',191)->nullable(false);
            $table->string('password2',100)->nullable(false);
            $table->string('adminname',255)->nullable();
            $table->text('permissions')->nullable();
            $table->integer('is_ban')->nullable()->default('1');
            $table->dateTime('last_login')->nullable();
            $table->string('first_name',191)->nullable();
            $table->string('last_name',191)->nullable();
            $table->char('leader',2)->nullable()->default('N');
            $table->char('superleader',2)->nullable()->default('N');
            $table->char('fundmanage',2)->nullable()->default('N');
            $table->integer('permission_type')->nullable()->default('1');
            $table->char('cs_type',255)->nullable()->default('99');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
