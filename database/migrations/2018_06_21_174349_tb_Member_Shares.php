<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbMemberShares extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Member_Shares', function (Blueprint $table) {
            $table->increments('member_id');


            $table->integer('amount')->nullable(false)->unsigned()->default(0);
            $table->integer('share_limit')->nullable(false)->unsigned()->default(0);
            $table->decimal('current_sales', 8, 2)->nullable(false)->default(0);
            $table->decimal('max_share_sale', 8, 2)->nullable(false)->default(0);;
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Member_Shares');
    }
}
