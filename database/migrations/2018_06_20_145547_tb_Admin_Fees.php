<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbAdminFees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('Admin_Fees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username',255)->nullable();
            $table->string('type',191)->nullable();
            $table->decimal('amount', 16, 4)->nullable(false)->default('0.0000');
            $table->timestamps();
            
            $table->index('type');
            $table->index('created_at');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('Admin_Fees');
    }
}
