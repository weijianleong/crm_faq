<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbTempLotsize9 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_lotsize9', function (Blueprint $table) {
            $table->integer('Deal')->nullable();
            $table->integer('Login')->nullable();
            $table->string('Email',255)->nullable();
            $table->string('Name',255)->nullable();
            $table->string('Time',255)->nullable();
            $table->string('Type',255)->nullable();
            $table->string('Symbol',255)->nullable();
            $table->decimal('Volume', 16, 2)->nullable();
            $table->integer('Commission')->nullable();
            $table->string('Swap',255)->nullable();
            $table->string('Profit',255)->nullable();
            $table->string('Currency',255)->nullable();
            $table->integer('member_id')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_lotsize9');
    }
}
