<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShiftToScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('helpdesk_schedule', function (Blueprint $table) {
            //
            $table->integer('shift')->nullable(false)->default('0')->after('work')->comment('0 -no shift , 1 - am , 1 - pm');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('helpdesk_schedule', function (Blueprint $table) {
            //
            $table->dropColumn('shift');
        });
    }
}
