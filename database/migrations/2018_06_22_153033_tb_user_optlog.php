<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbUserOptlog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_optlog', function (Blueprint $table) {
            $table->increments('id');

            $table->string('adminname',255)->nullable();
            $table->string('adminmemo',8000)->nullable(false);
            $table->dateTime('admintime')->nullable(false);
            $table->string('record_sql',255)->nullable(false);
            $table->integer('memberId')->nullable();
            $table->string('TransID',32)->nullable();
            $table->string('transmemo',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_optlog');
    }
}
