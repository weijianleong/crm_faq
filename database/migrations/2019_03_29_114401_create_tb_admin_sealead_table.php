<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbAdminSealeadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_sealead', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('member_id')->nullable(false)->index();
            $table->string('username',191)->nullable();
            $table->integer('type')->nullable(false)->default('1')->comment('1-withdrawal, 2 -deposit');
            $table->integer('status')->nullable(false)->default('1')->comment('1-pending, 2 -approvel, 3-reject');
            $table->integer('payment_type')->nullable(false)->default('1')->comment('1-bank, 2 -card');
            $table->string('bank_name',255)->nullable();
            $table->string('bank_account_number',255)->nullable();
            $table->string('bank_account_holder',255)->nullable();
            $table->string('bank_swiftcode',255)->nullable();
            $table->string('bank_country',255)->nullable();
            $table->string('bank_address',255)->nullable();
            $table->string('card_number',255)->nullable();
            $table->string('card_country',255)->nullable();
            $table->string('receipt',255)->nullable();
            $table->decimal('amount',16,4)->nullable(false)->default('0');
            $table->string('uniqueId',100)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_sealead');
    }
}
