<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbMemberDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Member_Detail', function (Blueprint $table) {
            $table->increments('member_id');

            $table->enum('gender', ['male', 'female'])->nullable(false)->default('male');
            $table->string('phone1',50)->nullable();
            $table->string('phone2',50)->nullable();
            $table->string('mobile_phone',50)->nullable();
            $table->string('bank_name',100)->nullable();
            $table->string('bank_account_number',100)->nullable();
            $table->string('bank_account_holder',255)->nullable();
            $table->string('bank_branch',255)->nullable();
            $table->string('spouse_name',255)->nullable();
            $table->string('spouse_dob',255)->nullable();
            $table->string('identification_number',255)->nullable();
            $table->string('nationality',100)->nullable();
            $table->string('date_of_birth',100)->nullable();
            $table->string('beneficiary_name',255)->nullable();
            $table->string('beneficiary_nationality',100)->nullable();
            $table->text('address')->nullable();
            $table->text('bank_address')->nullable();
            $table->string('bank_country',255)->nullable();
            $table->string('bank_swiftcode',255)->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Member_Detail');
    }
}
