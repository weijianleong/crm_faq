<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbHelpdeskComment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('helpdesk_comment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('helpdesk_id')->unsigned()->nullable(false);
            $table->string('helpdesk_type',255)->nullable();  
            $table->integer('cs_id')->nullable();
            $table->string('cs_username',255)->nullable();
            $table->string('cs_type',255)->nullable();
            $table->integer('client_id')->nullable();
            $table->string('client_username',255)->nullable();
            $table->text('comment')->nullable();
            $table->string('pic_1',255)->nullable();
            $table->string('pic_2',255)->nullable();
            $table->string('_token',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('helpdesk_comment');
    }
}
