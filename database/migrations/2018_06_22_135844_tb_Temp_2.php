<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbTemp2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Temp_2', function (Blueprint $table) {

            $table->integer('member_id')->nullable();
            $table->integer('orderid')->nullable(false)->default('0');
            $table->integer('booka')->nullable();
            $table->decimal('amount', 10, 2)->nullable();
            $table->char('status',2)->nullable()->default('N');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Temp_2');
    }
}
