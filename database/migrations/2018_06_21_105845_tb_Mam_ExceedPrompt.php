<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbMamExceedPrompt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Mam_ExceedPrompt', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('to_cs_id')->nullable();
            $table->string('PromptContent',2000)->nullable();
            $table->string('ReadName',50)->nullable();
            $table->bigInteger('MamAccount')->nullable();
            $table->bigInteger('Login')->nullable();
            $table->tinyInteger('status')->default('0');
            $table->string('LastOptUser',50)->nullable();
            $table->dateTime('Create_At')->useCurrent();
            $table->integer('PromptType')->nullable()->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Mam_ExceedPrompt');
    }
}
