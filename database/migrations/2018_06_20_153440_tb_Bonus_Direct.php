<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbBonusDirect extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Bonus_Direct', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_id')->unsigned()->nullable(false);
            $table->string('username',255)->nullable();
            $table->string('from_username',255)->nullable();
            $table->decimal('amount_cash', 16, 2)->nullable(false)->default(0.00);
            $table->decimal('amount_promotion', 16, 2)->nullable(false)->default(0.00);
            $table->decimal('total', 8, 2)->nullable(false)->default(0.00);
            $table->timestamps();


            $table->index('member_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Bonus_Direct');
    }
}
