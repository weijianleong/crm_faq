<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbSharesSellStatement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Shares_Sell_Statement', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('member_id')->nullable(false)->default('0')->unsigned();
            $table->integer('sell_id')->nullable(false)->default('0')->unsigned();
            $table->integer('amount')->nullable(false)->default('0')->unsigned();
            $table->string('status',50)->nullable();
            $table->decimal('share_price', 16, 4)->nullable(false)->default('0');
            $table->decimal('cash_point', 16, 4)->nullable(false)->default('0');
            $table->decimal('md_point', 16, 4)->nullable(false)->default('0');
            $table->decimal('purchase_point', 16, 4)->nullable(false)->default('0');
            $table->decimal('admin_fee', 16, 4)->nullable(false)->default('0');


            $table->index(['member_id','sell_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Shares_Sell_Statement');
    }
}
