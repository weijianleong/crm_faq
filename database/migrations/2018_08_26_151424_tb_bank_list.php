<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbBankList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_list', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('bank_code')->nullable(false)->index();
            $table->integer('bank_type')->nullable(false)->comment('1=FocalBank，2=GPayBank,3=DinPayBank,4=WithdrawalBank,5=WithdrawalGPayBank,6=XPayBank,7=PayCVBank,8=PaySecBank,9=RpnBank,10=IFEEPayBank')->index();
            $table->string('bank_value',200)->nullable(false);
            $table->string('bank_name',200)->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_list');
    }
}
