<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbSharesBuy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Shares_Buy', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('member_id')->nullable(false)->unsinged()->default('0')->index();
            $table->integer('amount')->nullable(false)->unsinged()->default('0');
            $table->integer('amount_left')->nullable(false)->unsinged()->default('0');
            $table->decimal('share_price', 16, 4)->nullable(false)->default('0');
            $table->decimal('total', 16, 4)->nullable(false)->default('0');
            $table->tinyInteger('has_process')->nullable(false)->default('0');

            $table->index(['share_price','has_process']);
            
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Shares_Buy');
    }
}
