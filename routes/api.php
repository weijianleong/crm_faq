<?php

use Illuminate\Http\Request;
use App\Models\Member;
use App\Repositories\MemberRepository;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('TradeStatus', function(Request $request) {
    $data = $request->json()->all();
    DB::table('API_log')->insert([ 'API_type' => 'TradeStatus', 'API_desc' => json_encode($data), 'created_at' => \Carbon\Carbon::now() ]);
                
    $book = DB::table('Member_Account_Fund')->where('system_manage', '=', 'S')->where('bookA', '=', $data['login'])->first();
    
    if(count($book)>0){
        if($data['type'] == 'Open'){
            DB::table('Orders_Fund')->insert([ 'member_id' => $book->member_id, 'bookA' => $book->bookA, 'bookAorderid' => $data['orderid'], 'status' => 'O', 'created_at' => \Carbon\Carbon::now() ]);
            DB::table('Member_Account_Fund')->where('bookA', $book->bookA)->update([ 'status' => -2, 'updated_at' => \Carbon\Carbon::now() ]);
        }
    
        if($data['type'] == 'Close'){
            DB::table('Orders_Fund')->where('bookAorderid', $data['orderid'])->update([ 'status' => 'C', 'updated_at' => \Carbon\Carbon::now() ]);
            DB::table('Member_Account_Fund')->where('bookA', $book->bookA)->update([ 'status' => -1, 'updated_at' => \Carbon\Carbon::now() ]);
        }
    }
    
    return array( 'status' => 200, 'status_code' => "SUCCESS" );
});






// API for TRCap
Route::group(['prefix' => 'v1'], function(){

    Route::get('testcallapi', ['as' => 'testcallapi', 'uses' => 'Api\V1\CallCapxController@testcallapi']);
    Route::get('getBalance', ['as' => 'testcallapi', 'uses' => 'Api\V1\CallCapxController@getBalance']);
    Route::get('getCoinPrice', ['as' => 'testcallapi', 'uses' => 'Api\V1\CallCapxController@getCoinPrice']);
    Route::get('loginRedirect', ['as' => 'loginRedirect', 'uses' => 'Api\V1\CallCapxController@loginRedirect']);
    Route::get('transferToCapx', ['as' => 'transferToCapx', 'uses' => 'Api\V1\CallCapxController@transferToCapx']);
    Route::get('profileUpdateByField', ['as' => 'profileUpdateByField', 'uses' => 'Api\V1\CallCapxController@profileUpdateByField']);
    Route::get('register', ['as' => 'capx.register', 'uses' => 'Api\V1\CallCapxController@register']);
    Route::get('pushTrMarginPrice', ['as' => 'pushTrMarginPrice', 'uses' => 'Api\V1\CallCapxController@pushTrMarginPrice']);


    Route::post('getLinkedKey', ['middleware' => 'partnerAuth','as' => 'getLinkedKey', 'uses' => 'Api\V1\CapxController@getLinkedKey']);
    Route::post('validation', ['middleware' => 'partnerAuth','as' => 'validation', 'uses' => 'Api\V1\CapxController@validation']);
    Route::post('fundTransfer', ['middleware' => 'partnerAuth','as' => 'fundTransfer', 'uses' => 'Api\V1\CapxController@fundTransfer']);
    Route::post('updateBalance', ['middleware' => 'partnerAuth','as' => 'updateBalance', 'uses' => 'Api\V1\CapxController@updateBalance']);
    Route::post('sealeadDeposit', ['middleware' => 'partnerAuth','as' => 'sealeadDeposit', 'uses' => 'Api\V1\CapxController@insertSealeadDeposit']);
    Route::post('sealeadWithdrawal', ['middleware' => 'partnerAuth','as' => 'sealeadWithdrawal', 'uses' => 'Api\V1\CapxController@insertWithdrawal']);

    // Route::post('updateProfile', ['as' => 'updateProfile', 'uses' => 'Api\V1\CapxController@updateProfile']);
    Route::post('checkAPIStatus', ['as' => 'checkAPIStatus', 'uses' => 'Api\V1\CapxController@checkAPIStatus']);


});
