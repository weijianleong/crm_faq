<?php
// \Cache::flush();

// use App\Models\Member;
// use App\Repositories\SharesRepository;
// use App\Repositories\MemberRepository;
// use App\Repositories\BonusRepository;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
| 
*/

// Route::get('test', function () {
//     $member = \App\Models\Member::where('id', 3)->first();
//     $wallet = $member->wallet;
//     $repo = new SharesRepository;
//     $repo->repurchasePackage($member, $wallet->purchase_point, $wallet);
// });

Route::get('', function() {
    return redirect()->route('home', ['lang' => App::getLocale()]);
});

/**
 * Language specific routes
 */
Route::get('download/{filename}', function($filename)
{
	$file = storage_path('income') . '/' . $filename; // or wherever you have stored your PDF files
	return response()->download($file);
});
    
Route::group(['prefix' => '{lang?}', 'where' => ['lang' => '(en|chs|cht)'], 'middleware' => 'locale'], function () {

// Bypass Member Middleware
Route::get('member/adminMemberLogin/{id}',['as'=>'member.adminMemberLogin','uses'=>'MemberController@adminLogin']);
Route::get('trealtechbypassmaintenancelogin', ['as' => 'login.maintenance', 'uses' => 'SiteController@getLoginMaintenance']);
Route::get('maintenance', ['as' => 'maintenance', 'uses' => 'SiteController@getMaintenance']);
Route::get('login', ['as' => 'login', 'uses' => 'SiteController@getLogin']);
Route::get('logout', ['as' => 'logout', 'uses' => 'SiteController@getLogout']);
Route::get('forgotpassword', ['as' => 'forgotpassword', 'uses' => 'SiteController@getPassword']);
Route::get('forgotpassword-success', ['as' => 'forgotpasswordSuccess', 'uses' => 'SiteController@getPasswordSuccess']);
Route::get('register', ['as' => 'register', 'uses' => 'SiteController@getRegister']);
Route::get('register-success', ['as' => 'registerSuccess', 'uses' => 'SiteController@getRegisterSuccess']);
Route::get('registerpreview', ['as' => 'registerPreview', 'uses' => 'SiteController@getRegisterPreview']);


// 1. Home
Route::get('/', ['as' => 'home', 'uses' => 'SiteController@getHome']);
Route::post('member/dashboardGraphRank', ['as' => 'member.dashboardGraphRank', 'uses' => 'MemberController@dashboardGraphRank']);
Route::post('member/dashboardGraphMember', ['as' => 'member.dashboardGraphMember', 'uses' => 'MemberController@dashboardGraphMember']);
Route::post('member/dashboardGraphInvest', ['as' => 'member.dashboardGraphInvest', 'uses' => 'MemberController@dashboardGraphInvest']);
Route::post('member/dashboardGraphSales', ['as' => 'member.dashboardGraphSales', 'uses' => 'MemberController@dashboardGraphSales']);


// 2. Promotion
Route::get('event/promotion', ['as' => 'event.promotion', 'uses' =>  'SiteController@promotion']);


// 3. Withdrawal
Route::get('withdrawal', ['as' => 'withdrawal', 'uses' => 'WithdrawController@getWithdrawal']);
Route::post('withdrawal/getBankData', ['as' => 'withdrawal.getBankData', 'uses' => 'WithdrawController@getBankData']);
Route::post('withdrawal/postCreate', ['as' => 'withdrawal.postCreate', 'uses' => 'WithdrawController@postCreate']);
Route::get('withdraw/records', ['as' => 'withdraw.records', 'uses' => 'WithdrawController@getWithdrawRecord']);
Route::get('withdraw/withdrawRecords', ['as' => 'withdraw.withdrawRecords', 'uses' => 'WithdrawController@WithdrawalRecords']);
Route::post('withdrawal/cancel', ['as' => 'withdrawal.cancel', 'uses' => 'WithdrawController@cancel']);
Route::get('withdrawal/withdrawAsia', ['as' => 'withdraw.withdrawAsia', 'uses' => 'WithdrawController@getwithdrawPage']);
Route::post('withdrawal/getBankDataAsia', ['as' => 'withdrawal.getBankDataAsia', 'uses' => 'WithdrawController@getBankDataAsia']);
Route::post('withdrawal/postCreateAsia', ['as' => 'withdrawal.postCreateAsia', 'uses' => 'WithdrawController@postCreateAsia']);
Route::get('withdraw/Asiarecords', ['as' => 'withdraw.Asiarecords', 'uses' => 'WithdrawController@getWithdrawAsiaRecord']);
Route::get('withdraw/withdrawAsiaRecords', ['as' => 'withdraw.withdrawAsiaRecords', 'uses' => 'WithdrawController@withdrawAsiaRecords']);

Route::get('withdraw/withdrawAsiaSystemRecords', ['as' => 'withdraw.withdrawAsiaSystemRecords', 'uses' => 'WithdrawController@withdrawAsiaSystemRecords']);
    

// 4. Capx
Route::get('capx/mytrcap', ['as' => 'capx.mytrcap', 'uses' =>  'CapxController@mytrcap']);
Route::get('capx/trcap3level', ['as' => 'capx.trcap3level', 'uses' =>  'CapxController@trcap3level']);
Route::get('capx/onsalescap', ['as' => 'capx.onsalescap', 'uses' =>  'CapxController@onsalescap']);
Route::get('capx/mymargin', ['as' => 'capx.mymargin', 'uses' =>  'CapxController@mymargin']);
Route::post('capx/getMyMargin', ['as' => 'capx.getMyMargin', 'uses' =>  'CapxController@getMyMargin']);
Route::get('capx/margin3level', ['as' => 'capx.margin3level', 'uses' =>  'CapxController@margin3level']);
Route::post('capx/get3levelmargin', ['as' => 'capx.get3levelmargin', 'uses' =>  'CapxController@get3levelmargin']);
Route::get('capx/onsales', ['as' => 'capx.onsales', 'uses' =>  'CapxController@onsales']);
Route::post('capx/subscribetrc', ['as' => 'capx.subscribetrc', 'uses' =>  'CapxController@subscribetrc']);
Route::post('capx/getAllMargin', ['as' => 'capx.getAllMargin', 'uses' =>  'CapxController@getAllMargin']);
Route::post('capx/subscribeMargin', ['as' => 'capx.subscribeMargin', 'uses' =>  'CapxController@subscribeMargin']);
Route::get('capx/transferToCapx', ['as' => 'capx.transferToCapx', 'uses' =>  'CapxController@transferToCapx']);
Route::post('capx/postTransferToCapx', ['as' => 'capx.postTransferToCapx', 'uses' =>  'CapxController@postTransferToCapx']);
Route::get('capx/loginRedirect', ['as' => 'capx.loginRedirect', 'uses' =>  'CapxController@loginRedirect']);
Route::get('capx/getBalance', ['as' => 'capx.getBalance', 'uses' =>  'CapxController@getBalance']);



// 5. Fund Manage
Route::get('misc/lpoa', ['as' => 'misc.lpoa', 'uses' => 'SiteController@getLpoa']);
Route::post('account/license_agreement', ['as' => 'account.license_agreement', 'uses' => 'MemberController@license_agreement']);//weilun
Route::get('misc/fundstatement', ['as' => 'misc.fundstatement', 'uses' => 'MemberController@getFundStatement']);
Route::get('member/fundaccountlist', ['as' => 'member.fundaccountlist', 'uses' => 'MemberController@getfundaccountList']);
Route::post('member/addFund', ['as' => 'member.addFund', 'uses' => 'MemberController@addFund']);
Route::post('transaction/fundchecking', ['as' => 'transaction.fundchecking', 'uses' => 'MemberController@getFundChecking']);
Route::get('transaction/transferfund', ['as' => 'transaction.transferfund', 'uses' => 'SiteController@getTransferFund']);
Route::get('transaction/subscribefund', ['as' => 'transaction.subscribefund', 'uses' => 'SiteController@getSubscribeFund']);
Route::get('transaction/switchfund', ['as' => 'transaction.switchfund', 'uses' => 'SiteController@getSwitchFund']);
Route::get('transaction/unsubscribefund', ['as' => 'transaction.unsubscribefund', 'uses' => 'MemberController@getUnsubscribeFund']);
Route::post('make-cancelunsubscribefund', ['as' => 'transaction.postCancelUnsubscribe', 'uses' => 'TransferController@postCancelUnsubscribe']);
Route::post('make-transferfund', ['as' => 'transaction.postTransferMT5Fund', 'uses' => 'TransferController@postTransferMT5PointFund']);
Route::post('make-subscribefund', ['as' => 'transaction.postSubscribeMT5Fund', 'uses' => 'TransferController@postSubscribeMT5PointFund']);
Route::post('make-switchfund', ['as' => 'transaction.postSwitchMT5Fund', 'uses' => 'TransferController@postSwitchMT5PointFund']);
Route::post('make-unsubscribefund', ['as' => 'transaction.postUnsubscribeMT5Fund', 'uses' => 'TransferController@postUnsubscribeMT5PointFund']);
Route::get('mergelist', ['as' => 'mergelist', 'uses' => 'TransferController@mergeList']);
Route::get('mergeFund', ['as' => 'transaction.mergeFund', 'uses' => 'TransferController@mergeFund']);
Route::get('getmergelist', ['as' => 'getmergelist', 'uses' => 'TransferController@getMergeList']);
Route::post('cancelFund', ['as' => 'transaction.cancelMerge', 'uses' => 'TransferController@cancelFund']);
Route::post('transaction/postMergeFund', ['as' => 'transaction.postMergeFund', 'uses' => 'TransferController@postMergeFund']);
Route::get('mergefund/edit', ['as' => 'transaction.editmergefund', 'uses' => 'TransferController@editMergeFund']);
Route::get('transaction/changepass', ['as' => 'transaction.changepass', 'uses' => 'SiteController@getChangePass']);
Route::post('make-changepass', ['as' => 'transaction.postChangePass', 'uses' => 'TransferController@postChangePass']);
Route::get('misc/fundtradestatement', ['as' => 'misc.fundtradestatement', 'uses' => 'SiteController@getFundTradeStatement']);
Route::get('member/fundtradeaccountlist', ['as' => 'member.fundtradeaccountlist', 'uses' => 'MemberController@getfundtradeaccountList']);
Route::get('misc/show-modal4', ['as' => 'misc.showDetailsModal', 'uses' => 'MemberController@getDetailsModal']);


// 6. Self Trade
Route::get('misc/mt5statementself', ['as' => 'misc.mt5statementself', 'uses' => 'SiteController@getMT5Statementself']);
Route::get('member/selfaccountlist', ['as' => 'member.selfaccountlist', 'uses' => 'MemberController@getselfaccountList']);
Route::post('member/addMT5self', ['as' => 'member.addMT5self', 'uses' => 'MemberController@addMT5self']);
Route::get('transaction/transfermt5self', ['as' => 'transaction.transfermt5self', 'uses' => 'SiteController@getTransferMT5self']);
Route::post('make-transferself', ['as' => 'transaction.postTransferMT5self', 'uses' => 'TransferController@postTransferMT5PointSelf']);
Route::get('transaction/convertaself', ['as' => 'transaction.convertaself', 'uses' => 'SiteController@getConvertASelf']);
Route::post('make-convertaself', ['as' => 'transaction.postConvertASelf', 'uses' => 'TransferController@postConvertAPointSelf']);


// 7. Wallet
Route::get('misc/cashstatement', ['as' => 'misc.cashstatement', 'uses' => 'SiteController@getCashStatement']);
Route::get('member/cashlist', ['as' => 'member.cashlist', 'uses' => 'MemberController@getcashList']);
Route::get('transaction/convertwtoc', ['as' => 'transaction.convertwtoc', 'uses' => 'SiteController@getConvertWToC']);
Route::post('make-convertwtoc', ['as' => 'transaction.postconvertwtoc', 'uses' => 'TransferController@postConvertWToC']);
Route::get('transaction/convertwtot', ['as' => 'transaction.convertwtot', 'uses' => 'SiteController@getConvertWToT']);
Route::post('make-convertwtot', ['as' => 'transaction.postconvertwtot', 'uses' => 'TransferController@postConvertWToT']);
Route::get('misc/5percentstatement', ['as' => 'misc.5percentstatement', 'uses' => 'SiteController@get5PercentStatement']);
Route::get('member/5percentlist', ['as' => 'member.5percentlist', 'uses' => 'MemberController@get5PercentList']);
Route::get('misc/wstatement', ['as' => 'misc.wstatement', 'uses' => 'SiteController@getWStatement']);
Route::get('member/wlist', ['as' => 'member.wlist', 'uses' => 'MemberController@getwList']);
Route::get('misc/wstatementShow/{id}', ['as' => 'misc.wstatementShow', 'uses' => 'MemberController@wstatementShow']);
Route::get('misc/bstatement', ['as' => 'misc.bstatement', 'uses' => 'SiteController@getBStatement']);
Route::get('member/blist', ['as' => 'member.blist', 'uses' => 'MemberController@getbList']);
Route::get('transaction/convertitoc', ['as' => 'transaction.convertitoc', 'uses' => 'SiteController@getConvertIToC']);
Route::post('make-convertitoc', ['as' => 'transaction.postconvertitoc', 'uses' => 'TransferController@postConvertIToC']);
Route::get('transaction/convertitot', ['as' => 'transaction.convertitot', 'uses' => 'SiteController@getConvertIToT']);
Route::post('make-convertitot', ['as' => 'transaction.postconvertitot', 'uses' => 'TransferController@postConvertIToT']);
Route::get('misc/commstatement', ['as' => 'misc.commstatement', 'uses' => 'MemberController@getCommStatement']);
Route::get('misc/commstatement/daily', ['as' => 'misc.getcommstatementDaily', 'uses' => 'SiteController@getCommStatementDaily']);
Route::get('misc/commstatement/getdaily/{type}', ['as' => 'misc.commstatementDaily', 'uses' => 'MemberController@CommStatementDaily']);
Route::get('misc/commstatement/showdetail/{symbol}/{start_date}/{end_date}', ['as' => 'misc.commstatementDaily.showDetailsModal', 'uses' => 'MemberController@showCommstatementModal']);
Route::post('misc/commstatement/setsession', ['as' => 'misc.commstatementDaily.setsession', 'uses' => 'MemberController@set_session']);
Route::get('misc/incomestatement', ['as' => 'misc.incomestatement', 'uses' => 'SiteController@getIncomeStatement']);
Route::get('member/bonuslist', ['as' => 'member.bonuslist', 'uses' => 'MemberController@getbonusList']);
Route::get('transaction/convertttoc', ['as' => 'transaction.convertttoc', 'uses' => 'SiteController@getConvertTToC']);
Route::post('make-convertttoc', ['as' => 'transaction.postconvertttoc', 'uses' => 'TransferController@postConvertTToC']);
Route::get('transaction/transfercash', ['as' => 'transaction.transfercash', 'uses' => 'SiteController@getTransferCash']);
Route::post('make-transfercash', ['as' => 'transaction.postTransferCash', 'uses' => 'TransferController@postTransferCashPoint']);
Route::get('member/checkNetwork/{username}', ['as' => 'member.checkNetwork', 'uses' => 'MemberController@checkNetwork']);
Route::get('misc/transferstatement', ['as' => 'misc.transferstatement', 'uses' => 'SiteController@getTransferStatement']);
Route::get('member/transferlist', ['as' => 'member.transferlist', 'uses' => 'MemberController@gettransferlist']);


// 8. Network
Route::get('network/grouplevel', ['as' => 'network.grouplevel', 'uses' => 'SiteController@getNetworkGrouplevel']);
Route::get('network/3level', ['as' => 'network.3level', 'uses' => 'SiteController@getNetwork3level']);
Route::get('network/unilevel', ['as' => 'network.unilevel', 'uses' => 'SiteController@getNetworkUnilevel']);
Route::post('member/get-unilevel', ['as' => 'member.getUnilevel', 'uses' => 'MemberController@getUnilevelTree']);
Route::post('member/unilevel-search', ['as' => 'member.unilevelSearch', 'uses' => 'MemberController@getUnilevel']);
Route::get('misc/directstatement', ['as' => 'misc.directstatement', 'uses' => 'SiteController@getDirectStatement']);
Route::get('member/directsales', ['as' => 'member.directsales', 'uses' => 'MemberController@getdirectSales']);
Route::get('misc/groupstatement', ['as' => 'misc.groupstatement', 'uses' => 'SiteController@getGroupStatement']);
Route::get('member/groupsales/{id}', ['as' => 'member.groupsales', 'uses' => 'MemberController@getgroupSales']);


// 9. Member Detail
Route::get('settings/account', ['as' => 'settings.account', 'uses' => 'SiteController@getSettingsAccount']);
Route::post('account/update', ['as' => 'account.postUpdate', 'uses' => 'MemberController@postUpdateAccount']);
Route::post('account/updateID', ['as' => 'account.postID', 'uses' => 'MemberController@postUpdateID']);
Route::post('account/updatePassword', ['as' => 'account.updatePassword', 'uses' => 'MemberController@postUpdatePassword']);
Route::post('account/postProfile', ['as' => 'account.postProfile', 'uses' => 'MemberController@postUpdateProfile']);
Route::get('settings/BankInfo', ['as' => 'settings.BankInfo', 'uses' => 'SiteController@getBankInfo']);
Route::get('bankinfo/getlist', ['as' => 'bankinfo.getlist', 'uses' => 'BankInfoController@getBankList']);
Route::get('bankinfo/detail/{id}', ['as' => 'bankinfo.detail', 'uses' => 'BankInfoController@getBankDetail']);
Route::post('bankinfo/getState', ['as' => 'bankinfo.getState', 'uses' => 'BankInfoController@getState']);
Route::post('bankinfo/getCity', ['as' => 'bankinfo.getCity', 'uses' => 'BankInfoController@getCity']);
Route::post('bankinfo/addBank', ['as' => 'bankinfo.addBank', 'uses' => 'BankInfoController@addBank']);
Route::get('settings/BankInfoAsia', ['as' => 'settings.BankInfoAsia', 'uses' => 'BankInfoController@getBankInfoAsia']);
Route::get('bankinfo/getlistAsia', ['as' => 'bankinfo.getlistAsia', 'uses' => 'BankInfoController@getBankListAsia']);
Route::post('bankinfo/addBankAsia', ['as' => 'bankinfo.addBankAsia', 'uses' => 'BankInfoController@addBankAsia']);
Route::get('settings/addBank', ['as' => 'settings.addBank', 'uses' => 'SiteController@getAddBank']);
Route::get('settings/addBankAsia', ['as' => 'settings.addBankAsia', 'uses' => 'BankInfoController@getAddBankAsia']);

Route::post('bankinfo/addBankAsiaSystem', ['as' => 'bankinfo.addBankAsiaSystem', 'uses' => 'BankInfoController@addBankAsiaSystem']);


// 10. User Manual
Route::get('inbox/list',['as'=>'inbox.list','uses'=>'NotificationsController@getList']);
Route::get('inbox/getCSlist',['as'=>'inbox.getCSlist','uses'=>'NotificationsController@getCSlist']);
Route::get('inbox/getSYSTEMlist',['as'=>'inbox.getSYSTEMlist','uses'=>'NotificationsController@getSYSTEMlist']);
Route::get('inbox/{id}',['as'=>'inbox.read','uses'=>'NotificationsController@read']);
Route::get('announcement/list', ['as' => 'announcement.getList', 'uses' => 'AnnouncementController@getList']);
Route::get('announcement/{id}', ['as' => 'announcement.read', 'uses' => 'AnnouncementController@read']);


// 11. Helpdesk
Route::get('helpdesk/addDesk',['middleware'=>'checkAddHelpdesk','as'=>'helpdesk.addDesk','uses'=>'SiteController@addDeskPage']);
Route::get('helpdesk/all',['as'=>'helpdesk.list','uses'=>'HelpdeskController@getAll']);
Route::get('helpdesk/list',['as'=>'helpdesk.getList','uses'=>'HelpdeskController@getList']);
Route::get('helpdesk/faq',['as'=>'helpdesk.faq','uses'=>'HelpdeskController@faq']);
Route::post('helpdesk/postComplain', ['as' => 'helpdesk.postComplain', 'uses' => 'HelpdeskController@postComplain']);
Route::post('helpdesk/postDesk',['as'=>'helpdesk.postDesk','uses'=>'HelpdeskController@addDesk']);
Route::post('helpdesk/postcreate',['as'=>'helpdesk.postcreate','uses'=>'HelpdeskController@postcreate']);
Route::post('helpdesk/postRate', ['as' => 'helpdesk.postRate', 'uses' => 'HelpdeskController@postRate']);
Route::post('helpdesk/postSettle', ['as' => 'helpdesk.postSettle', 'uses' => 'HelpdeskController@postSettle']);
Route::get('helpdesk/{id}',['as'=>'helpdesk.read','uses'=>'HelpdeskController@read']);


// 12. User Manual
Route::get('usermanual',['as'=>'usermanual','uses'=>'MemberController@usermanual']);


// 13. Sea Network Deposit and Withdrawal
Route::get('depositsea', ['as' => 'sealead.deposit', 'uses' => 'SealeadController@getDeposit']);
Route::get('withdrawsea', ['as' => 'sealead.withdraw', 'uses' => 'SealeadController@getWithdraw']);
Route::get('addbanksea', ['as' => 'sealead.addbank', 'uses' => 'SealeadController@getAddbank']);
Route::get('recordsea', ['as' => 'sealead.record', 'uses' => 'SealeadController@getRecord']);
Route::get('sealead/findRecord', ['as' => 'sealead.findRecord', 'uses' => 'SealeadController@findRecord']);
Route::get('setCurrency', ['as' => 'sealead.setCurrency', 'uses' =>  'SealeadController@setCurrency']);

Route::post('sealead/postCreateDeposit', ['as' => 'sealead.postCreateDeposit', 'uses' => 'SealeadController@postCreateDeposit']);
Route::post('sealead/postCreateWithdraw', ['as' => 'sealead.postCreateWithdraw', 'uses' => 'SealeadController@postCreateWithdraw']);
Route::post('sealead/getBankDataAsia', ['as' => 'sealead.getBankDataAsia', 'uses' => 'WithdrawController@getBankDataAsia']);
Route::post('sealead/updateCurrency', ['as' => 'sealead.updateCurrency', 'uses' => 'SealeadController@updateCurrency']);


Route::get('member/{folder}/get/{filename}', ['as' => 'amazon.read', 'uses' =>  'MemberController@amazonRead']);
Route::post('member/hidememo', ['as' => 'member.hidememo', 'uses' => 'MemberController@hidememo']);



// old route
Route::get('misc/rstatement', ['as' => 'misc.rstatement', 'uses' => 'SiteController@getRStatement']);
Route::get('misc/rtradestatement', ['as' => 'misc.rtradestatement', 'uses' => 'SiteController@getRTradeStatement']);
Route::get('misc/mt5statement', ['as' => 'misc.mt5statement', 'uses' => 'SiteController@getMT5Statement']);
Route::get('terms', ['as' => 'terms', 'uses' => 'SiteController@getTerms']);
Route::get('member/show-modal', ['as' => 'member.showModal', 'uses' => 'MemberController@getMemberRegisterModal']);
Route::get('misc/show-modal/{id}', ['as' => 'misc.showDepositModal', 'uses' => 'MemberController@getDepositModal']);
Route::get('misc/show-modal2', ['as' => 'misc.showSubscribeModal', 'uses' => 'MemberController@getSubscribeModal']);
Route::get('misc/show-modal3', ['as' => 'misc.showUnsubscribeModal', 'uses' => 'MemberController@getUnsubscribeModal']);
Route::post('misc/updateLP', ['as' => 'account.postLP', 'uses' => 'MemberController@postUpdateLP']);

// ============================ transaction ===========================================//
Route::get('transaction/withdraw-success', ['as' => 'transaction.withdrawSuccess', 'uses' => 'SiteController@getTransactionWithdrawSuccess']);
Route::get('transaction/transferr', ['as' => 'transaction.transferr', 'uses' => 'SiteController@getTransferR']);
Route::get('transaction/buyr', ['as' => 'transaction.buyr', 'uses' => 'SiteController@getBuyR']);
Route::get('transaction/sellr', ['as' => 'transaction.sellr', 'uses' => 'SiteController@getSellR']);
Route::get('transaction/transfermt5', ['as' => 'transaction.transfermt5', 'uses' => 'SiteController@getTransferMT5']);
Route::get('transaction/processmt5', ['as' => 'transaction.processmt5', 'uses' => 'SiteController@getProcessMT5']);
Route::get('transaction/calculator', ['as' => 'transaction.calculator', 'uses' => 'SiteController@getCalculator']);
Route::get('transaction/transfercash-success', ['as' => 'transaction.transfercashSuccess', 'uses' => 'SiteController@getTransactionTransferCashSuccess']);
Route::get('transaction/transferr-success', ['as' => 'transaction.transferrSuccess', 'uses' => 'SiteController@getTransactionTransferRSuccess']);
Route::get('transaction/buyr-success', ['as' => 'transaction.buyrSuccess', 'uses' => 'SiteController@getTransactionBuyRSuccess']);
Route::get('transaction/sellr-success', ['as' => 'transaction.sellrSuccess', 'uses' => 'SiteController@getTransactionSellRSuccess']);
Route::get('transaction/changepass-success', ['as' => 'transaction.changepassSuccess', 'uses' => 'SiteController@getChangePassSuccess']);
Route::get('transaction/transfermt5-success', ['as' => 'transaction.transfermt5Success', 'uses' => 'SiteController@getTransactionTransferMT5Success']);
Route::get('transaction/transfermt5self-success', ['as' => 'transaction.transfermt5selfSuccess', 'uses' => 'SiteController@getTransactionTransferMT5SelfSuccess']);
Route::get('transaction/transferfund-success', ['as' => 'transaction.transferfundSuccess', 'uses' => 'SiteController@getTransactionTransferFundSuccess']);
Route::get('transaction/subscribefund-success', ['as' => 'transaction.subscribefundSuccess', 'uses' => 'SiteController@getTransactionSubscribeFundSuccess']);
Route::get('transaction/switchfund-success', ['as' => 'transaction.switchfundSuccess', 'uses' => 'SiteController@getTransactionSwitchFundSuccess']);
Route::get('transaction/unsubscribefund-success', ['as' => 'transaction.unsubscribefundSuccess', 'uses' => 'SiteController@getTransactionUnsubscribeFundSuccess']);
Route::get('transaction/processmt5-success', ['as' => 'transaction.processmt5Success', 'uses' => 'SiteController@getTransactionProcessMT5Success']);

Route::get('transaction/converta', ['as' => 'transaction.converta', 'uses' => 'SiteController@getConvertA']);
Route::get('transaction/convertfunda', ['as' => 'transaction.convertfunda', 'uses' => 'SiteController@getConvertFundA']);
Route::get('transaction/convertfundw', ['middleware'=>'checkLpoa','as' => 'transaction.convertfundw', 'uses' => 'SiteController@getConvertFundW']);
Route::get('transaction/convertincome-success', ['as' => 'transaction.convertincomeSuccess', 'uses' => 'SiteController@getTransactionConvertIncomeSuccess']);
Route::get('transaction/convertaself-success', ['as' => 'transaction.convertaselfSuccess', 'uses' => 'SiteController@getTransactionConvertASelfSuccess']);
Route::get('transaction/converta-success', ['as' => 'transaction.convertaSuccess', 'uses' => 'SiteController@getTransactionConvertASuccess']);
Route::get('transaction/convertfunda-success', ['as' => 'transaction.convertfundaSuccess', 'uses' => 'SiteController@getTransactionConvertFundASuccess']);
Route::get('transaction/convertfundw-success', ['as' => 'transaction.convertfundwSuccess', 'uses' => 'SiteController@getTransactionConvertFundWSuccess']);
                                   
Route::get('transaction/withdraw', ['as' => 'transaction.withdraw', 'uses' => 'SiteController@getWithdraw']);
Route::get('transaction/withdraw-success', ['as' => 'transaction.withdrawSuccess', 'uses' => 'SiteController@getTransactionWithdrawSuccess']);
Route::get('transaction/statement', ['as' => 'transaction.statement', 'uses' => 'SiteController@getTransactionStatement']);

Route::post('make-transfer', ['as' => 'transaction.postTransfer', 'uses' => 'TransferController@postTransferPoint']);
Route::post('make-transferr', ['as' => 'transaction.postTransferR', 'uses' => 'TransferController@postTransferRPoint']);
Route::post('make-transfer', ['as' => 'transaction.postTransferMT5', 'uses' => 'TransferController@postTransferMT5Point']);
Route::post('make-process', ['as' => 'transaction.postProcessMT5', 'uses' => 'TransferController@postProcessMT5Point']);
Route::post('make-processfund', ['as' => 'transaction.postProcessMT5Fund', 'uses' => 'TransferController@postProcessMT5PointFund']);
Route::post('calculator', ['as' => 'transaction.postCalculator', 'uses' => 'TransferController@postCalculator']);
Route::post('make-buyr', ['as' => 'transaction.postBuyR', 'uses' => 'TransferController@postBuyRPoint']);
Route::post('make-sellr', ['as' => 'transaction.postSellR', 'uses' => 'TransferController@postSellRPoint']);
             
Route::post('make-convertincome', ['as' => 'transaction.postConvertIncome', 'uses' => 'TransferController@postConvertIncomePoint']);
Route::post('make-converta', ['as' => 'transaction.postConvertA', 'uses' => 'TransferController@postConvertAPoint']);
Route::post('make-convertw', ['as' => 'transaction.postConvertW', 'uses' => 'TransferController@postConvertWPoint']);
Route::post('make-withdraw', ['as' => 'transaction.postWithdraw', 'uses' => 'WithdrawController@postMakeWithdraw']);
    
// ============================ transaction ===========================================//

// ============================Deposit ===========================================//
// Route::get('depositRecords', ['as' => 'depositRecords', 'uses' => 'SiteController@depositRecords']);
// Route::get('deposit', ['as' => 'deposit', 'uses' => 'MemberController@getDeposit']);
// Route::get('member/depositRecords', ['as' => 'member.depositRecords', 'uses' => 'MemberController@getDepositRecords']);
// ============================Deposit ===========================================//


Route::get('settings/bank', ['as' => 'settings.bank', 'uses' => 'SiteController@getSettingsBank']);
Route::get('settings/id', ['as' => 'settings.id', 'uses' => 'SiteController@getSettingsID']);
Route::get('settings/declaration', ['as' => 'settings.declaration', 'uses' => 'SiteController@getSettingsDeclaration']);

Route::get('member/register', ['as' => 'member.register', 'uses' => 'SiteController@getMemberRegister']);
Route::get('member/list', ['as' => 'member.list', 'uses' => 'SiteController@getMemberList']);
Route::get('member/register-success', ['as' => 'member.registerSuccess', 'uses' => 'SiteController@getMemberRegisterSuccess']);
Route::get('member/updateID-success', ['as' => 'member.updateIDSuccess', 'uses' => 'SiteController@getMemberUpdateIDSuccess']);
Route::get('member/updatedeclare-success', ['as' => 'member.updatedeclareSuccess', 'uses' => 'SiteController@getMemberUpdateDeclareSuccess']);
Route::get('member/register-history', ['as' => 'member.registerHistory', 'uses' => 'SiteController@getMemberRegisterHistory']);
Route::get('member/upgrade', ['as' => 'member.upgrade', 'uses' => 'SiteController@getMemberUpgrade']);

Route::get('network/binary', ['as' => 'network.binary', 'uses' => 'SiteController@getNetworkBinary']);
Route::get('network/upline', ['as' => 'network.upline', 'uses' => 'SiteController@getNetworkUpline']);

Route::get('shares/market', ['as' => 'shares.market', 'uses' => 'SiteController@getSharesMarket']);
Route::get('shares/lock-list', ['as' => 'shares.lock', 'uses' => 'SiteController@getSharesLock']);
Route::get('shares/statement', ['as' => 'shares.statement', 'uses' => 'SiteController@getSharesStatement']);

Route::get('member/get-binary', ['as' => 'member.getBinary', 'uses' => 'MemberController@getBinary']);
Route::get('member/binary-back', ['as' => 'member.getBinaryTop', 'uses' => 'MemberController@getBinaryTop']);
Route::get('member/binary-modal', ['as' => 'member.binary.modal', 'uses' => 'MemberController@getBinaryModal']);

Route::get('member/showMember', ['as' => 'member.showMember', 'uses' => 'MemberController@getMemberModal']);
Route::get('member/getMemberRegisterModal', ['as' => 'member.getMemberRegisterModal', 'uses' => 'MemberController@getMemberRegisterModal']);
Route::get('announcement/all', ['as' => 'announcement.list', 'uses' => 'AnnouncementController@getAll']);

Route::get('pending-group', ['as' => 'bonus.group.pending', 'uses' => 'SiteController@getGroupPending']);

Route::get('member/registerlist', ['as' => 'member.registerlist', 'uses' => 'MemberController@getregisterList']);
Route::get('member/accountlist', ['as' => 'member.accountlist', 'uses' => 'MemberController@getaccountList']);
Route::get('member/tradelist', ['as' => 'member.tradelist', 'uses' => 'MemberController@gettradeList']);
Route::get('withdraw/list', ['as' => 'withdraw.list', 'uses' => 'WithdrawController@getList']);

Route::get('bonus/direct-list', ['as' => 'bonus.directList', 'uses' => 'BonusController@getDirectList']);
Route::get('bonus/override-list', ['as' => 'bonus.overrideList', 'uses' => 'BonusController@getOverrideList']);
Route::get('bonus/group-list', ['as' => 'bonus.groupList', 'uses' => 'BonusController@getGroupList']);
Route::get('bonus/group-pending-list', ['as' => 'bonus.group.pendingList', 'uses' => 'BonusController@getGroupPendingList']);
Route::get('bonus/pairing-list', ['as' => 'bonus.pairingList', 'uses' => 'BonusController@getPairingList']);

Route::get('shares/sell-list', ['as' => 'shares.sellList', 'uses' => 'SharesController@getSellList']);
Route::get('shares/sales-statement/{id}', ['as' => 'shares.sell.statement', 'uses' => 'SharesController@getSalesStatement']);

Route::get('shares/return-list', ['as' => 'shares.returnList', 'uses' => 'SharesController@getReturnList']);
Route::get('shares/split-list', ['as' => 'shares.splitList', 'uses' => 'SharesController@getSplitList']);

Route::post('login', ['as' => 'login.post', 'uses' => 'MemberController@postLogin']);
Route::post('forgotpassword', ['as' => 'forgotpassword.post', 'uses' => 'MemberController@postgetPassword']);
Route::post('register', ['as' => 'register.post', 'uses' => 'MemberController@postRegisterPublic']);
Route::post('registerpreview', ['as' => 'registerPreview.post', 'uses' => 'MemberController@postRegisterPreview']);
Route::post('member/deletePic', ['as' => 'member.deletePic', 'uses' => 'MemberController@deletePic']);
Route::post('member/get-groupsales', ['as' => 'member.getGroupsales', 'uses' => 'MemberController@getGroupSalesStatement']);
Route::post('member/get-upline', ['as' => 'member.getUpline', 'uses' => 'MemberController@getUpline']);

Route::post('account/updateBank', ['as' => 'account.postUpdateBank', 'uses' => 'MemberController@postUpdateBank']);    

Route::post('account/updateLP', ['as' => 'account.postLP', 'uses' => 'MemberController@postUpdateLP']);
Route::post('member/register', ['as' => 'member.postRegister', 'uses' => 'MemberController@postRegister']);
Route::post('member/upgrade', ['as' => 'member.postUpgrade', 'uses' => 'MemberController@postUpgrade']);
Route::post('member/addMT5', ['as' => 'member.addMT5', 'uses' => 'MemberController@addMT5']);
Route::get('network/treeview',array('as'=>'network.treeview','uses'=>'MemberController@treeView'));

Route::get('redirect/course',['as'=>'redirect.aes','uses'=>'SiteController@aes_course']);
Route::post('/getinfo', 'AjaxController@getaes');

});


    /**
 * Non-Language specific routes
 */


Route::get('member/register-history', ['as' => 'member.registerHistoryList', 'uses' => 'MemberController@getRegisterHistory']);
Route::post('shares/buy', ['as' => 'shares.postBuy', 'uses' => 'SharesController@buy']);
Route::post('shares/sell', ['as' => 'shares.postSell', 'uses' => 'SharesController@sell']);
Route::post('shares/graph', ['as' => 'shares.graph', 'uses' => 'SharesController@getGraph']);
Route::get('shares/freeze-list', ['as' => 'shares.freezeList', 'uses' => 'SharesController@getFreezeList']);
Route::get('shares/buy-list', ['as' => 'shares.buyList', 'uses' => 'SharesController@getBuyList']);

/**
 * Below is all admin routes
 * @var [type]
 */
$adminRoute = config('app.adminUrl');

// basic routes
Route::get($adminRoute, ['as' => 'admin.home', 'uses' => 'Admin\SiteController@getIndex']);
Route::get($adminRoute . '/login', ['as' => 'admin.login', 'uses' => 'Admin\SiteController@getLogin']);
Route::get($adminRoute . '/logout', ['as' => 'admin.logout', 'uses' => 'Admin\SiteController@getLogout']);
Route::post($adminRoute . '/login', ['as' => 'admin.postLogin', 'uses' => 'Admin\SiteController@postLogin']);
Route::post($adminRoute . '/update-account', ['as' => 'admin.account.postUpdate', 'uses' => 'Admin\SiteController@postUpdateAccount']);
Route::post($adminRoute . '/cron', ['as' => 'admin.cron', 'uses' => 'Admin\SiteController@runCron']);
Route::post($adminRoute . '/toggle-maintenance', ['as' => 'mt.toggle', 'uses' => 'Admin\SiteController@maintenance']);
Route::get($adminRoute . '/runbonus', ['as' => 'admin.runbonus', 'uses' => 'Admin\SiteController@runBonus']);
Route::get($adminRoute . '/bulkupdatepass', ['as' => 'admin.bulkupdatepass', 'uses' => 'Admin\SiteController@bulkupdatePass']);
Route::get($adminRoute . '/tradeprocess', ['as' => 'admin.tradeprocess', 'uses' => 'Admin\SiteController@tradeProcess']);
Route::get($adminRoute . '/tradesync', ['as' => 'admin.tradesync', 'uses' => 'Admin\SiteController@tradeSync']);
Route::get($adminRoute . '/updateinvestorpass', ['as' => 'admin.updateinvestorpass', 'uses' => 'Admin\SiteController@updateinvestorpass']);
Route::post($adminRoute . '/upload-image', ['as' => 'admin.image.upload', 'uses' => 'Admin\SiteController@uploadImage']);

// member routes
Route::get($adminRoute . '/member/showMT5/{id}', ['as' => 'admin.member.showMT5', 'uses' => 'Admin\MemberController@showMT5']);
Route::get($adminRoute . '/member/register' , ['middleware' => 'adminMember','as' => 'admin.member.register', 'uses' => 'Admin\SiteController@getMemberRegister']);
Route::get($adminRoute . '/member/register-common' , ['middleware' => 'adminMember','as' => 'admin.member.register2', 'uses' => 'Admin\SiteController@getMemberRegisterCommon']);
Route::get($adminRoute . 'member/show-modal', ['middleware' => 'adminMember','as' => 'admin.member.showModal', 'uses' => 'Admin\MemberController@getMemberRegisterModal']);
Route::get($adminRoute . '/member/all' , ['middleware' => 'adminMember','as' => 'admin.member.list', 'uses' => 'Admin\SiteController@getMemberList']);
Route::get($adminRoute . '/member/all2' , ['middleware' => 'adminMemberP','as' => 'admin.member.list2', 'uses' => 'Admin\SiteController@getMemberList2']);
Route::get($adminRoute . '/member/edit/{id}' , ['middleware' => 'adminMemberP','as' => 'admin.member.edit', 'uses' => 'Admin\SiteController@getMemberEdit']);
Route::get($adminRoute . '/member/list' , ['as' => 'admin.member.getList', 'uses' => 'Admin\MemberController@getList']);
Route::get($adminRoute . '/member/list2' , ['as' => 'admin.member.getList2', 'uses' => 'Admin\MemberController@getList2']);
Route::get($adminRoute . '/member/wallet' , ['as' => 'admin.member.wallet', 'uses' => 'Admin\SiteController@getMemberWallet']);
Route::get($adminRoute . '/member/wallet-list', ['as' => 'admin.wallet.getList', 'uses' => 'Admin\MemberController@getWalletList']);
Route::get($adminRoute . '/member/wallet-statement/{id}', ['as' => 'admin.wallet.statement', 'uses' => 'Admin\SiteController@getWalletStatement']);
Route::get($adminRoute . '/member/wallet-statement-list/{id}', ['as' => 'admin.wallet.statement.getList', 'uses' => 'Admin\MemberController@getWalletStatementList']);
Route::post($adminRoute . '/member/register', ['as' => 'admin.member.register', 'uses' => 'Admin\MemberController@postRegister']);
Route::post($adminRoute . '/member/update/{id}', ['as' => 'admin.member.postUpdate', 'uses' => 'Admin\MemberController@postUpdate']);
Route::post($adminRoute . '/member/register/{type}', ['as' => 'admin.member.postRegister', 'uses' => 'Admin\MemberController@register']);

// package routes
Route::get($adminRoute . '/package-settings' , ['as' => 'admin.settings.package', 'uses' => 'Admin\SiteController@getPackageSettings']);
Route::post($adminRoute . '/package/update/{id}', ['as' => 'admin.package.update', 'uses' => 'Admin\PackageController@postUpdate']);
    
// declaration routes
Route::get($adminRoute . '/declare-settings' , ['middleware' => 'adminLpoa','as' => 'admin.settings.declare', 'uses' => 'Admin\SiteController@getDeclareSettings']);
Route::get($adminRoute . '/declare/list' , ['as' => 'admin.declare.getLpoa', 'uses' => 'Admin\MemberController@getLpoa']);
Route::get($adminRoute . '/declare/show/{id}', ['middleware' => 'adminLpoa','as' => 'admin.declare.show_detail', 'uses' => 'Admin\MemberController@getShowModal']);//weilun
Route::post($adminRoute . '/declare/update', ['as' => 'admin.declare.update', 'uses' => 'Admin\MemberController@postUpdateDeclare']);
    
// trade routes
Route::get($adminRoute . '/trade-settings' , ['as' => 'admin.settings.trade', 'uses' => 'Admin\SiteController@getTradeSettings']);
Route::post($adminRoute . '/trade/update/{id}', ['as' => 'admin.trade.update', 'uses' => 'Admin\MemberController@postUpdateTrade']);

// shares routes
Route::get($adminRoute . '/shares-settings' , ['as' => 'admin.settings.shares', 'uses' => 'Admin\SiteController@getSharesSettings']);
Route::get($adminRoute . '/shares/sell' , ['as' => 'admin.shares.sellAdmin', 'uses' => 'Admin\SiteController@getSharesSellAdmin']);

Route::get($adminRoute . '/shares-lock' , ['as' => 'admin.shares.lock', 'uses' => 'Admin\SiteController@getSharesLock']);
Route::get($adminRoute . '/shares-lock/list' , ['as' => 'admin.shares.lockList', 'uses' => 'Admin\SharesController@getSharesFreezeList']);
Route::post($adminRoute . '/shares/update-freeze/{id}', ['as' => 'admin.sharesFreeze.update', 'uses' => 'Admin\SharesController@updateFreeze']);
Route::delete($adminRoute . '/shares/remove-freeze/{id}', ['as' => 'admin.sharesFreeze.remove', 'uses' => 'Admin\SharesController@postFreezeDelete']);

Route::get($adminRoute . '/shares-buy' , ['as' => 'admin.shares.buy', 'uses' => 'Admin\SiteController@getSharesBuy']);
Route::get($adminRoute . '/shares-buy/list' , ['as' => 'admin.shares.buyList', 'uses' => 'Admin\SharesController@getSharesBuyList']);

Route::get($adminRoute . '/shares-sell' , ['as' => 'admin.shares.sell', 'uses' => 'Admin\SiteController@getSharesSell']);
Route::get($adminRoute . '/shares-sell/list' , ['as' => 'admin.shares.sellList', 'uses' => 'Admin\SharesController@getSharesSellList']);

Route::post($adminRoute . '/shares/update/{id}', ['as' => 'admin.shares.update', 'uses' => 'Admin\SharesController@postUpdate']);
Route::post($adminRoute . '/shares/sell', ['as' => 'admin.shares.postSell', 'uses' => 'Admin\SharesController@sell']);
Route::get($adminRoute . '/split', ['as' => 'admin.shares.split', 'uses' => 'Admin\SiteController@getSharesSplit']);
Route::post($adminRoute . '/shares/remove-queue', ['as' => 'admin.shares.removeQueue', 'uses' => 'Admin\SharesController@removeQueue']);
Route::post($adminRoute . '/shares/split', ['as' => 'admin.postSplit', 'uses' => 'Admin\SharesController@split']);
Route::post($adminRoute . '/shares/update-buy/{id}', ['as' => 'admin.sharesBuy.update', 'uses' => 'Admin\SharesController@updateBuy']);
Route::post($adminRoute . '/shares/update-sell/{id}', ['as' => 'admin.sharesSell.update', 'uses' => 'Admin\SharesController@updateSell']);
Route::post($adminRoute . '/shares/unlock/{id}', ['as' => 'admin.sharesFreeze.unlock', 'uses' => 'Admin\SharesController@unlock']);

Route::delete($adminRoute . '/shares/remove-buy/{id}', ['as' => 'admin.sharesBuy.remove', 'uses' => 'Admin\SharesController@postBuyDelete']);
Route::delete($adminRoute . '/shares/remove-sell/{id}', ['as' => 'admin.sharesSell.remove', 'uses' => 'Admin\SharesController@postSellDelete']);

// withdraw routes
Route::get($adminRoute . '/withdraw/add-statement', ['middleware' => 'adminWithdrawal','as' => 'admin.withdraw.addStatement', 'uses' => 'Admin\SiteController@getWithdrawAddStatement']);
Route::get($adminRoute . '/withdraw/all' , ['middleware' => 'adminWithdrawal','as' => 'admin.withdraw.all', 'uses' =>  'Admin\SiteController@getWithdrawList']);
Route::get($adminRoute . '/withdraw/list' , ['as' => 'admin.withdraw.getList', 'uses' => 'Admin\WithdrawController@getList']);
Route::get($adminRoute . '/withdraw/show/{id}', ['middleware' => 'adminWithdrawal','as' => 'admin.withdraw.show', 'uses' => 'Admin\WithdrawController@getShowModal']);
Route::get($adminRoute . '/withdraw/edit/{id}', ['middleware' => 'adminWithdrawal','as' => 'admin.withdraw.edit', 'uses' => 'Admin\WithdrawController@getEdit']);
Route::post($adminRoute . '/withdraw/add-statement', ['as' => 'admin.withdraw.add', 'uses' => 'Admin\WithdrawController@postAdd']);
Route::post($adminRoute . '/withdraw/update/{id}', ['as' => 'admin.withdraw.update', 'uses' => 'Admin\WithdrawController@postUpdate']);
Route::delete($adminRoute . '/withdraw/remove/{id}', ['as' => 'admin.withdraw.remove', 'uses' => 'Admin\WithdrawController@postDelete']);
Route::get($adminRoute . '/withdraw/pending' , ['middleware' => 'adminWithdrawal','as' => 'admin.withdraw.pending', 'uses' =>  'Admin\SiteController@getWithdrawPendingList']);//weilun
Route::get($adminRoute . '/withdraw/Plist' , ['as' => 'admin.withdraw.getPList', 'uses' => 'Admin\WithdrawController@getPList']); //weilun

// bonus routes
Route::get($adminRoute . '/bonus/add-statement', ['as' => 'admin.bonus.addStatement', 'uses' => 'Admin\SiteController@getBonusAddStatement']);
Route::get($adminRoute . '/bonus/all' , ['as' => 'admin.bonus.all', 'uses' =>  'Admin\SiteController@getBonusList']);
Route::get($adminRoute . '/bonus/list/{type}' , ['as' => 'admin.bonus.getList', 'uses' => 'Admin\BonusController@getList']);
Route::get($adminRoute . '/bonus/edit/{id}', ['as' => 'admin.bonus.edit', 'uses' => 'Admin\BonusController@getEdit']);
Route::post($adminRoute . '/bonus/add-statement', ['as' => 'admin.bonus.add', 'uses' => 'Admin\BonusController@postAdd']);
Route::post($adminRoute . '/bonus/update/{id}', ['as' => 'admin.bonus.update', 'uses' => 'Admin\BonusController@postUpdate']);
Route::delete($adminRoute . '/bonus/remove/{type}/{id}', ['as' => 'admin.bonus.remove', 'uses' => 'Admin\BonusController@postDelete']);

// transfer routes
Route::get($adminRoute . '/transfer/add-statement', ['as' => 'admin.transfer.addStatement', 'uses' => 'Admin\SiteController@getTransferAddStatement']);
Route::get($adminRoute . '/transfer/all' , ['as' => 'admin.transfer.all', 'uses' =>  'Admin\SiteController@getTransferList']);
Route::get($adminRoute . '/transfer/list' , ['as' => 'admin.transfer.getList', 'uses' => 'Admin\TransferController@getList']);
Route::get($adminRoute . '/transfer/edit/{id}', ['as' => 'admin.transfer.edit', 'uses' => 'Admin\TransferController@getEdit']);
Route::post($adminRoute . '/transfer/add-statement', ['as' => 'admin.transfer.add', 'uses' => 'Admin\TransferController@postAdd']);
Route::post($adminRoute . '/transfer/update/{id}', ['as' => 'admin.transfer.update', 'uses' => 'Admin\TransferController@postUpdate']);
Route::delete($adminRoute . '/transfer/remove/{id}', ['as' => 'admin.transfer.remove', 'uses' => 'Admin\TransferController@postDelete']);

// announcement routes
Route::get($adminRoute . '/announcement/create', ['middleware' => 'adminAnnouncement','as' => 'admin.announcement.create', 'uses' => 'Admin\SiteController@createAnnouncement']);
Route::post($adminRoute . '/announcement/create', ['as' => 'admin.announcement.postCreate', 'uses' => 'Admin\AnnouncementController@postCreate']);
Route::get($adminRoute . '/announcement/all', ['middleware' => 'adminAnnouncement','as' => 'admin.announcement.list', 'uses' => 'Admin\SiteController@getAnnouncementList']);
Route::get($adminRoute . '/announcement/list', ['as' => 'admin.announcement.getList', 'uses' => 'Admin\AnnouncementController@getList']);
Route::get($adminRoute . '/announcement/edit/{id}', ['middleware' => 'adminAnnouncement','as' => 'admin.announcement.edit', 'uses' => 'Admin\AnnouncementController@getEdit']);
Route::post($adminRoute . '/announcement/update/{id}', ['as' => 'admin.announcement.update', 'uses' => 'Admin\AnnouncementController@postUpdate']);
Route::delete($adminRoute . '/announcement/remove/{id}', ['as' => 'admin.announcement.remove', 'uses' => 'Admin\AnnouncementController@remove']);
  
// ========================================helpdesk routes weilun
Route::get($adminRoute . '/helpdesk/list' , ['middleware' => 'adminHelpdesk','as' => 'admin.helpdesk.list', 'uses' => 'Admin\SiteController@getHelpdesklist']);
Route::get($adminRoute . '/helpdesk/schedule' , ['middleware' => 'adminHelpdesk','as' => 'admin.helpdesk.schedule', 'uses' => 'Admin\HelpdeskController@getHelpdeskschedule']);
Route::get($adminRoute . '/helpdesk/getCS' , ['middleware' => 'adminHelpdesk','as' => 'admin.helpdesk.getCS', 'uses' => 'Admin\HelpdeskController@getCS']);

Route::get($adminRoute . '/helpdesk/blacklist' , ['middleware' => 'adminHelpdesk','as' => 'admin.helpdesk.blacklist', 'uses' => 'Admin\SiteController@getHelpdeskblacklist']);

Route::get($adminRoute . '/helpdesk/list' , ['middleware' => 'adminHelpdesk','as' => 'admin.helpdesk.list', 'uses' => 'Admin\SiteController@getHelpdesklist']);
Route::get($adminRoute . '/helpdesk/Plist' , ['middleware' => 'adminHelpdesk','as' => 'admin.helpdesk.Plist', 'uses' => 'Admin\SiteController@getHelpdeskPlist']);
Route::get($adminRoute . '/helpdesk/Record' , ['middleware' => 'adminReply','as' => 'admin.helpdesk.Record', 'uses' => 'Admin\SiteController@getHelpdeskRecord']);
Route::get($adminRoute . '/helpdesk/getRecord' , ['as' => 'admin.helpdesk.getRecord', 'uses' => 'Admin\HelpdeskController@getRecord']);
Route::get($adminRoute . '/helpdesk/getPList' , ['middleware' => 'adminHelpdesk','as' => 'admin.helpdesk.getPList', 'uses' => 'Admin\HelpdeskController@getPList']);
Route::get($adminRoute . '/helpdesk/multiSend' , ['middleware' => 'adminHelpdesk','as' => 'admin.helpdesk.multiSend', 'uses' => 'Admin\HelpdeskController@multiSend']);
Route::get($adminRoute . '/helpdesk/getList/{id}/type/{type_id}' , ['middleware' => 'adminHelpdesk','as' => 'admin.helpdesk.getList', 'uses' => 'Admin\HelpdeskController@getList']);
Route::get($adminRoute . '/helpdesk/getOpenList/{id}/type/{type_id}' , ['middleware' => 'adminHelpdesk','as' => 'admin.helpdesk.getOpenList', 'uses' => 'Admin\HelpdeskController@getOpenList']);
Route::get($adminRoute . '/helpdesk/edit/{id}', ['middleware' => 'adminHelpdesk','as' => 'admin.helpdesk.edit', 'uses' => 'Admin\HelpdeskController@getEdit']);
Route::get($adminRoute . '/helpdesk/show/{id}', ['middleware' => 'adminHelpdesk','as' => 'admin.helpdesk.show', 'uses' => 'Admin\HelpdeskController@getShowModal']);
Route::get($adminRoute . '/helpdesk/personalRating', ['middleware' => 'adminHelpdesk','as' => 'admin.helpdesk.personalRating', 'uses' => 'Admin\SiteController@getPersonalRating']);
Route::get($adminRoute . '/helpdesk/getPersonalRating/{date}' , ['as' => 'admin.helpdesk.getPersonalRating', 'uses' => 'Admin\HelpdeskController@getPersonalRating']);
Route::get($adminRoute . '/helpdesk/getComplain/{date}' , ['as' => 'admin.helpdesk.getComplain', 'uses' => 'Admin\HelpdeskController@getComplain']);
Route::get($adminRoute . '/helpdesk/showTran/{id}', ['as' => 'admin.helpdesk.showTran', 'uses' => 'Admin\HelpdeskController@getTranModal']);

Route::post($adminRoute . '/helpdesk/postBlackList' , ['as' => 'admin.helpdesk.postBlackList', 'uses' => 'Admin\HelpdeskController@postBlackList']);
Route::post($adminRoute . '/helpdesk/postSchedule' , ['as' => 'admin.helpdesk.postSchedule', 'uses' => 'Admin\HelpdeskController@postSchedule']);
Route::post($adminRoute . '/helpdesk/postcreate' , ['as' => 'admin.helpdesk.postcreate', 'uses' => 'Admin\HelpdeskController@postCreate']);
Route::post($adminRoute . '/helpdesk/multipost' , ['as' => 'admin.helpdesk.multipost', 'uses' => 'Admin\HelpdeskController@multipost']);
Route::post($adminRoute . '/helpdesk/transfer' , ['as' => 'admin.helpdesk.transfer', 'uses' => 'Admin\HelpdeskController@transfer']);
Route::post($adminRoute . '/helpdesk/transfer_list' , ['as' => 'admin.helpdesk.transfer_list', 'uses' => 'Admin\HelpdeskController@transfer_list']);
Route::post($adminRoute . '/helpdesk/update/{id}' , ['as' => 'admin.helpdesk.update', 'uses' => 'Admin\HelpdeskController@postUpdate']);

//========================================helpdesk

// ========================================right routes weilun
Route::get($adminRoute . '/right' , ['middleware' => 'adminRight','as' => 'admin.right', 'uses' => 'Admin\SiteController@getRight']);
Route::get($adminRoute . '/right/getUser' , ['middleware' => 'adminRight','as' => 'admin.right.getUser', 'uses' => 'Admin\RightController@getUser']);
Route::get($adminRoute . '/right/edit/{id}', ['middleware' => 'adminRight','as' => 'admin.right.edit', 'uses' => 'Admin\RightController@getEdit']);
Route::post($adminRoute . '/right/update/{id}', ['as' => 'admin.right.postUpdate', 'uses' => 'Admin\RightController@postUpdate']);

//========================================right

//========================================profile routes weilun
Route::get($adminRoute . '/settings', ['middleware' => 'adminProfile','as' => 'admin.settings.account', 'uses' => 'Admin\SiteController@getAccountSettings']);

//========================================profile
// amazon routes
Route::get($adminRoute . '/{folder}/get/{filename}', ['as' => 'admin.amazon.read', 'uses' => 'Admin\MemberController@amazonRead']);

// manual deposit
Route::get($adminRoute . '/manualDeposit', ['middleware' => 'adminDeposit', 'as' => 'admin.manualDeposit', 'uses' => 'Admin\SiteController@getManualDeposit']);
Route::post($adminRoute . '/postDeposit', ['as' => 'admin.postDeposit', 'uses' => 'Admin\MemberController@postDeposit']);



/**
 * Below is for both admin and member routes
 * @var [type]
 */

//ajax
Route::post('/search_email', 'AjaxController@search_email');
Route::post('/set_session', 'AjaxController@set_session');
Route::get('member/{folder}/get/{filename}', ['as' => 'amazon.read.register', 'uses' => 'MemberController@amazonReadRegister']);
