<?php

use Illuminate\Http\Request;
use App\Models\Member;
use App\Repositories\MemberRepository;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('GetBalance', function(Request $request) {
        $data = $request->json()->all();
        DB::table('API_log')->insert(
         [
             'API_type' => 'GetBalance',
             'API_desc' => json_encode($data),
             'created_at' => \Carbon\Carbon::now()
         ]
         );
            
        $members = DB::table('users')->where('username', '=', $data['username'])->first();
     
        $sign = md5(md5($members->username).md5($members->password2));
           
            
        if($sign == $data['sign'])
        {
            $memberd = DB::table('Member')->join('Member_Wallet', 'Member.id', '=', 'Member_Wallet.member_id')->where('username', '=', $members->username)->first();
            
            return array(
                         'status' => 200,
                         'status_code' => "SUCCESS",
                         'balance' => $memberd->cash_point
                         );
        }
        else
        {
            return array(
                         'status' => 500,
                         'status_code' => "FAIL",
                         );
        }
           
    });
    
Route::post('Deposit', function(Request $request) {
        $data = $request->json()->all();
        $request_id = $request->json()->cookie('request_id');
            
            print_r($request_id);
        
       /*
        DB::table('API_log')->insert(
         [
             'API_type' => 'Deposit',
             'API_desc' => json_encode($data),
             'created_at' => \Carbon\Carbon::now()
         ]
         );
        
        $members = DB::table('users')->where('username', '=', $data['username'])->first();
     
        $sign = md5(md5($members->username).md5($members->password2));
        
      
        if($sign == $data['sign'])
        {
            
            $memberd = DB::table('Member')->join('Member_Wallet', 'Member.id', '=', 'Member_Wallet.member_id')->where('username', '=', $members->username)->first();
            
            $current_balance = $memberd->cash_point;
            $new_balance = $data['amount'] + $current_balance;
            
            DB::table('Member_Wallet')
            ->where('member_id', $memberd->member_id)
            ->update(['cash_point' => $new_balance, 'updated_at' => \Carbon\Carbon::now()]);
            
            DB::table('Member_Wallet_Statement')->insert([
            'member_id' => $memberd->id,
            'member_username' => $memberd->username,
            'cash_amount' => $data['amount'],
            'promotion_amount' => 0,
            'register_amount' => 0,
            'action_type' => 'Deposit',
            'wallet_type' => 'C',
            'transaction_type' => 'C',
            'balance' =>  $new_balance,
            'remark' => 'Deposit cash '.$data['amount'],
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

            
            return array(
                         'status' => 200,
                         'status_code' => "SUCCESS",
                         );
        }
            else
            {
            return array(
                         'status' => 500,
                         'status_code' => "FAIL",
                         );
            }
       
         */
    });
    
Route::post('Withdraw', function(Request $request) {
        $data = $request->json()->all();
        
        DB::table('API_log')->insert(
         [
             'API_type' => 'Withdraw',
             'API_desc' => json_encode($data),
             'created_at' => \Carbon\Carbon::now()
         ]
         );
        
        $members = DB::table('users')->where('username', '=', $data['username'])->first();
     
        $sign = md5(md5($members->username).md5($members->password2));
           
            
        if($sign == $data['sign'])
        {
            $memberd = DB::table('Member')->join('Member_Wallet', 'Member.id', '=', 'Member_Wallet.member_id')->where('username', '=', $members->username)->first();
            
            $current_balance = $memberd->cash_point;
            $new_balance = $current_balance - $data['amount'];
            
            DB::table('Member_Wallet')
            ->where('member_id', $memberd->member_id)
            ->update(['cash_point' => $new_balance, 'updated_at' => \Carbon\Carbon::now()]);
            
            DB::table('Member_Wallet_Statement')->insert([
            'member_id' => $memberd->id,
            'member_username' => $memberd->username,
            'cash_amount' => $data['amount'],
            'promotion_amount' => 0,
            'register_amount' => 0,
            'action_type' => 'Withdraw',
            'wallet_type' => 'C',
            'transaction_type' => 'D',
            'balance' =>  $new_balance,
            'remark' => 'Withdraw cash '.$data['amount'],
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

            
            return array(
                         'status' => 200,
                         'status_code' => "SUCCESS",
                         );
        }
            else
            {
            return array(
                         'status' => 500,
                         'status_code' => "FAIL",
                         );
            }
       
           
    });
