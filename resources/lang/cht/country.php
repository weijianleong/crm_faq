<?php

return [
    'Cambodia' => '柬埔寨',
    'China' => '中國',
    'Indonesia' => '印尼',
    'Malaysia' => '馬來西亞',
    'Philippines' => '菲律賓',
    'Singapore' => '新加坡',
    'Thailand' => '泰國',
    'Taiwan' => '台灣',
    'Vietnam' => '越南',
    'South-Korea' => '韓國',
    'Japan' => '日本',
    'Australia' => '澳大利亞',
    'New-Zealand' => '新西蘭',
    'Indonesia' => '印度尼西亞',
    'Myanmar' => '缅甸',
    'India' => '印度',
    'Brunei' => '文莱',
    'Bangladesh' => '孟加拉國',
    'United-arab-emirates'=>'阿拉伯联合酋长国',
    'selected' => '選定國家'
];
