<?php

return [
	'title' => '我的网络团队',
    'title2' => '团队概要',
    'title3' => '三代概要',
	'subTitle' => '空白为不存在直接推荐人。',
	'member' => '电邮',
	'secret' => '安全密码',
	'notice' => '请先输入电邮。',
    'level1' => '第一代',
    'level2' => '第二代',
    'level3' => '第三代',
    'totalmember' => '人数',
    'totalmember3level' => '三代总人数',
    'totallotsize3level' => '三代总手数',
    'totaltransfer3level' => '三代总流水量',
    'totalmembergroup' => '团队总人数',
    'totallotsize' => 'A 手数',
    'totaltransfer' => 'B 流水量',
    'totaltransfergroup' => '团队总流水量',
    'realtime' => '即时',
    'updatedby' => '更新于 28/04/2018',
    'terms' => '所有数据以月底更新为标准。',
   
    
];
