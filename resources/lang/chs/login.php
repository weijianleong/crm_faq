<?php

return [
'title'	=>	'登入',
'subtitle' => '欢迎!',
'username' => '用户编号',
'password' => '密码',
'remember' => '记得密码',
'captcha' => '图形验证码',
'email' => '电邮',
'retrieve' => '找回密码',
'back' => '返回登入页面',
'forgot' => '忘记密码'
];
