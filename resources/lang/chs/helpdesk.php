<?php

return [
    'Title' => '客服',
    'listDate' => '创建日期',
    'listTitle' => '标题',
    'listAction' => '状态',
    'addDesk' => '提交问题',
    'listStatus'=> '处理状态',
    'csStatus'=> '回复状态',
    'listDetail'=> '详情',

    'yourQs'=> '描述您的问题',
    'QsTitle'=> '问题标签',
    'QsType'=> '问题类型',
    'QsContent'=> '问题内容',
    'QsPic'=> '可提供照片',

    'addSucess'=> '已成功提交问题',
    'SucessTitle'=> '提交问题成功',
    'SucessWait'=> '我们已收到您的问题!',
    'SucessWaitcs'=> '客服即将给您答复，请耐心等候',
    'SucessBack' => '返回等待问题解答页面',


    'rate_button'=>'评价',
    'rate'=>'满意',
    'rateMe'=>'评价(请点击星星*)',
    'plzrateMe'=>'请对我们的本次服务做出评价。',
    'FailRate'=> '评价失败!',
    'SucessRate'=> '成功评价!',
    'SucessRates'=> '感谢您对我们本次服务做出的评价！',
    'remarkstar' => '备注：1星 = 非常不满意，2星 = 较不满意，3星 = 一般，4星 = 较满意，5星 = 非常满意',

    'complain'=>'反映',
    'complainT'=>'反映客服人员的服务',
    'complainC'=>'留言',
    'SucessComplain'=>'反映成功',
    'FailComplain'=>'反映失败,请填写原因',
    'Complained'=>'已收到您的反映。',

    'settleT'=>'问题解决了吗?',
    'settleC'=>'您好尊敬的客户，我们将在3天后自动关闭此票证，如果您的问题已得到解决，可直接点确认关闭并给我们评价，如有任何疑问可继续在此票证做回复，谢谢。',

    'you'=>'您',
    'cs'=>'客服人员',

    'warningt'=> '提醒',
    'warning'=> '尊敬的客户,请耐心等候客服回复您,请勿频繁重复提交,感谢您的理解和支持',
    'close'=>'关闭',

    'ReplyStatus0' => '处理中',
    'ReplyStatus1' => '已回复',
    'CaseClose' => '已解决',
    'CaseProcess' => '未解决',

    'readDetail' => '问题详情',
    'readBack' => '返回',
    'readReply' => '回复',
    'readCr8' => '回复成功',
    'readPic1' => '查看图片1',
    'readPic2' => '查看图片2',

    'fleshmsg' => '客服页有新的未读信息。',
    'fleshmsg2' => '点此阅读',

    'fund' =>  '托管问题',
    'withdrawal' =>  '出金问题',
    'advisory' =>  '咨询',
    'approval' =>  '审核问题',
    'capx' =>  'CAPX问题',
    'HTypeError' => '请选择问题类型',

    'errorAdd' =>  '提交问题失败',
    'errorAddtext' =>  '请确认所有资料都有填写',
    'errorAdd5' =>  '您今日已提交2个票证了，请等待客服回复您',
    'errorBlack' =>  '您已被禁止提交问题。',

    'systemAutoReply'=>'系统回复',
    'systemAutoReplyTittle'=>'尊贵的用户',
    'systemAutoReplyDetail'=>'客服部门全体同仁很荣幸为您服务。基于目前客户问题数量比较多，如有回复速度较慢，请多多包涵。客服团队将努力的为您提供优质服务。还请客户耐心等候~<br>谢谢！',




];
