<?php


return [
    'deposit_title' => '入金申请',
    'deposit_uploadfail' => '请检查是否有上传银行收据',
    'warning'=>'提醒',
    'success'=>'成功',
    'amountminNotice' => '最低金额为30美金',
    'depositSubmitted' => '申请入金成功',
    'withdrawSubmitted' => '申请出金成功',
    'deposit_sea' => '入金',
    'withdrawal_sea' => '出金',
    'application_type' => '申请类型',
    'dealTime' => '申请时间',
    'record' => '交易记录',
    'currency'=>'货币',
    'currency_exchange' => '汇率',
    'currency_wrong' => '货币不在选择内',
    'total_amount' => '总额',
    'systemAutoReplyTittle'=>'尊贵的用户',
    'systemAutoReplyDetail'=>'请选择出入金的货币。一但绑定将不能更改。',
    'currencySuccess'=>'设定货币成功',
    'currencySetting'=>'货币设置',

    'title' => '提现申请',
    'addtitle' => '储存银行帐号',
    'subTitle' => '现金积分提现。',
    'missingNotice' => ' <h4>银行资讯不存在!</h4> <strong>您尚未填写银行资讯。</strong> 您必须先设置您的银行资讯才能进行该交易。', // don't remove <h4></h4> and <strong></strong>
    'missingLink' => '现在设置',
    'account' => '电子钱包资料',
    'bank' => '银行名字',
    'bank.account' => '银行户口资讯',
    'amount' => '金额',
    'amountNotice' => '最低提现金额为30美金',
    'fee' => '手续费 (5%)', //this calculation method is if i want withdraw 1000, then add in 5%, so total become 1050.
    'total' => '总额',
    'notice' => '提现申请在每月的第一天和第十五天，这将需要五个工作日来处理。',
    'security' => '安全密码',
    'payment_method' => '付款方式', //weilun
    'update_payment' => '更新付款资料', //weilun
    'Bank_Information' => '银行资料', //weilun
    'Debit_Card' => '借记卡资料', //weilun
    'Card_Number' => '借记卡号码', //weilun
    'Confirm_Card_Number' => '确认借记卡', //weilun
    'Card_Country' => '借记卡国家', //weilun
    'Card_error' => '请确认借机卡是否输入正确', //weilun
    'update_succ' => '更新付款资料成功', //weilun
    'Select_Payment' => '选择付款方式', //weilun
    'Bank_Payment' => '电汇', //weilun
    'Card_Payment' => 'Visa 借记卡', //weilun

    //front
    'goAddbank' => '前往',
    'withdrawWarning' => '出金提醒',
    'withdrawText' => '请先去新增银行帐号才可申请出金。',
    'email' => '收款人钱包',
    'name' => '收款人姓名',
    'money' => '账户余额',
    'Method' => '取款方式',
    'addMethod' => '储存方式',
    'withdrawalNotice' => '*中国地区只能选择银联取款',
    'Unionpay' => '银联',
    'WithdrawlAmount' => '取款金额',

    'optionSelect' => '请选择',
    'openBank' => '开户行城市',
    'country' => '国家',
    'state' => '省份',
    'city' => '地级市、县',
    'selectState' => '请选择省份',
    'selectCity' => '请选择地级市、县',
    'subBank' => '支行名称',
    'saveAccount' => '您的银行帐号',
    'submit' => '新增银行帐号',
    'reset' => '重新填写',
    'remark' => '备注',
    'spMissing' => '1.*电汇不可输入汉字。<br>
                    2.请注意，基于国际反洗钱法要求，我们只接受同名账户的出金申请，即收款人必须是本账户持有人。 <br>
                    3.在提交银行转账申请之前先确认您所填写的个人信息是否准确，TECH REALFX LTD (TECH REALFX LTD)将不退回由于账户持有人错误操作所引起的所有费用。<br>
                    4.我们强烈建议在提交申请之前直接从您的银行处获得您银行的收款信息列表。 <br>
                    5.国际转账业务需要三到五个工作日，国内转账需要一到三个工作日。 <br>',
    //front

    'editBank' => "修改银行帐号",
    'addBank' => "新增帐号",
    'bankList' => "银行帐号列表",
    'editBanksuc' => '修改银行成功',
    'addBanksuc' => '新增银行成功',
    'goWithdrawal' => '您已可前往出金页面申请出金。',
    'hadAcc' => '该银行帐号已存在',
    'insertLogFail' => '提现申请失败',
    'AccFail' => '该银行帐号不存在',



    'pendingWithdraw' => '我们正在审核您的出金申请,可前往出金申请记录查看状态.',
    'Submitted' => '已提交',
    'Cancelled' => '已取消',
    'pending' => '处理中',
    'Invalid' => '无效订单',
    'refund' => '取消取款',
    'cancelFail' => '取消失败,此订单已取消 或 已成功',
    'confirmCancel' => '尊贵的客户确认取消取款?',
    'edit' => '编辑',
    'min' => '(最低: 30美金)',

    'amountless' => '很抱歉，根据规定最低取款金额不得低于150美元，如您有疑问请联系客户专员，谢谢！',
    'amountless50' => '很抱歉，根规定最低取款金额不得低于150美元，如您有疑问请联系客户专员，谢谢！',
    'waitting' => '已有出金申请正在审核 或 处理中，请耐心等候...',
    'withdraw2time' => '您这个月已达到申请提款上限了。',

];
