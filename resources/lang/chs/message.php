<?php

return [
	'loginSuccess'		=>	'成功登入。',
    'registerSuccess'   =>  '成功注册会员。',
    'upgradeSuccess'	=>	'配套升级成功。',
    'transferSuccess'	=>	'转移成功。',
    'transferFail'   =>  '转移失败.',
    'transferCashSuccess'    =>    '现金转移成功。',
    'transferMT5Success'    =>    '资金转移到交易账户成功。',
    'subscribeMT5Success'    =>    '提交托管成功',
    'unsubscribeMT5Success'    =>    '取消托管成功',
    'switchMT5Success'    =>    '提交更换托管成功',
    'transferRSuccess'    =>    'R钱包转移成功。',
    'tbuysellRSuccess'    =>    'R钱包挂单成功。',
    'convertIncomeSuccess'    =>    '收入钱包转换现金钱包成功。',
    'sharesBuySuccess'	=>	'MD积分买入成功。',
    'sharesSellSuccess'	=>	'MD积分卖出成功。',
    'withdrawSuccess'	=>	'提现申请成功。',
    'getpasswordSuccess'    =>    '找回密码成功。',
    'accountUpdateSuccess'	=>	'会员资料已更新。',
    'uploadSuccess'    =>    '上传成功。',
    'MT5addSuccess'    =>    '加MT5户口成功。',
    'FundaddSuccess'    =>    '加托管户口成功。',
    'mergeSuccess'    =>    '合并托管户口成功。',
    'transferCAPXSuccess'    =>    '资金转移到CAPX交易所成功。',
    'transferCAPXFail'   =>  '资金转移到CAPX交易所失败。',

];
