<?php

return [

    'title' => '常见问题',
    'all' => '常见问题',
    'fund' =>  '托管问题',
    'withdrawal' =>  '出金问题',
    'advisory' =>  '咨询问题',
    'approval' =>  '审核问题',
    'capx' =>  'CAPX问题',
    'table_title' => '问题',
    'table_content' => '回答'

];