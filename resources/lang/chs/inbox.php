<?php

return [
    'Title' => '收件箱',
    'csTitle' => '客服审核通知',
    'systemTitle' => '系统通知',
    'fleshmsg' => '邮箱有新的通知。',
    'fleshmsg2' => '点此阅读',
    'detail' => '详情',
    'attachments' => '附件',
    'subject' => '主题',
    'from' => '来自',
    'clickSee' => '点击查看',
    'back' => '返回',
    'system' => '系统'
];
