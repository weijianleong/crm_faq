<?php

return [
    'title' => '公司公告',
    'listDate' => '创建日期',
    'listTitle' => '标题',
    'listAction' => '状态',
    'new' => '有新的公司公告',
    'latestNews' => '公司最新公告'
];
