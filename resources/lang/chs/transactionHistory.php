<?php

return [
        //ParentRemark
        'TH00031'   => 'A仓盈利（账号: %%account%%; 订单号: %%trade%%）',
        'TH00032'   => 'A仓盈利 - 低于本金（账号: %%account%%; 订单号: %%trade%%）',
        'TH00033'   => 'B仓盈利（账号: %%account%%; 订单号: %%trade%%）',

        //MemberRepository
        'TH00026'   => '使用%%amount%%升级配套至%%package%%',

        //BonusRopository
        'TH00034'   => '团体奖金 (%%amount%%), 月份: %%date%%',
        'TH00035'   => '直接奖金 (%%amount%%), 注册新成员 (会员编号#%%username%%)',
        'TH00036'   => '每月奖金 (%%amount%%), 月份: %%date%%',

        //WithdrawalRepository
        'TH00010'   => '出金%%amount%%',
        'TH00012'   => '出金%%amount%%驳回',
        'TH00013'   => '从会员编号#%%username%%出金%%amount%%',
        'TH00079'   => '从会员编号#%%username%%入金%%amount%%',

        //StoreProd
        'TH00002'   => '入金%%amount%%',
        'TH00009'   => '出金%%amount%%',
        'TH00011'   => '出金%%amount%%取消',
        'TH00073'   => '出金%%amount%% (从TR转移)',

        //TransferRepository
        'TH00007'   => '转移%%amount%%至会员编号#%%username%%',
        'TH00004'   => '从会员编号#%%username%%转移%%amount%%',
        'TH00037'   => '从R钱包转换%%amount%%至会员编号#%%username%%',
        'TH00038'   => '从会员编号#%%username%%转移%%amount%%至R钱包',
        'TH00039'   => '购买R钱包获得%%amount%%',
        'TH00040'   => '使用%%amount%%购买R钱包',
        'TH00008'   => '转移%%amount%%至MT5账号%%account%%',
        'TH00001'   => '使用%%amount%%购买B钱包',
        'TH00016'   => '购买B钱包获得%%amount%%',
        'TH00020'   => 'LPOA费用%%amount%%（33.33%）',
        'TH00019'   => '扣除20%的投资盈利%%amount%%并转移至出金沉淀风险备用金资 (R钱包)',
        'TH00062'   => '1/3正滑点外20点 , 扣除%%amount%%并转移至出金沉淀风险备用金资 (R钱包)',
        'TH00063'   => '1/3正滑点外20点 , 扣除20%的滑点盈利%%amount%%并转移至出金沉淀风险备用金资 (R钱包)',
        'TH00064'   => '2/3(盈利+正滑点) , 扣除20%的投资盈利%%amount%%并转移至出金沉淀风险备用金资 (R钱包)',
        'TH00027'   => '从订单号%%trade%%获得隔夜利息%%amount%%',
        'TH00041'   => '从订单号%%trade%%扣除隔夜利息%%amount%%至MT5账号%%account%% (隔夜利息: %%amountSwap%%, R钱包吸收: %%amountAbsorbed%%)',
        'TH00028'   => '从订单号%%trade%%扣除隔夜利息%%amount%%至MT5账号%%account%%',
        'TH00017'   => '从MT5转移%%amount%%至账号%%account%%',
        'TH00018'   => '使用%%amount%%出售B钱包',
        'TH00029'   => '出售B钱包获得%%amount%%',
        'TH00042'   => '2/3滑点 , 从订单号%%trade%%转移%%amount%%至MT5账号%%account%%',
        'TH00043'   => '1/3滑点外20点 , 从订单号%%trade%%转移%%amount%%至MT5账号%%account%%',
        'TH00044'   => '出售R钱包获得%%amount%%',
        'TH00045'   => '使用%%amount%%出售R钱包',
        'TH00005'   => '从收入钱包转换%%amount%%至现金钱包',
        'TH00006'   => '从W钱包转换%%amount%%至现金钱包',
        'TH00003'   => '从账号A-%%account%%转移%%amount%%至现金钱包',
        'TH00030'   => '从账号A-%%account%%转移%%amount%%至W钱包',
        'TH00066'   => '从收入钱包转换%%amount%%至T钱包',
        'TH00067'   => '从T钱包转换%%amount%%至现金钱包',
        'TH00068'   => '从T钱包转移%%amount%%至会员 (%%username%%)',
        'TH00069'   => '从会员 (%%username%%) 转移%%amount%%至T钱包',
        'TH00070'   => '从W钱包转换%%amount%%至T钱包',
        'TH00071'   => '从W钱包转换%%amount%%至现金钱包',
        'TH00072'   => '从会员 (%%username%%) 转移%%amount%%至现金钱包',

        //Others
        'TH00014'   => '%%month%% %%year%% 奖金',
        'TH00015'   => '%%month%% %%year%% 奖金调整',

        'TH00049'   => '%%month%% %%year%% 奖金调整（提早颁发）',
        'TH00021'   => '重新调整 - 赔偿3个月没有利润至MT5账号%%account%%',
        'TH00022'   => '调整 - 5%赔偿金（至%%month%%）, MT5账号%%account%% %%amountString%%',
        'TH00023'   => '调整 - 托管公司从账号%%account%%的订单号%%order%%调整隔夜利息',
        'TH00024'   => '调整 - 托管公司报销账号 %%account%%',
        'TH00025'   => '调整 - 用户取消账号',
        'TH00046'   => '调整 - 重复取款',
        'TH00047'   => '调整 - 账号%%account%%没有资格获得5％的报销',
        'TH00053'   => '调整 - 特别调整',
        'TH00054'   => '调整 - 隔夜利息调整至账号%%account%%',
        'TH00055'   => '调整 - 滑点调整至账号%%account%%',
        'TH00057'   => '调整 - 8月2018年拆分组计算',
        'TH00058'   => '调整 - 9月2018扣除50%佣金',
        'TH00059'   => '调整至MT5账号%%account%%',
        'TH00060'   => '调整 - 因违反公司政策而出金。 MT5账号: %%account%%',
        'TH00061'   => '调整 - 扣除%%percentage%%佣金 ( %%monthyear%% )',
        'TH00065'   => '调整 - 用户A仓出金',

        'TH00048'   => '入金%%amount%% (从TR转移)',
        'TH00050'   => '入金%%amount%%通过电汇',
        'TH00052'   => '入金%%amount%%（%%name%%调整）',

        'TH00074'   => '从CAPX交易所转移资金至现金钱包',
        'TH00075'   => '买%%coin%%',
        'TH00076'   => '卖%%coin%%',

        'TH00077'   => '%%coin%%佣金 （%%startdate%% - %%enddate%%）',
        'TH00078'   => '从现金钱包转移资金至CAPX交易所',
        'TH00080'   => '%%coin%%分红 （%%startdate%% - %%enddate%%）',

        'TH00051'   => '手动出金',
        'TH00056'   => '电汇出金'
    
];
