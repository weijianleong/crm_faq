<?php

return [
	'title' => '升级配套',
	'subTitle' => '需用<span class="theme-text bold">100%</span>注册积分支付。', // don't remove <span></span>
	'package' => '配套',
    'packageNotice' => '已注册最高级配套',
    'renew' => 'RENEW',
	'renew' => '重购',
    'topup' => '升级需支付',
	'registerPoint' => '将用注册积分支付',
	'registerNotice' => '将用优惠积分支付' // like "90% will be used on promotion point"
];
