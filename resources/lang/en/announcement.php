<?php

return [
    'title' => 'All Announcements',
    'listDate' => 'Created Date',
    'listTitle' => 'Title',
    'listAction' => 'Action',
    'new' => 'You have a new announcement.',
    'latestNews' => 'Latest Announcement'
];
