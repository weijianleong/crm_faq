<?php

return [
	'title' => 'Top-up Package',
	'subTitle' => '<span class="theme-text bold">10-50%</span> of your promotion point, <span class="theme-text bold">50-90%</span> of your register point.', // don't remove <span></span>
	'package' => 'Package',
    'packageNotice' => 'No available package to top-up',
	'renew' => 'RENEW',
    'topup' => 'Upgrade to this,pay ',
	'registerPoint' => 'REGISTER POINT amount',
	'registerNotice' => 'will be used on promotion point.' // like "90% will be used on promotion point"
];
