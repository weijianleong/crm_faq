<?php

return [
    'Title' => 'Help & Support',
    'listDate' => 'DATE',
    'listTitle' => 'Tittle',
    'listAction' => 'Status',
    'addDesk' => 'Submit Question',
    'listStatus'=> 'List Status',
    'csStatus'=> 'Reply',
    'listDetail'=> 'Detail',

    'yourQs'=> 'Description Your Question',
    'QsTitle'=> 'Tittle',
    'QsType'=> 'Type',
    'QsContent'=> 'Content',
    'QsPic'=> 'can Upload the Picture',

    'addSucess'=> 'Submit Success',
    'SucessTitle'=> 'Submit Success',
    'SucessWait'=> 'Request Received',
    'SucessWaitcs'=> 'Thanks for reaching out to us. We would review your query and get back to you soon.',
    'SucessBack' => 'Back to Help&Support Page',

    'rate_button'=>'Rating',
    'rate'=>'Rate',
    'rateMe'=>'Rate(click the stars for rating*)',
    'plzrateMe'=>'Please Rate Our Service',
    'FailRate'=> 'Fail Rate!',
    'SucessRate'=> 'Rating Sucessful!',
    'SucessRates' => 'Thanks For Your Rating',
    'remarkstar' => 'Note：★ = Very dissatisfied，★★ = dissatisfied，★★★ = average，★★★★ = satisfy，★★★★★ = Very Satisfy',

    'complain'=>'Complain',
    'complainT'=>'Reflect customer service',
    'complainC'=>'Comment',
    'SucessComplain'=>'Reflect success',
    'FailComplain'=>'Reflect Fail,please fill in the reason',
    'Complained'=>'Your response has been received.',

    'settleT'=>'Has the problem been solved?',
    'settleC'=>'dear customer, we will automatically close this ticket after 3 days, if your problem has been resolved, you can directly confirm the closure and give us a comment, if you have any questions, you can continue to reply on this ticket, thank you.',

    'you'=>'You',
    'cs'=>'Customer Service',

    'warningt'=> 'Remind',
    'warning'=> 'Hello, respected customer, your question has been successfully submitted. Please wait patiently. Please repeat the submission frequently. Thank you for your understanding and support.',
    'close'=>'close',

    'ReplyStatus0' => 'Processing',
    'ReplyStatus1' => 'Replied',
    'CaseClose' => 'Solved',
    'CaseProcess' => 'Unsolve',

    'readDetail' => 'Question Detail',
    'readBack' => 'Back',
    'readReply' => 'Reply',
    'readCr8' => 'Reply Sucessful',
    'readPic1' => 'View Picture 1 ',
    'readPic2' => 'View Picture 2 ',

    'fleshmsg' => 'Unread message from Help & Support.',
    'fleshmsg2' => 'Click Here To Read',

    'fund' =>  'Fund Manage Issue',
    'withdrawal' =>  'Withdrawal Issue',
    'advisory' =>  'Advisory',
    'approval' =>  'Approval Issue',
    'capx' =>  'CAPX Issue',
    'HTypeError' => 'Please select the question type',

    'errorAdd' =>  'Failed to submit the question',
    'errorAddtext' =>  'Please confirm that all materials are filled out',
    'errorAdd5' =>  'You have submitted 2 tickets today, please wait for the customer service reply to you.',
    'errorBlack' =>  'You have been banned',

    'systemAutoReply'=>'System',
    'systemAutoReplyTittle'=>'Dear customers',
    'systemAutoReplyDetail'=>'All of us at the Customer Support Team are honoured to be of service. However, due to the high number of issues /problems recently, we like to sincerely apologize if the response is slow.The Customer Support Team strives to provide you with quality service.<br>Thank you for your patience.',

];
