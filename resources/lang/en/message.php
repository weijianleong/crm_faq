<?php

return [
	'loginSuccess'		=>	'Login success.',
    'registerSuccess'   =>  'Member register success.',
    'upgradeSuccess'	=>	'Package upgrade success',
    'transferSuccess'	=>	'Transfer successful.',
    'transferFail'   =>  'Transfer Fail.',
    'transferCashSuccess'    =>    'Cash Wallet transfer success.',
    'transferMT5Success'    =>    'Account transfer success',
    'subscribeMT5Success'    =>    'Subscribe manage account success',
    'switchMT5Success'    =>    'Switch manage account success',
    'unsubscribeMT5Success'    =>    'Unsubscribe manage account success',
    'transferRSuccess'    =>    'R Wallet transfer success',
    'tbuysellRSuccess'    =>    'R Wallet post trade success',
    'convertIncomeSuccess'    =>    'Income wallet convert Cash success',
    'sharesBuySuccess'	=>	'Shares purchase successful.',
    'sharesSellSuccess'	=>	'Shares sales successful.',
    'withdrawSuccess'	=>	'Withdraw successful.',
    'getpasswordSuccess'    =>    'Retrieve password successful.',
    'accountUpdateSuccess'	=>	'Account updated',
    'uploadSuccess'    =>    'Upload Success.',
    'MT5addSuccess'    =>    'Add MT5 account success.',
    'FundaddSuccess'    =>    'Add trading account success.',
    'mergeSuccess'    =>    'Merge MAM account success.',
    'transferCAPXSuccess'    =>    'Cash Wallet transfer to CAPX success.',
    'transferCAPXFail'   =>  'Cash Wallet transfer to CAPX fail.',


];
