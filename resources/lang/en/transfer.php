<?php

return [
	'cash.title' => 'Cash Wallet Transfer',
    'r.title' => 'R Wallet Transfer',
    'income.title' => 'Income Wallet Convert Cash Wallet',
    'br.title' => 'Buy R Wallet',
    'sr.title' => 'Sell R Wallet',
    'a.title' => 'Transfer Account A to Cash Wallet',
    'mt5.title' => 'Transfer to Account A',
	'subTitle' => 'Register, promotion, or cash point transfer.',
	'transfer' => 'Register Wallet Transfer',
    'mt5.title2' => 'Closed Trade Verification',
    'amount' => 'Transfer Amount',
	'convertamount' => 'Convert Amount',
    'amountA' => 'Amount of Account A',
    'buy.amount' => 'Buy Amount',
    'sell.amount' => 'Sell Amount',
	'amountNotice' => 'Divisible by 10.',
	'target' => 'Transfer To',
	'targetNotice' => 'Username, CASE SENSITIVE',
    'cash.wallet' => 'Cash Wallet',
    'income.wallet' => 'Income Wallet',
    'r.wallet' => 'R Wallet',
    'a.wallet' => 'Account A',
    'b.wallet' => 'Account B',
    'd.wallet' => 'Deposit',
    'mt5.wallet' => 'MT5 Account',
    'mt5.calculator' => 'Trade Calculator',
    'balance' => 'Balance',
    'remark' => 'Remark',
	'security' => 'Security Password',
    'mergetitle' => 'Please select the merge MAM account',
    'transferUpline' => 'Transfer to Upline',
    'transferDownline' => 'Transfer to Downline',
    'transferAll' => 'Transfer to All',
    'success' => ' Success'
];
