<?php

return [
    'Title' => 'Inbox',
    'csTitle' => 'Verify Notification',
    'systemTitle' => 'System Notification',
    'fleshmsg' => 'You has a new notification.',
    'fleshmsg2' => 'Click Here To Read',
    'detail' => 'Detail',
    'attachments' => 'Attachments',
    'subject' => 'Subject',
    'from' => 'From',
    'clickSee' => 'Click to view',
    'back' => 'back',
    'system' => 'By System'
];
