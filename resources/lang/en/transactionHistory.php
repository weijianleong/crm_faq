<?php

return [
        //ParentRemark
        'TH00031'   => 'Account A profit (Account: %%account%%; Trade: %%trade%%)',
        'TH00032'   => 'Account A profit - Less than deposit capital (Account: %%account%%; Trade: %%trade%%)',
        'TH00033'   => 'Account B profit (Account: %%account%%; Trade: %%trade%%)',

        //MemberRepository
        'TH00026'   => 'Debited %%amount%% for upgrade package to %%package%%',

        //BonusRopository
        'TH00034'   => 'Credited group bonus %%amount%% for month %%date%%',
        'TH00035'   => 'Credited direct bonus %%amount%% for registering new member with ID# %%username%%',
        'TH00036'   => 'Credited monthly bonus %%amount%% for month %%date%%',

        //WithdrawalRepository
        'TH00010'   => 'Debited %%amount%% for withdrawal',
        'TH00012'   => 'Credited %%amount%% for withdrawal reject',
        'TH00013'   => 'Credited %%amount%% for withdrawal from Member ID # %%username%%',
        'TH00079'   => 'Debited %%amount%% for  Deposit to Member ID # %%username%%',

        //StoreProd
        'TH00002'   => 'Deposit cash %%amount%%',
        'TH00009'   => 'Withdraw cash %%amount%%',
        'TH00011'   => 'Withdraw cash %%amount%% - cancelled',
        'TH00073'   => 'Withdraw cash %%amount%% (transfer from TR)',

        //TransferRepository
        'TH00007'   => 'Debited %%amount%% for transfer cash to Member ID # %%username%%',
        'TH00004'   => 'Credit %%amount%% for transfer cash from Member ID # %%username%%',
        'TH00037'   => 'Debited %%amount%% for transfer R Wallet to Member ID # %%username%%',
        'TH00038'   => 'Credit %%amount%% for transfer R Wallet from Member ID # %%username%%',
        'TH00039'   => 'Credited %%amount%% for buy R Wallet',
        'TH00040'   => 'Debited %%amount%% for buy R Wallet',
        'TH00008'   => 'Debited %%amount%% for transfer to MT5 Account %%account%%',
        'TH00001'   => 'Debited %%amount%% for buy B Wallet',
        'TH00016'   => 'Credited %%amount%% for buy B Wallet',
        'TH00020'   => 'Debited %%amount%% for 33.33% LPOA fee',
        'TH00019'   => 'Debited %%amount%% for 20% of investment profit for reserved fund (R Wallet)',
        'TH00062'   => 'Debited 1/3 positive slippage %%amount%% beyond 20 pips for reserved fund (R Wallet)',
        'TH00063'   => 'Debited 1/3 positive slippage %%amount%% beyond 20 pips for 20% of slippage investment profit for reserved fund (R Wallet)',
        'TH00064'   => 'Debited 2/3 positive slippage %%amount%% for 20% of investment profit for reserved fund (R Wallet)',
        'TH00027'   => 'Credited %%amount%% swap from trade %%trade%%',
        'TH00041'   => 'Debited %%amount%% swap (Swap : %%amountSwap%%, R Wallet absorbed : %%amountAbsorbed%%) from trade %%trade%% for MT5 Account %%account%%',
        'TH00028'   => 'Debited %%amount%% swap from trade %%trade%% for MT5 Account %%account%%',
        'TH00017'   => 'Credited %%amount%% from MT5 for Account %%account%%',
        'TH00018'   => 'Debited %%amount%% for selling B Wallet',
        'TH00029'   => 'Credited %%amount%% for selling B Wallet',
        'TH00042'   => 'Debited 2/3 slippage %%amount%% from trade %%trade%% for transfer to MT5 Account %%account%%',
        'TH00043'   => 'Debited 1/3 slippage %%amount%% beyond 20 pips, from trade %%trade%% for transfer to MT5 Account %%account%%',
        'TH00044'   => 'Credited %%amount%% for selling R wallet',
        'TH00045'   => 'Debited %%amount%% for selling R Wallet',
        'TH00005'   => 'Convert %%amount%% from Income Wallet to Cash Wallet',
        'TH00006'   => 'Convert %%amount%% from W Wallet to Cash Wallet',
        'TH00003'   => 'Convert %%amount%% from Account A %%account%% to Cash Wallet',
        'TH00030'   => 'Convert %%amount%% from Account A %%account%% to W Wallet',
        'TH00066'   => 'Convert %%amount%% from Income Wallet to T Wallet',
        'TH00067'   => 'Convert %%amount%% from T Wallet to Cash Wallet',
        'TH00068'   => 'Debited %%amount%% for transfer T Wallet to member (%%username%%)',
        'TH00069'   => 'Credit %%amount%% for transfer T Wallet from member (%%username%%)',
        'TH00070'   => 'Convert %%amount%% from W Wallet to T Wallet',
        'TH00071'   => 'Convert %%amount%% from W Wallet to Cash Wallet',
        'TH00072'   => 'Credit %%amount%% for transfer Cash Wallet from member (%%username%%)',

        //Others
        'TH00014'   => 'Bonus %%month%% %%year%%',
        'TH00015'   => 'Bonus %%month%% %%year%% - Adjustment',

        'TH00049'   => 'Bonus %%month%% %%year%% - Adjustment (given in advance)',
        'TH00021'   => 'Re-adjustment - Compensation for 3 months no profit for MT5 Account %%account%%',
        'TH00022'   => 'Adjustment - 5% compensation (until %%month%%), MT5 Account %%account%% %%amountString%%',
        'TH00023'   => 'Adjustment - Swap adjustment from fund manage company for order %%order%% account %%account%%',
        'TH00024'   => 'Adjustment - Reimbursement from fund manage company for account %%account%%',
        'TH00025'   => 'Adjustment - User Cancel Account',
        'TH00046'   => 'Adjustment - Double Withdrawal',
        'TH00047'   => 'Adjustment - Not entitled for 5% reimbursement for account %%account%%',
        'TH00053'   => 'Adjustment - Special Adjustment',
        'TH00054'   => 'Adjustment - Swap adjustment for account %%account%%',
        'TH00055'   => 'Adjustment - Slippage adjustment for account %%account%%',
        'TH00057'   => 'Adjustment - Split group calculation in August 2018',
        'TH00058'   => 'Adjustment - Deduct 50% commission for September 2018',
        'TH00059'   => 'Adjustment for MT5 Account %%account%%',
        'TH00060'   => 'Adjustment - Withdrawal due to violate company policy. MT5 Account: %%account%%',
        'TH00061'   => 'Adjustment - Deduct %%percentage%% commission ( %%monthyear%% )',
        'TH00065'   => 'Adjustment - User withdraw account A',
        
        'TH00048'   => 'Deposit cash %%amount%% (transfer from TR)',
        'TH00050'   => 'Deposit cash %%amount%% via TT',
        'TH00052'   => 'Deposit cash %%amount%% (Adjustment by %%name%%)',

        'TH00074'   => 'Transfer fund from CAPX to Cash Wallet',
        'TH00075'   => 'Buy %%coin%%',
        'TH00076'   => 'Sell %%coin%%',

        'TH00077'   => 'Commission for %%coin%% (%%startdate%% - %%enddate%%)',
        'TH00078'   => 'Transfer fund from Cash Wallet to CAPX',
        'TH00080'   => 'Bonus for %%coin%% (%%startdate%% - %%enddate%%)',

        'TH00051'   => 'Manual withdrawal',
        'TH00056'   => 'TT Withdrawal'
        
];
