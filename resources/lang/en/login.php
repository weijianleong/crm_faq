<?php

return [
'title'	=>	'Login',
'subtitle' => 'Welcome',
'username' => 'Member ID',
'password' => 'Password',
'remember' => 'Remember Me',
'captcha' => 'Captcha Code',
'email' => 'Email',
'retrieve' => 'Retrieve Password',
'back' => 'Back to Login Page',
'forgot' => 'Forgot Password'
];
