<?php

return [

    'title' => 'FAQ',
    'all' => 'All FAQ',
    'fund' =>  'Fund FAQ',
    'withdrawal' =>  'Withdrawal FAQ',
    'advisory' =>  'Advisory FAQ',
    'approval' =>  'Approval FAQ',
    'capx' =>  'CapX FAQ',
    'table_title' => 'Title',
    'table_content' => 'Content'

];