<?php

return [
	'title' => 'Withdrawal',
    'addtitle' => 'Saving BankAccount',
	'subTitle' => 'Cash point withdraw.',
	'missingNotice' => ' <h4>Bank Detail Missing!</h4> <strong>You have not set your bank detail.</strong> You need to set it in order to do make withdrawal.', // don't remove <h4></h4> and <strong></strong>
	'missingLink' => 'set now',
	'account' => 'Account Information',
	'bank'	=> 'Bank',
	'bank.account' => 'Account Detail',
	'amount' => 'Amount',
	'amountNotice' => 'minimum $50.',
	'fee' => 'Admin Fee (5%)',
	'total' => 'Total',
    'notice' => 'Withdrawal request on every month 1st and 15th and will take 5 working day to process.',
	'security' => 'Security Password',
	'payment_method' => 'Payment Method', //weilun
	'update_payment' => 'Update Payment', //weilun
	'Bank_Information' => 'Bank Information', //weilun
	'Debit_Card' => 'Card Information', //weilun
	'Card_Number' => 'Card Number', //weilun
	'Confirm_Card_Number' => 'Confirm Card Number', //weilun
	'Card_Country' => 'Card Country', //weilun
	'Card_error' => 'Please Confirm The Card Number', //weilun
	'update_succ' => 'Update Success', //weilun
	'Select_Payment' => 'Select Payment', //weilun
	'Bank_Payment' => 'Bank Wire Transfer', //weilun
	'Card_Payment' => 'Prepaid Visa Card', //weilun

    'withdrawSubmitted' => 'Successful application for withdrawal',
    'pendingWithdraw' => 'We are reviewing your withdrawal request .You can go to the withdrawal record to view the status.',
    'goAddbank' => 'Go AddBank',
    'withdrawWarning' => 'Withdrawal Reminder',
    'withdrawText' => 'Please go to add bank account before you apply for a withdrawal.',
    'email' => 'Payee Wallet',
    'name' => 'Payee Name',
    'money'=> 'Balance',
    'Method' => 'Withdrawal Method',
    'addMethod' => 'saveAccountAc Method',
    'withdrawalNotice'=> '*China can only choose UnionPay withdrawals',
    'Unionpay'=> 'Unionpay',
    'WithdrawlAmount' => 'Withdrawal Amount',
    'amountminNotice' => 'minimum $20.',
    'optionSelect' => 'Option Selection',
    'openBank' => 'Open Account City',
    'country' => 'Country',
    'state' => 'State',
    'city' => 'City',
	'selectState' => 'Select State',
    'selectCity' => 'Select City',
    'subBank' => 'Sub Bank Name',
    'saveAccount' => 'Your Saving Account',
    'submit' => 'Add Bank Account',
    'reset' => 'Reset',
    
    'editBank' => "Modify bank",
    'addBank' => "Add Bank account",
    'bankList' => "Bank account list",
    'editBanksuc' => 'Modify bank success',
    'addBanksuc' => 'Save bank account success',
    'goWithdrawal' => 'You can go to the withdrawal page to apply for a withdrawal.',
    'hadAcc' => 'The bank account already exists',
    'remark' => 'Remark',
    'spMissing' => '1.*电汇不可输入汉字。<br>
                2.请注意，基于国际反洗钱法要求，我们只接受同名账户的出金申请，即收款人必须是本账户持有人。 <br>
                3.在提交银行转账申请之前先确认您所填写的个人信息是否准确，TECH REALFX LTD (TECH REALFX LTD)将不退回由于账户持有人错误操作所引起的所有费用。<br>
                4.我们强烈建议在提交申请之前直接从您的银行处获得您银行的收款信息列表。 <br>
                5.国际转账业务需要三到五个工作日，国内转账需要一到三个工作日。 <br>',

    'insertLogFail' => 'Withdrawal of application failed',
    'AccFail' => 'The bank account does not exist',
    'dealTime' => 'Resgiter Withdrawal Time',

    'Submitted' => 'Submitted',
    'Cancelled' => 'Cancelled',
    'success' => 'success',
    'pending' => 'In Process',
    'Invalid'=>'Invalid Order',
    'refund'=>'Refund Withdrawal',
    'cancelFail' => 'Cancellation failed, this order is Cancelled or has been successful',
    'confirmCancel' => 'Confirm cancel withdrawal?',
    'edit' => 'Edit',
    'min' => '(Minimum: $150)',
    'amountless' => 'Sorry, withdrawal amount cannot less than $150 as stipulated in the rules, thanks for your cooperation',
    'amountless50' => 'Sorry, withdrawal amount cannot less than $150 as stipulated in the rules, thanks for your cooperation',
    'waitting' => 'There is pending withdrawal application, please waiting...',
    'withdraw2time' => 'You have reached the application withdrawn limit this month.',

];
