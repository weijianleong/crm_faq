<?php

return [
	'toggleNav' => 'Toggle Navigation',
    'language' => 'Language',
	'pointTitle' => 'My Points',
    'registerTitle' => 'R Wallet',
    'promotionTitle' => 'Income Wallet',
    'transferTitle' => 'T Wallet',
    'convertableAmount' => 'Convertable Amount',
    'wTitle' => 'W Wallet',
    'sTitle' => 'S Wallet',
    'bTitle' => 'B Wallet',
    'cashTitle' => 'Cash Wallet',
	'pointSub' => '(Your Wallet Statement)',
	'cash' => 'Cash',
	'register' => 'R',
	'promotion' => 'Income',
    'addMT5' => 'Add MT5 Account',
    'downloadMT5' => 'Download MT5',
    'addFund' => 'Add MAM Account',
    'mergeFund' => 'Merge MAM Account',
    'addFundExtra' => 'Request Extra MAM Account',
    'addFundExist' => 'Extra MAM Account already exist.',
	'sharesTitle' => 'My Shares',
	'sharesSub' => '(Your Shares Statement)',
	'purchase' => 'Purchase Point',
	'active' => 'Active Shares',
	'lock' => 'Locked Shares',
	'membersTitle' => 'My Members',
	'membersSub' => '(My Member Statement)',
	'direct' => 'Direct Members',
	'leftTotal' => 'Carry Forward (LEFT)',
	'rightTotal' => 'Carry Forward (RIGHT)',
	'currentShareTitle' => 'Current Share Price',
	'lastDate' => 'Last Update',
	'submit' => 'Submit',
	'cancel' => 'Cancel',
	'close' => 'Close',
	'modal.load' => 'loading..',
	'modal.error' => 'Something went wrong',
	'show' => 'Show',
	'confirm' => 'Confirm',
	'edit' => 'Edit',
	'shares' => 'Shares',
	'mdpoint' => 'MD Point',
	'mdpointsub' => 'You can get MD Point from selling shares.',
	'read' => 'Read',
	'pendingGroup' => 'Next Payout',
    'select' => 'Select Account',
    'select2' => 'Please Select',
    'selectcomp' => 'Select Company',
    'margin' => 'Margin Table',
    'accountAamount' => 'Account A Amount',
    'accountBamount' => 'Account B Amount',
	'from' => 'From',
	'to' => 'to',
    'cal.amount' => 'Amount',
    'cal.lot' => 'Lot Size',
    'cal.type' => 'Symbol',
    'cal.price' => 'Entry Price',
    'cal.amount' => 'Trade Amount USD$',
    'cal.swap' => 'Swap',
    'cal.buy' => 'Buy',
    'cal.sell' => 'Sell',
    'cal.sl' => 'Stop Loss(SL)',
    'cal.tp' => 'Take Profit(TP)',
	'status.process' => 'Processing',
	'status.done' => 'Done',
	'status.reject' => 'Reject',
    'withdrawalMessage' => 'Please login to TR for withdrawal.',
    'transID' => 'Invoice No.',
    'dealTime' => 'Deposit Time',
    'fundMoney' => 'Amount',
    'fundStatus' => 'Status',
    'invoice' => 'Invoice',
    'download' => 'Download',
    'success' => 'Payment Success',
    'pending' => 'Pending',
    'update' => 'Update',
    'notes' => 'Notes',
    'mywallet' => 'My Wallets',
    'weatherCountry' => 'Beijing',
    'today' => 'Today',
    'hidememo' => 'Do not show it again',
    'rules' => 'Rules',
    'ownnetwork' => 'Participation Requirement',
    'network' => 'With-Network',
    'nonetwork' => 'Non-Network',
    'noqualified' => 'Not Qualified',
    'newadd' => 'New',
    'progress' => 'Progress',
    'earn' => 'Receiving Quota',
    'copyright1' => 'Copyright',
    'copyright2' => 'TR Tech Solution. All rights reserved.'
];
