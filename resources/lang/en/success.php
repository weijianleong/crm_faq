<?php

return [
    'title' => 'Registration is complete',
    'text1' => 'Congratulations!',
    'text2' => 'Next : Download and sign LPOA',
    'back' => 'Back to registration',
    'rtitle' => 'R Wallet Transfer Success',
    'atitle' => 'Account A to Cash Wallet',
    'aback' => 'Back to Account A to Cash Wallet',
    'br.title' => 'Buy R Wallet',
    'sr.title' => 'Sell R Wallet',
    'rback' => 'Back to R wallet transfer',
    'tr.back' => 'Back to My Trading',
    'trtext2' => 'R Wallet post trade success',
    'txtitle' => 'Cash Wallet Transfer Success',
    'txtext2' => 'You have successfully transfer ',
    'txback' => 'Back to cash wallet transfer',
    'wwtitle' => 'W Wallet Transfer to Cash Wallet Success',
    'wwtext2' => 'You have successfully transfer ',
    'wwback' => 'Back to Transfer from W Wallet to Cash Wallet',
    'sftitle' => 'Subscribe Account Manage Company',
    'sftext2' => 'You have successfully subscribed ',
    'sfwtitle' => 'Switch Account Manage Company',
    'sfwtext2' => 'You have successfully submitted.',
    'sfwtext3' => 'Your request will be effective within 3 days before Non Farm of each month. ',
    'sfback' => 'Back to MAM Account',
    'uftitle' => 'Unsubscribe Account Manage Company',
    'uftext2' => 'You have successfully unsubscribed ',
    'ufback' => 'Back to MAM Account',
    'cptitle' => 'Password for all MAM account has been changed : ',
    'cptext1' => 'You have changed password for all MAM account ',
    'cptext2' => 'You can login to MT5 now to check trade history',
    'cpback' => 'Back to MAM Account',
    'cvtitle' => 'Income Wallet Convert Cash Wallet Success',
    'cvtext2' => 'You have successfully convert ',
    'cvback' => 'Back to income convert',
    'lptext' => 'Upload Successful',
    'lptext2x' => 'Proceed to MAM Account to add new account',
    'lpbackx' => 'Back to MAM Account',
    'lptext2' => 'Your account will be activated within 24 hours',
    'lpback' => 'Back to LPOA',
    'mttitle' => 'Account Transfer Success',
    'mtback' => 'Back to account transfer',
    'idtitle' => 'ID',
    'idtext2' => 'Upload Successful ',
    'idback' => 'Back to ID',
    'detitle' => 'Terms & Conditions',
    'detext' => 'Upload Successful',
    'detext2' => 'Your account will be activated within 24 hours',
    'deback' => 'Back to Terms & Conditions',
    'ptitle' => 'Retrieve Password Success',
    'ptext2' => 'Please check your email',
    'pback' => 'Back to Login',
    'wttitle' => 'Withdrawal Success',
    'wttext2' => 'You have successfully withdraw ',
    'wtback' => 'Back to withdraw',
    'tradetitle' => 'Your account is under second stage verifcation process',
    'tradetitle2' => 'Closed trade verification Success',
    'tradeback' => 'Back to My MT5 Account',
    'atobtext1' => 'Transfer Successful',
    'atobtext2' => 'Next 1 : Go to MT5 Platform to place trade',
    'atobtext3' => 'Next 2 : After entry market, go to Trade Calculator to check SL/TP',
    'processtext1' => 'Submission Successful',
    'processtext2' => 'Tips : Account A profit, go to Account A to Cash Wallet',
    'processtext3' => 'Tips : Account B profit, go to My Trading Account check status',
    'rtext1' => 'Processing Sell R Wallet Successful',
    'rtext2' => 'Note : R Wallet should be converted to cash wallet within 72 hours',
    'rtext3' => 'Tips : To track your sell R Wallet status, go to Transaction Status',
    'rtext4' => 'Back To Transaction Status',
];
