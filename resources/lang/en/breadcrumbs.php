<?php

return [
	'front'	=>	'Front Page',
	'dashboard'	=>	'Dashboard',
	'settingsPersonal' => 'Personal Information',
	'settingsBank' => 'Bank Information',
    'settingsID' => 'ID',
    'settingsDeclare' => 'Terms & Conditions',
	'hierarchy' => 'Sponsor Tree',
    'mlist' => 'Member List',
	'register' => 'Register New Member',
	'registerHistory' => 'Registration History',
	'registerSuccess' => 'Registration Successful',
    'upgradeSuccess' => 'Upgrade Successful',
	'upgrade' => 'Top-up Package',
	'binary' => 'Binary Hierarchy',
	'unilevel' => 'My Network',
	'sharesMarket' => 'Market',
	'sharesLock' => 'Shares Lock Statement',
	'sharesStatement' => 'Shares Statement',
	'withdrawStatement' => 'Withdraw Statement',
	'transfer.cash' => 'Cash Wallet Transfer',
    'transfer.mt5'    =>    'Transfer to Account A & B',
    'transfer.r' => 'R Wallet Transfer',
    'buy.r' => 'Buy R Wallet',
    'sell.r' => 'Sell R Wallet',
    'convert.income' => 'Income Wallet Convert Cash Wallet',
    'convert.a' => 'Account A Convert Cash Wallet',
    'withdraw' => 'Bonus Wallet Withdrawal',
    'transferSuccess' => 'Transfer Successful',
    'uploadSuccess' => 'Upload Successful',
    'convertSuccess' => 'Transfer Successful',
    'withdrawSuccess' => 'Withdraw Successful',
    'postSuccess' => 'Submitted closed trade for verification successful',
    'buysellSuccess' => 'Post trade Successful',
	'bonusStatement' => 'Bonus Statement',
	'summary' => 'Summary',
	'terms' => 'Terms & Condition',
	'announcementList' => 'All Announcements',
    'statementLink1'    =>    'Cash Wallet History',
    'statementLink2'    =>    'Income Wallet History',
    'statementLink3'    =>    'R Wallet History',
    'statementLink4' => 'My MT5 Account',
    'statementLink5' => 'Account A Convert Cash Wallet',
    'statementLink6' => 'My Trade',
    'statementLink7' => 'Transfer to Account A & B',
    'statementLink8' => 'Trade Calculator',
    'statementLink9' => 'My MT5 Trade',
    'commissionTitle'    =>    'Commission Report ',
    'groupPending' => 'Pending Group Bonus',
    'depositWithdrawal' => 'Deposit/Withdrawal',
    'deposit' => 'Deposit',
    'depositRecords' => 'Deposit Records',
    'withdraw' => 'Withdrawal',
    'withdrawal' => 'Withdrawal',
    'withdrawalRecords' => 'Withdrawal Records',
    'refreshBalance' => 'Refresh Balance',
    'Withdrawaloutside' => 'Withdrawal (Outside China)',
    'WithdrawaloutsideRecords' => 'Withdrawal Records (Outside China)'
];
