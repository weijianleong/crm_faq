<!-- <button class="btn btn-success btn-flat-border btn-warning" title="Edit" data-url="{{ route('admin.declare.update', ['id' => $model->id]) }}" type="submit" onclick="sure({{$model->id}})">
  <i class="md md-mode-edit"></i> Update
</button> -->

<button class="btn btn-info btn-flat-border btn-show" title="Detail" data-url="{{ route('admin.declare.show_detail', ['id' => $model->id]) }}" data-toggle="modal" data-target="#showModal">
    <i class="md md-mode-edit"></i> Edit
</button>