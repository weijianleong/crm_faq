<?php
    use Carbon\Carbon;
    use Illuminate\Support\Str;

    
    $memberlist = DB::table('Member')
    ->join('users', 'Member.username', '=', 'users.username')
    ->join('Member_Detail', 'Member.id', '=', 'Member_Detail.member_id')
    ->select('Member.id as id', 'Member.username as username',  'first_name', 'lpoa', 'lpoa_status','identification_number')
    ->take(500)
    ->where('process_status','Y')
    ->whereNotNull('lpoa')

    ->orderBy('Member.updated_at', 'DESC')
    ->where('lpoa_status', '<>','Y')->get();
    
    //print_r($memberlist);
    
    $destinationPath = config('misc.lpPath');
?>

@extends('back.app')

@section('title')
Package Settings | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">Front Page</a></li>
  <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
  <li class="active">Declaration Form Status</li>
</ul>
@stop

@section('content')
<main>
  @include('back.include.sidebar')
  <div class="main-container">
    @include('back.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
<!--       <section class="tables-data">
            <div class="page-header">
              <h1><i class="md md-wallet-giftcard"></i> LPOA Status</h1>
            </div>

            <div class="card">
              <div>
                <table id="packageListTable" class="table table-full table-bordered table-full-small" cellspacing="0" width="100%" role="grid">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>IC</th>
                      <th>Username</th>
                      <th>LPOA</th>
                      <th>Status</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    @if (count($memberlist) > 0)
                    @foreach ($memberlist as $memberx)
                    <tr>
                      <td>
                        <div class="input-group">
                        <span class="input-group-addon">{{ $memberx->first_name }}</span>
                        </div>
                      </td>
                     <td>
                        <div class="input-group">
                          <span class="input-group-addon">{{ $memberx->identification_number }}</span>
                        </div>
                      </td>
                      <td>
                        <div class="input-group">
                          <span class="input-group-addon">{{ $memberx->username }}</span>
                        </div>
                      </td>

                      <td>
                        <span class="input-group-addon"><a href="{{ route('admin.amazon.read', ['folder' => 'LP', 'filename' => $memberx->lpoa]) }}" target="blank">@lang('settings.title8')</a></span>
                        
                      </td>

                    <td>
                    <div class="switch">
                    <label class="filled">
                     <input type="checkbox" name="declare_status"> <span class="lever"></span>  </label>
                    </div>
                    </td>


                      <td>
                        <button class="btn btn-warning btn-flat-border btn-update" data-url="{{ route('admin.declare.update', ['id' => $memberx->id]) }}" type="submit">
                          <i class="md md-mode-edit"></i> Update
                        </button>
                      </td>
                    </tr>
                    @endforeach
                    @endif
                  </tbody>
                </table>
              </div>
            </div>
      </section> -->

      <section class="tables-data">
              <div class="page-header">
                <h1><i class="md md-group-add"></i> LPOA List</h1>
              </div>

              <div class="card">
                <div>
                  <div class="datatables">
                    <table class="table table-full table-full-small dt-responsive display nowrap table-grid" cellspacing="0" width="100%" role="grid" data-url="{{ route('admin.declare.getLpoa') }}">
                      <thead>
                        <tr>
                          
                          <th data-id="first_name">first_name</th>
                          <th data-id="Member_Detail.identification_number" >identification_number</th>
                          <th data-id="Member.username" data-orderable="false" >username</th>

                          <th data-id="action" data-orderable="false" data-searchable="false">action</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
     </section>
    </div>
  </div>
</main>

    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="showModalLabel">Helpdesk History</h4>
            </div>

            <form role="form" class="action-form" data-url="{{ route('admin.declare.update')}}" http-type="post" data-nationality="true">
                <div class="modal-body">

                  <div class="loading text-center">
                    <img src="{{ asset('assets/img/loading.gif') }}" alt="Network Loading">
                    <br>
                    <small class="text-primary">loading..</small>
                  </div>

                  <div class="error text-center">
                    <i class="md md-error"></i>
                    <br>
                    <small class="text-danger">Something went wrong</small>
                  </div>

                  <div id="modalContent">
                  </div>

                </div>

                <div class="modal-footer">
                  <button type="submit" class="btn btn-warning btn-raised" >Update</button>
                  <button type="button" class="btn btn-default btn-raised" data-dismiss="modal">Close</button>
                </div>
            </form>

          </div>
        </div>
    </div>
@stop
