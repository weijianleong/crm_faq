


<div class="row">
      <table style="margin-left: 25px">
        <thead>
          <tr >
            <th >IC</th>
            <td>: {{$model->identification_number}}</td>
          </tr>

          <tr><th> &nbsp;</th><td> &nbsp;</td></tr>

          <tr>
            <th>Name</th>
            <td>: {{$model->first_name}}</td>
          </tr>

          <tr><th> &nbsp;</th><td> &nbsp;</td></tr>

         <tr>
            <th>New Page</th>
            <td>: <a href="{{ route('admin.amazon.read', ['folder' => 'LP', 'filename' => $model->lpoa]) }}" target="blank">@lang('settings.title8')</a></td>
          </tr>

          <tr><th> &nbsp;</th><td> &nbsp;</td></tr>

          <tr>
            <th style="width: 100px">Approval</th>
            <td><div class="switch">:<label class="filled"><input type="checkbox"  name="declare_status" @if ($model->lpoa_status == 'Y') checked="checked" @endif> <span class="lever"></span></label></div></td>
          </tr>

          <tr><th> &nbsp;</th><td> &nbsp;</td></tr>
          
        </thead>

      </table>


      <div class="form-group" >
        <!-- <span class="input-group-addon"><a href="{{ route('admin.amazon.read', ['folder' => 'LP', 'filename' => $model->lpoa]) }}" target="blank">@lang('settings.title8')</a></span> -->
        <embed src="{{$model->lpoaFile}}"  width="100%" height="500px">
              

      </div>
            

      <div class="form-group">
        <input type="hidden" name="member_id" id="member_id"  value="{{$model->id}}" >
        
      </div>
</div>


