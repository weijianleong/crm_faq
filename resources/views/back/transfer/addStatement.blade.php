@extends('back.app')

@section('title')
  Add Transfer Statement | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">Front Page</a></li>
  <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
  <li class="active">Member Point Management</li>
</ul>
@stop

@section('content')
  <main>
    @include('back.include.sidebar')
    <div class="main-container">
      @include('back.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section>
          <div class="page-header">
            <h1><i class="md md-swap-vert"></i> Member Point Management</h1>
          </div>

          <div class="row m-b-40">
            <div class="col-md-6">
              <div class="panel panel-default">
                <div class="panel-body">
                  <form role="form" onsubmit="return false;" data-parsley-validate class="action-form shares-form" id="transferStatementForm" http-type="post" data-url="{{ route('admin.transfer.add') }}">
                    <fieldset>
                      <div class="form-group">

                      <div class="form-group">
                        <label class="control-label">Member ID</label>
                        <input type="text" name="to" required="" class="form-control">
                      </div>

                      <div class="form-group">
                        <label class="control-label">Point Type</label>
                        <select class="form-control" name="wallet_type" required="">
                          <option value="B">Bonus Wallet</option>
                          <option value="R">Register Wallet</option>
                        </select>
                      </div>

                      <div class="form-group">
                        <label class="control-label">Transaction</label>
                        <select class="form-control" name="transaction_type" required="">
                          <option value="C">Credit</option>
                          <option value="D">Debit</option>
                        </select>
                      </div>

                      <div class="form-group">
                        <label class="control-label">Amount</label>
                        <input type="number" step="any" class="form-control" name="amount" required="">
                        <span class="help-block">use dots(.) for decimals</span>
                      </div>

                      <div class="form-group">
                        <label class="control-label">Description</label>
                        <input type="text" name="remark" required="" class="form-control">
                      </div>

                      <div class="form-group">
                        <button type="submit" class="btn btn-primary">
                          <span class="btn-preloader">
                            <i class="md md-cached md-spin"></i>
                          </span>
                          <span>Submit</span>
                        </button>
                        <button type="reset" class="btn btn-default">Cancel</button>
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>
<div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="showModalLabel">
            <span class="md md-accessibility"></span> @lang('register.modal.title')
          </h4>
        </div>
        <div class="modal-body">
          <div class="loading text-center">
            <img src="{{ asset('assets/img/loading.gif') }}" alt="Network Loading">
            <br>
            <small class="text-primary">@lang('common.modal.load')</small>
          </div>

          <div class="error text-center">
            <i class="md md-error"></i>
            <br>
            <small class="text-danger">@lang('common.modal.error')</small>
          </div>

          <div id="modalContent"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-raised" data-dismiss="modal">@lang('common.close')</button>
        </div>
      </div>
    </div>
  </div>
@stop
