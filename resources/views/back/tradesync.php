<?php
    use App\Models\Member;
    use App\Repositories\MemberRepository;
    use App\Repositories\TransferRepository;
    use Carbon\Carbon;
   


                    $memberRepo = new MemberRepository;
                    $transferRepo = new TransferRepository;

                 // Sync missing open trade
/*
     $orders = DB::table('Member_Account_Fund')->where('mam_account', '=', 601275)->get();
    
    foreach($orders as $order)
    {
        $bookA = $order->bookA;

        $history = $memberRepo->HistoryAPI($bookA);
        echo 'Sync with MTM';
        echo "<BR>";
        echo $bookA;
        echo "<BR>";
    }
 */
 
 

    $orders = DB::connection('mysql2')->table('opentrade')->where('Login', 'like', '30%')->where('Login', '<>', 30012404)->where('Login', '<>', 30012472)->where('Login', '<>', 3000)->get();
   
    

                foreach($orders as $order)
                {
                    $orderid = $order->Order;
                    $bookA = $order->Login;
                    
                    $orderAP = DB::table('Orders_Fund')->where('bookAorderid', '=', $orderid)->first();
                    
                    if(count($orderAP) == 0)
                    {
                        $account = DB::table('Member_Account_Fund')->where('bookA', '=', $bookA)->first();
                        
                        $member_id = $account->member_id;
                       
                        DB::table('Orders_Fund')->insert(
                                             [
                                             'member_id' => $member_id,
                                             'bookA' => $bookA,
                                             'bookAorderid' => $orderid,
                                             'status' => 'O',
                                             'created_at' => \Carbon\Carbon::now()
                                             ]
                                             );
                        
                        echo 'From Open Trade';
                        echo "<BR>";
                        echo $member_id;
                        echo "<BR>";
                        echo $bookA;
                        echo "<BR>";
                        
                    }
                    
                }
 
 /*
                // Sync missing open trade that already closed
    
                $orders = DB::connection('mysql2')->table('historyposition')->where('Login', 'like', '30%')->where('Login', '<>', 30012404)->where('Login', '<>', 30012472)->where('Login', '<>', 30000001)->where('Login', '<>', 3000)->where('CloseTime', '>=', '2019-02-27')->get();
    
    //$orders = DB::connection('mysql2')->table('historyposition')->where('Login', '=', 30033489)->where('CloseTime', '>=', '2018-09-19 13:33:24')->get();
                
                foreach($orders as $order)
                {
                    $orderid = $order->OrderId;
                    $bookA = $order->Login;
                    
                    $orderAP = DB::table('Orders_Fund')->where('bookAorderid', '=', $orderid)->first();
                    
                    if(count($orderAP) == 0)
                    {
                        $account = DB::table('Member_Account_Fund')->where('bookA', '=', $bookA)->first();
                        
                        $member_id = $account->member_id;
                        

   
                        DB::table('Orders_Fund')->insert(
                                             [
                                             'member_id' => $member_id,
                                             'bookA' => $bookA,
                                             'bookAorderid' => $orderid,
                                             'status' => 'C',
                                             'created_at' => \Carbon\Carbon::now()
                                             ]
                                             );
                        
                        echo 'From Close Trade';
                        echo "<BR>";
                        echo $member_id;
                        echo "<BR>";
                        echo $bookA;
                        echo "<BR>";
                        
                    }
                    
                }

*/
 
              // Sync missing close trade
 

 
              $orders = DB::table('Orders_Fund')->where('status', '=', 'O')->whereIn('bookA', function($q){$q->select('bookA')->from('Member_Account_Fund')->where('status', '=', -1);})->get();
               // $orders = DB::table('Orders_Fund')->where('status', '=', 'O')->get();
                $no = 0;
                foreach($orders as $order)
                {
                    $member_id = $order->member_id;
                    $orderid = $order->bookAorderid;
                    $bookA = $order->bookA;
                    
                    
                    //$member = $memberRepo->findById($member_id);
                    
                    $orderAP = DB::connection('mysql2')->table('historyposition')->where('Orderid', '=', $orderid)->first();
                    
                    if(count($orderAP) > 0)
                    {
                        
                        DB::table('Orders_Fund')->where('bookAorderid', $orderid)
                        ->update([
                             'status' => 'C',
                             'updated_at' => \Carbon\Carbon::now()
                             ]);

   
                        echo 'Sync Close Trade';
                        echo "<BR>";
                        print_r($bookA);
                        echo "<BR>";
                    }
                    else
                    {
                        $orderAP2 = DB::connection('mysql2')->table('opentrade')->where('Order', '=', $orderid)->first();
                        
                        if(count($orderAP2) == 0)
                        {
                            $no++;
                            if($no <= 100)
                            {
                               $history = $memberRepo->HistoryAPI($bookA);
                                echo 'Sync with MTM';
                                echo "<BR>";
                                echo $no;
                                echo "<BR>";
                                echo $member_id;
                                echo "<BR>";
                                echo $bookA;
                                echo "<BR>";
                            }
                        }
                    }
                    
                }
 
 
                // Sync data from historyposition
    
                $orders = DB::table('Orders_Fund')->where('status', '=', 'C')->whereNull('CloseTime')->get();
    
                foreach($orders as $order)
                {
                    $member_id = $order->member_id;
                    $orderid = $order->bookAorderid;
                    $bookA = $order->bookA;
                    
                    
                    $member = $memberRepo->findById($member_id);
                    
                    $orderAP = DB::connection('mysql2')->table('historyposition')->where('OrderId', '=', $orderid)->orderBy('OrderId', 'desc')->first();
                    
                    if($orderAP->Cmd == 'SELL')
                        $tradecommand = 'BUY';
                    else
                        $tradecommand = 'SELL';
                    
                    DB::table('Orders_Fund')->where('bookAorderid', $orderid)
                    ->update([
                             'opentime' => $orderAP->OpenTime,
                             'openprice' => $orderAP->OpenPrice,
                             'tradecommand' => $tradecommand,
                             'symbol' => $orderAP->Symbol,
                             'lotsize' => $orderAP->Volume/10000,
                             'fundcompany' => $orderAP->PartnerId,
                             'mam_account' => $orderAP->MamAccount,
                             'profitA' => $orderAP->Profit,
                             'swapA' => $orderAP->Storage,
                             'closetime' => $orderAP->CloseTime,
                             'closeprice' => $orderAP->ClosePrice,
                             'stoploss' => $orderAP->StopLoss,
                             'takeprofit' => $orderAP->TakeProfit,
                             ]);
                }
 
    $orders = DB::table('Orders_Fund')->where('status', '=', 'C')->whereNull('mam_account')->get();
 
   // $orders = DB::table('Orders_Fund')->where('status', '=', 'P')->whereRaw('opentime = closetime')->get();
    
    foreach($orders as $order)
    {
        $member_id = $order->member_id;
        $orderid = $order->bookAorderid;
        $bookA = $order->bookA;
        
        
        $member = $memberRepo->findById($member_id);
        
        $orderAP = DB::connection('mysql2')->table('historyposition')->where('OrderId', '=', $orderid)->orderBy('OrderId', 'desc')->first();
        
        if($orderAP->Cmd == 'SELL')
            $tradecommand = 'BUY';
        else
            $tradecommand = 'SELL';
        
        DB::table('Orders_Fund')->where('bookAorderid', $orderid)
        ->update([
                 'opentime' => $orderAP->OpenTime,
                 'openprice' => $orderAP->OpenPrice,
                 'tradecommand' => $tradecommand,
                 'symbol' => $orderAP->Symbol,
                 'lotsize' => $orderAP->Volume/10000,
                 'fundcompany' => $orderAP->PartnerId,
                 'mam_account' => $orderAP->MamAccount,
                 'profitA' => $orderAP->Profit,
                 'swapA' => $orderAP->Storage,
                 'closetime' => $orderAP->CloseTime,
                 'closeprice' => $orderAP->ClosePrice,
                 'stoploss' => $orderAP->StopLoss,
                 'takeprofit' => $orderAP->TakeProfit,
                 ]);
     
   
        print_r($bookA);
        echo ',';
    }
 
