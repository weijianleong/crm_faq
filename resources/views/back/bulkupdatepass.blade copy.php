<?php
    use App\Models\Member;
    use App\Repositories\MemberRepository;
    use Carbon\Carbon;
   
    /*
    $users = DB::table('temp_member')->get();
    
    //print_r($users);
    
    foreach($users as $user2)
    {
        $email = $user2->{'Emails'};
        $username = $user2->{'Emails'};
        $first_name = $user2->{'Name'};
        $password = $user2->{'Password'};
        $password2 = $user2->{'Password'};
        
        
        $user = \Sentinel::registerAndActivate([
            'email'   => $email,
            'username'  =>  $username,
            'first_name' => $first_name,
            'password'  =>  $password,
            'password2'  =>  $password2,
            'is_ban'  =>  0,
            'permissions' =>  [
                'member' => true,
            ]
        ]);
       
   
    }
   */
    
        
        /*
        $sponsor_id = $member->register_by;
        $ic_number = $member->identification_number;
        
        if($member->gender == 'male')
            $gender = 'M';
        else
            $gender = 'F';
        
        $email = $member->email;
        $phone = $member->phone1;
        $country = $member->nationality;
        $address = $member->address;
        $ac1 = $member->bookA;
        $ac2 = $member->bookB;
        $credentials_1 = 'http://47.52.19.75/ID/'.$member->id_front;
        $credentials_2 = 'http://47.52.19.75/ID/'.$member->id_back;
        
        $internalPath = public_path('/ID/');
        
        $filename = $internalPath.'Front_'.$member->id_front;
        if (file_exists($filename.'.JPG'))
        {
            $credentials_1 = 'http://47.52.19.75/ID/'.'Front_'.$member->id_front.'.JPG';
            $credentials_2 = 'http://47.52.19.75/ID/'.'Back_'.$member->id_back.'.JPG';
            
        }
        
        if (file_exists($filename.'.jpg'))
        {
            $credentials_1 = 'http://47.52.19.75/ID/'.'Front_'.$member->id_front.'.jpg';
            $credentials_2 = 'http://47.52.19.75/ID/'.'Back_'.$member->id_back.'.jpg';
            
        }
        
        if (file_exists($filename.'.jpeg'))
        {
            $credentials_1 = 'http://47.52.19.75/ID/'.'Front_'.$member->id_front.'.jpeg';
            $credentials_2 = 'http://47.52.19.75/ID/'.'Back_'.$member->id_back.'.jpeg';
            
        }
        if (file_exists($filename.'.png'))
        {
            $credentials_1 = 'http://47.52.19.75/ID/'.'Front_'.$member->id_front.'.png';
            $credentials_2 = 'http://47.52.19.75/ID/'.'Back_'.$member->id_back.'.png';
            
        }
        
        if (file_exists($filename.'.PNG'))
        {
            $credentials_1 = 'http://47.52.19.75/ID/'.'Front_'.$member->id_front.'.PNG';
            $credentials_2 = 'http://47.52.19.75/ID/'.'Back_'.$member->id_back.'.PNG';
            
        }
        
        //echo $credentials_1;
        $memberRepo = new MemberRepository;
        
        $adduser = $memberRepo->addMember($username, $password, $member_name, $sponsor_id, $ic_number, $gender, $email, $phone, $country, $address, $ac1, $ac2, $credentials_1, $credentials_2);
        
        print_r($adduser);
    }
  
 
    */
    
    //$updateinfo = $memberRepo->UpdateInfo($username,$idfront,$idback,$sign);
    
    //$adduser = $memberRepo->addMember($username, $password, $member_name, $sponsor_id, $ic_number, $gender, $email, $phone, $country, $address, $ac1, $ac2, $credentials_1, $credentials_2)
    
    //print_r($$adduser);

    /*
    $memberRepo = new MemberRepository;
    $member = $memberRepo->findByUsername(trim($user->username));
    $member->load('wallet');
    $member->load('detail');
    //$member->load('shares');
    $member->load('package');
     */
    ?>

@extends('back.app')

@section('title')
  Admin | {{ config('app.name') }} 
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">Front Page</a></li>
  <li class="active">Dashboard</li>
</ul>
@stop

@section('content')
<main>
  @include('back.include.sidebar')
  <div class="main-container">
    @include('back.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section class="dashboard lighten-3">
        <div class="row">
          <div class="col-md-4">
            <div class="well white">
              <div class="btn-group">
                <?php
                    $memberRepo = new MemberRepository;
                    
                    $members = DB::table('Member')->join('temp_orders', 'Member.username', '=', 'temp_orders.member_username')->join('Member_Wallet', 'Member.id', '=', 'Member_Wallet.member_id')->where('temp_orders.status', '=', 'N')->skip(0)->take(60)->get();
    
                        foreach($members as $member)
                        {
                            
                            $users = DB::table('users')->where('username', '=', $member->username)->first();
                            
                            $member_id = $member->id;
                            $username =  trim($member->username);
                            $password = trim($users->password2);
                            $member_name = $users->first_name;
                            
                            $rbalance = DB::table('Member_Wallet')->where('member_id', '=', $member->id)->first();
                            $register_balance = $rbalance->register_point;
                          
                            $accounts = DB::table('Member_Account')->where('bookB', '=', $member->bookB)->first();
                            
                            $bookA = $accounts->bookA;
                            $bookB = $accounts->bookB;
                            //echo $bookA;
                            $error = 0;
                            
                            $statusBookA = $memberRepo->getTradeStatus2($username,$password,$bookA);
                            $statusBookB = $memberRepo->getTradeStatus2($username,$password,$bookB);
                            
                   
                         if($statusBookA && $statusBookB)
                         {
                       
                           
                            $orderA = $memberRepo->getOrders2($username,$password,$bookA);
                            
                            $orderAP = end($orderA);
                            $orderB = $memberRepo->getOrders2($username,$password,$bookB);
                            $orderBP = end($orderB);
                             
                            // print_r($orderAP);
                            $oldorder = DB::table('Orders')->where('bookAorderid', '=',  $orderAP->OrderId)->first();
                             
                           if(count($oldorder)> 0)
                           {
                               DB::table('temp_orders')
                               ->where('orderid', $member->orderid)
                               ->update(['status' => 'X', 'bookAorderid' => $orderAP->OrderId]);
                           }
                           else
                           {
                            
                            
                            $balanceA = $memberRepo->getBalanceAPI2($username,$password,$bookA);
                            $balanceB = $memberRepo->getBalanceAPI2($username,$password,$bookB);
                            $creditB = $memberRepo->getBookBCredit2($username,$password,$bookB);
                           
                            $openAtime = $orderAP->OpenTime;
                            $openAtime1 = explode("+", $orderAP->OpenTime);
                            $openAtime2 = explode("(", $openAtime1[0]);
                            $openAtime3 = substr($openAtime2[1],0, 10);
                            $openAtime4 = date("Y-m-d H:i:s", $openAtime3);
                       
                           $openBtime = $orderBP->OpenTime;
                            $openBtime1 = explode("+", $orderBP->OpenTime);
                            $openBtime2 = explode("(", $openBtime1[0]);
                            $openBtime3 = substr($openBtime2[1],0, 10);
                            $openBtime4 = date("Y-m-d H:i:s", $openBtime3);
                            
                            $closeAtime = $orderAP->CloseTime;
                            $closeAtime1 = explode("+", $orderAP->CloseTime);
                            $closeAtime2 = explode("(", $closeAtime1[0]);
                            $closeAtime3 = substr($closeAtime2[1],0, 10);
                            $closeAtime4 = date("Y-m-d H:i:s", $closeAtime3);
                            
                            $openBtime = $orderBP->CloseTime;
                            $closeBtime1 = explode("+", $orderBP->CloseTime);
                            $closeBtime2 = explode("(", $closeBtime1[0]);
                            $closeBtime3 = substr($closeBtime2[1],0, 10);
                            $closeBtime4 = date("Y-m-d H:i:s", $closeBtime3);
                            
                            
                            $order_idA = $orderAP->OrderId;
                            $opentimeA = $openAtime3;
                            $openpriceA = $orderAP->OpenPrice;
                            $currencypairA = $orderAP->Symbol;
                            $lotsizeA =  $orderAP->Volume;
                            $closetimeA = $closeAtime3;
                            $closepriceA = $orderAP->ClosePrice;
                            $amountA = $orderAP->Profit;
                            $swapA = $orderAP->Storage;
                            $profitA = $amountA + $swapA;
               
                            $order_idB = $orderBP->OrderId;
                            $opentimeB = $openBtime3;
                            $openpriceB = $orderBP->OpenPrice;
                            $currencypairB = $orderBP->Symbol;
                            $lotsizeB = $orderBP->Volume;
                            $closetimeB = $closeBtime3;
                            $closepriceB = $orderBP->ClosePrice;
                            $amountB = $orderBP->Profit;
                            $swapB = $orderBP->Storage;
                            $profitB = $amountB + $swapB;
                            
                            
                            $lotsizeAA = (ceil($lotsizeB*10)/10)/100;
                            $lotsizeBB = $lotsizeB/100;
                            
                            //Condition - 1 - check lot size
                            if($lotsizeAA <> $lotsizeBB)
                                $error = 1;
                            //Condition - 2 check open time
                            if($opentimeA-$opentimeB > 120 || $opentimeB-$opentimeA > 120)
                                $error = 1;
                            //Condition 3 - profit
                            if(($profitA >= 0 && $profitB >= 0) || ($profitA <= 0 && $profitB <= 0))
                                $error = 1;
                            //Condition - 4 - check SL/TP
                            
                            $priceAdifferent = $closepriceA - $openpriceA;
                            
                            if($profitA > 0)
                            {
                                if($currencypairA == 'XAUUSD+')
                                {
                                    if($priceAdifferent <= 9.2 || $priceAdifferent >= 10.1)
                                        $errors = 1;
                                    
                                }
                                else
                                {
                                    if($priceAdifferent <= 0.0096 || $priceAdifferent >= 0.0106)
                                        $errors = 1;
                                }
                                
                            }
                            else
                            {
                                if($currencypairA == 'XAUUSD+')
                                {
                                    if($priceAdifferent <= 6.82 || $priceAdifferent >= 7.82)
                                        $errors = 1;
                                    
                                }
                                else
                                {
                                    if($priceAdifferent <= 0.00675 || $priceAdifferent >= 0.00775)
                                        $errors = 1;
                                }
                            }
                            
                            $priceBdifferent = $closepriceB - $openpriceB;
                            
                            if($profitB > 0)
                            {
                                if($currencypairB == 'XAUUSD-')
                                {
                                    if($priceBdifferent <= 5.63 || $priceBdifferent >= 6.63)
                                        $errors = 1;
                                
                                }
                                else
                                {
                                    if($priceBdifferent <= 0.00596 || $priceBdifferent >= 0.00696)
                                        $errors = 1;
                                }
                                
                            }
                            else
                            {
                                if($currencypairB == 'XAUUSD-')
                                {
                                    if($priceBdifferent <= 10.36 || $priceBdifferent >= 11.36)
                                        $errors = 1;
                                    
                                }
                                else
                                {
                                    if($priceBdifferent <= 0.1038 || $priceBdifferent >= 0.1138)
                                        $errors = 1;
                                }
                            }
                            
                             
                            
                            if($error == 1)
                            {
                                DB::table('temp_orders')
                                ->where('orderid', $member->orderid)
                                ->update(['status' => 'E', 'bookAorderid' => $order_idA]);
                   
                               DB::table('Orders')->insert([
                                    'member_username' => $username,
                                    'bookB' => $bookB,
                                    'bookAorderid' => $order_idA,
                                    'status' => 'E',
                                    'balancea' => $balanceA,
                                    'balanceb' => $balanceB,
                                    'profitA' => $profitA,
                                    'profitB' => $profitB,
                                    'updatebalanceb' => abs($balanceB),
                                    'rwallet' => abs($profitA),
                                    'created_at' => \Carbon\Carbon::now(),
                                    'updated_at' => \Carbon\Carbon::now()
                                ]);
                            }
                           else
                           {
                               DB::table('temp_orders')
                               ->where('orderid', $member->orderid)
                               ->update(['status' => 'P', 'bookAorderid' => $order_idA]);
                               
                        
                            if($profitA > 0)
                            {
                                $updateBalanceB = abs($balanceB);
                                $updateCreditB = -$creditB;
                                
                                echo "<BR>";
                                echo $member_id;
                                echo "<BR>";
                                echo $member_name;
                                echo "<BR>";
                                echo $bookA;
                                echo "<BR>";
                                echo 'Book A Balance ' .$balanceA;
                                echo "<BR>";
                                echo 'Book B Balance update ' .$updateBalanceB;
                                echo "<BR>";
                                echo 'Book B Credit update ' .$updateCreditB;
                                
                                DB::table('Orders')->insert([
                                    'member_username' => $username,
                                    'bookB' => $bookB,
                                    'bookAorderid' => $order_idA,
                                    'status' => 'P',
                                    'balancea' => $balanceA,
                                    'balanceb' => $balanceB,
                                    'profitA' => $profitA,
                                    'profitB' => $profitB,
                                    'updatebalanceb' => $updateBalanceB,
                                    'rwallet' => 0,
                                    'created_at' => \Carbon\Carbon::now(),
                                    'updated_at' => \Carbon\Carbon::now()
                                ]);
                                
                                
                                $updatebalB = $memberRepo->updateBalanceAPI2($username,$password,$bookB,$updateBalanceB);
                                $updatecredit = $memberRepo->updateCreditAPI2($username,$password,$bookB,$updateCreditB);
                                
                            }
                            
                            if($profitB > 0)
                            {
                                $updateBalanceB = -$profitB;
                                $updateRWallet = abs($profitA);
                                
                                echo "<BR>";
                                echo $member_id;
                                echo "<BR>";
                                echo $member_name;
                                echo "<BR>";
                                echo $bookA;
                                echo "<BR>";
                                echo 'Book A Balance ' .$balanceA;
                                echo "<BR>";
                                echo 'Book B Balance update ' .$updateBalanceB;
                                echo "<BR>";
                                echo 'R-Wallet ' .$updateRWallet;
                                echo "<BR>";
                                echo 'R-Wallet balance ' .$register_balance;
                                
                                DB::table('Orders')->insert([
                                    'member_username' => $username,
                                    'bookB' => $bookB,
                                    'bookAorderid' => $order_idA,
                                    'status' => 'P',
                                    'balancea' => $balanceA,
                                    'balanceb' => $balanceB,
                                    'profitA' => $profitA,
                                    'profitB' => $profitB,
                                    'updatebalanceb' => $updateBalanceB,
                                    'rwallet' => $updateRWallet,
                                    'created_at' => \Carbon\Carbon::now(),
                                    'updated_at' => \Carbon\Carbon::now()
                                ]);
                               
                                $updatebalB = $memberRepo->updateBalanceAPI2($username,$password,$bookB,$updateBalanceB);
                                
                                $remark = 'Credited '.$updateRWallet.' from MT5';
                              
                                DB::table('Member_Wallet')
                                ->where('member_id', $member_id)
                                ->update(['register_point' => $register_balance + $updateRWallet]);
                                
                                DB::table('Member_Wallet_Statement')->insert([
                                    'member_id' => $member_id,
                                    'username' => $username,
                                    'register_amount' => $updateRWallet,
                                    'action_type' => 'MT5 to R-Wallet',
                                    'wallet_type' => 'R',
                                    'transaction_type' => 'C',
                                    'balance' =>  $register_balance + $updateRWallet,
                                    'remark' => $remark,
                                    'created_at' => \Carbon\Carbon::now(),
                                    'updated_at' => \Carbon\Carbon::now()
                                ]);
                               
                                
                            }
                            
                           }
                           }
                    
                         
                           }
                            else
                            {
                                DB::table('temp_orders')
                               ->where('orderid', $member->orderid)
                               ->update(['status' => 'X']);
                            }
                            
                         
                            
                            
                        }
                    
                ?>
              </div>
            </div>
          </div>





      </section>
    </div>
  </div>
</main>
@stop
