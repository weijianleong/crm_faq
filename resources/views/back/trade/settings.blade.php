<?php
    use Carbon\Carbon;
    use Illuminate\Support\Str;

    
    $processlist = DB::table('Orders')
    ->join('users', 'Orders.member_username', '=', 'users.username')
    ->join('Member_Account', 'Orders.bookB', '=', 'Member_Account.bookB')
    ->select('orderid','Member_Account.lockdate as created_at', 'member_username', 'password2', 'bookAorderid','Orders.rwallet as rwallet','checkstatus','Orders.bookB as bookB')
    ->orderBy('Member_Account.lockdate', 'ASC')
     ->where('Orders.status', '=','E')
    ->where('checkstatus', '=','N')->take(200)->get();
    
    //print_r($memberlist);
    
    $destinationPath = config('misc.declarePath');
?>

@extends('back.app')

@section('title')
Process Trade Settings | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">Front Page</a></li>
  <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
  <li class="active">Process Trade</li>
</ul>
@stop

@section('content')
<main>
  @include('back.include.sidebar')
  <div class="main-container">
    @include('back.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section class="tables-data">
        <div class="page-header">
          <h1><i class="md md-wallet-giftcard"></i> Process Trade</h1>
        </div>

        <div class="card">
          <div>
            <table id="packageListTable" class="table table-full table-bordered table-full-small" cellspacing="0" width="100%" role="grid">
              <thead>
                <tr>
                    <th>Lock Date</th>
                  <th>Email</th>
                  <th>Order ID</th>
                  <th>Book B</th>
                  <th>R Wallet</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @if (count($processlist) > 0)
                @foreach ($processlist as $processx)
                <tr>
                <td>
                    <div class="input-group">
                    <span class="input-group-addon">{{ $processx->created_at }}</span>
                    </div>
                  </td>
                  <td>
                    <div class="input-group">
                    <span class="input-group-addon">{{ $processx->member_username }}</span>
                    </div>
                  </td>
                  <td>
                    <div class="input-group">
                      <span class="input-group-addon">{{ $processx->bookAorderid }}</span>
                    </div>
                  </td>
                <td>
                    <div class="input-group">
                      <span class="input-group-addon">{{ $processx->bookB }}</span>
                    <input type="hidden" name="bookB" value="{{ $processx->bookB }}">
                    </div>
                  </td>



                <td>
                <div class="input-group">
                <input type="number" class="form-control" name="rwallet" value="{{ (float) $processx->rwallet }}">
                </div>
                </td>
                <td>
                <div class="switch">
                <label class="filled">
                 <input type="checkbox" name="checkstatus" @if ($processx->checkstatus) checked="checked" @endif> <span class="lever"></span>  </label>
                </div>
                </td>


                  <td>
                    <button class="btn btn-warning btn-flat-border btn-update" data-url="{{ route('admin.trade.update', ['id' => $processx->orderid]) }}" type="submit">
                      <i class="md md-mode-edit"></i> Update
                    </button>
                  </td>
                </tr>
                @endforeach
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </section>
    </div>
  </div>
</main>
@stop
