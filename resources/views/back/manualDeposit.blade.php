@extends('back.app')

@section('title')
    Manual Deposit | {{ config('app.name') }}
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
        <li class="active">Manual Deposit</li>
    </ul>
@stop

@section('content')
<main>
    @include('back.include.sidebar')
    <div class="main-container">
        @include('back.include.header')
        <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
            <section>
                <div class="row m-b-40">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-content">
                                <p class="text-uppercase theme-text">Account Information</p>
                                <form role="form" class="action-form" data-url="{{ route('admin.postDeposit') }}" http-type="post" data-nationality="true">
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="control-label" for="inputEmail">Email<sup>*</sup></label>
                                            <input type="email" name="email" class="form-control" id="email" value="" required=""> 
                                            <div class="form-group">
                                                <button class="btn btn-primary" onclick="search()">Check Email</button>
                                            </div>
                                            <div id="confrim_email"></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="inputID">Deposit Amount<sup>*</sup></label>
                                            <input type="text" name="amount" class="form-control" id="amount" value="" required="">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="inputPhone1">Special Password<sup>*</sup></label>
                                            <input type="password" name="password" class="form-control" id="password" value="" required="">
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">
                                                <span class="btn-preloader">
                                                    <i class="md md-cached md-spin"></i>
                                                </span>
                                                <span>Submit</span>
                                            </button>
                                            <button type="reset" class="btn btn-default">Cancel</button>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</main>

<div class="modal fade" id="warning" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Result</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger" role="alert">
                    This Email not found
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    function search(){
        $.ajax({
            url     : '/search_email',
            method  : 'post',
            data    : {
                username  : $("#email").val()
            },
            headers:
            {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success : function(response){
                
                var show_email = response["first_name"];

                if(show_email === undefined){

                  $('#warning').modal('show');

                }else{
                  // var html = '<div class="form-check">';
                  // html += '<input type="checkbox" checked class="form-check-input" id="exampleCheck1" name="email[]" value='+show_email+'>';
                  var html = '<div class="well white clearfix" style="background-color:lightblue !important;"><label class="form-check-label" for="exampleCheck1">'+show_email+'</label></div>';
                  // html +='</div>';

                  $("#confrim_email").empty().append(html);
                }
            }
        });
    }
</script>
@stop