@extends('back.app')

@section('title')
  Transfer #{{ $model->id }} | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">Front Page</a></li>
    <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
    <li><a href="{{ route('admin.transfer.all') }}">Transfer List</a></li>
    <li class="active">Edit Transfer #{{ $model->id }}</li>
  </ul>
@stop

@section('content')
  <main>
    @include('back.include.sidebar')
    <div class="main-container">
      @include('back.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section>
          <div class="page-header">
            <h1><i class="md md-settings"></i> Editing {{ $model->username }}</h1>
          </div>

              <div class="row m-b-40">
                  <div class="col-md-6">
                    <div class="card">
                      <div class="card-content">
                        <p class="text-uppercase theme-text">Account Information</p>

                        <div class="form-group">
                                <label class="control-label" for="inputFName">Full Name<sup>*</sup></label>
                                <input type="text" name="first_name" class="form-control" id="username" value="{{ $model->username }}" required="" disabled>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="inputEmail">Email<sup>*</sup></label>
                            <input type="text" name="email" class="form-control" id="email" value="{{ $model->email }}" disabled>
                        </div>




                        <form role="form" class="action-form" data-url="{{ route('admin.right.postUpdate', ['id' => $model->id]) }}" http-type="post" data-nationality="true">
                          <fieldset>

                            <div class="form-group" style="padding-bottom: 25px;">
                              <label class="control-label" for="inputNationality">CS Level</label>
                               <input type="text" class="form-control" id="email" value="{{ $model->level }}" name="level"  disabled>
                                    <!--                                   <select name="level" class="form-control" id="level" disabled>
                                    <option value="superadmin" @if($model->level == "superadmin") selected="" @endif >superadmin</option>
                                    <option value="admin" @if($model->level == "admin") selected="" @endif >admin</option>
                                  </select> -->
                            </div>

                            <table style="width: 100%;">
                                <tbody>
                                    @foreach($right as $rights)
                                        <tr style="height: 50px; border-bottom: 1px lightgrey solid;">
                                            <th style="padding-left: 10px;">{{ $rights->name }}</th>
                                            <th>
                                                <div class="switch" style="display: inline-block;">
                                                  <label class="filled">
                                                      <input type="checkbox" name="right[{{ $rights->id}}]" @if(isset($user_right[$rights->right_name]))  checked="checked" @endif>
                                                    <span class="lever"></span>
                                                  </label>
                                                </div>
                                            </th>
                                        </tr>
                                        @if($rights->right_name == "helpdesk")
                                            <tr style="height: 40px;">
                                                <td style="padding-left: 25px;">全能客服</td>
                                                <td>
                                                    <div class="switch">
                                                        <label class="filled">
                                                          <input type="checkbox" name="helpdesk1[0]" @if(!is_null($cs_type[0]))  checked="checked" @endif>
                                                          <span class="lever"></span>
                                                        </label>
                                                      </div>
                                                </td>
                                            </tr>
                                            <tr style="height: 40px;">
                                                <td style="padding-left: 25px;">托管</td>
                                                <td>
                                                    <div class="switch">
                                                        <label class="filled">
                                                          <input type="checkbox" name="helpdesk1[1]" @if(!is_null($cs_type[1]))  checked="checked" @endif>
                                                          <span class="lever"></span>
                                                        </label>
                                                      </div>
                                                </td>
                                            </tr>
                                            <tr style="height: 40px;">
                                                <td style="padding-left: 25px;">出入金</td>
                                                <td>
                                                    <div class="switch">
                                                        <label class="filled">
                                                          <input type="checkbox" name="helpdesk1[2]" @if(!is_null($cs_type[2]))  checked="checked" @endif>
                                                          <span class="lever"></span>
                                                        </label>
                                                      </div>
                                                </td>
                                            </tr>
                                            <tr style="height: 40px;">
                                                <td style="padding-left: 25px;">咨询</td>
                                                <td>
                                                    <div class="switch">
                                                        <label class="filled"> 
                                                          <input type="checkbox" name="helpdesk1[3]" @if(!is_null($cs_type[3]))  checked="checked" @endif>
                                                          <span class="lever"></span> 
                                                        </label>
                                                      </div>
                                                </td>
                                            </tr>
                                            <tr style="height: 40px;">
                                                <td style="padding-left: 25px;">审核</td>
                                                <td>
                                                    <div class="switch">
                                                        <label class="filled"> 
                                                          <input type="checkbox" name="helpdesk1[4]" @if(!is_null($cs_type[4]))  checked="checked" @endif>
                                                          <span class="lever"></span> 
                                                        </label>
                                                      </div>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>

                          </fieldset>
                            <br/>
                            <div class="form-group">
                              <button type="submit" class="btn btn-primary">
                                <span class="btn-preloader">
                                  <i class="md md-cached md-spin"></i>
                                </span>
                                <span>Submit</span>
                              </button>
                              <button type="reset" class="btn btn-default">Cancel</button>
                            </div>
                        </form>
                      </div>
                    </div>
                  </div>


            
              </div>

        </section>
      </div>
    </div>
  </main>
@stop
