@extends('back.app')

@section('title')
Login - {{ config('app.name') }}
@stop

@section('content')
<div class="center">
  <div class="card bordered z-depth-2" style="margin:0% auto; max-width:400px;">
    <div class="card-header">
      <div class="brand-logo">
        TR Admin Login
      </div>
    </div>
    <form class="form-floating action-form" http-type="post" data-url="{{ route('admin.postLogin') }}">
      <div class="card-content">

        <div class="form-group">
          <label for="email" class="control-label">Email</label>
          <input type="text" name="email" class="form-control" id="email" required="">
        </div>
        <div class="form-group">
          <label for="password" class="control-label">Password</label>
          <input type="password" name="password" class="form-control" id="password" required="">
        </div>
        <div class="form-group">
            {!! captcha_img('flat') !!}
            <input type="text" class="form-control" name="captcha" id="captcha" required="">

        </div>
      </div>
      <div class="card-action clearfix">
        <div class="pull-right">
          <button type="submit" class="btn btn-link black-text">
            <span class="btn-preloader">
              <i class="md md-cached md-spin"></i>
            </span>
            <span>Login</span>
          </button>
        </div>
      </div>
    </form>
  </div>
</div>

@stop
