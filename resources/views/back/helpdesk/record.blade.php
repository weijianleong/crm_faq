<?php
    $user = \Sentinel::getUser();
?>

@extends('back.app')

@section('title')
  All Website | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">Front Page</a></li>
    <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
    <li class="active">Helpdesk List</li>
  </ul>
@stop

@section('content')
  <main>
    @include('back.include.sidebar')
    <div class="main-container">
      @include('back.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section class="tables-data">
        <div class="page-header">
          <h1><i class="md md-local-attraction"></i> Helpdesk Statement</h1>
        </div>

        <div class="card">
          <div class="card-content">
            <div class="datatables">
              <table class="table table-full table-full-small dt-responsive display nowrap table-grid" cellspacing="0" width="100%" role="grid" id="" data-url="{{ route('admin.helpdesk.getRecord') }}">
                <thead>
                  <tr>
                    <th data-id="created_at" >Date</th>
                    <th data-id="email" >UserEmail</th>
                    <th data-id="reply_case" >Reply Number Of Question</th>
                    <th data-id="reply_sec" >Averge response time</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </section>
      </div>
    </div>
  </main>
@stop
