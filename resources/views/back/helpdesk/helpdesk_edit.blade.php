<?php
  $helpdeskPath = config('misc.helpdeskPath_testing');
  $user = \Sentinel::getUser();
?>

@extends('back.app')

@section('title')
  Create Website | {{ config('app.name') }}
@stop

@inject('helpdesk_edit_presenter','App\Presenters\back\helpdesk\helpdesk_edit')

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">Front Page</a></li>
    <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
    <!-- check cs type dashboard -->
    {!! $helpdesk_edit_presenter->cs_dashboard($user->cs_type) !!} 

    <li class="active">Edit Helpdesk</li>
  </ul>
@stop

@section('content')
  <main>
    @include('back.include.sidebar')
    <div class="main-container">
      @include('back.include.header')


      
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section>

          <div class="page-header">
            <h1><i class="md md-local-attraction">{!! $model->tittle !!}</i> </h1>
          </div>

          @if (session('status'))
              <div class="alert alert-danger">
                  {{ session('status') }}
              </div>
          @endif

          <div class="row m-b-40">
              <div class="col-md-2">
                
                <!-- check cs type Back Pending Page -->
                {!! $helpdesk_edit_presenter->cs_BackPending($user->cs_type) !!} 
              </div>

              <div class="col-md-8">
                    
                    <!-- check day and change color -->
                    {!! $helpdesk_edit_presenter->check_day( $model->refrence_id , $lengthOfAd) !!} 

                    {{ $model->username }}：
                    <div class="well  clearfix"  style="background-color: #add8e6">
                      <p class="theme-text pull-right">
                        <i class="md md-alarm"></i> {{ $model->created_at->format('d F Y H:i A') }}
                          <!-- check pic and show -->
                          {!! $helpdesk_edit_presenter->show_pic( $helpdeskPath , $model->pic_1 , $model->pic_2) !!} 
                      </p>

                      <div class="pull-left">
                          {!! $model->content !!}
                      </div>
                    </div>

                      @foreach($model2 as $comment)
                          @if( empty($comment->cs_username) )
                          
                            {{ $comment->client_username }}:
                            <div class="well  clearfix" style="background-color: #add8e6">
                              <p class="theme-text pull-right">
                                <i class="md md-alarm"></i> {{ $comment->created_at->format('d F Y H:i A') }}
                                  <!-- check pic and show -->
                                  {!! $helpdesk_edit_presenter->show_pic( $helpdeskPath , $comment->pic_1 , $comment->pic_2) !!} 
                              </p>
                                <div class="pull-left">
                                  {!! $comment->comment !!}
                                </div>

                            </div>
                          
                          @else
                          
                            {{ $comment->cs_username }}:
                            <div class="well  clearfix">
                              <p class="theme-text pull-right">
                                <i class="md md-alarm"></i> {{ $comment->created_at->format('d F Y H:i A') }}

                                  <!-- check pic and show -->
                                  {!! $helpdesk_edit_presenter->show_pic( $helpdeskPath , $comment->pic_1 , $comment->pic_2) !!} 

                              </p>
                                <div class="pull-left">
                                  {!! $comment->comment !!}
                                </div>

                            </div>
                          
                          @endif

                      @endforeach

                      @if($model->status != "done") 

                        <form  id="" role="form" class="form-floating" action="{{ route('admin.helpdesk.postcreate') }}"  
                          method="post" enctype="multipart/form-data" data-parsley-validate>

                            <div class="well white clearfix">
                              <div class="form-group">
                                  <textarea name="Comment" class="form-control" required=""></textarea>
                               
                              </div>

                              <div class="form-group">
                                  <label class="control-label" >@lang('helpdesk.QsPic')</label><BR>
                                      <input type="file" name="pic_1" class="form-control" id="pic_1" >

                              </div>

                              <div class="form-group">
                                  <label class="control-label" >@lang('helpdesk.QsPic')</label><BR>
                                      <input type="file" name="pic_2" class="form-control" id="pic_2">
                              </div>
                            </div>


                            <div class="form-group">
                                <input type="hidden" name="helpdesk_id"  value="{{ $model->id }}" >
                                <input type="hidden" name="helpdesk_type"  value="{{ $model->type }}" >
                                <input type="hidden" value="{{ csrf_token() }}" name="_token">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                  <span class="btn-preloader">
                                    <i class="md md-cached md-spin"></i>
                                  </span>
                                  <span>@lang('common.submit')</span>
                                </button>
                                <button type="reset" class="btn btn-default">@lang('common.cancel')</button>
                            </div>
                        </form>
                      @endif
              </div>

              <div class="col-md-2">
              
                <button type="button" class="btn btn-warning"  id="update_btn" data-target="#transfer_modal " data-toggle="modal">
                  Transfer CS
                </button>

                <button type="button" class="btn btn-primary"  id="update_btn" data-target="#setting_modal " data-toggle="modal">
                  Update Status
                </button>      
              </div>

          </div>
        </section>
      </div>
    </div>
  </main>

  <div class="modal fade" id="transfer_modal" tabindex="-1" role="dialog"  aria-hidden="true">
      <form id="" role="form" class="action-form" data-url="{{ route('admin.helpdesk.transfer') }}" data-parsley-validate="" 
        onsubmit="return false;" http-type="post">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLongTitle">Transfer to Another CS</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                                  
                                  <div class="form-group">
                                    <label class="control-label" for="transfer_cs">Transfer To Who</label>
                                        <select class="form-control" name="to_cs_id" id="to_cs_id">
                                              
                                            @foreach($admin_model as $user)

                                                <option value="{!!$user->cs_type!!}-{!!$user->id!!}-{!!$user->username!!}">
                                                  <!-- check cs type and show  -->
                                                  {!! $helpdesk_edit_presenter->show_type($user->cs_type) !!} 

                                                  {{ $user->username }}
                                                </option>
                                              
                                            @endforeach
                                        </select>
                                  </div>



                                  <div class="form-group">
                                    <input type="hidden" name="helpdesk_id"  value="{{ $model->id }}" >
                                    <input type="hidden" name="helpdesk_type"  value="{{ $model->type }}" >
                                  </div>

                                  
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary" >Save changes</button>
                    </div>
                  </div>
                </div>
      </form>
  </div>

  <div class="modal fade" id="setting_modal" tabindex="-1" role="dialog"  aria-hidden="true">
      <form id="" role="form" class="action-form" data-url="{{ route('admin.helpdesk.update', ['id' => $model->id]) }}" data-parsley-validate="" 
        onsubmit="return false;" http-type="post">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLongTitle">Update Comment Status</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>

                    <div class="modal-body">

                      <div class="form-group">
                        <label class="control-label" >Selecet Status</label>
                            <select class="form-control" name="status" id="status">
                                  
                                
                                <option value="resolved" @if ($model->status == 'resolved') selected="selected" @endif>
                                  Resolved
                                </option>

                                <option value="done" @if ($model->status == 'done') selected="selected" @endif>
                                  Close Case
                                </option>
                                 
                            </select>
                      </div>


                      <div id = "reply" >
                        <div class="form-group">
                          <label class="control-label">Remark</label>  
                        </div>

                        <div class="well white clearfix">
                          <div class="form-group">
                              <textarea name="remark" class="form-control" required="">{{ $model->remark }}</textarea> 
                          </div>
                        </div>
                      </div>

                    </div>

                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary" >Save changes</button>
                    </div>
                  </div>
                </div>
      </form>
  </div>
@stop


