@inject('helpdesk_edit_presenter','App\Presenters\back\helpdesk\helpdesk_edit')

@if (count($model) > 0)

<!--       <form id="Tran" role="form" class="action-form" action="{{ route('admin.helpdesk.transfer') }}" data-parsley-validate="" 
        onsubmit="return false;" http-type="post"> -->
<form id="Tran" role="form" class="action-form" data-url="{{ route('admin.helpdesk.transfer') }}" data-parsley-validate="" 
onsubmit="return false;" http-type="post">


            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Transfer to Another CS</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">

                          
                          <div class="form-group">
                            <label class="control-label" for="transfer_cs">Transfer To Who</label>
                                <select class="form-control" name="to_cs_id" id="to_cs_id">
                                            @foreach($admin_model as $user)

                                                <option value="{!!$user->cs_type!!}-{!!$user->id!!}-{!!$user->username!!}">
                                                  <!-- check cs type and show  -->
                                                  {!! $helpdesk_edit_presenter->show_type($user->cs_type) !!} 

                                                  {{ $user->username }}
                                                </option>
                                              
                                            @endforeach

                                </select>
                          </div>



                          <div class="form-group">
                            <input type="hidden" name="helpdesk_id" id="helpdesk_id"  value="{{ $model->id }}" >
                            <input type="hidden" name="helpdesk_type"  id="helpdesk_type"  value="{{ $model->type }}" >
                            <input type="hidden" value="{{ csrf_token() }}" name="_token">

                          </div>

                          
            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" onclick="transfer()">Save changes</button>
            </div>

</form>
  

@else
<div class="alert alert-danger">
  History not found.
</div>
@endif

  <script>
    function transfer(){

        //alert($("#to_cs_id").val());

      $.ajax({
            url     : '/admin/helpdesk/transfer_list',
            method  : 'post',
            data    : {

                to_cs_id  : $("#to_cs_id").val(),
                helpdesk_id  : $("#helpdesk_id").val(),
                helpdesk_type  : $("#helpdesk_type").val()
               
            },
            headers:
            {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success : function(response){
                
                alert(response["msg"]);

                //$("#mat_params").val(response["top"]);
            }
        });
    }

  </script>