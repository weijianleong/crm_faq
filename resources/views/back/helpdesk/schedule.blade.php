<?php
    $user = \Sentinel::getUser();

?>

@extends('back.app')

@section('title')
  All Website | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">Front Page</a></li>
    <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
    <li class="active">schedule List</li>
  </ul>
@stop

@section('content')
  <main>
    @include('back.include.sidebar')
    <div class="main-container">
      @include('back.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section class="tables-data">

            <form role="form" class="action-form" data-url="{{ route('admin.helpdesk.postSchedule') }}" http-type="post" data-nationality="true">
                <div class="page-header">
                    <select  class="btn btn-primary" name="schedule_date" id="schedule_date" onchange="search()">    
                            @foreach($getday as $key => $getdays)
                                <option value="{{ $key }}" @if ($date == $key) selected="" @endif > {{ $getdays }}</option>
                            @endforeach
                            

                    </select>
                </div>

                <div class="page-header">
                  <h1><i class="md md-local-attraction"></i> schedule </h1>
                </div>

                <div class="card">
                  <div class="card-content">
                    <table style="width: 100%;">
                        <tbody>
                            <tr>
                                <th></th>
                                <th>am</th>
                                <th>pm</th>
                            </tr>
                            @foreach($model as $key => $models)
                                <tr style="height: 50px; border-bottom: 1px lightgrey solid;">
                                    <th style="padding-left: 10px;">{{ $models->username }}</th>
                                    <th>
                                        <div class="switch" style="display: inline-block;">
                                          <label class="filled">
                                              <input type="checkbox" value="{{ $models->id}}" name="am[]" @if ($models->shift == 1) checked="checked" @endif> 
                                            <span class="lever"></span>
                                          </label>
                                        </div>
                                    </th>

                                    <th>
                                        <div class="switch" style="display: inline-block;">
                                          <label class="filled">
                                              <input type="checkbox" value="{{ $models->id}}" name="pm[]" @if ($models->shift == 2) checked="checked" @endif> 
                                            <span class="lever"></span>
                                          </label>
                                        </div>
                                    </th>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                  </div>
                </div>

                <div class="form-group">
                  <button type="submit" class="btn btn-primary">
                    <span class="btn-preloader">
                      <i class="md md-cached md-spin"></i>
                    </span>
                    <span>Submit</span>
                  </button>
                  <button type="reset" class="btn btn-default">Cancel</button>
                </div>


            </form>





      </section>
      </div>
    </div>
  </main>


@stop

    <script>
        function search(){


          $.ajax({
                url     : '/set_session',
                method  : 'post',
                data    : {

                    get_date  : $("#schedule_date").val()
                   
                },
                headers:
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success : function(response){
                    window.location.href = '{{route("admin.helpdesk.schedule")}}';
                }
            });
        }

    </script>


