<?php
  $helpdeskPath = config('misc.helpdeskPath_testing');
   $user = \Sentinel::getUser();
?>
@extends('back.app')


@section('title')
  Create Website | {{ config('app.name') }}
@stop

@inject('helpdesk_edit_presenter','App\Presenters\back\helpdesk\helpdesk_edit')

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">Front Page</a></li>
    <li><a href="{{ route('admin.home') }}">Dashboard</a></li>

    {!! $helpdesk_edit_presenter->cs_dashboard($user->cs_type) !!} 
    <li class="active">Black List</li>
  </ul>
@stop

@section('content')
  <main>
    @include('back.include.sidebar')
    <div class="main-container">
      @include('back.include.header')
      
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section>



          <div class="row m-b-40">
              <div class="col-md-2">
                <!-- check cs type Back Pending Page -->
                {!! $helpdesk_edit_presenter->cs_BackPending($user->cs_type) !!}
              </div>




              <div class="col-md-8">
                      

                      <div class="form-group">
                          <label class="control-label" for="inputFName">Client Email<sup>*</sup></label>
                          <input type="text" name="email" class="form-control" id="email" value="" required="">
                      </div>

                      <div class="form-group">
                            
                              <button type="submit" class="btn btn-primary" onclick="search()">
                                <span class="btn-preloader">
                                  <i class="md md-cached md-spin"></i>
                                </span>
                                <span>Check Email</span>
                              </button>

                      </div>

                    <form id="" role="form" class="action-form" data-url="{{ route('admin.helpdesk.postBlackList') }}"  
                        data-parsley-validate="" onsubmit="return false;" http-type="post">
                        <div class="form-group">
                          <label class="control-label">Confirm BlackList Email : </label>  
                        </div>

                        <div class="well white clearfix">

                          <div id="confrim_email"></div>

                        </div>


                         <div class="form-group">
                          <button type="submit" class="btn btn-primary">
                            <span class="btn-preloader">
                              <i class="md md-cached md-spin"></i>
                            </span>
                            <span>Submit</span>
                          </button>
                          
                        </div>
                    </form>
              </div>



    
          </div>
        </section>
      </div>
    </div>
  </main>


  <div class="modal fade" id="warning" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Result</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="alert alert-danger" role="alert">
            This Email not found
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <script>
    function search(){


      $.ajax({
            url     : '/search_email',
            method  : 'post',
            data    : {

                username  : $("#email").val()
               
            },
            headers:
            {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success : function(response){
                
                var show_email = response["email"];

                if(show_email === undefined){

                  $('#warning').modal('show');

                }else{

                  
                  var html = '<div class="form-check">';
                  html += '<input type="checkbox" checked class="form-check-input" id="exampleCheck1" name="email[]" value='+show_email+'>';
                  html += '<label class="form-check-label" for="exampleCheck1">'+show_email+'</label>';
                  html +='</div>';

                  $("#confrim_email").append(html);
                }

                //$("#mat_params").val(response["top"]);
            }
        });
    }

  </script>


@stop




      


