<?php
    $user = \Sentinel::getUser();
?>

@extends('back.app')

@section('title')
  All Website | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">Front Page</a></li>
    <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
    <li class="active">Helpdesk List</li>
  </ul>
@stop

@section('content')
  <main>
    @include('back.include.sidebar')
    <div class="main-container">
      @include('back.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section class="tables-data">
        <div class="page-header">
          <h1><i class="md md-local-attraction"></i> Your Ticket</h1>
        </div>

        <div class="card">
          <div class="card-content">
            <div class="datatables">
              <table class="table table-full table-full-small dt-responsive display nowrap table-grid" cellspacing="0" width="100%" role="grid" id="" data-url="{{ route('admin.helpdesk.getList', ['id' => $user->id,'type_id' => $user->cs_type]) }}">
                <thead>
                  <tr>
                    <th data-id="refrence_id" >ID</th>
                    <th data-id="username" data-orderable="false">Email</th>
                    <th data-id="created_at">Date</th>
                    <th data-id="tittle" data-orderable="false">Tittle</th>
                    <th data-id="type" data-orderable="false">Type</th>
                    <th data-id="status" >Case Status</th>
                    <th data-id="is_reply" >Reply Status</th>
                    <th data-id="action" data-orderable="false">Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div class="page-header">
          <h1><i class="md md-local-attraction"></i> Open Ticket</h1>
        </div>

        <div class="card">
          <div class="card-content">
            <div class="datatables">
              <table class="table table-full table-full-small dt-responsive display nowrap table-grid" cellspacing="0" width="100%" role="grid" id="" data-url="{{ route('admin.helpdesk.getOpenList', ['id' => $user->id,'type_id' => $user->cs_type]) }}">
                <thead>
                  <tr>
                    <th data-id="refrence_id" >ID</th>
                    <th data-id="username" data-orderable="false">Email</th>
                    <th data-id="created_at">Date</th>
                    <th data-id="tittle" data-orderable="false">Tittle</th>
                    <th data-id="type" data-orderable="false">Type</th>
                    <th data-id="status" >Case Status</th>
                    <th data-id="is_reply" >Reply Status</th>
                    <th data-id="action" data-orderable="false">Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </section>
      </div>
    </div>
  </main>

      <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">

            <div class="modal-body">

              <div class="loading text-center">
                <img src="{{ asset('assets/img/loading.gif') }}" alt="Network Loading">
                <br>
                <small class="text-primary">loading..</small>
              </div>

              <div class="error text-center">
                <i class="md md-error"></i>
                <br>
                <small class="text-danger">Something went wrong</small>
              </div>

              <div id="modalContent">
              </div>

            </div>

          </div>
        </div>
    </div>
@stop
