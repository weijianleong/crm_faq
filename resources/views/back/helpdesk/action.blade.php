<a href="{{ route('admin.helpdesk.edit', ['id' => $model->id]) }}" class="btn btn-info btn-flat-border">
  <i class="md md-input"></i> Read
</a>

<button class="btn btn-info btn-flat-border btn-show" title="Detail" data-url="{{ route('admin.helpdesk.show', ['id' => $model->id]) }}" data-toggle="modal" data-target="#showModal">
    <i class="md md-visibility"></i> History
</button>





@if($user->cs_type =="0")
<button class="btn btn-info btn-flat-border btn-show" title="Detail" data-url="{{ route('admin.helpdesk.showTran', ['id' => $model->id]) }}" data-toggle="modal" data-target="#showModal">
    <i class="md-swap-horiz"></i> Tran
</button>



    

@endif

@if(($model->rate_star!="0"))
    {{$model->rate_star}}  <i class="glyphicon glyphicon-star"></i> 
@endif

@if(($model->is_complain!="0"))
    <i class="glyphicon glyphicon-thumbs-down"></i> 
@endif



