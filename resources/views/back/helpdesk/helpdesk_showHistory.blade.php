@inject('helpdesk_showHistory_presenter','App\Presenters\back\helpdesk\helpdesk_showHistory')

@if (count($model) > 0)
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="showModalLabel">Helpdesk History</h4>
</div>

<div class="row">
          <div class="card" >
          <div style="overflow: auto">
            <table id="packageListTable" class="table table-full table-bordered table-full-small" cellspacing="0" width="100%" role="grid">
              <thead>
                <tr>
                  <th>Username</th>
                  <th>Action</th> 
                  <th>To Who</th>
                  <th>Comment</th>
                  <th>Time</th>
                  
                  
                </tr>
              </thead>
              <tbody>
                
                @foreach ($model as $comment)
                <tr>

                 <td>
                    <div class="input-group">
                      <span class="input-group-addon">{{ $comment->cs_username }}</span>
                    </div>
                  </td>

                  <td>
                    <div class="input-group">

                          <!-- check action type-->
                          {!! $helpdesk_showHistory_presenter->action_type($comment->action) !!} 
                      
   
                      
                    </div>
                  </td>
                  <td>
                          @if($comment->action == "T")
                            <!-- check action and show msg-->
                            {!! $helpdesk_showHistory_presenter->action_msg($comment->cs_type , $comment->to_cs_username) !!}


                         
                          @endif
                      
                  </td>
                  <td>
                          @if($comment->action == "C")
                            <a href="#" title="Comment" data-toggle="popover" data-trigger="focus" data-content="{{ $comment->comment }}">Check</a>
                          @elseif($comment->action == "R")
                            <a href="#" title="Remark" data-toggle="popover" data-trigger="focus" data-content="{{ $comment->comment }}">Remark</a>
                          @elseif($comment->action == "D")
                            <a href="#" title="Remark" data-toggle="popover" data-trigger="focus" data-content="{{ $comment->comment }}">Remark</a>
                          @endif
                      
                  </td>

                  <td>     
                          <div class="input-group">
                            <span class="input-group-addon">{{ $comment->created_at}}</span>
                          </div>
                  </td>
                </tr>
                @endforeach
               
              </tbody>
            </table>
          </div>
</div>

<div class="modal-footer">
  <button type="button" class="btn btn-danger btn-raised" data-dismiss="modal">Close</button>
</div>

@else
<div class="alert alert-danger">
  History not found.
</div>
@endif

<script>
  $(document).ready(function(){
      $('[data-toggle="popover"]').popover();   
  });
</script>

<style>

.popover-content {
    word-wrap: break-word;
}
</style>