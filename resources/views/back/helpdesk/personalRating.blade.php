<?php
    $user = \Sentinel::getUser();

?>

@extends('back.app')

@section('title')
  All Website | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">Front Page</a></li>
    <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
    <li class="active">Helpdesk List</li>
  </ul>
@stop

@section('content')
  <main>
    @include('back.include.sidebar')
    <div class="main-container">
      @include('back.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section class="tables-data">

        <div class="page-header">
            <select  class="btn btn-primary" name="personal_date" id="personal_date" onchange="search()">    
              @foreach($select_date as $data)
                    <option value="{{date('Y-m', strtotime($data->created_at)) }}" @if ($date == date('Y-m', strtotime($data->created_at))) selected="" @endif> {{date('Y-m', strtotime($data->created_at)) }}</option>
              @endforeach   
            </select>
        </div>

        <div class="page-header">
          <h1><i class="md md-local-attraction"></i> Personal Rating </h1>
        </div>

        <div class="card">
          <div class="card-content">
            <div class="datatables">
              <table class="table table-full table-full-small dt-responsive display nowrap table-grid" cellspacing="0" width="100%" role="grid" id="" data-url="{{ route('admin.helpdesk.getPersonalRating', ['date' => $date]) }}">
                <thead>
                  <tr>
                    <th data-id="email" >email</th>
                    <!-- <th data-id="complain" >total complain</th> -->
                    <th data-id="rate_star" data-searchable="false">total star</th>
                    <th data-id="ticket" data-searchable="false">ticket</th>
                    <th data-id="average" data-searchable="false">average</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div class="page-header">
          <h1><i class="md md-assignment-late"></i> Complain Record </h1>
        </div>

        <div class="card">
          <div class="card-content">
            <div class="datatables">
              <table class="table table-full table-full-small dt-responsive display nowrap table-grid" cellspacing="0" width="100%" role="grid" id="" data-url="{{ route('admin.helpdesk.getComplain', ['date' => $date]) }}">
                <thead>
                  <tr>
                    <th data-id="email" >email</th>
                    <th data-id="complain" data-searchable="false" >total complain</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>

      </section>
      </div>
    </div>
  </main>


@stop

    <script>
        function search(){


          $.ajax({
                url     : '/set_session',
                method  : 'post',
                data    : {

                    get_date  : $("#personal_date").val()
                   
                },
                headers:
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success : function(response){
                    window.location.href = '{{route("admin.helpdesk.personalRating")}}';
                }
            });
        }

    </script>


