<?php
use App\Models\Package;
//$packages = Package::where('status', 'Y')
    $packages = Package::orderBy('id', 'asc')
        ->get();
?>

@extends('back.app')

@section('title')
Package Settings | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">Front Page</a></li>
  <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
  <li class="active">Package Settings</li>
</ul>
@stop

@section('content')
<main>
  @include('back.include.sidebar')
  <div class="main-container">
    @include('back.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section class="tables-data">
        <div class="page-header">
          <h1><i class="md md-wallet-giftcard"></i> Package</h1>
        </div>

        <div class="card">
          <div>
            <table id="packageListTable" class="table table-full table-bordered table-full-small" cellspacing="0" width="100%" role="grid">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Monthly %</th>
                  <th>Direct Sponsor %</th>
                  <th>Group Level %</th>
                  <th>Group Level</th>
                  <th>Price</th>
                  <th>Status</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @if (count($packages) > 0)
                @foreach ($packages as $package)
                <tr>
                  <td>
                    <div class="input-group">
                    <input type="text" class="form-control" name="title" value="{{  $package->title }}" required="">
                    </div>
                  </td>

                  <td>
                    <div class="input-group">
                      <input type="number" class="form-control" name="month_percent" value="{{ (integer) $package->month_percent }}" required="" min="0">
                      <span class="input-group-addon">%</span>
                    </div>
                  </td>

                  <td>
                    <div class="input-group">
                      <input type="number" class="form-control" name="direct_percent" value="{{ (integer) $package->direct_percent }}" required="" min="0">
                      <span class="input-group-addon">%</span>
                    </div>
                  </td>
                    <td>
                    <div class="input-group">
                    <input type="number" class="form-control" name="group_percent" value="{{ (integer) $package->group_percent }}" required="" min="0">
                    <span class="input-group-addon">%</span>
                    </div>
                    </td>
                  <td>
                    <div class="input-group">
                      <input type="number" name="group_level" class="form-control" value="{{ (integer) $package->group_level }}" required="" min="0">
                      <span class="input-group-addon">level(s)</span>
                    </div>
                  </td>
                <td>
                <div class="input-group">
                <span class="input-group-addon">$</span>
                <input type="number" class="form-control" name="package_amount" value="{{ (float) $package->package_amount }}" required="" min="0">
                </div>
                </td>
                <td>
                <div class="switch">
                <label class="filled">
                 <input type="checkbox" name="status" @if ($package->status) checked="checked" @endif> <span class="lever"></span>  </label>
                </div>
                </td>


                  <td>
                    <button class="btn btn-warning btn-flat-border btn-update" data-url="{{ route('admin.package.update', ['id' => $package->id]) }}" type="submit">
                      <i class="md md-mode-edit"></i> Update
                    </button>
                  </td>
                </tr>
                @endforeach
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </section>
    </div>
  </div>
</main>
@stop
