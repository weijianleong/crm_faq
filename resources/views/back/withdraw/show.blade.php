<?php $member = $model->member; ?>

@if ($member)
<?php $detail = $member->detail; ?>
<?php $withdrawPath = config('misc.withdrawPath_live')?>
<div class="row">
  <div class="col-md-6 col-xs-12">
    <div class="table-responsive">
      <table class="table table-bordered table-hover">
        <tbody>

          <tr>
            <td>Username</td>
            <td>:</td>
            <td>{{ $member->username }}</td>
          </tr>
          <tr>
            <td>Identification Card No</td>
            <td>:</td>
            <td>@if ($detail->identification_number != '') {{ $detail->identification_number }} @else <label class="label label-danger">ID still empty</label>@endif</td>
          </tr>
          <tr>
            <td>Amount</td>
            <td>:</td>
            <td>{{ number_format($model->amount,2) }} </td>
          </tr>
          @if( $model->status == 'done') 
            <tr>
            <td>Receipt</td>
            <td>:</td>
            <td><a target=_blank href="{{ route('admin.amazon.read', ['folder' => 'Jreceipt', 'filename' => $model->receipt]) }}">View</a></td>
            <!-- <td><a target=_blank href="{{$withdrawPath}}{{ $model->receipt}}">View</a></td> -->
            </tr>
          @endif
        </tbody>
      </table>
    </div>
  </div>

  <div class="col-md-6 col-xs-12">
    <div class="table-responsive">
      <table class="table table-bordered table-hover">
          <tbody>

                        @if( $model->payment_type == 'bank') 
                          <tr>
                            <td>Bank Name</td>
                            <td>:</td>
                            <td>@if ($model->bank_name != '') {{ $model->bank_name }} @else <label class="label label-danger">Bank Name empty</label> @endif</td>
                          </tr>
                          <tr>
                            <td>Bank Account Number</td>
                            <td>:</td>
                            <td>@if ($model->bank_account_number != '') {{ $model->bank_account_number }} @else <label class="label label-danger">Bank Number still empty</label>@endif</td>
                          </tr>
                          <tr>
                            <td>Bank Account Holder</td>
                            <td>:</td>
                            <td>@if ($model->bank_account_holder != '') {{ $model->bank_account_holder }} @else <label class="label label-danger">Bank Holder still empty</label>@endif</td>
                          </tr>
                          <tr>
                            <td>Bank Address</td>
                            <td>:</td>
                            <td>@if ($model->bank_address != '') {{ $model->bank_address }} @else <label class="label label-danger">Bank Address still empty</label>@endif</td>
                          </tr>
                          <tr>
                            <td>Bank Country</td>
                            <td>:</td>
                            <td>@if ($model->bank_country != '') {{ $model->bank_country }} @else <label class="label label-danger">Bank Country still empty</label>@endif</td>
                          </tr>
                          <tr>
                            <td>Bank Swiftcode</td>
                            <td>:</td>
                            <td>@if ($model->bank_swiftcode != '') {{ $model->bank_swiftcode }} @else <label class="label label-danger">Bank Swiftcode still empty</label>@endif</td>
                          </tr>
                        @else
                          <tr>
                            <td>Card Number</td>
                            <td>:</td>
                            <td>@if ($model->card_number != '') {{ $model->card_number }} @else <label class="label label-danger">Card Number empty</label> @endif</td>
                          </tr>
                          <tr>
                            <td>Card Country</td>
                            <td>:</td>
                            <td>@if ($model->card_country != '') {{ $model->card_country }} @else <label class="label label-danger">Card Country still empty</label>@endif</td>
                          </tr>
                        @endif
          </tbody>
      </table>
    </div>
  </div>
</div>
@else
<div class="alert alert-danger">
  Member not found.
</div>
@endif
