<?php $member = $model->member; ?>
<?php $detail = $member->detail; ?>
<?php $user = $member->user; ?>
<?php $withdrawPath = config('misc.withdrawPath_live')?>

@extends('back.app')

@section('title')
  Withdraw #{{ $model->id }} | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
<!--     <li><a href="#">Front Page</a></li>
    <li><a href="{{ route('admin.home') }}">Dashboard</a></li> -->
    <li><a href="{{ route('admin.withdraw.all') }}">Withdraw List</a></li>
    <li class="active">Edit </li>
  </ul>
@stop

@section('content')
  <main>
    @include('back.include.sidebar')
    <div class="main-container">
      @include('back.include.header')

      
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section>
          <div class="row m-b-10">
            <div class="col-md-8">
              <div class="well white">
                
                  @foreach ($errors->all() as $error)
                  <div class="alert alert-danger">
                      {{ $error }}
                  </div>
                  @endforeach
               


                <form class="form-floating" name="registerForm" id="registerForm"  action="{{ route('admin.withdraw.update', ['id' => $model->id]) }}" method="post" enctype="multipart/form-data" data-parsley-validate>


                  <input type="hidden" value="{{ csrf_token() }}" name="_token">
                  <fieldset>
                    <div class="form-group">
                      <label class="control-label">UserName</label>
                      <input type="text" class="form-control" value="{{ $model->username }}" disabled="">
                      
                    </div>

                    <div class="form-group">
                      <label class="control-label">Payment</label>
                      <input type="text" class="form-control" value="{{ $model->payment_type }}" disabled="">
                      <input type="hidden"  value="{{  $model->payment_type }}"  name="payment_type" >
                    </div>

                    <div class="form-group">
                      <label class="control-label">Withdraw Detail : </label><br>
                      <button class="btn btn-info btn-flat-border btn-show" title="Detail" data-url="{{ route('admin.withdraw.show', ['id' => $model->id]) }}" data-toggle="modal" data-target="#showModal">
                        <i class="md md-visibility"></i> Detail
                      </button>
                    </div>

                    <div class="form-group">
                      <label class="control-label">Status</label>
                      <select class="form-control" name="status" >
                        <option value="process" {{ (Input::old('status') == 'process' ? 'selected':'') }}>Please Select</option>
                        <option value="done" {{ (Input::old('status') == 'done' ? 'selected':'') }}>Done</option>
                        <option value="reject" {{ (Input::old('status') == 'reject' ? 'selected':'') }}>Reject</option>

                      </select>
                    </div>

                    <div class="form-group">
                      <label class="control-label">Amount</label>
                      <input type="text" step="any" class="form-control" value="{{ number_format($model->amount,2) }}"  disabled="">
                      <input type="hidden"  value="{{ $model->amount }}"  name="amount" >
                    </div>

                    
                    @if(!empty ($model->receipt))
                      <div class="form-group">
                        
                        <a target=_blank href="{{ route('admin.amazon.read', ['folder' => 'Jreceipt', 'filename' => $model->receipt]) }}">Check receipt</a>
                        <!-- <a target=_blank href="{{$withdrawPath}}{{ $model->receipt}}">Check receipt</a> -->
                      </div>
                    @endif

                    <div class="form-group">
                        <label class="control-label" >Upload Receipt</label><BR>
                            <input type="file" name="receipt" class="form-control" id="receipt" >

                    </div>



                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">
                        <span class="btn-preloader">
                          <i class="md md-cached md-spin"></i>
                        </span>
                        <span>Submit</span>
                      </button>
                      <button type="reset" class="btn btn-default">Cancel</button>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>

  <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="showModalLabel">Withdraw Detail</h4>
        </div>
        <div class="modal-body">

          <div class="loading text-center">
            <img src="{{ asset('assets/img/loading.gif') }}" alt="Network Loading">
            <br>
            <small class="text-primary">loading..</small>
          </div>

          <div class="error text-center">
            <i class="md md-error"></i>
            <br>
            <small class="text-danger">Something went wrong</small>
          </div>

          <div id="modalContent">
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-raised" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@stop
