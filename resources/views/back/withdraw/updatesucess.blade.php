@extends('back.app')

@section('title')
  All Withdraws | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
<!--     <li><a href="#">Front Page</a></li>
    <li><a href="{{ route('admin.home') }}">Dashboard</a></li> -->
    <li class="active">Withdrawal status updated</li>
  </ul>
@stop

@section('content')
  <main>
    @include('back.include.sidebar')
    <div class="main-container">
      @include('back.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section>
          <div class="page-header">
            <h1><i class="md md-done-all"></i> Updated Successfully</h1>
          </div>

          <div class="row m-b-40">
            <div class="col-md-8">
              <div class="well white text-center">
                <h1 class="theme-text text-uppercase w300">Withdrawal status updated </h1>
                <h3></h3>
                <a href="{{ route('admin.withdraw.pending') }}" class="btn btn-primary btn-block m-t-50">
                  <i class="md md-navigate-before"></i>Back to Withdrawal Pending Apprval page
                </a>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>

  
@stop
