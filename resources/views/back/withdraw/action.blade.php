<!-- <button class="btn btn-info btn-flat-border btn-show" title="Detail" data-url="{{ route('admin.withdraw.show', ['id' => $model->id]) }}" data-toggle="modal" data-target="#showModal">
  <i class="md md-visibility"></i> Detail
</button> -->
@if($model->status == "done" || $model->status == "reject" )
                      <button class="btn btn-info btn-flat-border btn-show" title="Detail" data-url="{{ route('admin.withdraw.show', ['id' => $model->id]) }}" data-toggle="modal" data-target="#showModal">
                        <i class="md md-visibility"></i> Detail
                      </button>
@else
<a class="btn btn-warning btn-flat-border" title="Detail" href="{{ route('admin.withdraw.edit', ['id' => $model->id]) }}">
  <i class="md md-mode-edit"></i> Edit
</a>
@endif

<!-- <button class="btn btn-delete btn-danger btn-flat-bordered" data-url="{{ route('admin.withdraw.remove', ['id' => $model->id]) }}">
  <i class="md md-deleet"></i> Remove  
</button> -->
