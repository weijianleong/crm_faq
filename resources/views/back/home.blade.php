<?php
    use Carbon\Carbon;
    use App\Repositories\SharesRepository;
    $repo = new SharesRepository;
    $state = $repo->getCurrentShareState();
    $lastUpdate = Carbon::createFromFormat('Y-m-d H:i:s', $state->updated_at);
    $mt = \App::isDownForMaintenance();
    
    $user = \Sentinel::getUser();
    
    ?>

@extends('back.app')

@section('title')
Admin | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
<li><a href="#">Front Page</a></li>
<li class="active">Dashboard</li>
</ul>
@stop

@section('content')
<main>
@include('back.include.sidebar')
<div class="main-container">
@include('back.include.header')
<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
@if($user->email == 'admin@trealcap.com')
<section class="dashboard lighten-3">
<div class="col-md-12">
<div class="row">
    <div class="col-md-4">
    <div class="well white">
    <div class="btn-group">

    <a href="{{ route('admin.runbonus') }}"><i class="md md-blur-on"></i>&nbsp;<span>Run Bonus</span></a>

    </div>
    <div class="btn-group">


    <a href="{{ route('admin.bulkupdatepass') }}"><i class="md md-blur-on"></i>&nbsp;<span>Batch Update Password</span></a>
    </div>
    <div class="btn-group">


    <a href="{{ route('admin.tradeprocess') }}"><i class="md md-blur-on"></i>&nbsp;<span>Trade Process</span></a>
    </div>

    <div class="btn-group">


    <a href="{{ route('admin.tradesync') }}"><i class="md md-blur-on"></i>&nbsp;<span>Trade Sync</span></a>
    </div>

    <div class="btn-group">
        <a href="{{ route('admin.updateinvestorpass') }}"><i class="md md-blur-on"></i>&nbsp;<span>Update Investor Password</span></a>
    </div>
    </div>
</div>

<div class="col-md-4">
<div class="well white">
<p>Site Maintenace: @if ($mt) <span style="color:#f00;">ON</span> @else <span style="color:#239c1d;">OFF</span> @endif</p>
<button id="btnMaintenance" data-url="{{ route('mt.toggle') }}" class="btn btn-danger btn-flat-border">
<span class="btn-preloader">
<i class="md md-cached md-spin"></i>
</span>
<span><i class="md md-settings-power"></i> Toggle Maintenance</span>
</button>
</div>
</div>

<div class="col-md-12">
<div class="row">
    <div class="col-md-4">
    <div class="well white">
    <div class="btn-group">

    <div class="btn-group">

    </div>
    </div>
</div>



</section>
@endif
</div>
</div>
</main>
@stop
