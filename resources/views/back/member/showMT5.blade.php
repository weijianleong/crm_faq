@if (count($model) > 0)
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="showModalLabel">MT5</h4>
</div>

<div class="row">
          <div class="card" >
          <div style="overflow: auto">
            <table id="packageListTable" class="table table-full table-bordered table-full-small" cellspacing="0" width="100%" role="grid">
              <thead>
                <tr>
                  <th>Account A</th>
                  <th>Time</th>
                  
                  
                </tr>
              </thead>
              <tbody>
                
                @foreach ($model as $comment)
                <tr>

                 <td>
                    <div class="input-group">
                      <span class="input-group-addon">{{ $comment->bookA }}</span>
                    </div>
                  </td>

                  <td>
                    <div class="input-group">
                      <span class="input-group-addon">{{ $comment->cr8_at }}</span>
                    </div>
                  </td>
                </tr>
                @endforeach
               
              </tbody>
            </table>
          </div>
</div>

<div class="modal-footer">
  <button type="button" class="btn btn-danger btn-raised" data-dismiss="modal">Close</button>
</div>

@else
<div class="alert alert-danger">
  History not found.
</div>
@endif

<script>
  $(document).ready(function(){
      $('[data-toggle="popover"]').popover();   
  });
</script>

<style>

.popover-content {
    word-wrap: break-word;
}
</style>