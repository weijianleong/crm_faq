@extends('back.app')

@section('title')
  Member List | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">Front Page</a></li>
    <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
    <li class="active">Member List</li>
  </ul>
@stop

@section('content')
  <main>
    @include('back.include.sidebar')
    <div class="main-container">
      @include('back.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section class="tables-data">
          <div class="page-header">
            <h1><i class="md md-group-add"></i> Member List</h1>
          </div>

          <div class="card">
            <div>
              <div class="datatables">
                <table class="table table-full table-full-small dt-responsive display nowrap table-grid" cellspacing="0" width="100%" role="grid" data-url="{{ route('admin.member.getList') }}">
                  <thead>
                    <tr>
                      
                      <th data-id="username">Username</th>
                      <th data-id="user.first_name">Name</th>
                      <th data-id="password" data-orderable="false" data-searchable="false">Password</th>
                      <th data-id="secret_password" data-orderable="false" data-searchable="false">Security Password</th>
                      <th data-id="direct" data-orderable="false" data-searchable="false">Sponsor</tg>
                      <th data-id="status" data-orderable="false" data-searchable="false">Status</th>
                      <th data-id="lpoa"  data-searchable="false">LPOA</th>
                      <th data-id="is_ban"  data-searchable="false" data-orderable="false">is_ban</th>
                      <th data-id="created_at">Joined Date</th>
                      <th data-id="action" data-orderable="false" data-searchable="false">
                        Action
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>

  
      <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">

            <div class="modal-body">

              <div class="loading text-center">
                <img src="{{ asset('assets/img/loading.gif') }}" alt="Network Loading">
                <br>
                <small class="text-primary">loading..</small>
              </div>

              <div class="error text-center">
                <i class="md md-error"></i>
                <br>
                <small class="text-danger">Something went wrong</small>
              </div>

              <div id="modalContent">
              </div>

            </div>

          </div>
        </div>
    </div>
@stop
