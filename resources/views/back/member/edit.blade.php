<?php
  use App\Models\Package;
  
  $mUser = $model->user;
  $detail = $model->detail;
  session()->forget('old_data_detail');
  session()->forget('old_data_muser');
  session()->push('old_data_muser', $mUser);
  session()->push('old_data_detail', $detail);

  //$packages = Package::all();
    
    $idFront = $model->id_front;
    $idBack = $model->id_back;
    
    $destinationPath = config('misc.imagePath');
    
    $internalPath = public_path('/ID/');
    
    if(count(explode('.',$idFront)) == 1)
    {
        $filename = $internalPath.'Front_'.$idFront;
        
        if (file_exists($filename.'.JPG'))
        {
            $idFront = 'Front_'.$idFront.'.JPG';
            $idBack = 'Back_'.$idBack.'.JPG';
            
        }
        
        if (file_exists($filename.'.jpg'))
        {
            $idFront = 'Front_'.$idFront.'.jpg';
            $idBack = 'Back_'.$idBack.'.jpg';
            
        }
        
        if (file_exists($filename.'.jpeg'))
        {
            $idFront = 'Front_'.$idFront.'.jpeg';
            $idBack = 'Back_'.$idBack.'.jpeg';
            
        }
        if (file_exists($filename.'.png'))
        {
            $idFront = 'Front_'.$idFront.'.png';
            $idBack = 'Back_'.$idBack.'.png';
            
        }
        
        if (file_exists($filename.'.PNG'))
        {
            $idFront = 'Front_'.$idFront.'.PNG';
            $idBack = 'Back_'.$idBack.'.PNG';
            
        }
    }
?>

@extends('back.app')

@section('title')
  Edit Member | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">Front Page</a></li>
    <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
    <li><a href="{{ route('admin.member.list2') }}">Member List</a></li>
    <li class="active">Edit Member</li>
  </ul>
@stop

@section('content')
  <main>
    @include('back.include.sidebar')
    <div class="main-container">
      @include('back.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section>
          <div class="page-header">
            <h1><i class="md md-settings"></i> Editing {{ $model->username }}</h1>
          </div>
          <div class="row m-b-40">
            <div class="col-md-6">
              <div class="card">
                <div class="card-content">
                  <p class="text-uppercase theme-text">Account Information</p>
                  <form role="form" class="action-form" data-url="{{ route('admin.member.postUpdate', ['id' => $model->id]) }}" http-type="post" data-nationality="true">
                    <fieldset>
                      <input type="hidden" name="is_update_basic" value="true">
                      <div class="form-group">
                        <label class="control-label normal">Active?</label>
                        <div class="switch">
                          <label class="filled"> No
                          <input type="checkbox" name="process_status" @if ($model->process_status == 'Y') checked="checked" @endif> <span class="lever"></span> Yes </label>
                        </div>
                      </div>


                      <div class="form-group">
                        <label class="control-label" for="inputFName">Full Name<sup>*</sup></label>
                        <input type="text" name="first_name" class="form-control" id="first_name" value="{{ $mUser->first_name }}" required="">
                      </div>

                    <div class="form-group">
                        <label class="control-label" for="inputEmail">Email<sup>*</sup></label>
                        <input type="text" name="email" class="form-control" id="email" value="{{ $mUser->email }}" 
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="inputID">Identification Number<sup>*</sup></label>
                        <input type="text" name="ID" class="form-control" id="ID" value="{{ $detail->identification_number }}" required="">
                    </div>

                <div class="table-responsive">
                        <table class="table table-hover table-striped">
                          <tr>
                            <td class="theme-text">@lang('settings.idfront')</td>
                            <td>:</td>
                            <td><a href="{{ route('admin.amazon.read', ['folder' => 'ID', 'filename' => $idFront]) }}" target="blank">@lang('settings.onclick')</a></td>
                            <!-- <td><a href="{{ $destinationPath.$idFront }}" target="blank">@lang('settings.onclick')</a></td> -->
                          </tr>

                          <tr>
                            <td class="theme-text">@lang('settings.idback')</td>
                            <td>:</td>
                            <td><a href="{{ route('admin.amazon.read', ['folder' => 'ID', 'filename' => $idBack]) }}"  target="blank">@lang('settings.onclick')</a></td>
                            <!-- <td><a href="{{ $destinationPath.$idBack }}" target="blank">@lang('settings.onclick')</a></td> -->
                          </tr>

                        </table>
                      </div>

                      <?php $countries = config('misc.countries'); ksort($countries); ?>
                      <div class="form-group">
                        <label class="control-label" for="inputNationality">Country</label>
                        <select class="form-control dd-icon" name="nationality" id="inputNationality">
                          @foreach ($countries as $country => $value)
                            <option value="{{ $country }}" @if ($detail->nationality == $country) selected="" @endif data-imagesrc="{{ asset('assets/img/flags/' . $country . '.png') }}" data-description="Selected Country">{{ $country }}</option>
                          @endforeach
                        </select>
                      </div>

                    <div class="form-group">
                        <label class="control-label" for="inputPhone1">Contact<sup>*</sup></label>
                        <input type="text" name="phone1" class="form-control" id="phone1" value="{{ $detail->phone1 }}" required="">
                    </div>



                      <div class="form-group">
                        <button type="submit" class="btn btn-primary">
                          <span class="btn-preloader">
                            <i class="md md-cached md-spin"></i>
                          </span>
                          <span>Submit</span>
                        </button>
                        <button type="reset" class="btn btn-default">Cancel</button>
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>


                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>
@stop
