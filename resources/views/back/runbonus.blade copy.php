<?php
    use App\Models\Member;
    use App\Repositories\MemberRepository;
    use App\Repositories\BonusRepository;
    use Carbon\Carbon;
    
    $members = DB::table('Member')->where('package_amount', '=', 200)->get();
    //$members = DB::table('Member')->where('is_active', '=', 1)->get();
     //$members = DB::table('Member')->where('id', '=', 1)->get();
    
    $calculation = config('misc.bonus.calculation');
    $mybonus = array();

    //print_r($calculation['cash']);
    foreach($members as $member)
    {
        
        $date1 = new DateTime('now');
        $date1->modify('last day of previous month');
        $date2 = $date1->format('Y-m-d');
        
        $date3 = date_create($member->created_at);
        $date4 = $date3->format('Y-m-d');
     
        $datediff = strtotime($date2) - strtotime($date4);
        $datediff2 = floor($datediff / (60 * 60 * 24));

        
       $today =  new DateTime();
       
       $lastday = new DateTime('now');
       $lastday->modify('last day of previous month');
        $startdate =  date_create($member->created_at);
       $diff = date_diff($startdate, $lastday);
       $totaldays = $lastday->format('d');
       $prodays = $diff->format('%d');
       $prorated = $datediff2/$totaldays;
        
        $month_bonus = ($member->month_percent / 100) * $member->package_amount;
        

      if($datediff2 > 0)
      {
        if($diff->format('%m') >= 1)
        {
            $month_bonus = $month_bonus;
        }
        else
        {
            $month_bonus = $prorated * $month_bonus;
        }
        
       // print_r($diff);
       // print_r('<BR>');
        $totalgroup = 0;
        if($member->group_level > 0)
        {
            
            $memberRepo = new MemberRepository;
            $children = $memberRepo->findDirect($member);
        
            if(count($children) > 0)
            {
                foreach ($children as $child) {
                    
                    $group_bonus =($child->group_percent / 100) * $child->package_amount;
                    
                    if($diff->format('%m') >= 1)
                    {
                    
                        $totalgroup += $group_bonus;
                    }
                    else
                    {
                        $totalgroup += $prorated * $group_bonus;
                    }
                    
                    if($member->group_level > 1)
                    {
                        $children2 = $memberRepo->findDirect($child);
            
                        if(count($children2) > 0)
                        {
                            foreach ($children2 as $child2) {
                    
                                $group_bonus =($child2->group_percent / 100) * $child2->package_amount;
                    
                                    if($diff->format('%m') >= 1)
                                    {
                    
                                        $totalgroup += $group_bonus;
                                    }
                                    else
                                    {
                                        $totalgroup += $prorated * $group_bonus;
                                    }
                                
                                if($member->group_level > 2)
                                {
                                    $children3 = $memberRepo->findDirect($child2);
            
                                    if(count($children3) > 0)
                                    {
                                        foreach ($children3 as $child3) {
                    
                                            $group_bonus =($child3->group_percent / 100) * $child3->package_amount;
                    
                                            if($diff->format('%m') >= 1)
                                            {
                    
                                                $totalgroup += $group_bonus;
                                            }
                                            else
                                            {
                                                $totalgroup += $prorated * $group_bonus;
                                            }
                    
                                        }
                                    }
                                }
                    
                            }
                        }
                    }
                }
            }
            
            
        }
      }
      else
      {
          $month_bonus = 0;
          $totalgroup = 0;
      }
        
        $mcash = ($calculation['cash'] / 100) * $month_bonus;
        $mpromotion = ($calculation['promotion'] / 100) * $month_bonus;
        
        $gcash = ($calculation['cash'] / 100) * $totalgroup;
        $gpromotion = ($calculation['promotion']  / 100) * $totalgroup;
        
        $mybonus[] = array($member->username, number_format($mcash,2).' - '.number_format($mpromotion,2), number_format($gcash,2).' - '.number_format($gpromotion,2));
        
        $memberRepo = new MemberRepository;
        $member2 = $memberRepo->findByUsername(trim($member->username));
        $member2->load('wallet');
        /*
        $bonusRepo = new BonusRepository;
        $mBonus = $bonusRepo->calculateMonth ($member2, $mcash, $mpromotion,$lastday);
        $gBonus = $bonusRepo->calculateGroup ($member2, $gcash, $gpromotion,$lastday);
         */
    }
   
    $i = 1;
    /*
    $memberRepo = new MemberRepository;
    $member = $memberRepo->findByUsername(trim($user->username));
    $member->load('wallet');
    $member->load('detail');
    //$member->load('shares');
    $member->load('package');
     */
    ?>

@extends('back.app')

@section('title')
  Admin | {{ config('app.name') }} 
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">Front Page</a></li>
  <li class="active">Dashboard</li>
</ul>
@stop

@section('content')
<main>
  @include('back.include.sidebar')
  <div class="main-container">
    @include('back.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section class="dashboard lighten-3">
        <div class="table-responsive">
        <table class="table table-hover table-striped">
                    <tr>
                        <td class="theme-text">No</td>
                        <td class="theme-text">Member</td>
                        <td class="theme-text">Month Bonus</td>
                        <td class="theme-text">Direct Bonus</td>
                    </tr>

                   @foreach($mybonus as $k => $v)
                    <tr>
                        <td> {{ $i++ }} </td>
                        @foreach ($v as $key => $value)
                            <td>{{ $value }}</td>

                        @endforeach
                    </tr>
                    @endforeach

        </table>

    </div>





      </section>
    </div>
  </div>
</main>
@stop
