<aside class="sidebar fixed" style="width:260px;left:0px;">
  <div class="brand-logo">
    <div id="logo">
      <img src="{{ asset('assets/img/logo.png') }}" width="100">
    </div>
  </div>



  <ul class="menu-links">
    <li icon="md md-blur-on">
      <a href="{{ route('admin.home') }}"><i class="md md-blur-on"></i>&nbsp;<span>Home</span></a>
    </li>

    <li icon="md md-settings">
        <a href="{{ route('admin.settings.account') }}"><i class="md md-settings"></i>&nbsp;<span>Profile</span></a>
    </li>

    <li icon="md md-accessibility">
        <a href="{{ route('admin.member.list') }}"><i class="md md-accessibility"></i>&nbsp;<span>Members</span></a>
    </li>

    <li icon="md md-swap-vert">
        <a href="{{ route('admin.transfer.addStatement') }}"><i class="md md-swap-vert"></i>&nbsp;<span>Member Point</span></a>
    </li>


    <li icon="md md-assignment-returned">
    <a href="{{ route('admin.withdraw.all') }}"><i class="md md-assignment-returned"></i>&nbsp;<span>Withdrawal</span></a>
    </li>

    <li>
    <a href="#" data-toggle="collapse" data-target="#MDBonus" aria-expanded="false" aria-controls="MDBonus" class="collapsible-header waves-effect"><i class="md md-attach-money"></i>&nbsp;Reports</a>
    <ul id="MDBonus" class="collapse">
    <li>
    <a href="{{ route('admin.bonus.all') . '?t=group' }}">
    <span>Sales Report</span>
    </a>
    </li>
    <li>
    <a href="{{ route('admin.bonus.all') . '?t=direct' }}">
    <span>Wallet Report</span>
    </a>
    </li>

    <li>
    <a href="{{ route('admin.bonus.all') . '?t=group' }}">
    <span>Commission Report</span>
    </a>
    </li>


    </ul>
    </li>

    <li icon="md md-wallet-giftcard">
    <a href="{{ route('admin.settings.declare') }}"><i class="md md-wallet-giftcard"></i>&nbsp;<span>Declaration form</span></a>
    </li>


    <li>
      <a href="#" data-toggle="collapse" data-target="#MDAnnouncement" aria-expanded="false" aria-controls="MDAnnouncement" class="collapsible-header waves-effect"><i class="md md-new-releases"></i>&nbsp;Announcement</a>
      <ul id="MDAnnouncement" class="collapse">
        <li>
          <a href="{{ route('admin.announcement.create') }}">
            <span>Create</span>
          </a>
        </li>
        <li>
          <a href="{{ route('admin.announcement.list') }}">
            <span>List</span>
          </a>
        </li>
      </ul>
    </li>

    <li>
    <a href="#" data-toggle="collapse" data-target="#MDMessaging" aria-expanded="false" aria-controls="MDMessaging" class="collapsible-header waves-effect"><i class="md md-email"></i>&nbsp;Messaging</a>
    <ul id="MDMessaging" class="collapse">
    <li>
    <a href="{{ route('admin.announcement.list') }}">
    <span>Inbox</span>
    </a>
    </li>
    <li>
    <a href="{{ route('admin.announcement.create') }}">
    <span>Compose</span>
    </a>
    </li>
    <li>
    <a href="{{ route('admin.announcement.list') }}">
    <span>Outbox</span>
    </a>
    </li>
    </ul>
    </li>


    </li>

    <li icon="md md-settings-power">
      <a href="{{ route('admin.logout') }}"><i class="md md-settings-power"></i>&nbsp;<span>Logout</span></a>
    </li>
  </ul>
</aside>
