<?php
        $user = \Sentinel::getUser();
        $rights = Session::get('right');

        $get_right = DB::table('admin_sbar')
                    ->whereIn('right_name', $rights)
                    ->get();
        //  print_r($get_right);exit();        
        foreach ($get_right as $data) {
            if($data->parent_id > 0){
                $temp['name'] = (string)$data->name;
                $temp['routes'] = (string)$data->routes;
                $temp['icon'] = (string)$data->icon;
                $temp['class'] = (string)$data->class;
                $temp['data_target'] = (string)$data->data_target;

                $rightList[$data->parent_id]['child'][] = $temp;
            }else{
                $rightList[$data->id]['name'] = (string)$data->name;
                $rightList[$data->id]['routes'] = (string)$data->routes;
                $rightList[$data->id]['icon'] = (string)$data->icon;
                $rightList[$data->id]['class'] = (string)$data->class;
                $rightList[$data->id]['data_target'] = (string)$data->data_target;
            }
        }
         //print_r($rightList[5]["child"]);exit();
?>
<aside class="sidebar fixed" style="width:260px;left:0px;">
  <div class="brand-logo">
    <div id="logo">
      <img src="{{ asset('assets/img/new_logo.png') }}" width="100">
    </div>
  </div>



  <ul class="menu-links">

    @foreach($rightList as $rightItem)
        @if(!empty($rightItem['child']) )
            <li>
              <a href="#" data-toggle="collapse" data-target="#{{ $rightItem['data_target'] }}" aria-expanded="false" aria-controls="{{ $rightItem['data_target'] }}" class="collapsible-header waves-effect"><i class="{{ $rightItem['class'] }}"></i>&nbsp;{{ $rightItem['name'] }}</a>
              <ul id="{{ $rightItem['data_target'] }}" class="collapse">

                @foreach($rightItem['child'] as $child)
                    <li icon="{{ $child['icon'] }}">
                      <a href="{{ route($child['routes'] ) }}"><i class="{{ $child['class'] }}"></i>&nbsp;<span>{{ $child['name'] }}</span></a>
                    </li>
                @endforeach

              </ul>
            </li>
        @else
            <li icon="{{$rightItem['icon']}}">
              <a href="{{ route($rightItem['routes']) }}"><i class="{{$rightItem['class']}}"></i>&nbsp;<span>{{ $rightItem['name'] }}</span></a>
            </li>
        @endif

    @endforeach



<!-- <li icon="md md-settings">
        <a href="{{ route('admin.settings.account') }}"><i class="md md-settings"></i>&nbsp;<span>Profile</span></a>
    </li>
@if($user->email == 'admin@trfxsupport.com' )
  <li icon="md md-accessibility">
    <a href="{{ route('admin.withdraw.all') }}"><i class="md md-accessibility"></i>&nbsp;<span>Withdraw Statement</span></a>
  </li>
  <li icon="md md-accessibility">
    <a href="{{ route('admin.withdraw.pending') }}"><i class="md md-accessibility"></i>&nbsp;<span>Withdrawal Pending Approval</span></a>
  </li>
@else
<li icon="md md-blur-on">
<a href="{{ route('admin.home') }}"><i class="md md-blur-on"></i>&nbsp;<span>Home</span></a>
</li>

<li icon="md md-blur-on">
<a href="{{ route('admin.right') }}"><i class="md md-blur-on"></i>&nbsp;<span>Edit Right</span></a>
</li>

@if($user->email == 'admin@trealcap.com' || $user->email == 'beekun@trealcap.com' || $user->email == 'chia@trealcap.com' || $user->email == 'demi@trealcap.com' || $user->email == 'ruby@trealcap.com' || $user->email == 'ishmael@trealcap.com' || $user->email == 'emma@trealcap.com' || $user->email == 'thean@trealcap.com')



<li icon="md md-accessibility">
<a href="{{ route('admin.member.list') }}"><i class="md md-accessibility"></i>&nbsp;<span>Members</span></a>
</li>
@endif


    <li icon="md md-accessibility">
    <a href="{{ route('admin.member.list2') }}"><i class="md md-accessibility"></i>&nbsp;<span>Members Pending Verification</span></a>
    </li>

    </li>

    <li icon="md md-wallet-giftcard">
    <a href="{{ route('admin.settings.declare') }}"><i class="md md-wallet-giftcard"></i>&nbsp;<span>LPOA</span></a>
    </li>

</li>

<li icon="md md-wallet-giftcard">
<a href="{{ route('admin.settings.trade') }}"><i class="md md-wallet-giftcard"></i>&nbsp;<span>Process Trade</span></a>
</li>

    <li>
      <a href="#" data-toggle="collapse" data-target="#helpdesk" aria-expanded="false" aria-controls="helpdesk" class="collapsible-header waves-effect"><i class="glyphicon glyphicon-envelope"></i>&nbsp;Helpdesk</a>
      <ul id="helpdesk" class="collapse">

        @if($user->cs_type=="0")
            <li icon="glyphicon glyphicon-envelope">
              <a href="{{ route('admin.helpdesk.Plist') }}"><i class="glyphicon glyphicon-envelope"></i>&nbsp;<span>Help Desk</span></a>
            </li>
        @else
            <li icon="md md-accessibility">
              <a href="{{ route('admin.helpdesk.list') }}"><i class="glyphicon glyphicon-envelope"></i>&nbsp;<span>Pending Help Desk</span></a>
            </li>
        @endif


            <li icon="md md-accessibility">
              <a href="{{ route('admin.helpdesk.multiSend') }}"><i class="md md-accessibility"></i>&nbsp;<span>Multi Send</span></a>
            </li>
      </ul>
    </li>

    <li>
      <a href="#" data-toggle="collapse" data-target="#MDAnnouncement" aria-expanded="false" aria-controls="MDAnnouncement" class="collapsible-header waves-effect"><i class="md md-new-releases"></i>&nbsp;Announcement</a>
      <ul id="MDAnnouncement" class="collapse">
        <li>
          <a href="{{ route('admin.announcement.create') }}">
            <span>Create</span>
          </a>
        </li>
        <li>
          <a href="{{ route('admin.announcement.list') }}">
            <span>List</span>
          </a>
        </li>
      </ul>
    </li>
@endif -->
  

    <li icon="md md-settings-power">
      <a href="{{ route('admin.logout') }}"><i class="md md-settings-power"></i>&nbsp;<span>Logout</span></a>
    </li>
  </ul>
</aside>
