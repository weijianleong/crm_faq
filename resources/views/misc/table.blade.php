{{--@foreach($faqs as $faq)--}}
{{--@if($locale === 'en')--}}
{{--    <div class="card"  style="margin-left: 8%; margin-right: 8%">--}}

{{--    <div class="card-header">--}}
{{--        <strong style="font-size: 20px; margin-bottom: 10px">Q{{ $loop->iteration }}. {{ $faq->title_en }}</strong>--}}
{{--    </div>--}}

{{--    <div class="card-body">--}}
{{--        <p style="margin-top: 5px">{{ $faq->content_en }}</p>--}}
{{--    </div>--}}
{{--    </div>--}}
{{--    <br>--}}
{{--@else--}}
{{--    <div class="card"  style="margin-left: 10%; margin-right: 8%">--}}

{{--    <div class="card-header">--}}
{{--        <strong style="font-size: 20px; margin-bottom: 10px">Q{{ $loop->iteration }}. {{ $faq->title_cn }}</strong>--}}
{{--    </div>--}}

{{--    <div class="card-body" s>--}}
{{--        <p style="margin-top: 5px">{{ $faq->content_cn }}</p><br>--}}
{{--    </div>--}}
{{--    </div>--}}
{{--    <br>--}}
{{--@endif--}}
{{--@endforeach--}}
{{--<br>--}}
<div class="tab-pane table-responsive reset padding-all fade" id="{{ $id }}">
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTable no-footer" style="text-align: center; width: 100%;" id="{{ $id }}List" role="grid">
            <thead>
            <tr>
                <th>@lang('faq.table_title')</th>
                <th>@lang('faq.table_content')</th>
            </tr>
            </thead>
            <tbody>
            @if($locale === 'en')
                @foreach($faqs as $faq)
                    <tr>
                        <th>{{ $faq->title_en }}</th>
                        <th>{{ $faq->content_en }}</th>
                    </tr>
                @endforeach
            @else
                @foreach($faqs as $faq)
                    <tr>
                        <th>{{ $faq->title_cn }}</th>
                        <th>{{ $faq->content_cn }}</th>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
</div>


