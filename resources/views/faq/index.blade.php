@extends('front.app')

@section('title')
    @lang('faq.title') | {{ config('app.name') }}
@endsection

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/animate/css/animate.min.css')}}"/>
    <link type="text/css" rel="stylesheet"
          href="{{asset('assets/vendors/datatables/css/colReorder.bootstrap.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}"/>

@endsection

@section('main_content')
    @include('front.include.header')
    <div id="content" class="bg-container">
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-lg-5">
                        <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('faq.title')</h4>
                    </div>
                    <div class="col-lg-7">
                        <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                            <li class=" breadcrumb-item">
                                <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                                    <i class="fa fa-home" data-pack="default"
                                       data-tags=""></i> @lang('breadcrumbs.dashboard')
                                </a>
                            </li>
                            <li class="breadcrumb-item active">@lang('faq.title')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
        <div class="outer">
            <div class="inner bg-container">
                <div class="row web-mail">

                    <div class="col-lg-12">
                        <div class="card mail media_max_991 m-t-35">
                            <div class="card-body m-t-25 p-d-10">
                                <div class="tabs tabs-bordered tabs-icons">
                                    <ul class="nav nav-tabs">
                                        <li class="nav-item " id="primary2">
                                            <a href="#all" class="nav-link active " data-toggle="tab"
                                               aria-expanded="true"><i
                                                        class="fa fa-question-circle"></i> @lang('faq.all')
                                            </a>
                                        </li>
                                        <li class="nav-item" id="promotions2">
                                            <a href="#fund" class="nav-link" data-toggle="tab"
                                               aria-expanded="false"><i
                                                        class="fa fa-question-circle"></i> @lang('faq.fund') &nbsp;
                                            </a>
                                        </li>
                                        <li class="nav-item" id="promotions2">
                                            <a href="#withdraw" class="nav-link" data-toggle="tab"
                                               aria-expanded="false"><i
                                                        class="fa fa-question-circle"></i> @lang('faq.withdrawal')
                                                &nbsp;
                                            </a>
                                        </li>
                                        <li class="nav-item" id="promotions2">
                                            <a href="#advisory" class="nav-link" data-toggle="tab"
                                               aria-expanded="false"><i
                                                        class="fa fa-question-circle"></i> @lang('faq.advisory') &nbsp;
                                            </a>
                                        </li>
                                        <li class="nav-item" id="promotions2">
                                            <a href="#approval" class="nav-link" data-toggle="tab"
                                               aria-expanded="false"><i
                                                        class="fa fa-question-circle"></i> @lang('faq.approval') &nbsp;
                                            </a>
                                        </li>
                                        <li class="nav-item" id="promotions2">
                                            <a href="#capx" class="nav-link" data-toggle="tab"
                                               aria-expanded="false"><i
                                                        class="fa fa-question-circle"></i> @lang('faq.capx') &nbsp;
                                            </a>
                                        </li>
                                    </ul>

                                <!-- Tab panes -->
                                        <div class="tab-content">
                                            <br>
                                            <div class="tab-pane table-responsive reset padding-all fade active show " id="all">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered table-hover dataTable no-footer" style="text-align: center; width: 100%;" id="idList" role="grid">
                                                        <thead>
                                                        <tr>
                                                            <th>@lang('faq.table_title')</th>
                                                            <th>@lang('faq.table_content')</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if($locale === 'en')
                                                            @foreach($faqs as $faq)
                                                                <tr>
                                                                    <th>{{ $faq->title_en }}</th>
                                                                    <th>{{ $faq->content_en }}</th>
                                                                </tr>
                                                            @endforeach
                                                        @else
                                                            @foreach($faqs as $faq)
                                                                <tr>
                                                                    <th>{{ $faq->title_cn }}</th>
                                                                    <th>{{ $faq->content_cn }}</th>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            @component('misc.table')
                                                @slot('faqs', $funds)
                                                @slot('locale', $locale)
                                                @slot('id', 'fund')
                                            @endcomponent

                                            @component('misc.table')
                                                @slot('faqs', $withdraws)
                                                @slot('locale', $locale)
                                                @slot('id', 'withdraw')
                                            @endcomponent

                                            @component('misc.table')
                                                @slot('faqs', $advisories)
                                                @slot('locale', $locale)
                                                @slot('id', 'advisory')
                                            @endcomponent

                                            @component('misc.table')
                                                @slot('faqs', $approvals)
                                                @slot('locale', $locale)
                                                @slot('id', 'approval')
                                            @endcomponent

                                            @component('misc.table')
                                                @slot('faqs', $capxs)
                                                @slot('locale', $locale)
                                                @slot('id', 'capx')
                                            @endcomponent
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /.inner -->
    </div>
    @include('front.include.modal')
@endsection

@section('footer_scripts')

    <script type="text/javascript" src="{{asset('assets/js/pages/modals.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.colReorder.js')}}"></script>
{{--    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.js') }}"></script>--}}
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#idList').removeAttr('width').DataTable( {
                "oLanguage": {
                    "sProcessing":   "{{ \Lang::get('datatable.processing') }}",
                    "sLengthMenu":   "{{ \Lang::get('datatable.lengthMenu') }}",
                    "sZeroRecords":  "{{ \Lang::get('datatable.zeroRecords') }}",
                    "sInfo":         "{{ \Lang::get('datatable.info') }}",
                    "sInfoEmpty":    "{{ \Lang::get('datatable.infoEmpty') }}",
                    "sInfoFiltered": "{{ \Lang::get('datatable.infoFilter') }}",
                    "sSearch":       "{{ \Lang::get('datatable.search') }}",
                    "sEmptyTable":     "{{ \Lang::get('datatable.empty') }}",
                    "sLoadingRecords": "{{ \Lang::get('datatable.loadingRecords') }}",
                    "sInfoThousands":  ",",
                    "oPaginate": {
                        "sPrevious": "{{ \Lang::get('datatable.paginate.previous') }}",
                        "sNext":     "{{ \Lang::get('datatable.paginate.next') }}",
                    },
                    "oAria": {
                        "sSortAscending":  "{{ \Lang::get('datatable.aria.asc') }}",
                        "sSortDescending": "{{ \Lang::get('datatable.paginate.next') }}"
                    }
                },
                scrollCollapse: true,
                paging:         true,
                columnDefs: [
                    { width: 250, targets: 0 }
                ],
                fixedColumns: true
            } );
        } );
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#fundList').removeAttr('width').DataTable( {
                "oLanguage": {
                    "sProcessing":   "{{ \Lang::get('datatable.processing') }}",
                    "sLengthMenu":   "{{ \Lang::get('datatable.lengthMenu') }}",
                    "sZeroRecords":  "{{ \Lang::get('datatable.zeroRecords') }}",
                    "sInfo":         "{{ \Lang::get('datatable.info') }}",
                    "sInfoEmpty":    "{{ \Lang::get('datatable.infoEmpty') }}",
                    "sInfoFiltered": "{{ \Lang::get('datatable.infoFilter') }}",
                    "sSearch":       "{{ \Lang::get('datatable.search') }}",
                    "sEmptyTable":     "{{ \Lang::get('datatable.empty') }}",
                    "sLoadingRecords": "{{ \Lang::get('datatable.loadingRecords') }}",
                    "sInfoThousands":  ",",
                    "oPaginate": {
                        "sPrevious": "{{ \Lang::get('datatable.paginate.previous') }}",
                        "sNext":     "{{ \Lang::get('datatable.paginate.next') }}",
                    },
                    "oAria": {
                        "sSortAscending":  "{{ \Lang::get('datatable.aria.asc') }}",
                        "sSortDescending": "{{ \Lang::get('datatable.paginate.next') }}"
                    }
                },
                scrollCollapse: true,
                paging:         true,
                columnDefs: [
                    { width: 300, targets: 0 }
                ],
                fixedColumns: true
            } );
        } );
    </script>


    <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#withdrawList').removeAttr('width').DataTable( {
                "oLanguage": {
                    "sProcessing":   "{{ \Lang::get('datatable.processing') }}",
                    "sLengthMenu":   "{{ \Lang::get('datatable.lengthMenu') }}",
                    "sZeroRecords":  "{{ \Lang::get('datatable.zeroRecords') }}",
                    "sInfo":         "{{ \Lang::get('datatable.info') }}",
                    "sInfoEmpty":    "{{ \Lang::get('datatable.infoEmpty') }}",
                    "sInfoFiltered": "{{ \Lang::get('datatable.infoFilter') }}",
                    "sSearch":       "{{ \Lang::get('datatable.search') }}",
                    "sEmptyTable":     "{{ \Lang::get('datatable.empty') }}",
                    "sLoadingRecords": "{{ \Lang::get('datatable.loadingRecords') }}",
                    "sInfoThousands":  ",",
                    "oPaginate": {
                        "sPrevious": "{{ \Lang::get('datatable.paginate.previous') }}",
                        "sNext":     "{{ \Lang::get('datatable.paginate.next') }}",
                    },
                    "oAria": {
                        "sSortAscending":  "{{ \Lang::get('datatable.aria.asc') }}",
                        "sSortDescending": "{{ \Lang::get('datatable.paginate.next') }}"
                    }
                },
                scrollCollapse: true,
                paging:         true,
                columnDefs: [
                    { width: 250, targets: 0 }
                ],
                fixedColumns: true
            } );
        } );
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#advisoryList').removeAttr('width').DataTable( {
                "oLanguage": {
                    "sProcessing":   "{{ \Lang::get('datatable.processing') }}",
                    "sLengthMenu":   "{{ \Lang::get('datatable.lengthMenu') }}",
                    "sZeroRecords":  "{{ \Lang::get('datatable.zeroRecords') }}",
                    "sInfo":         "{{ \Lang::get('datatable.info') }}",
                    "sInfoEmpty":    "{{ \Lang::get('datatable.infoEmpty') }}",
                    "sInfoFiltered": "{{ \Lang::get('datatable.infoFilter') }}",
                    "sSearch":       "{{ \Lang::get('datatable.search') }}",
                    "sEmptyTable":     "{{ \Lang::get('datatable.empty') }}",
                    "sLoadingRecords": "{{ \Lang::get('datatable.loadingRecords') }}",
                    "sInfoThousands":  ",",
                    "oPaginate": {
                        "sPrevious": "{{ \Lang::get('datatable.paginate.previous') }}",
                        "sNext":     "{{ \Lang::get('datatable.paginate.next') }}",
                    },
                    "oAria": {
                        "sSortAscending":  "{{ \Lang::get('datatable.aria.asc') }}",
                        "sSortDescending": "{{ \Lang::get('datatable.paginate.next') }}"
                    }
                },
                scrollCollapse: true,
                paging:         true,
                columnDefs: [
                    { width: 250, targets: 0 }
                ],
                fixedColumns: true
            } );
        } );
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#approvalList').removeAttr('width').DataTable( {
                "oLanguage": {
                    "sProcessing":   "{{ \Lang::get('datatable.processing') }}",
                    "sLengthMenu":   "{{ \Lang::get('datatable.lengthMenu') }}",
                    "sZeroRecords":  "{{ \Lang::get('datatable.zeroRecords') }}",
                    "sInfo":         "{{ \Lang::get('datatable.info') }}",
                    "sInfoEmpty":    "{{ \Lang::get('datatable.infoEmpty') }}",
                    "sInfoFiltered": "{{ \Lang::get('datatable.infoFilter') }}",
                    "sSearch":       "{{ \Lang::get('datatable.search') }}",
                    "sEmptyTable":     "{{ \Lang::get('datatable.empty') }}",
                    "sLoadingRecords": "{{ \Lang::get('datatable.loadingRecords') }}",
                    "sInfoThousands":  ",",
                    "oPaginate": {
                        "sPrevious": "{{ \Lang::get('datatable.paginate.previous') }}",
                        "sNext":     "{{ \Lang::get('datatable.paginate.next') }}",
                    },
                    "oAria": {
                        "sSortAscending":  "{{ \Lang::get('datatable.aria.asc') }}",
                        "sSortDescending": "{{ \Lang::get('datatable.paginate.next') }}"
                    }
                },
                scrollCollapse: true,
                paging:         true,
                columnDefs: [
                    { width: 200, targets: 0 }
                ],
                fixedColumns: true
            } );
        } );
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#capxList').removeAttr('width').DataTable( {
                "oLanguage": {
                    "sProcessing":   "{{ \Lang::get('datatable.processing') }}",
                    "sLengthMenu":   "{{ \Lang::get('datatable.lengthMenu') }}",
                    "sZeroRecords":  "{{ \Lang::get('datatable.zeroRecords') }}",
                    "sInfo":         "{{ \Lang::get('datatable.info') }}",
                    "sInfoEmpty":    "{{ \Lang::get('datatable.infoEmpty') }}",
                    "sInfoFiltered": "{{ \Lang::get('datatable.infoFilter') }}",
                    "sSearch":       "{{ \Lang::get('datatable.search') }}",
                    "sEmptyTable":     "{{ \Lang::get('datatable.empty') }}",
                    "sLoadingRecords": "{{ \Lang::get('datatable.loadingRecords') }}",
                    "sInfoThousands":  ",",
                    "oPaginate": {
                        "sPrevious": "{{ \Lang::get('datatable.paginate.previous') }}",
                        "sNext":     "{{ \Lang::get('datatable.paginate.next') }}",
                    },
                    "oAria": {
                        "sSortAscending":  "{{ \Lang::get('datatable.aria.asc') }}",
                        "sSortDescending": "{{ \Lang::get('datatable.paginate.next') }}"
                    }
                },
                scrollCollapse: true,
                paging:         true,
                columnDefs: [
                    { width: 250, targets: 0 }
                ],
                fixedColumns: true
            } );
        } );
    </script>

@endsection