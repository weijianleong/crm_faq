@extends('front.app')

@section('title')
@lang('inbox.detail') | {{ config('app.name') }}
@stop

@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/datatables/css/colReorder.bootstrap.min.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/mail_box.css')}}"/>
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('inbox.detail')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                            <a href="{{ route('inbox.list', ['lang' => \App::getLocale()]) }}">
                                @lang('inbox.Title')
                           </a>
                       </li>
                        <li class="breadcrumb-item active">@lang('inbox.detail')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card m-t-35" style="border: none; border-left: 5px solid #2F61D5; background-color: #fff; box-shadow: 2px 4px 25px -8px #989898;">
                        <div class="card-body p-t-10">
                            <div class="m-t-15">
                                <a href="{{ route('inbox.list', ['lang' => \App::getLocale()]) }}"><i class="fa fa-reply" style="padding: 5px;"></i> @lang('inbox.back')</a>
                            </div>
                            <br><hr>
                            <div>
                                <div style="background-color: rgb(47,97,213,0.15); color: #2F61D5; padding: 8px; float: left; border-radius: 20px;">{{ $model->created_at }}</div>
                                <div style="padding: 10px; float: right; color: #787993;">@if($model->from == 'System')@lang('inbox.system') @else{{ $model->from }} @endif</div>
                            </div>
                            <div style="margin-top: 70px; color: #25265E; font-size: 20px;">{{ $model->subject }}</div>
                            <div class="m-t-20" style="color: rgb(120,121,147,0.8);">
                                <p>{{ $user->first_name }}: </p>
                                <p>{{ $model->content }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.colReorder.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/pages/mail_box.js')}}"></script>

@stop