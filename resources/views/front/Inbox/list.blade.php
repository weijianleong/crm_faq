@extends('front.app')

@section('title')
@lang('inbox.Title') | {{ config('app.name') }}
@stop

@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/animate/css/animate.min.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/datatables/css/colReorder.bootstrap.min.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}" />

@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('inbox.Title')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('inbox.Title')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row web-mail">

                <div class="col-lg-12">
                    <div class="card mail media_max_991 m-t-35">
                        <div class="card-body m-t-25 p-d-10">
                            <div class="tabs tabs-bordered tabs-icons">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item " id="primary2">
                                        <a href="#announcement" class="nav-link active " data-toggle="tab" aria-expanded="true"><i class="fa fa-info-circle"></i> @lang('sidebar.announcementTitle')
                                            @if($unRead['Announcement'] > 0)
                                            <i id="AnnouncementNotification" class="fa fa-circle" style="color: red; font-size: 8px;"></i>
                                            @endif
                                        </a>
                                    </li>
<!--                                     <li class="nav-item" id="social2">
                                        <a href="#cs" class="nav-link" data-toggle="tab" 
                                           aria-expanded="false"><i class="fa fa-group"></i> @lang('inbox.csTitle') 
                                            @if($unRead['CsNotification'] > 0)
                                                <span class="badge badge-pill badge-primary float-right calendar_badge menu_hide unreadAnnouncement" >{{ $unRead['CsNotification'] }}</span>
                                            @endif
                                       </a>

                                    </li> -->
                                    <li class="nav-item" id="promotions2">
                                        <a href="#system" class="nav-link" data-toggle="tab"
                                           aria-expanded="false"><i class="fa fa-envelope-square"></i> @lang('inbox.systemTitle') &nbsp;  
                                            @if($unRead['SystemNotification'] > 0)
                                                <span class="badge badge-pill badge-primary float-right calendar_badge menu_hide " >{{ $unRead['SystemNotification'] }}</span>
                                            @endif
                                       </a>
                                            
                                    </li>

                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <br>
                                    <div class="tab-pane table-responsive reset padding-all fade active show " id="announcement">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dataTable no-footer" style="text-align: center; width: 100%;" id="announcementList" role="grid">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>@lang('announcement.listDate')</th>
                                                    <th>@lang('announcement.listTitle')</th>
                                                    <th>@lang('announcement.listAction')</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
<!-- 
                                    <div class="tab-pane table-responsive reset padding-all fade" id="cs">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dataTable no-footer" style="text-align: center; width: 100%;" id="notification_CS" role="grid">
                                                <thead>
                                                <tr>
                                                    <th>@lang('announcement.listDate')</th>
                                                    <th>@lang('announcement.listTitle')</th>
                                                    <th>@lang('announcement.listAction')</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div> -->

                                    <div class="tab-pane table-responsive reset padding-all fade" id="system">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dataTable no-footer" style="text-align: center; width: 100%;" id="notification_SYSTEM" role="grid">
                                                <thead>
                                                <tr>
                                                    
                                                    <th>@lang('announcement.listDate')</th>
                                                    <th>@lang('announcement.listTitle')</th>
                                                    <th>@lang('announcement.listAction')</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- /.inner -->
    </div>
@include('front.include.modal')
@stop

@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/js/pages/modals.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.colReorder.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>

<script type="text/javascript">
    var tableId = "announcementList";
    var dataUrl = "{{ route('announcement.getList', ['lang' => \App::getLocale()]) }}";
    var columnIds = [
                    { "data": "refrence_id" },
                    { "data": "created_at" },
                    { "data": "title_{{ $lang }}" },
                    { "data": "action" },
                    ];
    var disableSearchIndex = [];
    $(document).ready( function () {
        callDatatables(tableId, dataUrl, columnIds, disableSearchIndex);
    });



    $( "#social2" ).one( "click", function() {
        var tableId = "notification_CS";
        var dataUrl = "{{ route('inbox.getCSlist', ['lang' => \App::getLocale()]) }}";
        var columnIds = [
                        { "data": "created_at" },
                        { "data": "subject_{{ $lang }}" },
                        { "data": "action" },
                        ];
        var disableSearchIndex = [2];
        $(document).ready( function () {
            callDatatables(tableId, dataUrl, columnIds, disableSearchIndex);
        });
    });

    $( "#promotions2" ).one( "click", function() {
        var tableId = "notification_SYSTEM";
        var dataUrl = "{{ route('inbox.getSYSTEMlist', ['lang' => \App::getLocale()]) }}";
        var columnIds = [
                        { "data": "created_at" },
                        { "data": "subject_{{ $lang }}" },
                        { "data": "action" },
                        ];
        var disableSearchIndex = [2];
        $(document).ready( function () {
            callDatatables(tableId, dataUrl, columnIds, disableSearchIndex);
        });
    });

    function getAnnouncementDetails(parentId) {
        var disable = loadingDisable();
        var modelContent = '';
        var ajaxUrl = "{{ route('announcement.read', ['id' =>'parentId', 'lang' => \App::getLocale()]) }}";
            ajaxUrl = ajaxUrl.replace('parentId', parentId);

        $.ajax({
            url     :   ajaxUrl,
            method  :   "get",
            headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
            success : function(response){
                if(response.status == 0){
                    if("{{ $lang }}" == "en"){
                        modelContent =  '<div style="font-size: 28px !important;">'+response.model.title_en+'</div>';
                        modelContent += '<div style="font-size: 15px !important;font-weight: 100;">'+response.model.created_at+'</div>';
                        modelContent += '<hr><br/>';
                        modelContent += '<div style="font-size: 15px !important;">'+response.model.content_en+'</div>';
                    }else{
                        modelContent =  '<div style="font-size: 28px !important;">'+response.model.title_chs+'</div>';
                        modelContent += '<div style="font-size: 15px !important;font-weight: 100;">'+response.model.created_at+'</div>';
                        modelContent += '<hr><br/>';
                        modelContent += '<div style="font-size: 15px !important;">'+response.model.content_chs+'</div>';
                    }
                    $('#modalContent').empty().append(modelContent);
                    $('#modalZoomIn').click();
                    $('#'+parentId).css('display', 'none');

                    if(response.newInsert){
                        var check = $('#unreadAnnouncement').text() - 1;
                        if(check > 0){
                            $('.unreadAnnouncement').text(check);
                        }else{
                            $('.unreadAnnouncement').css('display', 'none');
                        }

                        var check = $('#AnnouncementNotification').text() - 1;
                        if(check > 0){
                            $('#AnnouncementNotification').text(check);
                        }else{
                            $('#AnnouncementNotification').css('display', 'none');
                        }
                    }
                }else{
                    notiAlert(response.status, response.msg, '{{\App::getLocale()}}');
                }
                disable.out();
            }
        });
    }
    function getNotificationDetails(parentId) {
        
        var ajaxUrl = "{{ route('inbox.read', ['lang'=>\App::getLocale(),'id'=> 'parentId']) }}";
            ajaxUrl = ajaxUrl.replace('parentId', parentId);
        document.location.href=ajaxUrl;
    }




</script>

@stop