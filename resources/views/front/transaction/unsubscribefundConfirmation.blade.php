@extends('front.app')

@section('title')
  @lang('misc.modal.utitletnc')  | {{ config('app.name') }}
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('misc.modal.utitletnc')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('misc.modal.utitletnc')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="card m-t-35">
                <div class="card-header bg-white">@lang('misc.modal.utitletnc')</div>
                <div class="card-body p-t-10 m-t-25">
                    <div style="text-align: center;">
                        <h1 class="text-uppercase" style="color: red;">@lang('misc.modal.usure')</h1>
                    </div>
                    <div class="col-lg-12 m-t-25">
                        <p class="card-text m-t-20 text-justify">@lang('misc.modal.confirmcontent1')</p>
                        <p class="card-text m-t-20 text-justify">A: @lang('misc.modal.confirmcontent2')</p>
                        <p class="card-text m-t-20 text-justify">B: @lang('misc.modal.confirmcontent3')</p>
                        <p class="card-text m-t-20 text-justify">C: @lang('misc.modal.confirmcontent4')</p>
                        <p class="card-text m-t-20 text-justify">D: @lang('misc.modal.confirmcontent5')</p>
                    </div>
                    <div class="card-body p-t-10 m-t-25" style="text-align: center;">
                        <a href="{{ route('transaction.unsubscribefund', ['lang' => \App::getLocale(), 'id' => $id, 'step' => 3]) }}" class="btn btn-primary glow_button">@lang('common.confirm')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop