@extends('front.app')

@section('title')
@lang('merge.sidebar') | {{ config('app.name') }}
@stop

@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/datatables/css/colReorder.bootstrap.min.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}" />
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('merge.sidebar')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('merge.sidebar')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            <i class="fa fa-table"></i> @lang('merge.sidebar')
                        </div>
                        <div class="card-body p-t-10">
                            @if($mergeAccountDisplay)
                            <a class="btn btn-warning glow_button" style="margin-top: 10px;" href="{{ route('transaction.mergeFund', ['lang' => \App::getLocale()]) }}"><i class="fa fa-compress"></i> @lang('merge.addrequest')</a>
                            @endif
                            <div class=" m-t-25">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTable no-footer" style="text-align: center; width: 100%;" id="getMergeList" role="grid">
                                        <thead>
                                        <tr>
                                            <th>@lang('merge.mergefrom')</th>
                                            <th>@lang('merge.mergeto')</th>
                                            <th>@lang('merge.status')</th>
                                            <th>@lang('merge.remark')</th>
                                            <th>@lang('merge.createdate')</th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.colReorder.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script type="text/javascript">
    var tableId = "getMergeList";
    var dataUrl = "{{ route('getmergelist', ['lang' => \App::getLocale()]) }}";
    var columnIds = [
                    { "data": "merge_from" },
                    { "data": "merge_to" },
                    { "data": "status" },
                    { "data": "remark" },
                    { "data": "created_at" },
                    { "data": "edit" },
                    { "data": "cancel" },
                    ];
    var disableSearchIndex = [0,1,2,3,4];
    $(document).ready( function () {
        callDatatables(tableId, dataUrl, columnIds, disableSearchIndex);
    });

    function cancelMerge(id) {
        swal({
                title: "@lang('merge.confirmtext')",
                text: '',
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: '#00c0ef',
                cancelButtonColor: '#ff8080',
                confirmButtonText: '<i class="fa fa-check"></i> '+"@lang('common.confirm')"+'',
                cancelButtonText: '<i class="fa fa-times"></i> '+"@lang('common.cancel')"+'',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger'
            }).then(function () {
                confirmcancelmerge(id);
            }, function (dismiss) {
                if(dismiss==="cancel"){
                }
            });
    }

    function confirmcancelmerge(id) {
        var disable = loadingDisable();
        var dataUrl = "{{ route('transaction.cancelMerge', ['lang' => \App::getLocale()]) }}";
        $.ajax({
            url     : dataUrl,
            method  : 'post',
            data    : { id  : id },
            headers : { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success : function(response){
                notiAlert(response.status, response.msg, '{{\App::getLocale()}}');
                window.location.reload();
                disable.out();
            }
        });
    }
</script>
@stop