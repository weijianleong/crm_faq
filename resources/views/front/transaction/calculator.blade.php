<?php
    
    use App\Repositories\SharesRepository;
    use App\Repositories\MemberRepository;
    use Carbon\Carbon;
    use Illuminate\Support\Str;
    $sharesRepo = new SharesRepository;
    $memberRepo = new MemberRepository;
    $announcementModel = new \App\Models\Announcement;
    $user = \Sentinel::getUser();
    $member = $memberRepo->findByUsername(trim($user->username));
    $member->load('wallet');
    $member->load('detail');
    //$member->load('shares');
    $member->load('package');
    
    $datas = \Session::get('data');
    $bookASL = \Session::get('bookASL');
    $bookATP = \Session::get('bookATP');
    $bookBSL = \Session::get('bookBSL');
    $bookBTP = \Session::get('bookBTP');
    //$n13 = \Session::get('n13');
    //$n5 = \Session::get('n5');
    

?>

@extends('front.app')

@section('title')
@lang('transfer.mt5.calculator') | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
  <li class="active">@lang('breadcrumbs.statementLink8')</li>
</ul>
@stop

@section('content')
<main>
  @include('front.include.sidebar')
  <div class="main-container">
    @include('front.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section>
        <div class="page-header">
          <h1><i class="md md-swap-vert"></i> @lang('transfer.mt5.calculator')</h1>
        </div>
 <div class="table-responsive">
                <table class="table table-hover table-striped">
                  <tr>
                    <td></td>
                    <td>@lang('common.cal.amount')</td>
                    <td class='middle'>$1,000</td>
                    <td class='middle'>$2,000</td>
                    <td class='middle'>$3,000</td>
                    <td class='middle'>$4,000</td>
                    <td class='middle'>$5,000</td>
                    <td class='middle'>$6,000</td>
                    <td class='middle'>$7,000</td>
                    <td class='middle'>$8,000</td>
                    <td class='middle'>$9,000</td>
                    <td class='middle'>$10,000</td>
                  </tr>

                  <tr>
                    <td class="theme-text">@lang('common.cal.lot')</td>
                    <td>@lang('transfer.a.wallet')</td>
                    <td class='middle'>0.75</td>
                    <td class='middle'>1.50</td>
                    <td class='middle'>2.25</td>
                    <td class='middle'>3.00</td>
                    <td class='middle'>3.75</td>
                    <td class='middle'>4.50</td>
                    <td class='middle'>5.25</td>
                    <td class='middle'>6.00</td>
                    <td class='middle'>6.75</td>
                    <td class='middle'>7.50</td>
                  </tr>

                    <tr>
                    <td class="theme-text"></td>
                    <td>@lang('transfer.b.wallet')</td>
                    <td class='middle'>0.92</td>
                    <td class='middle'>1.83</td>
                    <td class='middle'>2.75</td>
                    <td class='middle'>3.66</td>
                    <td class='middle'>4.58</td>
                    <td class='middle'>5.49</td>
                    <td class='middle'>6.41</td>
                    <td class='middle'>7.32</td>
                    <td class='middle'>8.24</td>
                    <td class='middle'>9.20</td>
                  </tr>

                </table>
              </div>
        <div class="row">


          <div class="col-md-12">
            <div class="well white">
              <form id="transferForm" class="action-form" role="form" data-url="{{ route('transaction.postCalculator', ['lang' => \App::getLocale()]) }}" data-parsley-validate="" onsubmit="return false;" http-type="post">
                <fieldset>
 <div class="form-group">
                <label class="control-label" for="inputPair">@lang('common.cal.type')</label>

                 <select name="currencypair" class="form-control" required="">
                <option value="">({{\Lang::get('common.select2')}})</option>
                <option value="XAUUSD" <?php if($datas['currencypair'] == 'XAUUSD') echo "SELECTED" ?>>XAUUSD</option>
                <option value="AUDUSD" <?php if($datas['currencypair'] == 'AUDUSD') echo "SELECTED" ?>>AUDUSD</option>
                <option value="NZDUSD" <?php if($datas['currencypair'] == 'NZDUSD') echo "SELECTED" ?>>NZDUSD</option>
                <option value="EURUSD" <?php if($datas['currencypair'] == 'EURUSD') echo "SELECTED" ?>>EURUSD</option>
                <option value="GBPUSD" <?php if($datas['currencypair'] == 'GBPUSD') echo "SELECTED" ?>>GBPUSD</option>

<option value="USDJPY" <?php if($datas['currencypair'] == 'USDJPY') echo "SELECTED" ?>>USDJPY</option>
<option value="GBPJPY" <?php if($datas['currencypair'] == 'GBPJPY') echo "SELECTED" ?>>GBPJPY</option>
<option value="EURJPY" <?php if($datas['currencypair'] == 'EURJPY') echo "SELECTED" ?>>EURJPY</option>
<option value="AUDJPY" <?php if($datas['currencypair'] == 'AUDJPY') echo "SELECTED" ?>>AUDJPY</option>
<option value="NZDJPY" <?php if($datas['currencypair'] == 'NZDJPY') echo "SELECTED" ?>>NZDJPY</option>
<option value="CHFJPY" <?php if($datas['currencypair'] == 'CHFJPY') echo "SELECTED" ?>>CHFJPY</option>
<option value="CADJPY" <?php if($datas['currencypair'] == 'CADJPY') echo "SELECTED" ?>>CADJPY</option>
                </select>

              </div>
<div class="form-group">
                <label class="control-label">@lang('common.cal.lot')</label>
                    <div class="input-group">
                       <select name="lotsize" class="form-control") required="">
                            <option value="">({{\Lang::get('common.select2')}})</option>
                            <option value="1" <?php if($datas['lotsize'] == 1) echo "SELECTED" ?>>@lang('transfer.a.wallet'):0.75; @lang('transfer.b.wallet'):0.92</option>
                            <option value="2" <?php if($datas['lotsize'] == 2) echo "SELECTED" ?>>@lang('transfer.a.wallet'):1.50; @lang('transfer.b.wallet'):1.83</option>
                            <option value="3" <?php if($datas['lotsize'] == 3) echo "SELECTED" ?>>@lang('transfer.a.wallet'):2.25; @lang('transfer.b.wallet'):2.75</option>
                            <option value="4" <?php if($datas['lotsize'] == 4) echo "SELECTED" ?>>@lang('transfer.a.wallet'):3.00; @lang('transfer.b.wallet'):3.66</option>
                            <option value="5" <?php if($datas['lotsize'] == 5) echo "SELECTED" ?>>@lang('transfer.a.wallet'):3.75; @lang('transfer.b.wallet'):4.58</option>
                            <option value="6" <?php if($datas['lotsize'] == 6) echo "SELECTED" ?>>@lang('transfer.a.wallet'):4.50; @lang('transfer.b.wallet'):5.49</option>
                            <option value="7" <?php if($datas['lotsize'] == 7) echo "SELECTED" ?>>@lang('transfer.a.wallet'):5.25; @lang('transfer.b.wallet'):6.41</option>
                            <option value="8" <?php if($datas['lotsize'] == 8) echo "SELECTED" ?>>@lang('transfer.a.wallet'):6.00; @lang('transfer.b.wallet'):7.32</option>
                            <option value="9" <?php if($datas['lotsize'] == 9) echo "SELECTED" ?>>@lang('transfer.a.wallet'):6.75; @lang('transfer.b.wallet'):8.24</option>
                            <option value="10" <?php if($datas['lotsize'] == 10) echo "SELECTED" ?>>@lang('transfer.a.wallet'):7.50; @lang('transfer.b.wallet'):9.20</option>
                        </select>
                    </div>

              </div>
              <div class="col-md-6">
                <div class="well">
                <div class="table-header middle">
                    <h5>@lang('transfer.a.wallet')</h5>
                </div>
                  <div class="form-group">
                    <label class="control-label">@lang('common.cal.price')</label>
                    <div class="input-group">
<input type="float" class="form-control" name="priceA" required="" value="{{ $datas['priceA'] }}">
                    </div>
                    <span class="help-block"></span>
                  </div>
                <div class="form-group">
                    <label class="control-label">@lang('common.cal.amount')</label>
                    <div class="input-group">
                      <input type="float" class="form-control" name="amountA" required="" value="{{ $datas['amountA'] }}">
                    </div>
                    <span class="help-block"></span>
                  </div>


<div class="form-group">
                    <label class="control-label">@lang('common.cal.swap')</label>
                    <div class="input-group">
                      <input type="float" class="form-control" name="swapA" value="{{ $datas['swapA'] }}">
                    </div>
                    <span class="help-block"></span>
                     </div>
                 <div class="form-group">
                    <label class="control-label"></label>
                    <div class="input-group">
                     <select name="decision" class="form-control">
                        <option value="BUY" <?php if($datas['decision'] == 'BUY') echo "SELECTED" ?>>@lang('common.cal.buy')</option>
                        <option value="SELL" <?php if($datas['decision'] == 'SELL') echo "SELECTED" ?>>@lang('common.cal.sell')</option>

                    </select>
                    </div>
                    <span class="help-block"></span>
                     </div>
                    <div class="form-group">
                        <label class="control-label black">@lang('common.cal.sl') : {{$bookASL}}</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label black">@lang('common.cal.tp') : {{$bookATP}}</label>
                    </div>

                </div>
            </div>

            <div class="col-md-6">
                <div class="well">
                <div class="table-header middle">
                  <h5>@lang('transfer.b.wallet')</h5>
                </div>
                   <div class="form-group">
                    <label class="control-label">@lang('common.cal.price')</label>
                    <div class="input-group">
                      <input type="float" class="form-control" name="priceB" required="" value="{{ $datas['priceB'] }}">
                    </div>
                    <span class="help-block"></span>
                  </div>
                <div class="form-group">
                    <label class="control-label">@lang('common.cal.amount')</label>
                    <div class="input-group">
                      <input type="float" class="form-control" name="amountB" required="" value="{{ $datas['amountB'] }}">
                    </div>
                    <span class="help-block"></span>
                  </div>
                    <div class="form-group">
                    <label class="control-label">@lang('common.cal.swap')</label>
                    <div class="input-group">
                      <input type="float" class="form-control" name="swapB" value="{{ $datas['swapB'] }}">
                    </div>
                    <span class="help-block"></span>
                  </div>
                    <div class="form-group">
                    <label class="control-label"></label>
                    <div class="input-group">
                     <select name="decision2" class="form-control">
                        <option value="BUY" <?php if($datas['decision'] == 'SELL') echo "SELECTED" ?>>@lang('common.cal.buy')</option>
                        <option value="SELL" <?php if($datas['decision'] == 'BUY') echo "SELECTED" ?>>@lang('common.cal.sell')</option>

                    </select>
                    </div>
                <div class="form-group">
                        <label class="control-label black height='100'">@lang('common.cal.sl') : {{$bookBSL}}  </label>
                    </div>
                    <div class="form-group">
                        <label class="control-label black height='100'">@lang('common.cal.tp') : {{$bookBTP}}</label>
                    </div>
                </div>
<div class="form-group">
                    <button type="submit" class="btn btn-primary">
                      <span class="btn-preloader">
                        <i class="md md-cached md-spin"></i>
                      </span>
                      <span>@lang('common.submit')</span>
                    </button>
                    <button type="reset" class="btn btn-default">@lang('common.cancel')</button>
                  </div>
                </div>
            </div>



                </fieldset>
              </form>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</main>

@stop
