<?php
    use App\Models\Package;
    use App\Repositories\MemberRepository;
    $packages = Package::where('status', '!=', 0)->get();
    
    $memberRepo = new MemberRepository;
    $member = $memberRepo->findByUsername(trim($user->username));
    $member->load('wallet');
    $member->load('detail');
    //$member->load('shares');
    $member->load('package');
    
    $status =  Input::get('status');
    ?>

@extends('front.app')

@section('title')
  @lang('success.mttitle') | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
    <li><a href="{{ route('transaction.transfermt5', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.statementLink9')</a></li>
    <li class="active">@lang('transfer.mt5.title2')</li>
  </ul>
@stop

@section('content')
  <main>
    @include('front.include.sidebar')
    <div class="main-container">
      @include('front.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section>
          <div class="page-header">
            <h1><i class="md md-done-all"></i>

                    @lang('transfer.mt5.title2')

            </h1>
          </div>

          <div class="row m-b-40">
            <div class="col-md-8">
              <div class="well white text-center">
                <h1 class="theme-text text-uppercase w300">
                     @if($status == 'A')
                    @lang('success.tradetitle2')
                @else
                    @lang('success.tradetitle')
                @endif
                </h1>
            <h3>@lang('success.processtext2')</h3>
                <h3>@lang('success.processtext3')</h3>
                <a href="{{ route('misc.mt5statement', ['lang' => \App::getLocale()]) }}" class="btn btn-primary btn-block m-t-50">
                  <i class="md md-navigate-before"></i> @lang('success.tradeback')
                </a>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>
@stop
