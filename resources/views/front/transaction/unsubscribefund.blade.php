<?php
    use App\Repositories\MemberRepository;
    $memberRepo = new MemberRepository;
    $member = $memberRepo->findByUsername(trim($user->username));

    $account = \DB::table('Member_Account_Fund')->where('member_id', '=', $member->id)->where('system_manage', '=', 'S')->where('bookA', $id)->first();

    $temp = [];
    if(empty($account)){
        $temp['action'] = 'unsubscribeFund';
        $temp['bookA'] = $id;
        \DB::table('Error_Log')->insert(['member_id' => $member->id, 'msg' => json_encode($temp), 'created_at' => date('Y-m-d H:i:s')]);
        header('Location: '.route('home', ['lang' => \App::getLocale()]));exit();
    }

    $balanceA = $memberRepo->AccountAPI($account->bookA);

    $partners = \DB::connection('mysql3')->table('user')->where('UserType', '=', 2)->where('Status', '=', 1)->where('UserId', '<>', 84)->where('UserId', '<>', 86)->where('UserId', '<>', 85)->where('UserId', '<>', 88)->where('UserId', '<>', 153)->where('UserId', '<>', 89)->get();

    if($step == 1) $titleLang = \Lang::get('misc.modal.utitle');
    else $titleLang = \Lang::get('misc.modal.utitleconfirm');
    
?>

@extends('front.app')

@section('title')
    {{ $titleLang }} | {{ config('app.name') }}
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i>{{ $titleLang }}</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">{{ $titleLang }}</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="card m-t-35">
                <div class="card-header bg-white">{{ $titleLang }}</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12 m-t-25">
                            <fieldset>
                                <div class="form-group row">
                                    <div class="col-lg-3 text-lg-right">
                                        <label for="bookA" class="col-form-label">@lang('transfer.mt5.wallet')</label>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text border-right-0 rounded-left">
                                                <i class="fa fa-book"></i>
                                            </span>
                                            <input type="text" name="bookADisplay" id="bookADisplay" value="@lang('transfer.a.wallet'): {{$account->bookA}} @lang('transfer.balance'): ${{$balanceA->balance}} ; @lang('transfer.b.wallet'): {{$account->bookB}} @lang('transfer.balance'): ${{$account->balanceB}}" class="form-control" disabled="" style="cursor: not-allowed;">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-3 text-lg-right">
                                        <label for="oldfundcompany" class="col-form-label">@lang('misc.fundcompany')</label>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text border-right-0 rounded-left">
                                                <i class="fa fa-clone"></i>
                                            </span>
                                            @if (count($partners) > 0)
                                            @foreach ($partners as $partner)
                                            @if($account->fundcompany == $partner->UserId)
                                                <input type="text" value="@if (\App::getLocale() == 'chs') {{ $partner->ChineseName }} @else {{ $partner->FirstName }} {{ $partner->LastName }} @endif" class="form-control" disabled="" style="cursor: not-allowed;">
                                            @endif
                                            @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-9 ml-auto">
                                        @if($step == 3)
                                        <button class="btn btn-primary layout_btn_prevent" onclick="unSubscribeFundConfirm('{{$account->bookA}}','{{$step}}')">@lang('common.submit')</button>
                                        @else
                                        <button class="btn btn-primary layout_btn_prevent" onclick="unSubscribeFund('{{$account->bookA}}','{{$step}}')">@lang('common.submit')</button>
                                        @endif
                                        <a class="btn btn-secondary layout_btn_prevent" href="{{ route('misc.fundstatement', ['lang' => \App::getLocale()]) }}">@lang('common.cancel')</a>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer_scripts')
<script type="text/javascript">
    function unSubscribeFund(id, step){
        var url = "{{ route('transaction.postUnsubscribeMT5Fund', ['lang' => \App::getLocale()]) }}";
        var buttonDisplay = '{"Format":['+'{"confirmButtonText":'+'"@lang('common.confirm')"'+',"cancelButtonText":'+'"@lang('common.cancel')"'+',"setTittle":'+'"@lang('settings.secret')"'+',"error":'+'"@lang('error.securityPasswordError')"'+'}]}';
        var obj = {id: id, step: step};
        var final = { Data : obj};
        var data = JSON.stringify(final);

        obj = JSON.parse(buttonDisplay);

        swal({
            title: obj.Format[0].setTittle,
            input: 'password',
            showCancelButton: true,
            confirmButtonText: obj.Format[0].confirmButtonText,
            confirmButtonColor: '#00c0ef',
            cancelButtonText: obj.Format[0].cancelButtonText,
            cancelButtonColor: '#ff8080',
            width: 600,
            showLoaderOnConfirm: true,
            preConfirm: function (password) {
                return new Promise(function (resolve, reject) {
                    $.ajax({
                            url : url,
                            data: {
                                    SPassword  : password,
                                    Data : data,
                            },
                            type: 'POST',
                            headers:
                            {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success : function(response){
                                if (response.SPassword == '1') {
                                    var error = obj.Format[0].error;
                                    reject(error);
                                } else {
                                    resolve(response);
                                }
                            }
                    });
                });
            },
            allowOutsideClick: false
        }).then(function (response) {
            if(response.returnUrl) document.location.href=response.returnUrl;
        }).done();
    }

    function unSubscribeFundConfirm(id, step){
        var url = "{{ route('transaction.postUnsubscribeMT5Fund', ['lang' => \App::getLocale()]) }}";
        var buttonDisplay = '{"Format":['+'{"confirmButtonText":'+'"@lang('common.confirm')"'+',"cancelButtonText":'+'"@lang('common.cancel')"'+',"setTittle":'+'"@lang('settings.secret')"'+',"error":'+'"@lang('error.securityPasswordError')"'+'}]}';
        var obj = {id: id, step: step};
        var final = { Data : obj};
        var data = JSON.stringify(final);
        swalAlert(url,buttonDisplay,data);
        return false;
    }
</script>
@stop