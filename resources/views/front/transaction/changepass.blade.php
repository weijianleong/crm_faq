@extends('front.app')

@section('title')
  @lang('misc.cpTitle')  | {{ config('app.name') }}
@stop

@section('header_styles')
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/swiper/css/swiper.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/widgets.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/sweetalert/css/sweetalert2.min.css')}}"/>
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('misc.cpTitle')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('misc.cpTitle')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>

    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    
                        
                            <div class="row justify-content-md-center">

                                <div class="col-lg-4 m-t-35">
                                    <div class="card card-body m-t-35">
                                        <h4 class="card-title m-t-20">@lang('misc.cpTitle')</h4>
                                        <p class="card-text m-t-15 text-justify"></p>
                                        <a href="#" class="btn btn-primary" onclick = "UpdatePassword()">@lang('common.update')</a>
                                    </div>
                                </div>
                            </div>
                        
                        

                    
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer_scripts')

    <script type="text/javascript">
        function UpdatePassword(){
            var url = "{{ route('transaction.postChangePass', ['lang' => \App::getLocale()]) }}";
            var buttonDisplay = '{"Format":['+'{"confirmButtonText":'+'"@lang('common.confirm')"'+',"cancelButtonText":'+'"@lang('common.cancel')"'+',"setTittle":'+'"@lang('settings.secret')"'+',"error":'+'"@lang('error.securityPasswordError')"'+'}]}';
            var obj = {Password1: "", rePassword1:""};
            var final = { Data : obj};
            var data = JSON.stringify(final);
            swalAlert(url,buttonDisplay,data);
            return false;

        }


    </script>
@stop
