<?php
    use App\Repositories\MemberRepository;
    $memberRepo = new MemberRepository;
    $member = $memberRepo->findByUsername(trim($user->username));

    $account = \DB::table('Member_Account_Fund')->where('member_id', '=', $member->id)->where('status', '=', 0)->where('system_manage', '=', 'N')->where('bookA', $bookA)->first();

    $balanceA = $memberRepo->AccountAPI($account->bookA);

    $packages = \DB::table('Package')->where('status', '=', 1)->get();
?>

@extends('front.app')

@section('title')
    @lang('transfer.mt5.title') | {{ config('app.name') }}
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i>@lang('transfer.mt5.title')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('transfer.mt5.title')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="card m-t-35">
                <div class="card-header bg-white">@lang('transfer.mt5.title'){{$account->bookA}}</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12 m-t-25">
                            <fieldset>
                                <div class="form-group row">
                                    <div class="col-lg-3 text-lg-right">
                                        <label for="bookA" class="col-form-label">@lang('transfer.mt5.wallet')</label>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text border-right-0 rounded-left">
                                                <i class="fa fa-book"></i>
                                            </span>
                                            <input type="text" name="bookADisplay" id="bookADisplay" value="@lang('transfer.a.wallet'): {{$account->bookA}} @lang('transfer.balance'): ${{$balanceA->balance}} ; @lang('transfer.b.wallet'): {{$account->bookB}} @lang('transfer.balance'): ${{$account->balanceB}}" class="form-control" disabled="" style="cursor: not-allowed;">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-3 text-lg-right">
                                        <label for="gender3" class="col-form-label">@lang('transfer.amount')</label>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text border-right-0 rounded-left">
                                                <i class="fa fa-clone"></i>
                                            </span>
                                            <select class="form-control" name="package" id="package">
                                                @if (count($packages) > 0)
                                                @foreach ($packages as $package)
                                                    <option value="{{ $package->id }}">${{ $package->package_amount }} (@lang('transfer.a.wallet'): ${{ $package->cash_amount }} ; @lang('transfer.b.wallet'): ${{ $package->b_amount }} )</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-9 ml-auto">
                                        <button class="btn btn-primary layout_btn_prevent" onclick="depositFund('{{$account->id}}')">@lang('common.submit')</button>
                                        <a class="btn btn-secondary layout_btn_prevent" href="{{ route('misc.fundstatement', ['lang' => \App::getLocale()]) }}">@lang('common.cancel')</a>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer_scripts')
<script type="text/javascript">
    function depositFund(bookA){
        var package = $('#package').val();
        
        var url = "{{ route('transaction.postTransferMT5Fund', ['lang' => \App::getLocale()]) }}";
        var buttonDisplay = '{"Format":['+'{"confirmButtonText":'+'"@lang('common.confirm')"'+',"cancelButtonText":'+'"@lang('common.cancel')"'+',"setTittle":'+'"@lang('settings.secret')"'+',"error":'+'"@lang('error.securityPasswordError')"'+'}]}';
        var obj = {bookA: bookA, package: package};
        var final = { Data : obj};
        var data = JSON.stringify(final);
        swalAlert(url,buttonDisplay,data);
        return false;
    }
</script>
@stop