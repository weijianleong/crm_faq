@extends('front.app')

@section('title')
    @lang('merge.editrequest') | {{ config('app.name') }}
@stop

@section('header_styles')
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/swiper/css/swiper.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/widgets.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/sweetalert/css/sweetalert2.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/animate/css/animate.min.css')}}" />
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('merge.editrequest')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('merge.editrequest')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            <div class="row justify-content-md-center">
                                <div class="col-sm-6 col-lg-3 media_max_991">
                                    <div id="mergeAccountAll" class="icon_align" style="border-left: 5px solid #ff0000; box-shadow: 2px 4px 25px -8px #ff0000; min-height: 100px; padding: 15px; display: flex; align-items: center; justify-content: center;">
                                        @lang('merge.pleaseselect')@lang('merge.mergeaccount') (@lang('merge.from'))
                                    </div>
                                </div>

                                <div class="fa-icon d-none d-sm-block" style="padding-top: 10px;"><i class="fa fa-arrow-right fa-5x"></i> </div>
                                <div class="fa-icon d-md-none" style="padding-left: 35%;padding-top: 10%;"><i class="fa fa-arrow-down fa-5x"></i> </div>

                                <div class="col-sm-6 col-lg-3 media_max_991">
                                    <div id="mergeAccountMain" class="icon_align" style="border-left: 5px solid #ff751a; box-shadow: 2px 4px 25px -8px #ff751a; min-height: 100px; padding: 15px; display: flex; align-items: center; justify-content: center;">
                                        @lang('merge.pleaseselect')@lang('merge.mergeaccount') (@lang('merge.to'))
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="card-body m-t-35">
                            <div class="form-group row">
                                <div class="col-lg-9 ml-auto">@lang('merge.pleaseselect')@lang('merge.mergeaccount'):</div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-3 text-lg-right"></div>
                                <div class="col-lg-6">
                                    <span style="float: left;">@lang('merge.from'):</span>
                                    <span style="float: right;">:@lang('merge.to')</span>
                                </div>
                            </div>
                            <fieldset>
                                @foreach($model as $key => $item)
                                <div class="form-group row">
                                    <div class="col-lg-3 text-lg-right">
                                        <label for="bookA" class="col-form-label"></label>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text border-right-0 rounded-left">
                                                <input id="account{{ $key }}" name="account" value="{{ $key }}" type="checkbox">
                                            </span>
                                            <input type="text" value="{{ $item }}" class="form-control" disabled="" style="cursor: not-allowed;">
                                            <span class="input-group-text border-left-0 rounded-right account-to" id="{{ $key }}">&nbsp; -&nbsp;</span>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                <div class="form-group row">
                                    <div class="col-lg-9 ml-auto">
                                        <button class="btn btn-primary layout_btn_prevent" onclick="confirmEditMergeFund('{{$id}}')">@lang('common.submit')</button>
                                        <a class="btn btn-secondary layout_btn_prevent" href="{{ route('mergelist', ['lang' => \App::getLocale()]) }}">@lang('common.cancel')</a>
                                    </div>
                                </div>
                            </fieldset>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('front.include.modal')
@stop

@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/js/pages/modals.js')}}"></script>
<script>
    $(document).ready( function () {
        var selectedMerge = '{{ $selectedMerge }}';
        var selectedMergeTo = '{{ $selectedMergeTo }}';
        var selectedMergeAry = selectedMerge.split(",");
        var initialAccount = [];
        $.each(selectedMergeAry, function(index, item) {
            initialAccount.push(item);
            $('#mergeAccountAll').text(initialAccount.join(', '));
            $('#mergeAccountMain').text(selectedMergeTo);
            $('input[id = account'+item+']').prop("checked", true);
            if(item == selectedMergeTo) $('#'+item).empty().append('<input id="'+item+'" name="accountto" value="'+item+'" type="checkbox" onclick="checkedAccountTo('+item+')" checked>');
            else $('#'+item).empty().append('<input id="'+item+'" name="accountto" value="'+item+'" type="checkbox" onclick="checkedAccountTo('+item+')">');
        });

        $('input[name = account]').on('click', function(event) {
            var selectedAccount = [];
            if($('#mergeAccountMain').text() == $(this).val()){
                $('#mergeAccountMain').text("{{\Lang::get('merge.pleaseselect').''.\Lang::get('merge.mergeaccount').' ('.\Lang::get('merge.to').')'}}");
            }

            $('#'+$(this).val()).empty().append('&nbsp; -&nbsp;');
            if(this.checked){
                $('#'+$(this).val()).empty().append('<input id="'+$(this).val()+'" name="accountto" value="'+$(this).val()+'" type="checkbox" onclick="checkedAccountTo('+$(this).val()+')">');
            }

            $('input[name = account]').each(function() {
                if(this.checked){
                    selectedAccount.push($(this).val());
                    $('#mergeAccountAll').text(selectedAccount.join(', '));
                }
            });

            if($('input[name = account]:checked').length == 0) {
                $('#mergeAccountAll').text("{{\Lang::get('merge.pleaseselect').''.\Lang::get('merge.mergeaccount').' ('.\Lang::get('merge.from').')'}}");
                $('#mergeAccountMain').text("{{\Lang::get('merge.pleaseselect').''.\Lang::get('merge.mergeaccount').' ('.\Lang::get('merge.to').')'}}");
            }
        });
    });

    function checkedAccountTo(account){
        var selectedAccountTo = '';
        if($('input[name = accountto]:checked').length > 0) {
            $('input[name = accountto]:checked').each(function() {
                if($(this).val() != account) {
                    $('input[id = '+$(this).val()+']:checked').prop("checked", false);
                }
            });
            $('#mergeAccountMain').text(account);
        }else{
            $('#mergeAccountMain').text("{{\Lang::get('merge.pleaseselect').''.\Lang::get('merge.mergeaccount').' ('.\Lang::get('merge.to').')'}}");
        }
    }

    function confirmEditMergeFund(id) {
        var modelContent = '';

        modelContent = '<div style="text-align: center; font-size: 30px; padding: 15px;">@lang("misc.modal.merge")<br/>';
        modelContent += '<div style="text-align: left; font-size: 15px; padding: 15px;">@lang("misc.modal.merge1")<br/>';
        modelContent += '<div style="text-align: left; font-size: 15px;">@lang("misc.modal.merge2")<br/>';
        modelContent += '<div style="text-align: left; font-size: 15px;">@lang("misc.modal.merge3")<br/>';
        modelContent += '<div style="text-align: center; padding: 15px;"><button type="button" class="btn btn-primary glow_button" data-dismiss="modal" onclick="editMergeFund('+id+')">@lang("merge.understood")</button></div>';
        modelContent += '</div>';
        $('#modalContent').empty().append(modelContent);
        $('#modalZoomIn').click();
    }

    function editMergeFund(id){
        if($('input[name = account]:checked').length > 1) {
            var account = [];
            $('input[name = account]:checked').each(function() {
                account.push($(this).val());
            });

            var accountString = account.join();
            var accountto = $('input[name = accountto]:checked').val();

            var url = "{{ route('transaction.postMergeFund', ['lang' => \App::getLocale()]) }}";
            var buttonDisplay = '{"Format":['+'{"confirmButtonText":'+'"@lang('common.confirm')"'+',"cancelButtonText":'+'"@lang('common.cancel')"'+',"setTittle":'+'"@lang('settings.secret')"'+',"error":'+'"@lang('error.securityPasswordError')"'+'}]}';
            var obj = {account: accountString, accountto: accountto, id: id};
            var final = { Data : obj};
            var data = JSON.stringify(final);
            swalAlert(url,buttonDisplay,data);
            return false;
        }else{
            notiAlert(1, "{{\Lang::get('transfer.mergetitle')}}", '{{\App::getLocale()}}');
        }
    }
</script>
@stop