<?php
    
    use App\Repositories\SharesRepository;
    use App\Repositories\MemberRepository;
    use Carbon\Carbon;
    use Illuminate\Support\Str;
    $sharesRepo = new SharesRepository;
    $memberRepo = new MemberRepository;
    $announcementModel = new \App\Models\Announcement;
    
    $member = $memberRepo->findByUsername(trim($user->username));
    $member->load('wallet');
    $member->load('detail');


?>

@extends('front.app')

@section('title')
@lang('transfer.sr.title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
  <li class="active">@lang('breadcrumbs.sell.r')</li>
</ul>
@stop

@section('content')
<main>
  @include('front.include.sidebar')
  <div class="main-container">
    @include('front.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section>
        <div class="page-header">
          <h1><i class="md md-swap-vert"></i> @lang('transfer.sr.title')</h1>
        </div>

        <div class="row">


          <div class="col-md-8">
            <div class="well white">
              <form id="transferForm" role="form" class="action-form" data-url="{{ route('transaction.postSellR', ['lang' => \App::getLocale()]) }}" data-parsley-validate="" onsubmit="return false;" http-type="post">
                <fieldset>

                  <div class="form-group">
                    <label class="control-label">@lang('transfer.sell.amount')</label>
                    <div class="input-group">
                      <input type="float" class="form-control" name="amount" required="">
                    </div>
                    <span class="help-block"></span>
                  </div>

                  <div class="form-group">
                    <label class="control-label">@lang('transfer.r.wallet')</label>
                    <div class="input-group">
<input type="text" class="form-control" name="wallet" disabled="" readonly="" value="{{ number_format($member->wallet->register_point,2) }}">
                    </div>
                    <span class="help-block"></span>
                  </div>


                  <div class="form-group">
                    <label class="control-label">@lang('transfer.security')</label>
                    <input type="password" class="form-control" name="s" required="">
                  </div>

                  <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                      <span class="btn-preloader">
                        <i class="md md-cached md-spin"></i>
                      </span>
                      <span>@lang('common.submit')</span>
                    </button>
                    <button type="reset" class="btn btn-default">@lang('common.cancel')</button>
                  </div>
                </fieldset>
              </form>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</main>

<div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="showModalLabel">
          <span class="md md-accessibility"></span> @lang('register.modal.title')
        </h4>
      </div>
      <div class="modal-body">
        <div class="loading text-center">
          <img src="{{ asset('assets/img/loading.gif') }}" alt="Network Loading">
          <br>
          <small class="text-primary">@lang('common.modal.load')</small>
        </div>

        <div class="error text-center">
          <i class="md md-error"></i>
          <br>
          <small class="text-danger">@lang('common.modal.error')</small>
        </div>

        <div id="modalContent"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-raised" data-dismiss="modal">@lang('common.close')</button>
      </div>
    </div>
  </div>
</div>
@stop
