<?php
    use App\Repositories\MemberRepository;
    $memberRepo = new MemberRepository;

    $member = $memberRepo->findByUsername(trim($user->username));
    $member->load('wallet');
    $member->load('detail');
?>
@extends('front.app')

@section('title')
  @lang('sidebar.wTitle')@lang('sidebar.convertT') | {{ config('app.name') }}
@stop

@section('header_styles')
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/swiper/css/swiper.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/widgets.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/sweetalert/css/sweetalert2.min.css')}}"/>
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('sidebar.wTitle')@lang('sidebar.convertT')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('sidebar.wTitle')@lang('sidebar.convertT')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>

    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            <div class="row justify-content-md-center">

                                <div class="col-sm-6 col-lg-3 media_max_991 ">
                                    <div class="icon_align" style="border-left: 5px solid #3FB6DC; box-shadow: 2px 4px 25px -8px #989898;">
                                        <div class="float-left">
                                        <p id="wp_wallet" style="font-size: 25px; color: #3FB6DC; margin-bottom: 0px; margin-top: 5px">$ {{ number_format($member->wallet->w_wallet,2) }}</p>
                                        <p style="font-size: 15px; color: #6c6c86;">@lang('common.wTitle')</p>
                                    </div>
                                    <div class="text-right" style="margin: 5px 15px 11px"><i style="background-color: #3FB6DC; padding: 10px; border-radius: 70px; width: 50px; text-align: center; font-size: 30px; font-weight: bold;" class="fa"><img style="height: 20px; margin-bottom: 5px;" src="{{asset('assets/img/wallet.png')}}"></i></div>
                                    </div>

                                </div>

                                <div class="fa-icon d-none d-sm-block" style="padding-top: 25px;"><i class="fa fa-arrow-right fa-5x"></i> </div>
                                <div class="fa-icon d-md-none" style="padding-left: 35%;padding-top: 15%;"><i class="fa fa-arrow-down fa-5x"></i> </div>

                                <div class="col-sm-6 col-lg-3 media_max_991">
                                    <div class="icon_align" style="border-left: 5px solid #FF7052; box-shadow: 2px 4px 25px -8px #989898;">
                                        <div class="float-left">
                                            <p id="t_wallet"  style="font-size: 25px; color: #FF7052; margin-bottom: 0px; margin-top: 5px">$ {{ number_format($member->wallet->t_wallet,2) }}</p>
                                            <p style="font-size: 15px; color: #6c6c86;">@lang('common.transferTitle')</p>
                                        </div>
                                        <div class="text-right" style="margin: 5px 15px 11px"><i style="background-color: #FF7052; padding: 10px; border-radius: 70px; width: 50px; text-align: center; font-size: 30px; font-weight: bold;" class="fa"><img style="height: 20px; margin-bottom: 5px;" src="{{asset('assets/img/wallet.png')}}"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="card-body m-t-35">
                            <form action="#" class="form-horizontal login_validator" id="form_convert">
                                <div class="form-group row">
                                    <div class="col-lg-4 text-lg-right">
                                        <label for="Digits" class="col-form-label">@lang('common.convertableAmount') *</label>
                                    </div>
                                    <div class="col-lg-4">
                                        @if($member->wallet->w_wallet <= 0)
                                        <input class="form-control" type="text" value="$ 0.00" disabled="">
                                        @elseif($member->wallet->w_wallet >= $member->wallet->wp_wallet)
                                        <input class="form-control" type="text" value="$ {{ number_format($member->wallet->wp_wallet,2) }}" disabled="">
                                        @else
                                        <input class="form-control" type="text" value="$ {{ number_format($member->wallet->w_wallet,2) }}" disabled="">
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-4 text-lg-right">
                                        <label for="Digits" class="col-form-label">@lang('transfer.convertamount') *</label>
                                    </div>
                                    <div class="col-lg-4">
                                        <input id="amount" class="form-control" type="text" name="digits_only" maxlength="10" onkeypress="return validateFloatKeyPress(this, event);" onpaste="return false;">
                                    </div>
                                </div>

                                <div class="form-actions form-group row">
                                    <div class="col-xl-8 ml-auto">
                                        <input type="button" value="@lang('common.submit')" id="postConvert" class="btn btn-primary">
                                    </div>
                                </div>
                            </form>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer_scripts')

    <script type="text/javascript">

        var w_wallet = "{{ ($member->wallet->w_wallet) }}";
        var t_wallet = "{{ ($member->wallet->t_wallet) }}";
        var wp_wallet = "{{ ($member->wallet->wp_wallet) }}";

        $('#amount').on('keyup', function(){
            var amount = $('#amount').val();
            
            if(!amount || amount == '.'){
                amount = 0;
            } 

            if(parseFloat(amount) > parseFloat(wp_wallet)) {
                amount = wp_wallet;
                $('#amount').val(wp_wallet);
            }

            var newBalance1 = parseFloat(parseFloat(w_wallet)-parseFloat(amount)).toFixed(2);
            var newBalance2 = parseFloat(parseFloat(t_wallet)+parseFloat(amount)).toFixed(2);
            if(newBalance1<0){
                $('#amount').val('0');
                $('#t_wallet').text(format2(t_wallet,"$ "));
                $('#wp_wallet').text(format2(w_wallet,"$ "));
            }else{
                if(parseFloat(amount) == parseFloat(wp_wallet)) $('#amount').val(amount);
                $('#t_wallet').text(format2(newBalance2,"$ "));
                $('#wp_wallet').text(format2(newBalance1,"$ "));
            }
        });

        $('#postConvert').on('click', function(){

            var amount = $('#amount').val();
            if(!amount){
                
                notiAlert(1, "@lang('error.emptyAmount')", '{{\App::getLocale()}}');
                return false;
            }
            var url = "{{ route('transaction.postconvertwtot', ['lang' => \App::getLocale()]) }}";
            var buttonDisplay = '{"Format":['+'{"confirmButtonText":'+'"@lang('common.confirm')"'+',"cancelButtonText":'+'"@lang('common.cancel')"'+',"setTittle":'+'"@lang('settings.secret')"'+',"error":'+'"@lang('error.securityPasswordError')"'+'}]}';
            var data = '{"Data":['+'{"amount":"'+amount+'"}]}';
            swalAlert(url,buttonDisplay,data);
            return false;
        });
    </script>
@stop
