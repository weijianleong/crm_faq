<?php
    
    use App\Repositories\SharesRepository;
    use App\Repositories\MemberRepository;
    use Carbon\Carbon;
    use Illuminate\Support\Str;
    $sharesRepo = new SharesRepository;
    $memberRepo = new MemberRepository;
    $announcementModel = new \App\Models\Announcement;
    
    $member = $memberRepo->findByUsername(trim($user->username));
    $member->load('wallet');
    $member->load('detail');
    //$member->load('shares');
    $member->load('package');
    
    
    $today = new DateTime('now');
    $todaydate = $today->format('d');
    
    
    ?>
@extends('front.app')

@section('title')
@lang('withdraw.title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">@lang('breadcrumbs.front')</a></li>
    <li><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
    <li class="active">@lang('breadcrumbs.withdraw')</li>
  </ul>
@stop

@section('content')
  <main>
    @include('front.include.sidebar')
    <div class="main-container">
      @include('front.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section>
          <div class="page-header">
            <h1><i class="md md-swap-vert"></i> @lang('withdraw.title')</h1>
            <!-- <p class="lead">@lang('withdraw.notice')</p> -->
          </div>


            <div class="row">
              <div class="col-md-8">
                <div class="well white">
                  <form id="withdrawForm" role="form" class="action-form" data-url="{{ route('transaction.postWithdraw', ['lang' => \App::getLocale()]) }}" data-parsley-validate="" onsubmit="return false;" http-type="post">
                    <fieldset>

                      <div class="form-group">
                        <label class="control-label">@lang('settings.name')</label>
                        <input type="text" class="form-control" required="" value="{{ $member->user->first_name }}" name="first_name" disabled="" readonly="">
                      </div>

                      <div class="form-group">
                        <label class="control-label" for="inputId">@lang('settings.id')</label>
                        <input type="text" name="identification_number" class="form-control" id="inputId" value="{{ $member->detail->identification_number }}" disabled="" readonly="">
                      </div>

                      <div class="form-group">
                        <label class="control-label" >@lang('withdraw.payment_method'):</label><br>
                           
                            
                                <select class="select2-results__option" name="payment_type" id="payment_type" onchange="show_btn()">
                                            <option value="" >@lang('withdraw.Select_Payment')</option>
                                            <option value="bank" >@lang('withdraw.Bank_Payment')</option>
                                            <option value="debit"  >@lang('withdraw.Card_Payment')</option>
                                </select>
                        
                             <button type="button" class="btn btn-warning"  id="update_btn" disabled="">@lang('withdraw.update_payment')</button>     
                            
                      </div>



                      <div class="form-group">
                        <label class="control-label">@lang('withdraw.amount')</label>
                        <div class="input-group">

                          <input type="number" class="form-control" name="amount" required="" min="50">
                        </div>
                        <span class="help-block">@lang('withdraw.amountNotice')</span>
                      </div>



                      <div class="form-group">
                        <label class="control-label">@lang('withdraw.security')</label>
                        <input type="password" class="form-control" name="s" required="">
                      </div>

                      <div id="v">
                        <input type="hidden"  name="v_cardname" id="v_cardname" value=""  /> 
                        <input type="hidden"  name="v_country" id="v_country" value="" /> 
                        <input type="hidden"  name="v_error" id="v_error" value="@lang('withdraw.Card_error')" />      
                      </div>

                      <div id="b">
                        <input type="hidden"  name="bank_account_holder" id="bank_account_holder"  value="{{ $member->user->first_name}}"/> 
                        <input type="hidden"  name="bank_account_number" id="bank_account_number"  value="{{ $member->detail->bank_account_number}}" />
                        <input type="hidden"  name="bank_name" id="bank_name" value="{{ $member->detail->bank_name}}"  /> 
                        <input type="hidden"  name="bank_swiftcode" id="bank_swiftcode" value="{{ $member->detail->bank_swiftcode}}"   />
                        <input type="hidden"  name="bank_address" id="bank_address" value="{{ $member->detail->bank_address}}"  /> 
                        <input type="hidden"  name="bank_country" id="bank_country" value="{{ $member->detail->bank_country}}"   />
                      </div>

                      <input type="hidden"  name="update_s" id="update_s" value="@lang('withdraw.update_succ')" /> 
                      <input type="hidden" value="{{ csrf_token() }}" name="_token">
                      <div class="form-group">
                        <button type="submit" class="btn btn-primary">
                          <span class="btn-preloader">
                            <i class="md md-cached md-spin"></i>
                          </span>
                          <span>@lang('common.submit')</span>
                        </button>
                        <button type="reset" class="btn btn-default">@lang('common.cancel')</button>
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
            <!-- weilun -->
            <!-- b Modal -->
            <div class="modal fade" id="bank_modal" tabindex="-1" role="dialog"  aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLongTitle">@lang('withdraw.Bank_Information')</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                                  <div class="form-group">
                                    <label class="control-label">@lang('settings.bank.holder'):</label>
                                    <input type="text" class="form-control" value="{{ $member->user->first_name}}"  id="b_account_holder" disabled="">
                                  </div>

                                  <div class="form-group">
                                    <label class="control-label">@lang('settings.bank.number'):</label>
                                    <input type="text" class="form-control" value="{{ $member->detail->bank_account_number}}"  id="b_account_number" >
                                  </div>

                                  <div class="form-group">
                                    <label class="control-label">@lang('settings.bank.name'):</label>
                                    <input type="text" class="form-control"  value="{{ $member->detail->bank_name}}" id="b_name" >
                                  </div>

                                  <div class="form-group">
                                    <label class="control-label">Swift code:</label>
                                    <input type="text" class="form-control"  value="{{ $member->detail->bank_swiftcode}}" id="b_swiftcode" >
                                  </div>

                                  <div class="form-group">
                                    <label class="control-label">@lang('settings.bank.address'):</label>
                                    <textarea type="text" class="form-control" maxlength="500" name="b_address" id="b_address">{{ $member->detail->bank_address}}</textarea>
                                  </div>

                                  <?php $countries = config('misc.countries'); ksort($countries); ?>
                                  <div class="form-group">
                                    <label class="control-label" >@lang('settings.bank.country')</label>
                                    <select class="form-control" name="b_country" id="b_country">
                                      @foreach ($countries as $country => $value)
                                        <option value="{{ $country }}" @if ($member->detail->bank_country == $country) selected="" @endif >{{ $country }}</option>
                                      @endforeach
                                    </select>
                                  </div>              
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary" onclick="get_b_data()">Save changes</button>
                    </div>
                  </div>
                </div>    
            </div>
            <!-- v Modal --> 
            <div class="modal fade" id="visa_modal" tabindex="-1" role="dialog"  aria-hidden="true">
              
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLongTitle">@lang('withdraw.Debit_Card')</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">


                                  <div class="form-group">
                                    <label class="control-label">@lang('withdraw.Card_Number'):</label>
                                    <input type="text" class="form-control" name="vm_cardname" id="vm_cardname" >
                                  </div>

                                  <div class="form-group">
                                    <label class="control-label">@lang('withdraw.Confirm_Card_Number'):</label>
                                    <input type="text" class="form-control" name="vm_cardname2" id="vm_cardname2" >
                                  </div>


                                  <?php $countries = config('misc.countries');  ?>
                                  <div class="form-group">
                                    <label class="control-label" for="vm_country">@lang('withdraw.Card_Country')</label>
                                        <select class="form-control" name="vm_country" id="vm_country">
                                              @foreach ($countries as $country => $value)
                                                      <option value="{{ $country }}">@lang('country.' . $country)
                                                      </option>
                                              @endforeach
                                        </select>
                                  </div>
                                  
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary" onclick="get_v_data()">Save changes</button>
                    </div>
                  </div>
                </div>
            </div>


            <script type="text/javascript">
              function show_btn(){

                var button_type = document.getElementById("payment_type").value;

                if( button_type == "bank"){

                  document.getElementById( "update_btn" ).setAttribute( "onClick", "javascript: bank_btn();" );
                  document.getElementById( "update_btn" ).disabled=false;

                }else if ( button_type == "debit"){

                   document.getElementById( "update_btn" ).setAttribute( "onClick", "javascript: visa_btn();" );
                   document.getElementById( "update_btn" ).disabled=false;
                }else{
                   document.getElementById( "update_btn" ).disabled=true;
                }
              }

              function bank_btn(){
                  $('#bank_modal').modal('show')   
              }
              function visa_btn(){
                  $('#visa_modal').modal('show')   
              }

              function get_v_data(){
                var vm_cardname = document.getElementById( "vm_cardname" ).value;
                var vm_cardname2 = document.getElementById( "vm_cardname2" ).value;
                var paymentError = document.getElementById( "v_error" ).value;
                var update_succ = document.getElementById( "update_s" ).value;
                
                if( vm_cardname == ""){
                  document.getElementById( "vm_cardname" ).focus();
                  return false;
                }
                if( vm_cardname2 == ""){
                  document.getElementById( "vm_cardname2" ).focus();
                  return false;
                }

                if( vm_cardname != vm_cardname2){
                  alert(paymentError);
                  return false;
                }else{
                  document.getElementById( "v_cardname" ).value = vm_cardname;
                  document.getElementById( "v_country" ).value =  document.getElementById( "vm_country" ).value;
                  alert(update_succ);
                  $('#visa_modal').modal('hide')
                }
              }
              function get_b_data(){
                var b_account_holder = document.getElementById( "b_account_holder" ).value;
                var b_account_number = document.getElementById( "b_account_number" ).value;
                var b_name = document.getElementById( "b_name" ).value;
                var b_swiftcode = document.getElementById( "b_swiftcode" ).value;
                var b_address = document.getElementById( "b_address" ).value;
                var b_country = document.getElementById( "b_country" ).value;
                var update_succ = document.getElementById( "update_s" ).value;

                if( b_account_holder == ""){
                  document.getElementById( "b_account_holder" ).focus();
                  return false;
                }
                if( b_account_number == ""){
                  document.getElementById( "b_account_number" ).focus();
                  return false;
                }
                if( b_name == ""){
                  document.getElementById( "b_name" ).focus();
                  return false;
                }
                if( b_swiftcode == ""){
                  document.getElementById( "b_swiftcode" ).focus();
                  return false;
                }
                if( b_address == ""){
                  document.getElementById( "b_address" ).focus();
                  return false;
                }

                document.getElementById( "bank_account_holder" ).value = b_account_holder;
                document.getElementById( "bank_account_number" ).value = b_account_number;
                document.getElementById( "bank_name" ).value = b_name;
                document.getElementById( "bank_swiftcode" ).value = b_swiftcode;
                document.getElementById( "bank_address" ).value = b_address;
                document.getElementById( "bank_country" ).value = b_country;
                alert(update_succ);
                $('#bank_modal').modal('hide')
              }
            </script>




      

        </section>
      </div>
    </div>
  </main>
@stop
