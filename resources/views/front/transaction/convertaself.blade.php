<?php
    use App\Repositories\MemberRepository;
    $memberRepo = new MemberRepository;
    $member = $memberRepo->findByUsername(trim($user->username));
    $member->load('wallet');
    $member->load('detail');
    $accounts = \DB::table('Member_Account_Self')->where('member_id', '=', $member->id)->where('status', '=', 0)->get();
?>
@extends('front.app')

@section('title')
  @lang('transfer.a.title')  | {{ config('app.name') }}
@stop

@section('header_styles')
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/swiper/css/swiper.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/widgets.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/sweetalert/css/sweetalert2.min.css')}}"/>
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('transfer.a.title')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('transfer.a.title')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>

    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            <div class="row justify-content-md-center">

                                <div class="col-sm-6 col-lg-3 media_max_991">
                                    <div class="icon_align" style="border-left: 5px solid #3FB6DC; box-shadow: 2px 4px 25px -8px #989898;">
                                        <div class="float-left">
                                            <p id="to_balance" style="font-size: 25px; color: #3FB6DC; margin-bottom: 0px; margin-top: 5px">@lang('transfer.balance')</p>
                                            <p id="to_account" style="font-size: 15px; color: #6c6c86; ">@lang('common.select')</p>
                                            
                                        </div>
                                        <div class="text-right" style="margin: 5px 15px 11px"><i style="background-color: #3FB6DC; padding: 10px; border-radius: 70px; width: 50px; text-align: center; font-size: 30px; font-weight: bold;" class="fa fa-book text-white"></i></div>
                                    </div>
                                </div>

                                <div class="fa-icon d-none d-sm-block" style="padding-top: 25px;"><i class="fa fa-arrow-right fa-5x"></i> </div>
                                <div class="fa-icon d-md-none" style="padding-left: 35%;padding-top: 15%;"><i class="fa fa-arrow-down fa-5x"></i> </div>


                                <div class="col-sm-6 col-lg-3 media_max_991">
                                    <div class="icon_align" style="border-left: 5px solid #FFC800; box-shadow: 2px 4px 25px -8px #989898;">
                                        <div class="float-left">
                                            <p id="cash_point"  style="font-size: 25px; color: #FFC800; margin-bottom: 0px; margin-top: 5px">$ {{ number_format($member->wallet->cash_point,2) }}</p>
                                            <p style="font-size: 15px; color: #6c6c86;">@lang('common.cashTitle')</p>
                                        </div>
                                        <div class="text-right" style="margin: 5px 15px 11px"><i style="background-color: #FFC800; padding: 10px; border-radius: 70px; width: 50px; text-align: center; font-size: 30px; font-weight: bold;" class="fa"><img style="height: 20px; margin-bottom: 5px;" src="{{asset('assets/img/wallet.png')}}"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="card-body m-t-35">

                               
                                <div class="form-group row">
                                    <div class="col-lg-3 text-lg-right">
                                        <label for="gender3" class="col-form-label">@lang('transfer.mt5.wallet')</label>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text border-right-0 rounded-left">
                                                <i class="fa fa-book"></i>
                                            </span>

                                            <select class="form-control" name="account" id="account" onchange="showAccount(this.options[this.selectedIndex].getAttribute('bookA'),this.options[this.selectedIndex].getAttribute('banlance'))">
                                                <option value="0">({{\Lang::get('common.select')}})</option>
                                                @if (count($accounts) > 0)
                                                    @foreach ($accounts as $account)
                                                        <?php
                                                            $balanceA = $memberRepo->AccountAPI($account->bookA);
                                                            $balanceB = $memberRepo->AccountAPI($account->bookB);
                                                            
                                     
                                                            
                                                        ?>
                                                    <option value="{{ $account->id }}" bookA="{{ \Lang::get('transfer.a.wallet').' : '.$account->bookA }}" banlance ="{{ $balanceA->balance }}">
                                                        {{ \Lang::get('transfer.a.wallet').' : '.$account->bookA.' '.\Lang::get('transfer.balance').' '.$balanceA->balance }}
                                                    </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!-- first name-->
                                <div class="form-group row">
                                    <div class="col-lg-3 text-lg-right">
                                        <label for="email3" class="col-form-label">@lang('transfer.amount')</label>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text border-right-0 rounded-left">
                                                <i class="fa fa-dollar"></i>
                                            </span>
                                            <input id="amount" class="form-control" type="text" name="amount" maxlength="10" onkeypress="return validateFloatKeyPress(this, event);" onpaste="return false;">
                                        </div>
                                    </div>
                                </div>
                                <!-- re password name-->
                                <!-- re password name-->
                                <div class="form-group row">
                                    <div class="col-lg-9 ml-auto">
                                        <button class="btn btn-primary layout_btn_prevent" id="postConvert">@lang('common.submit')</button>
                                        <button class="btn btn-secondary layout_btn_prevent">@lang('common.cancel')</button>
                                    </div>
                                </div>
                           
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('front.include.modal')
@stop

@section('footer_scripts')
    <script type="text/javascript">
        var cash_point = "{{ ($member->wallet->cash_point) }}";

        $('#amount').on('keyup', function(){
            var amount = $('#amount').val();
            var to_balance = $('#to_balance').val();
            if(!amount || amount == '.'){
                amount = 0;
            } 
            
            var newBalance1 = parseFloat(parseFloat(cash_point)+parseFloat(amount)).toFixed(2);
            var newBalance2 = parseFloat(parseFloat(to_balance)-parseFloat(amount)).toFixed(2);
            if(newBalance2<0){
                $('#amount').val('0');
                $('#cash_point').text(format2(cash_point,"$ "));
                $('#to_balance').text(format2(to_balance,"$ "));
            }else{
                $('#cash_point').text(format2(newBalance1,"$ "));
                $('#to_balance').text(format2(newBalance2,"$ "));
            }
        });

        $('#postConvert').on('click', function(){
            var amount = $('#amount').val();
            var account = $('#account').val();
            
            var obj = {account: "account", amount: "amount"};
            var final = { Id : obj};
            var Id = JSON.stringify(final);
            var ans = checkValue(Id);

            if(ans!=0){
                return false;
            }
            
            if(!amount){
                
                notiAlert(1, "@lang('error.emptyAmount')", '{{\App::getLocale()}}');
                return false;
            }
            var url = "{{ route('transaction.postConvertASelf', ['lang' => \App::getLocale()]) }}";
            var buttonDisplay = '{"Format":['+'{"confirmButtonText":'+'"@lang('common.confirm')"'+',"cancelButtonText":'+'"@lang('common.cancel')"'+',"setTittle":'+'"@lang('settings.secret')"'+',"error":'+'"@lang('error.securityPasswordError')"'+'}]}';
            var obj = {amount: amount, account:account};
            var final = { Data : obj};
            var data = JSON.stringify(final);
            swalAlert(url,buttonDisplay,data);
            return false;
        });

        function showAccount(bookA,balance){
            var cash_point = "{{ ($member->wallet->cash_point) }}";
            var show_select = "@lang('common.select')";
            var show_balance = "@lang('transfer.balance')";
            $('#amount').val("");
            $('#cash_point').text(format2(cash_point,"$ "));


            if($('#account').val()==0){
                
                $('#to_account').text(show_select);
                $('#to_balance').text(show_balance);
                
            }else{
                $('#to_account').text(bookA);
                $('#to_balance').text(format2(balance,"$ "));
                $('#to_balance').val(balance);
            }
            
            

        }


    </script>
@stop
