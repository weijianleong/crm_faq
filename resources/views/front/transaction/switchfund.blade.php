<?php
    use App\Repositories\MemberRepository;
    $memberRepo = new MemberRepository;
    $member = $memberRepo->findByUsername(trim($user->username));

    $account = \DB::table('Member_Account_Fund')->where('member_id', '=', $member->id)->where('system_manage', '=', 'S')->where('bookA', $bookA)->first();

    $balanceA = $memberRepo->AccountAPI($account->bookA);

    $partners = \DB::connection('mysql3')->table('user')->where('UserType', '=', 2)->where('Status', '=', 1)->where('UserId', '<>', 84)->where('UserId', '<>', 86)->where('UserId', '<>', 85)->where('UserId', '<>', 88)->where('UserId', '<>', 153)->where('UserId', '<>', 89)->get();
?>

@extends('front.app')

@section('title')
    @lang('misc.modal.swtitle') | {{ config('app.name') }}
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i>@lang('misc.modal.swtitle')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('misc.modal.swtitle')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="card m-t-35">
                <div class="card-header bg-white">@lang('misc.modal.swtitle')</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12 m-t-25">
                            <fieldset>
                                <div class="form-group row">
                                    <div class="col-lg-3 text-lg-right">
                                        <label for="bookA" class="col-form-label">@lang('transfer.mt5.wallet')</label>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text border-right-0 rounded-left">
                                                <i class="fa fa-book"></i>
                                            </span>
                                            <input type="text" name="bookADisplay" id="bookADisplay" value="@lang('transfer.a.wallet'): {{$account->bookA}} @lang('transfer.balance'): ${{$balanceA->balance}} ; @lang('transfer.b.wallet'): {{$account->bookB}} @lang('transfer.balance'): ${{$account->balanceB}}" class="form-control" disabled="" style="cursor: not-allowed;">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-3 text-lg-right">
                                        <label for="oldfundcompany" class="col-form-label">@lang('misc.oldfundcompany')</label>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text border-right-0 rounded-left">
                                                <i class="fa fa-clone"></i>
                                            </span>
                                            @if (count($partners) > 0)
                                            @foreach ($partners as $partner)
                                            @if($account->fundcompany == $partner->UserId)
                                                <input type="text" value="@if (\App::getLocale() == 'chs') {{ $partner->ChineseName }} @else {{ $partner->FirstName }} {{ $partner->LastName }} @endif" class="form-control" disabled="" style="cursor: not-allowed;">
                                            @endif
                                            @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-3 text-lg-right">
                                        <label for="gender3" class="col-form-label">@lang('misc.fundcompany')</label>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="input-group input-group-prepend">
                                            <span class="input-group-text border-right-0 rounded-left">
                                                <i class="fa fa-clone"></i>
                                            </span>
                                            <select class="form-control" name="fundcompany" id="fundcompany">
                                                <option value="0">请选择托管公司 (Please Select Account Manage Company)</option>
                                                @if (count($partners) > 0)
                                                @foreach ($partners as $partner)
                                                    <option value="{{ $partner->UserId }}">@if (\App::getLocale() == 'chs') {{ $partner->ChineseName }} @else {{ $partner->FirstName }} {{ $partner->LastName }} @endif</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-9 ml-auto">
                                        <button class="btn btn-primary layout_btn_prevent" onclick="switchFund('{{$account->bookA}}')">@lang('common.submit')</button>
                                        <a class="btn btn-secondary layout_btn_prevent" href="{{ route('misc.fundstatement', ['lang' => \App::getLocale()]) }}">@lang('common.cancel')</a>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer_scripts')
<script type="text/javascript">
    function switchFund(bookA){
        var fundcompany = $('#fundcompany').val();
        
        var url = "{{ route('transaction.postSwitchMT5Fund', ['lang' => \App::getLocale()]) }}";
        var buttonDisplay = '{"Format":['+'{"confirmButtonText":'+'"@lang('common.confirm')"'+',"cancelButtonText":'+'"@lang('common.cancel')"'+',"setTittle":'+'"@lang('settings.secret')"'+',"error":'+'"@lang('error.securityPasswordError')"'+'}]}';
        var obj = {bookA: bookA, fundcompany: fundcompany};
        var final = { Data : obj};
        var data = JSON.stringify(final);
        swalAlert(url,buttonDisplay,data);
        return false;
    }
</script>
@stop
