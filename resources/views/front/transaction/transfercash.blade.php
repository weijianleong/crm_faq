<?php
    use App\Repositories\MemberRepository;
    $memberRepo = new MemberRepository;
    $member = $memberRepo->findByUsername(trim($user->username));
    $member->load('wallet');
    $member->load('detail');
?>
@extends('front.app')

@section('title')
  @lang('sidebar.cashLink1')  | {{ config('app.name') }}
@stop

@section('header_styles')
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/swiper/css/swiper.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/widgets.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/sweetalert/css/sweetalert2.min.css')}}"/>
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('sidebar.cashLink1')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('sidebar.cashLink1')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>

    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            <div class="row justify-content-md-center">
                                <div class="col-sm-6 col-lg-3 media_max_991">
                                    <div class="icon_align" style="border-left: 5px solid #FF7052; box-shadow: 2px 4px 25px -8px #989898;">
                                        <div class="float-left">
                                            <p id="t_wallet"  style="font-size: 25px; color: #FF7052; margin-bottom: 0px; margin-top: 5px">$ {{ number_format($member->wallet->t_wallet,2) }}</p>
                                            <p style="font-size: 15px; color: #6c6c86;">@lang('common.transferTitle')</p>
                                        </div>
                                        <div class="text-right" style="margin: 5px 15px 11px"><i style="background-color: #FF7052; padding: 10px; border-radius: 70px; width: 50px; text-align: center; font-size: 30px; font-weight: bold;" class="fa"><img style="height: 20px; margin-bottom: 5px;" src="{{asset('assets/img/wallet.png')}}"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="card-body" style="padding: 30px;">
                            <div class="tabs tabs-bordered tabs-icons">
                                @if(in_array($user->username, config('autosettings.transferToAll')))
                                <ul class="nav nav-tabs" style="border-bottom: 1px solid #dee2e6;">
                                    <li class="nav-item " id="primary1">
                                        <a id="downlineTab" href="#downline" class="nav-link active " data-toggle="tab" aria-expanded="true"><i class="fa fa-arrow-down"></i> @lang('transfer.transferAll')</a>
                                    </li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <br>
                                    <div class="tab-pane reset padding-all fade active show " id="downline">
                                        <div class="form-group row">
                                            <div class="col-lg-3 text-lg-right">
                                                <label for="name3" class="col-form-label">@lang('transfer.target')</label>
                                            </div>

                                            <div class="col-lg-8">
                                                <div class="input-group input-group-prepend">
                                                    <span class="input-group-text border-right-0 rounded-left">
                                                        <i class="fa fa-user"></i>
                                                    </span>
                                                    <input type="text" name="to_username" id="to_username_downline"  class="form-control" placeholder="">
                                                    <button class="btn btn-primary" type="button" onclick="checkNetwork()">
                                                        <span class="glyphicon glyphicon-search" aria-hidden="true">
                                                        </span>  @lang('register.checkID')
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- first name-->
                                    <div class="form-group row">
                                        <div class="col-lg-3 text-lg-right">
                                            <label for="email3" class="col-form-label">@lang('transfer.amount')</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="input-group input-group-prepend">
                                                <span class="input-group-text border-right-0 rounded-left">
                                                    <i class="fa fa-dollar"></i>
                                                </span>
                                                <input id="amount" class="form-control" type="text" name="amount" maxlength="10" onkeypress="return validateFloatKeyPress(this, event);" onpaste="return false;">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- re password name-->
                                    <div class="form-group row" style="display: none;">
                                        <div class="col-lg-3 text-lg-right">
                                            <label for="confirm3" class="col-form-label">@lang('transfer.remark')</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="input-group input-group-prepend">
                                                <span class="input-group-text border-right-0 rounded-left">
                                                    <i class="fa fa-pencil"></i>
                                                </span>
                                                <input id="remark" class="form-control" type="text" name="remark" onpaste="return false;">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- re password name-->
                                    <div class="form-group row">
                                        <div class="col-lg-9 ml-auto">
                                            <button class="btn btn-primary layout_btn_prevent" id="postConvert">@lang('common.submit')</button>
                                            <!-- <button class="btn btn-secondary layout_btn_prevent">@lang('common.cancel')</button> -->
                                        </div>
                                    </div>
                                </div>
                                @else
                                <ul class="nav nav-tabs" style="border-bottom: 1px solid #dee2e6;">
                                    <li class="nav-item " id="primary1">
                                        <a id="downlineTab" href="#downline" class="nav-link active " data-toggle="tab" aria-expanded="true"><i class="fa fa-arrow-down"></i> @lang('transfer.transferDownline')</a>
                                    </li>
                                    <li class="nav-item" id="primary2">
                                        <a id="uplineTab" href="#upline" class="nav-link" data-toggle="tab" aria-expanded="false"><i class="fa fa-arrow-up"></i> @lang('transfer.transferUpline')</a>
                                    </li>
                                </ul>
                                
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <br>
                                    <div class="tab-pane reset padding-all fade active show " id="downline">
                                        <div class="form-group row">
                                            <div class="col-lg-3 text-lg-right">
                                                <label for="name3" class="col-form-label">@lang('transfer.target')</label>
                                            </div>

                                            <div class="col-lg-8">
                                                <div class="input-group input-group-prepend">
                                                    <span class="input-group-text border-right-0 rounded-left">
                                                        <i class="fa fa-user"></i>
                                                    </span>
                                                    <input type="text" name="to_username" id="to_username_downline"  class="form-control" placeholder="">
                                                    <button class="btn btn-primary" type="button" onclick="checkNetwork()">
                                                        <span class="glyphicon glyphicon-search" aria-hidden="true">
                                                        </span>  @lang('register.checkID')
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane reset padding-all fade" id="upline">
                                        <div class="form-group row">
                                            <div class="col-lg-3 text-lg-right">
                                                <label for="name3" class="col-form-label">@lang('transfer.target')</label>
                                            </div>

                                            <div class="col-lg-8">
                                                <div class="input-group input-group-prepend">
                                                    <span class="input-group-text border-right-0 rounded-left">
                                                        <i class="fa fa-user"></i>
                                                    </span>
                                                    <input type="text" name="to_username" id="to_username_upline"  class="form-control" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- first name-->
                                    <div class="form-group row">
                                        <div class="col-lg-3 text-lg-right">
                                            <label for="email3" class="col-form-label">@lang('transfer.amount')</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="input-group input-group-prepend">
                                                <span class="input-group-text border-right-0 rounded-left">
                                                    <i class="fa fa-dollar"></i>
                                                </span>
                                                <input id="amount" class="form-control" type="text" name="amount" maxlength="10" onkeypress="return validateFloatKeyPress(this, event);" onpaste="return false;">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- re password name-->
                                    <div class="form-group row" style="display: none;">
                                        <div class="col-lg-3 text-lg-right">
                                            <label for="confirm3" class="col-form-label">@lang('transfer.remark')</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="input-group input-group-prepend">
                                                <span class="input-group-text border-right-0 rounded-left">
                                                    <i class="fa fa-pencil"></i>
                                                </span>
                                                <input id="remark" class="form-control" type="text" name="remark" onpaste="return false;">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- re password name-->
                                    <div class="form-group row">
                                        <div class="col-lg-9 ml-auto">
                                            <button class="btn btn-primary layout_btn_prevent" id="postConvert">@lang('common.submit')</button>
                                            <!-- <button class="btn btn-secondary layout_btn_prevent">@lang('common.cancel')</button> -->
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('front.include.modal')
@stop

@section('footer_scripts')
    <script type="text/javascript">
        var t_wallet = "{{ ($member->wallet->t_wallet) }}";

        $('#amount').on('keyup', function(){
            var amount = $('#amount').val();
            
            if(!amount || amount == '.'){
                amount = 0;
            } 
           
            var newBalance1 = parseFloat(parseFloat(t_wallet)-parseFloat(amount)).toFixed(2);
                if(newBalance1<0){
                    $('#amount').val('0');
                    $('#t_wallet').text(format2(t_wallet,"$ "));
                }else{
                    $('#t_wallet').text(format2(newBalance1,"$ "));
                }
        });

        $('#postConvert').on('click', function(){
            var getActive = $("ul.nav-tabs li a.active");
            if(getActive[0].id == 'downlineTab'){
                var to_username = $('#to_username_downline').val();
                var network = 'down';
                var obj = {to_username: "to_username_downline", amount: "amount"};
            }else{
                var to_username = $('#to_username_upline').val();
                var network = 'up';
                var obj = {to_username: "to_username_upline", amount: "amount"};
            }
            var amount = $('#amount').val();
            var remark = $('#remark').val();

            var final = { Id : obj};
            var Id = JSON.stringify(final);
            var ans = checkValue(Id);

            if(ans!=0){
                return false;
            }
            
            if(!amount){
                notiAlert(1, "@lang('error.emptyAmount')", '{{\App::getLocale()}}');
                return false;
            }
            var url = "{{ route('transaction.postTransferCash', ['lang' => \App::getLocale()]) }}";
            var buttonDisplay = '{"Format":['+'{"confirmButtonText":'+'"@lang('common.confirm')"'+',"cancelButtonText":'+'"@lang('common.cancel')"'+',"setTittle":'+'"@lang('settings.secret')"'+',"error":'+'"@lang('error.securityPasswordError')"'+'}]}';
            var obj = {amount: amount, to_username: to_username, remark: remark, network: network};
            var final = { Data : obj};
            var data = JSON.stringify(final);
            swalAlert(url,buttonDisplay,data);
            return false;
        });

        function checkNetwork() {
            var disable = loadingDisable();
            var modelContent = '';
            var id = $('#to_username_downline').val();
            var ajaxUrl = "{{ route('member.checkNetwork', ['username' =>'parentId', 'lang' => \App::getLocale()]) }}";
                ajaxUrl = ajaxUrl.replace('parentId', id);

            $.ajax({
                url     :   ajaxUrl,
                method  :   "get",
                headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
                success : function(response){
                    if(response.status == 0){
                       
                            modelContent =  '<div style="font-size: 28px !important;">'+response.msg+'</div>';
                            modelContent += '<hr><br/>';
                            modelContent += '<div style="font-size: 15px !important;font-weight: 100;">@lang("register.email"): '+response.model.username+'</div>';
                            modelContent += '<br/>';
                            modelContent += '<div style="font-size: 15px !important;">@lang("register.name"): '+response.modelIsban.first_name+'</div>';
                            modelContent += '<br/>';
                            modelContent += '<div style="font-size: 15px !important;">@lang("sidebar.rank"): '+response.rank+'</div>';
                        
                        $('#modalContent').empty().append(modelContent);
                        $('#modalZoomIn').click();
                    }else{
                        notiAlert(response.status, response.msg, '{{\App::getLocale()}}');
                    }
                    disable.out();
                }
            });
        }

    </script>
@stop
