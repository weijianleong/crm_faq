@extends('front.app')

@section('title')
  @lang('helpdesk.addDesk') | {{ config('app.name') }}
@stop

@section('header_styles')
    
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/radio_css/css/radiobox.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/checkbox_css/css/checkbox.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/radio_checkbox.css')}}" />
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li class="active"><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
  </ul>
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <script>
        window.onload = function() {
          remind();
        };
    </script>

    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i>@lang('helpdesk.addDesk')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="index1">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('helpdesk.addDesk')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>

    <div class="outer">
        <div class="inner bg-container">
            <div class="card m-t-35">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12 m-t-25">
                            <div>
                                <ul class="nav nav-inline view_user_nav_padding" id="content-tab">
                                    <li class="nav-item card_nav_hover">
                                        <a class="nav-link active" href="#bank" id="home-tab"
                                           data-toggle="tab" aria-expanded="true">@lang('helpdesk.yourQs')</a>
                                    </li>



                                </ul>

                                <div id="clothing-nav-content" class="tab-content m-t-10">
                                    <div role="tabpanel" class="tab-pane fade show active" id="bank">
                                        
                                            <fieldset>
                                                <?php $HType = config('helpdesk.type');  ?>
                                                <div class="form-group row">
                                                    <div class="col-lg-3 text-lg-right">
                                                        <label for="gender3" class="col-form-label"></label>
                                                    </div>

                                                    <div class="col-lg-8">
                                                        <div class="input-group input-group-prepend">
                                                            <div class="radio">
                                                                @foreach ($HType as $HTypes => $value)
                                                                    <label style="margin-right: 30px;">
                                                                        <input type="radio" name="HType" value="{{ $value }}">
                                                                        <span class="cr"><i class="cr-icon fa fa-star"></i></span>
                                                                        @lang('helpdesk.' . $HTypes)
                                                                    </label>   
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                        
                                                    </div>


                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-lg-3 text-lg-right">
                                                        <label for="subject2" class="col-form-label">@lang('helpdesk.QsTitle')</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="input-group input-group-append">
                                                            <input type="text" id="QsTitle" name="QsTitle" class="form-control" placeholder="@lang('helpdesk.QsTitle')">
                                                            <span class="input-group-text border-left-0 rounded-right"><i class="fa fa-text-width" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-lg-3 text-lg-right">
                                                        <label for="query2" class="col-form-label">@lang('helpdesk.QsContent')</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="input-group">
                                                            <textarea id="QsContent" name="QsContent" class="form-control" rows="8" placeholder="@lang('helpdesk.yourQs')"></textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-lg-3 text-lg-right">
                                                        <label for="query2" class="col-form-label"></label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <a href="#" data-toggle="tooltip"  onclick="uploadFront()">
                                                            <i class="fa fa-upload"></i>&nbsp; @lang('settings.plzChose')
                                                        </a>
                                                        <input type="file" name="inputIDfront"  class="form-control" id="inputIDfront"  hidden="" onchange="showFrontMsg()">
                                                        &nbsp;
                                                        <span id="showPic1Name"></span>
                                                        <span id="showFront"></span>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-lg-3 text-lg-right">
                                                        <label for="query2" class="col-form-label"></label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <a href="#" data-toggle="tooltip"  onclick="uploadBack()">
                                                            <i class="fa fa-upload"></i>&nbsp; @lang('settings.plzChose')
                                                        </a>
                                                        <input type="file" name="inputIDback"  class="form-control" id="inputIDback"  hidden="" onchange="showBackMsg()">
                                                        &nbsp;
                                                        <span id="showPic2Name"></span>
                                                        <span id="showBack"></span>
                                                    </div>
                                                </div>
                                                <!-- last name-->
                                                <div class="form-group row">
                                                    <div class="col-lg-9 ml-auto">
                                                        <button class="btn btn-primary layout_btn_prevent" onclick="addHelpdesk()">@lang('common.submit')</button>
                                                        <button class="btn btn-secondary layout_btn_prevent">@lang('common.cancel')</button>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        
                                    </div>

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer_scripts')

    <!--Plugin scripts-->

    <!--End of Page level scripts-->
    <script type="text/javascript">
        function uploadFront(){
            $('#inputIDfront').click();
        }
        function uploadBack(){
            $('#inputIDback').click();
        }
        function showFrontMsg(){
            if(document.getElementById("inputIDfront").files.length == 1){
                document.getElementById("showFront").innerHTML = "<i class='fa fa-check text-success'></i>";
                document.getElementById("showPic1Name").innerHTML = document.getElementById("inputIDfront").files[0].name;
            }
        }
        function showBackMsg(){
            if(document.getElementById("inputIDback").files.length == 1){
                document.getElementById("showBack").innerHTML = "<i class='fa fa-check text-success'></i>";
                document.getElementById("showPic2Name").innerHTML = document.getElementById("inputIDback").files[0].name;
            }
        }

        function addHelpdesk(){
            

            var HType = $("input[name='HType']:checked").val();
            if(!HType){
                notiAlert(1, "@lang('helpdesk.HTypeError')", '{{\App::getLocale()}}');
                return false;
                
            }
            var obj = {QsTitle: "QsTitle", QsContent:"QsContent"};
            var final = { Id : obj};
            var Id = JSON.stringify(final);
            var ans = checkValue(Id);

            if(ans!=0){
                return false;
            }
            var disable = loadingDisable();

            var QsTitle = $('#QsTitle').val();
            var QsContent = $('#QsContent').val();
            var image1 = $('#inputIDfront')[0].files[0];
            var image2 = $('#inputIDback')[0].files[0];


            var url = "{{ route('helpdesk.postDesk', ['lang' => \App::getLocale()]) }}";
            var obj = {HType:HType , QsTitle:QsTitle, QsContent:QsContent };
            var final = { Data : obj};
            var data = JSON.stringify(final);

            var form = new FormData();
            form.append('image1', image1);
            form.append('image2', image2);
            form.append('allData', data);


            $.ajax({
                url     : url,
                data    : form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                headers:
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success : function(response){
                    disable.out();
                    if(response.result == 0){

                        swal({
                            title: response.showTittle,
                            text: response.showText,
                            type: 'success',
                            confirmButtonColor: '#4fb7fe',
                            allowOutsideClick: false
                        }).then(function(){ 
                            if(response.returnUrl) document.location.href=response.returnUrl;
                        });
                    }else{

                        swal({
                            title: response.showTittle,
                            text: response.showText,
                            type: 'error',
                            confirmButtonColor: '#4fb7fe',
                            

                        }).done();
                    }   
                }
            });
            
            return false;
        }
        function remind(){
            var rateT = "@lang('helpdesk.rate')";
            var complainT = "@lang('helpdesk.complain')";
            var star = '<div class=starability-slot style="margin: auto;"><input type=radio id=slot-rate5 name=rating value="5"  /><label for="slot-rate5" title="" aria-label="Amazing, 5 stars" onclick=postStar(1)>5 stars</label><input type="radio" id="slot-rate4" name="rating" value="4" /><label for="slot-rate4" title="" aria-label="Very good, 4 stars" onclick=postStar(2)>4 stars</label><input type="radio" id="slot-rate3" name="rating" value="3"/><label for="slot-rate3" title="" aria-label="Average, 3 stars" onclick=postStar(3)>3 stars</label><input type="radio" id="slot-rate2" name="rating" value="2" /><label for="slot-rate2" title="" aria-label="Not good, 2 stars" onclick=postStar(4)>2 stars</label><input type="radio" id="slot-rate1" name="rating" value="1" /><label for="slot-rate1" title="" aria-label="Terrible, 1 star" onclick=postStar(5)>1 star</label></div>';
            var complain = '<textarea id="complain" name="complain" class="form-control" cols="50" rows="5" placeholder=""></textarea>';
            swal({
                title: "@lang('helpdesk.systemAutoReplyTittle')",
                text: "@lang('helpdesk.systemAutoReplyDetail')",
                type: 'info',

                confirmButtonColor: '#00c0ef',
                cancelButtonColor: '#ff8080',
                confirmButtonText: ''+'ok'+'',
                
                confirmButtonClass: 'btn btn-success',

            }).done();
            return false;
        }
    </script>
@stop
