@extends('front.app')

@section('title')
  @lang('helpdesk.readDetail') | {{ config('app.name') }}
@stop

@section('header_styles')
    
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/swiper/css/swiper.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/widgets.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/starability/css/starability-all.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstraprating/css/star-rating.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/rateyo/css/jquery.rateyo.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/sweetalert/css/sweetalert2.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/ratings.css')}}">
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li class="active"><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
  </ul>
@stop

@section('main_content')
@include('front.include.header')
@inject('helpdesk_read_presenter','App\Presenters\front\helpdesk\read')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i>@lang('helpdesk.readDetail')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                        <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                        </li>
                        <li class=" breadcrumb-item">
                           <a href="{{ route('helpdesk.list', ['lang' => \App::getLocale()]) }}">@lang('helpdesk.Title')</a>
                        </li>
                        <li class="breadcrumb-item active">@lang('helpdesk.readDetail')</li>
                    </ul>
                </div>
            </div>
        </div>
    </header>

    <div class="outer">
        <div class="inner bg-container">
            <div class="card m-t-35">
                <div class="card-header bg-white">@lang('helpdesk.QsContent')</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="chat-conversation p-d-15">
                                <ul class="conversation-list" id="message">
                                    @if( $model->is_multisend == 0 ) <!-- if is CLIENT will show blue color -->
                                        <li class="clearfix odd" style="padding-bottom: 10px;">
                                            <div class="conversation-text" style="max-width: 80%;">
                                                <div class="ctext-wrap">
                                                    <h4>
                                                        @if(!empty($model->pic_1)) 
                                                        <a style="float: left;" target=_blank href="{{ route('amazon.read', ['folder' => 'helpdesk', 'filename' => $model->pic_1, 'lang' => \App::getLocale()]) }}"><i class="fa fa-paperclip text-danger" style="font-size: 20px"></i></a>
                                                        @endif
                                                        @if(!empty($model->pic_2)) 
                                                        <a style="float: left; padding-left: 5px;" target=_blank href="{{ route('amazon.read', ['folder' => 'helpdesk', 'filename' => $model->pic_2, 'lang' => \App::getLocale()]) }}"><i class="fa fa-paperclip text-danger" style="font-size: 20px"></i></a>
                                                        @endif
                                                        <b>@lang('helpdesk.you')</b>
                                                    </h4>
                                                    <h5> {{ $model->created_at }}</h5><br>
                                                    <p>
                                                        {{ $model->content }}
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                    @else <!-- if is CS will show white color -->
                                        <li class="clearfix" style="padding-bottom: 10px;">
                                            <div class="conversation-text" style="max-width: 80%;">
                                                <div class="ctext-wrap">
                                                    <h4>
                                                        @if(!empty($model->pic_1)) 
                                                        <a style="float: right;" target=_blank href="{{ route('amazon.read', ['folder' => 'helpdesk', 'filename' => $model->pic_1, 'lang' => \App::getLocale()]) }}"><i class="fa fa-paperclip text-danger" style="font-size: 20px"></i></a>
                                                        @endif
                                                        @if(!empty($model->pic_2)) 
                                                        <a style="float: right; padding-left: 5px;" target=_blank href="{{ route('amazon.read', ['folder' => 'helpdesk', 'filename' => $model->pic_2, 'lang' => \App::getLocale()]) }}"><i class="fa fa-paperclip text-danger" style="font-size: 20px"></i></a>
                                                        @endif
                                                        <b>@lang('helpdesk.cs')</b>
                                                    </h4>
                                                    <h5> {{ $model->created_at }}</h5><br>
                                                    <p>
                                                        {{ $model->content }}
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                    @endif
                                    
                                    @foreach($model2 as $comment)  <!-- show ticket and comment -->
                                      @if( empty($comment->cs_username) ) <!-- if is CLIENT will show blue color -->
                                            <li class="clearfix odd" style="padding-bottom: 10px;">
                                                <div class="conversation-text" style="max-width: 80%;">
                                                    <div class="ctext-wrap">
                                                        <h4>
                                                            @if(!empty($comment->pic_1)) 
                                                            <a style="float: left;" target=_blank href="{{ route('amazon.read', ['folder' => 'helpdesk', 'filename' => $comment->pic_1, 'lang' => \App::getLocale()]) }}"><i class="fa fa-paperclip text-danger" style="font-size: 20px"></i></a>
                                                            @endif
                                                            @if(!empty($comment->pic_2)) 
                                                            <a style="float: left; padding-left: 5px;" target=_blank href="{{ route('amazon.read', ['folder' => 'helpdesk', 'filename' => $comment->pic_2, 'lang' => \App::getLocale()]) }}"><i class="fa fa-paperclip text-danger" style="font-size: 20px"></i></a>
                                                            @endif
                                                            <b>@lang('helpdesk.you')</b>
                                                        </h4>
                                                        <h5> {{ $comment->created_at }}</h5><br>
                                                        
                                                        <p>
                                                            {{ $comment->comment }}
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>
                                      @else <!-- if is CS will show white color -->
                                            <li class="clearfix" style="padding-bottom: 10px;">
                                                <div class="conversation-text" style="max-width: 80%;">
                                                    <div class="ctext-wrap">
                                                        <h4>
                                                            @if(!empty($comment->pic_1)) 
                                                            <a style="float: right; padding-left: 5px;" target=_blank href="{{ route('amazon.read', ['folder' => 'helpdesk', 'filename' => $comment->pic_1, 'lang' => \App::getLocale()]) }}"><i class="fa fa-paperclip text-danger" style="font-size: 20px"></i></a>
                                                            @endif
                                                            @if(!empty($comment->pic_2)) 
                                                            <a style="float: right;" target=_blank href="{{ route('amazon.read', ['folder' => 'helpdesk', 'filename' => $comment->pic_2, 'lang' => \App::getLocale()]) }}"><i class="fa fa-paperclip text-danger" style="font-size: 20px"></i></a>
                                                            @endif
                                                            <b>@lang('helpdesk.cs')</b>
                                                        </h4>
                                                        <h5> {{ $comment->created_at }}</h5><br>
                                                        <p>
                                                           {{ $comment->comment }}
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>
                                      @endif
                                    @endforeach
                                </ul>

                                @if($model->status != "done")
                                    <div class="row">
                                        <div class="col-12 m-b-15">
                                            <div class="input-group chat_btn">
                                                <input type="text" class="form-control chat-input custom_textbox" id="comment" name="comment" placeholder="@lang('helpdesk.yourQs')">
                                                <span class="input-group-prepend">
                                                    <button class="btn btn-primary waves-effect waves-light" ype="submit" onclick="postComment()"><i class="fa fa-paper-plane text-white" aria-hidden="true"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-4">
                                            <a href="#" data-toggle="tooltip"  onclick="uploadFront()">
                                                <i class="fa fa-upload"></i>&nbsp; @lang('settings.plzChose')
                                            </a>
                                            <input type="file" name="inputIDfront"  class="form-control" id="inputIDfront"  hidden="" onchange="showFrontMsg()">
                                            &nbsp;
                                            <span id="showPic1Name"></span>
                                            <span id="showFront"></span>
                                        </div>
                                        <div class="col-lg-4">
                                            <a href="#" data-toggle="tooltip"  onclick="uploadBack()">
                                                <i class="fa fa-upload"></i>&nbsp; @lang('settings.plzChose')
                                            </a>
                                            <input type="file" name="inputIDback"  class="form-control" id="inputIDback"  hidden="" onchange="showBackMsg()">
                                            &nbsp;
                                            <span id="showPic2Name"></span>
                                            <span id="showBack"></span>
                                        </div>
                                    </div>
                                @else
                                    @if($model->is_complain == 0 && $model->is_rate == 0)
                                        <script>
                                            window.onload = function() {
                                              rate();
                                            };
                                        </script>
                                        <div class="row">
                                            <div class="col-12 m-b-15">
                                                <div class="input-group chat_btn">
                                                    <span class="input-group-prepend" style="margin: auto;">
                                                        <a class="btn btn-warning glow_button"  onclick="rate()" ><i class="fa fa-star"></i> @lang('helpdesk.rate_button')</a>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer_scripts')
<script type="text/javascript">
    $(document).ready( function () {
            $('li').last().addClass('active-li').focus();
    });
    </script>
    <script type="text/javascript" src="{{asset('assets/vendors/moment/js/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/slimscroll/js/jquery.slimscroll.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/countUp.js/js/countUp.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/swiper/js/swiper.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/pages/widget2.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/bootstraprating/js/star-rating.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/rateyo/js/jquery.rateyo.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/sweetalert/js/sweetalert2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/pages/ratings.js')}}"></script>

    <!--Plugin scripts-->
    <script type="text/javascript">
        $(document).ready( function () {
             $('#message').animate({scrollTop: $('#message')[0].scrollHeight}, "slow");
        });

        function uploadFront(){
            $('#inputIDfront').click();
        }
        function uploadBack(){
            $('#inputIDback').click();
        }
        function showFrontMsg(){
            if(document.getElementById("inputIDfront").files.length == 1){
                document.getElementById("showFront").innerHTML = "<i class='fa fa-check text-success'></i>";
                document.getElementById("showPic1Name").innerHTML = document.getElementById("inputIDfront").files[0].name;
            }
        }
        function showBackMsg(){
            if(document.getElementById("inputIDback").files.length == 1){
                document.getElementById("showBack").innerHTML = "<i class='fa fa-check text-success'></i>";
                document.getElementById("showPic2Name").innerHTML = document.getElementById("inputIDback").files[0].name;
            }
        }
        function postComment() {
            var reply = "{{$model->is_reply}}";
            if( reply == "0"){
                notiAlert(2, "@lang('helpdesk.warning')", '{{\App::getLocale()}}');
                return false;
            }
            var obj = {comment: "comment"};
            var final = { Id : obj};
            var Id = JSON.stringify(final);
            var ans = checkValue(Id);

            if(ans!=0){
                return false;
            }

            var disable = loadingDisable();

            var comment = $('#comment').val();
            var helpdesk_id = "{{ $model->id }}";
            var image1 = $('#inputIDfront')[0].files[0];
            var image2 = $('#inputIDback')[0].files[0];


            var url = "{{ route('helpdesk.postcreate', ['lang' => \App::getLocale()]) }}";
            var obj = {comment:comment, helpdesk_id:helpdesk_id };
            var final = { Data : obj};
            var data = JSON.stringify(final);

            var form = new FormData();
            form.append('image1', image1);
            form.append('image2', image2);
            form.append('allData', data);


            $.ajax({
                url     : url,
                data    : form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                headers:
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success : function(response){
                    disable.out();
                    if(response.result == 0){

                        swal({
                            title: response.showTittle,
                            text: response.showText,
                            type: 'success',
                            confirmButtonColor: '#4fb7fe',
                            allowOutsideClick: false
                        }).then(function(){ 
                            location.reload();
                        });
                    }else{

                        swal({
                            title: response.showTittle,
                            text: response.showText,
                            type: 'error',
                            confirmButtonColor: '#4fb7fe',
                            

                        }).done();
                    }   
                }
            });
        }
        function rate(){
            var rateT = "@lang('helpdesk.rate')";
            var complainT = "@lang('helpdesk.complain')";
            var star = '<div class=starability-slot style="margin: auto;"><input type=radio id=slot-rate5 name=rating value="5"  /><label for="slot-rate5" title="" aria-label="Amazing, 5 stars" onclick=postStar(1)>5 stars</label><input type="radio" id="slot-rate4" name="rating" value="4" /><label for="slot-rate4" title="" aria-label="Very good, 4 stars" onclick=postStar(2)>4 stars</label><input type="radio" id="slot-rate3" name="rating" value="3"/><label for="slot-rate3" title="" aria-label="Average, 3 stars" onclick=postStar(3)>3 stars</label><input type="radio" id="slot-rate2" name="rating" value="2" /><label for="slot-rate2" title="" aria-label="Not good, 2 stars" onclick=postStar(4)>2 stars</label><input type="radio" id="slot-rate1" name="rating" value="1" /><label for="slot-rate1" title="" aria-label="Terrible, 1 star" onclick=postStar(5)>1 star</label></div>';
            var complain = '<textarea id="complain" name="complain" class="form-control" cols="50" rows="5" placeholder=""></textarea>';
            swal({
                title: "@lang('helpdesk.plzrateMe')",
                text: '',
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: '#00c0ef',
                cancelButtonColor: '#ff8080',
                confirmButtonText: '<i class="fa fa-thumbs-up"></i> '+rateT+'',
                cancelButtonText: '<i class="fa fa-thumbs-down"></i> '+complainT+'',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger'
            }).then(function () {
                swal({
                    title: "@lang('helpdesk.rateMe')",
                    html: star,
                    confirmButtonColor: '#00c0ef',
                    allowOutsideClick: false
                }).then(function() {
                    postStar_ok();
                });
            }, function (dismiss) {

                if(dismiss==="cancel"){
                    swal({
                    title: "@lang('helpdesk.complainT')",
                    html: complain,
                    confirmButtonColor: '#00c0ef',
                    allowOutsideClick: false
                }).then(function() {
                    postComplain();  
                });

                }
                
            });
            return false;
        }
        function postStar($id){
            if(!$id){
                var star = 5;
            }else{
                var star = $id;
            }
            var helpdesk_id = "{{ $model->id }}";
            var url = "{{ route('helpdesk.postRate', ['lang' => \App::getLocale()]) }}";
            var obj = {star:star, helpdesk_id:helpdesk_id };
            var final = { Data : obj};
            var data = JSON.stringify(final);

            $.ajax({
                url     : url,
                data    : {

                    allData : data,
                },
                type: 'POST',
                headers:
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success : function(response){
                    
                    if(response.result == 0){

                        notiAlert(0, response.showText, '{{\App::getLocale()}}');
                        
                    }else{

                        notiAlert(1, response.showTittle, '{{\App::getLocale()}}');
                        
                    }   
                }
            });
        }
        function postStar_ok($id){
            if(!$id){
                var star = 5;
            }else{
                var star = $id;
            }
            var helpdesk_id = "{{ $model->id }}";
            var url = "{{ route('helpdesk.postRate', ['lang' => \App::getLocale()]) }}";
            var obj = {star:star, helpdesk_id:helpdesk_id };
            var final = { Data : obj};
            var data = JSON.stringify(final);

            $.ajax({
                url     : url,
                data    : {

                    allData : data,
                },
                type: 'POST',
                headers:
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success : function(response){
                    
                    if(response.result == 0){

                        notiAlert(0, response.showText, '{{\App::getLocale()}}');
                        location.reload();
                    }else{

                        notiAlert(1, response.showTittle, '{{\App::getLocale()}}');
                        location.reload();
                    }   
                }
            });
        }
        function postComplain(){
            var complain = $('#complain').val();
            if(!complain){
                notiAlert(1, "@lang('helpdesk.FailComplain')", '{{\App::getLocale()}}');
                return false;
            }
            var helpdesk_id = "{{ $model->id }}";
            var url = "{{ route('helpdesk.postComplain', ['lang' => \App::getLocale()]) }}";
            var obj = {complain:complain, helpdesk_id:helpdesk_id };
            var final = { Data : obj};
            var data = JSON.stringify(final);

            $.ajax({
                url     : url,
                data    : {

                    allData : data,
                },
                type: 'POST',
                headers:
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success : function(response){
                    
                    if(response.result == 0){
                       
                        notiAlert(0, response.showTittle, '{{\App::getLocale()}}');
                        location.reload();
                        
                    }else{
                        
                        notiAlert(1, response.showTittle, '{{\App::getLocale()}}');
                        location.reload();

                    }   
                }
            });
        }
    </script>

    <!--End of Page level scripts-->

@stop
