<?php
    use App\Models\Package;
    use App\Repositories\MemberRepository;
    $packages = Package::where('status', '!=', 0)->get();
    
    $memberRepo = new MemberRepository;
    $member = $memberRepo->findByUsername(trim($user->username));
    $member->load('wallet');
    $member->load('detail');
    //$member->load('shares');
    $member->load('package');
    ?>

@extends('front.app')

@section('title')
  @lang('helpdesk.addSucess') | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">@lang('breadcrumbs.front')</a></li>
    <li><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
    <li><a href="{{ route('helpdesk.list', ['lang' => \App::getLocale()]) }}">@lang('helpdesk.Title')</a></li>
    <li class="active">@lang('helpdesk.addSucess')</li>
  </ul>
@stop

@section('content')
  <main>
    @include('front.include.sidebar')
    <div class="main-container">
      @include('front.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section>
          <div class="page-header">
            <h1><i class="md md-done-all"></i> @lang('helpdesk.SucessTitle')</h1>
          </div>

          <div class="row m-b-40">
            <div class="col-md-8">
              <div class="well white text-center">
                <h1 class="theme-text text-uppercase w300">@lang('helpdesk.SucessWait')</h1>
                <h3>@lang('helpdesk.SucessWaitcs')</h3>
                <a href="{{ route('helpdesk.list', ['lang' => \App::getLocale()]) }}" class="btn btn-primary btn-block m-t-50">
                  <i class="md md-navigate-before"></i> @lang('helpdesk.SucessBack')
                </a>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>
@stop
