<?php
    use App\Repositories\MemberRepository;
    $memberRepo = new MemberRepository;
    $member = $memberRepo->findByUsername(trim($user->username));

    $date = '2018-10-01';
    $up = [];
    $down = [];
    $own = [];
    $newadd = [];
    $default = [];
    $latestadd = 0;
    $nextstage = 0;
    $add = 0;
    $downlineAry = \DB::table('Member_Network')->where('parent_id', $member->id)->where('my_level', 1)->get(['member_id']);

    if(isset($downlineAry)){
        foreach ($downlineAry as $item) {
            $downline[] = $item->member_id;
        }

        for ($i=0; $i < 9; $i++) { 
            $selectedDate = date('Y-m-01', strtotime($date."+".$i."months"));
            $up[$selectedDate] = [];
            $down[$selectedDate] = [];
            $own[$selectedDate] = [];
            $newadd[$selectedDate] = [];

            if(isset($downline)){
                $conditions = \DB::table('Member_Sales_Summary')->join('Member', 'Member_Sales_Summary.member_id', '=', 'Member.id')->join('users', 'Member.username', '=', 'users.username')->whereIn('Member_Sales_Summary.member_id', $downline)->where('Member_Sales_Summary.date', $selectedDate)->get(['users.first_name', 'Member_Sales_Summary.member_id', 'Member_Sales_Summary.conditions2', 'Member_Sales_Summary.rank', 'Member_Sales_Summary.date']);
                
                if(count($conditions) > 0) $add = 0;
                
                foreach ($conditions as $item) {
                    if($item->rank == 'MIB' || $item->rank == 'PIB'){
                        if(in_array(6, json_decode($item->conditions2)) || in_array(7, json_decode($item->conditions2))) {
                            $up[$selectedDate][] = $item->first_name." -- ".$item->rank;

                            if($item->date == '2018-10-01') $default[] = $item->member_id;

                            if(!in_array($item->member_id, $default)) {
                                $newadd[$selectedDate][] = $item->first_name." -- ".$item->rank;
                                $add++;
                            }
                        }
                        else $down[$selectedDate][] = $item->first_name." -- ".$item->rank;
                    }
                }
            }

            $ownconditions = \DB::table('Member_Sales_Summary')->join('Member', 'Member_Sales_Summary.member_id', '=', 'Member.id')->join('users', 'Member.username', '=', 'users.username')->where('Member_Sales_Summary.member_id', $member->id)->where('Member_Sales_Summary.date', $selectedDate)->get(['Member_Sales_Summary.conditions2', 'Member_Sales_Summary.rank']);
            foreach ($ownconditions as $item) {
                if($item->rank == 'MIB' || $item->rank == 'PIB'){
                    if(in_array(6, json_decode($item->conditions2)) || in_array(7, json_decode($item->conditions2))) $own[$selectedDate][] = '<span style="color:#50e500;">'.\Lang::get('common.network').' -- '.$item->rank.'</span>';
                    else $own[$selectedDate][] = '<span style="color:#ffa500;">'.\Lang::get('common.nonetwork').' -- '.$item->rank.'</span>';
                }else{
                    $own[$selectedDate][] = '<span style="color:#ff0000;">'.\Lang::get('common.noqualified').'</span>';
                }
            }

            $latestadd = $add;
        }
    }

    $nextstage = 6;
?>

@extends('front.app')

@section('title')
    @lang('sidebar.event') | {{ config('app.name') }}
@stop

@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/swiper/css/swiper.min.css')}}"/>
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/widgets.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/animate/css/animate.min.css')}}" />
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <div class="form-group row">
        <div class="col-lg-2 text-lg-right">
            <label for="bookA" class="col-form-label"></label>
        </div>
        <div class="col-12 col-lg-8 m-t-35">
            <div class="section_border">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="swiper-container widget_swiper">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <img src="{{asset('assets/img/memo/memo1_'.\App::getLocale().'.jpg')}}" alt="Image missing" class="img-fluid"/>
                                </div>
                                <div class="swiper-slide">
                                    <img src="{{asset('assets/img/memo/memo2_'.\App::getLocale().'.jpg')}}" alt="Image missing" class="img-fluid"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="p-d-10 bg-white">
                            <a href="#" onclick="getAnnouncementDetails()"><h4 class="text-info" style="text-align: center;"><i class="fa fa-search-plus"></i> @lang('common.rules')</h4></a>
                            <h4 class="m-t-15">@lang('common.progress'): </h4>
                            <div class="progress" style="height: 20px !important;">
                                <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: {{$latestadd/$nextstage*100}}%" aria-valuemin="0" aria-valuemax="100">
                                    <span style="width: 90%; position: absolute; color: #000;">{{$latestadd}} / {{$nextstage}}</span>
                                </div>
                            </div>
                            <h4 class="m-t-15">@lang('common.earn'): &nbsp;&nbsp;
                                @if($latestadd >= 6)
                                    <img src="{{asset('assets/img/flight_ticket.png')}}" alt="Image missing" class="img-fluid" style="transform: rotate(-40deg);" />
                                    <img src="{{asset('assets/img/flight_ticket.png')}}" alt="Image missing" class="img-fluid" style="transform: rotate(-40deg);" />
                                @elseif($latestadd < 3)
                                    <strong>0</strong>
                                @else
                                    <img src="{{asset('assets/img/flight_ticket.png')}}" alt="Image missing" class="img-fluid" style="transform: rotate(-40deg);" />
                                @endif
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col" style="overflow: auto;">
                    <table class="table table-striped table-bordered table-hover dataTable no-footer" style="text-align: center; width: 100%;" role="grid">
                        <thead>
                            <tr>
                                <th></th>
                                <th>10 / 2018</th>
                                <th>11 / 2018</th>
                                <th>12 / 2018</th>
                                <th>01 / 2019</th>
                                <th>02 / 2019</th>
                                <th>03 / 2019</th>
                                <th>04 / 2019</th>
                                <th>05 / 2019</th>
                                <th>06 / 2019</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>@lang('common.ownnetwork')</td>
                                @foreach($own as $item)
                                    <td>
                                    @if(count($item) > 0)
                                        @foreach($item as $detail)
                                        {!! $detail !!}<br>
                                        @endforeach
                                    @else
                                        -
                                    @endif
                                    </td>
                                @endforeach
                            </tr>
                            <tr>
                                <td><label style="color:#50e500;">@lang('common.network')</label></td>
                                @foreach($up as $item)
                                    <td>
                                    @if(count($item) > 0)
                                        @foreach($item as $detail)
                                        {{ $detail }}<br>
                                        @endforeach
                                    @else
                                        -
                                    @endif
                                    </td>
                                @endforeach
                            </tr>
                            <tr>
                                <td><label style="color:#ffa500;">@lang('common.nonetwork')</label></td>
                                @foreach($down as $item)
                                    <td>
                                    @if(count($item) > 0)
                                        @foreach($item as $detail)
                                        {{ $detail }}<br>
                                        @endforeach
                                    @else
                                        -
                                    @endif
                                    </td>
                                @endforeach
                            </tr>
                            <tr>
                                <td>@lang('common.newadd')</td>
                                @foreach($newadd as $item)
                                    <td>
                                    @if(count($item) > 0)
                                        @foreach($item as $detail)
                                        {{ $detail }}<br>
                                        @endforeach
                                    @else
                                        -
                                    @endif
                                    </td>
                                @endforeach
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('front.include.modal')
@stop
@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/js/pages/modals.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/moment/js/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/slimscroll/js/jquery.slimscroll.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/countUp.js/js/countUp.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/swiper/js/swiper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/pages/widget2.js')}}"></script>
<script>
    $(document).ready( function () {
    });

    function getAnnouncementDetails() {
        var disable = loadingDisable();
        var modelContent = '';
        var ajaxUrl = "{{ route('announcement.read', ['id' =>'97', 'lang' => \App::getLocale()]) }}";

        $.ajax({
            url     :   ajaxUrl,
            method  :   "get",
            headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
            success : function(response){
                if(response.status == 0){
                    if("{{ $lang }}" == "en"){
                        modelContent =  '<div style="font-size: 28px !important;">'+response.model.title_en+'</div>';
                        modelContent += '<div style="font-size: 15px !important;font-weight: 100;">'+response.model.created_at+'</div>';
                        modelContent += '<hr><br/>';
                        modelContent += '<div style="font-size: 15px !important;">'+response.model.content_en+'</div>';
                    }else{
                        modelContent =  '<div style="font-size: 28px !important;">'+response.model.title_chs+'</div>';
                        modelContent += '<div style="font-size: 15px !important;font-weight: 100;">'+response.model.created_at+'</div>';
                        modelContent += '<hr><br/>';
                        modelContent += '<div style="font-size: 15px !important;">'+response.model.content_chs+'</div>';
                    }
                    $('#modalContent').empty().append(modelContent);
                    $('#modalZoomIn').click();
                }else{
                    notiAlert(response.status, response.msg, '{{\App::getLocale()}}');
                }
                disable.out();
            }
        });
    }
</script>
@stop