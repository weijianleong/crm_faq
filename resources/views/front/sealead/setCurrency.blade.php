@extends('front.app')

@section('title')
    @lang('sealead.currencySetting') | {{ config('app.name') }}
@stop

@section('header_styles')
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/calendar_custom.css')}}"/>
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li class="active"><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
    </ul>
@stop

@section('main_content')
    @include('front.include.header')
    <div id="content" class="bg-container">

        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-lg-5">
                        <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('sealead.currencySetting')</h4>
                    </div>
                    <div class="col-lg-7">
                        <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                            <li class=" breadcrumb-item">
                                <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                                    <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                                </a>
                            </li>
                            <li class="breadcrumb-item active">@lang('sealead.currencySetting')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <div class="outer">
            <div class="inner bg-container">
                <div class="card m-t-35">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12 m-t-25">
                                <div>
                                    <ul class="nav nav-inline view_user_nav_padding" id="content-tab">
                                        <li class="nav-item card_nav_hover">
                                            <a class="nav-link active" href="#bank" id="home-tab"
                                               data-toggle="tab" aria-expanded="true">@lang('sealead.currencySetting')</a>
                                        </li>

                                        <li class="nav-item card_nav_hover">
                                            <a class="nav-link " href="#IDpage" id="hats-tab"
                                               data-toggle="tab" aria-expanded="true"></a>
                                        </li>


                                    </ul>

                                    <div id="clothing-nav-content" class="tab-content m-t-10">
                                        <div role="tabpanel" class="tab-pane fade show active" id="bank">

                                            <fieldset>

                                                <!-- select currency-->
                                                <div class="form-group row">
                                                    <div class="col-lg-3 text-lg-right">
                                                        <label for="email3" class="col-form-label">@lang('sealead.currency')</label>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="input-group input-group-prepend">
                                                                <span class="input-group-text border-right-0 rounded-left">
                                                                    <i class="fa  fa-money"></i>
                                                                </span>
                                                            @if(empty($member->detail->currency_seanet))
                                                                <select class="form-control hide_search" tabindex="7" name="currency" id="currency">
                                                                    <option value="0">@lang('withdraw.optionSelect')</option>
                                                                    @foreach($currency as  $currencys )
                                                                        <option value="{{$currencys->id}}">{{$currencys->currency}}</option>
                                                                    @endforeach
                                                                </select>
                                                            @else
                                                                <input type="text" id="currency" name="currency" value="{{ $currency->currency  }}" class="form-control" placeholder="" readonly>
                                                            @endif

                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- last name-->
                                                <div class="form-group row">
                                                    <div class="col-lg-9 ml-auto">
                                                        @if(empty($member->detail->currency_seanet))
                                                            <button class="btn btn-primary layout_btn_prevent" onclick="UpdateCurrency()">@lang('common.submit')</button>
                                                            <button class="btn btn-secondary layout_btn_prevent">@lang('common.cancel')</button>
                                                        @endif
                                                    </div>
                                                </div>
                                            </fieldset>

                                        </div>

                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer_scripts')

    <!--Plugin scripts-->
    <script type="text/javascript" src="{{asset('assets/vendors/bootstrap_calendar/js/bootstrap_calendar.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/pages/mini_calendar.js')}}"></script>
    <!--End of Page level scripts-->
    <script type="text/javascript">
        function UpdateCurrency(){

            var currency = document.getElementById("currency").value;

            if(currency == 0){
                notiAlert(2, "@lang('sealead.currency_wrong')", '{{\App::getLocale()}}');
                return false;
            }


            var url = "{{ route('sealead.updateCurrency', ['lang' => \App::getLocale()]) }}";
            var buttonDisplay = '{"Format":['+'{"confirmButtonText":'+'"@lang('common.confirm')"'+',"cancelButtonText":'+'"@lang('common.cancel')"'+',"setTittle":'+'"@lang('settings.secret')"'+',"error":'+'"@lang('error.securityPasswordError')"'+'}]}';
            var obj = {currency: currency};
            var final = { Data : obj};
            var data = JSON.stringify(final);
            swalAlert(url,buttonDisplay,data);

            return false;
        }
    </script>
@stop
