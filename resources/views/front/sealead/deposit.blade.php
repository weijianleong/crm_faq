@extends('front.app')

@section('title')
    @lang('sealead.deposit_title') | {{ config('app.name') }}
@stop

@section('header_styles')
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/calendar_custom.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/widgets.css')}}">
@stop

@section('main_content')
    @include('front.include.header')
    <div id="content" class="bg-container">

        @if( empty($member->detail->currency_seanet))
            <script>
                window.onload = function() {
                    remind();
                };
            </script>
        @else

            <header class="head">
                <div class="main-bar">
                    <div class="row no-gutters">
                        <div class="col-lg-5">
                            <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('sealead.deposit_title')</h4>
                        </div>
                        <div class="col-lg-7">
                            <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                                <li class=" breadcrumb-item">
                                    <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                                        <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                                    </a>
                                </li>
                                <li class="breadcrumb-item active">@lang('sealead.deposit_title')</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header>

            <div class="outer">
                <div class="inner bg-container">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            <div class="row justify-content-md-center">
                                <div class="col-sm-6 col-lg-3 media_max_991">
                                    <div class="icon_align" style="border-left: 5px solid #FFC800; box-shadow: 2px 4px 25px -8px #989898;">
                                        <div class="float-left">
                                            <p id="cash_point"  style="font-size: 25px; color: #FFC800; margin-bottom: 0px; margin-top: 5px">$ {{ number_format($member->wallet->cash_point,2) }}</p>
                                            <p style="font-size: 15px; color: #6c6c86;">@lang('common.cashTitle')</p>
                                        </div>
                                        <div class="text-right" style="margin: 5px 15px 11px"><i style="background-color: #FFC800; padding: 10px; border-radius: 70px; width: 50px; text-align: center; font-size: 30px; font-weight: bold;" class="fa"><img style="height: 20px; margin-bottom: 5px;" src="{{asset('assets/img/wallet.png')}}"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12 m-t-25">
                                    <div>
                                        <ul class="nav nav-inline view_user_nav_padding" id="content-tab">
                                            <li class="nav-item card_nav_hover">
                                                <a class="nav-link active" href="#bank" id="home-tab" data-toggle="tab" aria-expanded="true">@lang('sealead.deposit_title')</a>
                                            </li>
                                            <li class="nav-item card_nav_hover">
                                                <a class="nav-link " href="#IDpage" id="hats-tab" data-toggle="tab" aria-expanded="true"></a>
                                            </li>
                                        </ul>

                                        <div id="clothing-nav-content" class="tab-content m-t-10">
                                            <div role="tabpanel" class="tab-pane fade show active" id="bank">
                                                <fieldset>

                                                    <!-- amount-->
                                                    <div class="form-group row">
                                                        <div class="col-lg-3 text-lg-right">
                                                            <label for="email3" class="col-form-label">@lang('sealead.amount')</label>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="input-group input-group-prepend">
                                                                <span class="input-group-text border-right-0 rounded-left">
                                                                    <i class="fa  fa-dollar"></i>
                                                                </span>
                                                                <input id="amount" class="form-control" type="text" name="amount" maxlength="10" onkeypress="return validateFloatKeyPress(this, event);" onkeyup="showCurrency();" onpaste="return false;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- select currency-->
                                                    <div class="form-group row">
                                                        <div class="col-lg-3 text-lg-right">
                                                            <label for="email3" class="col-form-label">@lang('sealead.currency')</label>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="input-group input-group-prepend">
                                                                <span class="input-group-text border-right-0 rounded-left">
                                                                    <i class="fa  fa-dollar"></i>
                                                                </span>
                                                                @if(empty($member->detail->currency_seanet))
                                                                    <select class="form-control hide_search" tabindex="7" name="currency" id="currency" onchange="showCurrency()" disabled>
                                                                        <option value="0">@lang('withdraw.optionSelect')</option>
                                                                        @foreach($currency as  $currencys )
                                                                            <option value="{{$currencys->exchangeRate}}-{{$currencys->currency}}-{{$currencys->id}}">{{$currencys->currency}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                @else
                                                                    <input type="text" id="currency" name="currency" value="{{ $currency->currency  }}" class="form-control" placeholder="" readonly>
                                                                @endif
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <!-- user.name-->
                                                    <div class="form-group row">
                                                        <div class="col-lg-3 text-lg-right">
                                                            <label for="email3" class="col-form-label">@lang('sealead.currency_exchange')</label>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="input-group input-group-prepend">
                                                                <span class="input-group-text border-right-0 rounded-left">
                                                                    <i class="fa  fa-money"></i>
                                                                </span>
                                                                <input type="text" id="currency_value" name="currency_value" value="{{ $currency->exchangeRate * $seanet5 }}" class="form-control" placeholder="" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- user.name-->
                                                    <div class="form-group row">
                                                        <div class="col-lg-3 text-lg-right">
                                                            <label for="email3" class="col-form-label">@lang('sealead.total_amount')</label>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="input-group input-group-prepend">
                                                                <span class="input-group-text border-right-0 rounded-left">
                                                                    <i class="fa  fa-money"></i>
                                                                </span>
                                                                <input type="text" id="total_amount" name="total_amount" value="0.00" class="form-control" placeholder="" readonly>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!-- upload-->
                                                    <div class="form-group row">
                                                        <div class="col-lg-3 text-lg-right">
                                                            <label for="query2" class="col-form-label"></label>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <a href="#" data-toggle="tooltip"  onclick="uploadFront()">
                                                                <i class="fa fa-upload"></i>&nbsp; @lang('settings.plzChose')
                                                            </a>
                                                            <input type="file" name="inputIDfront"  class="form-control" id="inputIDfront"  hidden="" onchange="showFrontMsg()">
                                                            &nbsp;
                                                            <span id="showPic1Name"></span>
                                                            <span id="showFront"></span>
                                                        </div>
                                                    </div>
                                                    <!-- submit-->
                                                    <div class="form-group row" >
                                                        <div class="col-lg-9 ml-auto">
                                                            <button class="btn btn-primary layout_btn_prevent" onclick="postdeposit()" id="postBtn">@lang('common.submit')</button>
                                                            <button class="btn btn-secondary layout_btn_prevent">@lang('common.cancel')</button>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@stop

@section('footer_scripts')

    <!--Plugin scripts-->
    <script type="text/javascript" src="{{asset('assets/vendors/bootstrap_calendar/js/bootstrap_calendar.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/pages/mini_calendar.js')}}"></script>
    <!--End of Page level scripts-->
    <script type="text/javascript">
        function uploadFront(){
            $('#inputIDfront').click();
        }
        function showFrontMsg(){
            if(document.getElementById("inputIDfront").files.length == 1){
                document.getElementById("showFront").innerHTML = "<i class='fa fa-check text-success'></i>";
                document.getElementById("showPic1Name").innerHTML = document.getElementById("inputIDfront").files[0].name;
            }
        }
        function showCurrency() {
            var amount = document.getElementById("amount").value;
            var showTotal = amount * "{{ $currency->exchangeRate * $seanet5 }}";

            if(amount < 0 ){
                document.getElementById("total_amount").value = 0;
            }else{
                document.getElementById("total_amount").value = showTotal.toFixed(2);
            }
        }
        function postdeposit(){

            var amount = document.getElementById("amount").value;

            var obj = {amount: "amount"};
            var final = { Id : obj};
            var Id = JSON.stringify(final);
            var ans = checkValue(Id);

            if(ans!=0){
                return false;
            }

            var image1 = $('#inputIDfront')[0].files[0];
            if(!image1){

                notiAlert(2, "@lang('sealead.deposit_uploadfail')", '{{\App::getLocale()}}');
                return false;
            }

            var disable = loadingDisable();
            var url = "{{ route('sealead.postCreateDeposit', ['lang' => \App::getLocale()]) }}";
            var buttonDisplay = '{"Format":['+'{"confirmButtonText":'+'"@lang('common.confirm')"'+',"cancelButtonText":'+'"@lang('common.cancel')"'+',"setTittle":'+'"@lang('settings.secret')"'+',"error":'+'"@lang('error.securityPasswordError')"'+'}]}';
            var obj = {amount: amount};
            var final = { Data : obj};
            var data = JSON.stringify(final);

            var form = new FormData();
            form.append('image1', image1);
            form.append('allData', data);

            $.ajax({
                url     : url,
                data    : form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                headers:
                    {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                success : function(response){
                    disable.out();
                    if(response.result == 0){

                        swal({
                            title: response.showTittle,
                            text: response.showText,
                            type: 'success',
                            confirmButtonColor: '#4fb7fe',
                            allowOutsideClick: false
                        }).then(function(){
                            if(response.returnUrl) document.location.href=response.returnUrl;
                        });
                    }else{

                        swal({
                            title: response.showTittle,
                            text: response.showText,
                            type: 'error',
                            confirmButtonColor: '#4fb7fe',


                        }).done();
                    }
                }
            });
        }
        function remind(){
            swal({
                title: "@lang('sealead.systemAutoReplyTittle')",
                text: "@lang('sealead.systemAutoReplyDetail')",
                type: 'info',

                confirmButtonColor: '#00c0ef',
                cancelButtonColor: '#ff8080',
                confirmButtonText: ''+'ok'+'',

                confirmButtonClass: 'btn btn-success',
                allowOutsideClick: false

            }).then(function () {
                var url = "{{ route('sealead.setCurrency', ['lang' => \App::getLocale()]) }}";
                document.location.href=url;
            });
            return false;
        }

    </script>
@stop
