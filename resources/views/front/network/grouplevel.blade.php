<?php
    use App\Repositories\MemberRepository;
    $memberRepo = new MemberRepository;

    $member = $memberRepo->findByUsername(trim($user->username));
    $myusername = trim($user->username);
    
    $totalmember = DB::table('Member_Network')->where('parent_id', '=', $member->id)->count();
    
    //$totaltx = DB::table('Member_Network')->join('Member_Sales', 'Member_Network.member_id', '=', 'Member_Sales.member_id')->where('parent_username', '=', $myusername)->sum('totaltransfer');
    
    //$totalb = DB::table('Member_Network')->join('Member_Sales', 'Member_Network.member_id', '=', 'Member_Sales.member_id')->where('parent_username', '=', $myusername)->sum('balanceb');
    
    //$totalsales = $totaltx;
    
    // $totalsales = DB::table('Member')->join('Member_Wallet_Statement_Fund', 'Member.id', '=', 'Member_Wallet_Statement_Fund.member_id')->where('direct_id', '=', $member->id)->where('action_type', '=', 'Transfer to MT5')->where('Member_Wallet_Statement_Fund.created_at', '>', '2018-04-01 05:00:00')->where('Member_Wallet_Statement_Fund.created_at', '<=', '2018-05-01 06:00:00')->sum('b_amount');
    
    $totalsales1 = DB::table('Member_Network')->join('Member_Wallet_Statement_Fund', 'Member_Network.member_id', '=', 'Member_Wallet_Statement_Fund.member_id')->where('parent_id', '=', $member->id)->where('action_type', '=', 'Transfer to MT5')->where('Member_Wallet_Statement_Fund.created_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('Member_Wallet_Statement_Fund.created_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->sum('b_amount');
    
    $totalsales2 = DB::table('Member_Network')->join('Member_Blacklist', 'Member_Network.member_id', '=', 'Member_Blacklist.member_id')->where('parent_id', '=', $member->id)->where('Member_Blacklist.blacklist_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('Member_Blacklist.blacklist_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->where('b_book', '<>', 0)->sum('b_book');
    
    $totalsales = $totalsales1 - ($totalsales2/2);
    
    $totaltransferx1 = DB::table('Member_Blacklist')->where('member_id', '=', $member->id)->where('Member_Blacklist.blacklist_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('Member_Blacklist.blacklist_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->where('b_book', '<>', 0)->sum('b_book');
    
    $totaltransferx2 = \DB::table('Member_Wallet_Statement_Fund')->where('member_id', '=',$member->id)->where('action_type', '=', 'Transfer to MT5')->where('created_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('created_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->sum('b_amount');
    
    $transferamountx1 = ($totaltransferx2 * 2) - $totaltransferx1;

    $investtrfx = 0;
    $investcoin = 0;
    $investmargin = 0;

    $investtrfx = \DB::table('Member_Account_Fund')->where('member_id', $member->id)->sum('balanceB');
    $coinWallet = \DB::table('Capx_Member_Coin')->where('member_id', $member->id)->first();

    $coin = \DB::table('Capx_Coin')->get();
    foreach ($coin as $coinData) {
        $symbol = $coinData->symbol;
        $investcoin += $coinData->price * $coinWallet->$symbol;
    }

    $margin = \DB::table('Capx_Margin')->get();
    foreach ($margin as $marginData) {
        $symbol = $marginData->symbol;
        $investmargin += $marginData->current_price * $coinWallet->$symbol;
    }

    $totalinvest = $investtrfx * 3 + $investcoin + $investmargin;
    
    $ranking = \DB::table('Member_Sales_Summary')->where('member_id', $member->id)->orderBy('date', 'DESC')->first();
?>
@extends('front.app')

@section('title')
@lang('unilevel.title2') | {{ config('app.name') }}
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/widgets.css')}}">
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('unilevel.title2')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('unilevel.title2')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">

                    @if(isset($ranking))
                    @if($ranking->rank == 'MIB' || $ranking->rank == 'PIB' || in_array($user->username, config('member.bypassNetwork')))
                    <div class="row user_widget">
                        <div class="col-12 col-sm-6 col-xl m-t-35">
                            <div class="bg-danger b_r_5" style="padding: 10px;">
                                <div>
                                    <div class="user_wid_font"><i class="fa fa-user-o"></i> {{ number_format($totalmember) }}</div>
                                    <div style="text-align: center;">@lang('unilevel.totalmembergroup')</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-xl m-t-35">
                            <div class="bg-success b_r_5" style="padding: 10px;">
                                <div>
                                    <div class="user_wid_font"><i class="fa fa-dollar"></i> {{ number_format($totalsales*2,2) }}</div>
                                    <div style="text-align: center;">@lang('unilevel.totaltransfergroup')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endif
                    <div class="row user_widget">
                        <div class="col-12 col-sm-6 col-xl m-t-35">
                            <div class="bg-warning b_r_5" style="padding: 10px;">
                                <div>
                                    <div class="user_wid_font"><i class="fa fa-dollar"></i> {{ number_format($totalinvest,2) }}</div>
                                    <div style="text-align: center;">@lang('misc.peinvest')</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-xl m-t-35">
                            <div class="bg-info b_r_5" style="padding: 10px;">
                                <div>
                                    <div class="user_wid_font"><i class="fa fa-dollar"></i> {{ number_format($transferamountx1,2) }}</div>
                                    <div style="text-align: center;">@lang('misc.pesales')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop