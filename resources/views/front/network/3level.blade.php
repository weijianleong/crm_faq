<?php
    use App\Repositories\MemberRepository;
    $memberRepo = new MemberRepository;
    $member = $memberRepo->findByUsername(trim($user->username));

    $level1 = \DB::table('Member_Network')->where('parent_id', $member->id)->where('my_level', 1)->get(['member_id']);
    if(count($level1) > 0){
        $totalmember1 = count($level1);
        foreach ($level1 as $item) {
            $level1MemberId[] = $item->member_id;
        }
        $level1Account = \DB::table('Member_Account_Fund')->whereIn('member_id', $level1MemberId)->get(['bookA']);

        if(count($level1Account) > 0){
            foreach ($level1Account as $item) {
                $level1AccountId[] = $item->bookA;
            }
            $totallotsize1 = \DB::connection('mysql2')->table('historyposition')->whereIn('Login', $level1AccountId)->where('CloseTime', '>=', config('misc.selectedMonthStart'))->where('CloseTime', '<', config('misc.selectedMonthEnd'))->sum('Volume');
            $totallotsize1 = $totallotsize1 / 10000;
        }else{
            $totallotsize1 = 0;
        }

        $totaltransfer1 = \DB::table('Member_Blacklist')->whereIn('member_id', $level1MemberId)->where('Member_Blacklist.blacklist_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('Member_Blacklist.blacklist_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->where('b_book', '<>', 0)->sum('b_book');
        $totaltransfer2 = \DB::table('Member_Wallet_Statement_Fund')->whereIn('member_id', $level1MemberId)->where('action_type', '=', 'Transfer to MT5')->where('created_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('created_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->sum('b_amount');
        $totaltransfer1x =  ($totaltransfer2 * 2) - $totaltransfer1;
    }else{
        $totalmember1 = 0;
        $totallotsize1 = 0;
        $totaltransfer1x = 0;
    }

    $level2 = \DB::table('Member_Network')->where('parent_id', $member->id)->where('my_level', 2)->get(['member_id']);
    if(count($level2) > 0){
        $totalmember2 = count($level2);
        foreach ($level2 as $item) {
            $level2MemberId[] = $item->member_id;
        }
        $level2Account = \DB::table('Member_Account_Fund')->whereIn('member_id', $level2MemberId)->get(['bookA']);

        if(count($level2Account) > 0){
            foreach ($level2Account as $item) {
                $level2AccountId[] = $item->bookA;
            }
            $totallotsize2 = \DB::connection('mysql2')->table('historyposition')->whereIn('Login', $level2AccountId)->where('CloseTime', '>=', config('misc.selectedMonthStart'))->where('CloseTime', '<', config('misc.selectedMonthEnd'))->sum('Volume');
            $totallotsize2 = $totallotsize2 / 10000;
        }else{
            $totallotsize2 = 0;
        }

        $totaltransfer1 = \DB::table('Member_Blacklist')->whereIn('member_id', $level2MemberId)->where('Member_Blacklist.blacklist_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('Member_Blacklist.blacklist_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->where('b_book', '<>', 0)->sum('b_book');
        $totaltransfer2 = \DB::table('Member_Wallet_Statement_Fund')->whereIn('member_id', $level2MemberId)->where('action_type', '=', 'Transfer to MT5')->where('created_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('created_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->sum('b_amount');
        $totaltransfer2x =  ($totaltransfer2 * 2) - $totaltransfer1;
    }else{
        $totalmember2 = 0;
        $totallotsize2 = 0;
        $totaltransfer2x = 0;
    }

    $level3 = \DB::table('Member_Network')->where('parent_id', $member->id)->where('my_level', 3)->get(['member_id']);
    if(count($level3) > 0){
        $totalmember3 = count($level3);
        foreach ($level3 as $item) {
            $level3MemberId[] = $item->member_id;
        }
        $level3Account = \DB::table('Member_Account_Fund')->whereIn('member_id', $level3MemberId)->get(['bookA']);

        if(count($level3Account) > 0){
            foreach ($level3Account as $item) {
                $level3AccountId[] = $item->bookA;
            }
            $totallotsize3 = \DB::connection('mysql2')->table('historyposition')->whereIn('Login', $level3AccountId)->where('CloseTime', '>=', config('misc.selectedMonthStart'))->where('CloseTime', '<', config('misc.selectedMonthEnd'))->sum('Volume');
            $totallotsize3 = $totallotsize3 / 10000;
        }else{
            $totallotsize3 = 0;
        }

        $totaltransfer1 = \DB::table('Member_Blacklist')->whereIn('member_id', $level3MemberId)->where('Member_Blacklist.blacklist_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('Member_Blacklist.blacklist_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->where('b_book', '<>', 0)->sum('b_book');
        $totaltransfer2 = \DB::table('Member_Wallet_Statement_Fund')->whereIn('member_id', $level3MemberId)->where('action_type', '=', 'Transfer to MT5')->where('created_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('created_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->sum('b_amount');
        $totaltransfer3x =  ($totaltransfer2 * 2) - $totaltransfer1;
    }else{
        $totalmember3 = 0;
        $totallotsize3 = 0;
        $totaltransfer3x = 0;
    }

    $totallevelmember = $totalmember1 + $totalmember2 + $totalmember3;
    $totallevelsales = $totaltransfer1x + $totaltransfer2x + $totaltransfer3x;
    $totallevellotsize = $totallotsize1 + $totallotsize2 + $totallotsize3;
?>
@extends('front.app')

@section('title')
@lang('unilevel.title') | {{ config('app.name') }}
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/widgets.css')}}">
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('unilevel.title')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('unilevel.title')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="row user_widget">
                        <div class="col-12 col-sm-6 col-xl">
                            <div class="card m-t-35 cardSpecialhover" style="border: none; border-left: 5px solid #2F61D5; background-color: #fff; box-shadow: 2px 4px 25px -8px #989898;">
                                <div class="card-body p-t-10">
                                    <div class="m-t-15">
                                        <span style="background-color: rgb(47,97,213,0.15); color: #2F61D5; padding: 10px 20px; border-radius: 20px; position: absolute; right: 15px;">@lang('unilevel.level1')</span>
                                    </div>
                                    <div style="color: #25265E; font-size: 25px; margin-top: 60px;">@lang('unilevel.totalmember') : <span style="color: rgb(120,121,147,0.8);">{{ number_format($totalmember1) }}</span></div>
                                    <div style="color: #25265E; font-size: 25px;">@lang('unilevel.totallotsize') : <span style="color: rgb(120,121,147,0.8);">{{ number_format($totallotsize1,2) }}</span></div>
                                    <div style="color: #25265E; font-size: 25px;">@lang('unilevel.totaltransfer') : <span style="color: rgb(120,121,147,0.8);">{{ number_format($totaltransfer1x,2) }}</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-xl">
                            <div class="card m-t-35 cardSpecialhover" style="border: none; border-left: 5px solid #2F61D5; background-color: #fff; box-shadow: 2px 4px 25px -8px #989898;">
                                <div class="card-body p-t-10">
                                    <div class="m-t-15">
                                        <span style="background-color: rgb(47,97,213,0.15); color: #2F61D5; padding: 10px 20px; border-radius: 20px; position: absolute; right: 15px;">@lang('unilevel.level2')</span>
                                    </div>
                                    <div style="color: #25265E; font-size: 25px; margin-top: 60px;">@lang('unilevel.totalmember') : <span style="color: rgb(120,121,147,0.8);">{{ number_format($totalmember2) }}</span></div>
                                    <div style="color: #25265E; font-size: 25px;">@lang('unilevel.totallotsize') : <span style="color: rgb(120,121,147,0.8);">{{ number_format($totallotsize2,2) }}</span></div>
                                    <div style="color: #25265E; font-size: 25px;">@lang('unilevel.totaltransfer') : <span style="color: rgb(120,121,147,0.8);">{{ number_format($totaltransfer2x,2) }}</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-xl">
                            <div class="card m-t-35 cardSpecialhover" style="border: none; border-left: 5px solid #2F61D5; background-color: #fff; box-shadow: 2px 4px 25px -8px #989898;">
                                <div class="card-body p-t-10">
                                    <div class="m-t-15">
                                        <span style="background-color: rgb(47,97,213,0.15); color: #2F61D5; padding: 10px 20px; border-radius: 20px; position: absolute; right: 15px;">@lang('unilevel.level3')</span>
                                    </div>
                                    <div style="color: #25265E; font-size: 25px; margin-top: 60px;">@lang('unilevel.totalmember') : <span style="color: rgb(120,121,147,0.8);">{{ number_format($totalmember3) }}</span></div>
                                    <div style="color: #25265E; font-size: 25px;">@lang('unilevel.totallotsize') : <span style="color: rgb(120,121,147,0.8);">{{ number_format($totallotsize3,2) }}</span></div>
                                    <div style="color: #25265E; font-size: 25px;">@lang('unilevel.totaltransfer') : <span style="color: rgb(120,121,147,0.8);">{{ number_format($totaltransfer3x,2) }}</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row user_widget">
                        <div class="col-12 col-sm-6 col-xl">
                            <div class="card m-t-35 cardSpecialhover" style="border: none; border-left: 5px solid #2F61D5; background-color: #fff; box-shadow: 2px 4px 25px -8px #989898;">
                                <div class="card-body p-t-10">
                                    <div class="m-t-15">
                                        <span style="background-color: rgb(47,97,213,0.15); color: #2F61D5; padding: 10px 20px; border-radius: 20px; position: absolute; right: 15px;">@lang('unilevel.totalmember3level')</span>
                                    </div>
                                    <div style="color: #25265E; font-size: 25px; margin-top: 60px;"><span style="color: rgb(120,121,147,0.8);">{{ number_format($totallevelmember) }}</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-xl">
                            <div class="card m-t-35 cardSpecialhover" style="border: none; border-left: 5px solid #2F61D5; background-color: #fff; box-shadow: 2px 4px 25px -8px #989898;">
                                <div class="card-body p-t-10">
                                    <div class="m-t-15">
                                        <span style="background-color: rgb(47,97,213,0.15); color: #2F61D5; padding: 10px 20px; border-radius: 20px; position: absolute; right: 15px;">@lang('unilevel.totaltransfer3level')</span>
                                    </div>
                                    <div style="color: #25265E; font-size: 25px; margin-top: 60px;"><span style="color: rgb(120,121,147,0.8);">{{ number_format($totallevelsales,2) }}</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-xl">
                            <div class="card m-t-35 cardSpecialhover" style="border: none; border-left: 5px solid #2F61D5; background-color: #fff; box-shadow: 2px 4px 25px -8px #989898;">
                                <div class="card-body p-t-10">
                                    <div class="m-t-15">
                                        <span style="background-color: rgb(47,97,213,0.15); color: #2F61D5; padding: 10px 20px; border-radius: 20px; position: absolute; right: 15px;">@lang('unilevel.totallotsize3level')</span>
                                    </div>
                                    <div style="color: #25265E; font-size: 25px; margin-top: 60px;"><span style="color: rgb(120,121,147,0.8);">{{ number_format($totallevellotsize,2) }}</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop