<?php
    use App\Repositories\MemberRepository;
    $memberRepo = new MemberRepository;
    
    $member = $memberRepo->findByUsername(trim($user->username));
    
    if (\Input::has('id')) {
        $id = trim(\Input::get('id'));
        $sMember = \DB::table('Member')->where('id', $id)->first();
    } else {
        $id = $member->id;
        $sMember = $member;
    }
    
    if ($sMember->level <= $member->level) {
        $sMember = $member;
    }
    
    $checknetwork = \DB::table('Member_Network')->where('member_id', '=', $id)->where('parent_id', '=', $member->id)->first();
    
    if(count($checknetwork) == 0)
    {
        $sMember = $member;
    }
    
    $userdetail = \DB::table('users')->where('username', '=',$sMember->username)->first();
    
    $totaltransferx1 = \DB::table('Member_Blacklist')->where('member_id', '=', $sMember->id)->where('Member_Blacklist.blacklist_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('Member_Blacklist.blacklist_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->where('b_book', '<>', 0)->sum('b_book');
    
    $totaltransferx2 = \DB::table('Member_Wallet_Statement_Fund')->where('member_id', '=',$sMember->id)->where('action_type', '=', 'Transfer to MT5')->where('created_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('created_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->sum('b_amount');
    
    $transferamountx1 = ($totaltransferx2 * 2) - $totaltransferx1;

    $rank = \DB::table('Member_Sales')->where('member_id', '=',$sMember->id)->first();

    $investtrfx = 0;
    $investcoin = 0;
    $investmargin = 0;

    $investtrfx = \DB::table('Member_Account_Fund')->where('member_id', $sMember->id)->sum('balanceB');
    $coinWallet = \DB::table('Capx_Member_Coin')->where('member_id', $sMember->id)->first();

    $coin = \DB::table('Capx_Coin')->get();
    foreach ($coin as $coinData) {
        $symbol = $coinData->symbol;
        $investcoin += $coinData->price * $coinWallet->$symbol;
    }

    $margin = \DB::table('Capx_Margin')->get();
    foreach ($margin as $marginData) {
        $symbol = $marginData->symbol;
        $investmargin += $marginData->current_price * $coinWallet->$symbol;
    }

    $totalinvest = $investtrfx * 3 + $investcoin + $investmargin;

    $ranking = \DB::table('Member_Sales_Summary')->where('member_id', $member->id)->orderBy('date', 'DESC')->first();

    $rankDisplay = 0;
    if(isset($ranking)){
        if($ranking->rank == 'MIB' || $ranking->rank == 'PIB' || in_array($user->username, config('member.bypassNetwork'))) $rankDisplay = 1;
    }
?>
@extends('front.app')

@section('title')
@lang('unilevel.title') | {{ config('app.name') }}
@stop

@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/unilevel.css')}}"/>
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('unilevel.title')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('unilevel.title')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card m-t-35">
                        <div class="card-header bg-white"> @lang('unilevel.title')</div>
                        <div class="card-body p-t-10 m-t-25">
                            <div class="member">
                                @if($rankDisplay > 0)
                                @lang('unilevel.member')
                                <div class="form-group row form_inline_inputs_bot">
                                    <div class="col-lg-4">
                                        <div class="input-group">
                                            <input type="text" value="{{ $sMember->username }}" name="u" class="form-control" required="" id="username">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <button id="getUsernameUnilevel" type="button" class="btn btn-primary layout_btn_prevent btn-responsive form_inline_btn_margin-top">@lang('common.submit')</button>
                                    </div>
                                </div>
                                @endif
                                <div style="margin-bottom: 15px;">@lang('common.notes') : 
                                    <span style="padding: 3px 10px 3px 10px; margin-right: 5px; background-color: rgb(117,64,238,0.1); color: #8657f0; border-radius: 20px;">@lang('misc.grank')</span>
                                    <span style="padding: 3px 10px 3px 10px; margin-right: 5px; background-color: rgb(45,199,109,0.1); color: #2DC76D; border-radius: 20px;">@lang('misc.ginvest')</span>
                                    <span style="padding: 3px 10px 3px 10px; margin-right: 5px; background-color: rgb(255,112,82,0.1); color: #FF7052; border-radius: 20px;">@lang('misc.gsales')</span>
                                </div>
                                <div class="member__tree">
                                    <div class="member__tree--inner">
                                        <div class="member__tree--switcher">
                                            <i class="fa fa-th-list active list"></i>
                                            <i class="fa fa-th grid"></i>
                                        </div>
                                        <ul data-parent="0" class="act">
                                            <li>
                                                <div class="item">
                                                    @if (!empty($member->detail->profile_pic_64))
                                                    <img class="media-object img-thumbnail user-img rounded-circle unilevel_img" alt="User Picture" src="data:image/;base64,{{ $member->detail->profile_pic_64 }}" >
                                                    <!-- <img class="media-object img-thumbnail user-img rounded-circle unilevel_img" alt="User Picture" src="{{asset('assets/img/profile.png')}}"> -->
                                                    @else
                                                    <img class="media-object img-thumbnail user-img rounded-circle unilevel_img" alt="User Picture" src="{{asset('assets/img/profile.png')}}">
                                                    @endif
                                                    <div style="float: left;">
                                                        <span class="member__name" style="display: inline-block;">{{ $userdetail->first_name }}</span>
                                                        @if(isset($rank->rank))
                                                        <div style="display: inline-block; padding: 3px 10px 3px 10px; margin-bottom: 3px; background-color: rgb(117,64,238,0.1); color: #8657f0; border-radius: 20px; text-align: center;">{{ $rank->rank }}</div>
                                                        @endif
                                                        <a href="#" class="member__email">{{ $userdetail->username }}</a>
                                                    </div>
                                                    <div class="member__amount" style="float: right; margin-top: 0px">
                                                        <div style="padding: 3px 10px 3px 10px; margin-bottom: 3px; background-color: rgb(45,199,109,0.1); color: #2DC76D; border-radius: 20px; text-align: center;">${{ number_format($totalinvest,2) }}</div>
                                                        <div style="padding: 3px 10px 3px 10px; margin-bottom: 3px; background-color: rgb(255,112,82,0.1); color: #FF7052; border-radius: 20px; text-align: center;">${{ number_format($transferamountx1,2) }}</div>
                                                    </div>
                                                    <i class="fa fa-plus" data-id="{{ $id }}"></i>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul data-parent="{{ $id }}" id="{{ $id }}"></ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer_scripts')
<script type="text/javascript">
    var loadedId = [];
    var ranking = "{{ $rankDisplay }}";
    findChildTree("{{ $id }}");

    $(document).ready( function () {
        $("#getUsernameUnilevel").click(function(event){
            var disable = loadingDisable();
            $.ajax({
                url     :   "{{ route('member.getUnilevel', ['lang' => \App::getLocale()]) }}",
                method  :   "post",
                data    :   {
                                username : $("#username").val()
                            },
                headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
                success : function(response){
                    if(response.status == 0){
                        var url = "{{ route('network.unilevel', ['lang' => \App::getLocale(), 'id' => 'targetId']) }}";
                        url = url.replace('targetId', response.targetId);;
                        document.location.href=url;
                    }else{
                        notiAlert(response.status, response.msg, '{{\App::getLocale()}}');
                        disable.out();
                    }
                }
            });
            event.preventDefault();
        });

        $("body").addClass("active-list");
    
        $(".member__tree--switcher .list").click(function(){
            $(".member__tree--switcher i").removeClass("active");
            $("body").removeClass("active-grid").addClass("active-list");
            $(this).addClass("active");
        });
        
        $(".member__tree--switcher .grid").click(function(){
            $(".member__tree--switcher i").removeClass("active");
            $("body").removeClass("active-list").addClass("active-grid");
            $(this).addClass("active");
        });

        $( '.member__tree--inner' ).on( 'click', '[data-id]', function( event ) {
            event.preventDefault();
            var button = $( this ),
                id = button.data( 'id' ),
                li = button.closest( 'li' );

            var activeSibling = li.parent().find( 'li.act' ).not( li );
            deactivateSiblings( activeSibling );

            var parent = $( 'ul[data-parent=' + id + ']' );
            li.toggleClass( 'act' );
            parent.toggleClass( 'act' );
            parent.addClass( 'index-' + ( li.index() + 1 ) );
        });

        var first = $( 'ul:eq(0)', '.member__tree--inner' );
        first.addClass( 'act' );
        first.find( '[data-id]:eq(0)' ).trigger( 'click' );
    });

    function findChildTree(id){
        if(!loadedId.includes(id)){
            loadedId.push(id);
            var disable = loadingDisable();
            var child = '';
            $.ajax({
                url     :   "{{ route('member.unilevelSearch', ['lang' => \App::getLocale()]) }}",
                method  :   "post",
                data    :   {
                                targetId : id
                            },
                headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
                success : function(response){
                    if(response.model.length > 0){
                        $.each(response.model, function (key, value) {
                                child += '<li>';
                                child += '<div class="item">';
                                if(value["profile"])
                                // child += '<img class="media-object img-thumbnail user-img rounded-circle unilevel_img" alt="User Picture" src="https://s3-ap-southeast-1.amazonaws.com/trealcap/'+value["profile"]+'">';
                                child += '<img class="media-object img-thumbnail user-img rounded-circle unilevel_img" alt="User Picture" src="data:image/;base64,'+value["profile"]+'">';
                                else
                                child += '<img class="media-object img-thumbnail user-img rounded-circle unilevel_img" alt="User Picture" src="{{asset("assets/img/profile.png")}}">';
                                child += '<div style="float: left;">';
                                child += '<span class="member__name" style="display: inline-block;">'+value["firstName"]+'</span>';
                                if(value["rank"])
                                child += '<div style="display: inline-block; padding: 3px 10px 3px 10px; margin-bottom: 3px; background-color: rgb(117,64,238,0.1); color: #8657f0; border-radius: 20px; text-align: center;">'+value["rank"]+'</div>';
                                child += '<a href="#" class="member__email">'+value["username"]+'</a>';
                                child += '</div>';
                                child += '<div class="member__amount" style="float: right; margin-top: 0px">';
                                child += '<div style="padding: 3px 10px 3px 10px; margin-bottom: 3px; background-color: rgb(45,199,109,0.1); color: #2DC76D; border-radius: 20px; text-align: center;">$'+value["totalinvest"]+'</div>';
                                child += '<div style="padding: 3px 10px 3px 10px; margin-bottom: 3px; background-color: rgb(255,112,82,0.1); color: #FF7052; border-radius: 20px; text-align: center;">$'+value["transferamount"]+'</div>';
                                child += '</div>';
                                if(value["children"] > 0 && ranking > 0)
                                child += '<i class="fa fa-plus" data-id="'+value["id"]+'" onclick="findChildTree('+value["id"]+')"></i>';
                                child += '</div>';
                                if(value["children"] > 0 && ranking > 0)
                                child += '<ul data-parent="'+value["id"]+'" id="'+value["id"]+'"></ul>';
                                child += '</li>';
                        });
                        $('#'+id).append(child);
                    }else{
                        $('#'+id).css('display','none');
                    }
                    disable.out();
                }
            });
        }
    }

    function deactivateSiblings( items ) {
        if ( 0 === items.length ) {
            return;
        }

        items.each(function() {
            var item = $( this );
            item.removeClass( 'act' );
            var child = $( 'ul[data-parent=' + item.find( '[data-id]' ).data( 'id' ) + ']' );

            child.removeClass( 'act' );
            var activeSibling = child.find( 'li.act' );
            deactivateSiblings( activeSibling );
        });
    }
</script>
@stop