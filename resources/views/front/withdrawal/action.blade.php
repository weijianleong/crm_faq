@if($model->status == "1" && $model->xPayStatus == "0" && $model->xPayWithdrawal ==0)
<button class="btn btn-primary glow_button"  id="btn_{{$model->payid}}" onclick ="rate({{$model->payid}})">
  <i class="fa fa-pencil"></i> @lang('common.cancel')
</button>
@endif

  <script>
    function cancel($id){

            var id = $id;
            var dataUrl = "{{ route('withdrawal.cancel', ['lang' => \App::getLocale()]) }}";
            $.ajax({
                    url     : dataUrl,
                    method  : 'post',
                    data    : {

                        id  : id
                       
                    },
                    headers:
                    {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success : function(response){
                        if(response.result == "fail"){
                            notiAlert(1, "@lang('withdraw.cancelFail')", '{{\App::getLocale()}}');
                        }else{
                            notiAlert(0, "@lang('withdraw.Cancelled')", '{{\App::getLocale()}}');
                            document.getElementById(response.model.payid).innerHTML = '<span style="color:red;">@lang('withdraw.Cancelled')</span>';
                            $('#btn_'+response.model.payid).hide();
                        }  
                    }
            });
    }
    function rate($id){

            swal({
                title: "@lang('withdraw.confirmCancel')",
                text: '',
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: '#00c0ef',
                cancelButtonColor: '#ff8080',
                confirmButtonText: '<i class="fa fa-check"></i> '+"@lang('common.confirm')"+'',
                cancelButtonText: '<i class="fa fa-times"></i> '+"@lang('common.cancel')"+'',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger'
            }).then(function () {

                cancel($id);

            }, function (dismiss) {

                if(dismiss==="cancel"){
                    

                }
                
            });
            return false;
        }

  </script>