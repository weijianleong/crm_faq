@extends('front.app')

@section('title')
  @lang('withdraw.title') | {{ config('app.name') }}
@stop

@section('header_styles')
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/calendar_custom.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/widgets.css')}}">
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">

    @if($bankList->isEmpty())
        <script>
            window.onload = function() {
                  goAddbank();
            };
        </script>
    @else

        <header class="head">
            <div class="main-bar">
               <div class="row no-gutters">
                   <div class="col-lg-5">
                       <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('withdraw.title')</h4>
                   </div>
                   <div class="col-lg-7">
                       <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                           <li class=" breadcrumb-item">
                               <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                                   <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                               </a>
                           </li>
                           <li class="breadcrumb-item active">@lang('withdraw.title')</li>
                       </ul>
                   </div>
               </div>
            </div>
        </header>

        <div class="outer">
            <div class="inner bg-container">
                <div class="card m-t-35">
                    <div class="card-header bg-white">
                        <div class="row justify-content-md-center">
                            <div class="col-sm-6 col-lg-3 media_max_991">
                                <div class="icon_align" style="border-left: 5px solid #FFC800; box-shadow: 2px 4px 25px -8px #989898;">
                                    <div class="float-left">
                                        <p id="cash_point"  style="font-size: 25px; color: #FFC800; margin-bottom: 0px; margin-top: 5px">$ {{ number_format($member->wallet->cash_point,2) }}</p>
                                        <p style="font-size: 15px; color: #6c6c86;">@lang('common.cashTitle')</p>
                                    </div>
                                <div class="text-right" style="margin: 5px 15px 11px"><i style="background-color: #FFC800; padding: 10px; border-radius: 70px; width: 50px; text-align: center; font-size: 30px; font-weight: bold;" class="fa"><img style="height: 20px; margin-bottom: 5px;" src="{{asset('assets/img/wallet.png')}}"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12 m-t-25">
                                <div>
                                    <ul class="nav nav-inline view_user_nav_padding" id="content-tab">
                                        <li class="nav-item card_nav_hover">
                                            <a class="nav-link active" href="#bank" id="home-tab" data-toggle="tab" aria-expanded="true">@lang('settings.subTitle2')</a>
                                        </li>
                                        <li class="nav-item card_nav_hover">
                                            <a class="nav-link " href="#IDpage" id="hats-tab" data-toggle="tab" aria-expanded="true"></a>
                                        </li>
                                    </ul>

                                    <div id="clothing-nav-content" class="tab-content m-t-10">
                                        <div role="tabpanel" class="tab-pane fade show active" id="bank">  
                                                <fieldset>
                                                    <!-- payment -->
                                                    <div class="form-group row">
                                                        <div class="col-lg-3 text-lg-right">
                                                            <label for="email3" class="col-form-label">@lang('withdraw.payment_method')</label>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="input-group input-group-prepend">
                                                                <span class="input-group-text border-right-0 rounded-left">
                                                                    <i class="fa  fa-edit"></i>
                                                                </span>

                                                                <select class="form-control hide_search" tabindex="7" name="payment_type" id="payment_type" onchange="show_payment_type()">
                                                                        <option value="bank" >@lang('withdraw.Bank_Payment')</option>
                                                                        <option value="debit"  >@lang('withdraw.Card_Payment')</option>
                                                                </select>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- email-->
                                                    <div class="form-group row">
                                                        <div class="col-lg-3 text-lg-right">
                                                            <label for="email3" class="col-form-label">@lang('withdraw.email')</label>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="input-group input-group-prepend">
                                                                <span class="input-group-text border-right-0 rounded-left">
                                                                    <i class="fa  fa-suitcase"></i>
                                                                </span>
                                                                <input type="text" id="email" name="email" value="{{ $member->user->email }}" class="form-control" placeholder="" readonly>          
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- user.name-->
                                                    <div class="form-group row">
                                                        <div class="col-lg-3 text-lg-right">
                                                            <label for="email3" class="col-form-label">@lang('withdraw.name')</label>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="input-group input-group-prepend">
                                                                <span class="input-group-text border-right-0 rounded-left">
                                                                    <i class="fa  fa-user"></i>
                                                                </span>
                                                                <input type="text" id="first_name" name="first_name" value="{{ $member->user->first_name }}" class="form-control" placeholder="" readonly>   
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- amount-->
                                                    <div class="form-group row">
                                                        <div class="col-lg-3 text-lg-right">
                                                            <label for="email3" class="col-form-label">@lang('withdraw.WithdrawlAmount')</label>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="input-group input-group-prepend">
                                                                <span class="input-group-text border-right-0 rounded-left">
                                                                    <i class="fa  fa-dollar"></i>
                                                                </span>
                                                                <input id="amount" class="form-control" type="text" name="amount" maxlength="10" onkeypress="return validateFloatKeyPress(this, event);" onpaste="return false;">   
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <label for="email3" class="col-form-label" style="color:red">@lang('withdraw.min')</label>
                                                        </div>
                                                    </div>
                                                    <!-- bank.number-->
                                                    <div id="Name_bank_number_div">
                                                        <div class="form-group row" >
                                                            <div class="col-lg-3 text-lg-right">
                                                                <label for="email3" class="col-form-label">@lang('settings.bank.number')</label>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="input-group input-group-prepend">
                                                                    <span class="input-group-text border-right-0 rounded-left">
                                                                        <i class="fa  fa-bank"></i>
                                                                    </span>

                                                                    <select class="form-control hide_search" tabindex="7" name="bank_no" id="bank_no" onchange="show_bankData()">
                                                                            <option value="">@lang('withdraw.optionSelect')</option>
                                                                        @foreach($bankList as $bankLists)
                                                                            <option value="{{$bankLists->id}}">{{$bankLists->bank_no}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Name bank-->
                                                        <div class="form-group row " id="Name_bank_div" style="display: none;">
                                                            <div class="col-lg-3 text-lg-right">
                                                                <label for="name3" class="col-form-label">@lang('settings.bank.name')</label>
                                                            </div>

                                                            <div class="col-lg-8">
                                                                <div class="input-group input-group-prepend">
                                                                    <span class="input-group-text border-right-0 rounded-left">
                                                                        <i class="fa fa-bank"></i>
                                                                    </span>
                                                                    <input type="text" id="bank_name" name="bank_name" value="" class="form-control" placeholder="" readonly>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--bank_account_holder-->
                                                        <div class="form-group row" id="bank_account_holder_div" style="display: none;">
                                                            <div class="col-lg-3 text-lg-right">
                                                                <label for="password3" class="col-form-label">@lang('settings.bank.holder')</label>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="input-group input-group-prepend">
                                                                    <span class="input-group-text border-right-0 rounded-left">
                                                                        <i class="fa fa-user"></i>
                                                                    </span>
                                                                    <input type="text" name="bank_account_holder" id="bank_account_holder"  class="form-control" placeholder="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Open bank-->
                                                        <div class="form-group row " id="Open_bank_div" style="display: none;">
                                                            <div class="col-lg-3 text-lg-right">
                                                                <label for="confirm3" class="col-form-label">@lang('withdraw.openBank')</label>
                                                            </div>
                                                            <div class="col-lg-3"> 
                                                                <div class="input-group input-group-prepend">
                                                                    <span class="input-group-text border-right-0 rounded-left">
                                                                        <i class="fa fa-bank"></i>
                                                                    </span>
                                                                    <input type="text" id="countryName" name="countryName" value="" class="form-control" placeholder="" readonly>
                                                                </div>  
                                                            </div>

                                                            <div class="col-lg-3" > 
                                                                <div class="input-group input-group-prepend">
                                                                    <span class="input-group-text border-right-0 rounded-left">
                                                                        <i class="fa fa-bank"></i>
                                                                    </span>
                                                                    <input type="text" id="stateName" name="stateName" value="" class="form-control" placeholder="" readonly>
                                                                </div>  
                                                            </div>

                                                            <div class="col-lg-2" > 
                                                                <div class="input-group input-group-prepend">
                                                                    <span class="input-group-text border-right-0 rounded-left">
                                                                        <i class="fa fa-bank"></i>
                                                                    </span>
                                                                    <input type="text" id="cityName" name="cityName" value="" class="form-control" placeholder="" readonly>
                                                                </div>  
                                                            </div>
                                                        </div>
                                                        <!-- swift-->
                                                        <div class="form-group row" id="Swiftcode_div" style="display: none;">
                                                            <div class="col-lg-3 text-lg-right">
                                                                <label for="confirm3" class="col-form-label">Swiftcode</label>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="input-group input-group-prepend">
                                                                <span class="input-group-text border-right-0 rounded-left">
                                                                <i class="fa fa-barcode"></i>
                                                            </span>
                                                                    <input type="text" name="swift" id="swift" value="" class="form-control" placeholder="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- sub_bank-->
                                                        <div class="form-group row" id="Sub_bank_div" style="display: none;">
                                                            <div class="col-lg-3 text-lg-right">
                                                                <label for="confirm3" class="col-form-label">@lang('withdraw.subBank')</label>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="input-group input-group-prepend">
                                                                    <span class="input-group-text border-right-0 rounded-left">
                                                                        <i class="fa fa-bank"></i>
                                                                    </span>
                                                                    <input type="text" name="sub_bank" id="sub_bank" class="form-control" placeholder="" value="" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- bank.address-->
                                                        <div class="form-group row" id="Address_bank_div" style="display: none;">
                                                            <div class="col-lg-3 text-lg-right">
                                                                <label for="confirm3" class="col-form-label">@lang('settings.bank.address')</label>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="input-group input-group-prepend">
                                                                    <textarea class="form-control"  name="bank_address" id="bank_address" rows="4" placeholder="@lang('settings.bank.address')" readonly></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- remark-->
                                                        <div class="form-group row" id="remark_div" style="display: none;">
                                                            <div class="col-lg-3 text-lg-right">
                                                                <label for="confirm3" class="col-form-label">@lang('withdraw.remark')</label>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="input-group input-group-prepend">
                                                                    <textarea class="form-control"  name="remark" id="remark" rows="4" placeholder="@lang('withdraw.remark')" maxlength="500"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- card.number-->
                                                    <div id="Visa_div" style="display: none;">
                                                        <!-- card_number-->
                                                        <div class="form-group row" id="card_number_div" >
                                                            <div class="col-lg-3 text-lg-right">
                                                                <label for="email3" class="col-form-label">@lang('withdraw.Card_Number')</label>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="input-group input-group-prepend">
                                                                    <span class="input-group-text border-right-0 rounded-left">
                                                                        <i class="fa  fa-credit-card"></i>
                                                                    </span>
                                                                    <input id="vm_cardname" class="form-control" type="text" name="vm_cardname"  onpaste="return false;">   
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Confirm_Card_Number-->
                                                        <div class="form-group row" id="Confirm_Card_Number_div" >
                                                            <div class="col-lg-3 text-lg-right">
                                                                <label for="email3" class="col-form-label">@lang('withdraw.Confirm_Card_Number')</label>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="input-group input-group-prepend">
                                                                    <span class="input-group-text border-right-0 rounded-left">
                                                                        <i class="fa  fa-credit-card"></i>
                                                                    </span>
                                                                    <input id="vm_cardname2" class="form-control" type="text" name="vm_cardname2"  onpaste="return false;">  
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- bank.number-->
                                                        <?php $countries = config('misc.countries');  ?>
                                                        <div class="form-group row" id="vm_country_div" >
                                                            <div class="col-lg-3 text-lg-right">
                                                                <label for="email3" class="col-form-label">@lang('withdraw.Card_Country')</label>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="input-group input-group-prepend">
                                                                    <span class="input-group-text border-right-0 rounded-left">
                                                                        <i class="fa  fa-bank"></i>
                                                                    </span>

                                                                    <select class="form-control hide_search" tabindex="7" name="vm_country" id="vm_country" >
                                                                            <option value="">@lang('withdraw.optionSelect')</option>
                                                                            @foreach ($countries as $country => $value)
                                                                                @if ($country == "China")
                                                                                @else
                                                                                    <option value="{{  $country }}" >
                                                                                      @lang('country.' . $country)
                                                                                    </option>
                                                                                @endif
                                                                            @endforeach
                                                                    </select>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- card.number-->
                                                    <div id="Bank_13000_div" style="display: none;">
                                                            <!-- bank.number-->
                                                            <div class="form-group row" id="No_bank_div">
                                                                <div class="col-lg-3 text-lg-right">
                                                                    <label for="email3" class="col-form-label">@lang('settings.bank.number')</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <div class="input-group input-group-prepend">
                                                                        <span class="input-group-text border-right-0 rounded-left">
                                                                            <i class="fa  fa-bank"></i>
                                                                        </span>

                                                                        <select class="form-control hide_search" tabindex="7" name="bank_no_asia" id="bank_no_asia" onchange="show_bankDataAsia()">
                                                                                <option value="">@lang('withdraw.optionSelect')</option>
                                                                            @foreach($bankListAsia as $bankListAsias)
                                                                                <option value="{{$bankListAsias->id}}">{{$bankListAsias->bank_no}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Name bank-->
                                                            <div class="form-group row " id="Name_bank_asia_div" style="display: none;">
                                                                <div class="col-lg-3 text-lg-right">
                                                                    <label for="name3" class="col-form-label">@lang('settings.bank.name')</label>
                                                                </div>

                                                                <div class="col-lg-8">
                                                                    <div class="input-group input-group-prepend">
                                                                        <span class="input-group-text border-right-0 rounded-left">
                                                                            <i class="fa fa-bank"></i>
                                                                        </span>
                                                                        <input type="text" id="bank_name_asia" name="bank_name_asia" value="" class="form-control" placeholder="" readonly>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- holder bank-->
                                                            <div class="form-group row " id="Holder_bank_asia_div" style="display: none;">
                                                                <div class="col-lg-3 text-lg-right">
                                                                    <label for="name3" class="col-form-label">@lang('settings.bank.holder')</label>
                                                                </div>

                                                                <div class="col-lg-8">
                                                                    <div class="input-group input-group-prepend">
                                                                        <span class="input-group-text border-right-0 rounded-left">
                                                                            <i class="fa fa-bank"></i>
                                                                        </span>
                                                                        <input type="text" id="bank_account_holder_asia" name="bank_account_holder_asia" value="" class="form-control" placeholder="" readonly>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Open bank-->
                                                            <div class="form-group row " id="Open_bank_asia_div" style="display: none;">
                                                                <div class="col-lg-3 text-lg-right">
                                                                    <label for="confirm3" class="col-form-label">@lang('settings.bank.country')</label>
                                                                </div>
                                                                <div class="col-lg-8"> 
                                                                    <div class="input-group input-group-prepend">
                                                                        <span class="input-group-text border-right-0 rounded-left">
                                                                            <i class="fa fa-bank"></i>
                                                                        </span>
                                                                        <input type="text" id="countryName_asia" name="countryName_asia" value="" class="form-control" placeholder="" readonly>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                            <!-- Swiftcode-->
                                                            <div class="form-group row" id="swift_asia_div" style="display: none;">
                                                                <div class="col-lg-3 text-lg-right">
                                                                    <label for="confirm3" class="col-form-label">Swiftcode</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <div class="input-group input-group-prepend">
                                                                        <span class="input-group-text border-right-0 rounded-left">
                                                                            <i class="fa fa-barcode"></i>
                                                                        </span>
                                                                        <input type="text" name="swift_asia" id="swift_asia" class="form-control" placeholder="" value="" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- bank.address-->
                                                            <div class="form-group row" id="Address_bank_asia_div" style="display: none;">
                                                                <div class="col-lg-3 text-lg-right">
                                                                    <label for="confirm3" class="col-form-label">@lang('settings.bank.address')</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <div class="input-group input-group-prepend">
                                                                        <textarea class="form-control"  name="bank_address_asia" id="bank_address_asia" rows="4" placeholder="@lang('settings.bank.address')" readonly></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <!-- submit-->
                                                    <div class="form-group row" >
                                                        <div class="col-lg-9 ml-auto">
                                                            <button class="btn btn-primary layout_btn_prevent" onclick="postwithdrawal()" id="postBtn" >@lang('common.submit')</button>
                                                            <button class="btn btn-secondary layout_btn_prevent">@lang('common.cancel')</button>
                                                        </div>
                                                    </div>
                                                </fieldset>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>
@stop

@section('footer_scripts')

    <!--Plugin scripts-->
    <script type="text/javascript" src="{{asset('assets/vendors/bootstrap_calendar/js/bootstrap_calendar.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/pages/mini_calendar.js')}}"></script>
    <!--End of Page level scripts-->
    <script type="text/javascript">

        function show_payment_type(){
            var disable = loadingDisable();
            var type = $('#payment_type').val();

            if(type == "bank"){
                disable.out();
                $('#Name_bank_number_div').show();
                $('#Visa_div').hide();
                $('#Bank_13000_div').hide();

            }else if(type == "debit"){
                disable.out();
                $('#Visa_div').show();
                $('#Name_bank_number_div').hide();
                $('#Bank_13000_div').hide();


            }else{
                disable.out();
                $('#Visa_div').hide();
                $('#Name_bank_number_div').hide();
                $('#Bank_13000_div').show();
            }
        }

        function show_bankData(){
            var disable = loadingDisable();
            var dataUrl = "{{ route('withdrawal.getBankData', ['lang' => \App::getLocale()]) }}";
            $.ajax({
                url     : dataUrl,
                method  : 'post',
                data    : {
                    id  : $('#bank_no').val(),     
                },
                headers:
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success : function(response){
                    disable.out();
                    if(response.result==0){
                        bankData = JSON.parse(response.bankData);
                        $('#Name_bank_div').show();
                        $('#bank_name').val(bankData.bank_name);
                        $('#bank_account_holder_div').show();
                        $('#bank_account_holder').val(bankData.bank_account_holder);
                        $('#Open_bank_div').show();
                        $('#countryName').val(bankData.countryName);
                        $('#stateName').val(bankData.stateName);
                        $('#cityName').val(bankData.cityName);
                        $('#Swiftcode_div').show();
                        $('#swift').val(bankData.swift);
                        $('#Sub_bank_div').show();
                        $('#sub_bank').val(bankData.sub_bank);
                        $('#Address_bank_div').show();
                        $('#bank_address').text(bankData.bank_address);
                        $('#remark_div').show();
                        $('#postBtn').prop('disabled', false);
                    }else{
                        $('#Name_bank_div').hide();
                        $('#bank_name').val("");
                        $('#bank_account_holder_div').hide();
                        $('#bank_account_holder').val("");
                        $('#Open_bank_div').hide();
                        $('#countryName').val("");
                        $('#stateName').val("");
                        $('#cityName').val("");
                        $('#Swiftcode_div').hide();
                        $('#swift').val("");
                        $('#Sub_bank_div').hide();
                        $('#sub_bank').val("");
                        $('#Address_bank_div').hide();
                        $('#bank_address').text("");
                        $('#remark_div').hide();
                        $('#postBtn').prop('disabled', true);
                        notiAlert(response.result, response.showTittle, '{{\App::getLocale()}}');
                    }
                }
            });
        }
        function show_bankDataAsia(){
            var disable = loadingDisable();
            var dataUrl = "{{ route('withdrawal.getBankDataAsia', ['lang' => \App::getLocale()]) }}";
            $.ajax({
                url     : dataUrl,
                method  : 'post',
                data    : {
                    id  : $('#bank_no_asia').val(),     
                },
                headers:
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success : function(response){
                    disable.out();
                    if(response.result==0){
                        bankData = JSON.parse(response.bankData);
                        $('#Name_bank_asia_div').show();
                        $('#bank_name_asia').val(bankData.bank_name);
                        $('#Holder_bank_asia_div').show();
                        $('#bank_account_holder_asia').val(bankData.bank_account_holder);
                        $('#Open_bank_asia_div').show();
                        $('#countryName_asia').val(bankData.countryName);
                        $('#stateName_asia').val(bankData.stateName);
                        $('#cityName_asia').val(bankData.cityName);
                        $('#swift_asia_div').show();
                        $('#swift_asia').val(bankData.swift);
                        $('#Address_bank_asia_div').show();
                        $('#bank_address_asia').text(bankData.bank_address);
                        $('#remark_asia_div').show();
                        
                    }else{
                        $('#Name_bank_asia_div').hide();
                        $('#bank_name_asia').val("");
                        $('#Holder_bank_asia_div').hide();
                        $('#bank_account_holder_asia').val("");
                        $('#Open_bank_asia_div').hide();
                        $('#countryName_asia').val("");
                        $('#stateName_asia').val("");
                        $('#cityName_asia').val("");
                        $('#swift_asia_div').hide();
                        $('#swift_asia').val("");
                        $('#Address_bank_asia_div').hide();
                        $('#bank_address_asia').text("");
                        $('#remark_asia_div').hide();
                        
                        notiAlert(response.result, response.showTittle, '{{\App::getLocale()}}');
                    }
                }
            });
        }
        function postwithdrawal(){
            var type = $('#payment_type').val();

            if(type == "bank"){

                var amount = $('#amount').val();
                var bank_id = $('#bank_no').val();
                var remark = $('#remark').val();
                var vm_cardname = $('#vm_cardname').val();
                var vm_cardname2 = $('#vm_cardname2').val();
                var vm_country = $('#vm_country').val();
                var swift = $('#swift').val();
                var bank_account_holder = $('#bank_account_holder').val();


                var obj = {amount: "amount", bank_no: "bank_no",swift:"swift",bank_account_holder:"bank_account_holder"};
                var final = { Id : obj};
                var Id = JSON.stringify(final);
                var ans = checkValue(Id);

                if(ans!=0){
                    return false;
                }

                if(amount >= 13000 ){

                    var url = "{{ route('withdrawal.postCreateAsia', ['lang' => \App::getLocale()]) }}";
                    var buttonDisplay = '{"Format":['+'{"confirmButtonText":'+'"@lang('common.confirm')"'+',"cancelButtonText":'+'"@lang('common.cancel')"'+',"setTittle":'+'"@lang('settings.secret')"'+',"error":'+'"@lang('error.securityPasswordError')"'+'}]}';
                    var obj = {type:type,amount: amount, bank_id: bank_id , vm_cardname:vm_cardname,vm_cardname2:vm_cardname2,vm_country:vm_country,swift:swift,bank_account_holder:bank_account_holder};
                    var final = { Data : obj};
                    var data = JSON.stringify(final);
                    swalAlert(url,buttonDisplay,data);

                }else{

                    var url = "{{ route('withdrawal.postCreate', ['lang' => \App::getLocale()]) }}";
                    var buttonDisplay = '{"Format":['+'{"confirmButtonText":'+'"@lang('common.confirm')"'+',"cancelButtonText":'+'"@lang('common.cancel')"'+',"setTittle":'+'"@lang('settings.secret')"'+',"error":'+'"@lang('error.securityPasswordError')"'+'}]}';
                    var obj = {amount: amount, bank_id: bank_id , remark:remark,swift:swift,bank_account_holder:bank_account_holder};
                    var final = { Data : obj};
                    var data = JSON.stringify(final);
                    swalAlert(url,buttonDisplay,data);

                }
                


            }else if(type == "debit"){

                var amount = $('#amount').val();
                var vm_cardname = $('#vm_cardname').val();
                var vm_cardname2 = $('#vm_cardname2').val();
                var vm_country = $('#vm_country').val();

                var obj = {vm_country: "vm_country",vm_cardname2: "vm_cardname2",vm_cardname: "vm_cardname",amount: "amount"};
                var final = { Id : obj};
                var Id = JSON.stringify(final);
                var ans = checkValue(Id);

                if(ans!=0){
                    return false;
                }

                if(vm_cardname != vm_cardname2){
                    
                    notiAlert(1, "@lang('withdraw.Card_error')", '{{\App::getLocale()}}');
                    $('#vm_cardname2').focus();
                    return false;
                }
                
                var url = "{{ route('withdrawal.postCreateAsia', ['lang' => \App::getLocale()]) }}";
                var buttonDisplay = '{"Format":['+'{"confirmButtonText":'+'"@lang('common.confirm')"'+',"cancelButtonText":'+'"@lang('common.cancel')"'+',"setTittle":'+'"@lang('settings.secret')"'+',"error":'+'"@lang('error.securityPasswordError')"'+'}]}';
                var obj = {type:type,amount: amount,bank_id:"viet_card", vm_cardname:vm_cardname,vm_cardname2:vm_cardname2,vm_country:vm_country};
                var final = { Data : obj};
                var data = JSON.stringify(final);
                swalAlert(url,buttonDisplay,data);


            }

        }
        function goAddbank(){
            var withdrawWarning = "@lang('withdraw.withdrawWarning')";
            var goAddbank = "@lang('withdraw.goAddbank')";
            var star = '<div class=starability-slot style="margin: auto;"><input type=radio id=slot-rate5 name=rating value="5"  /><label for="slot-rate5" title="Amazing" aria-label="Amazing, 5 stars" onclick=postStar(1)>5 stars</label><input type="radio" id="slot-rate4" name="rating" value="4" /><label for="slot-rate4" title="Very good" aria-label="Very good, 4 stars" onclick=postStar(2)>4 stars</label><input type="radio" id="slot-rate3" name="rating" value="3"/><label for="slot-rate3" title="Average" aria-label="Average, 3 stars" onclick=postStar(3)>3 stars</label><input type="radio" id="slot-rate2" name="rating" value="2" /><label for="slot-rate2" title="Not good" aria-label="Not good, 2 stars" onclick=postStar(4)>2 stars</label><input type="radio" id="slot-rate1" name="rating" value="1" /><label for="slot-rate1" title="Terrible" aria-label="Terrible, 1 star" onclick=postStar(5)>1 star</label></div>';
            var complain = '<textarea id="complain" name="complain" class="form-control" cols="50" rows="5" placeholder=""></textarea>';
            swal({
                title: "@lang('withdraw.withdrawWarning')",
                text: "@lang('withdraw.withdrawText')",
                type: 'info',
                confirmButtonColor: '#00c0ef',
                confirmButtonText: '<i class="fa fa-link"></i> '+goAddbank+'',
                confirmButtonClass: 'btn btn-success',
                allowOutsideClick: false
            }).then(function () {
                var url = "{{ route('settings.addBankAsia', ['lang' => \App::getLocale()]) }}";
                document.location.href=url;
            });
        }
        var cash_point = "{{ ($member->wallet->cash_point) }}";
        $('#amount').on('keyup', function(){
            var amount = $('#amount').val();
            if(!amount || amount == '.'){
                amount = 0;
            } 
            var newBalance1 = parseFloat(parseFloat(cash_point)-parseFloat(amount)).toFixed(2);
            if(newBalance1<0){
                $('#amount').val('0');
                $('#cash_point').text(format2(cash_point,"$"));
            }else{
                $('#cash_point').text(format2(newBalance1,"$"));
            }
            
        });
    </script>
@stop
