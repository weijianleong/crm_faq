@extends('front.app')
@section('title')
    @lang('breadcrumbs.dashboard') | {{ config('app.name') }}
@stop
@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <div style="padding-top: 15%;">
        <div style="text-align: center; font: bold 2.5em Open Sans,sans-serif; color: #084D8B;">**&nbsp;~~~~~~~~~~~~~~&nbsp;**</div><br>
        <div style="text-align: center; font: bold 2.5em Open Sans,sans-serif; color: #084D8B;">敬请期待</div><br>
        <div style="text-align: center; font: bold 2.5em Open Sans,sans-serif; color: #084D8B;">Coming Soon</div><br>
        <div style="text-align: center; font: bold 2.5em Open Sans,sans-serif; color: #084D8B;">**&nbsp;~~~~~~~~~~~~~~&nbsp;**</div>
    </div>
</div>
@stop