@extends('front.app')

@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/login2.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/animate/css/animate.min.css')}}" />
@stop

@section('title')
    New Registration | {{ config('app.name') }}
@stop

@section('main_content')
<div class="container wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s" style="margin: 0 auto 0 auto">
    <div class="row">
        <div class="col-11 mx-auto">
            <div class="row">
                <div class="col-lg-8 col-md-6 col-sm-6 mx-auto login_image login_section login_section_top text-login">
                    @if(\App::getLocale() == 'en')
                    <p class="text-right" style="margin-bottom: 0;"><a href="{{ route('register', ['lang' => 'chs']) }}" class="font-weight-bold font_16 text-login text-hover"><i class="fa fa-language"></i> 中文</a></p>
                    @else
                    <p class="text-right" style="margin-bottom: 0;"><a href="{{ route('register', ['lang' => 'en']) }}" class="font-weight-bold font_16 text-login text-hover"><i class="fa fa-language"></i> English</a></p>
                    @endif
                    <div class="login_logo login_border_radius1">
                        <h3 class="text-center" style="margin-bottom: 0;">
                            <img src="{{asset('assets/img/new_logo.png')}}" alt="josh logo" class="admire_logo">
                        </h3>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            <label class="font_18 font-weight-bold" style="margin-bottom: 0px;">@lang('register.title2')</label>
                        </div>
                    </div>
                    <div class="row m-t-10">
                        <div class="col-lg-6">
                            <!--direct_id-->
                            <div style="margin-bottom: 2px;">
                                <label for="login.captcha" class="col-form-label font-weight-bold" style="width: 100%">@lang('register.direct')</label>
                                <input type="email" class="form-control b_r_20 check" name="direct_id" id="direct_id" required="" placeholder="@lang('register.direct')" style="width: 70%; display: inline-block;">
                                <div style="width: 25%; display: inline-block;">
                                    <i class="fa fa-question-circle fa-1x" aria-hidden="true" style="cursor: pointer; width: 100%" onclick="checkMember()" id="checkMember"><span>@lang('register.checkID')</span></i>
                                </div>
                            </div>
                            <!-- first_name -->
                            <div style="margin-bottom: 2px;">
                                <label for="email" class="col-form-label font-weight-bold">@lang('settings.name')</label>
                                <input type="text" class="form-control b_r_20 check" id="first_name" name="first_name" placeholder="@lang('settings.name')" required="">
                            </div>
                            <!-- identification_number -->
                            <div style="margin-bottom: 2px;">
                                <label for="password" class="col-form-label font-weight-bold">@lang('settings.id')</label>
                                <input type="text" class="form-control b_r_20 check" id="identification_number" name="identification_number" placeholder="@lang('settings.id')" required="">
                            </div>
                            <!-- inputGender -->
                            <div style="margin-bottom: 2px;">
                                <label for="password" class="col-form-label font-weight-bold">@lang('settings.gender')</label>
                                <select class="form-control" name="inputGender" id="inputGender" class="form-control b_r_20 check">
                                    <option value="Male" >@lang('settings.gender.male')</option>
                                    <option value="Female">@lang('settings.gender.female')</option>
                                </select>
                            </div>
                            <!-- email -->
                            <div style="margin-bottom: 2px;">
                                <label for="email" class="col-form-label font-weight-bold">@lang('settings.email')</label>
                                <input type="email" class="form-control b_r_20 check" id="email" name="email" placeholder="@lang('settings.email')" required="" onpaste="return false;">
                            </div>
                            <!-- reemail -->
                            <div style="margin-bottom: 2px;">
                                <label for="email" class="col-form-label font-weight-bold">@lang('settings.reemail')</label>
                                <input type="email" class="form-control b_r_20 check" id="reemail" name="reemail" placeholder="@lang('settings.reemail')" required="" onpaste="return false;">
                            </div>
                            <!-- inputPhone -->
                            <div style="margin-bottom: 2px;">
                                <label for="email" class="col-form-label font-weight-bold">@lang('settings.phone1')</label>
                                <input type="text" class="form-control b_r_20 check" id="inputPhone" name="inputPhone" placeholder="@lang('settings.phone1')" required="">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <!-- countries -->
                            <?php $countries = config('misc.countries');  ?>
                            <div style="margin-bottom: 2px;">
                                <label for="email" class="col-form-label font-weight-bold">@lang('register.nationality')</label>
                                <select class="form-control" name="inputNationality" id="inputNationality">
                                    @foreach ($countries as $country => $value)
                                        <option value="{{ $country }}">@lang('country.' . $country)</option>
                                    @endforeach
                                </select>
                            </div>
                            <!-- address -->
                            <div style="margin-bottom: 2px;">
                                <label for="email" class="col-form-label font-weight-bold">@lang('settings.address')</label>
                                <textarea type="text" class="form-control b_r_20 check" id="address" name="address" placeholder="@lang('settings.address')" required=""></textarea>
                            </div>
                            <!-- inputPassword -->
                            <div style="margin-bottom: 2px;">
                                <label for="email" class="col-form-label font-weight-bold">@lang('settings.password')</label>
                                <input type="password" class="form-control b_r_20 check" id="inputPassword" name="inputPassword" placeholder="@lang('settings.password')(@lang('register.alphanumeric'))" required="">
                            </div>
                            <!-- inputRepassword -->
                            <div style="margin-bottom: 2px;">
                                <label for="email" class="col-form-label font-weight-bold">@lang('settings.repassword')</label>
                                <input type="password" class="form-control b_r_20 check" id="inputRepassword" name="inputRepassword" placeholder="@lang('settings.repassword')" required="">
                            </div>
                            <!-- inputIDfront -->
                            <div style="margin-bottom: 2px;">
                                <label for="email" class="col-form-label font-weight-bold"> @lang('settings.idfront')</label>
                                <br>
                                <a href="#" data-toggle="tooltip"  onclick="uploadFront()">
                                    <i class="fa fa-upload"></i>&nbsp; @lang('settings.plzChose')
                                </a>
                                <input type="file" name="inputIDfront"  class="form-control" id="inputIDfront"  hidden="" onchange="showFrontMsg()">
                                &nbsp;
                                <span id="showPic1Name"></span>
                                <span id="showFront"></span>
                            </div>
                            <!-- inputIDback -->
                            <div style="margin-bottom: 2px;">
                                <label for="email" class="col-form-label font-weight-bold"> @lang('settings.idback')</label>
                                <br>
                                <a href="#" data-toggle="tooltip"  onclick="uploadBack()">
                                    <i class="fa fa-upload"></i>&nbsp; @lang('settings.plzChose')
                                </a>
                                <input type="file" name="inputIDback"  class="form-control" id="inputIDback"  hidden="" onchange="showBackMsg()">
                                &nbsp;
                                <span id="showPic2Name"></span>
                                <span id="showBack"></span>
                            </div>
                            <div style="margin-bottom: 2px;">
                                <label for="login.captcha" class="col-form-label font-weight-bold" style="width: 100%">@lang('login.captcha')</label>
                                <input type="text" class="form-control b_r_20 check" name="captcha" id="captcha" required="" style="width: 48%; display: inline-block;">
                                <div style="width: 49%; display: inline-block;"><img src="{{captcha_src('flat2')}}" class="b_r_20" style="cursor: pointer; width: 100%" onclick="this.src='{{captcha_src('flat2')}}'+Math.random()" id="captchaCode"></div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="text-center login_bottom" style="padding-top: 5px;">
                                <button  onclick="postRegister()" id="register_btn" type="button" class="btn btn-block b_r_20 m-t-10 text-white btn-login-hover" style="background-color: #25265E;">@lang('register.next')</button>
                            </div>
                            <div class="m-t-15 text-left" style="display: inline-block; width: 49%;">
                                <a href="{{ route('login', ['lang' => \App::getLocale()]) }}" class="font_16 font-weight-bold text-login text-hover">@lang('login.title')</a>
                            </div>
                            <div class="m-t-15 text-right" style="display: inline-block; width: 48%;">
                                <a href="{{ route('forgotpassword', ['lang' => \App::getLocale()]) }}" class="col-form-label font-weight-bold text-login text-hover">@lang('login.forgot')</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('front.include.modal')
@stop

@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/js/pages/login2.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/pages/modals.js')}}"></script>
<script>
    $(document).ready(function () {
        var check = 0;
        $('.check').keypress(function (e) {
            if(e.which == 13 && check == 0) $("#loginForm").click();
        });

        $("#modalBtn").click(function(event){
            // var disable = loadingDisable();
            var inputIDfront = $('#inputIDfront')[0].files[0];
            var inputIDback = $('#inputIDback')[0].files[0];
            var url = "{{ route('register.post', ['lang' => \App::getLocale()]) }}";
            var obj = {captcha:$('#captcha').val(),inputRepassword: $('#inputRepassword').val(), inputPassword:$('#inputPassword').val(),address: $('#address').val(), inputPhone:$('#inputPhone').val(),reemail: $('#reemail').val(), email:$('#email').val(),identification_number: $('#identification_number').val(), first_name:$('#first_name').val(),direct_id: $('#direct_id').val(),inputGender: $('#inputGender').val(),inputNationality: $('#inputNationality').val()};
            var final = { Data : obj};
            var data = JSON.stringify(final);

            var form = new FormData();
            form.append('inputIDfront', inputIDfront);
            form.append('inputIDback', inputIDback);
            form.append('allData', data);
            var disable = loadingDisable();

            $.ajax({
                url     : url,
                data    : form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                headers:
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success : function(response){
                    disable.out();
                    if(response.status == 0){

                        swal({
                            title: response.msg,
                            text: response.showText,
                            type: 'success',
                            confirmButtonColor: '#4fb7fe',
                            allowOutsideClick: false
                        }).then(function(){ 
                            if(response.returnUrl) document.location.href=response.returnUrl;
                        });
                    }else{

                        swal({
                            title: response.msg,
                            text: "",
                            type: 'error',
                            confirmButtonColor: '#4fb7fe',
                        }).done();
                    }  
                },
                error: function (response) {
                    if(response.readyState == 4){
                        notiAlert(2, "{{ \Lang::get('error.sessionExpired') }}", '{{\App::getLocale()}}');
                        window.location.reload();
                    }
                }
            });
            
            return false; 
        });



    });
</script>

<script type="text/javascript">
        function uploadFront(){
            $('#inputIDfront').click();
        }
        function uploadBack(){
            $('#inputIDback').click();
        }
        function showFrontMsg(){
            if(document.getElementById("inputIDfront").files.length == 1){
                document.getElementById("showFront").innerHTML = "<i class='fa fa-check text-success'></i>";
                document.getElementById("showPic1Name").innerHTML = document.getElementById("inputIDfront").files[0].name;
            }
        }
        function showBackMsg(){
            if(document.getElementById("inputIDback").files.length == 1){
                document.getElementById("showBack").innerHTML = "<i class='fa fa-check text-success'></i>";
                document.getElementById("showPic2Name").innerHTML = document.getElementById("inputIDback").files[0].name;
            }
        }
        function checkMember(parentId) {
            var disable = loadingDisable();
            var modelContent = '';
            var id = $('#direct_id').val();
            var ajaxUrl = "{{ route('member.getMemberRegisterModal', ['id' =>'parentId', 'lang' => \App::getLocale()]) }}";
                ajaxUrl = ajaxUrl.replace('parentId', id);

            $.ajax({
                url     :   ajaxUrl,
                method  :   "get",
                headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
                success : function(response){
                    disable.out();
                    if(response.status == 0){
                       
                            modelContent =  '<div style="font-size: 28px !important;">'+response.msg+'</div>';
                            modelContent += '<hr><br/>';
                            modelContent += '<div style="font-size: 15px !important;font-weight: 100;">'+response.model.username+'</div>';
                            modelContent += '<hr><br/>';
                            modelContent += '<div style="font-size: 15px !important;">'+response.modelIsban.first_name+'</div>';
                        
                        $('#modalContent').empty().append(modelContent);
                        $('#modalZoomIn').click();
                        $("#checkMember").attr('class', 'fa fa-check text-success');
                        $("#checkMember").val('0');
                        
                    }else{
                        notiAlert(response.status, response.msg, '{{\App::getLocale()}}');
                        $("#checkMember").attr('class', 'fa fa-times text-danger');
                        $("#checkMember").val('1');
                    }
                    
                }
            });
        }
        function postRegister(){ 

            var obj = {captcha:"captcha",inputRepassword: "inputRepassword", inputPassword:"inputPassword",address: "address", inputPhone:"inputPhone",reemail: "reemail", email:"email",identification_number: "identification_number", first_name:"first_name",direct_id: "direct_id"};
            var final = { Id : obj};
            var Id = JSON.stringify(final);
            var ans = checkValue(Id);

            if(ans!=0){
                return false;
            }

            var obj = {reemail: "reemail", email:"email",direct_id: "direct_id"};
            var final = { Id : obj};
            var Id = JSON.stringify(final);
            var ans = ValidateEmail(Id);
            
            if(ans != 0){
                notiAlert(2, "@lang('error.emailFormat')", '{{\App::getLocale()}}');
                return false;
            }
            if( $('#reemail').val()!=$('#email').val() ){
                notiAlert(1, "@lang('error.emailNotsame')", '{{\App::getLocale()}}');
                return false;
            }

            inputRepassword = $('#inputRepassword').val();
            inputPassword = $('#inputPassword').val();

            ans = CheckPassword(inputPassword);
            if(ans == false) 
            { 
                notiAlert(2, "{{ \Lang::get('register.passwordRemind') }}", '{{\App::getLocale()}}');
                $('#inputPassword').focus();
                return false;
            }
            
            ans = CheckPassword(inputRepassword);
            if(ans == false) 
            { 
                notiAlert(2, "{{ \Lang::get('register.passwordRemind') }}", '{{\App::getLocale()}}');
                $('#inputRepassword').focus();
                return false;
            }
            
            if( $('#inputPassword').val()!=$('#inputRepassword').val() ){
                notiAlert(1, "@lang('error.securityPasswordError')", '{{\App::getLocale()}}');
                return false;
            }


            var inputIDfront = $('#inputIDfront')[0].files[0];
            if(!inputIDfront){
                
                notiAlert(2, "@lang('error.uploadfrontFail')", '{{\App::getLocale()}}');
                return false;
            }

            var inputIDback = $('#inputIDback')[0].files[0];
            if(!inputIDback){
                
                notiAlert(2, "@lang('error.uploadbackFail')", '{{\App::getLocale()}}');
                return false;
            }

            var disable = loadingDisable();
            var url = "{{ route('registerPreview.post', ['lang' => \App::getLocale()]) }}";
            var obj = {captcha:$('#captcha').val(),inputRepassword: $('#inputRepassword').val(), inputPassword:$('#inputPassword').val(),address: $('#address').val(), inputPhone:$('#inputPhone').val(),reemail: $('#reemail').val(), email:$('#email').val(),identification_number: $('#identification_number').val(), first_name:$('#first_name').val(),direct_id: $('#direct_id').val(),inputGender: $('#inputGender').val(),inputNationality: $('#inputNationality').val()};
            var final = { Data : obj};
            var data = JSON.stringify(final);

            var form = new FormData();
            form.append('inputIDfront', inputIDfront);
            form.append('inputIDback', inputIDback);
            form.append('allData', data);


            $.ajax({
                url     : url,
                data    : form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                headers:
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success : function(response){
                    notiAlert(response.status, response.msg, '{{\App::getLocale()}}');
                    if(response.status == 0){
                        
                        memberData = JSON.parse(response.memberData);
                        modelContent =  '<div style="font-size: 28px !important;">'+"@lang('settings.title1')"+'</div>';
                        modelContent += '<br/>';
                        modelContent += '<div style="font-size: 15px !important;font-weight: 100;">'+"&nbsp;@lang('settings.name'):&nbsp;"+memberData.Data.first_name+'</div>';
                        modelContent += '<hr><br/>';
                        modelContent += '<div style="font-size: 15px !important;font-weight: 100;">'+"&nbsp;@lang('settings.id'):&nbsp;"+memberData.Data.identification_number+'</div>';
                        modelContent += '<hr><br/>';
                        modelContent += '<div style="font-size: 15px !important;font-weight: 100;">'+"&nbsp;@lang('settings.gender'):&nbsp;"+memberData.Data.inputGender+'</div>';
                        modelContent += '<hr><br/>';
                        modelContent += '<div style="font-size: 15px !important;font-weight: 100;">'+"&nbsp;@lang('settings.email'):&nbsp;"+memberData.Data.email+'</div>';
                        modelContent += '<hr><br/>';
                        modelContent += '<div style="font-size: 15px !important;font-weight: 100;">'+"&nbsp;@lang('settings.phone1'):&nbsp;"+memberData.Data.inputPhone+'</div>';
                        modelContent += '<hr><br/>';
                        modelContent += '<div style="font-size: 15px !important;font-weight: 100;">'+"&nbsp;@lang('register.nationality'):&nbsp;"+memberData.Data.inputNationality+'</div>';
                        modelContent += '<hr><br/>';
                        modelContent += '<div style="font-size: 15px !important;font-weight: 100;">'+"&nbsp;@lang('settings.address'):&nbsp;"+memberData.Data.address+'</div>';
                        modelContent += '<hr><br/>';
                        modelContent += '<div style="font-size: 15px !important;font-weight: 100;">'+'<img id="myImg" src="{{ URL::to('/') }}/ID/'+response.pic1+'"  style="width:100%;max-width:300px"></div>';
                        modelContent += '<hr><br/>';
                        modelContent += '<div style="font-size: 15px !important;font-weight: 100;">'+'<img id="myImg" src="{{ URL::to('/') }}/ID/'+response.pic2+'"  style="width:100%;max-width:300px"></div>';
                        disable.out();
                        $('#modalContent').empty().append(modelContent);
                        $('#modalBtn').show();
                        $('#modalZoomIn').click();
                        $('#captchaCode').click();
                        setTimeout(function(){ deletePic(response.pic1,response.pic2); }, 2000);
                        //deletePic(response.pic1,response.pic2);
                       

                    }else{
                        check = 0;
                        disable.out();
                        $('#captchaCode').click();
                        
                    }  
                },
                error: function (response) {
                    if(response.readyState == 4){
                        notiAlert(2, "{{ \Lang::get('error.sessionExpired') }}", '{{\App::getLocale()}}');
                        window.location.reload();
                    }
                }
            });
            
            return false;    
        }
        function deletePic($idfront,$idback){
            var url = "{{ route('member.deletePic', ['lang' => \App::getLocale()]) }}";
            $.ajax({
                url     : url,
                data    : {
                    idfront : $idfront,
                    idback : $idback,
                },
                type: 'POST',
                headers:
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success : function(response){ 
                }
            });
        }
</script>


@stop