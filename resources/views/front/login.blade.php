@extends('front.app')

@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/login2.css')}}"/>
@stop

@section('title')
@lang('login.title') | {{ config('app.name') }}
@stop

@section('main_content')
<div class="container wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s" style="margin: 0 auto 0 auto">
    <div class="row">
        <div class="col-11 mx-auto">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 mx-auto login_image login_section login_section_top text-login">
                    @if(\App::getLocale() == 'en')
                    <p class="text-right" style="margin-bottom: 0;"><a href="{{ route('login', ['lang' => 'chs']) }}" class="font-weight-bold font_16 text-login text-hover"><i class="fa fa-language"></i> 中文</a></p>
                    @else
                    <p class="text-right" style="margin-bottom: 0;"><a href="{{ route('login', ['lang' => 'en']) }}" class="font-weight-bold font_16 text-login text-hover"><i class="fa fa-language"></i> English</a></p>
                    @endif
                    <div class="login_logo login_border_radius1">
                        <h3 class="text-center" style="margin-bottom: 0;">
                            <img src="{{asset('assets/img/new_logo.png')}}" alt="josh logo" class="admire_logo">
                        </h3>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            <label class="font_18 font-weight-bold" style="margin-bottom: 0px;">@lang('login.title')</label>
                        </div>
                    </div>
                    <div class="m-t-10">
                        <!-- <form class="form-floating action-form" action="login" method="post" id="loginForm"> -->
                            <div style="margin-bottom: 2px;">
                                <label for="email" class="col-form-label font-weight-bold">@lang('login.email')</label>
                                <input type="email" class="form-control b_r_20 check" id="email" name="email" placeholder="@lang('login.email')" required="">
                            </div>
                            <div style="margin-bottom: 2px;">
                                <label for="password" class="col-form-label font-weight-bold">@lang('login.password')</label>
                                <input type="password" class="form-control b_r_20 check" id="password" name="password" placeholder="@lang('login.password')" required="">
                            </div>

                            <div style="margin-bottom: 2px;">
                                <label for="login.captcha" class="col-form-label font-weight-bold" style="width: 100%">@lang('login.captcha')</label>
                                <input type="text" class="form-control b_r_20 check" name="captcha" id="captcha" required="" style="width: 48%; display: inline-block;">
                                <div style="width: 49%; display: inline-block;"><img src="{{captcha_src('flat2')}}" class="b_r_20" style="cursor: pointer; width: 100%" onclick="this.src='{{captcha_src('flat2')}}'+Math.random()" id="captchaCode"></div>
                            </div>

                            <div class="text-center login_bottom" style="padding-top: 5px;">
                                <button action="login" method="post" id="loginForm" type="button" class="btn btn-block b_r_20 m-t-10 text-white btn-login-hover" style="background-color: #25265E;">@lang('login.title') <i class="fa fa-sign-in"></i></button>
                            </div>
                            <div class="m-t-15 text-left" style="display: inline-block; width: 49%;">
                                <a href="{{ route('register', ['lang' => \App::getLocale()]) }}" class="font_16 font-weight-bold text-login text-hover">@lang('register.title2')</a>
                            </div>
                            <div class="m-t-15 text-right" style="display: inline-block; width: 48%;">
                                <a href="{{ route('forgotpassword', ['lang' => \App::getLocale()]) }}" class="col-form-label font-weight-bold text-login text-hover">@lang('login.forgot')</a>
                            </div>
                        <!-- </form> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/js/pages/login2.js')}}"></script>
<script>
    $(document).ready(function () {
        var check = 0;
        $('.check').keypress(function (e) {
            if(e.which == 13 && check == 0) $("#loginForm").click();
        });

        $("#loginForm").click(function(event){
            check = 1;
            var disable = loadingDisable();
            $.ajax({
                url     :   $(this).attr("action"),
                method  :   $(this).attr("method"),
                data    :   {
                                email       : $("#email").val(),
                                password    : $("#password").val(),
                                captcha     : $("#captcha").val(),
                            },
                headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
                success : function(response){
                    notiAlert(response.status, response.msg, '{{\App::getLocale()}}');
                    if(response.status == 0){
                        var url = "{!! route('home', ['lang' => \App::getLocale()]); !!}";
                        document.location.href=url;
                    }else{
                        check = 0;
                        disable.out();
                        $('#captchaCode').click();
                    }
                },
                error: function (response) {
                    if(response.readyState == 4){
                        notiAlert(2, "{{ \Lang::get('error.sessionExpired') }}", '{{\App::getLocale()}}');
                        window.location.reload();
                    }
                }
            });
            event.preventDefault();
        });
    });
</script>
@stop