@extends('front.app')

@section('title')
  @lang('capx.mymargin') | {{ config('app.name') }}
@stop

@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/widgets.css')}}">
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/countdown-clock.css')}}" />
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('capx.mymargin')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('capx.mymargin')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>

    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">@lang('capx.mymargin') :
                            <select id="selectProduct" class="form-control" style="margin-top: 5px;">
                                @foreach ($margin as $item)
                                    <option value="{{ $item->symbol }}">{{ \App::getLocale() == 'en' ? $item->name_en : $item->name_chs }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="card-body m-t-15" style="margin-bottom: 15px;">
                            <div id="refresh" class="row" style="text-align: right; display: block;"><i class="fa fa-refresh" style="background-color: #ccccff; border: 2px solid #00007f; border-radius: 15px; padding: 6px; margin: 0px 15px; cursor: pointer;"></i></div>
                        </div>
                        <div class="card-body" style="margin-bottom: 20px;">
                            <div class="col-lg-4" style="float: none; margin: 0 auto; box-shadow: 0px 0px 30px 0px #989898; padding: 15px; border-bottom: 8px solid #FFD700; border-top: 8px solid #FFD700; border-radius: 100px; background-color: #ffee81;">
                                <!-- <div class="row justify-content-md-center" style="text-align: center; display: block;"><img style="height: 50px; margin-bottom: 5px;" src="{{asset('assets/img/capx/trcap.png')}}"></div> -->
                                <div id="product" class="row justify-content-md-center" style="text-align: center; display: block; font-size: 20px;">-</div>
                                <br/>
                                <div id="fundsize" class="row justify-content-md-center" style="text-align: center; display: block; font-size: 40px;">0.00</div>
                            </div>
                        </div>

                        <div id="countdown"></div>

                        <div class="card-body m-t-20" style="margin-bottom: 20px;">
                            <div class="row justify-content-md-center">
                                <div class="col-sm-6 col-lg-3 media_max_991">
                                    <div style="box-shadow: 0px 0px 30px -5px #989898; padding: 15px; border-top: 8px solid #1b449d; border-radius: 5px; background-color: #fcfdfe;">
                                        <p style="font-size: 15px;">@lang('capx.units'):</p>
                                        <p id="unit" style="text-align: center; font-size: 25px; font-weight: 1;">0</p>
                                    </div>
                                </div>

                                <div class="fa-icon d-none d-sm-block" style="padding: 25px 15px 0px 15px; font-size: 60px;"><i class="fa fa-5x"></i>&times; </div>
                                <div class="fa-icon d-md-none" style="padding-left: 45%;padding-top: 10%; font-size: 60px;"><i class="fa fa-5x"></i>&times; </div>

                                <div class="col-sm-6 col-lg-3 media_max_991">
                                    <div style="box-shadow: 0px 0px 30px -5px #989898; padding: 15px; border-top: 8px solid #1b449d; border-radius: 5px; background-color: #fcfdfe;">
                                        <p style="font-size: 15px;">@lang('capx.price'):</p>
                                        <p id="price" style="text-align: center; font-size: 25px; font-weight: 1;">0.00</p>
                                    </div>
                                </div>

                                <div class="fa-icon d-none d-sm-block" style="padding: 25px 15px 0px 15px; font-size: 60px;"><i class="fa fa-5x"></i>&asymp; </div>
                                <div class="fa-icon d-md-none" style="padding-left: 45%;padding-top: 10%; font-size: 60px;"><i class="fa fa-5x"></i>&asymp; </div>

                                <div class="col-sm-6 col-lg-3 media_max_991">
                                    <div style="box-shadow: 0px 0px 30px -5px #989898; padding: 15px; border-top: 8px solid #1b449d; border-radius: 5px; background-color: #ecfbfe;">
                                        <p style="font-size: 15px;">@lang('capx.value'):</p>
                                        <p id="marginValue" style="text-align: center; font-size: 25px; font-weight: 1;">0.00</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div> 

                        <div id="login"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer_scripts')
<script type="text/javascript">
    $(document).ready( function () {
        getMyMargin('{{ $margin[0]->symbol }}');

        $('select#selectProduct').on('change', function(event) {
            getMyMargin($("select#selectProduct option:selected").val());
        });

        $('#refresh').on('click', function(event) {
            getMyMargin($("select#selectProduct option:selected").val());
        });
    });

    function getMyMargin(symbol){
        var disable = loadingDisable();
        $.ajax({
            url     :   "{{ route('capx.getMyMargin', ['lang' => \App::getLocale()]) }}",
            method  :   "post",
            data    :   { symbol : symbol },
            headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
            success : function(response){
                var data = response.details;
                var countdown = "";
                var login = "";
                $('#countdown').empty();
                $('#login').empty();

                $('#product').text(("{{ \App::getLocale() }}" == "en" ? data.name_en : data.name_chs) + ' {{ \Lang::get("capx.pool") }}');
                $('#fundsize').text(number_format(data.sales_unit, 2));

                if(data.sales_date > '{{ date("Y-m-d H:i:s") }}' && data.current_unit <= 0){
                    countdown = '<div class="col-lg-4" style="float: none; margin: 0 auto; text-align: center; padding: 0px 0px 25px 0px;">';
                    countdown += '    <div style="font-size: 20px; padding-bottom: 15px;">@lang('capx.countdownsalesdate')</div>';
                    countdown += '    <div id="clockdiv_{{ $item->symbol }}" class="clockdivcss">';
                    countdown += '        <div>';
                    countdown += '            <div class="smalltext">@lang('capx.day')</div>';
                    countdown += '            <span class="days"></span>';
                    countdown += '        </div>';
                    countdown += '        <div>';
                    countdown += '            <div class="smalltext">@lang('capx.hour')</div>';
                    countdown += '            <span class="hours"></span>';
                    countdown += '        </div>';
                    countdown += '        <div>';
                    countdown += '            <div class="smalltext">@lang('capx.minute')</div>';
                    countdown += '            <span class="minutes"></span>';
                    countdown += '        </div>';
                    countdown += '        <div>';
                    countdown += '            <div class="smalltext">@lang('capx.second')</div>';
                    countdown += '            <span class="seconds"></span>';
                    countdown += '        </div>';
                    countdown += '    </div>';
                    countdown += '</div>';

                    $('#countdown').empty().append(countdown);
                    initializeClock('clockdiv_'+data.symbol, data.sales_date);
                }else{
                    $('#countdown').empty();
                }

                if(data.login && data.sales_date <= '{{ date("Y-m-d H:i:s") }}'){
                    login = '<div class="col-lg-4" style="float: none; margin: 0 auto; text-align: center; padding: 0px 0px 25px 0px;">';
                    login += '    <div style="font-size: 20px; padding-bottom: 15px;">@lang('capx.logindetails')</div>';
                    login += '    <div class="clockdivcss">';
                    login += '        <div>';
                    login += '            <div class="smalltext">@lang('capx.mt5')</div>';
                    login += '            <span style="font-size: 20px;">'+data.login+'</span>';
                    login += '        </div>';
                    login += '        <div>';
                    login += '            <div class="smalltext">@lang('capx.password')</div>';
                    login += '            <span style="font-size: 20px;">'+data.password+'</span>';
                    login += '        </div>';
                    login += '    </div>';
                    login += '</div>';
                    login += '<div class="col-lg-4" style="float: none; margin: 0 auto; text-align: center; padding: 0px 0px 25px 0px;">';
                    login += '<a class="btn btn-warning glow_button" style="margin-top: 10px;" href="https://download.mql5.com/cdn/web/11421/mt5/techrealfx5setup.exe"><i class="fa fa-download"></i> @lang('common.downloadMT5')</a>';
                    login += '</div>';
                    $('#login').empty().append(login);
                }
                
                $('#unit').text(number_format((data.my_unit.toString().match(/^-?\d+(?:\.\d{0,4})?/)[0]), 4));
                $('#price').text('$ '+number_format(data.current_price, 4));
                var marginValue = data.my_unit * data.current_price;
                $('#marginValue').text('$ '+number_format(marginValue, 2));
                
                disable.out();
            },
            error: function (response) {
                if(response.readyState == 4){
                    notiAlert(2, "{{ \Lang::get('error.sessionExpired') }}", '{{\App::getLocale()}}');
                    window.location.reload();
                }
            }
        });
    }

    function getTimeRemaining(endtime) {
        var b = endtime.split(/\D+/);
        var newEndTime = b[1]+"/"+b[2]+"/"+b[0]+" "+b[3]+":"+b[4]+":"+b[5];

        var t = Date.parse(newEndTime) - Date.parse(new Date());
        var seconds = Math.floor((t / 1000) % 60);
        var minutes = Math.floor((t / 1000 / 60) % 60);
        var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        var days = Math.floor(t / (1000 * 60 * 60 * 24));
        
        if(days == 0 && hours == 0 && minutes == 0 && seconds == 0) window.location.reload();
        
        return {
                'total': t,
                'days': days,
                'hours': hours,
                'minutes': minutes,
                'seconds': seconds
                };
    }

    function initializeClock(id, endtime) {
        var clock = document.getElementById(id);
        var daysSpan = clock.querySelector('.days');
        var hoursSpan = clock.querySelector('.hours');
        var minutesSpan = clock.querySelector('.minutes');
        var secondsSpan = clock.querySelector('.seconds');

        function updateClock() {
            var t = getTimeRemaining(endtime);

            daysSpan.innerHTML = t.days;
            hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
            minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
            secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

            if (t.total <= 0) {
                clearInterval(timeinterval);
            }
        }

        updateClock();
        var timeinterval = setInterval(updateClock, 1000);
    }
</script>
@stop
