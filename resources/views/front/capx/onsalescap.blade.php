@extends('front.app')

@section('title')
@lang('capx.onsales') | {{ config('app.name') }}
@stop

@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/countdown-clock.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/swiper/css/swiper.min.css')}}"/>
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/widgets.css')}}">
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/sweetalert/css/sweetalert2.min.css')}}"/>
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/login2.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/animate/css/animate.min.css')}}" />
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('capx.onsales')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('capx.onsales')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            <i class="fa fa-table"></i> @lang('capx.onsales')
                        </div>
                        <div class="card-body p-t-10">
                            <div class=" m-t-25">
                                <div style="box-shadow: 0px 0px 30px -5px #989898; padding: 15px; border-radius: 10px; background-color: #fcfdfe; margin-bottom: 25px;">
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.product'): <span style="color: #1b449d;">@lang('capx.trcap')</span></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.price'): <span style="color: #1b449d;">$ {{ number_format($coin->price,2) }}</span></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.launcheddate'): <span style="color: #1b449d;">{{ $coin->launched_date }}</span></div>
                                    </div>
                                    <div class="row" style="margin-bottom: 20px;">
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.rank'): <span style="color: #1b449d;">{{ $rank }} </span></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.fundbalance'): <span style="color: #1b449d;">{{ $maxSubscribe }} </span></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.soldamount'): <span style="color: #1b449d;">{{ number_format($soldout) }} </span></div>
                                    </div>
                                    @if($coin->launched_date > date("Y-m-d H:i:s"))
                                    <div class="col-lg-4" style="float: none; margin: 0 auto; text-align: center; padding: 25px 0px 25px 0px;">
                                        <div style="font-size: 20px; padding-bottom: 15px;">@lang('capx.countdownselldate')</div>
                                        <div id="clockdiv" class="clockdivcss">
                                            <div>
                                                <div class="smalltext">@lang('capx.day')</div>
                                                <span class="days"></span>
                                            </div>
                                            <div>
                                                <div class="smalltext">@lang('capx.hour')</div>
                                                <span class="hours"></span>
                                            </div>
                                            <div>
                                                <div class="smalltext">@lang('capx.minute')</div>
                                                <span class="minutes"></span>
                                            </div>
                                            <div>
                                                <div class="smalltext">@lang('capx.second')</div>
                                                <span class="seconds"></span>
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                    <div class="row justify-content-md-center m-t-20" style="font-size: 20px;">
                                        <div class="text-center login_bottom col-lg-3" style="padding-top: 5px; border-bottom: none;">
                                            <button  onclick="subscribeForm()" type="button" class="btn btn-block b_r_20 m-t-10 text-white btn-login-hover" style="background-color: #25265E;">@lang('capx.subscribe')</button>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('front.include.modal')
@stop

@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/js/pages/modals.js')}}"></script>
<script type="text/javascript">
    $(document).ready( function () {
        var modelContent = '';

        modelContent = '<div style="font-size: 20px; padding: 15px;">@lang("capx.subscribeCoin"): @lang("capx.trcap")<br/><br/>';
        modelContent += '<div class="input-group input-group-prepend col-lg-10" style="padding-left:0px;">';
        modelContent += '<span class="input-group-text border-right-0 rounded-left" style="color:#000;width:200px;" id="pay_title">@lang("common.cashTitle")</span>';
        modelContent += '<input class="form-control" type="text" value="$ {{ $cashBalance }}" disabled="" id="displaywallet"></div>';
        modelContent += '<div class="input-group input-group-prepend col-lg-10" style="padding-left:0px;">';
        modelContent += '<span class="input-group-text border-right-0 rounded-left" style="color:#000;width:200px;">@lang("capx.rank")</span>';
        modelContent += '<input class="form-control" type="text" value="{{ $rank }}" disabled=""></div>';
        modelContent += '<div class="input-group input-group-prepend col-lg-10" style="padding-left:0px;">';
        modelContent += '<span class="input-group-text border-right-0 rounded-left" style="color:#000;width:200px;">@lang("capx.subscribeBalance")</span>';
        modelContent += '<input id="maxSubscribe" class="form-control" type="text" value="{{ $maxSubscribe }}" disabled=""></div>';
        modelContent += '<div class="input-group input-group-prepend col-lg-10" style="padding-left:0px;">';
        modelContent += '<span class="input-group-text border-right-0 rounded-left" style="color:#000;width:200px;">@lang("capx.subscribeAmount")</span>';
        modelContent += '<input id="amount" class="form-control" type="text" value="" onkeypress="return isAmountKey(this, event);"><span style="padding-top:5px;">&nbsp; x 500 = </span>&nbsp;<span id="multipleamount" style="padding-top:5px;">0</span></div>';
        modelContent += '<br/><button type="button" class="btn btn-primary glow_button" data-dismiss="modal" onclick="submitSubscribe()">@lang("common.confirm")</button>';
        modelContent += '</div>';
        $('#modalContent').empty().append(modelContent);
        
        var coin = JSON.parse('{!! json_encode($coin) !!}');
        if(coin.launched_date > '{{ date("Y-m-d H:i:s") }}') initializeClock('clockdiv', coin.launched_date);

        $('#amount').on('keyup', function(){
            if($('#amount').val()){
                var value = $('#amount').val()*500;
                $('#multipleamount').text(number_format(value));
            }else{
                $('#multipleamount').text(0);
            }
        });

    });

    function subscribeForm() {
        $('#modalZoomIn').click();
    }

    function submitSubscribe() {
        var amount = $('#amount').val();
        var url = "{{ route('capx.subscribetrc', ['lang' => \App::getLocale()]) }}";
        var buttonDisplay = '{"Format":['+'{"confirmButtonText":'+'"@lang('common.confirm')"'+',"cancelButtonText":'+'"@lang('common.cancel')"'+',"setTittle":'+'"@lang('settings.secret')"'+',"error":'+'"@lang('error.securityPasswordError')"'+'}]}';
        var obj = {amount: amount};
        var final = { Data : obj};
        var data = JSON.stringify(final);
        swalAlert(url,buttonDisplay,data);
    }

    function getTimeRemaining(endtime) {
        var b = endtime.split(/\D+/);
        var newEndTime = b[1]+"/"+b[2]+"/"+b[0]+" "+b[3]+":"+b[4]+":"+b[5];

        var t = Date.parse(newEndTime) - Date.parse(new Date());
        var seconds = Math.floor((t / 1000) % 60);
        var minutes = Math.floor((t / 1000 / 60) % 60);
        var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        var days = Math.floor(t / (1000 * 60 * 60 * 24));
        
        if(days == 0 && hours == 0 && minutes == 0 && seconds == 0) window.location.reload();
        
        return {
                'total': t,
                'days': days,
                'hours': hours,
                'minutes': minutes,
                'seconds': seconds
                };
    }

    function initializeClock(id, endtime) {
        var clock = document.getElementById(id);
        var daysSpan = clock.querySelector('.days');
        var hoursSpan = clock.querySelector('.hours');
        var minutesSpan = clock.querySelector('.minutes');
        var secondsSpan = clock.querySelector('.seconds');

        function updateClock() {
            var t = getTimeRemaining(endtime);

            daysSpan.innerHTML = t.days;
            hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
            minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
            secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

            if (t.total <= 0) {
                clearInterval(timeinterval);
            }
        }

        updateClock();
        var timeinterval = setInterval(updateClock, 1000);
    }
</script>
@stop