@extends('front.app')

@section('title')
  @lang('capx.registration') | {{ config('app.name') }}
@stop

@section('header_styles')
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/swiper/css/swiper.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/widgets.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/sweetalert/css/sweetalert2.min.css')}}"/>
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('capx.registration')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('capx.registration')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>

    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">@lang('capx.registration')</div>
                        <div class="card-body m-t-10">
                            <div class="row" style="padding: 20px;">
                                <div>
                                    <div style="color: red; font-size: 20px;">@lang('capx.note'):</div><br/>
                                    <div style="padding-bottom: 10px; font-size: 15px;">@lang('capx.note1'):</div>
                                    <div style="padding-bottom: 5px; font-weight: 1;">@lang('capx.note2')</div>
                                    <div style="padding-bottom: 5px; font-weight: 1;">@lang('capx.note3')</div>
                                    <div style="padding-bottom: 5px; font-weight: 1;">@lang('capx.note4')</div>
                                    <div style="padding-bottom: 5px; font-weight: 1;">@lang('capx.note5')</div>
                                    <div style="padding-bottom: 5px; font-weight: 1;">@lang('capx.note6')</div>
                                </div>
                            </div>
                            <hr>
                            
                            @if($isChina)
                            <div style="margin-bottom: 2px;">
                                <label for="email" class="col-form-label font-weight-bold">@lang('withdraw.state')</label>
                                <div class="col-lg-4"> 
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text border-right-0 rounded-left">
                                            <i class="fa fa-bank"></i>
                                        </span>
                                        <select class="form-control hide_search" tabindex="7" name="stateName" id="state"  onchange="ChangeState()">
                                            <option value="" id="id_state">@lang('withdraw.state')</option>
                                        </select>
                                    </div>  
                                </div>
                            </div>
                            <div style="margin-bottom: 2px;">
                                <label for="email" class="col-form-label font-weight-bold">@lang('withdraw.city')</label>
                                <div class="col-lg-4"> 
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text border-right-0 rounded-left">
                                            <i class="fa fa-bank"></i>
                                        </span>
                                        <select class="form-control hide_search" tabindex="7" name="cityName" id="city"  onchange="">
                                            <option value="" id="id_city">@lang('withdraw.city')</option>
                                        </select>
                                    </div>  
                                </div>
                            </div>

                            <div style="margin-bottom: 2px;">
                                <label for="email" class="col-form-label font-weight-bold">@lang('settings.address')</label>
                                <div class="col-lg-4"> 
                                    <textarea type="text" class="form-control b_r_20 check" id="address" name="address" placeholder="@lang('settings.address')" required=""></textarea>
                                </div>
                            </div>
                            <div style="margin-bottom: 2px;">
                                <br>
                                <a href="#" data-toggle="tooltip"  onclick="uploadBack()">
                                    <i class="fa fa-upload"></i>&nbsp; @lang('settings.plzChose')
                                </a>
                                <input type="file" name="inputIDback"  class="form-control" id="inputIDback"  hidden="" onchange="showBackMsg()">
                                &nbsp;
                                <span id="showPic2Name"></span>
                                <span id="showBack"></span>
                            </div>
                            <div class="text-center login_bottom col-lg-4" style="padding-top: 5px;">
                                <button  onclick="postRegister()" id="register_btn" type="button" class="btn btn-block b_r_20 m-t-10 text-white btn-login-hover" style="background-color: #25265E;">Submit</button>
                            </div>
                            @else
                            <div style="margin-bottom: 2px;">
                                <label for="email" class="col-form-label font-weight-bold">@lang('settings.address')</label>
                                <div class="col-lg-4"> 
                                    <textarea type="text" class="form-control b_r_20 check" id="address" name="address" placeholder="@lang('settings.address')" required=""></textarea>
                                </div>
                            </div>
                            <div style="margin-bottom: 2px;">
                                <br>
                                <a href="#" data-toggle="tooltip"  onclick="uploadBack()">
                                    <i class="fa fa-upload"></i>&nbsp; @lang('settings.plzChose')
                                </a>
                                <input type="file" name="inputIDback"  class="form-control" id="inputIDback"  hidden="" onchange="showBackMsg()">
                                &nbsp;
                                <span id="showPic2Name"></span>
                                <span id="showBack"></span>
                            </div>
                            <div class="text-center login_bottom col-lg-4" style="padding-top: 5px;">
                                <button  onclick="postRegister()" id="register_btn" type="button" class="btn btn-block b_r_20 m-t-10 text-white btn-login-hover" style="background-color: #25265E;">Submit</button>
                            </div>
                            @endif
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer_scripts')

<script type="text/javascript">

</script>
@stop
