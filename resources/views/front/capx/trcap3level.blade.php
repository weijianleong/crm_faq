@extends('front.app')

@section('title')
  @lang('capx.purchased3level') | {{ config('app.name') }}
@stop

@section('header_styles')
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/widgets.css')}}">
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('capx.purchased3level')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('capx.purchased3level')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>

    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header bg-white">@lang('capx.purchased3level')
                        </div>
                        <div class="row" style="padding: 0px 25px 0px 25px;">
                            <div class="col-lg-4">
                                <div class="card m-t-35 cardSpecialhover" style="border: none; border-left: 5px solid #2F61D5; background-color: #fff; box-shadow: 2px 4px 25px -8px #989898;">
                                    <div class="card-body p-t-10">
                                        <div class="m-t-15">
                                            <span style="background-color: rgb(47,97,213,0.15); color: #2F61D5; padding: 10px 20px; border-radius: 20px; position: absolute; right: 15px;">@lang('unilevel.level1')</span>
                                        </div>
                                        <div style="color: #25265E; font-size: 25px; margin-top: 60px;">@lang('capx.purchaseperson') : <span style="color: rgb(120,121,147,0.8);">{{ $level1member }}</span></div>
                                        <div style="color: #25265E; font-size: 25px;">@lang('capx.purchaseunit') : <span style="color: rgb(120,121,147,0.8);">{{ number_format(floor($level1amount * 10000) / 10000, 4) }}</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card m-t-35 cardSpecialhover" style="border: none; border-left: 5px solid #2F61D5; background-color: #fff; box-shadow: 2px 4px 25px -8px #989898;">
                                    <div class="card-body p-t-10">
                                        <div class="m-t-15">
                                            <span style="background-color: rgb(47,97,213,0.15); color: #2F61D5; padding: 10px 20px; border-radius: 20px; position: absolute; right: 15px;">@lang('unilevel.level2')</span>
                                        </div>
                                        <div style="color: #25265E; font-size: 25px; margin-top: 60px;">@lang('capx.purchaseperson') : <span style="color: rgb(120,121,147,0.8);">{{ $level2member }}</span></div>
                                        <div style="color: #25265E; font-size: 25px;">@lang('capx.purchaseunit') : <span style="color: rgb(120,121,147,0.8);">{{ number_format(floor($level2amount * 10000) / 10000, 4) }}</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card m-t-35 cardSpecialhover" style="border: none; border-left: 5px solid #2F61D5; background-color: #fff; box-shadow: 2px 4px 25px -8px #989898;">
                                    <div class="card-body p-t-10">
                                        <div class="m-t-15">
                                            <span style="background-color: rgb(47,97,213,0.15); color: #2F61D5; padding: 10px 20px; border-radius: 20px; position: absolute; right: 15px;">@lang('unilevel.level3')</span>
                                        </div>
                                        <div style="color: #25265E; font-size: 25px; margin-top: 60px;">@lang('capx.purchaseperson') : <span style="color: rgb(120,121,147,0.8);">{{ $level3member }}</span></div>
                                        <div style="color: #25265E; font-size: 25px;">@lang('capx.purchaseunit') : <span style="color: rgb(120,121,147,0.8);">{{ number_format(floor($level3amount * 10000) / 10000, 4) }}</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding: 0px 25px 35px 25px;">
                            <div class="col-lg-4">
                                <div class="card m-t-35 cardSpecialhover" style="border: none; border-left: 5px solid #2F61D5; background-color: #fff; box-shadow: 2px 4px 25px -8px #989898;">
                                    <div class="card-body p-t-10">
                                        <div class="m-t-15">
                                            <span style="background-color: rgb(47,97,213,0.15); color: #2F61D5; padding: 10px 20px; border-radius: 20px; position: absolute; right: 15px;">@lang('capx.totalpurchaseperson')</span>
                                        </div>
                                        <div style="color: #25265E; font-size: 25px; margin-top: 60px;"><span style="color: rgb(120,121,147,0.8);">{{ $totalmember }}</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card m-t-35 cardSpecialhover" style="border: none; border-left: 5px solid #2F61D5; background-color: #fff; box-shadow: 2px 4px 25px -8px #989898;">
                                    <div class="card-body p-t-10">
                                        <div class="m-t-15">
                                            <span style="background-color: rgb(47,97,213,0.15); color: #2F61D5; padding: 10px 20px; border-radius: 20px; position: absolute; right: 15px;">@lang('capx.totalpurchaseunit')</span>
                                        </div>
                                        <div style="color: #25265E; font-size: 25px; margin-top: 60px;"><span style="color: rgb(120,121,147,0.8);">{{ number_format(floor($totalamount * 10000) / 10000, 4) }}</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer_scripts')
<script type="text/javascript">
    $(document).ready( function () {
    });
</script>
@stop
