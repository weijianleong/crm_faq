@extends('front.app')

@section('title')
@lang('capx.upcoming') | {{ config('app.name') }}
@stop

@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/datatables/css/colReorder.bootstrap.min.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/progress-bar.css')}}" />
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('capx.upcoming')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('capx.upcoming')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            <i class="fa fa-table"></i> @lang('capx.upcoming')
                        </div>
                        <div class="card-body p-t-10">
                            <div class=" m-t-25">
                                <div style="box-shadow: 0px 0px 30px -5px #989898; padding: 15px; border-radius: 10px; background-color: #fcfdfe; margin-bottom: 25px;">
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.product'): <span style="color: #1b449d;">TR Turkey</span></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.launcheddate'): <span style="color: #1b449d;">2019-02-14</span></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.expireddate'): <span style="color: #1b449d;">2019-05-31</span></div>
                                    </div>
                                    <div class="row" style="margin-bottom: 20px;">
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.price'): <span style="color: #1b449d;">$1.01</span></div>
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.fundsize'): <span style="color: #1b449d;">$6,000,000.00</span></div>
                                    </div>
                                </div>
                                <div style="box-shadow: 0px 0px 30px -5px #989898; padding: 15px; border-radius: 10px; background-color: #fcfdfe; margin-bottom: 25px;">
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.product'): <span style="color: #1b449d;">TR Turkey</span></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.launcheddate'): <span style="color: #1b449d;">2019-02-14</span></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.expireddate'): <span style="color: #1b449d;">2019-05-31</span></div>
                                    </div>
                                    <div class="row" style="margin-bottom: 20px;">
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.price'): <span style="color: #1b449d;">$1.01</span></div>
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.fundsize'): <span style="color: #1b449d;">$6,000,000.00</span></div>
                                    </div>
                                </div>
                                <div style="box-shadow: 0px 0px 30px -5px #989898; padding: 15px; border-radius: 10px; background-color: #fcfdfe; margin-bottom: 25px;">
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.product'): <span style="color: #1b449d;">TR Turkey</span></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.launcheddate'): <span style="color: #1b449d;">2019-02-14</span></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.expireddate'): <span style="color: #1b449d;">2019-05-31</span></div>
                                    </div>
                                    <div class="row" style="margin-bottom: 20px;">
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.price'): <span style="color: #1b449d;">$1.01</span></div>
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.fundsize'): <span style="color: #1b449d;">$6,000,000.00</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.colReorder.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script type="text/javascript">
    // var tableId = "cashStatement";
    // var dataUrl = "{{ route('member.cashlist', ['lang' => \App::getLocale()]) }}";
    // var columnIds = [
    //                 { "data": "created_at" },
    //                 { "data": "remark" },
    //                 { "data": "credit" },
    //                 { "data": "debit" },
    //                 { "data": "balance" },
    //                 ];
    // var disableSearchIndex = [2,3,4];
    // $(document).ready( function () {
    //     callDatatables(tableId, dataUrl, columnIds, disableSearchIndex);
    // });
</script>
@stop