@extends('front.app')

@section('title')
  @lang('capx.trcap') | {{ config('app.name') }}
@stop

@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/swiper/css/swiper.min.css')}}"/>
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/widgets.css')}}">
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/sweetalert/css/sweetalert2.min.css')}}"/>
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/login2.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/animate/css/animate.min.css')}}" />
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('capx.trcap')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('capx.trcap')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>

    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col m-t-35">
                    <div class="col-lg-4" style="float: none; margin: 0 auto; box-shadow: 0px 0px 30px 0px #989898; padding: 15px; border-bottom: 8px solid #FFD700; border-top: 8px solid #FFD700; border-radius: 100px; background-color: #ffee81;">
                        <!-- <div class="row justify-content-md-center" style="text-align: center; display: block;"><img style="height: 50px; margin-bottom: 5px;" src="{{asset('assets/img/capx/')}}"></div> -->
                        <div class="row justify-content-md-center" style="text-align: center; display: block; font-size: 20px;">@lang('capx.trcap') @lang('capx.pool'):</div>
                        <br/>
                        <div id="trcappool" class="row justify-content-md-center" style="text-align: center; display: block; font-size: 45px;">$ {{ number_format($poolAmount,2) }}</div>
                        <br/>
                    </div>
                    <div class="row justify-content-md-center m-t-20" style="font-size: 20px;">
                        <div class="text-center login_bottom col-lg-3" style="padding-top: 5px; border-bottom: none;">
                            <button  onclick="subscribeForm()" type="button" class="btn btn-block b_r_20 m-t-10 text-white btn-login-hover" style="background-color: #25265E;">@lang('capx.subscribe')</button>
                        </div>
                    </div>
                    <br/>
                    <div class="card m-t-25">
                        <div class="card-header bg-white">@lang('capx.mytrcap')</div>
                        <div class="card-body m-t-35" style="margin-bottom: 20px;">
                            <div class="row justify-content-md-center">
                                <div class="col-sm-6 col-lg-3 media_max_991">
                                    <div style="box-shadow: 0px 0px 30px -5px #989898; padding: 15px; border-top: 8px solid #1b449d; border-radius: 5px; background-color: #fcfdfe;">
                                        <p style="font-size: 15px;">@lang('capx.units'):</p>
                                        <p style="text-align: center; font-size: 25px; font-weight: 1;">{{ number_format($coinUnit) }}</p>
                                    </div>
                                </div>

                                <div class="fa-icon d-none d-sm-block" style="padding: 25px 15px 0px 15px; font-size: 60px;"><i class="fa fa-5x"></i>&times; </div>
                                <div class="fa-icon d-md-none" style="padding-left: 45%;padding-top: 10%; font-size: 60px;"><i class="fa fa-5x"></i>&times; </div>

                                <div class="col-sm-6 col-lg-3 media_max_991">
                                    <div style="box-shadow: 0px 0px 30px -5px #989898; padding: 15px; border-top: 8px solid #1b449d; border-radius: 5px; background-color: #fcfdfe;">
                                        <p style="font-size: 15px;">@lang('capx.price'):</p>
                                        <p style="text-align: center; font-size: 25px; font-weight: 1;">$ {{ number_format($coinPrice,2) }}</p>
                                    </div>
                                </div>

                                <div class="fa-icon d-none d-sm-block" style="padding: 25px 15px 0px 15px; font-size: 60px;"><i class="fa fa-5x"></i>&asymp; </div>
                                <div class="fa-icon d-md-none" style="padding-left: 45%;padding-top: 10%; font-size: 60px;"><i class="fa fa-5x"></i>&asymp; </div>

                                <div class="col-sm-6 col-lg-3 media_max_991">
                                    <div style="box-shadow: 0px 0px 30px -5px #989898; padding: 15px; border-top: 8px solid #1b449d; border-radius: 5px; background-color: #ecfbfe;">
                                        <p style="font-size: 15px;">@lang('capx.value'):</p>
                                        <p style="text-align: center; font-size: 25px; font-weight: 1;">$ {{ number_format(($coinUnit*$coinPrice),2) }}</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('front.include.modal')
@stop

@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/js/pages/modals.js')}}"></script>
<script type="text/javascript">
    $(document).ready( function () {
        var modelContent = '';

        modelContent = '<div style="font-size: 20px; padding: 15px;">@lang("capx.subscribeCoin"): @lang("capx.trcap")<br/><br/>';
        modelContent += '<div class="input-group input-group-prepend col-lg-10" style="padding-left:0px;">';
        modelContent += '<span class="input-group-text border-right-0 rounded-left" style="color:#000;width:200px;">@lang("common.cashTitle")</span>';
        modelContent += '<input class="form-control" type="text" value="$ {{ $cashBalance }}" disabled=""></div>';
        modelContent += '<div class="input-group input-group-prepend col-lg-10" style="padding-left:0px;">';
        modelContent += '<span class="input-group-text border-right-0 rounded-left" style="color:#000;width:200px;">@lang("capx.subscribeBalance")</span>';
        modelContent += '<input id="maxSubscribe" class="form-control" type="text" value="{{ $maxSubscribe }}" disabled=""></div>';
        modelContent += '<div class="input-group input-group-prepend col-lg-10" style="padding-left:0px;">';
        modelContent += '<span class="input-group-text border-right-0 rounded-left" style="color:#000;width:200px;">@lang("capx.subscribeAmount")</span>';
        modelContent += '<input id="amount" class="form-control" type="text" value="" onkeypress="return isAmountKey(this, event);"><span style="padding-top:5px;">&nbsp; x500</span></div>';
        modelContent += '<br/><button type="button" class="btn btn-primary glow_button" data-dismiss="modal" onclick="submitSubscribe()">@lang("common.confirm")</button>';
        modelContent += '</div>';
        $('#modalContent').empty().append(modelContent);

    });

    function subscribeForm() {
        $('#modalZoomIn').click();
    }

    function submitSubscribe() {
        var amount = $('#amount').val();
        var url = "{{ route('capx.subscribetrc', ['lang' => \App::getLocale()]) }}";
        var buttonDisplay = '{"Format":['+'{"confirmButtonText":'+'"@lang('common.confirm')"'+',"cancelButtonText":'+'"@lang('common.cancel')"'+',"setTittle":'+'"@lang('settings.secret')"'+',"error":'+'"@lang('error.securityPasswordError')"'+'}]}';
        var obj = {amount: amount};
        var final = { Data : obj};
        var data = JSON.stringify(final);
        swalAlert(url,buttonDisplay,data);
    }
</script>
@stop
