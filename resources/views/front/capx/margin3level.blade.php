@extends('front.app')

@section('title')
  @lang('capx.purchased3level') | {{ config('app.name') }}
@stop

@section('header_styles')
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/widgets.css')}}">
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('capx.purchased3level')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('capx.purchased3level')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>

    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header bg-white">@lang('capx.purchased3level') :
                            <select id="selectProduct" class="form-control" style="margin-top: 5px;">
                                @foreach ($margin as $item)
                                    <option value="{{ $item->symbol }}">{{ \App::getLocale() == 'en' ? $item->name_en : $item->name_chs }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="row" style="padding: 0px 25px 0px 25px;">
                            <div class="col-lg-4">
                                <div class="card m-t-35 cardSpecialhover" style="border: none; border-left: 5px solid #2F61D5; background-color: #fff; box-shadow: 2px 4px 25px -8px #989898;">
                                    <div class="card-body p-t-10">
                                        <div class="m-t-15">
                                            <span style="background-color: rgb(47,97,213,0.15); color: #2F61D5; padding: 10px 20px; border-radius: 20px; position: absolute; right: 15px;">@lang('unilevel.level1')</span>
                                        </div>
                                        <div style="color: #25265E; font-size: 25px; margin-top: 60px;">@lang('capx.purchaseperson') : <span id="level1member" style="color: rgb(120,121,147,0.8);">0</span></div>
                                        <div style="color: #25265E; font-size: 25px;">@lang('capx.purchaseunit') : <span id="level1amount" style="color: rgb(120,121,147,0.8);">0.0000</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card m-t-35 cardSpecialhover" style="border: none; border-left: 5px solid #2F61D5; background-color: #fff; box-shadow: 2px 4px 25px -8px #989898;">
                                    <div class="card-body p-t-10">
                                        <div class="m-t-15">
                                            <span style="background-color: rgb(47,97,213,0.15); color: #2F61D5; padding: 10px 20px; border-radius: 20px; position: absolute; right: 15px;">@lang('unilevel.level2')</span>
                                        </div>
                                        <div style="color: #25265E; font-size: 25px; margin-top: 60px;">@lang('capx.purchaseperson') : <span id="level2member" style="color: rgb(120,121,147,0.8);">0</span></div>
                                        <div style="color: #25265E; font-size: 25px;">@lang('capx.purchaseunit') : <span id="level2amount" style="color: rgb(120,121,147,0.8);">0.0000</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card m-t-35 cardSpecialhover" style="border: none; border-left: 5px solid #2F61D5; background-color: #fff; box-shadow: 2px 4px 25px -8px #989898;">
                                    <div class="card-body p-t-10">
                                        <div class="m-t-15">
                                            <span style="background-color: rgb(47,97,213,0.15); color: #2F61D5; padding: 10px 20px; border-radius: 20px; position: absolute; right: 15px;">@lang('unilevel.level3')</span>
                                        </div>
                                        <div style="color: #25265E; font-size: 25px; margin-top: 60px;">@lang('capx.purchaseperson') : <span id="level3member" style="color: rgb(120,121,147,0.8);">0</span></div>
                                        <div style="color: #25265E; font-size: 25px;">@lang('capx.purchaseunit') : <span id="level3amount" style="color: rgb(120,121,147,0.8);">0.0000</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding: 0px 25px 35px 25px;">
                            <div class="col-lg-4">
                                <div class="card m-t-35 cardSpecialhover" style="border: none; border-left: 5px solid #2F61D5; background-color: #fff; box-shadow: 2px 4px 25px -8px #989898;">
                                    <div class="card-body p-t-10">
                                        <div class="m-t-15">
                                            <span style="background-color: rgb(47,97,213,0.15); color: #2F61D5; padding: 10px 20px; border-radius: 20px; position: absolute; right: 15px;">@lang('capx.totalpurchaseperson')</span>
                                        </div>
                                        <div style="color: #25265E; font-size: 25px; margin-top: 60px;"><span id="totalmember" style="color: rgb(120,121,147,0.8);">0</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card m-t-35 cardSpecialhover" style="border: none; border-left: 5px solid #2F61D5; background-color: #fff; box-shadow: 2px 4px 25px -8px #989898;">
                                    <div class="card-body p-t-10">
                                        <div class="m-t-15">
                                            <span style="background-color: rgb(47,97,213,0.15); color: #2F61D5; padding: 10px 20px; border-radius: 20px; position: absolute; right: 15px;">@lang('capx.totalpurchaseunit')</span>
                                        </div>
                                        <div style="color: #25265E; font-size: 25px; margin-top: 60px;"><span id="totalamount" style="color: rgb(120,121,147,0.8);">0.0000</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer_scripts')
<script type="text/javascript">
    $(document).ready( function () {
        get3levelmargin('{{ $margin[0]->symbol }}');

        $('select#selectProduct').on('change', function(event) {
            get3levelmargin($("select#selectProduct option:selected").val());
        });
    });

    function get3levelmargin(symbol){
        var disable = loadingDisable();
        $.ajax({
            url     :   "{{ route('capx.get3levelmargin', ['lang' => \App::getLocale()]) }}",
            method  :   "post",
            data    :   { symbol : symbol },
            headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
            success : function(response){
                var data = response.details;

                $('#level1member').text(number_format(data.level1member, 0));
                $('#level1amount').text(number_format((data.level1amount.toString().match(/^-?\d+(?:\.\d{0,4})?/)[0]), 4));
                $('#level2member').text(number_format(data.level2member, 0));
                $('#level2amount').text(number_format((data.level2amount.toString().match(/^-?\d+(?:\.\d{0,4})?/)[0]), 4));
                $('#level3member').text(number_format(data.level3member, 0));
                $('#level3amount').text(number_format((data.level3amount.toString().match(/^-?\d+(?:\.\d{0,4})?/)[0]), 4));
                $('#totalmember').text(number_format(data.totalmember, 0));
                $('#totalamount').text(number_format((data.totalamount.toString().match(/^-?\d+(?:\.\d{0,4})?/)[0]), 4));
                
                disable.out();
            },
            error: function (response) {
                if(response.readyState == 4){
                    notiAlert(2, "{{ \Lang::get('error.sessionExpired') }}", '{{\App::getLocale()}}');
                    window.location.reload();
                }
            }
        });
    }
</script>
@stop
