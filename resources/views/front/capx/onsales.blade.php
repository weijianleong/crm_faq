@extends('front.app')

@section('title')
@lang('capx.onsales') | {{ config('app.name') }}
@stop

@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/progress-bar.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/countdown-clock.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/login2.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/animate/css/animate.min.css')}}" />
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('capx.onsales')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('capx.onsales')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            <i class="fa fa-table"></i> @lang('capx.onsales')
                        </div>
                        <div class="card-body p-t-10">
                            <div class=" m-t-25">
                                @if(count($margin) > 0)
                                @foreach ($margin as $item)
                                <?php $fundBalance = ($item->initial_unit - $item->current_unit) / $item->initial_unit * 100; ?>
                                <div style="box-shadow: 0px 0px 30px -5px #989898; padding: 15px; border-radius: 10px; background-color: #fcfdfe; margin-bottom: 25px;">
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.product'): <span style="color: #1b449d;">{{ \App::getLocale() == 'en' ? $item->name_en : $item->name_chs }}</span></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.fundsize'): <span style="color: #1b449d;">{{ number_format($item->initial_unit) }} </span></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.launcheddate'): <span style="color: #1b449d;">{{ $item->launched_date }}</span></div>
                                    </div>
                                    <div class="row" style="margin-bottom: 20px;">
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.price'): <span id="price_{{ $item->symbol }}" style="color: #1b449d;">$ {{ number_format($item->current_price,4) }}</span></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.fundbalance'): <span id="unit_{{ $item->symbol }}" style="color: #1b449d;">{{ number_format($item->current_unit) }} </span></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.salesdate'): <span style="color: #1b449d;">{{ $item->sales_date }} </span></div>
                                    </div>
                                    @if($item->launched_date > date("Y-m-d H:i:s"))
                                    <div class="col-lg-4" style="float: none; margin: 0 auto; text-align: center; padding: 25px 0px 25px 0px;">
                                        <div id="clockdiv_{{ $item->symbol }}" class="clockdivcss">
                                            <div>
                                                <div class="smalltext">@lang('capx.day')</div>
                                                <span class="days"></span>
                                            </div>
                                            <div>
                                                <div class="smalltext">@lang('capx.hour')</div>
                                                <span class="hours"></span>
                                            </div>
                                            <div>
                                                <div class="smalltext">@lang('capx.minute')</div>
                                                <span class="minutes"></span>
                                            </div>
                                            <div>
                                                <div class="smalltext">@lang('capx.second')</div>
                                                <span class="seconds"></span>
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                    <div id="progress_{{ $item->symbol }}">
                                        <p style="width:{{ $fundBalance<4?4:$fundBalance }}%" data-value="{{ number_format($fundBalance,2) }}">&nbsp;</p>
                                        <progress max="100" value="{{ $fundBalance }}" class="html5">
                                            <div class="progress-bar">
                                                <span style="width: 80%">{{ $fundBalance }}</span>
                                            </div>
                                        </progress>
                                    </div>
                                    <div class="row">
                                        <div class="text-center login_bottom col-lg-2" style="padding-top: 5px; border-bottom: none;">
                                            @if($item->current_unit <= 0)
                                                <label class="btn btn-block b_r_20 m-t-10" style="background-color: #ededed; cursor: not-allowed;" >@lang('capx.soldout')</label>
                                            @elseif(date('Y-m-d H:i:s') >= $item->launched_date && date('Y-m-d H:i:s') <= $item->expired_date)
                                                <button  onclick="subscribeForm('{{ \App::getLocale() == 'en' ? $item->name_en : $item->name_chs }}','{{ $item->symbol }}')" id="register_btn" type="button" class="btn btn-block b_r_20 m-t-10 text-white btn-login-hover" style="background-color: #25265E;">@lang('capx.buy')</button>
                                            @else
                                                <label class="btn btn-block b_r_20 m-t-10" style="background-color: #ededed; cursor: not-allowed;" >@lang('capx.upcoming')</label>
                                            @endif
                                        </div>
                                    </div>
                                    @if($item->sales_date > date('Y-m-d H:i:s') && $item->current_unit <= 0)
                                    <div class="col-lg-4" style="float: none; margin: 0 auto; text-align: center; padding: 25px 0px 25px 0px;">
                                        <div style="font-size: 20px; padding-bottom: 15px;">@lang('capx.countdownsalesdate')</div>
                                        <div id="clockdiv_{{ $item->symbol }}" class="clockdivcss">
                                            <div>
                                                <div class="smalltext">@lang('capx.day')</div>
                                                <span class="days"></span>
                                            </div>
                                            <div>
                                                <div class="smalltext">@lang('capx.hour')</div>
                                                <span class="hours"></span>
                                            </div>
                                            <div>
                                                <div class="smalltext">@lang('capx.minute')</div>
                                                <span class="minutes"></span>
                                            </div>
                                            <div>
                                                <div class="smalltext">@lang('capx.second')</div>
                                                <span class="seconds"></span>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    @endif
                                </div>
                                @endforeach
                                @else
                                    <div style="text-align: center; font: bold 2.5em Open Sans,sans-serif; color: #084D8B;">**&nbsp;~~~~~~~~~~~~~~&nbsp;**</div><br>
                                    <div style="text-align: center; font: bold 2.5em Open Sans,sans-serif; color: #084D8B;">敬请期待</div><br>
                                    <div style="text-align: center; font: bold 2.5em Open Sans,sans-serif; color: #084D8B;">Coming Soon</div><br>
                                    <div style="text-align: center; font: bold 2.5em Open Sans,sans-serif; color: #084D8B;">**&nbsp;~~~~~~~~~~~~~~&nbsp;**</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('front.include.modal')
@stop

@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/js/pages/modals.js')}}"></script>
<script type="text/javascript">
    $(document).ready( function () {
        var margin = '{!! $margin !!}';
        var callInterval = 0;
        $.each(JSON.parse(margin), function(key,value){
            if(value.launched_date > '{{ date("Y-m-d H:i:s") }}') initializeClock('clockdiv_'+value.symbol, value.launched_date);
            if(value.sales_date > '{{ date("Y-m-d H:i:s") }}' && value.current_unit <= 0) initializeClock('clockdiv_'+value.symbol, value.sales_date);
            if(value.current_unit > 0) callInterval = 1;
        });

        if(callInterval){
            setInterval(function(){
                $.ajax({
                    url     :   "{{ route('capx.getAllMargin', ['lang' => \App::getLocale()]) }}",
                    method  :   "post",
                    headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
                    success : function(response){
                        $.each(response.margin, function(key,value){
                            $('#price_'+value.symbol).text('$ '+number_format(value.current_price, 4));
                            $('#unit_'+value.symbol).text(number_format(value.current_unit));

                            var progress = "";
                            var fundBalance = (value.initial_unit - value.current_unit) / value.initial_unit * 100; 
                            var bar = fundBalance<4?4:fundBalance;
                            progress = '<p style="width:'+bar+'%" data-value="'+number_format(fundBalance,2)+'">&nbsp;</p>';
                            progress += '<progress max="100" value="'+fundBalance+'" class="html5">';
                            progress += '<div class="progress-bar">';
                            progress += '<span style="width: 80%">'+fundBalance+'</span>';
                            progress += '</div>';
                            progress += '</progress>';

                            $('#progress_'+value.symbol).empty().append(progress);
                        });
                    }
                });
            }, 5*1000);
        }
    });

    function subscribeForm(productName, symbol) {
        var modelContent = '';

        modelContent = '<div style="font-size: 20px; padding: 15px;">@lang("capx.subscribeCoin"): '+productName+'<br/><br/>';
        modelContent += '<div class="input-group input-group-prepend col-lg-10" style="padding-left:0px;">';
        modelContent += '<span class="input-group-text border-right-0 rounded-left" style="color:#000;width:150px;" id="pay_title">@lang("common.cashTitle")</span>';
        modelContent += '<input class="form-control" type="text" value="$ {{ $cashBalance }}" disabled="" id="displaywallet"></div>';
        modelContent += '<div class="input-group input-group-prepend col-lg-10" style="padding-left:0px;">';
        modelContent += '<span class="input-group-text border-right-0 rounded-left" style="color:#000;width:150px;">@lang("capx.subscribeAmount")</span>';
        modelContent += '<input id="amount" class="form-control" type="text" value="" ><span style="padding-top:5px;">&nbsp; x 500 = </span>&nbsp;<span id="multipleamount" style="padding-top:5px;">0</span></div>';
        modelContent += '<br/><button type="button" class="btn btn-primary glow_button" data-dismiss="modal" onclick="submitSubscribe(\''+symbol+'\')">@lang("common.confirm")</button>';
        modelContent += '</div>';
        $('#modalContent').empty().append(modelContent);
        $('#modalZoomIn').click();

        $('#amount').on('keyup', function(){
            if($('#amount').val()){
                var value = $('#amount').val()*500;
                $('#multipleamount').text(number_format(value));
            }else{
                $('#multipleamount').text(0);
            }
        });
    }

    function submitSubscribe(symbol) {
        var amount = $('#amount').val();
        var url = "{{ route('capx.subscribeMargin', ['lang' => \App::getLocale()]) }}";
        var buttonDisplay = '{"Format":['+'{"confirmButtonText":'+'"@lang('common.confirm')"'+',"cancelButtonText":'+'"@lang('common.cancel')"'+',"setTittle":'+'"@lang('settings.secret')"'+',"error":'+'"@lang('error.securityPasswordError')"'+'}]}';
        var obj = {symbol: symbol, amount: amount};
        var final = { Data : obj};
        var data = JSON.stringify(final);
        swalAlert(url,buttonDisplay,data);
    }

    function getTimeRemaining(endtime) {
        var b = endtime.split(/\D+/);
        var newEndTime = b[1]+"/"+b[2]+"/"+b[0]+" "+b[3]+":"+b[4]+":"+b[5];

        var t = Date.parse(newEndTime) - Date.parse(new Date());
        var seconds = Math.floor((t / 1000) % 60);
        var minutes = Math.floor((t / 1000 / 60) % 60);
        var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        var days = Math.floor(t / (1000 * 60 * 60 * 24));
        
        if(days == 0 && hours == 0 && minutes == 0 && seconds == 0) window.location.reload();
        
        return {
                'total': t,
                'days': days,
                'hours': hours,
                'minutes': minutes,
                'seconds': seconds
                };
    }

    function initializeClock(id, endtime) {
        var clock = document.getElementById(id);
        var daysSpan = clock.querySelector('.days');
        var hoursSpan = clock.querySelector('.hours');
        var minutesSpan = clock.querySelector('.minutes');
        var secondsSpan = clock.querySelector('.seconds');

        function updateClock() {
            var t = getTimeRemaining(endtime);

            daysSpan.innerHTML = t.days;
            hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
            minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
            secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

            if (t.total <= 0) {
                clearInterval(timeinterval);
            }
        }

        updateClock();
        var timeinterval = setInterval(updateClock, 1000);
    }
</script>
@stop