@extends('front.app')

@section('title')
  @lang('capx.mytrcap') | {{ config('app.name') }}
@stop

@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/widgets.css')}}">
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/countdown-clock.css')}}" />
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('capx.mytrcap')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('capx.mytrcap')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>

    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">@lang('capx.mytrcap')
                        </div>
                        <div class="card-body m-t-15" style="margin-bottom: 15px;">
                            <div id="refresh" class="row" style="text-align: right; display: block;"><i class="fa fa-refresh" style="background-color: #ccccff; border: 2px solid #00007f; border-radius: 15px; padding: 6px; margin: 0px 15px; cursor: pointer;"></i></div>
                        </div>
                        <div class="card-body row" style="margin-bottom: 20px;">
                            @if(date('d') <= 15)
                            <div class="col-lg-4" style="float: none; margin: 0 auto 10px; box-shadow: 0px 0px 30px 0px #989898; padding: 15px; border-bottom: 8px solid #FFD700; border-top: 8px solid #FFD700; border-radius: 100px; background-color: #ffee81;">
                                <div class="row justify-content-md-center" style="text-align: center; display: block; font-size: 20px;">@lang('capx.trcappool')</div>
                                <div class="row justify-content-md-center" style="text-align: center; display: block; font-size: 15px;">( @if(\App::getLocale() == 'en') {{ date("F", strtotime('-1 month')) }} @else {{ date("n", strtotime('-1 month')) }}@lang('capx.month') @endif )</div>
                                <?php $oldSplit = explode('.', number_format($oldPoolAmount, 4)); ?>
                                <br>
                                <div class="row justify-content-md-center" style="text-align: center; display: block; font-size: 35px; color: red;">$ {{ $oldSplit[0] }}<span style="font-size: 25px;">.{{ $oldSplit[1] }}</span></div>
                            </div>
                            @endif
                            <div class="col-lg-4" style="float: none; margin: 0 auto 10px; box-shadow: 0px 0px 30px 0px #989898; padding: 15px; border-bottom: 8px solid #FFD700; border-top: 8px solid #FFD700; border-radius: 100px; background-color: #ffee81;">
                                <div class="row justify-content-md-center" style="text-align: center; display: block; font-size: 20px;">@lang('capx.trcappool')</div>
                                <div class="row justify-content-md-center" style="text-align: center; display: block; font-size: 15px;">( @if(\App::getLocale() == 'en') {{ date("F") }} @else {{ date("n") }}@lang('capx.month') @endif )</div>
                                <?php $split = explode('.', number_format($poolAmount, 4)); ?>
                                <br>
                                <div class="row justify-content-md-center" style="text-align: center; display: block; font-size: 35px; color: #32CD32;">$ {{ $split[0] }}<span style="font-size: 25px;">.{{ $split[1] }}</span></div>
                            </div>
                        </div>

                        @if($coin->launched_date > date("Y-m-d H:i:s"))
                        <div class="col-lg-4" style="float: none; margin: 0 auto; text-align: center; padding: 0px 0px 25px 0px;">
                            <div style="font-size: 20px; padding-bottom: 15px;">@lang('capx.countdownselldate')</div>
                            <div id="clockdiv" class="clockdivcss">
                                <div>
                                    <div class="smalltext">@lang('capx.day')</div>
                                    <span class="days"></span>
                                </div>
                                <div>
                                    <div class="smalltext">@lang('capx.hour')</div>
                                    <span class="hours"></span>
                                </div>
                                <div>
                                    <div class="smalltext">@lang('capx.minute')</div>
                                    <span class="minutes"></span>
                                </div>
                                <div>
                                    <div class="smalltext">@lang('capx.second')</div>
                                    <span class="seconds"></span>
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="card-body m-t-20" style="margin-bottom: 20px;">
                            <div class="row justify-content-md-center">
                                <div class="col-sm-6 col-lg-3 media_max_991">
                                    <div style="box-shadow: 0px 0px 30px -5px #989898; padding: 15px; border-top: 8px solid #1b449d; border-radius: 5px; background-color: #fcfdfe;">
                                        <p style="font-size: 15px;">@lang('capx.units'):</p>
                                        <p id="unit" style="text-align: center; font-size: 25px; font-weight: 1;">{{ number_format(floor($coinUnit * 10000) / 10000, 4) }}</p>
                                    </div>
                                </div>

                                <div class="fa-icon d-none d-sm-block" style="padding: 25px 15px 0px 15px; font-size: 60px;"><i class="fa fa-5x"></i>&times; </div>
                                <div class="fa-icon d-md-none" style="padding-left: 45%;padding-top: 10%; font-size: 60px;"><i class="fa fa-5x"></i>&times; </div>

                                <div class="col-sm-6 col-lg-3 media_max_991">
                                    <div style="box-shadow: 0px 0px 30px -5px #989898; padding: 15px; border-top: 8px solid #1b449d; border-radius: 5px; background-color: #fcfdfe;">
                                        <p style="font-size: 15px;">@lang('capx.price'):</p>
                                        <p id="price" style="text-align: center; font-size: 25px; font-weight: 1;">$ {{ number_format($coin->price, 4) }}</p>
                                    </div>
                                </div>

                                <div class="fa-icon d-none d-sm-block" style="padding: 25px 15px 0px 15px; font-size: 60px;"><i class="fa fa-5x"></i>&asymp; </div>
                                <div class="fa-icon d-md-none" style="padding-left: 45%;padding-top: 10%; font-size: 60px;"><i class="fa fa-5x"></i>&asymp; </div>

                                <div class="col-sm-6 col-lg-3 media_max_991">
                                    <div style="box-shadow: 0px 0px 30px -5px #989898; padding: 15px; border-top: 8px solid #1b449d; border-radius: 5px; background-color: #ecfbfe;">
                                        <p style="font-size: 15px;">@lang('capx.value'):</p>
                                        <p id="marginValue" style="text-align: center; font-size: 25px; font-weight: 1;">$ {{ number_format(($coinUnit*$coin->price), 2) }}</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer_scripts')
<script type="text/javascript">
    $(document).ready( function () {
        var coin = JSON.parse('{!! json_encode($coin) !!}');
        if(coin.launched_date > '{{ date("Y-m-d H:i:s") }}') initializeClock('clockdiv', coin.launched_date);

        $('#refresh').on('click', function(event) {
            window.location.reload();
        });
    });

    function getTimeRemaining(endtime) {
        var b = endtime.split(/\D+/);
        var newEndTime = b[1]+"/"+b[2]+"/"+b[0]+" "+b[3]+":"+b[4]+":"+b[5];

        var t = Date.parse(newEndTime) - Date.parse(new Date());
        var seconds = Math.floor((t / 1000) % 60);
        var minutes = Math.floor((t / 1000 / 60) % 60);
        var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        var days = Math.floor(t / (1000 * 60 * 60 * 24));
        
        if(days == 0 && hours == 0 && minutes == 0 && seconds == 0) window.location.reload();
        
        return {
                'total': t,
                'days': days,
                'hours': hours,
                'minutes': minutes,
                'seconds': seconds
                };
    }

    function initializeClock(id, endtime) {
        var clock = document.getElementById(id);
        var daysSpan = clock.querySelector('.days');
        var hoursSpan = clock.querySelector('.hours');
        var minutesSpan = clock.querySelector('.minutes');
        var secondsSpan = clock.querySelector('.seconds');

        function updateClock() {
            var t = getTimeRemaining(endtime);

            daysSpan.innerHTML = t.days;
            hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
            minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
            secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

            if (t.total <= 0) {
                clearInterval(timeinterval);
            }
        }

        updateClock();
        var timeinterval = setInterval(updateClock, 1000);
    }
</script>
@stop
