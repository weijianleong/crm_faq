<a href="{{ route('bankinfo.detail', ['lang'=>\App::getLocale(),'id'=> $model->id] ) }}" class="btn btn-primary glow_button">
  <i class="fa fa-share-square-o"></i> @lang('withdraw.edit')
</a>
@if($model->read == "1")
<i class="fa fa-circle" style="position: absolute; color: red; font-size: 8px;"></i>
@endif
