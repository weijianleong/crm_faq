<?php
    use App\Models\Package;
    use App\Repositories\MemberRepository;
    $user = \Sentinel::getUser();
    $memberRepo = new MemberRepository;
    $member = $memberRepo->findByUsername(trim($user->username));
    $member->load('wallet');
    $member->load('detail');
    //$member->load('shares');
    $member->load('package');
    ?>

@extends('front.app')

@section('title')
  @lang('success.idtitle') | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
<li><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
<li>@lang('breadcrumbs.settingsID')</li>
    <li class="active">@lang('breadcrumbs.uploadSuccess')</li>
  </ul>
@stop

@section('content')
  <main>
    @include('front.include.sidebar')
    <div class="main-container">
      @include('front.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section>
          <div class="page-header">
            <h1><i class="md md-done-all"></i> @lang('success.idtitle')</h1>
          </div>

          <div class="row m-b-40">
            <div class="col-md-8">
              <div class="well white text-center">
                <h1 class="theme-text text-uppercase w300">@lang('success.text1')</h1>
                <h3>@lang('success.idtext2')</h3>
                <a href="{{ route('settings.id', ['lang' => \App::getLocale()]) }}" class="btn btn-primary btn-block m-t-50">
                  <i class="md md-navigate-before"></i> @lang('success.idback')
                </a>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>
@stop
