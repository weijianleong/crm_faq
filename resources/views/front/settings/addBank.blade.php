@extends('front.app')

@section('title')
  @lang('withdraw.addBank') | {{ config('app.name') }}
@stop

@section('header_styles')
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/calendar_custom.css')}}"/>
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">

    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('withdraw.addBank')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                        <li class=" breadcrumb-item">
                           <a href="{{ route('settings.BankInfo', ['lang' => \App::getLocale()]) }}">
                               @lang('withdraw.bankList')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('withdraw.addBank')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>

    <div class="outer">
        <div class="inner bg-container">
            <div class="card m-t-35">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12 m-t-25">
                            <div>
                                <ul class="nav nav-inline view_user_nav_padding" id="content-tab">
                                    <li class="nav-item card_nav_hover">
                                        <a class="nav-link active" href="#bank" id="home-tab" data-toggle="tab" aria-expanded="true">@lang('settings.subTitle2')</a>
                                    </li>
                                    <li class="nav-item card_nav_hover">
                                        <a class="nav-link " href="#IDpage" id="hats-tab" data-toggle="tab" aria-expanded="true"></a>
                                    </li>
                                </ul>

                                <div id="clothing-nav-content" class="tab-content m-t-10">
                                    <div role="tabpanel" class="tab-pane fade show active" id="bank">  
                                            <fieldset>
                                                <!-- Name bank-->
                                                <div class="form-group row ">
                                                    <div class="col-lg-3 text-lg-right">
                                                        <label for="name3" class="col-form-label">@lang('settings.bank.name')</label>
                                                    </div>

                                                    <div class="col-lg-8">
                                                        <div class="input-group input-group-prepend">
                                                            <span class="input-group-text border-right-0 rounded-left">
                                                                <i class="fa fa-bank"></i>
                                                            </span>
                                                            <select class="form-control hide_search" tabindex="7" name="bank_name" id="bank_name">
                                                                @foreach($bankList as $bankLists)
                                                                    <option value="{{ $bankLists->bank_value }}" >{{ $bankLists->bank_name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- open city-->
                                                <div class="form-group row  ">
                                                    <div class="col-lg-3 text-lg-right">
                                                        <label for="confirm3" class="col-form-label">@lang('withdraw.openBank')</label>
                                                    </div>
                                                    <div class="col-lg-3"> 
                                                        <div class="input-group input-group-prepend">
                                                            <span class="input-group-text border-right-0 rounded-left">
                                                                <i class="fa fa-bank"></i>
                                                            </span>
                                                            <select class="form-control hide_search" tabindex="7" name="countryName" id="countryName" onchange="ChangeCountry()">
                                                                    <option value="" >@lang('withdraw.country')</option>
                                                                    @foreach($country as $countrys)
                                                                        <option value="{{ $countrys->countrycode }}" >{{ $countrys->countryname }}</option>
                                                                    @endforeach
                                                            </select>
                                                        </div>  
                                                    </div>

                                                    <div class="col-lg-3"> 
                                                        <div class="input-group input-group-prepend">
                                                            <span class="input-group-text border-right-0 rounded-left">
                                                                <i class="fa fa-bank"></i>
                                                            </span>
                                                            <select class="form-control hide_search" tabindex="7" name="stateName" id="state"  onchange="ChangeState()">
                                                                <option value="" id="id_state">@lang('withdraw.state')</option>
                                                            </select>
                                                        </div>  
                                                    </div>

                                                    <div class="col-lg-2"> 
                                                        <div class="input-group input-group-prepend">
                                                            <span class="input-group-text border-right-0 rounded-left">
                                                                <i class="fa fa-bank"></i>
                                                            </span>
                                                            <select class="form-control hide_search" tabindex="7" name="cityName" id="city"  onchange="">
                                                                <option value="" id="id_city">@lang('withdraw.city')</option>
                                                            </select>
                                                        </div>  
                                                    </div>
                                                </div>
                                                <!-- sub_bank-->
                                                <div class="form-group row">
                                                    <div class="col-lg-3 text-lg-right">
                                                        <label for="confirm3" class="col-form-label">@lang('withdraw.subBank')</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="input-group input-group-prepend">
                                                            <span class="input-group-text border-right-0 rounded-left">
                                                                <i class="fa fa-bank"></i>
                                                            </span>
                                                            <input type="text" name="sub_bank" id="sub_bank" class="form-control" placeholder="" value="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- bank.number-->
                                                <div class="form-group row">
                                                    <div class="col-lg-3 text-lg-right">
                                                        <label for="email3" class="col-form-label">@lang('settings.bank.number')</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="input-group input-group-prepend">
                                                            <span class="input-group-text border-right-0 rounded-left">
                                                                <i class="fa  fa-bank"></i>
                                                            </span>
                                                            <input type="text" id="bank_no" name="bank_no" value="" class="form-control" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- bank.address-->
                                                <div class="form-group row">
                                                    <div class="col-lg-3 text-lg-right">
                                                        <label for="confirm3" class="col-form-label">@lang('settings.bank.address')</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="input-group input-group-prepend">
                                                            <textarea class="form-control"  name="bank_address" id="bank_address" rows="4" placeholder="@lang('settings.bank.address')"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- last name-->
                                                <div class="form-group row">
                                                    <div class="col-lg-9 ml-auto">
                                                        <button class="btn btn-primary layout_btn_prevent" onclick="addBank()">@lang('common.submit')</button>
                                                        <button class="btn btn-secondary layout_btn_prevent">@lang('common.cancel')</button>
                                                    </div>
                                                </div>
                                            </fieldset>  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer_scripts')

    <!--Plugin scripts-->
    <script type="text/javascript" src="{{asset('assets/vendors/bootstrap_calendar/js/bootstrap_calendar.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/pages/mini_calendar.js')}}"></script>
    <!--End of Page level scripts-->
    <script type="text/javascript">
            function ChangeCountry(){
                var disable = loadingDisable();
                var language = "{{ $language }}";
                var dataUrl = "{{ route('bankinfo.getState', ['lang' => \App::getLocale()]) }}";

                $.ajax({
                    url     : dataUrl,
                    method  : 'post',
                    data    : {

                        countryname  : $("#countryName").val(),
                        language : language

                       
                    },
                    headers:
                    {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success : function(response){
                        disable.out();
                        //$("select[name='stateName'").html('');
                        document.getElementById('state').innerHTML= response.options;
                        document.getElementById('city').innerHTML= response.city;
                        // $("select[name='stateName'").html(response.options);
                        // $("select[name='cityName'").html(response.city);

                    }
                });
            }
            function ChangeState(){
                var disable = loadingDisable();
                var language = "{{ $language }}";
                var dataUrl = "{{ route('bankinfo.getCity', ['lang' => \App::getLocale()]) }}";
                

                $.ajax({
                    url     : dataUrl,
                    method  : 'post',
                    data    : {

                        statename  : $("#state").val(),
                        language : language

                       
                    },
                    headers:
                    {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success : function(response){
                        disable.out();
                        // $("select[name='cityName'").html('');
                        // $("select[name='cityName'").html(response.options);
                        document.getElementById('city').innerHTML= response.options;
                        

                    }
                });
            }
            function addBank(){
                var bank_name = $('#bank_name').val();
                var countryName = $('#countryName').val();
                var state = $('#state').val();
                var city = $('#city').val();
                var sub_bank = $('#sub_bank').val();
                var bank_no = $('#bank_no').val();
                var bank_address = $('#bank_address').val();

                var obj = {bank_name: "bank_name", countryName: "countryName", state: "state", city: "city", sub_bank: "sub_bank",bank_no: "bank_no",bank_address: "bank_address"};
                var final = { Id : obj};
                var Id = JSON.stringify(final);
                var ans = checkValue(Id);

                if(ans!=0){
                    return false;
                }
                
                var url = "{{ route('bankinfo.addBank', ['lang' => \App::getLocale()]) }}";
                var buttonDisplay = '{"Format":['+'{"confirmButtonText":'+'"@lang('common.confirm')"'+',"cancelButtonText":'+'"@lang('common.cancel')"'+',"setTittle":'+'"@lang('settings.secret')"'+',"error":'+'"@lang('error.securityPasswordError')"'+'}]}';
                var obj = {bank_name: bank_name, countryName: countryName, state: state, city: city, sub_bank: sub_bank,bank_no: bank_no,bank_address:bank_address};
                var final = { Data : obj};
                var data = JSON.stringify(final);
                swalAlert(url,buttonDisplay,data);
                
                return false;
            }
    </script>
@stop
