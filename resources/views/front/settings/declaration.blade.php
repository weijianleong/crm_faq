<?php
    use App\Repositories\SharesRepository;
    use App\Repositories\MemberRepository;
    use Carbon\Carbon;
    use Illuminate\Support\Str;
    $sharesRepo = new SharesRepository;
    $memberRepo = new MemberRepository;
    $announcementModel = new \App\Models\Announcement;
    
    $member = $memberRepo->findByUsername(trim($user->username));
    $member->load('wallet');
    $member->load('detail');
    //$member->load('shares');
    $member->load('package');
    
    $declareform = $member->declare_form;
    $declareform2 = $member->declare_form2;
   
    
    $destinationPath = config('misc.declarePath');
    

    
  
    
?>

@extends('front.app')

@section('title')
  @lang('settings.title4') | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
    <li class="active">@lang('breadcrumbs.settingsDeclare')</li>
  </ul>
@stop

@section('content')
  <main>
    @include('front.include.sidebar')
    <div class="main-container">
      @include('front.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section>
          <div class="page-header">
            <h1><i class="md md-settings"></i> @lang('settings.title4')</h1>

          </div>

          <div class="row m-b-40">
            <div class="col-md-8">
              <div class="well white">
                <div class="table-responsive">
                        <table class="table table-hover table-striped">
                        <tr>
                            <td class="theme-text">@lang('settings.download')</td>
                            <td>:</td>

                            <td>

                            <a href="{{ $destinationPath.'声明书 v1.4  中文.pdf' }}" target="blank">声明书 v1.4  中文.pdf</a>
                        
                                                                                                    
                        </td>

                          </tr>
                         <tr>
                            <td class="theme-text">@lang('settings.download')</td>
                            <td>:</td>

                            <td>

                            <a href="{{ $destinationPath.'Terms & Conditions v1.4   ENGLISH.pdf' }}" target="blank">Terms & Conditions v1.4   ENGLISH.pdf</a>
                                                                                                
                        </td>
                        </tr>
                       <tr>
                            <td class="theme-text">@lang('settings.download')</td>
                            <td>:</td>

                            <td>

                            <a href="{{ $destinationPath.'Terms & Conditions v1.4   VIETNAM.pdf' }}" target="blank">Terms & Conditions v1.4   VIETNAM.pdf</a>
                                                                                                
                        </td>

                          </tr>

                          <tr>
                            <td class="theme-text">@lang('settings.title5')</td>
                            <td>:</td>
                            @if (!is_null($member->declare_form))
                            <td><a href="{{ $destinationPath.$declareform }}" target="blank">@lang('settings.title8')</a>
                    
                            @endif

                            @if (!is_null($member->declare_form2))
                            <a href="{{ $destinationPath.$declareform2 }}" target="blank">@lang('settings.title9')</a>

                            @endif
                            </td>
                          </tr>


                        </table>
                      </div>

                <form class="form-floating" name="registerForm" id="registerForm"  action="{{ route('account.postDeclare', ['lang' => \App::getLocale()]) }}" method="post" enctype="multipart/form-data" data-parsley-validate>


                  <fieldset>


                    <div class="form-group">
              <label class="control-label" for="inputDeclarelabel">@lang('settings.upload')</label><BR>
                 @lang('settings.title8') <input type="file" name="inputDeclare" class="form-control" id="inputDeclare" required="">
               @lang('settings.title9') <input type="file" name="inputDeclare2" class="form-control" id="inputDeclare2">

        </div>




<input type="hidden" value="{{ csrf_token() }}" name="_token">
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">
                        <span class="btn-preloader">
                          <i class="md md-cached md-spin"></i>
                        </span>
                        <span>@lang('common.submit')</span>
                      </button>
                      <button type="reset" class="btn btn-default">@lang('common.cancel')</button>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>
@stop
