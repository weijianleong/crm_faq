<option value="">@lang('withdraw.selectState')</option>
@if(!empty($state))
  @foreach($state as $states => $value )
    <option value="{{ $states }}">{{ $value }}</option>
  @endforeach
@endif