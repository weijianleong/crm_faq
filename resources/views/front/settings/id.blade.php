<?php
    use App\Repositories\SharesRepository;
    use App\Repositories\MemberRepository;
    use Carbon\Carbon;
    use Illuminate\Support\Str;
    $sharesRepo = new SharesRepository;
    $memberRepo = new MemberRepository;
    $announcementModel = new \App\Models\Announcement;
    
    $member = $memberRepo->findByUsername(trim($user->username));
    $member->load('wallet');
    $member->load('detail');
    //$member->load('shares');
    $member->load('package');
    
    $idFront = $member->id_front;
    $idBack = $member->id_back;
    
    $destinationPath = config('misc.imagePath');
    
    $internalPath = public_path('/ID/');
    
    if(count(explode('.',$idFront)) == 1)
    {
        $filename = $internalPath.'Front_'.$idFront;
     
        if (file_exists($filename.'.JPG'))
        {
            $idFront = 'Front_'.$idFront.'.JPG';
            $idBack = 'Back_'.$idBack.'.JPG';
            
        }
        
        if (file_exists($filename.'.jpg'))
        {
            $idFront = 'Front_'.$idFront.'.jpg';
            $idBack = 'Back_'.$idBack.'.jpg';
            
        }
        
        if (file_exists($filename.'.jpeg'))
        {
            $idFront = 'Front_'.$idFront.'.jpeg';
            $idBack = 'Back_'.$idBack.'.jpeg';
            
        }
            if (file_exists($filename.'.png'))
            {
                $idFront = 'Front_'.$idFront.'.png';
                $idBack = 'Back_'.$idBack.'.png';
                
            }
            
            if (file_exists($filename.'.PNG'))
            {
                $idFront = 'Front_'.$idFront.'.PNG';
                $idBack = 'Back_'.$idBack.'.PNG';
                
            }
    }
    
  
    
?>

@extends('front.app')

@section('title')
  @lang('settings.title3') | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
    <li class="active">@lang('breadcrumbs.settingsID')</li>
  </ul>
@stop

@section('content')
  <main>
    @include('front.include.sidebar')
    <div class="main-container">
      @include('front.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section>
          <div class="page-header">
            <h1><i class="md md-settings"></i> @lang('settings.title3')</h1>

          </div>

          <div class="row m-b-40">
            <div class="col-md-6">
              <div class="well white">
                <div class="table-responsive">
                        <table class="table table-hover table-striped">
                          <tr>
                            <td class="theme-text">@lang('settings.idfront')</td>
                            <td>:</td>
                            <td><a href="{{ route('amazon.read', ['folder' => 'ID', 'filename' => $idFront, 'lang' => \App::getLocale()]) }}" target="blank">@lang('settings.onclick')</a></td>
                            <!-- <td><a href="{{ $destinationPath.$idFront }}" target="blank">@lang('settings.onclick')</a></td> -->
                          </tr>

                          <tr>
                            <td class="theme-text">@lang('settings.idback')</td>
                            <td>:</td>
                            <td><a href="{{ route('amazon.read', ['folder' => 'ID', 'filename' => $idBack, 'lang' => \App::getLocale()]) }}" target="blank">@lang('settings.onclick')</a></td>
                            <!-- <td><a href="{{ $destinationPath.$idBack }}" target="blank">@lang('settings.onclick')</a></td> -->
                          </tr>

                        </table>
                      </div>

                <form class="form-floating" name="registerForm" id="registerForm"  action="{{ route('account.postID', ['lang' => \App::getLocale()]) }}" method="post" enctype="multipart/form-data" data-parsley-validate>


                  <fieldset>


                    <div class="form-group">
              <label class="control-label" for="inputIDfront">@lang('settings.idfront')</label><BR>
                  <input type="file" name="inputIDfront" class="form-control" id="inputIDfront" required="">

        </div>

        <div class="form-group">
              <label class="control-label" for="inputIDback">@lang('settings.idback')</label><BR>
                  <input type="file" name="inputIDback" class="form-control" id="inputIDback" required="">
        </div>


<input type="hidden" value="{{ csrf_token() }}" name="_token">
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">
                        <span class="btn-preloader">
                          <i class="md md-cached md-spin"></i>
                        </span>
                        <span>@lang('common.submit')</span>
                      </button>
                      <button type="reset" class="btn btn-default">@lang('common.cancel')</button>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>
@stop
