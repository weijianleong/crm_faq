<?php
    use App\Repositories\MemberRepository;
    $memberRepo = new MemberRepository;
    $member = $memberRepo->findByUsername(trim($user->username));
    $idFront = $member->id_front;
    $idBack = $member->id_back;
?>

@extends('front.app')

@section('title')
  @lang('sidebar.settingsLink1') | {{ config('app.name') }}
@stop

@section('header_styles')
    <!--Plugin css-->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/fullcalendar/css/fullcalendar.min.css')}}"/>
    <!--End off plugin css-->
    <!--Page level css-->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/timeline2.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/calendar_custom.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/profile.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/gallery.css')}}"/>
    <!--end of page level css-->
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li class="active"><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
  </ul>
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    @if($user->is_rstpass == 0)
        <script>
            window.onload = function() {
                  alertRstpass();
            };
        </script>
    @endif

    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('settings.title1')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('settings.title1')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>

    <div class="outer">
        <div class="inner bg-container">
            <div class="card m-t-35">
                <div class="card-body">
                    <div class="row">

                        <div class="col-lg-6 m-t-35">
                            <div class="text-center">
                                <div class="form-group">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumb_zoom zoom admin_img_width">
                                            @if (!empty($member->detail->profile_pic_64))
                                                <img src="data:image/;base64,{{ $member->detail->profile_pic_64 }}" alt="admin" class="admin_img_width">
                                            @else
                                                <img src="{{asset('assets/img/profile.png')}}" alt="admin" class="admin_img_width">       
                                            @endif
                                            
                                        </div>



                                        <div class="fileinput-preview fileinput-exists thumb_zoom zoom admin_img_width"></div>
                                        <div class="btn_file_position">
                                                    <span class="btn btn-primary btn-file">
                                                        <span class="fileinput-new" >@lang('settings.plzChose')</span>
                                                        <span class="fileinput-exists" >@lang('common.edit')</span>
                                                        <input type="file" name="Changefile" id="Changefile">
                                                        
                                                    </span>

                                            <a href="#" class="btn btn-warning fileinput-exists" onclick="uploadPic()">@lang('settings.uploadProfile')</a>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg text-lg-right">*@lang('settings.uploadProfileformat') </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg text-lg-right">*@lang('settings.uploadProfilesize')</div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                        
                        <div class="col-lg-6 m-t-25">
                            <div>
                                <ul class="nav nav-inline view_user_nav_padding" id="content-tab">
                                    <li class="nav-item card_nav_hover">
                                        <a class="nav-link active" href="#user" id="home-tab"
                                           data-toggle="tab" aria-expanded="true">@lang('sidebar.settingsLink1')</a>
                                    </li>

                                    <li class="nav-item card_nav_hover">
                                        <a class="nav-link " href="#IDpage" id="hats-tab"
                                           data-toggle="tab" aria-expanded="true">@lang('sidebar.IDTitle')</a>
                                    </li>

                                    <li class="nav-item card_nav_hover">
                                        <a class="nav-link " href="#ChangePass" id="hats-tab"
                                           data-toggle="tab" aria-expanded="true">@lang('settings.changepass')</a>
                                    </li>
                                </ul>

                                <div id="clothing-nav-content" class="tab-content m-t-10">
                                    <div role="tabpanel" class="tab-pane fade show active" id="user">
                                        <table class="table" id="users">
                                            <tr>
                                                <td>@lang('settings.name')</td>
                                                <td class="inline_edit">
                                                    <span >{{ $member->user->first_name }}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>@lang('settings.id')</td>
                                                <td>
                                                    <span >{{ $member->detail->identification_number }}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>@lang('settings.email')</td>
                                                <td>
                                                    <span >{{ $member->user->email }}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>@lang('settings.nationality')</td>
                                                <td>
                                                    <span >{{ $member->detail->nationality }}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>@lang('settings.phone1')</td>
                                                <div>
                                                    <td id="phone1">
                                                        <!-- <span class="editable" data-title="Edit City"  >{{ $member->detail->phone1 }}</span> -->
                                                        <span>{{ $member->detail->phone1 }}</span>
                                                    </td>
                                                </div>
                                            </tr>
                                        </table>

                                        <div class="col-sm-6 m-t-35">
                                              <button type="button" class="btn btn-primary" onclick = "UpdateInfo()">
                                                <span class="btn-preloader">
                                                  <i class="md md-cached md-spin"></i>
                                                </span>
                                                <span>@lang('common.submit')</span>
                                              </button>
                                        </div>
                                    </div>

                                    <div role="tabpanel" class="tab-pane fade" id="IDpage">
                                        <table class="table" id="tab2">
                                            <tr>
                                                <td>@lang('settings.idfront')</td>
                                                <td class="inline_edit">
                                                    <a href="{{ route('amazon.read', ['folder' => 'ID', 'filename' => $idFront, 'lang' => \App::getLocale()]) }}" target="blank" data-toggle="tooltip" data-placement="top" title="@lang('settings.checkidfront')">
                                                        <i class="fa fa-eye text-success"></i>&nbsp;@lang('settings.onclick')
                                                    </a>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>@lang('settings.idback')</td>
                                                <td class="inline_edit">
                                                    <a href="{{ route('amazon.read', ['folder' => 'ID', 'filename' => $idBack, 'lang' => \App::getLocale()]) }}" target="blank" data-toggle="tooltip" data-placement="top" title="@lang('settings.checkidback')">
                                                        <i class="fa fa-eye text-success"></i>&nbsp;@lang('settings.onclick')
                                                    </a>
                                                </td>
                                            </tr>

                                            @if($member->process_status!='Y')
                                                <tr>
                                                    <td>@lang('settings.upidFront')</td>
                                                    <td>
                                                        <a href="#" data-toggle="tooltip"  onclick="uploadFront()">
                                                            <i class="fa fa-upload"></i>&nbsp; @lang('settings.plzChose')
                                                        </a>
                                                        <input type="file" name="inputIDfront"  class="form-control" id="inputIDfront"  hidden="" onchange="showFrontMsg()">
                                                        &nbsp;
                                                        <span id="showFront"></span>
                                                        
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>@lang('settings.upidBack')</td>
                                                    <td>
                                                        <a href="#" data-toggle="tooltip"  onclick="uploadBack()">
                                                            <i class="fa fa-upload"></i>&nbsp; @lang('settings.plzChose')
                                                        </a>
                                                        <input type="file" name="inputIDback"  class="form-control" id="inputIDback" hidden="" onchange="showBackMsg()">
                                                        &nbsp;
                                                        <span id="showBack"></span>
                                                    </td>
                                                </tr>
                                            @endif
                                        </table>

                                        @if($member->process_status!='Y')
                                            <div class="col-sm-6 m-t-35">
                                                  <button type="button" class="btn btn-primary" onclick = "uploadID()" disabled="" id="ID_btn">
                                                    <span class="btn-preloader">
                                                      <i class="md md-cached md-spin"></i>
                                                    </span>
                                                    <span>@lang('common.submit')</span>
                                                  </button>
                                            </div>
                                        @endif
                                    </div>

                                    <div role="tabpanel" class="tab-pane fade" id="ChangePass">
                                        <table class="table" id="users">
                                            <div class="col-lg-6 input_field_sections">
                                                <h5>@lang('settings.password')</h5>
                                                <input type="password" class="form-control" id="Password1" minlength="8" maxlength="20" onchange=""/>
                                                <span id="Password1_alert" style="color: red"></span>
                                            </div>
                                            <div class="col-lg-6 input_field_sections">
                                                <h5>@lang('settings.repassword')</h5>
                                                <input type="password" class="form-control" id="rePassword1"/>
                                                <span id="rePassword1_alert" style="color: red"></span>
                                            </div>
                                            <div class="col-lg-6 input_field_sections">
                                                <h5>@lang('settings.newSecret')</h5>
                                                <input type="password" class="form-control" id="Password2"/>
                                                <span id="Password2_alert" style="color: red"></span>
                                            </div>
                                            <div class="col-lg-6 input_field_sections">
                                                <h5>@lang('settings.reNewSecret')</h5>
                                                <input type="password" class="form-control" id="rePassword2"/>
                                                <span id="rePassword2_alert" style="color: red"></span>
                                            </div>

                                        </table>

                                        <div class="col-sm-6 m-t-35">
                                              <button type="button" class="btn btn-primary" onclick = "UpdatePassword()" >
                                                <span class="btn-preloader">
                                                  <i class="md md-cached md-spin"></i>
                                                </span>
                                                <span>@lang('common.submit')</span>
                                              </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer_scripts')

    <!--Plugin scripts-->
    <script type="text/javascript" src="{{asset('assets/vendors/slimscroll/js/jquery.slimscroll.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/bootstrap_calendar/js/bootstrap_calendar.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/moment/js/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/fullcalendar/js/fullcalendar.min.js')}}"></script>
    <!--End of Plugin scripts-->
    <!--Page level scripts-->
    <script type="text/javascript" src="{{asset('assets/js/pages/mini_calendar.js')}}"></script>
    <!--End of Page level scripts-->
    <script>

        $("#content-tab li a").click(function () {
            $("#clothing-nav-content .tab-pane").removeClass("show active");
        })
    </script>



    <script type="text/javascript">
        function UpdateInfo(){
            var phone1 = $('#phone1 span').html();
            var url = "{{ route('account.postUpdate', ['lang' => \App::getLocale()]) }}";
            var buttonDisplay = '{"Format":['+'{"confirmButtonText":'+'"@lang('common.confirm')"'+',"cancelButtonText":'+'"@lang('common.cancel')"'+',"setTittle":'+'"@lang('settings.secret')"'+',"error":'+'"@lang('error.securityPasswordError')"'+'}]}';
            var data = '{"Data":['+'{"phone1":"'+phone1+'"}]}';
            swalAlert(url,buttonDisplay,data);
            return false;
        }

        function uploadFront(){
            $('#inputIDfront').click();
        }
        function uploadBack(){
            $('#inputIDback').click();
        }
        function showFrontMsg(){
            if(document.getElementById("inputIDfront").files.length == 1){
                document.getElementById("showFront").innerHTML = "<i class='fa fa-check text-success'></i>";
            }
            if(document.getElementById("inputIDfront").files.length == 1 && document.getElementById("inputIDback").files.length == 1){
               
                $("#ID_btn").prop("disabled",false);
            }
        }
        function showBackMsg(){
            if(document.getElementById("inputIDback").files.length == 1){
                document.getElementById("showBack").innerHTML = "<i class='fa fa-check text-success'></i>";
            }
            if(document.getElementById("inputIDfront").files.length == 1 && document.getElementById("inputIDback").files.length == 1){
                $("#ID_btn").prop("disabled",false);
            }
        }
        function uploadID(){

            var disable = loadingDisable();
            var image1 = $('#inputIDfront')[0].files[0];
            var image2 = $('#inputIDback')[0].files[0];
            var form = new FormData();
            form.append('image1', image1);
            form.append('image2', image2);

            var url ="{{ route('account.postID', ['lang' => \App::getLocale()]) }}";

            $.ajax({
                url     : url,
                data: form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                headers:
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success : function(response){
                    disable.out();
                    if(response.result == 0){
                        var title =   "@lang('success.text1')" ;
                        var text =  "@lang('success.idtext2')" ;

                        swal({
                            title: title,
                            text: text,
                            type: 'success',
                            confirmButtonColor: '#4fb7fe',
                            allowOutsideClick: false
                        }).then(function(){ 
                            location.reload();
                        });
                    }else{
                        var titleF =   "@lang('error.uploadFail')" ;
                        var textF =  "@lang('error.uploadProfileFail')" ;

                        swal({
                            title: titleF,
                            text: textF,
                            type: 'error',
                            confirmButtonColor: '#4fb7fe',
                            

                        }).done();
                    }   
                }
            });
        }

        function UpdatePassword(){

            var is_rstpass = "{{ $user->is_rstpass}}";
            if(is_rstpass == 0){
                var obj = {rePassword2: "rePassword2", Password2: "Password2", rePassword1: "rePassword1", Password1: "Password1"};
                var final = { Id : obj};
                var Id = JSON.stringify(final);
                var ans = checkValue(Id);

                if(ans!=0){
                    return false;
                }

            }


            if($('#Password1').val() == "" && $('#Password2').val() == ""){

                $('#Password1_alert').html("@lang('settings.passsword_alert')");
                $('#Password2_alert').html("@lang('settings.passsword_alert')");
                return false;
            }
            else{

                $('#Password1_alert').html("");
                $('#Password2_alert').html("");

                if($('#Password1').val() != ""){
                    
                    ans = CheckPassword($('#Password1').val());
                    if(ans == false) 
                    { 
                        notiAlert(2, "{{ \Lang::get('register.passwordRemind') }}", '{{\App::getLocale()}}');
                        $('#Password1').focus();
                        return false;
                    }

                    if($('#rePassword1').val() == ""){
                        $('#rePassword1_alert').html("@lang('settings.passsword_alert')");
                        return false;
                    }else{
                        $('#rePassword1_alert').html("");



                        if($('#rePassword1').val() != $('#Password1').val()){
                            notiAlert(1, "@lang('settings.passwordNosame')", '{{\App::getLocale()}}');
                            $('#rePassword1').focus();
                            return false;
                        }
                    }
                    var getPassword1 =  $('#Password1').val();   
                }else{
                    var getPassword1 =  "";
                }

                if($('#Password2').val() != ""){
                    ans = CheckPassword($('#Password2').val());
                    if(ans == false) 
                    { 
                        notiAlert(2, "{{ \Lang::get('register.passwordRemind') }}", '{{\App::getLocale()}}');
                        $('#Password2').focus();
                        return false;
                    }

                    if($('#rePassword2').val() == ""){
                        $('#rePassword2_alert').html("@lang('settings.passsword_alert')");
                        return false;
                    }else{
                        $('#rePassword2_alert').html("");

                        if($('#rePassword2').val() != $('#Password2').val()){
                            notiAlert(1, "@lang('settings.passwordNosame')", '{{\App::getLocale()}}');
                            $('#rePassword2').focus();
                            return false;
                        }else{

                        }
                    }
                    var getPassword2 =  $('#Password2').val();
                }else{
                    var getPassword2 =  "";
                }
                

                var url = "{{ route('account.updatePassword', ['lang' => \App::getLocale()]) }}";
                var buttonDisplay = '{"Format":['+'{"confirmButtonText":'+'"@lang('common.confirm')"'+',"cancelButtonText":'+'"@lang('common.cancel')"'+',"setTittle":'+'"@lang('settings.secret')"'+',"error":'+'"@lang('error.securityPasswordError')"'+'}]}';
                var data = '{"Data":['+'{"getPassword1":"'+getPassword1+'","getPassword2":"'+getPassword2+'"}]}';
                swalAlert(url,buttonDisplay,data);
                return false;
            }
        }

        function uploadPic(){

            var disable = loadingDisable();
            var image1 = $('#Changefile')[0].files[0];

            var form = new FormData();
            form.append('image1', image1);


            var url ="{{ route('account.postProfile', ['lang' => \App::getLocale()]) }}";

            $.ajax({
                url     : url,
                data: form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                headers:
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success : function(response){
                    disable.out();
                    if(response.result == 0){
                        var title =   "@lang('success.text1')" ;
                        var text =  "@lang('message.uploadSuccess')" ;

                        swal({
                            title: title,
                            text: text,
                            type: 'success',
                            confirmButtonColor: '#4fb7fe',
                            allowOutsideClick: false
                        }).then(function(){ 
                            location.reload();
                            
                        });
                    }else{
                        var titleF =   "@lang('error.uploadFail')" ;
                        var textF =  "@lang('error.uploadProfileFail')" ;

                        swal({
                            title: titleF,
                            text: textF,
                            type: 'error',
                            confirmButtonColor: '#4fb7fe',
                            

                        }).done();
                    }   
                }
            });
        }
        function alertRstpass(){
                swal({
                    title: "@lang('settings.rstPassTittle')",
                    text: "@lang('settings.rstPass')",
                    type: 'info',
                    confirmButtonColor: '#00c0ef',
                    confirmButtonText: "@lang('common.confirm')",
                    confirmButtonClass: 'btn btn-success',
                    allowOutsideClick: false
                }).done();
        }


    </script>








@stop
