<option value="">@lang('withdraw.selectCity')</option>
@if(!empty($city))
  @foreach($city as $citys => $value )
    <option value="{{ $citys }}">{{ $value }}</option>
  @endforeach
@endif