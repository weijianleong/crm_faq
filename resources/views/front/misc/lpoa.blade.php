<?php
    use App\Repositories\MemberRepository;
    $memberRepo = new MemberRepository;
    $member = $memberRepo->findByUsername(trim($user->username));
    
    $lpoaform = $member->lpoa;
    
    $get_lang = explode("/",App::getLocale()); //weilun
    $get_lpoa_ver = $get_lang[0]."_lpoa";//weilun
?>
@extends('front.app')

@section('title')
  @lang('sidebar.fundLink5') | {{ config('app.name') }}
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/animate/css/animate.min.css')}}" />

@stop

@section('main_content')
    @include('front.include.header')
    <script type="text/javascript" src="{{asset('assets/js/front/ss.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/front/base64.js')}}"></script>

        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                   <div class="row no-gutters">
                       <div class="col-lg-5">
                           <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('sidebar.fundLink5')</h4>
                       </div>
                       <div class="col-lg-7">
                           <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                               <li class=" breadcrumb-item">
                                   <a href="index1">
                                       <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                                   </a>
                               </li>
                               <li class="breadcrumb-item active">@lang('sidebar.fundLink5')</li>
                           </ul>
                       </div>
                   </div>
                </div>
            </header>
            
            <div class="outer">
                <div class="col-lg-12">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="card m-t-35">

                        <div class="card-body">
                            <div class="table-responsive m-t-35">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                            </th> 
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="highlight">
                                                <span class="success"></span>
                                                <a href="#">@lang('settings.check_lpoa_chs')</a></td>
                                            
                                            <td>
                                                <a href="{{ route('amazon.read', ['folder' => 'LP', 'filename' => '托管授权书.pdf', 'lang' => \App::getLocale()]) }}" target="blank" class="btn btn-warning btn-xs purple">
                                                    <i class="fa fa-eye"></i> @lang('settings.onclick')
                                                </a>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="highlight">
                                                <span class="success"></span>
                                                <a href="#">@lang('settings.check_lpoa_eng')</a>
                                            </td>
                                            
                                            
                                            <td>
                                                <a href="{{ route('amazon.read', ['folder' => 'LP', 'filename' => 'LPOA.pdf', 'lang' => \App::getLocale()]) }}" target="blank" class="btn btn-warning btn-xs purple">
                                                    <i class="fa fa-eye"></i> @lang('settings.onclick')
                                                </a>
                                            </td>
                                        </tr>
                                        @if (!empty($member->lpoa))
                                            <tr>
                                                <td class="highlight">
                                                    <span class="success"></span>
                                                    <a href="#">@lang('settings.title7')</a>
                                                </td>
                                                
                                                
                                                <td>
                                                    
                                                        <a href="{{ route('amazon.read', ['folder' => 'LP', 'filename' => $lpoaform, 'lang' => \App::getLocale()]) }}" target="blank" class="btn btn-primary btn-xs purple">
                                                            <i class="fa fa-eye"></i> @lang('settings.onclick')
                                                        </a>
                                                    
                                                </td>
                                            </tr>
                                        @endif
                                        @if ($member->lpoa_status!='Y')
                                            <tr>
                                                <td class="highlight">
                                                    <span class="success"></span>
                                                    <a href="#">@lang('settings.online_lpoa')</a>
                                                </td>
                                                
                                                
                                                <td>
                                                    <button class="btn btn-raised btn-success adv_cust_mod_btn zoomin" data-toggle="modal" data-target="#modal-zoomin" id="modalZoomIn" >
                                                        <i class="fa fa-pencil"></i>&nbsp; @lang('settings.signature')
                                                    </button>
                                                    
                                                </td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->

                </div>
            </div>
        </div>


                                    
<div class="modal" tabindex="-1" id="modal-zoomin" role="dialog" aria-labelledby="modalLabelzoom" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #25265E;">
                <h4 class="modal-title text-white" id="modalLabel">@lang('misc.details')</h4>
            </div>
            <div class="modal-body" id="modalContent">
                <div style="font-size: 15px !important;font-weight: 100;">@lang('settings.name'):{{ $user->first_name}}</div><hr><br/>
                <div style="font-size: 15px !important;font-weight: 100;">@lang('settings.id'):{{ $member->detail->identification_number}}</div><hr><br/>
                <div style="font-size: 15px !important;font-weight: 50;" >@lang('settings.lpoa'):
                    <?php $LPOA = config('misc.LPOA');  ?>
                   <select style="display: inline-block; width: 100px;" name="lpoa" id="lpoa" class="form-control ">
                        @foreach ($LPOA as $lp => $value)
                          <option value="{{ $lp }}" @if ($get_lpoa_ver == $lp) selected="" @endif >{{ $value }}</option>
                        @endforeach
                    </select>
                </div>
                <hr><br/>

                <div id='ctlSignature_Container' style='width:250px;height:250px;' >
                      <script language="javascript" type="text/javascript" >
                            var ieVer = getInternetExplorerVersion();
                            if (isIE) {
                                if (ieVer >= 9.0)
                                    isIE = false;
                            }
                
                            if (isIE) 
                            {
                                document.write("<div ID='ctlSignature' style='width:250;height:250px;' ></div>");
                            }
                            else 
                            {
                                document.write("<canvas ID='ctlSignature' width='250' height='250'></canvas>");
                            }
                      </script>
                  </div>
            </div>
            <div class="modal-footer" style="display: block;">
                <div style="float: left; padding-top: 10px;">
                    <input type="checkbox" name="readme" id="readme" />@lang('misc.lpoa_read')
                </div>
                <div style="float: right;">
                    <input type="button" class="btn btn-warning btn-flat-border btn-update"  name="btnSave" value="@lang('common.submit') " onclick="javascript:return ValidateSignature();" id="btnSave" disabled="" />
                    <button class="btn btn-secondary" data-dismiss="modal" >@lang('misc.close')</button>
                </div>
            </div>
        </div>
    </div>
</div> 



@stop
    
@section('footer_scripts')
    <script type="text/javascript" src="{{asset('assets/js/pages/login2.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/pages/modals.js')}}"></script>
    <script type="text/javascript">
        function UpLpoa(){

            var image = $('#inputLP')[0].files[0];
            if(!image){
                
                notiAlert(2, "@lang('error.uploadFailText')", '{{\App::getLocale()}}');
                return false;
            }

            var disable = loadingDisable();
            var form = new FormData();
            form.append('image', image);

            $.ajax({
                url     : 'updateLP',
                data: form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                headers:
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success : function(response){
                    disable.out();

                    if(response.result == 0){
                        var title =   "@lang('success.lptext')" ;
                        var text =  "@lang('success.lptext2')" ;

                        swal({
                            title: title,
                            text: text,
                            type: 'success',
                            confirmButtonColor: '#4fb7fe',
                            allowOutsideClick: false
                        }).then(function(){ 
                            location.reload();
                        });
                    }else{
                        var titleF =   "@lang('error.uploadFail')" ;
                        var textF =  "@lang('error.uploadFailText')" ;

                        swal({
                            title: titleF,
                            text: textF,
                            type: 'error',
                            confirmButtonColor: '#4fb7fe',
                            

                        }).done();
                    }   
                }
            });
        }
        $('#readme').click(function() {
            
            if(document.getElementById('readme').checked) {
                $("#btnSave").prop("disabled",false);
            } else {
                $("#btnSave").prop("disabled",true);
            }
        });


        $("#btnSave").click(function(event){

            var ctlSignature_data = $('#ctlSignature_data').val();
            var ctlSignature_data_canvas = $('#ctlSignature_data_canvas').val();
            var lpoa = $('#lpoa').val();

            var obj = {ctlSignature_data: "ctlSignature_data", ctlSignature_data_canvas: "ctlSignature_data_canvas"};
            var final = { Id : obj};
            var Id = JSON.stringify(final);
            var ans = checkValue(Id);

            if(ans!=0){
                return false;
            }
            var disable = loadingDisable();
            var url = "{{ route('account.license_agreement', ['lang' => \App::getLocale()]) }}";
            var obj = {ctlSignature_data: ctlSignature_data, ctlSignature_data_canvas:ctlSignature_data_canvas,lpoa:lpoa};
            var final = { Data : obj};
            var data = JSON.stringify(final);
            
            $.ajax({
                url     : url,
                data    : {
                    Data:data
                },
                type: 'POST',
                headers:
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success : function(response){
                    disable.out();
                    notiAlert(response.status, response.msg, '{{\App::getLocale()}}');
                    if(response.status == 0){

                        location.reload();
                       

                    } 
                }
            });
            return false;
        });
    </script>

    <script type="text/javascript">
      var get_lang = "{{ $get_lang[0] }}";
      

      if(get_lang == "en"){
        var signObjects = new Array('ctlSignature');
      
        var objctlSignature = new SuperSignature({SignObject:"ctlSignature",SignWidth: "250",SignHeight: "250",PenColor: "#0000FF",IeModalFix: false,BorderStyle: "Dashed",BorderWidth: "2px",BorderColor: "#DDDDDD",RequiredPoints: "15",ClearImage:"{{URL::asset('assets/img/common/refresh.png')}}", PenCursor:"{{URL::asset('assets/img/common/pencil.cur')}}", Visible: "true",SuccessMessage: "Signature OK.",StartMessage: "Please sign.",ErrorMessage: "Please continue your signature."});

      }else{
        var signObjects = new Array('ctlSignature');
      
        var objctlSignature = new SuperSignature({SignObject:"ctlSignature",SignWidth: "250",SignHeight: "250",PenColor: "#0000FF",IeModalFix: false,BorderStyle: "Dashed",BorderWidth: "2px",BorderColor: "#DDDDDD",RequiredPoints: "15",ClearImage:"{{URL::asset('assets/img/common/refresh.png')}}", PenCursor:"{{URL::asset('assets/img/common/pencil.cur')}}", Visible: "true",SuccessMessage: "完成签名.",StartMessage: "请签名.",ErrorMessage: "可以继续签名."});
      }
      
      $(document).ready(function() 
      {
        objctlSignature.Init();
      });

    </script>
@stop