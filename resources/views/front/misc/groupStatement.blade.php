<?php
    
    use App\Repositories\SharesRepository;
    use App\Repositories\MemberRepository;
    use Carbon\Carbon;
    use Illuminate\Support\Str;
    $sharesRepo = new SharesRepository;
    $memberRepo = new MemberRepository;
    $announcementModel = new \App\Models\Announcement;
    
    $member = $memberRepo->findByUsername(trim($user->username));
    $member->load('wallet');
    $member->load('detail');
    
    if (\Input::has('id')) {
        $sMember = \App\Models\Member::where('id', trim(\Input::get('id')))->first();
    } else {
        $sMember = $member;
    }
    //print_r($sMember);
    if ($sMember->level <= $member->level) {
        $sMember = $member;
    }
    
    $checknetwork = \DB::table('Member_Network')->where('member_id', '=', trim(\Input::get('id')))->where('parent_id', '=', $member->id)->first();
    
    if(count($checknetwork) == 0)
    {
        $sMember = $member;
    }
    
    \Session::put('meid', $sMember->id);
    $userdetail = \DB::table('users')->where('username', '=',$sMember->username)->first();
    

    $myusername = trim($user->username);
    
    $totalmember = DB::table('Member_Network')->where('parent_id', '=', $sMember->id)->count();
    
    $totaldeposit = DB::table('Member_Network')->join('Member_Wallet_Statement', 'Member_Network.member_id', '=', 'Member_Wallet_Statement.member_id')->where('parent_id', '=', $sMember->id)->where('action_type', '=', 'Deposit')->where('Member_Wallet_Statement.created_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('Member_Wallet_Statement.created_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->sum('cash_amount');
    
    $totalwithdrawal = DB::table('Member_Network')->join('Member_Wallet_Statement', 'Member_Network.member_id', '=', 'Member_Wallet_Statement.member_id')->where('parent_id', '=', $sMember->id)->where('action_type', '=', 'Withdraw')->where('Member_Wallet_Statement.created_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('Member_Wallet_Statement.created_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->sum('cash_amount');
    
    $totaltransfer1 = DB::table('Member_Network')->join('Transfer', 'Member_Network.member_id', '=', 'Transfer.from_member_id')->where('parent_id', '=', $sMember->id)->whereIn('type', ['C', 'T'])->where('from_member_id', '<>', 'to_member_id')->where('Transfer.created_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('Transfer.created_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->sum('amount');
    
    $totaltransfer2 = DB::table('Member_Network')->join('Transfer', 'Member_Network.member_id', '=', 'Transfer.to_member_id')->where('parent_id', '=', $sMember->id)->where('from_member_id', '<>', 'to_member_id')->whereIn('type', ['C', 'T'])->where('Transfer.created_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('Transfer.created_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->sum('amount');
    
    $totaldeposit2 = $totaldeposit + $totaltransfer2;
    
    $totalwithdrawal2 = $totalwithdrawal + $totaltransfer1;
    
    
    $totalsales1 = DB::table('Member_Network')->join('Member_Wallet_Statement_Fund', 'Member_Network.member_id', '=', 'Member_Wallet_Statement_Fund.member_id')->where('parent_id', '=', $sMember->id)->where('action_type', '=', 'Transfer to MT5')->where('Member_Wallet_Statement_Fund.created_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('Member_Wallet_Statement_Fund.created_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->sum('b_amount');
    
    $totalsales2 = DB::table('Member_Network')->join('Member_Blacklist', 'Member_Network.member_id', '=', 'Member_Blacklist.member_id')->where('parent_id', '=', $sMember->id)->where('Member_Blacklist.blacklist_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('Member_Blacklist.blacklist_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->where('b_book', '<>', 0)->sum('b_book');
    
    $totalsales = $totalsales1 - ($totalsales2/2);

    $investtrfx = 0;
    $investcoin = 0;
    $investmargin = 0;

    $investtrfx = DB::table('Member_Network')->join('Member_Account_Fund', 'Member_Network.member_id', '=', 'Member_Account_Fund.member_id')->where('parent_id', '=', $sMember->id)->sum('balanceb');
    $level1 = DB::table('Member_Network')->join('Capx_Member_Coin', 'Member_Network.member_id', '=', 'Capx_Member_Coin.member_id')->where('parent_id', $sMember->id)->get();

    $coin = DB::table('Capx_Coin')->get();
    foreach ($coin as $coinData) {
        $symbol = $coinData->symbol;
        if(count($level1) > 0){
            foreach ($level1 as $item) {
                if($item->$symbol > 0) $investcoin += $coinData->price * $item->$symbol;
            }
        }
    }

    $margin = DB::table('Capx_Margin')->get();
    foreach ($margin as $marginData) {
        $symbol = $marginData->symbol;
        if(count($level1) > 0){
            foreach ($level1 as $item) {
                if($item->$symbol > 0) $investmargin += $marginData->current_price * $item->$symbol;
            }
        }
    }

    $totalinvest = $investtrfx * 3 + $investcoin + $investmargin;
    
    $totallotsize = 0;
    /*
    $accounts = DB::table('Member_Network')->join('Member_Account_Fund', 'Member_Network.member_id', '=', 'Member_Account_Fund.member_id')->where('parent_id', '=', $member->id)->get();
    
    foreach($accounts as $account)
    {
        $bookA = $account->bookA;
        
        $volume = DB::connection('mysql2')->table('historyposition')->where('Login', '=', $bookA)->where('CloseTime', '>=', '2018-04-01')->where('CloseTime', '<', '2018-05-01')->sum('Volume');
        
        $volume2 = DB::connection('mysql2')->table('historyposition')->where('Login', '=', $bookA)->where('CloseTime', '>=', '2018-04-01')->where('OpenTime', '<', '2018-04-01')->sum('Volume');
        
        $lotsize1 = $volume / 10000;
        
        $lotsize2 = ($volume2 / 10000) / 2;
        
        $totallotsize = $totallotsize + $lotsize1 - $lotsize2;
    }

    */
?>
@extends('front.app')

@section('title')
@lang('misc.ggroup') | {{ config('app.name') }}
@stop

@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/widgets.css')}}">
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/datatables/css/colReorder.bootstrap.min.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}" />
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> {{ $userdetail->first_name }} @lang('misc.ggroup') {{date('m/Y')}}</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('misc.ggroup')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="row user_widget">
                        <div class="col-12 col-sm-6 col-xl m-t-35">
                            <div class="bg-danger b_r_5" style="padding: 10px;">
                                <div>
                                    <div class="user_wid_font"><i class="fa fa-user-o"></i> {{ $totalmember }}</div>
                                    <div style="text-align: center;">@lang('misc.gmember')</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-xl m-t-35">
                            <div class="bg-success b_r_5" style="padding: 10px;">
                                <div>
                                    <div class="user_wid_font"><i class="fa fa-dollar"></i> {{ number_format($totaldeposit2,2) }}</div>
                                    <div style="text-align: center;">@lang('misc.gdeposit')</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-xl m-t-35">
                            <div class="bg-warning b_r_5" style="padding: 10px;">
                                <div>
                                    <div class="user_wid_font"><i class="fa fa-dollar"></i> {{ number_format($totalwithdrawal2,2) }}</div>
                                    <div style="text-align: center;">@lang('misc.gwithdrawal')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row user_widget">
                        <div class="col-12 col-sm-6 col-xl m-t-35">
                            <div class="bg-info b_r_5" style="padding: 10px;">
                                <div>
                                    <div class="user_wid_font"><i class="fa fa-dollar"></i> {{ number_format($totalinvest,2) }}</div>
                                    <div style="text-align: center;">@lang('misc.ginvest')</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-xl m-t-35">
                            <div class="bg-mint b_r_5" style="padding: 10px;">
                                <div>
                                    <div class="user_wid_font"><i class="fa fa-dollar"></i> {{ number_format($totalsales*2,2) }}</div>
                                    <div style="text-align: center;">@lang('misc.gsales')</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            <i class="fa fa-table"></i> @lang('misc.ggroup') {{date('m/Y')}}
                        </div>
                        <div class="card-body p-t-10 m-t-25">
                            @lang('unilevel.member')
                            <div class="form-group row form_inline_inputs_bot">
                                <div class="col-lg-4">
                                    <div class="input-group">
                                        <input type="text" value="{{ $sMember->username }}" name="u" class="form-control" required="" id="username">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <button id="getUsernameGroupreport" method="post" type="button" class="btn btn-primary layout_btn_prevent btn-responsive form_inline_btn_margin-top">@lang('common.submit')</button>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTable no-footer" style="text-align: center; width: 100%;" id="directsales" role="grid">
                                    <thead>
                                    <tr>
                                        <th>@lang('misc.gid')</th>
                                        <th>@lang('misc.firstname')</th>
                                        <th>@lang('misc.gupline')</th>
                                        <th>@lang('misc.glevel')</th>
                                        <th>@lang('misc.grank')</th>
                                        <th>@lang('misc.gdeposit')</th>
                                        <th>@lang('misc.gwithdrawal')</th>
                                        <th>@lang('misc.ginvest')</th>
                                        <th>@lang('misc.gsales')</th>
                                        <th>@lang('misc.glostsize')</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.colReorder.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script type="text/javascript">
    var tableId = "directsales";
    var dataUrl = "{{ route('member.groupsales', ['id' => $id,'lang' => \App::getLocale()]) }}";
    var columnIds = [
                    { "data": "username" },
                    { "data": "firstname" },
                    { "data": "upline" },
                    { "data": "level" },
                    { "data": "rank" },
                    { "data": "totaldeposit" },
                    { "data": "totalwithdrawal" },
                    { "data": "totalinvest" },
                    { "data": "totalsales" },
                    { "data": "totallotsize" }
                    ];
    var disableSearchIndex = [1,2,3,4,5,6,7,8,9];
    $(document).ready( function () {
        callDatatables(tableId, dataUrl, columnIds, disableSearchIndex);

        $("#getUsernameGroupreport").click(function(event){
            var disable = loadingDisable();
            $.ajax({
                url     :   "{{ route('member.getGroupsales', ['lang' => \App::getLocale()]) }}",
                method  :   $(this).attr("method"),
                data    :   {
                                username : $("#username").val()
                            },
                headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
                success : function(response){
                    if(response.status == 0){
                        var url = "{{ route('misc.groupstatement', ['lang' => \App::getLocale(), 'id' => 'targetId']) }}";
                        url = url.replace('targetId', response.targetId);;
                        document.location.href=url;
                    }else{
                        notiAlert(response.status, response.msg, '{{\App::getLocale()}}');
                        disable.out();
                    }
                }
            });
            event.preventDefault();
        });
    });
</script>
@stop