<?php
    
    use App\Repositories\SharesRepository;
    use App\Repositories\MemberRepository;
    use Carbon\Carbon;
    use Illuminate\Support\Str;
    $sharesRepo = new SharesRepository;
    $memberRepo = new MemberRepository;
    $announcementModel = new \App\Models\Announcement;
    
    $member = $memberRepo->findByUsername(trim($user->username));
    $member->load('wallet');
    $member->load('detail');
    //$member->load('shares');
    $member->load('package');
    
    ?>
@extends('front.app')

@section('title')
@lang('misc.mt5Title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
  <li class="active">@lang('breadcrumbs.statementLink4')</li>
</ul>
@stop

@section('content')
<main>
  @include('front.include.sidebar')
  <div class="main-container">
    @include('front.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section class="tables-data">
        <div class="page-header">
          <h1><i class="md md-account-balance"></i> @lang('misc.mt5Title')</h1>

        </div>

        <div class="row m-b-10">
          <div class="col-md-12">
            <div class="card">
            <form id="transferForm" role="form" class="action-form" data-url="{{ route('member.addMT5', ['lang' => \App::getLocale()]) }}" data-parsley-validate="" onsubmit="return false;" http-type="post">
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary" disabled>
                      <span class="btn-preloader">
                        <i class="md md-cached md-spin"></i>
                      </span>
                      <span>@lang('common.addMT5')</span>
                    </button>
                  </div>
                </fieldset>
              </form>

              <div class="card-content">
                <div class="datatables">
                  <table class="table table-full table-full-small dt-responsive display nowrap table-grid" cellspacing="0" width="100%" role="grid" data-url="{{ route('member.accountlist', ['lang' => \App::getLocale()]) }}">
                    <thead>
                      <tr>
                        <th data-id="id">ID</th>
                        <th data-id="bookA">@lang('misc.bookA')</th>
                        <th data-id="balanceA" data-searchable="false">@lang('misc.balanceA')</th>
                        <th data-id="bookB">@lang('misc.bookB')</th>
                        <th data-id="balanceB" data-searchable="false">@lang('misc.balanceB')</th>
                        <th data-id="status" data-searchable="false">@lang('misc.status')</th>
                        <th data-id="lockdate" data-searchable="false">@lang('misc.lockdate')</th>
                     </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>



      </section>
    </div>
  </div>
</main>
@stop
