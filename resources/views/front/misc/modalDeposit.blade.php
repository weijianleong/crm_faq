<?php
    
    use App\Repositories\SharesRepository;
    use App\Repositories\MemberRepository;
    use Carbon\Carbon;
    use Illuminate\Support\Str;
    
   $user = \Sentinel::getUser();
    
    $memberRepo = new MemberRepository;

    $member = $memberRepo->findByUsername(trim($user->username));

    
    $accounts = DB::table('Member_Account_Fund')->where('member_id', '=', $member->id)->where('status', '=', 0)->get();
    
    $packages = DB::table('Package')->where('status', '=', 1)->get();
    
    ?>
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="showModalLabel">
<span class="md md-accessibility"></span> @lang('transfer.mt5.title')
</h4>
</div>

<div class="col-md-12">
            <div class="well white">

                <fieldset>
                <div class="form-group">
                <label class="control-label" for="inputAccount">@lang('transfer.mt5.wallet')</label>
                <div class="input-group">
                  <select class="form-control" name="account" id="account">
                    <option value="0">({{\Lang::get('common.select')}})</option>
                    @if (count($accounts) > 0)
                    @foreach ($accounts as $account)
                        <?php
                            $balanceA = $memberRepo->AccountAPI($account->bookA);
                            $balanceB = $memberRepo->AccountAPI($account->bookB);
                            
                            
                        ?>
                    <option value="{{ $account->id }}">
                        {{ \Lang::get('transfer.a.wallet').' : '.$account->bookA.' '.\Lang::get('transfer.balance').' '.$balanceA->balance.', '.\Lang::get('transfer.b.wallet').': '.$account->bookB.' '.\Lang::get('transfer.balance').' '.$balanceB->balance  }}
                    </option>
                    @endforeach
                    @endif
                  </select>
                </div>
              </div>
            <div class="form-group">
                <label class="control-label" for="inputAccount">@lang('transfer.amount')</label>
                <div class="input-group">
                 <select name="currencypair" class="form-control">
                @if (count($packages) > 0)
                @foreach ($packages as $package)


                <option value="{{ $package->id }}">${{ $package->package_amount }} (@lang('transfer.a.wallet') ${{ $package->cash_amount }} @lang('transfer.b.wallet') ${{ $package->b_amount }} @lang('transfer.d.wallet') ${{ $package->deposit_amount }})</option>
                @endforeach
                @endif
                </select>
                </div>
              </div>

                  <div class="form-group">
                    <label class="control-label">@lang('transfer.security')</label>
                    <input type="password" class="form-control" name="s" required="">
                  </div>


                </fieldset>

            </div>
          </div>
