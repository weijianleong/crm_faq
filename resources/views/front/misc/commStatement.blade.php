@extends('front.app')

@section('title')
@lang('sidebar.incomeLink3') | {{ config('app.name') }}
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('sidebar.incomeLink3')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('sidebar.incomeLink3')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            <i class="fa fa-table"></i> @lang('sidebar.incomeLink3')
                        </div>
                        <div class="card-body p-t-10">
                            <div class=" m-t-25">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTable no-footer" style="text-align: center; width: 100%;" id="bStatement" role="grid">
                                        <thead>
                                        <tr>
                                            <th>@lang('misc.create')</th>
                                            <th>@lang('misc.commtitle1')</th>
                                            <th>@lang('misc.commtitle2')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($temp))
                                            @foreach($temp as $data)
                                            <tr>
                                                <td>@if(\App::getLocale() == 'en') {{date("M Y", strtotime($data['date']))}} @else {{date("n月 Y", strtotime($data['date']))}} @endif</td>
                                                <td>
                                                    @if($data['summary'])
                                                    <a class="btn btn-primary glow_button" href="{{ route('amazon.read', ['folder' => 'incomeSummary', 'filename' => $data['summary'], 'lang' => \App::getLocale()]) }}"  target="blank"><i class="fa fa-download"></i> @lang('misc.commtitle3')</a>
                                                    @else
                                                    <label class="btn" style="background-color: #8de8ff; color: #fff; cursor: not-allowed;"><i class="fa fa-download"></i> @lang('misc.commtitle3')</label>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($data['summary'])
                                                    <a class="btn btn-primary glow_button" href="{{ route('amazon.read', ['folder' => 'incomeDetails', 'filename' => $data['detail'], 'lang' => \App::getLocale()]) }}"  target="blank"><i class="fa fa-download"></i> @lang('misc.commtitle3')</a>
                                                    @else
                                                    <label class="btn" style="background-color: #8de8ff; color: #fff; cursor: not-allowed;"><i class="fa fa-download"></i> @lang('misc.commtitle3')</label>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                            @else
                                            <tr>
                                                <td colspan="3">@lang('misc.noresult')</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop