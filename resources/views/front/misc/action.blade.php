@if ($model->system_manage <> 'N') 
<label class="btn" style="background-color: #8de8ff; color: #fff; cursor: not-allowed;">@lang('misc.action1')</label>
@else
<button class="btn btn-primary glow_button" onclick="transferfund('{{$model->bookA}}')">@lang('misc.action1')</button>
@endif

@if ($model->system_manage <> 'N' || $model->balanceB == 0)
<label class="btn" style="background-color: #8de8ff; color: #fff; cursor: not-allowed;">@lang('misc.action2')</label>
@else
<button class="btn btn-primary glow_button" onclick="subscribefund('{{$model->bookA}}')">@lang('misc.action2')</button>
@endif

@if (($model->system_manage == 'S' && $model->newfundrequest == 'Y') || $model->system_manage <> 'S')
<label class="btn" style="background-color: #8de8ff; color: #fff; cursor: not-allowed;">@lang('misc.action4')</label>
@else
<a class="btn btn-primary glow_button" title="@lang('misc.action4')" href="{{ route('transaction.switchfund', ['lang' => \App::getLocale(), 'id' => $model->bookA]) }}">@lang('misc.action4')</a>
@endif

@if ($model->system_manage == 'U')
<button class="btn btn-danger glow_button" onclick="cancelunsubscribefund('{{$model->bookA}}')">@lang('misc.action5')</button>
@elseif ($model->system_manage <> 'S')
<label class="btn" style="background-color: #8de8ff; color: #fff; cursor: not-allowed;">@lang('misc.action3')</label>
@else
<button class="btn btn-primary glow_button" onclick="unsubscribefund('{{$model->bookA}}')">@lang('misc.action3')</button>
@endif
