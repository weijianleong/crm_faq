@extends('front.app')

@section('title')
@lang('sidebar.fundLink6') | {{ config('app.name') }}
@stop

@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/datatables/css/colReorder.bootstrap.min.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/animate/css/animate.min.css')}}" />
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('sidebar.fundLink6')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('sidebar.fundLink6')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            <i class="fa fa-table"></i> @lang('sidebar.fundLink6')
                        </div>
                        <div class="card-body p-t-10">
                            <div class=" m-t-25">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTable no-footer" style="text-align: center; width: 100%;" id="fundTradeStatement" role="grid">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>@lang('misc.bookA')</th>
                                            <th>@lang('misc.balanceA')</th>
                                            <th>@lang('misc.bookB')</th>
                                            <th>@lang('misc.balanceB')</th>
                                            <th>@lang('misc.startdate')</th>
                                            <th>@lang('misc.enddate')</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('front.include.modal')
@stop

@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/js/pages/modals.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.colReorder.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script type="text/javascript">
    var tableId = "fundTradeStatement";
    var dataUrl = "{{ route('member.fundtradeaccountlist', ['lang' => \App::getLocale()]) }}";
    var columnIds = [
                    { "data": "id" },
                    { "data": "bookA" },
                    { "data": "balanceA" },
                    { "data": "bookB" },
                    { "data": "balanceB" },
                    { "data": "startdate" },
                    { "data": "enddate" },
                    { "data": "action" },
                    ];
    var disableSearchIndex = [2,4,5,6,7];
    $(document).ready( function () {
        callDatatables(tableId, dataUrl, columnIds, disableSearchIndex);
    });

    function getTradeDetails(bookA) {
        var disable = loadingDisable();
        var modelContent = '';
        var ajaxUrl = '{{ route("misc.showDetailsModal", ["id" => "bookA", "lang" => \App::getLocale()]) }}';
            ajaxUrl = ajaxUrl.replace('bookA', bookA);

        $.ajax({
            url     :   ajaxUrl,
            method  :   "get",
            headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
            success : function(response){
                if(response.status == 0){
                    modelContent = '<div style="position: relative;padding: 20px;height: 500px;overflow-y: scroll;">'
                    modelContent += '<table class="table table-striped table-bordered table-hover dataTable no-footer" style="text-align: center;" role="grid">';
                    modelContent += '<thead><tr>';
                    modelContent += '<th>@lang('misc.tradedate')</th>';
                    modelContent += '<th>@lang('misc.bookA')</th>';
                    modelContent += '<th>@lang("misc.bookB")</th>';
                    modelContent += '<th>@lang("misc.winA")</th>';
                    modelContent += '<th>@lang("common.cal.lot")</th>';
                    modelContent += '<th>@lang("misc.fundcompany")</th>';
                    modelContent += '</tr></thead>';
                    modelContent += '<tbody>';
                    $.each(response.model, function(index, item) {
                    modelContent += '<tr>';
                    modelContent += '<td>'+item["opentime"]+'<br/>'+item["closetime"]+'</td>';
                    modelContent += '<td>'+item["Adetails"]+'</td>';
                    modelContent += '<td>'+item["Bdetails"]+'</td>';
                    modelContent += '<td>'+item["Awin"]+'</td>';
                    modelContent += '<td>'+item["lotsize"]+'</td>';
                    modelContent += '<td>'+item["partnername"]+'</td>';
                    modelContent += '</tr>';
                    });
                    modelContent += '</tbody>';
                    modelContent += '</table>';
                    modelContent += '</div>';
                    $('#modalContent').empty().append(modelContent);
                    $('#modalZoomIn').click();
                }else{
                    notiAlert(response.status, response.msg, '{{\App::getLocale()}}');
                }
                disable.out();
            }
        });
    }
</script>
@stop