@extends('front.app')

@section('title')
@lang('misc.wTitle') | {{ config('app.name') }}
@stop

@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/datatables/css/colReorder.bootstrap.min.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/animate/css/animate.min.css')}}" />
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('misc.wTitle')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('misc.wTitle')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            <i class="fa fa-table"></i> @lang('misc.wTitle')
                        </div>
                        <div class="card-body p-t-10">
                            <div class=" m-t-25">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTable no-footer" style="text-align: center; width: 100%;" id="wStatement" role="grid">
                                        <thead>
                                        <tr>
                                            <th>@lang('misc.create')</th>
                                            <th>@lang('misc.description')</th>
                                            <th>@lang('misc.credit')</th>
                                            <th>@lang('misc.debit')</th>
                                            <th>@lang('misc.balance')</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('front.include.modal')
@stop

@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/js/pages/modals.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.colReorder.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script type="text/javascript">
    var tableId = "wStatement";
    var dataUrl = "{{ route('member.wlist', ['lang' => \App::getLocale()]) }}";
    var columnIds = [
                    { "data": "created_at" },
                    { "data": "remark" },
                    { "data": "credit" },
                    { "data": "debit" },
                    { "data": "balance" },
                    { "data": "action" },
                    ];
    var disableSearchIndex = [2,3,4,5];
    $(document).ready( function () {
        callDatatables(tableId, dataUrl, columnIds, disableSearchIndex);
    });

    function getStatementDetails(parentId) {
        var disable = loadingDisable();
        var modelContent = '';
        var ajaxUrl = '{{ route("misc.wstatementShow", ["id" => "parentId", "lang" => \App::getLocale()]) }}';
            ajaxUrl = ajaxUrl.replace('parentId', parentId);

        $.ajax({
            url     :   ajaxUrl,
            method  :   "get",
            headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
            success : function(response){
                if(response.status == 0){
                    modelContent = '<table class="table table-striped table-bordered table-hover dataTable no-footer" style="text-align: center;" role="grid">';
                    modelContent += '<thead><tr>';
                    modelContent += '<th>@lang("misc.description")</th>';
                    modelContent += '<th>@lang("misc.credit")</th>';
                    modelContent += '<th>@lang("misc.debit")</th>';
                    modelContent += '<th>@lang("misc.balance")</th>';
                    modelContent += '</tr></thead>';
                    modelContent += '<tbody>';
                    $.each(response.model, function(index, item) {
                    modelContent += '<tr>';
                    modelContent += '<td>'+item["remark_display"]+'</td>';
                    if(item["transaction_type"] == "C"){
                    modelContent += '<td>'+item["w_amount"]+'</td>';
                    modelContent += '<td>0.00</td>';
                    }else if(item["transaction_type"] == "D"){
                    modelContent += '<td>0.00</td>';
                    modelContent += '<td>'+item["w_amount"]+'</td>';
                    }
                    modelContent += '<td>'+item["balance"]+'</td>';
                    modelContent += '</tr>';
                    });
                    modelContent += '</tbody>';
                    modelContent += '</table>';
                    $('#modalContent').empty().append(modelContent);
                    $('#modalZoomIn').click();
                }else{
                    notiAlert(response.status, response.msg, '{{\App::getLocale()}}');
                }
                disable.out();
            }
        });
    }
</script>
@stop