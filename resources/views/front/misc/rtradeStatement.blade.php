<?php
    
    use App\Repositories\SharesRepository;
    use App\Repositories\MemberRepository;
    use Carbon\Carbon;
    use Illuminate\Support\Str;
    $sharesRepo = new SharesRepository;
    $memberRepo = new MemberRepository;
    $announcementModel = new \App\Models\Announcement;
    $tradeModel = new \App\Models\Trade;
    
    $member = $memberRepo->findByUsername(trim($user->username));
    $member->load('wallet');
    $member->load('detail');
    //$member->load('shares');
    $member->load('package');
    
    ?>
@extends('front.app')

@section('title')
@lang('misc.rtTitle') | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
  <li class="active">@lang('breadcrumbs.statementLink6')</li>
</ul>
@stop

@section('content')
<main>
  @include('front.include.sidebar')
  <div class="main-container">
    @include('front.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section class="tables-data">
        <div class="page-header">
          <h1><i class="md md-account-balance"></i> @lang('misc.rtTitle')</h1>

        </div>

        <div class="row m-b-10">
          <div class="col-md-12">
            <div class="card">

              <div class="card-content">
                <div class="datatables">
                  <table class="table table-full table-full-small dt-responsive display nowrap table-grid" cellspacing="0" width="100%" role="grid" data-url="{{ route('member.tradelist', ['lang' => \App::getLocale()]) }}">
                    <thead>
                      <tr>
                        <th data-id="created_at">@lang('misc.create')</th>
                        <th data-id="trade_type">@lang('misc.description')</th>
                        <th data-id="amount" data-searchable="false">@lang('misc.transfer.amount')</th>
                        <th data-id="status">@lang('misc.status')</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>



      </section>
    </div>
  </div>
</main>
@stop
