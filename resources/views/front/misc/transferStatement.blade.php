@extends('front.app')

@section('title')
@lang('misc.transferTitle') | {{ config('app.name') }}
@stop

@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/datatables/css/colReorder.bootstrap.min.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}" />
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('misc.transferTitle')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('misc.transferTitle')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            <i class="fa fa-table"></i> @lang('misc.transferTitle')
                        </div>
                        <div class="card-body p-t-10">
                            <div class=" m-t-25">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTable no-footer" style="text-align: center; width: 100%;" id="transferStatement" role="grid">
                                        <thead>
                                        <tr>
                                            <th>@lang('misc.create')</th>
                                            <th>@lang('misc.description')</th>
                                            <th>@lang('misc.credit')</th>
                                            <th>@lang('misc.debit')</th>
                                            <th>@lang('misc.balance')</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.colReorder.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script type="text/javascript">
    var tableId = "transferStatement";
    var dataUrl = "{{ route('member.transferlist', ['lang' => \App::getLocale()]) }}";
    var columnIds = [
                    { "data": "created_at" },
                    { "data": "remark" },
                    { "data": "credit" },
                    { "data": "debit" },
                    { "data": "balance" },
                    ];
    var disableSearchIndex = [2,3,4];
    $(document).ready( function () {
        callDatatables(tableId, dataUrl, columnIds, disableSearchIndex);
    });
</script>
@stop