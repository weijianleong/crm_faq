<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="showModalLabel">
<span class="md md-accessibility"></span> @lang('misc.modal.utitle')
</h4>
</div>

<div class="col-md-12">
            <div class="well white">
              <form id="transferForm" class="action-form" role="form" data-url="{{ route('transaction.postTransferMT5', ['lang' => \App::getLocale()]) }}" data-parsley-validate="" onsubmit="return false;" http-type="post">
                <fieldset>

            <div class="form-group">
                <h3 class="theme-text w300">@lang('misc.fundcancelnotice')</h3>
                </div>
              </div>

                  <div class="form-group">
                    <label class="control-label">@lang('transfer.security')</label>
                    <input type="password" class="form-control" name="s" required="">
                  </div>


                </fieldset>
              </form>
            </div>
          </div>
