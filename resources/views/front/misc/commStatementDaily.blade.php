@extends('front.app')

@section('title')
@lang('sidebar.incomeLink4') | {{ config('app.name') }}
@stop

@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/datatables/css/colReorder.bootstrap.min.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}" />
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('sidebar.incomeLink4') </h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('sidebar.incomeLink4') </li>
                   </ul>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">

                <div class="col">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            <i class="fa fa-table"></i> @lang('sidebar.incomeLink4') 
                        </div>

                        <div class="col-lg-3 m-t-25">
                            <?php $HType = config('helpdesk.comm_type');  ?>
                            <select class="form-control hide_search" tabindex="7" onchange="search()"  id="option_type" >
                                @foreach ($HType as $HTypes => $value)
                                    <option value="{{ $value }}" @if($option_session==$value) selected=""  @endif > @lang('misc.' . $HTypes)</option>
                                @endforeach


         
                            </select>
                        </div>

                        <div class="card-body p-t-10">
                            <div class=" m-t-25">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTable no-footer" style="text-align: center; width: 100%;" id="incomeStatement" role="grid">
                                        <thead>
                                        <tr>
                                            <th>@lang('misc.daily.startdate')</th>
                                            <th>@lang('misc.daily.enddate')</th>
                                            <th>@lang('misc.transfer.amount')</th>
                                            <th>@lang('misc.type')</th>
                                            <th>@lang('misc.status')</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('front.include.modal')
@stop

@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.colReorder.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script type="text/javascript">
    var tableId = "incomeStatement";
    var dataUrl = "{{ route('misc.commstatementDaily', ['lang' => \App::getLocale(),'option_session' => $option_session]) }}";
    var columnIds = [
                    { "data": "start_date" },
                    { "data": "end_date" },
                    { "data": "amount" },
                    { "data": "type" },
                    { "data": "status" },
                    { "data": "action" },
                    ];
    var disableSearchIndex = [];
    $(document).ready( function () {
        callDatatables(tableId, dataUrl, columnIds, disableSearchIndex);
    });

    function showDetail(symbol,start_date,end_date) {
        var disable = loadingDisable();
        var modelContent = '';
        var ajaxUrl = '{{ route("misc.commstatementDaily.showDetailsModal", ["symbol" => "symbol","start_date" => "start_date", "end_date" => "end_date","lang" => \App::getLocale()]) }}';
            ajaxUrl = ajaxUrl.replace('symbol', symbol);
            ajaxUrl = ajaxUrl.replace('start_date', start_date);
            ajaxUrl = ajaxUrl.replace('end_date', end_date);


        $.ajax({
            url     :   ajaxUrl,
            method  :   "get",
            headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
            success : function(response){
                if(response.status == 0){
                    modelContent = '<div style="position: relative;padding: 20px;height: 500px;overflow-y: scroll;">'
                    modelContent += '<table class="table table-striped table-bordered table-hover dataTable no-footer" style="text-align: center;" role="grid">';
                    modelContent += '<thead><tr>';
                    modelContent += '<th>'+"@lang('misc.daily.detail.date')"+'</th>';
                    modelContent += '<th>'+"@lang('misc.daily.detail.rank')"+'</th>';
                    if(response.symbol == "trc")
                    modelContent += '<th>'+"@lang('misc.daily.detail.lv0')"+'</th>';
                    modelContent += '<th>'+"@lang('misc.daily.detail.lv1')"+'</th>';
                    modelContent += '<th>'+"@lang('misc.daily.detail.lv2')"+'</th>';
                    modelContent += '<th>'+"@lang('misc.daily.detail.lv3')"+'</th>';
                    modelContent += '<th>'+"@lang('misc.transfer.amount')"+'</th>';
                    modelContent += '</tr></thead>';
                    modelContent += '<tbody>';
                    $.each(response.model, function(index, item) {
                    modelContent += '<tr>';
                    modelContent += '<td>'+item["date"]+'</td>';
                    if(item["rank"])
                    modelContent += '<td>'+item["rank"]+'</td>';
                    else
                    modelContent += '<td>'+"-"+'</td>';
                    if(item["symbol"] == "trc")
                    modelContent += '<td>'+item["level0"]+'</td>';
                    modelContent += '<td>'+item["level1"]+'</td>';
                    modelContent += '<td>'+item["level2"]+'</td>';
                    modelContent += '<td>'+item["level3"]+'</td>';

                    modelContent += '<td>'+item["total"]+'</td>';
                    modelContent += '</tr>';
                    });
                    modelContent += '</tbody>';
                    modelContent += '</table>';
                    modelContent += '</div>';
                    $('#modalContent').empty().append(modelContent);
                    $('#modalZoomIn').click();
                }else{
                    notiAlert(response.status, response.msg, '{{\App::getLocale()}}');
                }
                disable.out();
            }
        });
    }

    function search(){

          var disable = loadingDisable();
          $.ajax({
                url     : "{{ route('misc.commstatementDaily.setsession', ['lang' => \App::getLocale()]) }}",
                method  : 'post',
                data    : {

                    option_session  : $("#option_type").val()
                   
                },
                headers:
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success : function(response){
                    disable.out();
                    window.location.href = "{{ route('misc.getcommstatementDaily', ['lang' => \App::getLocale()]) }}";

                }
            });
    }
</script>
@stop