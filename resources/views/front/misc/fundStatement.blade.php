@extends('front.app')

@section('title')
@lang('sidebar.fundTitle') | {{ config('app.name') }}
@stop

@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/datatables/css/colReorder.bootstrap.min.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/animate/css/animate.min.css')}}" />
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('sidebar.fundTitle')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('sidebar.fundTitle')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            <i class="fa fa-table"></i> @lang('sidebar.fundLink1')
                        </div>
                        <div class="card-body p-t-10">
                            @if($addAccountDisplay)
                            <button class="btn btn-warning glow_button" style="margin-top: 10px;" onclick="addFund()"><i class="fa fa-plus"></i> @lang('common.addFund')</button>
                            @endif
                            <a class="btn btn-warning glow_button" style="margin-top: 10px;" href="https://download.mql5.com/cdn/web/11421/mt5/techrealfx5setup.exe"><i class="fa fa-download"></i> @lang('common.downloadMT5')</a>
                            <div class=" m-t-25">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTable no-footer" style="text-align: center; width: 100%;" id="fundaccountlist" role="grid">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>@lang('misc.bookA')</th>
                                            <th>@lang('misc.balanceA')</th>
                                            <th>@lang('misc.bookB')</th>
                                            <th>@lang('misc.balanceB')</th>
                                            <th>@lang('misc.status')</th>
                                            <th></th>
                                            <th>@lang('misc.newfunddate')</th>
                                            <th>@lang('misc.requestunsubscribedate')</th>
                                            <th>@lang('misc.tradedate')</th>
                                            <th>@lang('misc.bookA')</th>
                                            <th>@lang('misc.bookB')</th>
                                            <th>@lang('misc.winA')</th>
                                            <th>@lang('misc.fundcompany')</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('front.include.modal')
@stop

@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/js/pages/modals.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.colReorder.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script type="text/javascript">
    var tableId = "fundaccountlist";
    var dataUrl = "{{ route('member.fundaccountlist', ['lang' => \App::getLocale()]) }}";
    var columnIds = [
                    { "data": "id" },
                    { "data": "bookA" },
                    { "data": "balanceA" },
                    { "data": "bookB" },
                    { "data": "balanceB" },
                    { "data": "status" },
                    { "data": "action" },
                    { "data": "newfunddate" },
                    { "data": "request_unsubscribe" },
                    { "data": "txndate" },
                    { "data": "bookAtxn" },
                    { "data": "bookBtxn" },
                    { "data": "bookAwin" },
                    { "data": "fundcompany" },
                    ];
    var disableSearchIndex = [2,4,5,6,7,8,9,10,11,12,13];
    $(document).ready( function () {
        callDatatables(tableId, dataUrl, columnIds, disableSearchIndex);
    });

    function addFund() {
        var disable = loadingDisable();
        var ajaxUrl = "{{ route('member.addFund', ['lang' => \App::getLocale()]) }}";

        $.ajax({
            url     :   ajaxUrl,
            method  :   "post",
            headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
            success : function(response){
                notiAlert(response.status, response.msg, '{{\App::getLocale()}}');
                if(response.status == 0){
                    var url = "{!! route('misc.fundstatement', ['lang' => \App::getLocale()]); !!}";
                    document.location.href=url;
                }
                disable.out();
            }
        });
    }

    function transferfund(accountA) {
        var disable = loadingDisable();
        var ajaxUrl = "{{ route('transaction.fundchecking', ['lang' => \App::getLocale()]) }}";

        $.ajax({
            url     :   ajaxUrl,
            method  :   "post",
            data    :   { "bookA" : accountA },
            headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
            success : function(response){
                if(response.status == 0){
                    var url = "{!! route('transaction.transferfund', ['lang' => \App::getLocale(), 'id' => 'bookA']); !!}";
                    url = url.replace('bookA', response.bookA);
                    document.location.href=url;
                }else{
                    notiAlert(response.status, response.msg, '{{\App::getLocale()}}');
                    disable.out();
                }
            }
        });
    }

    function subscribefund(accountA) {
        var url = "{!! route('transaction.subscribefund', ['lang' => \App::getLocale(), 'id' => 'bookA']); !!}";
        url = url.replace('bookA', accountA);
        document.location.href=url;
        
        // var disable = loadingDisable();
        // var ajaxUrl = "{{ route('transaction.fundchecking', ['lang' => \App::getLocale()]) }}";

        // $.ajax({
        //     url     :   ajaxUrl,
        //     method  :   "post",
        //     data    :   { "bookA" : accountA },
        //     headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
        //     success : function(response){
        //         if(response.status == 0){
        //             var url = "{!! route('transaction.subscribefund', ['lang' => \App::getLocale(), 'id' => 'bookA']); !!}";
        //             url = url.replace('bookA', response.bookA);
        //             document.location.href=url;
        //         }else{
        //             notiAlert(response.status, response.msg, '{{\App::getLocale()}}');
        //             disable.out();
        //         }
        //     }
        // });
    }

    function unsubscribefund(accountA) {
        var modelContent = '';

        modelContent = '<div style="text-align: center; font-size: 30px; padding: 15px;">@lang("misc.modal.usure1")<br/>';
        modelContent += '<button type="button" class="btn btn-primary glow_button" onclick="confirmunsubscribefund('+accountA+')">@lang("common.confirm")</button>';
        modelContent += '</div>';
        $('#modalContent').empty().append(modelContent);
        $('#modalZoomIn').click();
    }

    function confirmunsubscribefund(accountA) {
        var url = "{!! route('transaction.unsubscribefund', ['lang' => \App::getLocale(), 'id' => 'accountA', 'step' => '1']); !!}";
        url = url.replace('accountA', accountA);
        document.location.href=url;
    }

    function cancelunsubscribefund(accountA) {
        var modelContent = '';

        modelContent = '<div style="text-align: center; font-size: 30px; padding: 15px;">@lang("misc.modal.cancelunsubscribe")<br/>';
        modelContent += '<div style="text-align: center; font-size: 15px; padding: 15px;">@lang("misc.modal.cancelunsubscribe1")<br/>';
        modelContent += '<button type="button" class="btn btn-primary glow_button" data-dismiss="modal" onclick="confirmcancelunsubscribefund('+accountA+')">@lang("common.confirm")</button>';
        modelContent += '</div>';
        $('#modalContent').empty().append(modelContent);
        $('#modalZoomIn').click();
    }

    function confirmcancelunsubscribefund(accountA) {
        var url = "{{ route('transaction.postCancelUnsubscribe', ['lang' => \App::getLocale()]) }}";
        var buttonDisplay = '{"Format":['+'{"confirmButtonText":'+'"@lang('common.confirm')"'+',"cancelButtonText":'+'"@lang('common.cancel')"'+',"setTittle":'+'"@lang('settings.secret')"'+',"error":'+'"@lang('error.securityPasswordError')"'+'}]}';
        var obj = {bookA: accountA};
        var final = { Data : obj};
        var data = JSON.stringify(final);

        obj = JSON.parse(buttonDisplay);

        swal({
            title: obj.Format[0].setTittle,
            input: 'password',
            showCancelButton: true,
            confirmButtonText: obj.Format[0].confirmButtonText,
            confirmButtonColor: '#00c0ef',
            cancelButtonText: obj.Format[0].cancelButtonText,
            cancelButtonColor: '#ff8080',
            width: 600,
            showLoaderOnConfirm: true,
            preConfirm: function (password) {
                return new Promise(function (resolve, reject) {
                    $.ajax({
                            url : url,
                            data: {
                                    SPassword  : password,
                                    Data : data,
                            },
                            type: 'POST',
                            headers:
                            {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success : function(response){
                                if (response.SPassword == '1') {
                                    var error = obj.Format[0].error;
                                    reject(error);
                                } else {
                                    resolve(response);
                                }
                            }
                    });
                });
            },
            allowOutsideClick: false
        }).then(function (response) {
            if(response.returnUrl) document.location.href=response.returnUrl;
        }).done();
    }
</script>
@stop