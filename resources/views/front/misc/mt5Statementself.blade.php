@extends('front.app')

@section('title')
@lang('misc.mt5Title') | {{ config('app.name') }}
@stop

@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/datatables/css/colReorder.bootstrap.min.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}" />
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('misc.mt5Title')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('misc.mt5Title')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            <i class="fa fa-table"></i> @lang('misc.mt5Title')
                        </div>
                        <div class="card-body p-t-10">
                            <button class="btn btn-warning glow_button" style="margin-top: 10px;" onclick="addMT5self()"><i class="fa fa-plus"></i> @lang('common.addMT5')</button>
                            <a class="btn btn-warning glow_button" style="margin-top: 10px;" href="https://download.mql5.com/cdn/web/11421/mt5/techrealfx5setup.exe"><i class="fa fa-download"></i> @lang('common.downloadMT5')</a>
                            <div class=" m-t-25">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTable no-footer" style="text-align: center; width: 100%;" id="mt5AccountList" role="grid">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>@lang('misc.bookA')</th>
                                            <th>@lang('misc.balanceA')</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.colReorder.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script type="text/javascript">
    var tableId = "mt5AccountList";
    var dataUrl = "{{ route('member.selfaccountlist', ['lang' => \App::getLocale()]) }}";
    var columnIds = [
                    { "data": "id" },
                    { "data": "bookA" },
                    { "data": "balanceA" },
                    ];
    var disableSearchIndex = [2];
    $(document).ready( function () {
        callDatatables(tableId, dataUrl, columnIds, disableSearchIndex);
    });

    function addMT5self() {
        var disable = loadingDisable();
        var ajaxUrl = "{{ route('member.addMT5self', ['lang' => \App::getLocale()]) }}";

        $.ajax({
            url     :   ajaxUrl,
            method  :   "post",
            headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
            success : function(response){
                notiAlert(response.status, response.msg, '{{\App::getLocale()}}');
                if(response.status == 0){
                    var url = "{!! route('misc.mt5statementself', ['lang' => \App::getLocale()]); !!}";
                    document.location.href=url;
                }
                disable.out();
            }
        });
    }
</script>
@stop