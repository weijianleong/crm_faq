@extends('front.app')

@section('title')
  @lang('sidebar.usermanual') | {{ config('app.name') }}
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/animate/css/animate.min.css')}}" />
@stop

@section('main_content')
    @include('front.include.header')
    <script type="text/javascript" src="{{asset('assets/js/front/ss.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/front/base64.js')}}"></script>

        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                   <div class="row no-gutters">
                       <div class="col-lg-5">
                           <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('sidebar.usermanual')</h4>
                       </div>
                       <div class="col-lg-7">
                           <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                               <li class=" breadcrumb-item">
                                   <a href="index1">
                                       <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                                   </a>
                               </li>
                               <li class="breadcrumb-item active">@lang('sidebar.usermanual')</li>
                           </ul>
                       </div>
                   </div>
                </div>
            </header>
            
            <div class="outer">
                <div class="row">
                </div>
                <div class="col-lg-12">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <div class="card-body">

                            @if($isSeanet == 0 )
                                <div class="col-lg-12" >
                                    <div class="card m-t-35">
                                        <div class="card-header bg-white">
                                            @lang('home.usermanualvideo')
                                        </div>
                                        <div class="card-body m-t-10">
                                            <video src="https://crm.trtechsolution.com/ID/user.mp4" width="100%" height="auto" controls></video>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive m-t-35">
                                    <table class="table table-striped table-bordered table-advance table-hover">
                                        <thead>
                                            <tr>
                                                <th>
                                                </th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <tr>
                                                <td class="highlight">
                                                    <span class="success"></span>
                                                    <a href="#">@lang('home.usermanualpdf')</a></td>

                                                <td>
                                                    <a href="https://crm.trtechsolution.com/ID/operation.pdf" target="_blank" class="btn btn-primary btn-xs purple">
                                                        <i class="fa fa-eye"></i> @lang('settings.onclick')
                                                    </a>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="highlight">
                                                    <span class="success"></span>
                                                    <a href="#">@lang('home.usermanualzip')</a>
                                                </td>


                                                <td>
                                                    <a href="https://crm.trtechsolution.com/ID/user.zip" target="blank" class="btn btn-warning btn-xs purple">
                                                        <i class="fa fa-download"></i> @lang('common.download')
                                                    </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive m-t-35">
                                    <table class="table table-striped table-bordered table-advance table-hover">
                                        <thead>
                                        <tr>
                                            <th>
                                            </th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <tr>
                                            <td class="highlight">
                                                <span class="success"></span>
                                                <a href="#">@lang('capx.usermanualpdf')</a></td>

                                            <td>
                                                <a href="https://crm.trtechsolution.com/ID/capx.pdf" target="_blank" class="btn btn-primary btn-xs purple">
                                                    <i class="fa fa-eye"></i> @lang('settings.onclick')
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            @else
                                <div class="table-responsive m-t-35">
                                    <table class="table table-striped table-bordered table-advance table-hover">
                                        <thead>
                                        <tr>
                                            <th>
                                            </th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <tr>
                                            <td class="highlight">
                                                <span class="success"></span>
                                                <a href="#">@lang('capx.usermanualpdf')</a></td>

                                            <td>
                                                <a href="https://crm.trtechsolution.com/ID/capx_.pdf" target="_blank" class="btn btn-primary btn-xs purple">
                                                    <i class="fa fa-eye"></i> @lang('settings.onclick')
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            @endif

                        </div>
                    
                    <!-- END SAMPLE TABLE PORTLET-->

                </div>
            </div>
        </div>

@stop
    
@section('footer_scripts')

@stop