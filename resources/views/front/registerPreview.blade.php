<?php
    $datas = \Session::get('data');
    $destinationPath = config('misc.imagePath');;
    $idFront = \Session::get('idFront');
    $idBack = \Session::get('idBack');
    //print_r($datas);
?>
@extends('front.app')

@section('title')
New Registration - {{ config('app.name') }}
@stop

@section('content')
<div class="center">
  <div class="card bordered z-depth-2" style="margin:0% auto; max-width:800px;">
    <div class="card-header">
        <p><a href="{{ route('register', ['lang' => 'en']) }}">English</a> | <a href="{{ route('register', ['lang' => 'chs']) }}">中文</a></p>
        <div class="brand-logo">
            <img src="{{ asset('assets/img/new_logo.png') }}" width="120">
        </div>
        <div class="card-title strong pink-text middle">@lang('login.subtitle')</div>
    </div>

<form class="form-floating" name="registerForm" id="registerForm"  action="{{ route('register.post', ['lang' => \App::getLocale()]) }}" method="post" enctype="multipart/form-data">
      <div class="card-content">
        <div class="m-b-30">
          <div class="card-title strong middle pink-text">@lang('register.title2')</div>


        </div>
        <div class="form-group">
                      <label class="control-label" for="inputDirect">@lang('register.direct')<sup>*</sup></label>
                      <input type="text" class="form-control" required="" name="direct_id" id="inputDirect" disabled="" value="{{ $datas['direct_id'] }}">
                    </div>

        <div class="form-group">
              <label class="control-label">@lang('settings.name')<sup>*</sup></label>
              <input type="text" name="first_name" class="form-control" disabled="" value="{{ $datas['first_name'] }}">
        </div>
        <div class="form-group">
              <label class="control-label">@lang('settings.id')<sup>*</sup></label>
              <input type="text" name="identification_number" class="form-control" disabled="" value="{{ $datas['identification_number'] }}">
        </div>

        <div class="form-group">
          <label class="control-label" for="inputGender">@lang('settings.gender')</label>
              <input type="text" name="gender" class="form-control" disabled="" value="{{ $datas['gender'] }}">
        </div>

        <div class="form-group">
          <label for="email" class="control-label">@lang('login.email')</label>
          <input type="email" name="email" class="form-control" id="email" disabled="" value="{{ $datas['email'] }}">
        </div>

        <div class="form-group">
              <label class="control-label" for="inputPhone">@lang('settings.phone1')</label>
                  <input type="text" name="phone1" class="form-control" id="inputPhone" disabled="" value="{{ $datas['phone1'] }}">
        </div>

        <?php $countries = config('misc.countries');  ?>
        <div class="form-group">
          <label class="control-label" for="inputNationality">@lang('register.nationality')</label>

            <select class="form-control" name="nationality" id="inputNationality">

                     <option value="{{ $datas['nationality'] }}"  selected="">@lang('country.' . $datas['nationality'])</option>

                  </select>
        </div>

        <div class="form-group">
          <label class="control-label" for="inputAddress">@lang('settings.address')</label>
              <textarea name="address" class="form-control" id="inputAddress" required="" disabled="">{{ $datas['address'] }}</textarea>
        </div>

        <div class="form-group">
          <label class="control-label" for="inputPassword">@lang('settings.password')</label>
          <input type="password" name="password" class="form-control" id="inputPassword" required="" disabled="" value="{{ $datas['password'] }}">
        </div>


        <div class="form-group">
              <label class="control-label" for="inputIDback">@lang('settings.idfront')</label><BR>
              <a target=_blank href="{{ route('amazon.read.register', ['folder' => 'ID', 'filename' => $idFront]) }}">@lang('helpdesk.readPic1')</a>

        </div>

        <div class="form-group">
              <label class="control-label" for="inputIDback">@lang('settings.idback')</label><BR>
              <a target=_blank href="{{ route('amazon.read.register', ['folder' => 'ID', 'filename' => $idBack]) }}">@lang('helpdesk.readPic2')</a>
                
        </div>

<input type="hidden" value="{{ csrf_token() }}" name="_token">
        <div>
          <button type="submit" class="btn btn-primary btn-block m-t-50">
            <span class="btn-preloader">
              <i class="md md-cached md-spin"></i>
            </span>
            <span>@lang('register.action')</span>
          </button>
<a href="{{ route('register', ['lang' => \App::getLocale()]) }}" class="btn btn-primary btn-block m-t-50">@lang('register.previous')</a>
        </div>
      </div>

    </form>
  </div>
</div>


@stop
