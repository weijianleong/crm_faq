<?php
    use App\Repositories\SharesRepository;
    use App\Repositories\MemberRepository;
    use Carbon\Carbon;
    use Illuminate\Support\Str;
    $sharesRepo = new SharesRepository;
    $memberRepo = new MemberRepository;
    $announcementModel = new \App\Models\Announcement;
    
    $member = $memberRepo->findByUsername(trim($user->username));
    $member->load('wallet');
    $member->load('detail');
    //$member->load('shares');

    ?>
@extends('front.app')

@section('title')
  {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
    <li class="active">@lang('breadcrumbs.depositWithdrawal')</li>
  </ul>
@stop

@section('content')
  <main>
    @include('front.include.sidebar')
    <div class="main-container">
      @include('front.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <div class="dashboard grey lighten-3">
          <div class="row no-gutter">
            <div class="col-sm-12 col-md-12 col-lg-12" style="background:#F9F9F9;">
              <div class="p-20 clearfix">

              </div>

              <div class="p-20 no-p-t">


                <div class="row gutter-14">
                  <div class="col-md-11">
                        <div style="background:#F9F9F9;">
                            <div class="panel-heading middle">
                                <h1 class="m-t-10 m-b-5 f30">
                                    @lang('common.withdrawalMessage')
                                </h1>
                            <a href="https://trealcapital.trealfx.com" target="_blank">
                                <img src="{{ asset('assets/img/TechFX.png') }}" >
                                </a>
                            </div>
                        </div>
                  </div>



                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
@stop
