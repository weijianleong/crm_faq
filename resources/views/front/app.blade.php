<!DOCTYPE html>
<html lang="en" prefix="og:http://ogp.me/ns#" class="no-js">
<head>
    <meta charset="utf-8">
    <title>@yield('title', config('app.name'))</title>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <link rel="shortcun icon" href="{{ asset('assets/img/new_logo.png') }}" type="image/x-icon" />
    <link href="{{ \URL::current() }}" rel="canonical">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="alternate" href="{{ \URL::current() }}" hreflang="x-default" />

    <!-- global styles-->
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/components.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/modal-loading.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/modal-loading-animate.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/pages/buttons.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/baguetteBox.min.css') }}">
    <link type="text/css" rel="stylesheet" href="#" id="skin_change" />
    <!-- end of global styles-->

    <!--Plugin styles-->
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/vendors/wow/css/animate.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/vendors/toastr/css/toastr.min.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/vendors/Buttons/css/buttons.min.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/sweetalert/css/sweetalert2.min.css')}}"/>
    <!--End of Plugin styles-->

    @yield('header_styles')
</head>

<?php
    $seaNetwork = 0;
    $route = \Route::currentRouteName();
    if($user = \Sentinel::getUser()){
        $member = \DB::table('Member')->where('username', $user->username)->first();
        $seaNetwork = \DB::table('Member_Network')->where('parent_username', 'seanetlead@gmail.com')->where('username', $member->register_by)->count();
        if(in_array($user->username, ['seanetlead@gmail.com']) || $member->register_by == 'seanetlead@gmail.com') $seaNetwork = 1;
    }
?>

<body @if ($route == 'login' || $route == 'logout' || $route == 'forgotpassword'|| $route == 'forgotpasswordSuccess'|| $route == 'register'|| $route == 'registerPreview'|| $route == 'registerSuccess') class="login_background" @elseif (is_null($route)) class="page-error" @else class="fixedMenu_left fixedNav_position" @endif>
    <div class="preloader" style=" position: fixed; width: 100%; height: 100%; top: 0; left: 0; z-index: 100000; backface-visibility: hidden; background: #ffffff;">
        <div class="preloader_img" style="width: 200px; height: 200px; position: absolute; left: 48%; top: 48%; background-position: center; z-index: 999999">
            <img src="{{asset('assets/img/loader.gif')}}"  style=" width: 60px;" alt="loading...">
        </div>
    </div>

    @yield('main_content')
    @if(!($route == 'login.maintenance' || $route == 'login' || $route == 'logout' || $route == 'forgotpassword'|| $route == 'forgotpasswordSuccess'|| $route == 'register'|| $route == 'registerPreview'|| $route == 'registerSuccess'))
    </div>
    </div>
    <div class="footer">@lang('common.copyright1') <i class="fa fa-copyright"></i> {{date('Y')}} @lang('common.copyright2')

    <div class="memo" style="display: none;">
        @if(!$seaNetwork)
        <a href="{{asset('assets/img/memo/memo2_'.\App::getLocale().'.jpg')}}" data-caption="<a href='#' onclick='hidememo()' style='color:#fff;'><i class='fa fa-hand-pointer-o'></i> @lang('common.hidememo')</a>"></a>
        <a href="{{asset('assets/img/memo/memo1_'.\App::getLocale().'.jpg')}}" data-caption="<a href='#' onclick='hidememo()' style='color:#fff;'><i class='fa fa-hand-pointer-o'></i> @lang('common.hidememo')</a>"></a>
        @endif
    </div>
    @endif

    <!-- global js -->
    <script type="text/javascript" src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/popper.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/components.js') }}"></script>
    @if(!($route == 'login' || $route == 'logout' || $route == 'forgotpassword'|| $route == 'forgotpasswordSuccess'|| $route == 'register'|| $route == 'registerPreview'|| $route == 'registerSuccess'))
    <script type="text/javascript" src="{{ asset('assets/js/custom.js') }}"></script>
    @endif
    <script type="text/javascript" src="{{ asset('assets/js/general.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/format.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/modal-loading.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/baguetteBox.min.js') }}" ></script>
    <!-- end of global js-->
    
    <!--Plugin js-->
    <script type="text/javascript" src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/wow/js/wow.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/jquery.backstretch/js/jquery.backstretch.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/toastr/js/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/sweetalert/js/sweetalert2.min.js')}}"></script>
    <!--End of plugin js-->

    <!-- Notification -->
    @if (session()->has('flashMessage'))
        <?php $msg = session('flashMessage'); ?>
        @if (isset($msg['class']) && isset($msg['message']))
        <?php
            if($msg['class'] == 'warning') $type = 2;
            else $type = 1;
        ?>
            <script>
                notiAlert('{{ $type }}', '{{ $msg["message"] }}', '{{\App::getLocale()}}');
            </script>
        @endif
    @endif
    <!-- End of Notification -->

    <!--Datatables js-->
    <script type="text/javascript">
        if('{{\session()->has('Memo')}}'){
            const memo = baguetteBox.run('.memo',{
                                                    noScrollbars: true,
                                                });
            baguetteBox.show(0, memo[0]);
        }

        if('{{\session()->has('Notification')}}'){
            var a = document.createElement('a');
            var linkText = document.createTextNode('{{\Lang::get('inbox.fleshmsg2')}}');
            a.appendChild(linkText);
            a.href = '{{\Session::get('Notificationresendurl')}}';
            var msg = '{{\Session::get('Notification')}}';
            toastr.options = {
              "closeButton": true,
              "debug": false,
              "positionClass": "toast-top-right",
              "onclick": null,
              "showDuration": "1000",
              "hideDuration": "1000",
              "timeOut": "10000",
              "extendedTimeOut": "1000",
              "showEasing": "swing",
              "hideEasing": "swing",
              "showMethod": "show"
            }
            Command: toastr["info"](a, msg)
        }
        if('{{\session()->has('message')}}'){
            var a = document.createElement('a');
            var linkText = document.createTextNode('{{\Lang::get('helpdesk.fleshmsg2')}}');
            a.appendChild(linkText);
            a.href = '{{\Session::get('resendurl')}}';
            var msg = '{{\Session::get('message')}}';
            toastr.options = {
              "closeButton": true,
              "debug": false,
              "positionClass": "toast-top-right",
              "onclick": null,
              "showDuration": "1000",
              "hideDuration": "1000",
              "timeOut": "10000",
              "extendedTimeOut": "1000",
              "showEasing": "swing",
              "hideEasing": "swing",
              "showMethod": "show"
            }
            Command: toastr["info"](a, msg)
        }

        if('{{\session()->has('announcement')}}'){
            var a = document.createElement('a');
            var linkText = document.createTextNode('{{\Lang::get('helpdesk.fleshmsg2')}}');
            a.appendChild(linkText);
            a.href = '{{\Session::get('announcementresendurl')}}';
            var msg = '{{\Session::get('announcement')}}';
            toastr.options = {
              "closeButton": true,
              "debug": false,
              "positionClass": "toast-top-right",
              "onclick": null,
              "showDuration": "1000",
              "hideDuration": "1000",
              "timeOut": "10000",
              "extendedTimeOut": "1000",
              "showEasing": "swing",
              "hideEasing": "swing",
              "showMethod": "show"
            }
            Command: toastr["info"](a, msg)
        }

        function callDatatables(tableId, dataUrl, columnIds, disableSearchIndex) {
            $('#'+tableId).DataTable({
                "dom":'<"tableContentWidth"<"lengthPos"l><"searchPos"f>r<"tablePos"ti>p>',
                "processing"    : true,
                "serverSide"    : true,
                "ordering"      : false,
                "oLanguage"     : {
                "sProcessing"    : '{{ \Lang::get('datatable.processing') }}',
                "sLengthMenu"    : '{{ \Lang::get('datatable.lengthMenu') }}',
                "sZeroRecords"   : '{{ \Lang::get('datatable.zeroRecords') }}',
                "sInfo"          : '{{ \Lang::get('datatable.info') }}',
                "sInfoEmtpy"     : '{{ \Lang::get('datatable.infoEmpty') }}',
                "sInfoFiltered"  : '{{ \Lang::get('datatable.infoFilter') }}',
                "sInfoPostFix"   : '{{ \Lang::get('datatable.infoPostfix') }}',
                "sSearch"        : '{{ \Lang::get('datatable.search') }}',
                "oPaginate"      : {
                "sFirst"         : '{{ \Lang::get('datatable.paginate.first') }}',
                "sPrevious"      : '{{ \Lang::get('datatable.paginate.previous') }}',
                "sNext"          : '{{ \Lang::get('datatable.paginate.next') }}',
                "sLast"          : '{{ \Lang::get('datatable.paginate.last') }}'
                                   },
                                  },
                "ajax"          : dataUrl,
                "columnDefs"    : [{ "searchable": false, "targets": disableSearchIndex }],
                "columns": columnIds
            });
        }

        function hidememo(){
            var ajaxUrl = "{{ route('member.hidememo', ['lang' => \App::getLocale()]) }}";
            $.ajax({
                url     :   ajaxUrl,
                method  :   "post",
                headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
                success : function(response){
                }
            });
            baguetteBox.hide();
        }
    </script>
    <!--End of datatables js-->
    
    @yield('footer_scripts')
</body>
</html>