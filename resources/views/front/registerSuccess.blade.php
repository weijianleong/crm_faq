@extends('front.app')

@section('title')
Registration - {{ config('app.name') }}
@stop

@section('content')
<div class="center">
<div class="card bordered z-depth-2" style="margin:0% auto; max-width:400px;">
<div class="card-header">
<div class="brand-logo">
<img src="{{ asset('assets/img/new_logo.png') }}" width="100">
</div>
</div>
<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section>

          <div class="row m-b-40">
            <div class="col-md-12">
              <div class="well white text-center">
<h2> @lang('success.title')</h2>
                <h3>@lang('success.text2')</h3>
                <a href="{{ route('login', ['lang' => \App::getLocale()]) }}" class="btn btn-primary btn-block m-t-50">
                  <i class="md md-navigate-before"></i> @lang('success.pback')
                </a>
              </div>
            </div>
          </div>
        </section>
      </div>
</div>
</div>

@stop

