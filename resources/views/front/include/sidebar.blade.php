<?php
    $account2Network = \DB::table('Member_Network')->where('parent_username', 'account2@trfxbiz.com')->where('username', $user->username)->count();
    if(in_array($user->username, ['fxdb222@gmail.com'])) $account2Network = 1;
    
    $rank = \DB::table('Member_Sales')->where('username', $user->username)->first();

    $myrank = '';
    if(count($rank) > 0) $myrank = $rank->rank;
    else $myrank = '-';

    $unreadTicket = \DB::table('helpdesk')->where('user_id', $member->id)->where('is_reply', 1)->where('read', 1)->count();
    $unreadNotification = \DB::table('notification_target')->where('member_id', $member->id)->where('read', 0)->count();
    $unreadAnnouncement = 0;
    if($member->detail->announcement) $unreadAnnouncement = 1;
    
    $ranking = \DB::table('Member_Sales_Summary')->where('member_id', $member->id)->orderBy('date', 'DESC')->first();

    $access = \DB::table('Member_Access_Type')->where('id', $member->detail->access_type)->first();
    $userRights = json_decode($access->access);
    $menus = config('member.menus');
?>
<div class="wrapper fixedNav_top">
<div id="left" class="fixed" style="background-color: #25265E; height: 100%;">
        <div class="menu_scroll left_scrolled">
            <div class="left_media">
                <div class="media">
                    <div style="margin: auto; padding-top: 17px; padding-bottom: 5px;">
                        <a class="user-link" href="{{ route('settings.account', ['lang' => \App::getLocale()]) }}">
                            
                            @if (!empty($member->detail->profile_pic_64))
                                <img class="media-object img-thumbnail user-img rounded-circle admin_img3" alt="User Picture" src="data:image/;base64,{{ $member->detail->profile_pic_64 }}" id="profilePic" >        
                            @else
                                <img class="media-object img-thumbnail user-img rounded-circle admin_img3" alt="User Picture" src="{{asset('assets/img/profile.png')}}" id="profilePic" >        
                            @endif

                        </a>
                    </div>
                </div>
                <div class="media menu_hide">
                    <div style="margin: auto; padding-bottom: 5px;" class="text-white">{{ $user->first_name }}</div>
                </div>
                <div class="media menu_hide" style="color: #a8a8af;">
                    <div style="margin: auto; padding-bottom: 5px;">{{ $user->username }}</div>
                </div>
                <div class="media menu_hide text-white">
                    <div class="glow_button" style="margin: auto; background-color: #8657f0;padding: 1px 10px 1px 10px; border-radius: 5px;margin-top: 5px;">@lang('sidebar.lastrank') : @if(isset($ranking)) @if($ranking->rank) {{ $ranking->rank }} @else - @endif @endif</div>
                </div>
                <div class="media menu_hide text-white">
                    <div class="glow_button" style="margin: auto; background-color: #8657f0;padding: 1px 10px 1px 10px; border-radius: 5px;margin-top: 5px;">@lang('sidebar.rank') : @if($myrank){{ $myrank }}@else-@endif</div>
                </div>
                <hr/>
            </div>


            <ul id="menu">
                @foreach($menus as $name => $item)
                    @if(in_array($item['right'], $userRights))
                        @if(isset($item['child']))
                            @if(isset($item['check']))
                                <?php 
                                    $showDisplay = 0;
                                    
                                    if(in_array('account2Network', $item['check'])){
                                        if($account2Network > 0) $showDisplay = 1;
                                    }

                                    if(in_array('seaNetwork', $item['check'])){
                                        if($seaNetwork > 0) $showDisplay = 1;
                                    }

                                    if(in_array('ranking', $item['check'])){
                                        if(isset($ranking)){
                                            if($ranking->rank) $showDisplay = 1;

                                            if(in_array('MIBPIBRank', $item['check'])){
                                                if($ranking->rank <> 'MIB' && $ranking->rank <> 'PIB') $showDisplay = 0;
                                            }
                                        }
                                    }

                                    if(in_array('purchasedTrcap', $item['check'])){
                                        if($member->coin->trc > 0) $showDisplay = 1;
                                    }

                                    if(in_array('lpoa', $item['check'])){
                                        if($user->permission_type == 1 && $member->lpoa_status == 'Y') $showDisplay = 1;

                                        if(in_array('fundchgpass', $item['check'])){
                                            if(date('l') == 'Saturday' || date('l') == 'Sunday'){
                                                if(date('l') == 'Saturday' && date('H', time()) < 9) $showDisplay = 0;
                                            }else{
                                                $showDisplay = 0;
                                            }
                                        }
                                    }

                                    if(in_array('bypassTWallet', $item['check'])){
                                        if(in_array($user->username, config('member.bypassTWallet'))) $showDisplay = 1;
                                    }

                                    if(in_array('bypassNetwork', $item['check'])){
                                        if(in_array($user->username, config('member.bypassNetwork'))) $showDisplay = 1;
                                    }
                                ?>
                            @else
                                <?php $showDisplay = 1; ?>
                            @endif

                            @if($showDisplay)
                                <li class="dropdown_menu">
                                    <a class="text-white" href="#">
                                        <i class="{{ $item['icon'] }}"></i>
                                        <span class="link-title menu_hide">&nbsp; {{ \Lang::get($item['displayName']) }}</span>
                                        <span class="fa arrow menu_hide"></span>
                                    </a>
                                    <ul>
                                        @foreach($item['child'] as $childname => $childitem)
                                            @if(in_array($childitem['right'], $userRights))
                                                @if(isset($childitem['child']))
                                                    @if(isset($childitem['check']))
                                                        <?php
                                                            $showDisplay = 0;
                                                            
                                                            if(in_array('account2Network', $childitem['check'])){
                                                                if($account2Network > 0) $showDisplay = 1;
                                                            }

                                                            if(in_array('seaNetwork', $childitem['check'])){
                                                                if($seaNetwork > 0) $showDisplay = 1;
                                                            }

                                                            if(in_array('ranking', $childitem['check'])){
                                                                if(isset($ranking)){
                                                                    if($ranking->rank) $showDisplay = 1;

                                                                    if(in_array('MIBPIBRank', $childitem['check'])){
                                                                        if($ranking->rank <> 'MIB' && $ranking->rank <> 'PIB') $showDisplay = 0;
                                                                    }
                                                                }
                                                            }

                                                            if(in_array('purchasedTrcap', $childitem['check'])){
                                                                if($member->coin->trc > 0) $showDisplay = 1;
                                                            }

                                                            if(in_array('lpoa', $childitem['check'])){
                                                                if($user->permission_type == 1 && $member->lpoa_status == 'Y') $showDisplay = 1;

                                                                if(in_array('fundchgpass', $childitem['check'])){
                                                                    if(date('l') == 'Saturday' || date('l') == 'Sunday'){
                                                                        if(date('l') == 'Saturday' && date('H', time()) < 9) $showDisplay = 0;
                                                                    }else{
                                                                        $showDisplay = 0;
                                                                    }
                                                                }
                                                            }

                                                            if(in_array('bypassTWallet', $childitem['check'])){
                                                                if(in_array($user->username, config('member.bypassTWallet'))) $showDisplay = 1;
                                                            }

                                                            if(in_array('bypassNetwork', $childitem['check'])){
                                                                if(in_array($user->username, config('member.bypassNetwork'))) $showDisplay = 1;
                                                            }
                                                        ?>
                                                    @else
                                                        <?php $showDisplay = 1; ?>
                                                    @endif
                                                    
                                                    @if($showDisplay)
                                                        <li>
                                                            <a class="text-white" href="#"><i class="fa fa-angle-right"></i>&nbsp; {{ \Lang::get($childitem['displayName']) }}<span class="fa arrow"></span></a>
                                                            <ul class="sub-menu sub-submenu">
                                                                @foreach($childitem['child'] as $child1name => $child1item)
                                                                    @if(in_array($child1item['right'], $userRights))
                                                                        @if(isset($child1item['check']))
                                                                            <?php
                                                                                $showDisplay = 0;
                                                                                
                                                                                if(in_array('account2Network', $child1item['check'])){
                                                                                    if($account2Network > 0) $showDisplay = 1;
                                                                                }

                                                                                if(in_array('seaNetwork', $child1item['check'])){
                                                                                    if($seaNetwork > 0) $showDisplay = 1;
                                                                                }

                                                                                if(in_array('ranking', $child1item['check'])){
                                                                                    if(isset($ranking)){
                                                                                        if($ranking->rank) $showDisplay = 1;

                                                                                        if(in_array('MIBPIBRank', $child1item['check'])){
                                                                                            if($ranking->rank <> 'MIB' && $ranking->rank <> 'PIB') $showDisplay = 0;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                if(in_array('purchasedTrcap', $child1item['check'])){
                                                                                    if($member->coin->trc > 0) $showDisplay = 1;
                                                                                }

                                                                                if(in_array('lpoa', $child1item['check'])){
                                                                                    if($user->permission_type == 1 && $member->lpoa_status == 'Y') $showDisplay = 1;

                                                                                    if(in_array('fundchgpass', $child1item['check'])){
                                                                                        if(date('l') == 'Saturday' || date('l') == 'Sunday'){
                                                                                            if(date('l') == 'Saturday' && date('H', time()) < 9) $showDisplay = 0;
                                                                                        }else{
                                                                                            $showDisplay = 0;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                if(in_array('bypassTWallet', $child1item['check'])){
                                                                                    if(in_array($user->username, config('member.bypassTWallet'))) $showDisplay = 1;
                                                                                }

                                                                                if(in_array('bypassNetwork', $child1item['check'])){
                                                                                    if(in_array($user->username, config('member.bypassNetwork'))) $showDisplay = 1;
                                                                                }
                                                                            ?>
                                                                        @else
                                                                            <?php $showDisplay = 1; ?>
                                                                        @endif
                                                                        
                                                                        @if($showDisplay)
                                                                            <li>
                                                                                <a class="text-white" href="{{ route($child1item['route'], ['lang' => \App::getLocale()]) }}" @if(isset($child1item['newTab']))target="_level2"@endif>
                                                                                    <i class="fa fa-angle-right"></i>
                                                                                    &nbsp; {{ \Lang::get($child1item['displayName']) }}
                                                                                </a>
                                                                            </li>
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                            </ul>
                                                        </li>
                                                    @endif
                                                @else
                                                    @if(isset($childitem['check']))
                                                        <?php
                                                            $showDisplay = 0;
                                                            
                                                            if(in_array('account2Network', $childitem['check'])){
                                                                if($account2Network > 0) $showDisplay = 1;
                                                            }

                                                            if(in_array('seaNetwork', $childitem['check'])){
                                                                if($seaNetwork > 0) $showDisplay = 1;
                                                            }

                                                            if(in_array('ranking', $childitem['check'])){
                                                                if(isset($ranking)){
                                                                    if($ranking->rank) $showDisplay = 1;

                                                                    if(in_array('MIBPIBRank', $childitem['check'])){
                                                                        if($ranking->rank <> 'MIB' && $ranking->rank <> 'PIB') $showDisplay = 0;
                                                                    }
                                                                }
                                                            }

                                                            if(in_array('purchasedTrcap', $childitem['check'])){
                                                                if($member->coin->trc > 0) $showDisplay = 1;
                                                            }

                                                            if(in_array('lpoa', $childitem['check'])){
                                                                if($user->permission_type == 1 && $member->lpoa_status == 'Y') $showDisplay = 1;

                                                                if(in_array('fundchgpass', $childitem['check'])){
                                                                    if(date('l') == 'Saturday' || date('l') == 'Sunday'){
                                                                        if(date('l') == 'Saturday' && date('H', time()) < 9) $showDisplay = 0;
                                                                    }else{
                                                                        $showDisplay = 0;
                                                                    }
                                                                }
                                                            }

                                                            if(in_array('bypassTWallet', $childitem['check'])){
                                                                if(in_array($user->username, config('member.bypassTWallet'))) $showDisplay = 1;
                                                            }

                                                            if(in_array('bypassNetwork', $childitem['check'])){
                                                                if(in_array($user->username, config('member.bypassNetwork'))) $showDisplay = 1;
                                                            }
                                                        ?>
                                                    @else
                                                        <?php $showDisplay = 1; ?>
                                                    @endif
                                                    
                                                    @if($showDisplay)
                                                        <li>
                                                            <a class="text-white" href="{{ route($childitem['route'], ['lang' => \App::getLocale()]) }}" @if(isset($childitem['newTab']))target="_level1"@endif>
                                                                <i class="fa fa-angle-right"></i>
                                                                &nbsp; {{ \Lang::get($childitem['displayName']) }}
                                                            </a>
                                                        </li>
                                                    @endif
                                                @endif
                                            @endif
                                        @endforeach
                                    </ul>
                                </li>
                            @endif
                        @else
                            @if(isset($item['check']))
                                <?php 
                                    $showDisplay = 0;
                                    
                                    if(in_array('account2Network', $item['check'])){
                                        if($account2Network > 0) $showDisplay = 1;
                                    }

                                    if(in_array('seaNetwork', $item['check'])){
                                        if($seaNetwork > 0) $showDisplay = 1;
                                    }

                                    if(in_array('ranking', $item['check'])){
                                        if(isset($ranking)){
                                            if($ranking->rank) $showDisplay = 1;

                                            if(in_array('MIBPIBRank', $item['check'])){
                                                if($ranking->rank <> 'MIB' && $ranking->rank <> 'PIB') $showDisplay = 0;
                                            }
                                        }
                                    }

                                    if(in_array('purchasedTrcap', $item['check'])){
                                        if($member->coin->trc > 0) $showDisplay = 1;
                                    }

                                    if(in_array('lpoa', $item['check'])){
                                        if($user->permission_type == 1 && $member->lpoa_status == 'Y') $showDisplay = 1;

                                        if(in_array('fundchgpass', $item['check'])){
                                            if(date('l') == 'Saturday' || date('l') == 'Sunday'){
                                                if(date('l') == 'Saturday' && date('H', time()) < 9) $showDisplay = 0;
                                            }else{
                                                $showDisplay = 0;
                                            }
                                        }
                                    }

                                    if(in_array('bypassTWallet', $item['check'])){
                                        if(in_array($user->username, config('member.bypassTWallet'))) $showDisplay = 1;
                                    }

                                    if(in_array('bypassNetwork', $item['check'])){
                                        if(in_array($user->username, config('member.bypassNetwork'))) $showDisplay = 1;
                                    }
                                ?>
                            @else
                                <?php $showDisplay = 1; ?>
                            @endif
                            
                            @if($showDisplay)
                                <li>
                                    <a class="text-white" href="{{ route($item['route'], ['lang' => \App::getLocale()]) }}" @if(isset($item['newTab']))target="_level0"@endif>
                                        <i class="{{ $item['icon'] }}"></i>
                                        <span class="link-title menu_hide">&nbsp; {{ \Lang::get($item['displayName']) }}</span>
                                        @if($item['right'] == 10)
                                            @if($unreadNotification + $unreadAnnouncement > 0)
                                                <span class="badge badge-pill badge-primary float-right calendar_badge menu_hide unreadAnnouncement" id="unreadAnnouncement">{{ $unreadNotification + $unreadAnnouncement}}</span>
                                            @endif
                                        @elseif($item['right'] == 11)
                                            @if($unreadTicket > 0)
                                                <span class="badge badge-pill badge-primary float-right calendar_badge menu_hide">{{ $unreadTicket }}</span>
                                            @endif
                                        @endif
                                    </a>
                                </li>
                            @endif
                        @endif
                    @endif
                @endforeach
            </ul>
        </div>
    </div>