<?php

    use App\Repositories\MemberRepository;
    $memberRepo = new MemberRepository;
    $user = \Sentinel::getUser();
    $member = $memberRepo->findByUsername(trim($user->username));
    $member->load('wallet');
    $member->load('detail');
    
    if (time() >= strtotime(config('autosettings.maintenanceStart')) && time() < strtotime(config('autosettings.maintenanceEnd'))) {
        if(!in_array($user->username, config('autosettings.maintenanceUserAccess'))){
            session()->flush();
            header('Location: '.route('maintenance', ['lang' => \App::getLocale()]));exit();
        }
    }

    $route = \Route::currentRouteName();
    if (is_null($route)) $route = 'home';

    if ($route == 'helpdesk.read' || $route == 'bankinfo.detail' || $route == 'inbox.read') {
        $routeEN = route($route, ['lang' => 'en', 'id' => $model->id]);
        $routeCHS = route($route, ['lang' => 'chs', 'id' => $model->id]);
    }else{
        $routeEN = route($route, ['lang' => 'en']);
        $routeCHS = route($route, ['lang' => 'chs']);
    }

    $unreadTicket = \DB::table('helpdesk')->where('user_id', $member->id)->where('is_reply', 1)->where('read', 1)->count();
    $unreadNotification = \DB::table('notification_target')->where('member_id', $member->id)->where('read', 0)->count();
    $unreadAnnouncement = 0;
    if($member->detail->announcement) $unreadAnnouncement = 1;

    $ranking = \DB::table('Member_Sales_Summary')->where('member_id', $member->id)->orderBy('date', 'DESC')->first();
    $twalletDisplay = 0;
    if(isset($ranking)){
        if($ranking->rank) $twalletDisplay = 1;
    }

    $seaNetwork = \DB::table('Member_Network')->where('parent_username', 'seanetlead@gmail.com')->where('username', $member->register_by)->count();
    if(in_array($user->username, ['seanetlead@gmail.com']) || $member->register_by == 'seanetlead@gmail.com') $seaNetwork = 1;
?>
<div id="wrap">
    <div id="top" class="fixed">
        <nav class="navbar navbar-static-top">

            <div class="container-fluid m-0">
                <div class="menu" style="margin-left: 0px; padding-right: 15px;">
                    <span class="toggle-left" id="menu-toggle">
                        <i class="fa fa-bars" style="color: #8657f0;"></i>
                    </span>
                </div>
                <a class="navbar-brand mr-sm-auto" href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                    <h4><img src="{{asset('assets/img/new_logo.png')}}" class="admin_img" alt="logo" style="width: 80px; height: 35px;"></h4>
                </a>

                @if(in_array($member->detail->access_type, [3,4]))
                <a href="#" class="btnHover" style="margin-right: 15px;" onclick="getBalance()">
                    <i class="fa fa-refresh" style="font-size: 18px;"></i>
                </a>

                <a href="{{ route('inbox.list', ['lang' => \App::getLocale()]) }}" class="btnHover" style="margin-right: 15px;">
                    <i class="fa fa-envelope-o" style="font-size: 18px;"></i>
                    @if($unreadNotification + $unreadAnnouncement > 0)
                    <span class="badge badge-pill badge-danger notifications_badge_top unreadAnnouncement" style="position: relative;top: -12px !important;">{{$unreadNotification + $unreadAnnouncement}}</span>
                    @endif
                </a>
                @endif

                <a href="{{ route('helpdesk.list', ['lang' => \App::getLocale()]) }}" class="btnHover" style="margin-right: 15px;">
                    <i class="fa fa-ticket" style="font-size: 18px;"></i>
                    @if($unreadTicket > 0)
                    <span class="badge badge-pill badge-danger notifications_badge_top" style="position: relative;top: -12px !important;">{{$unreadTicket}}</span>
                    @endif
                </a>

                <div class="btn-group" style="padding-right: 15px;">
                    @if(\App::getLocale() == 'en')
                    <strong><a href="{{ $routeCHS }}"><span class="btnHover"><i class="fa fa-language" style="font-size: 18px;"></i></span></a></strong>
                    @else
                    <strong><a href="{{ $routeEN }}"><span class="btnHover"><i class="fa fa-language" style="font-size: 18px;"></i></span></a></strong>
                    @endif
                </div>
                <div class="btn-group" style="padding-right: 15px;">
                <strong><a href="{{ route('logout', ['lang' => \App::getLocale()]) }}"><span class="btnHover"><i class="fa fa-sign-out"></i> @lang('sidebar.logout')</span></a>
                </div>
            </div>

        </nav>
    </div>

    @include('front.include.sidebar')

    @if(in_array($member->detail->access_type, [3,4]))
    <div id="right">
        <div class="right_content">
            <div class="well-small dark">
                <div class="row m-0">
                    <div style="background-color: #8657f0; width: 100%; height: 100px; border-radius: 10px 10px 0px 0px;">
                        <div style="margin: 10px 0 0 25px;"><i style="background-color: #6529eb; padding: 10px; border-radius: 70px; width: 40px; text-align: center; font-size: 20px; font-weight: bold; margin-right: 10px;" class="fa"><img style="height: 20px; margin-bottom: 2px;" src="{{asset('assets/img/wallet.png')}}"></i><span style="font-size: 20px; color: #fff;">@lang('common.mywallet')</span></div>
                    </div>
                    <div style="border-left: 5px solid #FFC800; background-color: #fff; width: 100%; border-radius: 5px; box-shadow: 2px 4px 25px -8px #989898; margin: -35px 25px 10px 25px; padding: 5px 15px;">
                        <div class="float-left">
                            <p style="font-size: 18px; color: #FFC800; margin-bottom: 0px; margin-top: 5px" class="balanceCash">$ {{ number_format($member->wallet->cash_point,2) }}</p>
                            <p style="font-size: 12px; color: #a3a3b5;">@lang('common.cashTitle')</p>
                        </div>
                        <div class="text-right" style="font-size: 33px;"><i style="background-color: #FFC800; padding: 10px; border-radius: 70px; width: 40px; text-align: center; font-size: 20px; font-weight: bold;" class="fa"><img style="height: 20px; margin-bottom: 2px;" src="{{asset('assets/img/wallet.png')}}"></i></div>
                    </div>
                    @if(!$seaNetwork)
                    <div style="border-left: 5px solid #3FB6DC; background-color: #fff; width: 100%; border-radius: 5px; box-shadow: 2px 4px 25px -8px #989898; margin: 0px 25px 10px 25px; padding: 5px 15px;">
                        <div class="float-left">
                            <p style="font-size: 18px; color: #3FB6DC; margin-bottom: 0px; margin-top: 5px" class="balanceW">$ {{ number_format($member->wallet->w_wallet,2) }}</p>
                            <p style="font-size: 12px; color: #a3a3b5;">@lang('common.wTitle')</p>
                        </div>
                        <div class="text-right" style="font-size: 33px;"><i style="background-color: #3FB6DC; padding: 10px; border-radius: 70px; width: 40px; text-align: center; font-size: 20px; font-weight: bold;" class="fa"><img style="height: 20px; margin-bottom: 2px;" src="{{asset('assets/img/wallet.png')}}"></i></div>
                    </div>
                    
                    @endif
                    <div style="border-left: 5px solid #2DC76D; background-color: #fff; width: 100%; border-radius: 5px; box-shadow: 2px 4px 25px -8px #989898; margin: 0px 25px 10px 25px; padding: 5px 15px;">
                        <div class="float-left">
                            <p style="font-size: 18px; color: #2DC76D; margin-bottom: 0px; margin-top: 5px" class="balanceIncome">$ {{ number_format(floor($member->wallet->promotion_point * 100) / 100, 2) }}</p>
                            <p style="font-size: 12px; color: #a3a3b5;">@lang('common.promotionTitle')</p>
                        </div>
                        <div class="text-right" style="font-size: 33px;"><i style="background-color: #2DC76D; padding: 10px; border-radius: 70px; width: 40px; text-align: center; font-size: 20px; font-weight: bold;" class="fa"><img style="height: 20px; margin-bottom: 2px;" src="{{asset('assets/img/wallet.png')}}"></i></div>
                    </div>
                    @if($twalletDisplay || in_array($user->username, config('member.bypassTWallet')))
                        <div style="border-left: 5px solid #6f42c1; background-color: #fff; width: 100%; border-radius: 5px; box-shadow: 2px 4px 25px -8px #989898; margin: 0px 25px 10px 25px; padding: 5px 15px;">
                            <div class="float-left">
                                <p style="font-size: 18px; color: #6f42c1; margin-bottom: 0px; margin-top: 5px" class="balanceT">$ {{ number_format($member->wallet->t_wallet,2) }}</p>
                                <p style="font-size: 12px; color: #a3a3b5;">@lang('common.transferTitle')</p>
                            </div>
                            <div class="text-right" style="font-size: 33px;"><i style="background-color: #6f42c1; padding: 10px; border-radius: 70px; width: 40px; text-align: center; font-size: 20px; font-weight: bold;" class="fa"><img style="height: 20px; margin-bottom: 2px;" src="{{asset('assets/img/wallet.png')}}"></i></div>
                        </div>
                    @else
                        @if($member->wallet->t_wallet > 0)
                            <div style="border-left: 5px solid #bac4c5; background-color: #fff; width: 100%; border-radius: 5px; box-shadow: 2px 4px 25px -8px #989898; margin: 0px 25px 10px 25px; padding: 5px 15px;">
                                <div class="float-left">
                                    <p style="font-size: 18px; color: #bac4c5; margin-bottom: 0px; margin-top: 5px" class="balanceT">$ {{ number_format($member->wallet->t_wallet,2) }}</p>
                                    <p style="font-size: 12px; color: #a3a3b5;">@lang('common.transferTitle')</p>
                                </div>
                                <div class="text-right" style="font-size: 33px;"><i style="background-color: #bac4c5; padding: 10px; border-radius: 70px; width: 40px; text-align: center; font-size: 20px; font-weight: bold;" class="fa"><img style="height: 20px; margin-bottom: 2px;" src="{{asset('assets/img/wallet.png')}}"></i></div>
                            </div>
                        @endif
                    @endif
                    <div style="border-left: 5px solid #33DACA; background-color: #fff; width: 100%; border-radius: 5px; box-shadow: 2px 4px 25px -8px #989898; margin: 0px 25px 10px 25px; padding: 5px 15px;">
                        <div class="float-left">
                            <p style="font-size: 18px; color: #33DACA; margin-bottom: 0px; margin-top: 5px" class="balanceCapx">$ {{ number_format($member->coin->usd,2) }}</p>
                            <p style="font-size: 12px; color: #a3a3b5;">@lang('capx.capxwallet')</p>
                        </div>
                        <div class="text-right" style="font-size: 33px;"><i style="padding: 10px 10px 10px 0px; border-radius: 70px; width: 40px; text-align: center; font-size: 20px; font-weight: bold; position: relative; right: 20px;" class="fa"><img style="height: 20px; margin-bottom: 2px;" src="{{asset('assets/img/capx/capx.png')}}"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

<script>
    function getBalance(){
        var disable = loadingDisable();
        $.ajax({
            url     :   "{{ route('capx.getBalance', ['lang' => \App::getLocale()]) }}",
            method  :   "get",
            headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
            success : function(response){
                $('.balanceCash').text('$ '+number_format(response.balance.cash, 2));
                $('.balanceW').text('$ '+number_format(response.balance.w, 2));
                $('.balanceIncome').text('$ '+number_format(response.balance.income, 2));
                $('.balanceT').text('$ '+number_format(response.balance.t, 2));
                $('.balanceCapx').text('$ '+number_format(response.balance.capx, 2));
                disable.out();
            },
            error: function (response) {
                if(response.readyState == 4){
                    notiAlert(2, "{{ \Lang::get('error.sessionExpired') }}", '{{\App::getLocale()}}');
                    window.location.reload();
                }
            }
        });
    }
</script>