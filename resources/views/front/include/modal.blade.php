<button class="btn btn-raised btn-success adv_cust_mod_btn zoomin" data-toggle="modal" data-target="#modal-zoomin" id="modalZoomIn" style="display: none;"></button>
                                    
<div class="modal" tabindex="-1" id="modal-zoomin" role="dialog" aria-labelledby="modalLabelzoom" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #25265E;">
                <h4 class="modal-title text-white" id="modalLabel">@lang('misc.details')</h4>
                <button class="btn btn-secondary" data-dismiss="modal" >@lang('misc.close')</button>
            </div>
            <div class="modal-body" id="modalContent">
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary"  id="modalBtn" style="display: none;">@lang('common.confirm')</button>
                <button class="btn btn-secondary" data-dismiss="modal" >@lang('misc.close')</button>
            </div>
        </div>
    </div>
</div> 


