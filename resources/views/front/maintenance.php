<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width">
        <link rel="shortcun icon" href="{{ asset('assets/img/new_logo.png') }}" type="image/x-icon" />
        <title>Performing Maintenance</title>
        <meta name="description" content="Performing maintenance...">
        <style type="text/css">
            /* Reset */
            html, body, div, span, applet, object, iframe,
            h1, h2, h3, h4, h5, h6, p, blockquote, pre,
            a, abbr, acronym, address, big, cite, code,
            del, dfn, em, img, ins, kbd, q, s, samp,
            small, strike, strong, sub, sup, tt, var,
            b, u, i, center,
            dl, dt, dd, ol, ul, li,
            fieldset, form, label, legend,
            table, caption, tbody, tfoot, thead, tr, th, td,
            article, aside, canvas, details, embed, 
            figure, figcaption, footer, header, hgroup, 
            menu, nav, output, ruby, section, summary,
            time, mark, audio, video {
                margin: 0;
                padding: 0;
                border: 0;
                font-size: 100%;
                font: inherit;
                vertical-align: baseline;
            }
            
            /* HTML5 display-role reset for older browsers */
            article, aside, details, figcaption, figure, 
            footer, header, hgroup, menu, nav, section {
                display: block;
            }
            body {
                line-height: 1;
            }
            ol, ul {
                list-style: none;
            }
            blockquote, q {
                quotes: none;
            }
            blockquote:before, blockquote:after, q:before, q:after {
                content: '';
                content: none;
            }
            table {
                border-collapse: collapse;
                border-spacing: 0;
            }
              
            html {font-size: 16px;}
            body { text-align: center; padding: 50px; }
            h1 { font-size: 40px; font-weight: bold; margin-bottom: 1rem;}
            p {font-size: 1.5rem; margin-bottom: 1rem;}
            body { font: 20px Helvetica, sans-serif; color: #333; }
            #article { display: block; text-align: left; width: 650px; margin: 0 auto; }
            a { color: #dc8100; text-decoration: none; }
            a:hover { color: #333; text-decoration: none; }
              
            #maintenance-page {
              text-align: center;
              font: 1em "Open Sans",sans-serif;
              color: #333;
            }
            #maintenance-page h1 {
              text-align: center;
              font: bold 2.5em "Open Sans",sans-serif;
              color: #084D8B;
              margin: 0;
            }
            #maintenance-page p {
                font-size: 1em;
            }
        </style>
    </head>
    <body data-gr-c-s-loaded="true">
        <div id="article">
            <div>
                <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
                <div id="maintenance-page">
                    <h1 class="nl_open-sans">Website Is Undergoing Update Maintenance<br />本网站正在进行更新维护</h1>
                    <img src="https://myprovidence.healthtrioconnect.com/asset/maintenance.png">
                    <p>
                        This website is temporarily unavailable due to update maintenance.<br />
                        由于网站正在进行更新维护，此页面暂时无法使用。<br />
                        Please try again later. <br />
                        请稍后再试。<br />
                        We apologize for any inconvenience caused.<br />
                        如有不便之处，敬请见谅。<br />
                    </p>
                </div>
            </div>
        </div>
    </body>
</html>