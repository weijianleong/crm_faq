<?php
    use App\Repositories\MemberRepository;
    $memberRepo = new MemberRepository;
    $member = $memberRepo->findByUsername(trim($user->username));
    $member->load('wallet');
    $member->load('detail');

    $seaNetwork = \DB::table('Member_Network')->where('parent_username', 'seanetlead@gmail.com')->where('username', $member->register_by)->count();
    if(in_array($user->username, ['seanetlead@gmail.com']) || $member->register_by == 'seanetlead@gmail.com') $seaNetwork = 1;

    $lang = \App::getLocale();
    if($seaNetwork > 0) $announcement = \DB::table('Announcement')->whereIn('access_type', [0,2])->orderBy('created_at', 'DESC')->first();
    else $announcement = \DB::table('Announcement')->whereIn('access_type', [0,1])->orderBy('created_at', 'DESC')->first();
    
    if(isset($announcement)){
        $announcementTitle = null;
        $announcementContent = null;
        if (isset($announcement->{"title_$lang"})) $announcementTitle = $announcement->{"title_$lang"};
        else $announcementTitle = $announcement->title_en;
        if (isset($announcement->{"content_$lang"})) $announcementContent = $announcement->{"content_$lang"};
        else $announcementContent = $announcement->content_en;
    }

    $downline = \DB::table('Member_Network')->where('parent_id', $member->id)->count();

    $ranking = \DB::table('Member_Sales_Summary')->where('member_id', $member->id)->orderBy('date', 'DESC')->first();

    $rankDisplay = 0;
    $twalletDisplay = 0;
    $maxSubscribe = '';
    $rank = '';

    if(isset($ranking)){
        if($ranking->rank == 'MIB' || $ranking->rank == 'PIB') $rankDisplay = 1;
        if($ranking->rank) $twalletDisplay = 1;

        if(isset($ranking->conditions2)){
            $conditions2 = json_decode($ranking->conditions2);
            if(in_array(7, $conditions2)){
                if(in_array($member->id, config('autosettings.bypasstrcapcheck'))){
                    $maxSubscribe = \Lang::get('capx.nolimit');
                    $rank = $ranking->rank.' ('.\Lang::get('capx.networkPIB').')';
                }else{
                    $currentMonth = $ranking->date;
                    $realNetwork = \DB::table('Member_Sales_Summary')->join('Member', 'Member_Sales_Summary.member_id', '=', 'Member.id')
                                ->where('Member.direct_id', $member->id)
                                ->whereIn('Member_Sales_Summary.rank', ['MIB','PIB'])
                                ->where('conditions2', 'LIKE', '%6%')
                                ->where('date', $currentMonth)
                                ->count();

                    if($realNetwork >= 6){
                        $maxSubscribe = \Lang::get('capx.nolimit');
                        $rank = $ranking->rank.' ('.\Lang::get('capx.networkPIB').')';
                    }else{
                        $maxSubscribe = config('capx.maxTrcap.MIB')-$member->coin->trc;
                        $maxSubscribe = $maxSubscribe <= 0 ? 0 : number_format($maxSubscribe);
                        $rank = $ranking->rank.' ('.\Lang::get('capx.networkMIB').')';
                    }
                }
            }elseif(in_array(6, $conditions2)){
                $maxSubscribe = config('capx.maxTrcap.MIB')-$member->coin->trc;
                $maxSubscribe = $maxSubscribe <= 0 ? 0 : number_format($maxSubscribe);
                $rank = $ranking->rank.' ('.\Lang::get('capx.networkMIB').')';
            }else{
                $maxSubscribe = config('capx.maxTrcap.IB')-$member->coin->trc;
                $maxSubscribe = $maxSubscribe <= 0 ? 0 : number_format($maxSubscribe);
                $rank = $ranking->rank.' ('.\Lang::get('capx.networkIB').')';
            }
        }
    }

    $margin = DB::table('Capx_Margin')->where('status', 1)->orderBy('id', 'DESC')->limit(1)->get();
    $cashBalance = $member->wallet->cash_point;
    $coin = DB::table('Capx_Coin')->where('type', 0)->first();
    $soldout = DB::table('Capx_Member_Coin')->sum('trc');
?>
@extends('front.app')

@section('title')
    @lang('breadcrumbs.dashboard') | {{ config('app.name') }}
@stop

@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/widgets.css')}}">
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/new_dashboard.css')}}">
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/progress-bar.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/countdown-clock.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/login2.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/animate/css/animate.min.css')}}" />
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <div class="outer" style="margin-top: 0px;">
        <div class="inner bg-container">
            <div class="row">
                <div class="col-xl-12 col-lg-10 col-12">
                    <div class="row">
                            <div class="col-sm-3 col-12 m-t-15">
                                <div class="icon_align" style="border-left: 5px solid #FFC800; box-shadow: 2px 4px 25px -8px #989898;">
                                    <div class="float-left">
                                    <p style="font-size: 25px; color: #FFC800; margin-bottom: 0px; margin-top: 5px" class="balanceCash">$ {{ number_format($member->wallet->cash_point,2) }}</p>
                                    <p style="font-size: 15px; color: #6c6c86;">@lang('common.cashTitle')</p>
                                </div>
                                <div class="text-right" style="margin: 5px 15px 11px"><i style="background-color: #FFC800; padding: 10px; border-radius: 70px; width: 50px; text-align: center; font-size: 30px; font-weight: bold;" class="fa"><img style="height: 20px; margin-bottom: 5px;" src="{{asset('assets/img/wallet.png')}}"></i></div>
                                </div>
                            </div>
                            @if(!$seaNetwork)
                            <div class="col-sm-3 col-12 m-t-15">
                                <div class="icon_align" style="border-left: 5px solid #3FB6DC; box-shadow: 2px 4px 25px -8px #989898;">
                                    <div class="float-left">
                                    <p style="font-size: 25px; color: #3FB6DC; margin-bottom: 0px; margin-top: 5px" class="balanceW">$ {{ number_format($member->wallet->w_wallet,2) }}</p>
                                    <p style="font-size: 15px; color: #6c6c86;">@lang('common.wTitle')</p>
                                </div>
                                <div class="text-right" style="margin: 5px 15px 11px"><i style="background-color: #3FB6DC; padding: 10px; border-radius: 70px; width: 50px; text-align: center; font-size: 30px; font-weight: bold;" class="fa"><img style="height: 20px; margin-bottom: 5px;" src="{{asset('assets/img/wallet.png')}}"></i></div>
                                </div>
                            </div>
                            @endif
                            <div class="col-sm-3 col-12 m-t-15">
                                <div class="icon_align" style="border-left: 5px solid #2DC76D; box-shadow: 2px 4px 25px -8px #989898;">
                                    <div class="float-left">
                                    <p style="font-size: 25px; color: #2DC76D; margin-bottom: 0px; margin-top: 5px" class="balanceIncome">$ {{ number_format(floor($member->wallet->promotion_point * 100) / 100, 2) }}</p>
                                    <p style="font-size: 15px; color: #6c6c86;">@lang('common.promotionTitle')</p>
                                </div>
                                <div class="text-right" style="margin: 5px 15px 11px"><i style="background-color: #2DC76D; padding: 10px; border-radius: 70px; width: 50px; text-align: center; font-size: 30px; font-weight: bold;" class="fa"><img style="height: 20px; margin-bottom: 5px;" src="{{asset('assets/img/wallet.png')}}"></i></div>
                                </div>
                            </div>

                            @if($twalletDisplay || in_array($user->username, config('member.bypassTWallet')))
                                <div class="col-sm-3 col-12 m-t-15">
                                    <div class="icon_align" style="border-left: 5px solid #6f42c1; box-shadow: 2px 4px 25px -8px #989898;">
                                        <div class="float-left">
                                        <p style="font-size: 25px; color: #6f42c1; margin-bottom: 0px; margin-top: 5px" class="balanceT">$ {{ number_format($member->wallet->t_wallet,2) }}</p>
                                        <p style="font-size: 15px; color: #6c6c86;">@lang('common.transferTitle')</p>
                                    </div>
                                    <div class="text-right" style="margin: 5px 15px 11px"><i style="background-color: #6f42c1; padding: 10px; border-radius: 70px; width: 50px; text-align: center; font-size: 30px; font-weight: bold;" class="fa"><img style="height: 20px; margin-bottom: 5px;" src="{{asset('assets/img/wallet.png')}}"></i></div>
                                    </div>
                                </div>
                            @else
                                @if($member->wallet->t_wallet > 0)
                                <div class="col-sm-3 col-12 m-t-15">
                                    <div class="icon_align" style="border-left: 5px solid #bac4c5; box-shadow: 2px 4px 25px -8px #989898;">
                                        <div class="float-left">
                                        <p style="font-size: 25px; color: #bac4c5; margin-bottom: 0px; margin-top: 5px" class="balanceT">$ {{ number_format($member->wallet->t_wallet,2) }}</p>
                                        <p style="font-size: 15px; color: #6c6c86;">@lang('common.transferTitle')</p>
                                    </div>
                                    <div class="text-right" style="margin: 5px 15px 11px"><i style="background-color: #bac4c5; padding: 10px; border-radius: 70px; width: 50px; text-align: center; font-size: 30px; font-weight: bold;" class="fa"><img style="height: 20px; margin-bottom: 5px;" src="{{asset('assets/img/wallet.png')}}"></i></div>
                                    </div>
                                </div>
                                @endif
                            @endif
                            <div class="col-sm-3 col-12 m-t-15">
                                <div class="icon_align" style="border-left: 5px solid #33DACA; box-shadow: 2px 4px 25px -8px #989898;">
                                    <div class="float-left">
                                    <p style="font-size: 25px; color: #33DACA; margin-bottom: 0px; margin-top: 5px" class="balanceCapx">$ {{ number_format($member->coin->usd,2) }}</p>
                                    <p style="font-size: 15px; color: #6c6c86;">@lang('capx.capxwallet')</p>
                                </div>
                                <div class="text-right" style="margin: 5px 15px 11px"><i style="padding: 10px 10px 10px 0px; border-radius: 70px; width: 50px; text-align: center; font-size: 30px; font-weight: bold;" class="fa"><img style="height: 20px; margin-bottom: 5px;" src="{{asset('assets/img/capx/capx.png')}}"></i></div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    @if(count($margin) > 0)
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            <i class="fa fa-table"></i> @lang('capx.trmargin')
                        </div>
                        <div class="card-body p-t-10">
                            <div class=" m-t-25">

                                @foreach ($margin as $item)
                                <?php $fundBalance = ($item->initial_unit - $item->current_unit) / $item->initial_unit * 100; ?>
                                <div style="box-shadow: 0px 0px 30px -5px #989898; padding: 15px; border-radius: 10px; background-color: #fcfdfe; margin-bottom: 25px;">
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.product'): <span style="color: #1b449d;">{{ \App::getLocale() == 'en' ? $item->name_en : $item->name_chs }}</span></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.fundsize'): <span style="color: #1b449d;">{{ number_format($item->initial_unit) }} </span></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.launcheddate'): <span style="color: #1b449d;">{{ $item->launched_date }}</span></div>
                                    </div>
                                    <div class="row" style="margin-bottom: 20px;">
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.price'): <span id="price_{{ $item->symbol }}" style="color: #1b449d;">$ {{ number_format($item->current_price,4) }}</span></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.fundbalance'): <span id="unit_{{ $item->symbol }}" style="color: #1b449d;">{{ number_format($item->current_unit) }} </span></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.salesdate'): <span style="color: #1b449d;">{{ $item->sales_date }} </span></div>
                                    </div>
                                    @if($item->launched_date > date("Y-m-d H:i:s"))
                                    <div class="col-lg-4" style="float: none; margin: 0 auto; text-align: center; padding: 25px 0px 25px 0px;">
                                        <div id="clockdiv_{{ $item->symbol }}" class="clockdivcss">
                                            <div>
                                                <div class="smalltext">@lang('capx.day')</div>
                                                <span class="days"></span>
                                            </div>
                                            <div>
                                                <div class="smalltext">@lang('capx.hour')</div>
                                                <span class="hours"></span>
                                            </div>
                                            <div>
                                                <div class="smalltext">@lang('capx.minute')</div>
                                                <span class="minutes"></span>
                                            </div>
                                            <div>
                                                <div class="smalltext">@lang('capx.second')</div>
                                                <span class="seconds"></span>
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                    <div id="progress_{{ $item->symbol }}">
                                        <p style="width:{{ $fundBalance<4?4:$fundBalance }}%" data-value="{{ number_format($fundBalance,2) }}">&nbsp;</p>
                                        <progress max="100" value="{{ $fundBalance }}" class="html5">
                                            <div class="progress-bar">
                                                <span style="width: 80%">{{ $fundBalance }}</span>
                                            </div>
                                        </progress>
                                    </div>
                                    <div class="row">
                                        <div class="text-center login_bottom col-lg-2" style="padding-top: 5px; border-bottom: none;">
                                            @if($item->current_unit <= 0)
                                                <label class="btn btn-block b_r_20 m-t-10" style="background-color: #ededed; cursor: not-allowed;" >@lang('capx.soldout')</label>
                                            @elseif(date('Y-m-d H:i:s') >= $item->launched_date && date('Y-m-d H:i:s') <= $item->expired_date)
                                                <button  onclick="subscribeForm('{{ \App::getLocale() == 'en' ? $item->name_en : $item->name_chs }}','{{ $item->symbol }}')" id="register_btn" type="button" class="btn btn-block b_r_20 m-t-10 text-white btn-login-hover" style="background-color: #25265E;">@lang('capx.buy')</button>
                                            @else
                                                <label class="btn btn-block b_r_20 m-t-10" style="background-color: #ededed; cursor: not-allowed;" >@lang('capx.upcoming')</label>
                                            @endif
                                        </div>
                                    </div>
                                    @if($item->sales_date > date('Y-m-d H:i:s') && $item->current_unit <= 0)
                                    <div class="col-lg-4" style="float: none; margin: 0 auto; text-align: center; padding: 25px 0px 25px 0px;">
                                        <div style="font-size: 20px; padding-bottom: 15px;">@lang('capx.countdownsalesdate')</div>
                                        <div id="clockdiv_{{ $item->symbol }}" class="clockdivcss">
                                            <div>
                                                <div class="smalltext">@lang('capx.day')</div>
                                                <span class="days"></span>
                                            </div>
                                            <div>
                                                <div class="smalltext">@lang('capx.hour')</div>
                                                <span class="hours"></span>
                                            </div>
                                            <div>
                                                <div class="smalltext">@lang('capx.minute')</div>
                                                <span class="minutes"></span>
                                            </div>
                                            <div>
                                                <div class="smalltext">@lang('capx.second')</div>
                                                <span class="seconds"></span>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    @endif
                                </div>

                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
@if(isset($ranking))
@if($ranking->rank)
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            <i class="fa fa-table"></i> @lang('capx.trcap')
                        </div>
                        <div class="card-body p-t-10">
                            <div class=" m-t-25">
                                <div style="box-shadow: 0px 0px 30px -5px #989898; padding: 15px; border-radius: 10px; background-color: #fcfdfe; margin-bottom: 25px;">
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.product'): <span style="color: #1b449d;">@lang('capx.trcap')</span></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.price'): <span style="color: #1b449d;">$ {{ number_format($coin->price,2) }}</span></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.launcheddate'): <span style="color: #1b449d;">{{ $coin->launched_date }}</span></div>
                                    </div>
                                    <div class="row" style="margin-bottom: 20px;">
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.rank'): <span style="color: #1b449d;">{{ $rank }} </span></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.fundbalance'): <span style="color: #1b449d;">{{ $maxSubscribe }} </span></div>
                                        <div class="col-lg-4" style="font-size: 20px;">@lang('capx.soldamount'): <span style="color: #1b449d;">{{ number_format($soldout) }} </span></div>
                                    </div>
                                    @if($coin->launched_date > date("Y-m-d H:i:s"))
                                    <div class="col-lg-4" style="float: none; margin: 0 auto; text-align: center; padding: 25px 0px 25px 0px;">
                                        <div style="font-size: 20px; padding-bottom: 15px;">@lang('capx.countdownselldate')</div>
                                        <div id="clockdiv" class="clockdivcss">
                                            <div>
                                                <div class="smalltext">@lang('capx.day')</div>
                                                <span class="days"></span>
                                            </div>
                                            <div>
                                                <div class="smalltext">@lang('capx.hour')</div>
                                                <span class="hours"></span>
                                            </div>
                                            <div>
                                                <div class="smalltext">@lang('capx.minute')</div>
                                                <span class="minutes"></span>
                                            </div>
                                            <div>
                                                <div class="smalltext">@lang('capx.second')</div>
                                                <span class="seconds"></span>
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                    <div class="row justify-content-md-center m-t-20" style="font-size: 20px;">
                                        <div class="text-center login_bottom col-lg-3" style="padding-top: 5px; border-bottom: none;">
                                            <button  onclick="subscribeCapForm()" type="button" class="btn btn-block b_r_20 m-t-10 text-white btn-login-hover" style="background-color: #25265E;">@lang('capx.subscribe')</button>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
@endif
@if($downline > 0 && $rankDisplay > 0)
    <div class="outer">
        <div class="inner bg-container m-t-15">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-12">
                    <div class="card" style="border: none; box-shadow: 2px 4px 25px -8px #989898; background-color: #fff;">
                        <div style="padding: 25px 20px; border-bottom: 1px solid #e5e5e5;">
                            <div style="float: left; color: #25265E; padding-top: 10px; margin-right: 80px;">
                                @lang('misc.grank')
                            </div>
                            <div style="float: left; color: #787993;">
                                @lang('home.type') : 
                                <select class="form-control" id="levelRank" style="width: 85px; display: inline-block; padding-left: 5px; margin-left: 5px; border-bottom: 1px solid #e5e5e5; border-left: none; border-right: none; border-top: none; color: #7540EE;">
                                    <option value="1">@lang('home.direct')</option>
                                    <option value="3">@lang('home.3level')</option>
                                    <option value="0">@lang('home.group')</option>
                                </select>
                            </div>
                            <div style="float: right; color: #787993;">
                                @lang('home.month') : @lang('home.near')
                                <select class="form-control" id="monthRank" style="width: 45px; display: inline-block; padding-left: 5px; margin-left: 5px; border-bottom: 1px solid #e5e5e5; border-left: none; border-right: none; border-top: none; color: #7540EE;">
                                    <option value="2">3</option>
                                </select>@lang('home.monthUnit')
                            </div>
                        </div>
                        <div style="padding: 20px 15px 25px;">
                            <div id="rankingChart" style="width: 100%; height: 400px;"></div>
                        </div>
                        <div style="padding: 15px 0px 15px 20px; color: #C0C0C0;">* @lang('home.graphDisclaimer')</div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-12">
                    <div class="card" style="border: none; box-shadow: 2px 4px 25px -8px #989898; background-color: #fff;">
                        <div style="padding: 25px 20px; border-bottom: 1px solid #e5e5e5;">
                            <div style="float: left; color: #25265E; padding-top: 10px; margin-right: 80px;">
                                @lang('misc.gmember')
                            </div>
                            <div style="float: left; color: #787993;">
                                @lang('home.type') : 
                                <select class="form-control" id="levelMember" style="width: 85px; display: inline-block; padding-left: 5px; margin-left: 5px; border-bottom: 1px solid #e5e5e5; border-left: none; border-right: none; border-top: none; color: #7540EE;">
                                    <option value="1">@lang('home.direct')</option>
                                    <option value="3">@lang('home.3level')</option>
                                    <option value="0">@lang('home.group')</option>
                                </select>
                            </div>
                            <div style="float: right; color: #787993;">
                                @lang('home.month') : @lang('home.near')
                                <select class="form-control" id="monthMember" style="width: 45px; display: inline-block; padding-left: 5px; margin-left: 5px; border-bottom: 1px solid #e5e5e5; border-left: none; border-right: none; border-top: none; color: #7540EE;">
                                    <option value="2">3</option>
                                </select>@lang('home.monthUnit')
                            </div>
                        </div>
                        <div style="padding: 20px 15px 25px;">
                            <div id="memberChart" style="width: 100%; height: 400px;"></div>
                        </div>
                        <div style="padding: 15px 0px 15px 20px; color: #C0C0C0;">* @lang('home.graphDisclaimer')</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-12">
                    <div class="card" style="border: none; box-shadow: 2px 4px 25px -8px #989898; background-color: #fff;">
                        <div style="padding: 25px 20px; border-bottom: 1px solid #e5e5e5;">
                            <div style="float: left; color: #25265E; padding-top: 10px; margin-right: 80px;">
                                @lang('misc.ginvest')
                            </div>
                            <div style="float: left; color: #787993;">
                                @lang('home.type') : 
                                <select class="form-control" id="levelInvest" style="width: 85px; display: inline-block; padding-left: 5px; margin-left: 5px; border-bottom: 1px solid #e5e5e5; border-left: none; border-right: none; border-top: none; color: #7540EE;">
                                    <option value="1">@lang('home.direct')</option>
                                    <option value="3">@lang('home.3level')</option>
                                    <option value="0">@lang('home.group')</option>
                                </select>
                            </div>
                            <div style="float: right; color: #787993;">
                                @lang('home.month') : @lang('home.near')
                                <select class="form-control" id="monthInvest" style="width: 45px; display: inline-block; padding-left: 5px; margin-left: 5px; border-bottom: 1px solid #e5e5e5; border-left: none; border-right: none; border-top: none; color: #7540EE;">
                                    <option value="2">3</option>
                                </select>@lang('home.monthUnit')
                            </div>
                        </div>
                        <div style="padding: 20px 15px 25px;">
                            <div id="investChart" style="width: 100%; height: 400px;"></div>
                        </div>
                        <div style="padding: 15px 0px 15px 20px; color: #C0C0C0;">* @lang('home.graphDisclaimer')</div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-12">
                    <div class="card" style="border: none; box-shadow: 2px 4px 25px -8px #989898; background-color: #fff;">
                        <div style="padding: 25px 20px; border-bottom: 1px solid #e5e5e5;">
                            <div style="float: left; color: #25265E; padding-top: 10px; margin-right: 80px;">
                                @lang('misc.gsales')
                            </div>
                            <div style="float: left; color: #787993;">
                                @lang('home.type') : 
                                <select class="form-control" id="levelSales" style="width: 85px; display: inline-block; padding-left: 5px; margin-left: 5px; border-bottom: 1px solid #e5e5e5; border-left: none; border-right: none; border-top: none; color: #7540EE;">
                                    <option value="1">@lang('home.direct')</option>
                                    <option value="3">@lang('home.3level')</option>
                                    <option value="0">@lang('home.group')</option>
                                </select>
                            </div>
                            <div style="float: right; color: #787993;">
                                @lang('home.month') : @lang('home.near')
                                <select class="form-control" id="monthSales" style="width: 45px; display: inline-block; padding-left: 5px; margin-left: 5px; border-bottom: 1px solid #e5e5e5; border-left: none; border-right: none; border-top: none; color: #7540EE;">
                                    <option value="2">3</option>
                                </select>@lang('home.monthUnit')
                            </div>
                        </div>
                        <div style="padding: 20px 15px 25px;">
                            <div id="salesChart" style="width: 100%; height: 400px;"></div>
                        </div>
                        <div style="padding: 15px 0px 15px 20px; color: #C0C0C0;">* @lang('home.graphDisclaimer')</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
@if(isset($announcement))
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card m-t-20" style="border: none; border-left: 5px solid #2F61D5; background-color: #fff; box-shadow: 2px 4px 25px -8px #989898;">
                        <div class="card-body p-t-10">
                            <div class="m-t-15">
                                <div style="background-color: rgb(47,97,213,0.15); color: #2F61D5; padding: 8px; float: left; border-radius: 20px;">{{ $announcement->created_at }}</div>
                                <div style="padding: 10px; float: right; color: #787993;">@lang('announcement.latestNews')</div>
                            </div>
                            <div style="margin-top: 70px; color: #25265E; font-size: 20px;">{{ $announcementTitle }}</div>
                            <div class="m-t-20" style="color: rgb(120,121,147,0.8);">{!! $announcementContent !!}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
</div>
@include('front.include.modal')
@stop

@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/amcharts/amcharts.js')}}" ></script>
<script type="text/javascript" src="{{asset('assets/amcharts/serial.js')}}" ></script>
<script type="text/javascript" src="{{asset('assets/js/pages/modals.js')}}"></script>
<script>
    $(document).ready( function () {
        var downline = "{{ $downline }}";
        var ranking = "{{ $rankDisplay }}";
        if(downline > 0 && ranking > 0){
            dashboardGraphRank(1,2);
            dashboardGraphMember(1,2);
            dashboardGraphInvest(1,2);
            dashboardGraphSales(1,2);
        }

        $('select#levelRank').on('change', function(event) {
            dashboardGraphRank($("select#levelRank option:selected").val(), $("select#monthRank option:selected").val());
        });
        $('select#monthRank').on('change', function(event) {
            dashboardGraphRank($("select#levelRank option:selected").val(), $("select#monthRank option:selected").val());
        });

        $('select#levelMember').on('change', function(event) {
            dashboardGraphMember($("select#levelMember option:selected").val(), $("select#monthMember option:selected").val());
        });
        $('select#monthMember').on('change', function(event) {
            dashboardGraphMember($("select#levelMember option:selected").val(), $("select#monthMember option:selected").val());
        });

        $('select#levelInvest').on('change', function(event) {
            dashboardGraphInvest($("select#levelInvest option:selected").val(), $("select#monthInvest option:selected").val());
        });
        $('select#monthInvest').on('change', function(event) {
            dashboardGraphInvest($("select#levelInvest option:selected").val(), $("select#monthInvest option:selected").val());
        });

        $('select#levelSales').on('change', function(event) {
            dashboardGraphSales($("select#levelSales option:selected").val(), $("select#monthSales option:selected").val());
        });
        $('select#monthSales').on('change', function(event) {
            dashboardGraphSales($("select#levelSales option:selected").val(), $("select#monthSales option:selected").val());
        });

        var coin = JSON.parse('{!! json_encode($coin) !!}');
        if('{{ isset($ranking) }}'){
            if('{{ isset($ranking->rank) }}'){
                if(coin.launched_date > '{{ date("Y-m-d H:i:s") }}') initializeClock('clockdiv', coin.launched_date);
            }
        }

        var margin = '{!! $margin !!}';
        var callInterval = 0;
        $.each(JSON.parse(margin), function(key,value){
            if(value.launched_date > '{{ date("Y-m-d H:i:s") }}') initializeClock('clockdiv_'+value.symbol, value.launched_date);
            if(value.sales_date > '{{ date("Y-m-d H:i:s") }}' && value.current_unit <= 0) initializeClock('clockdiv_'+value.symbol, value.sales_date);
            if(value.current_unit > 0) callInterval = 1;
        });

        if(callInterval){
            setInterval(function(){
                $.ajax({
                    url     :   "{{ route('capx.getAllMargin', ['lang' => \App::getLocale()]) }}",
                    method  :   "post",
                    headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
                    success : function(response){
                        $.each(response.margin, function(key,value){
                            $('#price_'+value.symbol).text('$ '+number_format(value.current_price, 4));
                            $('#unit_'+value.symbol).text(number_format(value.current_unit));

                            var progress = "";
                            var fundBalance = (value.initial_unit - value.current_unit) / value.initial_unit * 100; 
                            var bar = fundBalance<4?4:fundBalance;
                            progress = '<p style="width:'+bar+'%" data-value="'+number_format(fundBalance,2)+'">&nbsp;</p>';
                            progress += '<progress max="100" value="'+fundBalance+'" class="html5">';
                            progress += '<div class="progress-bar">';
                            progress += '<span style="width: 80%">'+fundBalance+'</span>';
                            progress += '</div>';
                            progress += '</progress>';

                            $('#progress_'+value.symbol).empty().append(progress);
                        });
                    }
                });
            }, 5*1000);
        }

    });

    function dashboardGraphRank(level, month){
        var disable = loadingDisable();
        $.ajax({
            url     :   "{{ route('member.dashboardGraphRank', ['lang' => \App::getLocale()]) }}",
            method  :   "post",
            data    :   {
                            level : level,
                            month : month
                        },
            headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
            success : function(response){
                var chart;
                var chartData = response.model;

                // AmCharts.ready(function () {
                    // SERIAL CHART
                    chart = new AmCharts.AmSerialChart();
                    chart.dataProvider = chartData;
                    chart.categoryField = "month";
                    chart.plotAreaBorderAlpha = 0.2;

                    // AXES
                    // category
                    var categoryAxis = chart.categoryAxis;
                    categoryAxis.gridAlpha = 0.1;
                    categoryAxis.axisAlpha = 0;
                    categoryAxis.gridPosition = "start";

                    // value
                    var valueAxis = new AmCharts.ValueAxis();
                    valueAxis.stackType = "regular";
                    valueAxis.gridAlpha = 0.1;
                    valueAxis.axisAlpha = 0;
                    chart.addValueAxis(valueAxis);

                    // GRAPHS
                    // first graph
                    var graph = new AmCharts.AmGraph();
                    graph.title = "IB";
                    graph.labelText = "[[value]]";
                    graph.valueField = "IB";
                    graph.type = "column";
                    graph.lineAlpha = 0;
                    graph.fillAlphas = 1;
                    graph.lineColor = "#2F61D5";
                    graph.balloonText = "<span style='color:#555555;'>[[category]]</span><br><span style='font-size:14px'>[[title]]:<b>[[value]]</b></span>";
                    chart.addGraph(graph);

                    // second graph
                    graph = new AmCharts.AmGraph();
                    graph.title = "MIB";
                    graph.labelText = "[[value]]";
                    graph.valueField = "MIB";
                    graph.type = "column";
                    graph.lineAlpha = 0;
                    graph.fillAlphas = 1;
                    graph.lineColor = "#FF7052";
                    graph.balloonText = "<span style='color:#555555;'>[[category]]</span><br><span style='font-size:14px'>[[title]]:<b>[[value]]</b></span>";
                    chart.addGraph(graph);

                    // third graph
                    graph = new AmCharts.AmGraph();
                    graph.title = "PIB";
                    graph.labelText = "[[value]]";
                    graph.valueField = "PIB";
                    graph.type = "column";
                    graph.lineAlpha = 0;
                    graph.fillAlphas = 1;
                    graph.lineColor = "#3FB6DC";
                    graph.balloonText = "<span style='color:#555555;'>[[category]]</span><br><span style='font-size:14px'>[[title]]:<b>[[value]]</b></span>";
                    chart.addGraph(graph);

                    // LEGEND
                    var legend = new AmCharts.AmLegend();
                    legend.borderAlpha = 0;
                    legend.horizontalGap = 10;
                    legend.markerType = "circle";
                    legend.align = "center";
                    chart.addLegend(legend);

                    chart.depth3D = 25;
                    chart.angle = 30;

                    // WRITE
                    chart.write("rankingChart");
                // });
                disable.out();
            },
            error: function (response) {
                if(response.readyState == 4){
                    notiAlert(2, "{{ \Lang::get('error.sessionExpired') }}", '{{\App::getLocale()}}');
                    window.location.reload();
                }
            }
        });
    }

    function dashboardGraphMember(level, month){
        var disable = loadingDisable();
        $.ajax({
            url     :   "{{ route('member.dashboardGraphMember', ['lang' => \App::getLocale()]) }}",
            method  :   "post",
            data    :   {
                            level : level,
                            month : month
                        },
            headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
            success : function(response){
                var chart;
                var chartData = response.model;

                // AmCharts.ready(function () {
                    // SERIAL CHART
                    chart = new AmCharts.AmSerialChart();
                    chart.dataProvider = chartData;
                    chart.categoryField = "month";
                    chart.plotAreaBorderAlpha = 0.2;

                    // AXES
                    // category
                    var categoryAxis = chart.categoryAxis;
                    categoryAxis.gridAlpha = 0.1;
                    categoryAxis.axisAlpha = 0;
                    categoryAxis.gridPosition = "start";

                    // value
                    var valueAxis = new AmCharts.ValueAxis();
                    valueAxis.stackType = "regular";
                    valueAxis.gridAlpha = 0.1;
                    valueAxis.axisAlpha = 0;
                    chart.addValueAxis(valueAxis);

                    // GRAPHS
                    // first graph
                    var graph = new AmCharts.AmGraph();
                    graph.title = "{{\Lang::get('home.gotInvest')}}";
                    graph.labelText = "[[value]]";
                    graph.valueField = "totalinvest";
                    graph.type = "column";
                    graph.lineAlpha = 0;
                    graph.fillAlphas = 1;
                    graph.lineColor = "#2F61D5";
                    graph.balloonText = "<span style='color:#555555;'>[[category]]</span><br><span style='font-size:14px'>[[title]]:<b>[[value]]</b></span>";
                    chart.addGraph(graph);

                    // second graph
                    graph = new AmCharts.AmGraph();
                    graph.title = "{{\Lang::get('home.noInvest')}}";
                    graph.labelText = "[[value]]";
                    graph.valueField = "totalnoinvest";
                    graph.type = "column";
                    graph.lineAlpha = 0;
                    graph.fillAlphas = 1;
                    graph.lineColor = "#FF7052";
                    graph.balloonText = "<span style='color:#555555;'>[[category]]</span><br><span style='font-size:14px'>[[title]]:<b>[[value]]</b></span>";
                    chart.addGraph(graph);

                    // LEGEND
                    var legend = new AmCharts.AmLegend();
                    legend.borderAlpha = 0;
                    legend.horizontalGap = 10;
                    legend.markerType = "circle";
                    legend.align = "center";
                    chart.addLegend(legend);

                    chart.depth3D = 25;
                    chart.angle = 30;

                    // WRITE
                    chart.write("memberChart");
                // });
                disable.out();
            },
            error: function (response) {
                if(response.readyState == 4){
                    notiAlert(2, "{{ \Lang::get('error.sessionExpired') }}", '{{\App::getLocale()}}');
                    window.location.reload();
                }
            }
        });
    }

    function dashboardGraphInvest(level, month){
        var disable = loadingDisable();
        $.ajax({
            url     :   "{{ route('member.dashboardGraphInvest', ['lang' => \App::getLocale()]) }}",
            method  :   "post",
            data    :   {
                            level : level,
                            month : month
                        },
            headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
            success : function(response){
                var chart;
                var chartData = response.model;

                // AmCharts.ready(function () {
                    // SERIAL CHART
                    chart = new AmCharts.AmSerialChart();
                    chart.dataProvider = chartData;
                    chart.categoryField = "month";
                    chart.synchronizeGrid = true; // this makes all axes grid to be at the same intervals

                    // AXES
                    // category
                    var categoryAxis = chart.categoryAxis;
                    categoryAxis.gridAlpha = 0.1;
                    categoryAxis.axisAlpha = 0;
                    categoryAxis.gridPosition = "start";

                    // first value axis (on the left)
                    var valueAxis = new AmCharts.ValueAxis();
                    // valueAxis.stackType = "regular";
                    valueAxis.gridAlpha = 0.1;
                    valueAxis.axisAlpha = 0;
                    chart.addValueAxis(valueAxis);

                    // GRAPHS
                    var graph = new AmCharts.AmGraph();
                    graph.valueAxis = valueAxis; // we have to indicate which value axis should be used
                    graph.title = "{{\Lang::get('misc.ginvest')}}";
                    graph.valueField = "totalinvest";
                    graph.bullet = "round";
                    graph.hideBulletsCount = 30;
                    graph.bulletBorderThickness = 2;
                    graph.lineColor = "#7540EE";
                    chart.addGraph(graph);

                    // CURSOR
                    var chartCursor = new AmCharts.ChartCursor();
                    chartCursor.cursorAlpha = 0.1;
                    chartCursor.fullWidth = true;
                    chartCursor.valueLineBalloonEnabled = false;
                    chartCursor.valueLineEnabled = false;
                    chartCursor.cursorColor = "#25265e";
                    chart.addChartCursor(chartCursor);

                    // LEGEND
                    var legend = new AmCharts.AmLegend();
                    legend.marginLeft = 110;
                    legend.useGraphSettings = true;
                    legend.align = "center";
                    chart.addLegend(legend);

                    // WRITE
                    chart.write("investChart");
                // });
                disable.out();
            },
            error: function (response) {
                if(response.readyState == 4){
                    notiAlert(2, "{{ \Lang::get('error.sessionExpired') }}", '{{\App::getLocale()}}');
                    window.location.reload();
                }
            }
        });
    }

    function dashboardGraphSales(level, month){
        var disable = loadingDisable();
        $.ajax({
            url     :   "{{ route('member.dashboardGraphSales', ['lang' => \App::getLocale()]) }}",
            method  :   "post",
            data    :   {
                            level : level,
                            month : month
                        },
            headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
            success : function(response){
                var chart;
                var chartData = response.model;

                // AmCharts.ready(function () {
                    // SERIAL CHART
                    chart = new AmCharts.AmSerialChart();
                    chart.dataProvider = chartData;
                    chart.categoryField = "month";
                    chart.synchronizeGrid = true; // this makes all axes grid to be at the same intervals

                    // AXES
                    // category
                    var categoryAxis = chart.categoryAxis;
                    categoryAxis.gridAlpha = 0.1;
                    categoryAxis.axisAlpha = 0;
                    categoryAxis.gridPosition = "start";

                    // first value axis (on the left)
                    var valueAxis = new AmCharts.ValueAxis();
                    // valueAxis.stackType = "regular";
                    valueAxis.gridAlpha = 0.1;
                    valueAxis.axisAlpha = 0;
                    chart.addValueAxis(valueAxis);

                    // GRAPHS
                    var graph = new AmCharts.AmGraph();
                    graph.valueAxis = valueAxis; // we have to indicate which value axis should be used
                    graph.title = "{{\Lang::get('misc.gsales')}}";
                    graph.valueField = "totalsales";
                    graph.bullet = "round";
                    graph.hideBulletsCount = 30;
                    graph.bulletBorderThickness = 2;
                    graph.lineColor = "#7540EE";
                    chart.addGraph(graph);

                    // CURSOR
                    var chartCursor = new AmCharts.ChartCursor();
                    chartCursor.cursorAlpha = 0.1;
                    chartCursor.fullWidth = true;
                    chartCursor.valueLineBalloonEnabled = false;
                    chartCursor.valueLineEnabled = false;
                    chartCursor.cursorColor = "#25265e";
                    chart.addChartCursor(chartCursor);

                    // LEGEND
                    var legend = new AmCharts.AmLegend();
                    legend.marginLeft = 110;
                    legend.useGraphSettings = true;
                    legend.align = "center";
                    chart.addLegend(legend);

                    // WRITE
                    chart.write("salesChart");
                // });
                disable.out();
            },
            error: function (response) {
                if(response.readyState == 4){
                    notiAlert(2, "{{ \Lang::get('error.sessionExpired') }}", '{{\App::getLocale()}}');
                    window.location.reload();
                }
            }
        });
    }

    function subscribeCapForm() {
        var modelContent = '';

        modelContent = '<div style="font-size: 20px; padding: 15px;">@lang("capx.subscribeCoin"): @lang("capx.trcap")<br/><br/>';
        modelContent += '<div class="input-group input-group-prepend col-lg-10" style="padding-left:0px;">';
        modelContent += '<span class="input-group-text border-right-0 rounded-left" style="color:#000;width:200px;" id="pay_title">@lang("common.cashTitle")</span>';
        modelContent += '<input class="form-control" type="text" value="$ {{ $cashBalance }}" disabled="" id="displaywallet"></div>';
        modelContent += '<div class="input-group input-group-prepend col-lg-10" style="padding-left:0px;">';
        modelContent += '<span class="input-group-text border-right-0 rounded-left" style="color:#000;width:200px;">@lang("capx.rank")</span>';
        modelContent += '<input class="form-control" type="text" value="{{ $rank }}" disabled=""></div>';
        modelContent += '<div class="input-group input-group-prepend col-lg-10" style="padding-left:0px;">';
        modelContent += '<span class="input-group-text border-right-0 rounded-left" style="color:#000;width:200px;">@lang("capx.subscribeBalance")</span>';
        modelContent += '<input id="maxSubscribe" class="form-control" type="text" value="{{ $maxSubscribe }}" disabled=""></div>';
        modelContent += '<div class="input-group input-group-prepend col-lg-10" style="padding-left:0px;">';
        modelContent += '<span class="input-group-text border-right-0 rounded-left" style="color:#000;width:200px;">@lang("capx.subscribeAmount")</span>';
        modelContent += '<input id="amount" class="form-control" type="text" value="" onkeypress="return isAmountKey(this, event);"><span style="padding-top:5px;">&nbsp; x 500 = </span>&nbsp;<span id="multipleamount" style="padding-top:5px;">0</span></div>';
        modelContent += '<br/><button type="button" class="btn btn-primary glow_button" data-dismiss="modal" onclick="submitCapSubscribe()">@lang("common.confirm")</button>';
        modelContent += '</div>';
        $('#modalContent').empty().append(modelContent);
        $('#modalZoomIn').click();

        $('#amount').on('keyup', function(){
            if($('#amount').val()){
                var value = $('#amount').val()*500;
                $('#multipleamount').text(number_format(value));
            }else{
                $('#multipleamount').text(0);
            }
        });
    }

    function submitCapSubscribe() {
        var amount = $('#amount').val();
        var url = "{{ route('capx.subscribetrc', ['lang' => \App::getLocale()]) }}";
        var buttonDisplay = '{"Format":['+'{"confirmButtonText":'+'"@lang('common.confirm')"'+',"cancelButtonText":'+'"@lang('common.cancel')"'+',"setTittle":'+'"@lang('settings.secret')"'+',"error":'+'"@lang('error.securityPasswordError')"'+'}]}';
        var obj = {amount: amount};
        var final = { Data : obj};
        var data = JSON.stringify(final);
        swalAlert(url,buttonDisplay,data);
    }

    function subscribeForm(productName, symbol) {
        var modelContent = '';

        modelContent = '<div style="font-size: 20px; padding: 15px;">@lang("capx.subscribeCoin"): '+productName+'<br/><br/>';
        modelContent += '<div class="input-group input-group-prepend col-lg-10" style="padding-left:0px;">';
        modelContent += '<span class="input-group-text border-right-0 rounded-left" style="color:#000;width:150px;" id="pay_title">@lang("common.cashTitle")</span>';
        modelContent += '<input class="form-control" type="text" value="$ {{ $cashBalance }}" disabled="" id="displaywallet"></div>';
        modelContent += '<div class="input-group input-group-prepend col-lg-10" style="padding-left:0px;">';
        modelContent += '<span class="input-group-text border-right-0 rounded-left" style="color:#000;width:150px;">@lang("capx.subscribeAmount")</span>';
        modelContent += '<input id="amount" class="form-control" type="text" value="" ><span style="padding-top:5px;">&nbsp; x 500 = </span>&nbsp;<span id="multipleamount" style="padding-top:5px;">0</span></div>';
        modelContent += '<br/><button type="button" class="btn btn-primary glow_button" data-dismiss="modal" onclick="submitSubscribe(\''+symbol+'\')">@lang("common.confirm")</button>';
        modelContent += '</div>';
        $('#modalContent').empty().append(modelContent);
        $('#modalZoomIn').click();

        $('#amount').on('keyup', function(){
            if($('#amount').val()){
                var value = $('#amount').val()*500;
                $('#multipleamount').text(number_format(value));
            }else{
                $('#multipleamount').text(0);
            }
        });
    }

    function submitSubscribe(symbol) {
        var amount = $('#amount').val();
        var url = "{{ route('capx.subscribeMargin', ['lang' => \App::getLocale()]) }}";
        var buttonDisplay = '{"Format":['+'{"confirmButtonText":'+'"@lang('common.confirm')"'+',"cancelButtonText":'+'"@lang('common.cancel')"'+',"setTittle":'+'"@lang('settings.secret')"'+',"error":'+'"@lang('error.securityPasswordError')"'+'}]}';
        var obj = {symbol: symbol, amount: amount };
        var final = { Data : obj};
        var data = JSON.stringify(final);
        swalAlert(url,buttonDisplay,data);
    }

    function getTimeRemaining(endtime) {
        var b = endtime.split(/\D+/);
        var newEndTime = b[1]+"/"+b[2]+"/"+b[0]+" "+b[3]+":"+b[4]+":"+b[5];

        var t = Date.parse(newEndTime) - Date.parse(new Date());
        var seconds = Math.floor((t / 1000) % 60);
        var minutes = Math.floor((t / 1000 / 60) % 60);
        var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        var days = Math.floor(t / (1000 * 60 * 60 * 24));
        
        if(days == 0 && hours == 0 && minutes == 0 && seconds == 0) window.location.reload();
        
        return {
                'total': t,
                'days': days,
                'hours': hours,
                'minutes': minutes,
                'seconds': seconds
                };
    }

    function initializeClock(id, endtime) {
        var clock = document.getElementById(id);
        var daysSpan = clock.querySelector('.days');
        var hoursSpan = clock.querySelector('.hours');
        var minutesSpan = clock.querySelector('.minutes');
        var secondsSpan = clock.querySelector('.seconds');

        function updateClock() {
            var t = getTimeRemaining(endtime);

            daysSpan.innerHTML = t.days;
            hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
            minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
            secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

            if (t.total <= 0) {
                clearInterval(timeinterval);
            }
        }

        updateClock();
        var timeinterval = setInterval(updateClock, 1000);
    }
</script>
@stop