@extends('front.app')

@section('title')
@lang('announcement.title') | {{ config('app.name') }}
@stop

@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/datatables/css/colReorder.bootstrap.min.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/animate/css/animate.min.css')}}" />
@stop

@section('main_content')
@include('front.include.header')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-lg-5">
                   <h4 class="nav_top_align"><i class="fa fa-th"></i> @lang('announcement.title')</h4>
               </div>
               <div class="col-lg-7">
                   <ul class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class=" breadcrumb-item">
                           <a href="{{ route('home', ['lang' => \App::getLocale()]) }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i> @lang('breadcrumbs.dashboard')
                           </a>
                       </li>
                       <li class="breadcrumb-item active">@lang('announcement.title')</li>
                   </ul>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            <i class="fa fa-table"></i> @lang('announcement.title')
                        </div>
                        <div class="card-body p-t-10">
                            <div class=" m-t-25">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTable no-footer" style="text-align: center; width: 100%;" id="announcementList" role="grid">
                                        <thead>
                                        <tr>
                                            <th>@lang('announcement.listDate')</th>
                                            <th>@lang('announcement.listTitle')</th>
                                            <th>@lang('announcement.listAction')</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('front.include.modal')
@stop

@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/js/pages/modals.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.colReorder.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script type="text/javascript">
    var tableId = "announcementList";
    var dataUrl = "{{ route('announcement.getList', ['lang' => \App::getLocale()]) }}";
    var columnIds = [
                    { "data": "created_at" },
                    { "data": "title_{{ $lang }}" },
                    { "data": "action" },
                    ];
    var disableSearchIndex = [2];
    $(document).ready( function () {
        callDatatables(tableId, dataUrl, columnIds, disableSearchIndex);
    });

    function getAnnouncementDetails(parentId) {
        var disable = loadingDisable();
        var modelContent = '';
        var ajaxUrl = "{{ route('announcement.read', ['id' =>'parentId', 'lang' => \App::getLocale()]) }}";
            ajaxUrl = ajaxUrl.replace('parentId', parentId);

        $.ajax({
            url     :   ajaxUrl,
            method  :   "get",
            headers :   {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
            success : function(response){
                if(response.status == 0){
                    if("{{ $lang }}" == "en"){
                        modelContent =  '<div style="font-size: 28px !important;">'+response.model.title_en+'</div>';
                        modelContent += '<div style="font-size: 15px !important;font-weight: 100;">'+response.model.created_at+'</div>';
                        modelContent += '<hr><br/>';
                        modelContent += '<div style="font-size: 15px !important;">'+response.model.content_en+'</div>';
                    }else{
                        modelContent =  '<div style="font-size: 28px !important;">'+response.model.title_chs+'</div>';
                        modelContent += '<div style="font-size: 15px !important;font-weight: 100;">'+response.model.created_at+'</div>';
                        modelContent += '<hr><br/>';
                        modelContent += '<div style="font-size: 15px !important;">'+response.model.content_chs+'</div>';
                    }
                    $('#modalContent').empty().append(modelContent);
                    $('#modalZoomIn').click();
                    $('#'+parentId).css('display', 'none');

                    if(response.newInsert){
                        var check = $('#unreadAnnouncement').text() - 1;
                        if(check > 0){
                            $('.unreadAnnouncement').text(check);
                        }else{
                            $('.unreadAnnouncement').css('display', 'none');
                        }
                    }
                }else{
                    notiAlert(response.status, response.msg, '{{\App::getLocale()}}');
                }
                disable.out();
            }
        });
    }
</script>
@stop