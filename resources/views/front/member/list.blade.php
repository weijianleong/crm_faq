<?php
    
    use App\Repositories\SharesRepository;
    use App\Repositories\MemberRepository;
    use Carbon\Carbon;
    use Illuminate\Support\Str;
    $sharesRepo = new SharesRepository;
    $memberRepo = new MemberRepository;
    $announcementModel = new \App\Models\Announcement;
    
    $member = $memberRepo->findByUsername(trim($user->username));
    $member->load('wallet');
    $member->load('detail');
    //print_r($member->id);
    $memberlist = DB::table('Member')->join('users', 'Member.username', '=', 'users.username')->whereNotNull('declare_form')->where('declare_form', '<>','Y')->get();
   
    $destinationPath = config('misc.declarePath');
    ?>
@extends('front.app')

@section('title')
@lang('misc.mtitle') | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
  <li class="active">@lang('breadcrumbs.mlist')</li>
</ul>
@stop

@section('content')
<main>
  @include('front.include.sidebar')
  <div class="main-container">
    @include('front.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section class="tables-data">
        <div class="page-header">
          <h1><i class="md md-account-balance"></i> @lang('misc.mtitle')</h1>

        </div>

        <div class="row m-b-10">
          <div class="col-md-12">
            <div class="card">

              <div class="card-content">
                <div>
                  <table cellspacing="0" width="100%" >
                    <thead>
                      <tr>
                    
                      <th>Name</th>
                      <th>Username</th>
                      <th>Declaration form</th>
                      <th>Action</th>
                      </tr>
                    @foreach ($memberlist as $memberx)
                      <tr>

                    <td>{{ $memberx->first_name }}</td>
                      <td>{{ $memberx->username }}</td>
                     <td><a href="{{ $destinationPath.$memberx->declare_form }}" target="blank">@lang('settings.onclick')</a></td>
                      <td>
                    <button class="btn btn-warning btn-flat-border btn-update" data-url="{{ route('member.list.update', ['id' => $memberx->id]) }}" type="submit">
                      <i class="md md-mode-edit"></i> OK
                    </button>
                  </td>
                      </tr>
                    @endforeach
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>



      </section>
    </div>
  </div>
</main>
@stop
