<?php
  use App\Models\Package;
  use App\Repositories\MemberRepository;
  $packages = Package::where('status', '!=', 0)->get();

    $memberRepo = new MemberRepository;
    $member = $memberRepo->findByUsername(trim($user->username));
    $member->load('wallet');
    $member->load('detail');
    //$member->load('shares');
    $member->load('package');
?>

@extends('front.app')

@section('title')
  @lang('register.title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">@lang('breadcrumbs.front')</a></li>
    <li><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
    <li class="active">@lang('breadcrumbs.register')</li>
  </ul>
@stop

@section('content')
  <main>
    @include('front.include.sidebar')
    <div class="main-container">
      @include('front.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section>
          <div class="page-header">
            <h1><i class="md md-person-add"></i> @lang('register.title')</h1>
            <p class="lead">@lang('register.notice')</p>
          </div>

            <div class="col-md-12">
                <form data-parsley-validate="" role="form" class="action-form" id="registerForm" http-type="post" data-url="{{ route('member.postRegister', ['lang' => \App::getLocale()]) }}" data-nationality="true">
             <div class="col-md-6">
              <div class="well white">
                <p class="text-uppercase theme-text">@lang('register.sectiontitle1')</p>
                

                    <fieldset>

                    <div class="form-group">
                      <label class="control-label" for="inputDirect">@lang('register.direct')<sup>*</sup></label>
                      <input type="text" class="form-control" required="" name="direct_id" id="inputDirect">
                      <span class="help-block">
                        <div class="btn-group">
                          <button type="button" class="btn btn-success btn-show" data-toggle="modal" data-target="#showModal" data-url="{{ route('member.showModal', ['lang' => \App::getLocale()]) }}">
                            <span class="md md-help"></span> @lang('register.checkID')
                          </button>
                        </div>
                      </span>
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="inputPackage">@lang('register.package')</label>
                      <div class="input-group">
                        <select class="form-control" name="package_id" id="inputPackage" required="">
                        @if (count($packages) > 0)
                          @foreach ($packages as $package)
                            <option value="{{ $package->id }}" data-value="{{ $package->package_amount }}">
                              {{ number_format($package->package_amount, 0) }}
                            </option>
                          @endforeach
                        @endif
                        </select>
                        <span class="input-group-addon">USD</span>
                        <span class="help-block">
                        <div class="btn-group">
                            <button type="button" class="btn btn-success btn-show" data-toggle="modal" data-target="#favoritesModal" data-url="{{ route('member.register', ['lang' => \App::getLocale()]) }}">
                                <span class="md md-help"></span> @lang('register.package')
                            </button>
                        </div>
                        </span>
                      </div>
                    </div>


              </div>
            </div>

            <div class="col-md-6">
             <div class="well white">
                <p class="text-uppercase theme-text">@lang('register.sectiontitle2')</p>
                    <div class="form-group">
                      <label class="control-label">@lang('register.name')<sup>*</sup></label>
                      <input type="text" name="name" class="form-control" required="">
                    </div>
                    <div class="form-group">
                      <label class="control-label">@lang('register.username')<sup>*</sup></label>
                      <input type="text" name="username" class="form-control" required="">
                    </div>
                    <div class="form-group">
                      <label class="control-label" for="inputPassword">@lang('register.password')<sup>*</sup></label>
                      <input type="password" id="inputPassword" name="password" class="form-control" required="" minlength="5">
                    </div>

                    <div class="form-group">
                      <label class="control-label">@lang('register.repassword')<sup>*</sup></label>
                      <input type="password" data-parsley-equalto="#inputPassword" class="form-control" required="" minlength="5">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="inputSPassword">@lang('register.spassword')<sup>*</sup></label>
                      <input type="password" id="inputSPassword" name="secret" class="form-control" required="" minlength="5">
                    </div>

                    <div class="form-group">
                      <label class="control-label">@lang('register.respassword')<sup>*</sup></label>
                      <input type="password" data-parsley-equalto="#inputSPassword" class="form-control" required="" minlength="5">
                    </div>

             </div>
            </div>

          <div class="col-md-12">
            <div class="well white">
                <p class="text-uppercase theme-text">@lang('register.sectiontitle3')</p>
                <div class="form-group">
                    <label class="control-label">@lang('register.email')<sup>*</sup></label>
                    <input type="email" name="email" class="form-control" required="">
                </div>
                    <?php $countries = config('misc.countries');  ?>
                    <div class="form-group">
                      <label class="control-label" for="inputNationality">@lang('register.nationality')</label>
                      <select class="form-control dd-icon" name="nationality" id="inputNationality">
                        @foreach ($countries as $country => $value)
                          <option value="{{ $country }}" data-imagesrc="{{ asset('assets/img/flags/' . $country . '.png') }}" data-description="{{ \Lang::get('country.selected') }}">@lang('country.' . $country)</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group">
                      <label class="control-label" for="inputMobile">@lang('register.phone1')<sup>*</sup></label>
                      <input type="text" class="form-control" required="" name="phone1" id="inputphone1">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="inputS">@lang('register.security')<sup>*</sup></label>
                      <input type="password" class="form-control" required="" name="s" id="inputS">
                    </div>

                    <div class="form-group">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" required="" name="terms"> @lang('register.agree')
                        </label>
                      </div>
                    </div>

                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">
                        <span class="btn-preloader">
                          <i class="md md-cached md-spin"></i>
                        </span>
                        <span>@lang('common.submit')</span>
                      </button>
                      <button type="reset" class="btn btn-default">@lang('common.cancel')</button>
                    </div>

            </div>
        </div>

          </div>
            </form>
         </div>
        </section>
      </div>
    </div>
  </main>

  <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="showModalLabel">
            <span class="md md-accessibility"></span> @lang('register.modal.title')
          </h4>
        </div>
        <div class="modal-body">
          <div class="loading text-center">
            <img src="{{ asset('assets/img/loading.gif') }}" alt="Network Loading">
            <br>
            <small class="text-primary">@lang('common.modal.load')</small>
          </div>

          <div class="error text-center">
            <i class="md md-error"></i>
            <br>
            <small class="text-danger">@lang('common.modal.error')</small>
          </div>

          <div id="modalContent"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-raised" data-dismiss="modal">@lang('common.close')</button>
        </div>
      </div>
    </div>
  </div>

<div class="modal fade" id="favoritesModal"
tabindex="-1" role="dialog"
aria-labelledby="favoritesModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close"
data-dismiss="modal"
aria-label="Close">
<span aria-hidden="true">&times;</span></button>
<h4 class="modal-title"
id="favoritesModalLabel"> <span class="md md-wallet-giftcard"></span> @lang('register.package')</h4>
</div>
<div class="modal-body">
<div class="table-responsive">
<table class="table table-full table-full-small table-dashboard-widget-1">
<thead>
<tr>
<th>@lang('register.package')</th>
<th>@lang('register.directSponsor')</th>
<th>@lang('register.groupBonus')</th>
<th>@lang('register.roi')</th>
</tr>
</thead>
<tbody>
@foreach ($packages as $index => $package)
<tr>
<td>{{ $package['title'] }}</td>
<td>{{ $package['direct_percent'].'%' }}</td>
@if ($package['group_level'] != 0)
<td>{{ $package['group_level'].' x '.$package['group_percent'].'%' }}</td>
@else
<td> - </td>
@endif
<td>{{ $package['month_percent'].'%' }}</td>
</tr>
@endforeach
</tbody>
</table>
</div>
</div>
<div class="modal-footer">
<button type="button"
class="btn btn-default"
data-dismiss="modal">Close</button>

</div>
</div>
</div>
</div>

@stop
