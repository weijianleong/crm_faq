@if (!$model)
  <div class="alert alert-danger">
    @lang('register.modal.notFound')
  </div>
@else
  @if($model->process_status != 'Y' || $checkBan->is_ban ==0)
    <div class="alert alert-danger">
      @lang('error.transferFail')
    </div>    
  @else
    <div class="table-responsive">
      <table class="table table-hover table-striped">
        <tr>
          <td class="theme-text">@lang('register.modal.username')</td>
          <td>:</td>
          <td>{{ $model->username }}</td>
        </tr>

        <tr>
          <td class="theme-text">@lang('register.modal.name')</td>
          <td>:</td>
          <td>{{ $model->user->first_name }}</td>
        </tr>

      </table>
    </div>
  @endif
@endif
