function loadingDisable() {
    var loading = new Loading({
                                direction: 'hor',
                                defaultApply:   true,
                            });
    return loading;
}

function isAmountKey(obj, evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;

    var allowCharCode = [8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57];

    if (!allowCharCode.includes(charCode))
        return false;

    var currentAmount = document.getElementById(obj.id).value;

    return true;
}

function validateFloatKeyPress(el, evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    var number = el.value.split('.');
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    //just one dot 
    if(number.length>1 && charCode == 46){
         return false;
    }
    //get the carat position
    var caratPos = getSelectionStart(el);
    var dotPos = el.value.indexOf(".");
    if( caratPos > dotPos && dotPos>-1 && (number[1].length > 1)){
        return false;
    }
    return true;
}

function getSelectionStart(o) {
    if (o.createTextRange) {
        var r = document.selection.createRange().duplicate()
        r.moveEnd('character', o.value.length)
        if (r.text == '') return o.value.length
        return o.value.lastIndexOf(r.text)
    } else return o.selectionStart
}

function notiAlert(status, message, lang){
    var alertType = "";
    var alertTitle = "";
    var successTitle = "";
    var errorTitle = "";
    var warningTitle = "";
    if(lang == 'en'){
        successTitle = "Success";
        errorTitle = "Error";
        warningTitle = "Attention";
    }else{
        successTitle = "成功";
        errorTitle = "错误";
        warningTitle = "注意";
    }
    if(status == 0){
        alertType = "success";
        alertTitle = successTitle;
    }
    else if(status == 1){
        alertType = "error";
        alertTitle = errorTitle;
    }
    else if(status == 2){
        alertType = "warning";
        alertTitle = warningTitle;
    }
    var i = -1;
    var toastCount = 0;
    var $toastlast;

    var shortCutFunction = alertType;
    var msg = message;
    var title = "<h3 class='text-white'>"+alertTitle+"</h3>";
    var $showDuration = 1000;
    var $hideDuration = 1000;
    var $timeOut = 5000;
    var $extendedTimeOut = 1000;
    var $showEasing = "swing";
    var $hideEasing = "linear";
    var $showMethod = "fadeIn";
    var $hideMethod = "fadeOut";
    var toastIndex = toastCount++;
    toastr.options = {
        closeButton: $('#closeButton').prop('checked'),
        debug: $('#debugInfo').prop('checked'),
        positionClass: 'toast-center',
        onclick: null
    };
    if ($('#addBehaviorOnToastClick').prop('checked')) {
        toastr.options.onclick = function() {
            alert('You can perform some custom action after a toast goes away');
        };
    }
    if ($showDuration.length) {
        toastr.options.showDuration = $showDuration;
    }
    if ($hideDuration.length) {
        toastr.options.hideDuration = $hideDuration;
    }
    if ($timeOut.length) {
        toastr.options.timeOut = $timeOut;
    }
    if ($extendedTimeOut.length) {
        toastr.options.extendedTimeOut = $extendedTimeOut;
    }
    if ($showEasing.length) {
        toastr.options.showEasing = $showEasing;
    }
    if ($hideEasing.length) {
        toastr.options.hideEasing = $hideEasing;
    }
    if ($showMethod.length) {
        toastr.options.showMethod = $showMethod;
    }
    if ($hideMethod.length) {
        toastr.options.hideMethod = $hideMethod;
    }
    $("#toastrOptions").text("Command: toastr[" + shortCutFunction + "](\"" + msg + (title ? "\", \"" + title : '') + "\")\n\ntoastr.options = " + JSON.stringify(toastr.options, null, 2));
    var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
    $toastlast = $toast;
}

function swalAlert(url,buttonDisplay,data){

    obj = JSON.parse(buttonDisplay);
    // alert(data);
    swal({
        title: obj.Format[0].setTittle,
        input: 'password',
        showCancelButton: true,
        confirmButtonText: obj.Format[0].confirmButtonText,
        confirmButtonColor: '#00c0ef',
        cancelButtonText: obj.Format[0].cancelButtonText,
        cancelButtonColor: '#ff8080',
        width: 600,
        showLoaderOnConfirm: true,
        preConfirm: function (password) {
            return new Promise(function (resolve, reject) {
                $.ajax({
                        url : url,
                        data: {
                                SPassword  : password,
                                Data : data,
                        },
                        type: 'POST',
                        headers:
                        {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success : function(response){
                            if (response.SPassword == '1') {
                                var error = obj.Format[0].error;
                                reject(error);
                            } else {
                                resolve(response);
                            
                            }
                        }
                });
            });
        },
        allowOutsideClick: false
    }).then(function (response) {

        if (response.result == '0') {
  
            swal({
                type: 'success',
                title: response.showTittle,
                text: response.showText,
                allowOutsideClick: false
            }).then(function() {
                if(response.returnUrl) document.location.href=response.returnUrl;
            });
        }else if(response.result == '1') {

            swal({
                type: 'error',
                title: response.showTittle,
                text: response.showText,
            });
        
        }else{
            swal({
                type: 'info',
                title: response.showTittle,
                text: response.showText,
                html: response.showHtml,
            });
        }
    }).done();
}

function checkValue(data){

    obj = JSON.parse(data);
    var count = 0;

    $.each(obj.Id, function(index, item) {
        var GetId = ('#'+item);

        if($(GetId).val().trim() ==""){
            $(GetId).focus();
            $(GetId).addClass( "form-control form-control-danger is-invalid" );
            count++;
        }else{
            $(GetId).removeClass( "form-control-danger is-invalid" );
            
        }

    });
    
    return count;
    
}

function ValidateEmail(data) 
{
    obj = JSON.parse(data);
    var count = 0;
    $.each(obj.Id, function(index, item) {
        var GetId = ('#'+item);
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($(GetId).val())){}
        else{
            count++;
            $(GetId).focus();
        }

    });
    return count;
}

function format2(n, currency) {
  return currency + Number(n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
}

function number_format(n, c, commas, d, t) {
    commas = commas ? commas : true;

    n = parseFloat(n);
    c = c ? c : 0;
    c = isNaN(c = Math.abs(c)) ? 2 : c;
    d = d == undefined ? "." : d;
    t = t == undefined ? "," : t;
    s = n < 0 ? "-" : "";
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c)));

    if(commas == true) {
      j = (j = i.length) > 3 ? j % 3 : 0;
    }
    else {
      j = 0;
    }

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

function CheckPassword($value){

    var passw =  /^(?=.*[A-Za-z])(?=.*[0-9])(?!.*[^a-zA-Z0-9]).{8,30}$/;
    if($value.match(passw)) 
    { 
        return true;
    }
    else
    {  
        return false;
    }
}