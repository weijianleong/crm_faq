function number_format(n, c, commas, d, t) {
    commas = commas ? commas : true;

    n = parseFloat(n);
    c = c ? c : 0;
    c = isNaN(c = Math.abs(c)) ? 2 : c;
    d = d == undefined ? "." : d;
    t = t == undefined ? "," : t;
    s = n < 0 ? "-" : "";
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c)));

    if(commas == true) {
      j = (j = i.length) > 3 ? j % 3 : 0;
    }
    else {
      j = 0;
    }

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}