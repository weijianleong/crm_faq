<?php
return [
    'bypassTWallet' => ['seanetlead@gmail.com'],
    'bypassNetwork' => ['seanetlead@gmail.com'],


    'menus'=>[
                '1'=>[
                            'route'         => 'home',
                            'displayName'   => 'breadcrumbs.dashboard',
                            'icon'          => 'fa fa-home',
                            'right'         => '1',
                        ],

                '2'=>[
                            'route'         => 'event.promotion',
                            'displayName'   => 'sidebar.event',
                            'icon'          => 'fa fa-snowflake-o',
                            'right'         => '2',
                        ],

                '13'=>[
                            'route'         => '',
                            'displayName'   => 'breadcrumbs.depositWithdrawal',
                            'icon'          => 'fa fa-money',
                            'right'         => '13',
                            'child'         => [
                                                '13_1'=>[
                                                            'route'         => 'sealead.deposit',
                                                            'displayName'   => 'sealead.deposit_sea',
                                                            'right'         => '13_1',
                                                        ],
                                                '13_2'=>[
                                                            'route'         => 'sealead.withdraw',
                                                            'displayName'   => 'sealead.withdrawal_sea',
                                                            'right'         => '13_2',
                                                        ],
                                                '13_3'=>[
                                                            'route'         => 'sealead.record',
                                                            'displayName'   => 'sealead.record',
                                                            'right'         => '13_3',
                                                        ],
                                                ],
                        ],

                '3'=>[
                            'route'         => '',
                            'displayName'   => 'breadcrumbs.withdraw',
                            'icon'          => 'fa fa-money',
                            'right'         => '3',
                            'child'         => [
                                                '3_1'=>[
                                                            'route'         => 'withdrawal',
                                                            'displayName'   => 'breadcrumbs.withdrawal',
                                                            'right'         => '3_1',
                                                        ],
                                                '3_2'=>[
                                                            'route'         => 'withdraw.records',
                                                            'displayName'   => 'breadcrumbs.withdrawalRecords',
                                                            'right'         => '3_2',
                                                        ],
                                                '3_3'=>[
                                                            'route'         => 'withdraw.withdrawAsia',
                                                            'displayName'   => 'breadcrumbs.Withdrawaloutside',
                                                            'right'         => '3_3',
                                                            'check'         => ['account2Network'],
                                                        ],
                                                '3_4'=>[
                                                            'route'         => 'withdraw.Asiarecords',
                                                            'displayName'   => 'breadcrumbs.WithdrawaloutsideRecords',
                                                            'right'         => '3_4',
                                                            'check'         => ['account2Network'],
                                                        ],
                                                ],
                        ],

                '4'=>[
                            'route'         => '',
                            'displayName'   => 'capx.capx',
                            'icon'          => 'fa fa-exchange',
                            'right'         => '4',
                            'child'         => [
                                                '4_3'=>[
                                                            'route'         => 'capx.loginRedirect',
                                                            'displayName'   => 'capx.logincapx',
                                                            'right'         => '4_3',
                                                            'newTab'        => '1',
                                                        ],
                                                '4_4'=>[
                                                            'route'         => 'capx.transferToCapx',
                                                            'displayName'   => 'capx.transfer',
                                                            'right'         => '4_4',
                                                        ],
                                                '4_1'=>[
                                                            'route'         => '',
                                                            'displayName'   => 'capx.trcap',
                                                            'right'         => '4_1',
                                                            'check'         => ['ranking','purchasedTrcap'],
                                                            'child'         => [
                                                                                '4_1_1'=>[
                                                                                            'route'         => 'capx.mytrcap',
                                                                                            'displayName'   => 'capx.mytrcap',
                                                                                            'right'         => '4_1_1',
                                                                                            'check'         => ['ranking','purchasedTrcap'],
                                                                                        ],
                                                                                '4_1_2'=>[
                                                                                            'route'         => 'capx.trcap3level',
                                                                                            'displayName'   => 'capx.purchased3level',
                                                                                            'right'         => '4_1_2',
                                                                                            'check'         => ['ranking','MIBPIBRank'],
                                                                                        ],
                                                                                '4_1_3'=>[
                                                                                            'route'         => 'capx.onsalescap',
                                                                                            'displayName'   => 'capx.onsales',
                                                                                            'right'         => '4_1_3',
                                                                                            'check'         => ['ranking'],
                                                                                        ],
                                                                                ],
                                                        ],
                                                '4_2'=>[
                                                            'route'         => '',
                                                            'displayName'   => 'capx.trmargin',
                                                            'right'         => '4_2',
                                                            'child'         => [
                                                                                '4_2_1'=>[
                                                                                            'route'         => 'capx.mymargin',
                                                                                            'displayName'   => 'capx.mymargin',
                                                                                            'right'         => '4_2_1',
                                                                                        ],
                                                                                '4_2_2'=>[
                                                                                            'route'         => 'capx.margin3level',
                                                                                            'displayName'   => 'capx.purchased3level',
                                                                                            'right'         => '4_2_2',
                                                                                            'check'         => ['ranking','MIBPIBRank'],
                                                                                        ],
                                                                                '4_2_3'=>[
                                                                                            'route'         => 'capx.onsales',
                                                                                            'displayName'   => 'capx.onsales',
                                                                                            'right'         => '4_2_3',
                                                                                        ],
                                                                                ],
                                                        ],
                                                ],
                        ],

                '5'=>[
                            'route'         => '',
                            'displayName'   => 'sidebar.fundTitle',
                            'icon'          => 'fa fa-address-book-o',
                            'right'         => '5',
                            'child'         => [
                                                '5_1'=>[
                                                            'route'         => 'misc.lpoa',
                                                            'displayName'   => 'sidebar.fundLink5',
                                                            'right'         => '5_1',
                                                        ],
                                                '5_2'=>[
                                                            'route'         => 'misc.fundstatement',
                                                            'displayName'   => 'sidebar.fundLink1',
                                                            'right'         => '5_2',
                                                            'check'         => ['lpoa'],
                                                        ],
                                                '5_3'=>[
                                                            'route'         => 'mergelist',
                                                            'displayName'   => 'merge.sidebar',
                                                            'right'         => '5_3',
                                                            'check'         => ['lpoa'],
                                                        ],
                                                '5_4'=>[
                                                            'route'         => 'transaction.changepass',
                                                            'displayName'   => 'misc.cpTitle',
                                                            'right'         => '5_4',
                                                            'check'         => ['lpoa','fundchgpass'],
                                                        ],
                                                '5_5'=>[
                                                            'route'         => 'misc.fundtradestatement',
                                                            'displayName'   => 'sidebar.fundLink6',
                                                            'right'         => '5_5',
                                                            'check'         => ['lpoa'],
                                                        ],
                                                ],
                        ],

                '6'=>[
                            'route'         => '',
                            'displayName'   => 'sidebar.selftrade',
                            'icon'          => 'fa fa-line-chart',
                            'right'         => '6',
                            'child'         => [
                                                '6_1'=>[
                                                            'route'         => 'misc.mt5statementself',
                                                            'displayName'   => 'sidebar.mt5Link1',
                                                            'right'         => '6_1',
                                                        ],
                                                '6_2'=>[
                                                            'route'         => 'transaction.transfermt5self',
                                                            'displayName'   => 'sidebar.mt5Link2',
                                                            'right'         => '6_2',
                                                        ],
                                                '6_3'=>[
                                                            'route'         => 'transaction.convertaself',
                                                            'displayName'   => 'sidebar.mt5Link5',
                                                            'right'         => '6_3',
                                                        ],
                                                ],
                        ],

                '7'=>[
                            'route'         => '',
                            'displayName'   => 'sidebar.wallet',
                            'icon'          => 'fa fa-credit-card',
                            'right'         => '7',
                            'child'         => [
                                                '7_1'=>[
                                                            'route'         => '',
                                                            'displayName'   => 'common.cashTitle',
                                                            'right'         => '7_1',
                                                            'child'         => [
                                                                                '7_1_1'=>[
                                                                                            'route'         => 'misc.cashstatement',
                                                                                            'displayName'   => 'sidebar.cashLink2',
                                                                                            'right'         => '7_1_1',
                                                                                        ],
                                                                                ],
                                                        ],
                                                '7_2'=>[
                                                            'route'         => '',
                                                            'displayName'   => 'common.wTitle',
                                                            'right'         => '7_2',
                                                            'child'         => [
                                                                                '7_2_1'=>[
                                                                                            'route'         => 'transaction.convertwtoc',
                                                                                            'displayName'   => 'sidebar.convertC',
                                                                                            'right'         => '7_2_1',
                                                                                        ],
                                                                                '7_2_2'=>[
                                                                                            'route'         => 'transaction.convertwtot',
                                                                                            'displayName'   => 'sidebar.convertT',
                                                                                            'right'         => '7_2_2',
                                                                                            'check'         => ['ranking'],
                                                                                        ],
                                                                                '7_2_3'=>[
                                                                                            'route'         => 'misc.5percentstatement',
                                                                                            'displayName'   => 'sidebar.5percent',
                                                                                            'right'         => '7_2_3',
                                                                                        ],
                                                                                '7_2_4'=>[
                                                                                            'route'         => 'misc.wstatement',
                                                                                            'displayName'   => 'sidebar.cashLink2',
                                                                                            'right'         => '7_2_4',
                                                                                        ],
                                                                                ],
                                                        ],
                                                '7_3'=>[
                                                            'route'         => '',
                                                            'displayName'   => 'common.bTitle',
                                                            'right'         => '7_3',
                                                            'child'         => [
                                                                                '7_3_1'=>[
                                                                                            'route'         => 'misc.bstatement',
                                                                                            'displayName'   => 'sidebar.cashLink2',
                                                                                            'right'         => '7_3_1',
                                                                                        ],
                                                                                ],
                                                        ],
                                                '7_4'=>[
                                                            'route'         => '',
                                                            'displayName'   => 'sidebar.incomeTitle',
                                                            'right'         => '7_4',
                                                            'child'         => [
                                                                                '7_4_1'=>[
                                                                                            'route'         => 'transaction.convertitoc',
                                                                                            'displayName'   => 'sidebar.convertC',
                                                                                            'right'         => '7_4_1',
                                                                                        ],
                                                                                '7_4_2'=>[
                                                                                            'route'         => 'transaction.convertitot',
                                                                                            'displayName'   => 'sidebar.convertT',
                                                                                            'right'         => '7_4_2',
                                                                                            'check'         => ['ranking'],
                                                                                        ],
                                                                                '7_4_3'=>[
                                                                                            'route'         => 'misc.commstatement',
                                                                                            'displayName'   => 'sidebar.incomeLink3',
                                                                                            'right'         => '7_4_3',
                                                                                        ],
                                                                                '7_4_4'=>[
                                                                                            'route'         => 'misc.getcommstatementDaily',
                                                                                            'displayName'   => 'sidebar.incomeLink4',
                                                                                            'right'         => '7_4_4',
                                                                                        ],
                                                                                '7_4_5'=>[
                                                                                            'route'         => 'misc.incomestatement',
                                                                                            'displayName'   => 'sidebar.cashLink2',
                                                                                            'right'         => '7_4_5',
                                                                                        ],
                                                                                ],
                                                        ],
                                                '7_5'=>[
                                                            'route'         => '',
                                                            'displayName'   => 'sidebar.transferTitle',
                                                            'right'         => '7_5',
                                                            'check'         => ['ranking','bypassTWallet'],
                                                            'child'         => [
                                                                                '7_5_1'=>[
                                                                                            'route'         => 'transaction.convertttoc',
                                                                                            'displayName'   => 'sidebar.convertC',
                                                                                            'right'         => '7_5_1',
                                                                                            'check'         => ['ranking','bypassTWallet'],
                                                                                        ],
                                                                                '7_5_2'=>[
                                                                                            'route'         => 'transaction.transfercash',
                                                                                            'displayName'   => 'sidebar.cashLink1',
                                                                                            'right'         => '7_5_2',
                                                                                            'check'         => ['ranking','bypassTWallet'],
                                                                                        ],
                                                                                '7_5_3'=>[
                                                                                            'route'         => 'misc.transferstatement',
                                                                                            'displayName'   => 'sidebar.cashLink2',
                                                                                            'right'         => '7_5_3',
                                                                                            'check'         => ['ranking','bypassTWallet'],
                                                                                        ],
                                                                                ],
                                                        ],
                                                ],
                        ],

                '8'=>[
                            'route'         => '',
                            'displayName'   => 'sidebar.registerTitle',
                            'icon'          => 'fa fa-users',
                            'right'         => '8',
                            'child'         => [
                                                '8_1'=>[
                                                            'route'         => 'network.grouplevel',
                                                            'displayName'   => 'unilevel.title2',
                                                            'right'         => '8_1',
                                                        ],
                                                '8_2'=>[
                                                            'route'         => 'network.3level',
                                                            'displayName'   => 'unilevel.title3',
                                                            'right'         => '8_2',
                                                            'check'         => ['ranking','MIBPIBRank','bypassNetwork'],
                                                        ],
                                                '8_3'=>[
                                                            'route'         => 'network.unilevel',
                                                            'displayName'   => 'sidebar.networkTitle',
                                                            'right'         => '8_3',
                                                        ],
                                                '8_4'=>[
                                                            'route'         => 'misc.directstatement',
                                                            'displayName'   => 'misc.gdirectsales',
                                                            'right'         => '8_4',
                                                        ],
                                                '8_5'=>[
                                                            'route'         => 'misc.groupstatement',
                                                            'displayName'   => 'misc.ggroup',
                                                            'right'         => '8_5',
                                                            'check'         => ['ranking','MIBPIBRank','bypassNetwork'],
                                                        ],
                                                ],
                        ],

                '9'=>[
                            'route'         => '',
                            'displayName'   => 'sidebar.settingsTitle',
                            'icon'          => 'fa fa-user-o',
                            'right'         => '9',
                            'child'         => [
                                                '9_1'=>[
                                                            'route'         => 'settings.account',
                                                            'displayName'   => 'sidebar.settingsLink1',
                                                            'right'         => '9_1',
                                                        ],
                                                '9_2'=>[
                                                            'route'         => 'settings.BankInfo',
                                                            'displayName'   => 'sidebar.settingsLink2',
                                                            'right'         => '9_2',
                                                        ],
                                                '9_3'=>[
                                                            'route'         => 'settings.BankInfoAsia',
                                                            'displayName'   => 'sidebar.settingsLink3',
                                                            'right'         => '9_3',
                                                            'check'         => ['account2Network','seaNetwork'],
                                                        ],
                                                '9_4'=>[
                                                            'route'         => 'settings.BankInfoAsia',
                                                            'displayName'   => 'sidebar.settingsLink2',
                                                            'right'         => '9_4',
                                                            'check'         => ['account2Network','seaNetwork'],
                                                        ],
                                                ],
                        ],

                '10'=>[
                            'route'         => 'inbox.list',
                            'displayName'   => 'inbox.Title',
                            'icon'          => 'fa fa-envelope-o',
                            'right'         => '10',
                        ],

                '11'=>[
                            'route'         => 'helpdesk.list',
                            'displayName'   => 'helpdesk.Title',
                            'icon'          => 'fa fa-ticket',
                            'right'         => '11',
                            'child'         => [
//                                '11_1'=>[
//                                    'route'         => 'faq.index',
//                                    'displayName'   => 'sidebar.faq',
//                                    'right'         => '11',
//                                ],
                                '11_1'=>[
                                    'route'         => 'helpdesk.faq',
                                    'displayName'   => 'sidebar.faq',
                                    'right'         => '11',
                                ],
                            ],

                        ],

                '12'=>[
                            'route'         => 'usermanual',
                            'displayName'   => 'sidebar.usermanual',
                            'icon'          => 'fa fa-file-pdf-o',
                            'right'         => '12',
                        ],
	       ],





    'routes'=>[
                // Home
                'home'                          => '1',
                'member.dashboardGraphRank'     => '1',
                'member.dashboardGraphMember'   => '1',
                'member.dashboardGraphInvest'   => '1',
                'member.dashboardGraphSales'    => '1',
                'capx.getBalance'               => '1',


                // Event
                'event.promotion'               => '2',


                // Withdrawal
                'withdrawal'                    => '3_1',
                'withdrawal.getBankData'        => '3_1',
                'withdrawal.postCreate'         => '3_1',

                'withdraw.records'              => '3_2',
                'withdraw.withdrawRecords'      => '3_2',
                'withdrawal.cancel'             => '3_2',

                'withdraw.withdrawAsia'         => '3_3',
                'withdrawal.postCreateAsia'     => '3_3',

                'withdraw.Asiarecords'          => '3_4',
                'withdraw.withdrawAsiaRecords'  => '3_4',


                // Capx
                'capx.mytrcap'                  => '4_1_1',

                'capx.trcap3level'              => '4_1_2',

                'capx.onsalescap'               => '4_1_3',

                'capx.mymargin'                 => '4_2_1',
                'capx.getMyMargin'              => '4_2_1',

                'capx.margin3level'             => '4_2_2',
                'capx.get3levelmargin'          => '4_2_2',

                'capx.onsales'                  => '4_2_3',

                'capx.loginRedirect'            => '4_3',

                'capx.transferToCapx'           => '4_4',
                'capx.postTransferToCapx'       => '4_4',
                

                // Fund Manage
                'misc.lpoa'                     => '5_1',
                'account.license_agreement'     => '5_1',

                'misc.fundstatement'            => '5_2',
                'member.fundaccountlist'        => '5_2',
                'member.addFund'                => '5_2',
                'transaction.fundchecking'      => '5_2',
                'transaction.transferfund'      => '5_2',
                'transaction.subscribefund'     => '5_2',
                'transaction.switchfund'        => '5_2',
                'transaction.unsubscribefund'   => '5_2',
                'transaction.postCancelUnsubscribe' => '5_2',
                'transaction.postTransferMT5Fund'   => '5_2',
                'transaction.postSubscribeMT5Fund'  => '5_2',
                'transaction.postSwitchMT5Fund'     => '5_2',
                'transaction.postUnsubscribeMT5Fund'    => '5_2',

                'mergelist'                     => '5_3',
                'transaction.mergeFund'         => '5_3',
                'getmergelist'                  => '5_3',
                'transaction.cancelMerge'       => '5_3',
                'transaction.postMergeFund'     => '5_3',
                'transaction.editmergefund'     => '5_3',

                'transaction.changepass'        => '5_4',
                'transaction.postChangePass'    => '5_4',

                'misc.fundtradestatement'       => '5_5',
                'member.fundtradeaccountlist'   => '5_5',
                'misc.showDetailsModal'         => '5_5',


                // Self Trade
                'misc.mt5statementself'         => '6_1',
                'member.selfaccountlist'        => '6_1',
                'member.addMT5self'             => '6_1',

                'transaction.transfermt5self'       => '6_2',
                'transaction.postTransferMT5self'   => '6_2',

                'transaction.convertaself'      => '6_3',
                'transaction.postConvertASelf'  => '6_3',


                // Wallet
                'misc.cashstatement'            => '7_1_1',
                'member.cashlist'               => '7_1_1',

                'transaction.convertwtoc'       => '7_2_1',
                'transaction.postconvertwtoc'   => '7_2_1',

                'transaction.convertwtot'       => '7_2_2',
                'transaction.postconvertwtot'   => '7_2_2',

                'misc.5percentstatement'        => '7_2_3',
                'member.5percentlist'           => '7_2_3',

                'misc.wstatement'               => '7_2_4',
                'member.wlist'                  => '7_2_4',
                'misc.wstatementShow'           => '7_2_4',

                'misc.bstatement'               => '7_3_1',
                'member.blist'                  => '7_3_1',

                'transaction.convertitoc'       => '7_4_1',
                'transaction.postconvertitoc'   => '7_4_1',

                'transaction.convertitot'       => '7_4_2',
                'transaction.postconvertitot'   => '7_4_2',

                'misc.commstatement'            => '7_4_3',

                'misc.getcommstatementDaily'    => '7_4_4',
                'misc.commstatementDaily'       => '7_4_4',
                'misc.commstatementDaily.showDetailsModal'  => '7_4_4',
                'misc.commstatementDaily.setsession'        => '7_4_4',

                'misc.incomestatement'          => '7_4_5',
                'member.bonuslist'              => '7_4_5',

                'transaction.convertttoc'       => '7_5_1',
                'transaction.postconvertttoc'   => '7_5_1',

                'transaction.transfercash'      => '7_5_2',
                'transaction.postTransferCash'  => '7_5_2',
                'member.checkNetwork'           => '7_5_2',

                'misc.transferstatement'        => '7_5_3',
                'member.transferlist'           => '7_5_3',


                // Network
                'network.grouplevel'            => '8_1',

                'network.3level'                => '8_2',

                'network.unilevel'              => '8_3',
                'member.getUnilevel'            => '8_3',
                'member.unilevelSearch'         => '8_3',

                'misc.directstatement'          => '8_4',
                'member.directsales'            => '8_4',

                'misc.groupstatement'           => '8_5',
                'member.groupsales'             => '8_5',
                'member.getGroupsales'          => '8_5',


                // Member Details
                'settings.account'              => '9_1',
                'account.postUpdate'            => '9_1',
                'account.postID'                => '9_1',
                'account.updatePassword'        => '9_1',
                'account.postProfile'           => '9_1',

                'settings.BankInfo'             => '9_2',
                'bankinfo.getlist'              => '9_2',
                'bankinfo.getState'             => '9_2',
                'bankinfo.getCity'              => '9_2',
                'bankinfo.addBank'              => '9_2',


                // Inbox
                'inbox.list'                    => '10',
                'announcement.getList'          => '10',
                'inbox.getCSlist'               => '10',
                'inbox.getSYSTEMlist'           => '10',
                'announcement.read'             => '10',
                'inbox.read'                    => '10',


                // Helpdesk
                'helpdesk.list'                 => '11',
                'helpdesk.addDesk'              => '11',
                'helpdesk.getList'              => '11',
                'helpdesk.read'                 => '11',
                'helpdesk.postComplain'         => '11',
                'helpdesk.postDesk'             => '11',
                'helpdesk.postcreate'           => '11',
                'helpdesk.postRate'             => '11',
                'helpdesk.postSettle'           => '11',
//                'faq.index'            => '11',
                'helpdesk.faq'              => '11',



        // User Manual
                'usermanual'                    => '12',


                // Sealead
                'sealead.deposit'               => '13_1',
                'sealead.postCreateDeposit'     => '13_1',

                'sealead.withdraw'              => '13_2',
                'sealead.addbank'               => '13_2',
                'sealead.postCreateWithdraw'    => '13_2',

                'sealead.record'                => '13_3',
                'sealead.findRecord'            => '13_3',


                // Share routes
                'settings.addBank'              => '3_1,3_3,9_2',
                'settings.addBankAsia'          => '9_3,9_4,13_2',
                'settings.BankInfoAsia'         => '9_3,9_4,13_2',
                'settings.getlistAsia'          => '9_3,9_4,13_2',
                'settings.detail'               => '9_3,9_4,13_2',
                'bankinfo.detail'               => '9_2,9_3,9_4,13_2',
                'bankinfo.getlistAsia'          => '9_3,9_4',
                'bankinfo.addBankAsia'          => '9_3,9_4',
                'sealead.setCurrency'           => '13_1,13_2,13_3',
                'sealead.updateCurrency'        => '13_1,13_2,13_3',
                'capx.subscribetrc'             => '1,4_1_3',
                'capx.getAllMargin'             => '1,4_2_3',
                'capx.subscribeMargin'          => '1,4_2_3',
                'withdrawal.getBankDataAsia'    => '3_3,13_2',
                'amazon.read'                   => '5_1,9_1',
                'maintenance'                   => '11',
           ]

];
