<?php

return [
        'maintenanceStart' => '2019-05-01 00:00:00',
        'maintenanceEnd' => '2019-05-01 06:00:00',
        'maintenanceUserAccess'=> [
                                    'fxdb222@gmail.com', 
                                    'weiluntan0523@gmail.com', 
                                    'seanetlead@gmail.com'
                                ],

        'transferToAll' => [
                                '90330918lav@163.com', 
                                'finex@finex.com'
                            ],
                            
        'bypasstrcapcheck' => [
                                '2916', // fxdb222@gmail.com
                                '2491', // leest@gmail.com
                                '10117' // fjmxl123456@sina.com
                            ]
];
