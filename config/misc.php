<?php
// Note: for december and january this case cant use
$serverdate = date("Y-m-d H:i:s", strtotime('+8 hours'));
$date = date("Y-m-01 H:i:s", strtotime($serverdate));

if(date('d', strtotime($serverdate)) <= 7){
    $selectedMonthStart = date('Y-m-01', strtotime($date. "-1 month"));
    $selectedMonthEnd = date('Y-m-01', strtotime($date));
}else{
    $selectedMonthStart = date('Y-m-01', strtotime($date));
    $selectedMonthEnd = date('Y-m-01', strtotime($date. "+1 month"));
}

return [
    'apiURL' => 'http://trealcap.trealfx.com/common/publicprots/interface_new.php',
    'newapiURL' => 'http://35.178.12.116/mt5/api/',
    'newapiURL2' => 'http://35.178.12.116:9527/MT5Sync/Api/',
    'newapiURL3' => 'http://13.250.99.203/MT5Sync/Api/',
    'newapiURL4' => 'http://35.178.12.116:8081/MT5Sync/Api/',
    //'imagePath' => 'http://localhost/CRM/public/ID/',
    'lpPath_testing' => 'http://testing.trtechsolution.com/LP/',
    'imagePath' => 'https://crm.trtechsolution.com/ID/',
    'declarePath' => 'https://crm.trtechsolution.com/declare/',
    'lpPath' => 'https://crm.trtechsolution.com/LP/',
    'withdrawPath_live' => 'https://crm.trtechsolution.com/Jreceipt/',
    'withdrawPath' => 'http://staging.trtechsolution.com/Jreceipt/',
    'withdrawPath_testing' => 'http://testing.trtechsolution.com/Jreceipt/',
    'helpdeskPath_live' => 'https://crm.trtechsolution.com/helpdesk/',
    'helpdeskPath' => 'http://staging.trtechsolution.com/helpdesk/',
    'helpdeskPath_testing' => 'https://crm.trtechsolution.com/helpdesk/',
    
    //'lpPath' => 'http://staging.trtechsolution.com/LP/',
    //'incomePath' => 'https://crm.trtechsolution.com/income/',
    'selectedMonthStart' => $selectedMonthStart,
    'selectedMonthEnd' => $selectedMonthEnd,
    'startTime' => ' 05:00:00',
    'endTime' => ' 01:00:00',
	'withdrawAdminFee'	=>	5, // in percent
    'insurance'    =>    550,
    'book'    =>    [
        'bookA' => [
            'groupname'    =>  'live|H|PLUS|USD',
            'lever'    =>    125
        ],
        'bookB' => [
            'groupname'    =>    'live|M|MINUS|USD',
            'lever'    =>    500
        ],
    ],
    
    'newbook'    =>    [
        'newbookA' => [
            //'groupname'    =>  'demo\demoforex',
            'groupname'    =>  'live\H\PLUS\USD',
            'lever'    =>    125
        ],
        'newbookB' => [
            //'groupname'    =>    'demo\demoforex',
            'groupname'    =>    'live\M\MINUS\USD',
            'lever'    =>    500
        ],
    ],
    
    'fundbook'    =>    [
    'fundbookA' => [
    //'groupname'    =>  'demo\demoforex',
    'groupname'    =>  'live\MANAGE\PLUS\USD',
    'lever'    =>    125
    ],
    'fundbookB' => [
    //'groupname'    =>    'demo\demoforex',
    'groupname'    =>    'live\MANAGE\MINUS\USD',
    'lever'    =>    500
    ],
    ],
    
    'pbook'    =>    [
    'pbookA' => [
    //'groupname'    =>  'demo\demoforex',
    'groupname'    =>  'live\M\PLUS\USD',
    'lever'    =>    100
    ],
    ],
   
    
	'bonus'	=>	[
		'group'		=>	1, // in percent
		'override'	=>	12, // in percent
		'pairing'	=>	500, // in USD
		'calculation' => [ // in percent
			'cash'	=>	20,
			'promotion'	=>	80
		],
	],
	'shares' => [
		'sellRange'	=>	0.003,
		'sellValue' => [ // all values in percent
	        'cash'  =>  48,
	        'buyBack'   =>  30,
	        'point' =>  12,
	        'fee'   =>  10,
	    ]
	],
    'LPOA' => [ //weilun
    'chs_lpoa' =>  '中文',
    'en_lpoa' =>  'English',
    //'vn_lpoa' =>  'Việt Nam',
    
    
    ],
	'countries' => [

        'China'    =>    [
            'code'    =>    'CN',
            'currency'    =>    'CNY',
            'buy'    =>    '7.00',
            'sell'    =>    '6.00',
            'banks'    =>    [
                'ICBC Bank (工商银行)',
                'CMB Bank (招商银行)',
                'Bank of China (中国银行)',
                'ABC Bank (农业银行)'
            ]
        ],
        'Cambodia' => [
            'code'    =>    'KH',
            'currency'  =>  'KHR',
            'buy'   =>  '5100.00',
            'sell'  =>  '4000.00',
            'banks' =>  [
                'Public Bank',
                'MayBank',
                'ACLEDA Bank'
            ]
        ],

        'Cayman-Islands'    =>  [
            'code'    =>    'BD',
            'currency'  =>  'BDT',
            'buy'   =>  '84.14',
            'sell'  =>  '75.73',
            'banks' =>  [
                'Bangladesh Bank'
            ]
        ],
        'Cyprus'    =>  [
            'code'    =>    'BD',
            'currency'  =>  'BDT',
            'buy'   =>  '84.14',
            'sell'  =>  '75.73',
            'banks' =>  [
                'Bangladesh Bank'
            ]
        ],
        'Australia'    =>  [
            'code'    =>    'AU',
            'currency'  =>  'AUD',
            'buy'   =>  '1.35',
            'sell'  =>  '1.22',
            'banks' =>  [
                'Commonwealth Bank',
                'Australia and New Zealand Banking Group',
                'National Australian Bank '
            ]
        ],
        'Austria'    =>  [
            'code'    =>    'BD',
            'currency'  =>  'BDT',
            'buy'   =>  '84.14',
            'sell'  =>  '75.73',
            'banks' =>  [
                'Bangladesh Bank'
            ]
        ],
        'Brazil'    =>  [
            'code'    =>    'BD',
            'currency'  =>  'BDT',
            'buy'   =>  '84.14',
            'sell'  =>  '75.73',
            'banks' =>  [
                'Bangladesh Bank'
            ]
        ],
        'Brunei'    =>  [
            'code'    =>    'BN',
            'currency'  =>  'BND',
            'buy'   =>  '1.51',
            'sell'  =>  '1.36',
            'banks' =>  [
                'Baiduri Bank',
                'Bank Islam Brunei Darussalam',
                'Perbadanan Tabung Amanah Islam Brunei'
            ]
        ],

        'Bangladesh'    =>  [
            'code'    =>    'BD',
            'currency'  =>  'BDT',
            'buy'   =>  '84.14',
            'sell'  =>  '75.73',
            'banks' =>  [
                'Bangladesh Bank'
            ]
        ],
        'Belize'    =>  [
            'code'    =>    'BD',
            'currency'  =>  'BDT',
            'buy'   =>  '84.14',
            'sell'  =>  '75.73',
            'banks' =>  [
                'Bangladesh Bank'
            ]
        ],
        'British-virgin'    =>  [
            'code'    =>    'BD',
            'currency'  =>  'BDT',
            'buy'   =>  '84.14',
            'sell'  =>  '75.73',
            'banks' =>  [
                'Bangladesh Bank'
            ]
        ],
        'Bahrain'    =>  [
            'code'    =>    'BD',
            'currency'  =>  'BDT',
            'buy'   =>  '84.14',
            'sell'  =>  '75.73',
            'banks' =>  [
                'Bangladesh Bank'
            ]
        ],
        'Egypt'    =>  [
            'code'    =>    'BD',
            'currency'  =>  'BDT',
            'buy'   =>  '84.14',
            'sell'  =>  '75.73',
            'banks' =>  [
                'Bangladesh Bank'
            ]
        ],

        'France'    =>  [
            'code'    =>    'BD',
            'currency'  =>  'BDT',
            'buy'   =>  '84.14',
            'sell'  =>  '75.73',
            'banks' =>  [
                'Bangladesh Bank'
            ]
        ],
        'Germany'    =>  [
            'code'    =>    'BD',
            'currency'  =>  'BDT',
            'buy'   =>  '84.14',
            'sell'  =>  '75.73',
            'banks' =>  [
                'Bangladesh Bank'
            ]
        ],
        'Hong-Kong'    =>  [
            'code'    =>    'BD',
            'currency'  =>  'BDT',
            'buy'   =>  '84.14',
            'sell'  =>  '75.73',
            'banks' =>  [
                'Bangladesh Bank'
            ]
        ],
        'Indonesia' =>  [
            'code'    =>    'ID',
            'currency'  =>  'IDR',
            'buy'   =>  '14000.00',
            'sell'  =>  '11500.00',
            'banks' =>  [
                'BCA Bank',
                'Mandiri Bank'
            ]
        ],
        'India'    =>  [
            'code'    =>    'IN',
            'currency'  =>  'INR',
            'buy'   =>  '68.12',
            'sell'  =>  '61.31',
            'banks' =>  [
                'Bank of Bahrain and Kuwait',
                'AB Bank',
                'Sonali Bank'
            ]
        ],
        'Japan'    =>  [
            'code'    =>    'JP',
            'currency'  =>  'JPY',
            'buy'   =>  '109.67',
            'sell'  =>  '98.70',
            'banks' =>  [
                'Development Bank of Japan',
                'Japan Finance Corporation',
                'Japan Bank for International Cooperation'
            ]
        ],
        'London'    =>  [
            'code'    =>    'BD',
            'currency'  =>  'BDT',
            'buy'   =>  '84.14',
            'sell'  =>  '75.73',
            'banks' =>  [
                'Bangladesh Bank'
            ]
        ],
        'Labuan'    =>  [
            'code'    =>    'BD',
            'currency'  =>  'BDT',
            'buy'   =>  '84.14',
            'sell'  =>  '75.73',
            'banks' =>  [
                'Bangladesh Bank'
            ]
        ],
        'Laos'    =>  [
            'code'    =>    'BD',
            'currency'  =>  'BDT',
            'buy'   =>  '84.14',
            'sell'  =>  '75.73',
            'banks' =>  [
                'Bangladesh Bank'
            ]
        ],
        'Myanmar'    =>  [
            'code'    =>    'MM',
            'currency'  =>  'MMK',
            'buy'   =>  '1383.20',
            'sell'  =>  '1244.88',
            'banks' =>  [
                'Myanma Agricultural Development Bank',
                'Myanma Economic Bank',
                'Myanma Foreign Trade Bank'
            ]
        ],
        'Mauritius'    =>  [
            'code'    =>    'MM',
            'currency'  =>  'MMK',
            'buy'   =>  '1383.20',
            'sell'  =>  '1244.88',
            'banks' =>  [
                'Myanma Agricultural Development Bank',
                'Myanma Economic Bank',
                'Myanma Foreign Trade Bank'
            ]
        ],
        'Malaysia'    =>    [
            'code'    =>    'MY',
            'currency'    =>    'MYR',
            'buy'    =>    '4.50',
            'sell'    =>    '4.00',
            'banks'    =>    [
                'Maybank',
                'Public Bank',
                'CIMB Bank',
                'RHB Bank'
            ]
        ],
        'New-Zealand'    =>  [
            'code'    =>    'NZ',
            'currency'  =>  'NZD',
            'buy'   =>  '1.45',
            'sell'  =>  '1.31',
            'banks' =>  [
                'Bank of New Zealand',
                'ANZ Bank New Zealand',
                'ASB Bank'
            ]
        ],
        'Philippines'   =>  [
            'code'    =>    'PH',
            'currency'  =>  'PHP',
            'buy'   =>  '48.00',
            'sell'  =>  '41.00',
            'banks' =>  [
                'RCBC Bank',
                'BPI Bank',
                'Metro Bank',
                'BDO Bank',
                'Land Bank of the Phillipines',
                'PNB Phillipines'
            ]
        ],
        'Sweden' =>  [
            'code'    =>    'SG',
            'currency'  =>  'SGD',
            'buy'   =>  '1.60',
            'sell'  =>  '1.30',
            'banks' =>  [
                'UOB Bank'
            ]
        ],
        'Singapore' =>  [
            'code'    =>    'SG',
            'currency'  =>  'SGD',
            'buy'   =>  '1.60',
            'sell'  =>  '1.30',
            'banks' =>  [
                'UOB Bank'
            ]
        ],
        'South-Korea'    =>  [
            'code'    =>    'KR',
            'currency'  =>  'KRW',
            'buy'   =>  '1120.00',
            'sell'  =>  '1008.00',
            'banks' =>  [
                'Korea Development Bank (한국산업은행)',
                'Woori Bank (우리은행)',
                'Industrial Bank of Korea (기업은행)'
            ]
        ],
        'Seychelles'    =>  [
            'code'    =>    'BD',
            'currency'  =>  'BDT',
            'buy'   =>  '84.14',
            'sell'  =>  '75.73',
            'banks' =>  [
                'Bangladesh Bank'
            ]
        ],
        'Saudi-arabia'    =>  [
            'code'    =>    'BD',
            'currency'  =>  'BDT',
            'buy'   =>  '84.14',
            'sell'  =>  '75.73',
            'banks' =>  [
                'Bangladesh Bank'
            ]
        ],
        'Switzerland'    =>  [
            'code'    =>    'BD',
            'currency'  =>  'BDT',
            'buy'   =>  '84.14',
            'sell'  =>  '75.73',
            'banks' =>  [
                'Bangladesh Bank'
            ]
        ],
        'Thailand'  =>  [
            'code'    =>    'TH',
            'currency'  =>  'THB',
            'buy'   =>  '40.00',
            'sell'  =>  '32.00',
            'banks' =>  [
                'TMB Bank',
                'Krungsri Bank',
                'Kasikom',
                'Siam Commercial Bank',
                'Siam City Bank',
                'Bangkok Bank'
            ]
        ],
        'Taiwan'    =>  [
            'code'    =>    'TW',
            'currency'  =>  'TWD',
            'buy'   =>  '35.00',
            'sell'  =>  '30.00',
            'banks' =>  [
                'CTBC Bank (中国信托商业银行)',
                'Bank of Taiwan (台湾银行)',
                'Esun Bank (玉山银行)'
            ]
        ],
        'United-arab-emirates'    =>  [
            'code'    =>    'BD',
            'currency'  =>  'BDT',
            'buy'   =>  '84.14',
            'sell'  =>  '75.73',
            'banks' =>  [
                'Bangladesh Bank'
            ]
        ],
        'Vietnam'    =>    [
            'code'    =>    'VN',
            'currency'    =>    'VND',
            'buy'    =>    '25000.00',
            'sell'    =>    '21000.00',
            'banks'    =>    [
                'ABC Asia Pacific',
                'VietComBank',
                'BIDV Bank'
            ]
        ],
        'Vanuatu'    =>  [
            'code'    =>    'BD',
            'currency'  =>  'BDT',
            'buy'   =>  '84.14',
            'sell'  =>  '75.73',
            'banks' =>  [
                'Bangladesh Bank'
            ]
        ],











	]
];
