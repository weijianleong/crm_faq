<?php

namespace App\Http\Middleware;
use App;
use Closure;

class RankMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!\Sentinel::check()) {
            if(\Route::currentRouteName() == 'home'){
                return redirect()->route('login', ['lang' => \App::getLocale()]);
            }else{
                return redirect()->route('login', ['lang' => \App::getLocale()])->with('flashMessage', [
                'class'  =>  'warning',
                'message'   =>  \Lang::get('error.login')
                ]);
            }
            
        }
        $user = \Sentinel::getUser();
        $member = \DB::table('Member')->where('username', trim($user->username))->first();
        $ranking = \DB::table('Member_Sales_Summary')->where('member_id', $member->id)->orderBy('date', 'DESC')->first();
        if(isset($ranking)){
            if(!$ranking->rank) return redirect()->route('home', ['lang' => \App::getLocale()]);
        }else{
            return redirect()->route('home', ['lang' => \App::getLocale()]);
        }
        return $next($request);
    }
}
