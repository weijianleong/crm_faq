<?php

namespace App\Http\Middleware;
use App\Repositories\MemberRepository;
use App;
use Closure;

class CheckSeaLeaderMiddleware
{
    protected $MemberRepository;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function __construct(MemberRepository $MemberRepository) {
        $this->MemberRepository = $MemberRepository;
    }
    public function handle($request, Closure $next)
    {
        if (!\Sentinel::check()) {
            return redirect()->route('login', ['lang' => \App::getLocale()])->with('flashMessage', [
                'class'  =>  'warning',
                'message'   =>  \Lang::get('error.login')
            ]);
        }
        
        $user = \Sentinel::getUser();
        $model = $this->MemberRepository->findByUsername($user->email);
        $detail = $model->detail;
        
        if($detail->nationality!="China"){

            if($model->register_by!="seanetlead@gmail.com"){

                $leader = \DB::table('Member_Network')->where('parent_username', 'seanetlead@gmail.com')->where('username', '=',$model->register_by)->first();
                if(!empty($leader)){
                    $model->leader = $leader->parent_username;
                }else{
                    $model->leader = '';
                }
            }else{
                $model->leader = $model->register_by;
            }

            if($model->process_status=='N'){
                return redirect()->route('home', ['lang' => App::getLocale()])->with('flashMessage',[
                    'class' => 'danger',
                    'message' => \Lang::get('error.memberPending')
                ]);
            }

            if($model->leader=="seanetlead@gmail.com"){
                return redirect()->route('home', ['lang' => App::getLocale()])->with('flashMessage',[
                    'class' => 'danger',
                    'message' => \Lang::get('error.noright')
                ]);
            }

        }



        return $next($request);
    }
}
