<?php

namespace App\Http\Middleware\API;
use DB;
use Closure;
use Carbon\Carbon;

class PartnerAuthenticate
{
    public function handle($request, Closure $next)
    {
        $temp = [];
        $data = $request->all();

        try{
            // Check Partner Authenticate
            $validPartner = DB::table('Partner')->where('access_id', $data['access_id'])->where('secret_access_key', $data['secret_access_key'])->count();
            if(!$validPartner) return response()->json(['code' => 100, 'message' => "Authenticate Failed", 'result' => array()]);

            // Check valid sign
            foreach ($data as $key => $item) {
                $item = strtolower($item);
                if(is_object(json_decode($item))) $item = json_decode($item);
                if($key == 'access_id' || $key == 'sign') continue;
                $temp[$key] = $item;
            }
            ksort($temp);
            $temp['secret_access_key'] = $data['secret_access_key'];
            if(md5(json_encode($temp)) != $data['sign']) return response()->json(['code' => 101, 'message' => "Invalid Sign", 'result' => array()]);
            
            // Insert API Log
            if(empty($data['service_id'])){
                $uniqueKey = uniqid();
                $service_id = $uniqueKey;
                $request->request->add(['service_id' => $uniqueKey]);
            }else{
                $service_id = $data['service_id'];
            }
            DB::table('API_Services')->insert([ 'access_id' =>  $data['access_id'], 'type' => 1, 'name' => \Route::currentRouteName(), 'dataIn' => json_encode($data), 'status' => 'Pending', 'service_id' => $service_id, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now() ]);

        }catch (\Exception $e) {
            $dataOut = ['code' => 400, 'message' => "Bad Gateway", 'result' => array()];
            return response()->json($dataOut);
        }

        return $next($request);
    }
}
