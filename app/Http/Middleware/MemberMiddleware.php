<?php

namespace App\Http\Middleware;

use Closure;
use view;
use App\Repositories\MemberRepository;

class MemberMiddleware
{
    protected $MemberRepository;
    public function __construct(MemberRepository $MemberRepository) {
        $this->MemberRepository = $MemberRepository;
    }

    public function handle($request, Closure $next)
    {
        if (!\Sentinel::check()) {
            if(\Route::currentRouteName() == 'home'){
                return redirect()->route('login', ['lang' => \App::getLocale()]);
            }else{
                return redirect()->route('login', ['lang' => \App::getLocale()])->with('flashMessage', [
                'class'  =>  'warning',
                'message'   =>  \Lang::get('error.login')
                ]);
            }
            
        }
        $user = \Sentinel::getUser();
        $permissions = $user->permissions;
        if (!isset($permissions['member'])) {
            return redirect()->route('login', ['lang' => \App::getLocale()])->with('flashMessage', [
                'class'  =>  'warning',
                'message'   =>  \Lang::get('error.login')
            ]);
        }
        if ($permissions['member'] != 1) {
            return redirect()->route('login', ['lang' => \App::getLocale()])->with('flashMessage', [
                'class'  =>  'warning',
                'message'   =>  \Lang::get('error.login')
            ]);
        }

        $member = $this->MemberRepository->findByUsername($user->username);
        $access = \DB::table('Member_Access_Type')->where('id', $member->detail->access_type)->first();
        $userRightsAry = json_decode($access->access);
        
        $account2Network = \DB::table('Member_Network')->where('parent_username', 'account2@trfxbiz.com')->where('username', $user->username)->count();
        if(in_array($user->username, ['fxdb222@gmail.com'])) $account2Network = 1;
        
        $seaNetwork = \DB::table('Member_Network')->where('parent_username', 'seanetlead@gmail.com')->where('username', $member->register_by)->count();
        if(in_array($user->username, ['seanetlead@gmail.com']) || $member->register_by == 'seanetlead@gmail.com') $seaNetwork = 1;

        if(isset(config('member.routes')[\Route::currentRouteName()])){
            $routeKey = config('member.routes')[\Route::currentRouteName()];
            $rights = explode(',', $routeKey);
            $pass = false;

            foreach ($rights as $item) {
                if(in_array($item, $userRightsAry)){
                    $routeKey = $item;
                    $pass = true;
                    break;
                }
            }

            if($pass){
                if(preg_match('/_/', $routeKey)){
                    $splitRouteKey = explode('_', $routeKey);
                    $block = true;

                    if(count($splitRouteKey) == 3){
                        if(isset(config('member.menus')[$splitRouteKey[0]]['child'][$splitRouteKey[0].'_'.$splitRouteKey[1]]['child'][$routeKey]['check'])){
                            $checkArray = config('member.menus')[$splitRouteKey[0]]['child'][$splitRouteKey[0].'_'.$splitRouteKey[1]]['child'][$routeKey]['check'];
                            
                            if(in_array('account2Network', $checkArray)){
                                if($account2Network > 0) $block = false;
                            }

                            if(in_array('seaNetwork', $checkArray)){
                                if($seaNetwork > 0) $block = false;
                            }

                            if(in_array('ranking', $checkArray)){
                                $ranking = \DB::table('Member_Sales_Summary')->where('member_id', $member->id)->orderBy('date', 'DESC')->first();
                                if(isset($ranking)){
                                    if($ranking->rank) $block = false;

                                    if(in_array('MIBPIBRank', $checkArray)){
                                        if($ranking->rank <> 'MIB' && $ranking->rank <> 'PIB') $block = true;
                                    }
                                }
                            }

                            if(in_array('purchasedTrcap', $checkArray)){
                                if($member->coin->trc > 0) $block = false;
                            }

                            if(in_array('lpoa', $checkArray)){
                                if($user->permission_type == 1 && $member->lpoa_status == 'Y') $block = false;

                                if(in_array('fundchgpass', $checkArray)){
                                    if(date('l') == 'Saturday' || date('l') == 'Sunday'){
                                        if(date('l') == 'Saturday' && date('H', time()) < 9) $block = true;
                                    }else{
                                        $block = true;
                                    }
                                }
                            }

                            if(in_array('bypassTWallet', $checkArray)){
                                if(in_array($user->username, config('member.bypassTWallet'))) $block = false;
                            }

                            if(in_array('bypassNetwork', $checkArray)){
                                if(in_array($user->username, config('member.bypassNetwork'))) $block = false;
                            }
                        }else{
                            $block = false;
                        }

                    }
                    else if(count($splitRouteKey) == 2){
                        if(isset(config('member.menus')[$splitRouteKey[0]]['child'][$routeKey]['check'])){
                            $checkArray = config('member.menus')[$splitRouteKey[0]]['child'][$routeKey]['check'];
                            
                            if(in_array('account2Network', $checkArray)){
                                if($account2Network > 0) $block = false;
                            }

                            if(in_array('seaNetwork', $checkArray)){
                                if($seaNetwork > 0) $block = false;
                            }

                            if(in_array('ranking', $checkArray)){
                                $ranking = \DB::table('Member_Sales_Summary')->where('member_id', $member->id)->orderBy('date', 'DESC')->first();
                                if(isset($ranking)){
                                    if($ranking->rank) $block = false;

                                    if(in_array('MIBPIBRank', $checkArray)){
                                        if($ranking->rank <> 'MIB' && $ranking->rank <> 'PIB') $block = true;
                                    }
                                }
                            }

                            if(in_array('purchasedTrcap', $checkArray)){
                                if($member->coin->trc > 0) $block = false;
                            }

                            if(in_array('lpoa', $checkArray)){
                                if($user->permission_type == 1 && $member->lpoa_status == 'Y') $block = false;

                                if(in_array('fundchgpass', $checkArray)){
                                    if(date('l') == 'Saturday' || date('l') == 'Sunday'){
                                        if(date('l') == 'Saturday' && date('H', time()) < 9) $block = true;
                                    }else{
                                        $block = true;
                                    }
                                }
                            }

                            if(in_array('bypassTWallet', $checkArray)){
                                if(in_array($user->username, config('member.bypassTWallet'))) $block = false;
                            }

                            if(in_array('bypassNetwork', $checkArray)){
                                if(in_array($user->username, config('member.bypassNetwork'))) $block = false;
                            }
                        }else{
                            $block = false;
                        }

                    }

                }else{
                    $block = true;
                    if(isset(config('member.menus')[$routeKey]['check'])){
                        $checkArray = config('member.menus')[$routeKey]['check'];

                        if(in_array('account2Network', $checkArray)){
                            if($account2Network > 0) $block = false;
                        }

                        if(in_array('seaNetwork', $checkArray)){
                            if($seaNetwork > 0) $block = false;
                        }

                        if(in_array('ranking', $checkArray)){
                            $ranking = \DB::table('Member_Sales_Summary')->where('member_id', $member->id)->orderBy('date', 'DESC')->first();
                            if(isset($ranking)){
                                if($ranking->rank) $block = false;

                                if(in_array('MIBPIBRank', $checkArray)){
                                    if($ranking->rank <> 'MIB' && $ranking->rank <> 'PIB') $block = true;
                                }
                            }
                        }

                        if(in_array('purchasedTrcap', $checkArray)){
                            if($member->coin->trc > 0) $block = false;
                        }

                        if(in_array('lpoa', $checkArray)){
                            if($user->permission_type == 1 && $member->lpoa_status == 'Y') $block = false;

                            if(in_array('fundchgpass', $checkArray)){
                                if(date('l') == 'Saturday' || date('l') == 'Sunday'){
                                    if(date('l') == 'Saturday' && date('H', time()) < 9) $block = true;
                                }else{
                                    $block = true;
                                }
                            }
                        }

                        if(in_array('bypassTWallet', $checkArray)){
                            if(in_array($user->username, config('member.bypassTWallet'))) $block = false;
                        }

                        if(in_array('bypassNetwork', $checkArray)){
                            if(in_array($user->username, config('member.bypassNetwork'))) $block = false;
                        }
                    }else{
                        $block = false;
                    }

                }

                if($block){
                    if(in_array('1', $userRightsAry)){
                        return redirect()->route('home', ['lang' => \App::getLocale()]);
                    }else{
                        if($seaNetwork) return redirect()->route('sealead.withdraw', ['lang' => \App::getLocale()]);
                        else return redirect()->route('withdrawal', ['lang' => \App::getLocale()]);
                    }
                }
            }else{
                if(in_array('1', $userRightsAry)){
                    return redirect()->route('home', ['lang' => \App::getLocale()]);
                }else{
                    if($seaNetwork) return redirect()->route('sealead.withdraw', ['lang' => \App::getLocale()]);
                    else return redirect()->route('withdrawal', ['lang' => \App::getLocale()]);
                }
            }
        }else{
            if(in_array('1', $userRightsAry)){
                return redirect()->route('home', ['lang' => \App::getLocale()]);
            }else{
                if($seaNetwork) return redirect()->route('sealead.withdraw', ['lang' => \App::getLocale()]);
                else return redirect()->route('withdrawal', ['lang' => \App::getLocale()]);
            }
        }

        if($user->is_rstpass == 0){
            $aryCheck = ['settings.account','account.updatePassword'];
            if(!in_array(\Route::currentRouteName(), $aryCheck) ) return redirect()->route('settings.account', ['lang' => \App::getLocale()]);
        }

        return $next($request);
    }
}
