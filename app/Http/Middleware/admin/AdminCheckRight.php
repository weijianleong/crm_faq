<?php

namespace App\Http\Middleware\admin;
use App\Repositories\AdminRightRepository;
use Closure;

class AdminCheckRight
{
    protected $AdminRightRepository;
    /**
     * Create a new AnnouncementController instance.
     *
     * @param \App\Repositories\AnnouncementRepository $AnnouncementRepository
     * @return void
     */
    public function __construct(AdminRightRepository $AdminRightRepository) {

        $this->AdminRightRepository = $AdminRightRepository;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!\Sentinel::check()) {
            return redirect()->route('admin.login')->with('flashMessage', [
                'class'  =>  'warning',
                'message'   =>  'Please login first.'
            ]);
        }
        $user = \Sentinel::getUser();
        $permissions = $user->permissions;
        if (!isset($permissions['admin'])) {
            return redirect()->route('admin.login')->with('flashMessage', [
                'class'  =>  'warning',
                'message'   =>  'Please login first.'
            ]);
        }
        if ($permissions['admin'] != 1) {
            return redirect()->route('admin.login')->with('flashMessage', [
                'class'  =>  'warning',
                'message'   =>  'Please login first.'
            ]);
        }
        $model = $this->AdminRightRepository->findById($user->id);
        $right = json_decode($model->right, true);
        
        if ($model->level != "superadmin") {
            return redirect()->route('admin.login')->with('flashMessage', [
                'class'  =>  'danger',
                'message'   =>  'You no have the Permissions!!'
            ]);
        }
        
        return $next($request);
    }
}
