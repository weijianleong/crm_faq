<?php

namespace App\Http\Middleware;

use Closure;
use App;
use App\Repositories\HelpdeskRepository;
use App\Repositories\MemberRepository;


class CheckAddHelpDeskMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function __construct(MemberRepository $MemberRepository, HelpdeskRepository $HelpdeskRepository) {
        $this->MemberRepository = $MemberRepository;
        $this->HelpdeskRepository = $HelpdeskRepository;
    }
    public function handle($request, Closure $next)
    {
        if (!\Sentinel::check()) {
            return redirect()->route('login', ['lang' => \App::getLocale()])->with('flashMessage', [
                'class'  =>  'warning',
                'message'   =>  \Lang::get('error.login')
            ]);
        }

        $user = \Sentinel::getUser();
        $model = $this->MemberRepository->findByUsername($user->email);
        if($checkBlackList = $this->HelpdeskRepository->checkBlackList($model->id)){
            if($checkBlackList->is_ban == 1){

                return redirect()->back()->with('flashMessage', [
                    'class' => 'danger',
                    'message' => \Lang::get('helpdesk.errorBlack')
                ]);

            }
        }



        $get_DeskNum = $this->HelpdeskRepository->get_DeskNum($model->id);

        if( $get_DeskNum >= 2 ){

            return redirect()->back()->with('flashMessage', [
                'class' => 'danger',
                'message' => \Lang::get('helpdesk.errorAdd5')
            ]);

        }
        
            
        
        return $next($request);
    }
}
