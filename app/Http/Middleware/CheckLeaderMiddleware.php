<?php

namespace App\Http\Middleware;
use App;
use Closure;

class CheckLeaderMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!\Sentinel::check()) {
            return redirect()->route('login', ['lang' => \App::getLocale()])->with('flashMessage', [
                'class'  =>  'warning',
                'message'   =>  \Lang::get('error.login')
            ]);
        }
        $user = \Sentinel::getUser();
        if($user->leader!='Y'){
            return redirect()->route('logout', ['lang' => App::getLocale()])->with('flashMessage',[
                'class' => 'danger',
                'message' => \Lang::get('error.notLeader')
            ]);
        }
        return $next($request);
    }
}
