<?php

namespace App\Http\Middleware;
use App\Repositories\MemberRepository;
use App;
use Closure;

class CheckMemberStatusMiddleware
{
    protected $MemberRepository;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function __construct(MemberRepository $MemberRepository) {
        $this->MemberRepository = $MemberRepository;
    }
    public function handle($request, Closure $next)
    {
        if (!\Sentinel::check()) {
            return redirect()->route('login', ['lang' => \App::getLocale()])->with('flashMessage', [
                'class'  =>  'warning',
                'message'   =>  \Lang::get('error.login')
            ]);
        }
        
        $user = \Sentinel::getUser();
        $model = $this->MemberRepository->findByUsername($user->email);

        if($model->process_status=='N'){
            return redirect()->route('logout', ['lang' => App::getLocale()])->with('flashMessage',[
                'class' => 'danger',
                'message' => \Lang::get('error.memberPending')
            ]);
        }
        return $next($request);
    }
}
