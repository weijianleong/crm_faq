<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\TransferRepository;
use App\Repositories\MemberRepository;
use Uuid;
use DB;

class TransferController extends Controller
{
    /**
     * The TransferRepository instance.
     *
     * @var \App\Repositories\TransferRepository
     */
    protected $TransferRepository;

    /**
     * The MemberRepository instance.
     *
     * @var \App\Repositories\MemberRepository
     */
    protected $MemberRepository;

    /**
     * Create a new TransferController instance.
     *
     * @param \App\Repositories\TransferRepository $TransferRepository
     * @return void
     */
    public function __construct(
        TransferRepository $TransferRepository,
        MemberRepository $MemberRepository
    ) {
        $this->TransferRepository = $TransferRepository;
        $this->MemberRepository = $MemberRepository;
        $this->middleware('member');
    }

    /**
     * Transfer register, promo, or cash point
     * @return json
     */
    public function postTransferPoint () {
        $data = \Input::get('data');
        $user = \Sentinel::getUser();
       // $member = $user->member;
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        if ($member->secret_password != trim($data['s'])) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  \Lang::get('error.securityPasswordError')
            ]);
        }

        if (!$toMember = $this->MemberRepository->findByUsername(trim($data['to_username']))) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  \Lang::get('error.memberNotFound')
            ]);
        }
        
        try {
            $this->TransferRepository->makeTransfer($member, $toMember, $data['amount'], $data['remark']);
        } catch (\Exception $e) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  $e->getMessage()
            ]);
        }

        \Cache::forget('member.' . $user->id);


        return \Response::json([
            'type'  => 'success',
            'message' => \Lang::get('message.transferSuccess'),
            'redirect' => route('transaction.transferSuccess', ['lang' => \App::getLocale()])
        ]);
    }
    
    public function postTransferCashPoint (Request $request) {
        $SPassword = $request->SPassword;
        $getData = $request->Data;
        $data = json_decode($getData,true); //$data["Data"]["getPassword1"] example
        $to_username = $data["Data"]["to_username"];
        $amount = $data["Data"]["amount"];
        $remark = $data["Data"]["remark"];
        $network = $data["Data"]["network"];
        $user = \Sentinel::getUser();
        //$data = \Input::get('data');
        //$user = \Sentinel::getUser();
       // $member = $user->member;
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        if ($member->secret_password != $SPassword) {
            return response()->json(['SPassword'=>"1"]);
        }

        if(!$toMember = $this->MemberRepository->findByUsername(trim($to_username))) {
            $tittle = \Lang::get('error.memberNotFound');
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>""]);
        }

        if($toMember->process_status != 'Y'){ // activated account
            $tittle = \Lang::get('error.transferFail');
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>""]);
        }

        // ban or processing weilun
        if($toMemberIsban = $this->MemberRepository->findUser(trim($to_username))) { 
            if($toMemberIsban->is_ban == 0){
                $tittle = \Lang::get('error.memberNotFound');
                return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>""]);
            }
            if($toMemberIsban->special_group_block == 1){
                $tittle = \Lang::get('error.memberNotFound');
                return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>""]);
            }
        }else{
            $tittle = \Lang::get('error.memberNotFound');
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>""]);
        }

        if(!in_array($user->username, config('autosettings.transferToAll'))){
            if($network == 'down'){
                $checkNetwork = \DB::table('Member_Network')->where('parent_username', $user->username)->where('username', trim($to_username))->count();
                if($checkNetwork <= 0){
                    $tittle = \Lang::get('error.transferundernetwork');
                    return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>""]);
                }
            }else if($network == 'up'){
//                $checkNetwork = \DB::table('Member_Network')->where('parent_username', trim($to_username))->where('username', $user->username)->count();
                $checkNetwork = \DB::table('Member')->where('register_by', trim($to_username))->where('username', $user->username)->count();
                if($checkNetwork <= 0){
                    $tittle = \Lang::get('error.transferundernetwork');
                    return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>""]);
                }
            }
        }

        if(empty($amount) || $amount <= 0){
            $tittle = \Lang::get('error.emptyAmount');
            $text = \Lang::get('error.fillAmount');
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
        }

        if ($member->wallet->t_wallet < $amount) {
            $tittle = \Lang::get('error.cashPointNotEnough');
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>""]);
        }
        
        try {
            $uuid = Uuid::generate();
            $checkRequest = DB::table('Member_Transaction_Log')->where('member_id', $member->id)
                            ->where(function ($query) {
                                $query->whereNull('updated_at')
                                    ->orWhere('updated_at', '<>', \Carbon\Carbon::now());
                                })
                            ->update([ 'updated_at' => \Carbon\Carbon::now() ]);
            if(!$checkRequest){
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postTransferCashPoint1', 'created_at' => \Carbon\Carbon::now()]);
                $tittle = "Too Many Request...";
                $text = "";
                return response()->json(['result'=>"1","showTittle"=>$tittle]);
            }

            $updateLog = DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 0)->update(['transaction_id' => $uuid->string, 'route' => 'postTransferCashPoint', 'status' => 1, 'updated_at' => \Carbon\Carbon::now()]);
            if(!$updateLog){
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postTransferCashPoint2', 'created_at' => \Carbon\Carbon::now()]);
                $tittle = "Oops...";
                $text = "";
                $html = \Lang::get('error.helpDesk');
                return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"showHtml"=>$html]);
            }
            sleep(1);

            $checklog = DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('transaction_id', $uuid->string)->count();
            if($checklog){
                $this->TransferRepository->makeTransfer($member, $toMember, $amount, $remark,'T');
            }else{
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postTransferCashPoint3', 'created_at' => \Carbon\Carbon::now()]);
            }
            DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);
        } catch (\Exception $e) {
            DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);
            $tittle = $e->getMessage();
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>""]);
        }

        \Cache::forget('member.' . $user->id);

        $tittle = \Lang::get('message.transferSuccess');
        $text = "";
        $returnUrl = route('transaction.transfercash', ['lang' => \App::getLocale()]);
        return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,"returnUrl"=>$returnUrl]);
    }
    
    public function postTransferRPoint () {
        $data = \Input::get('data');
        $user = \Sentinel::getUser();
       // $member = $user->member;
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        if ($member->secret_password != trim($data['s'])) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  \Lang::get('error.securityPasswordError')
            ]);
        }

        if (!$toMember = $this->MemberRepository->findByUsername(trim($data['to_username']))) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  \Lang::get('error.memberNotFound')
            ]);
        }
        
        try {

            $this->TransferRepository->makeTransfer($member, $toMember, $data['amount'], $data['remark'],'R');
        } catch (\Exception $e) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  $e->getMessage()
            ]);
        }

        \Cache::forget('member.' . $user->id);


        return \Response::json([
            'type'  => 'success',
            'message' => \Lang::get('message.transferRSuccess'),
            'redirect' => route('transaction.transferrSuccess', ['lang' => \App::getLocale()])
        ]);
    }
    
    public function postTransferMT5Point () {

        $data = \Input::get('data');
        $user = \Sentinel::getUser();
        
        // $member = $user->member;
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        
        if ( $data['account'] == 0) {
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  \Lang::get('error.noaccount')
                                   ]);
        }
        
        if ($member->secret_password != trim($data['s'])) {
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  \Lang::get('error.securityPasswordError')
                                   ]);
        }
        
        
        try {
            $this->TransferRepository->makeMT5Transfer($member, $data['account'], $data['amountA'], $data['amountB']);
        } catch (\Exception $e) {
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  $e->getMessage()
                                   ]);
        }
        
        \Cache::forget('member.' . $user->id);
        
        
        return \Response::json([
                               'type'  => 'success',
                               'message' => \Lang::get('message.transferMT5Success'),
                               'redirect' => route('transaction.transfermt5Success', ['lang' => \App::getLocale()])
                               ]);
    }
    
    public function postTransferMT5PointSelf (Request $request) {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $getData = $request->Data;
        $data = json_decode($getData,true); //$data["Data"][0]["getPassword1"] example
        $amount = $data["Data"]["amount"];
        $account = $data["Data"]["account"];
        $SPassword = $request->SPassword;

        if($member->secret_password != $SPassword) {
            return response()->json(['SPassword'=>"1"]);
        }
        
        if($account == 0) {
            $tittle = \Lang::get('error.noaccount');
            $text ="";
            return response()->json(['result'=>"1","showTittle"=>$tittle]);
        }
        
        if($amount < 0) {
            $tittle = \Lang::get('error.cashPointNotEnough');
            $text ="";
            return response()->json(['result'=>"1","showTittle"=>$tittle]);
        }

        if($member->wallet->cash_point < $amount) {
            $tittle = \Lang::get('error.cashPointNotEnough');
            $text ="";
            return response()->json(['result'=>"1","showTittle"=>$tittle]);
        }

        $ownaccount = DB::table('Member_Account_Self')->where('member_id', $member->id)->where('id', $account)->count();
        if($ownaccount == 0){
            $tittle = "Oops...";
            $text = "";
            $html = \Lang::get('error.helpDesk');
            return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"showHtml"=>$html]);
        }

        // Total selftrade <= Sum of total invest
        $investtrfx = 0;
        $investcoin = 0;
        $investmargin = 0;

        $investtrfx = DB::table('Member_Account_Fund')->where('member_id', $member->id)->sum('balanceB');
        $coinWallet = DB::table('Capx_Member_Coin')->where('member_id', $member->id)->first();

        $coin = DB::table('Capx_Coin')->get();
        foreach ($coin as $coinData) {
            $symbol = $coinData->symbol;
            $investcoin += $coinData->price * $coinWallet->$symbol;
        }

        $margin = DB::table('Capx_Margin')->get();
        foreach ($margin as $marginData) {
            $symbol = $marginData->symbol;
            $investmargin += $marginData->current_price * $coinWallet->$symbol;
        }

        $totalInvest = $investtrfx * 3 + $investcoin + $investmargin;

        $allselfacc = DB::table('Member_Account_Self')->where('member_id', $member->id)->get(['bookA']);
        if(count($allselfacc) > 0){
            foreach ($allselfacc as $item) {
                $tempSelf[] = $item->bookA;
            }
        }

        $totalSelfInvest = DB::connection('mysql4')->table('mt5_accounts')->whereIn('Login', $tempSelf)->sum('Balance');

        if(floatval($totalInvest) < floatval($totalSelfInvest+$amount)){
            $tittle = \Lang::get('error.selfinvestfailed');
            $text ="";
            return response()->json(['result'=>"1","showTittle"=>$tittle]);
        }

        try {
            $uuid = Uuid::generate();
            $checkRequest = DB::table('Member_Transaction_Log')->where('member_id', $member->id)
                            ->where(function ($query) {
                                $query->whereNull('updated_at')
                                    ->orWhere('updated_at', '<>', \Carbon\Carbon::now());
                                })
                            ->update([ 'updated_at' => \Carbon\Carbon::now() ]);
            if(!$checkRequest){
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postTransferMT5PointSelf1', 'created_at' => \Carbon\Carbon::now()]);
                $tittle = "Too Many Request...";
                $text = "";
                return response()->json(['result'=>"1","showTittle"=>$tittle]);
            }

            $updateLog = DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 0)->update(['transaction_id' => $uuid->string, 'route' => 'postTransferMT5PointSelf', 'status' => 1, 'updated_at' => \Carbon\Carbon::now()]);
            if(!$updateLog){
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postTransferMT5PointSelf2', 'created_at' => \Carbon\Carbon::now()]);
                $tittle = "Oops...";
                $text = "";
                $html = \Lang::get('error.helpDesk');
                return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"showHtml"=>$html]);
            }
            sleep(1);

            $checklog = DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('transaction_id', $uuid->string)->count();
            if($checklog){
                $this->TransferRepository->makeMT5TransferSelf($member, $account, $amount);
            }else{
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postTransferMT5PointSelf3', 'created_at' => \Carbon\Carbon::now()]);
            }
            DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);
        } catch (\Exception $e) {
            DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);
            $tittle = "Oops...";
            $text = "";
            $html = \Lang::get('error.helpDesk');
            return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"showHtml"=>$html]);
        }
        
        \Cache::forget('member.' . $user->id);

        $tittle = \Lang::get('message.transferMT5Success');
        $text = \Lang::get('success.atobtext2');
        return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,'returnUrl'=>route('misc.mt5statementself', ['lang' => \App::getLocale()])]);
    }
    
    public function postTransferMT5PointFund (Request $request) {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        $SPassword = $request->SPassword;
        $getData = $request->Data;
        $data = json_decode($getData,true); //$data["Data"]["getPassword1"] example
        
        if ($member->secret_password != $SPassword) {
            $response = array('SPassword' => 1);
            return response()->json($response);
        }

        if ($data['Data']['bookA'] == 0) {
            $response = array('result' => 1, 'showTittle' => \Lang::get('error.noaccount'), 'showText' => '');
            return response()->json($response);
        }

        $ownaccount = DB::table('Member_Account_Fund')->where('member_id', $member->id)->where('id', $data['Data']['bookA'])->count();
        if($ownaccount == 0){
            $response = array('result' => 1, 'showTittle' => \Lang::get('error.helpDesk'), 'showText' => '');
            return response()->json($response);
        }
        
        try {
            $uuid = Uuid::generate();
            $checkRequest = DB::table('Member_Transaction_Log')->where('member_id', $member->id)
                            ->where(function ($query) {
                                $query->whereNull('updated_at')
                                    ->orWhere('updated_at', '<>', \Carbon\Carbon::now());
                                })
                            ->update([ 'updated_at' => \Carbon\Carbon::now() ]);
            if(!$checkRequest){
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postTransferMT5PointFund1', 'created_at' => \Carbon\Carbon::now()]);
                $response = array('result' => 1, 'showTittle' => "Too Many Request...", 'showText' => '');
                return response()->json($response);
            }

            $updateLog = DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 0)->update(['transaction_id' => $uuid->string, 'route' => 'postTransferMT5PointFund', 'status' => 1, 'updated_at' => \Carbon\Carbon::now()]);
            if(!$updateLog){
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postTransferMT5PointFund2', 'created_at' => \Carbon\Carbon::now()]);
                $response = array('result' => 1, 'showTittle' => \Lang::get('error.helpDesk'), 'showText' => '');
                return response()->json($response);
            }
            sleep(1);

            $checklog = DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('transaction_id', $uuid->string)->count();
            if($checklog){
                $this->TransferRepository->makeMT5TransferFund($member, $data['Data']['bookA'], $data['Data']['package']);
            }else{
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postTransferMT5PointFund3', 'created_at' => \Carbon\Carbon::now()]);
            }
            DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);
        } catch (\Exception $e) {
            DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);
            $response = array('result' => 1, 'showTittle' => $e->getMessage(), 'showText' => '');
            return response()->json($response);
        }
        
        \Cache::forget('member.' . $user->id);

        $response = array('result' => 0, 'showTittle' => \Lang::get('message.transferMT5Success'), 'showText' => '', 'returnUrl' => route('misc.fundstatement', ['lang' => \App::getLocale()]));
        return response()->json($response);
    }
    
    public function postSubscribeMT5PointFund (Request $request) {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        $SPassword = $request->SPassword;
        $getData = $request->Data;
        $data = json_decode($getData,true); //$data["Data"]["getPassword1"] example

        if ($member->secret_password != $SPassword) {
            $response = array('SPassword' => 1);
            return response()->json($response);
        }

        if ( $data['Data']['bookA'] == 0) {
            $response = array('result' => 1, 'showTittle' => \Lang::get('error.noaccount'), 'showText' => '');
            return response()->json($response);
        }

        if($data['Data']['fundcompany'] <> 0){
            $partners = DB::connection('mysql3')->table('user')->where('UserID', '=', $data['Data']['fundcompany'])->first();
        
            $partner_limit = $partners->AccountLimit;
        
            if($partner_limit > 0){
                $users = DB::table('Member_Account_Fund')->where('fundcompany', '=', $data['Data']['fundcompany'])->count();
            
                if(($users + 1) > $partner_limit){
                    $response = array('result' => 1, 'showTittle' => \Lang::get('error.companylimit'), 'showText' => '');
                    return response()->json($response);
                }
            }
        }

        $account = DB::table('Member_Account_Fund')->where('member_id', '=', $member->id)->where('id', $data['Data']['bookA'])->first();
        if($account->balanceB < $account->ini_amount){
            $failedMessage2 = str_replace(array('%%amount%%','%%bookA%%'), array((number_format($account->ini_amount*3,2)),$account->bookA), \Lang::get('error.minFundError'));
            $response = array('result' => 1, 'showTittle' => $failedMessage2, 'showText' => '');
            return response()->json($response);
        }

        try {
            $this->TransferRepository->makeMT5SubscribeFund($member, $data['Data']['bookA'], $data['Data']['fundcompany']);
        } catch (\Exception $e) {
            $response = array('result' => 1, 'showTittle' => $e->getMessage(), 'showText' => '');
            return response()->json($response);
        }
        
        \Cache::forget('member.' . $user->id);

        $response = array('result' => 0, 'showTittle' => \Lang::get('message.subscribeMT5Success'), 'showText' => '', 'returnUrl' => route('misc.fundstatement', ['lang' => \App::getLocale()]));
        return response()->json($response);
    }
    
      public function postUnsubscribeMT5PointFund (Request $request) {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        $SPassword = $request->SPassword;
        $getData = $request->Data;
        $data = json_decode($getData,true); //$data["Data"]["getPassword1"] example

        if ($member->secret_password != $SPassword) {
            $response = array('SPassword' => 1);
            return response()->json($response);
        }
        
        if ( $data['Data']['step'] == 1) {
            $response = array('result' => 0, 'showTittle' => '', 'showText' => '', 'returnUrl' => route('transaction.unsubscribefund', ['lang' => \App::getLocale(), 'id' => $data['Data']['id'], 'step' => 2]));
            return response()->json($response);
        }

        
        if ( $data['Data']['id'] == 0) {
            $response = array('result' => 1, 'showTittle' => \Lang::get('error.noaccount'), 'showText' => '');
            return response()->json($response);
        }

        try {
            $this->TransferRepository->makeMT5UnsubscribeFund($member, $data['Data']['id']);
        } catch (\Exception $e) {
            $response = array('result' => 1, 'showTittle' => $e->getMessage(), 'showText' => '');
            return response()->json($response);
        }
        
        \Cache::forget('member.' . $user->id);

        $response = array('result' => 0, 'showTittle' => \Lang::get('message.unsubscribeMT5Success'), 'showText' => '', 'returnUrl' => route('misc.fundstatement', ['lang' => \App::getLocale()]));
        return response()->json($response);
    }
    
    public function postCancelUnsubscribe (Request $request) {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        $SPassword = $request->SPassword;
        $getData = $request->Data;
        $data = json_decode($getData,true); //$data["Data"]["getPassword1"] example

        if ($member->secret_password != $SPassword) {
            $response = array('SPassword' => 1);
            return response()->json($response);
        }
        
        if (!$data['Data']['bookA']) {
            $response = array('result' => 1, 'showTittle' => \Lang::get('error.noaccount'), 'showText' => '');
            return response()->json($response);
        }

        try {
            $this->TransferRepository->makeMT5CancelUnsubscribeFund($member, $data['Data']['bookA']);
        } catch (\Exception $e) {
            $response = array('result' => 1, 'showTittle' => $e->getMessage(), 'showText' => '');
            return response()->json($response);
        }
        
        \Cache::forget('member.' . $user->id);

        $response = array('result' => 0, 'showTittle' => \Lang::get('misc.action5').\Lang::get('misc.success'), 'showText' => '', 'returnUrl' => route('misc.fundstatement', ['lang' => \App::getLocale()]));
        return response()->json($response);
    }

    public function postSwitchMT5PointFund (Request $request) {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        $SPassword = $request->SPassword;
        $getData = $request->Data;
        $data = json_decode($getData,true); //$data["Data"]["getPassword1"] example
        
        if ($member->secret_password != $SPassword) {
            $response = array('SPassword' => 1);
            return response()->json($response);
        }

        if ( $data['Data']['bookA'] == 0) {
            $response = array('result' => 1, 'showTittle' => \Lang::get('error.noaccount'), 'showText' => '');
            return response()->json($response);
        }

        if ( $data['Data']['fundcompany'] == 0) {
            $response = array('result' => 1, 'showTittle' => \Lang::get('error.nocompany'), 'showText' => '');
            return response()->json($response);
        }
        
        try {
            $this->TransferRepository->makeMT5SwitchFund($member, $data['Data']['bookA'], $data['Data']['fundcompany']);
        } catch (\Exception $e) {
            $response = array('result' => 1, 'showTittle' => $e->getMessage(), 'showText' => '');
            return response()->json($response);
        }
        
        \Cache::forget('member.' . $user->id);

        $response = array('result' => 0, 'showTittle' => \Lang::get('message.switchMT5Success'), 'showText' => '', 'returnUrl' => route('misc.fundstatement', ['lang' => \App::getLocale()]));
        return response()->json($response);
    }
    
    public function postProcessMT5Point () {
        $data = \Input::get('data');
        $user = \Sentinel::getUser();
        
        // $member = $user->member;
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        
        if ( $data['account'] == 0) {
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  \Lang::get('error.noaccount')
                                   ]);
        }
        
        if ($member->secret_password != trim($data['s'])) {
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  \Lang::get('error.securityPasswordError')
                                   ]);
        }
        
        $registerbalance = DB::table('Member_Wallet')->where('member_id', '=', $member->id)->first();
        
        $book = DB::table('Member_Account')->where('id', '=', $data['account'])->first();
        
        $bookA =$book->bookA;
        $bookB =$book->bookB;
        
        $tradestatus1 = DB::connection('mysql2')->table('opentrade')->where('Login', '=', $bookA)->first();
        
        $tradestatus2 = DB::connection('mysql2')->table('opentrade')->where('Login', '=', $bookB)->first();
        
        if (count($tradestatus1) > 0  || count($tradestatus2) > 0) {
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  \Lang::get('error.trading')
                                   ]);
        }
        
        $orderAP = DB::connection('mysql2')->table('historyposition')->where('Login', '=', $bookA)->orderBy('OrderId', 'desc')->first();
       
        if(!empty($orderAP))
        {
        
            //$orderAP = end($orderA);
            $order_idA = $orderAP->OrderId;
            
            $oldorder = DB::table('Orders')->where('bookAorderid', '=', $order_idA)->first();
            
            if (count($oldorder) > 0)
            {
                return \Response::json([
                                       'type'  =>  'error',
                                       'message'   =>  \Lang::get('error.notrade')
                                       ]);
            }
        }
        else
        {
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  \Lang::get('error.notrade')
                                   ]);
        }
        
        try {
            $status = $this->TransferRepository->makeMT5Process($member, $data['account'], $bookA, $bookB, $registerbalance->register_point);
          
        } catch (\Exception $e) {
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  $e->getMessage()
                                   ]);
        }
        
        \Cache::forget('member.' . $user->id);
        
       
        return \Response::json([
                               'type'  => 'success',
                               //'message' => \Lang::get('message.processMT5Success'),
                               'redirect' => route('transaction.processmt5Success', ['lang' => \App::getLocale(), 'status' => $status])
                               ]);
        
    }
    
    public function postProcessMT5PointFund () {
        $data = \Input::get('data');
        $user = \Sentinel::getUser();
        
        // $member = $user->member;
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        
     
        $book = DB::table('Member_Account_Fund')->where('id', '=', $data['account'])->first();
        
        $bookA =$book->bookA;
       
        
        try {
            $status = $this->TransferRepository->makeMT5ProcessFund($member, $bookA);
            
        } catch (\Exception $e) {
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  $e->getMessage()
                                   ]);
        }
        
        \Cache::forget('member.' . $user->id);
        
        
        return \Response::json([
                               'type'  => 'success',
                               //'message' => \Lang::get('message.processMT5Success'),
                               'redirect' => route('transaction.processmt5Success', ['lang' => \App::getLocale(), 'status' => $status])
                               ]);
        
    }
    
    public function postBuyRPoint () {
        $data = \Input::get('data');
        $user = \Sentinel::getUser();
        // $member = $user->member;
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        
        if ($member->secret_password != trim($data['s'])) {
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  \Lang::get('error.securityPasswordError')
                                   ]);
        }

        
        try {
            $this->TransferRepository->makeBuy($member, $data['amount']);
        } catch (\Exception $e) {
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  $e->getMessage()
                                   ]);
        }
        
        \Cache::forget('member.' . $user->id);
        
        
        return \Response::json([
                               'type'  => 'success',
                               'message' => \Lang::get('message.tbuysellRSuccess'),
                               'redirect' => route('transaction.buyrSuccess', ['lang' => \App::getLocale()])
                               ]);
    }
    
    public function postChangePass (Request $request) {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $SPassword = $request->SPassword;

        if ($member->secret_password != $SPassword) {
            return response()->json(['SPassword'=>"1"]);
        } 
   
        try {
            $this->TransferRepository->changePass($member, $user->password2);
        } catch (\Exception $e) {
            $tittle = "Oops...";
            $text = "";
            $html = \Lang::get('error.helpDesk');
            return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"showHtml"=>$html]);
        }
        
        \Cache::forget('member.' . $user->id);

        $tittle = \Lang::get('success.cptitle');
        $text = $user->password2;
        return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,'returnUrl'=>route('misc.fundstatement', ['lang' => \App::getLocale()])]);
        
    }
    
    public function postCalculator () {
        $data = \Input::get('data');
        
        $bookALeverage  = 100;
        $bookBLeverage = 500;
        $bookAStopout = 0.5;
        $bookBStopout = 0;
        $bookASL = 0;
        $bookATP = 0;
        $bookBSL = 0;
        $bookBTP = 0;
        $swapA = 0;
        $swapB = 0;
        
        $currencypair = $data["currencypair"];
        $lotsize = $data["lotsize"];
        $amountA = $data["amountA"];
        $amountB = $data["amountB"];
        if($data["swapA"] <> "")
            $swapA = $data["swapA"];
        if($data["swapB"] <> "")
            $swapB = $data["swapB"];
        
        $priceA = $data["priceA"];
        $priceB = $data["priceB"];
        $decision = $data["decision"];
        
        switch ($lotsize) {
            case 1:
                $lotA = 0.75;
                $lotB = 0.92;
                break;
            case 2:
                $lotA = 1.50;
                $lotB = 1.83;
                break;
            case 3:
                $lotA = 2.25;
                $lotB = 2.75;
                break;
            case 4:
                $lotA = 3.00;
                $lotB = 3.66;
                break;
            case 5:
                $lotA = 3.75;
                $lotB = 4.58;
                break;
            case 6:
                $lotA = 4.50;
                $lotB = 5.49;
                break;
            case 7:
                $lotA = 5.25;
                $lotB = 6.41;
                break;
            case 8:
                $lotA = 6.00;
                $lotB = 7.32;
                break;
            case 9:
                $lotA = 6.75;
                $lotB = 8.24;
                break;
            case 10:
                $lotA = 7.50;
                $lotB = 9.20;
                break;
                
        }
        
        if($currencypair == 'XAUUSD')
        {
            $spread = 5;
            //$bookAMargin =  round(($priceA * $lotA),2);
            //$bookBMargin = round((($bookAMargin)/5),2);
            
            //$bookAMargin =  round((1200),2);
            //$bookBMargin = round((($bookAMargin)/5),2);
            
            $bookAMargin =  1200;
            $bookBMargin = $bookAMargin/5;
            
            
            $n13 = round((($amountA + $swapA - ($bookAMargin * $bookAStopout*$lotA)) / $lotA / 10),2);
            $n5 = round((($amountB + $swapB - ($bookBMargin * $bookBStopout*$lotB)) / $lotB / 10),2);
            
           
            
            if($decision == 'BUY')
            {
                $bookASL = round(($priceA - ($n13/10) + 0.01),2);
                $bookATP = round(( $priceB + ($n5/10) - ($spread/10) - 0.2),2);
                $bookBSL = round(($priceB + ($n5/10) - 0.01),2);
                $bookBTP = round(($priceA - ($n13/10) + ($spread/10) + 0.2),2);
                
                
            }
            else
            {
                $bookASL = round(($priceA + ($n13/10) - 0.01),2);
                $bookATP = round(($priceB - ($n5/10) + ($spread/10) + 0.2),2);
                $bookBSL = round(($priceB - ($n5/10) + 0.01),2);
                $bookBTP = round(($priceA + ($n13/10) - ($spread/10) - 0.2),2);
                
              
            }
        }
        else if($currencypair == 'USDJPY' || $currencypair == 'GBPJPY' || $currencypair == 'EURJPY' || $currencypair == 'AUDJPY' || $currencypair == 'NZDJPY' || $currencypair == 'CHFJPY' || $currencypair == 'CADJPY')
        {
            $UJ = 1.06;
            
            $spread = 4;
            
            $bookAMargin =  1200;
            $bookBMargin = $bookAMargin/5;
            
            $n13 = round((($amountA + $swapA - ($bookAMargin * $bookAStopout*$lotA)) / $lotA / 10),3);
            $n5 = round((($amountB + $swapB - ($bookBMargin * $bookBStopout*$lotB)) / $lotB / 10),3);
            
            
            
            if($decision == 'BUY')
            {
                $bookASL = round(($priceA - (($n13*$UJ)/100) + 0.001),3);
                $bookATP = round(( $priceB + (($n5*$UJ)/100) - ($spread/100) - 0.02),3);
                $bookBSL = round(($priceB + (($n5*$UJ)/100) - 0.001),3);
                $bookBTP = round(($priceA - (($n13*$UJ)/100) + ($spread/100) + 0.02),3);
                
                
            }
            else
            {
                $bookASL = round(($priceA + (($n13*$UJ)/100) - 0.001),3);
                $bookATP = round(($priceB - (($n5*$UJ)/100) + ($spread/100) + 0.02),3);
                $bookBSL = round(($priceB - (($n5*$UJ)/100) + 0.001),3);
                $bookBTP = round(($priceA + (($n13*$UJ)/100) - ($spread/100) - 0.02),3);
                
                
            }
            
            
        }
        else
        {
            $spread = 3;

            
            $bookAMargin =  1200;
            $bookBMargin = $bookAMargin/5;
            
            
            $n13 = round((($amountA + $swapA - ($bookAMargin * $bookAStopout*$lotA)) / $lotA / 10),5);
            $n5 = round((($amountB + $swapB - ($bookBMargin * $bookBStopout*$lotB)) / $lotB / 10),5);
            
            
            
            if($decision == 'BUY')
            {
                $bookASL = round(($priceA - ($n13/10000) + 0.00001),5);
                $bookATP = round(($priceB + ($n5/10000) - ($spread/10000) - 0.0002),5);
                $bookBSL = round(($priceB + ($n5/10000) - 0.00001),5);
                $bookBTP = round(($priceA - ($n13/10000) + ($spread/10000) + 0.0002),5);
                
                
            }
            else
            {
                $bookASL = round(($priceA + ($n13/10000) - 0.00001),5);
                $bookATP = round(($priceB - ($n5/10000) + ($spread/10000) + 0.0002),5);
                $bookBSL = round(($priceB - ($n5/10000) + 0.00001),5);
                $bookBTP = round(($priceA + ($n13/10000) - ($spread/10000) - 0.0002),5);
                
                
            }
        }
        
        \Session::put('bookASL', $bookASL);
        \Session::put('bookATP', $bookATP);
        \Session::put('bookBSL', $bookBSL);
        \Session::put('bookBTP', $bookBTP);
        //\Session::put('n13', $n13);
        //\Session::put('n5', $n5);
        \Session::put('data', $data);
        
        return \Response::json([
                               'type'  => 'success',
                               //'message' => \Lang::get('message.tbuysellRSuccess'),
                               'redirect' => route('transaction.calculator', ['lang' => \App::getLocale()])
                               ]);
  
    }
    
    public function postSellRPoint () {
        $data = \Input::get('data');
        $user = \Sentinel::getUser();
        // $member = $user->member;
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        
        if ($member->secret_password != trim($data['s'])) {
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  \Lang::get('error.securityPasswordError')
                                   ]);
        }
        
        
        
        try {
            $this->TransferRepository->makeSell($member, $data['amount']);
        } catch (\Exception $e) {
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  $e->getMessage()
                                   ]);
        }
        
        \Cache::forget('member.' . $user->id);
        
        
        return \Response::json([
                               'type'  => 'success',
                               'message' => \Lang::get('message.tbuysellRSuccess'),
                               'redirect' => route('transaction.sellrSuccess', ['lang' => \App::getLocale()])
                               ]);
    }
    
    public function postConvertIToC (Request $request) {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $getData = $request->Data;
        $data = json_decode($getData,true); //$data["Data"][0]["getPassword1"] example
        $amount = $data["Data"][0]["amount"];
        $SPassword = $request->SPassword;

        if($member->secret_password != $SPassword) {
            return response()->json(['SPassword'=>"1"]);
        }

        if(empty($amount) || $amount <= 0){
            $tittle = \Lang::get('error.emptyAmount');
            $text = \Lang::get('error.fillAmount');
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
        }

        if($member->wallet->promotion_point < $amount) {
            $tittle = \Lang::get('error.bonusPointNotEnough');
            $text = "";
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
        }
        
        try {
            $uuid = Uuid::generate();
            $checkRequest = DB::table('Member_Transaction_Log')->where('member_id', $member->id)
                            ->where(function ($query) {
                                $query->whereNull('updated_at')
                                    ->orWhere('updated_at', '<>', \Carbon\Carbon::now());
                                })
                            ->update([ 'updated_at' => \Carbon\Carbon::now() ]);
            if(!$checkRequest){
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postConvertIToC1', 'created_at' => \Carbon\Carbon::now()]);
                $tittle = "Too Many Request...";
                $text = "";
                return response()->json(['result'=>"1","showTittle"=>$tittle]);
            }

            $updateLog = DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 0)->update(['transaction_id' => $uuid->string, 'route' => 'postConvertIToC', 'status' => 1, 'updated_at' => \Carbon\Carbon::now()]);
            if(!$updateLog){
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postConvertIToC2', 'created_at' => \Carbon\Carbon::now()]);
                $tittle = "Oops...";
                $text = "";
                $html = \Lang::get('error.helpDesk');
                return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"showHtml"=>$html]);
            }
            sleep(1);

            $checklog = DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('transaction_id', $uuid->string)->count();
            if($checklog){
                $this->TransferRepository->makeConvertIToC($member, $amount);
            }else{
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postConvertIToC3', 'created_at' => \Carbon\Carbon::now()]);
            }
            DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);
        } catch (\Exception $e) {
            DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);
            $tittle = "Oops...";
            $text = "";
            $html = \Lang::get('error.helpDesk');
            return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"showHtml"=>$html]);
        }

        \Cache::forget('member.' . $user->id);

        $tittle = \Lang::get('sidebar.incomeTitle').\Lang::get('sidebar.convertC').\Lang::get('transfer.success');
        $text = "";
        return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,'returnUrl'=>route('transaction.convertitoc', ['lang' => \App::getLocale()])]);
    }


    public function postConvertIToT (Request $request) {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $getData = $request->Data;
        $data = json_decode($getData,true); //$data["Data"][0]["getPassword1"] example
        $amount = $data["Data"][0]["amount"];
        $SPassword = $request->SPassword;

        if ($member->secret_password != $SPassword) {
            return response()->json(['SPassword'=>"1"]);
        }

        if(empty($amount) || $amount <= 0){
            $tittle = \Lang::get('error.emptyAmount');
            $text = \Lang::get('error.fillAmount');
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
        }

        if($member->wallet->promotion_point < $amount) {
            $tittle = \Lang::get('error.bonusPointNotEnough');
            $text = "";
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
        }
        
        try {
            $uuid = Uuid::generate();
            $checkRequest = DB::table('Member_Transaction_Log')->where('member_id', $member->id)
                            ->where(function ($query) {
                                $query->whereNull('updated_at')
                                    ->orWhere('updated_at', '<>', \Carbon\Carbon::now());
                                })
                            ->update([ 'updated_at' => \Carbon\Carbon::now() ]);
            if(!$checkRequest){
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postConvertIToT1', 'created_at' => \Carbon\Carbon::now()]);
                $tittle = "Too Many Request...";
                $text = "";
                return response()->json(['result'=>"1","showTittle"=>$tittle]);
            }

            $updateLog = DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 0)->update(['transaction_id' => $uuid->string, 'route' => 'postConvertIToT', 'status' => 1, 'updated_at' => \Carbon\Carbon::now()]);
            if(!$updateLog){
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postConvertIToT2', 'created_at' => \Carbon\Carbon::now()]);
                $tittle = "Oops...";
                $text = "";
                $html = \Lang::get('error.helpDesk');
                return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"showHtml"=>$html]);
            }
            sleep(1);

            $checklog = DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('transaction_id', $uuid->string)->count();
            if($checklog){
                $this->TransferRepository->makeConvertIToT($member, $amount);
            }else{
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postConvertIToT3', 'created_at' => \Carbon\Carbon::now()]);
            }
            DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);
        } catch (\Exception $e) {
            DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);
            $tittle = "Oops...";
            $text = "";
            $html = \Lang::get('error.helpDesk');
            return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"showHtml"=>$html]);
        }

        \Cache::forget('member.' . $user->id);

        $tittle = \Lang::get('sidebar.incomeTitle').\Lang::get('sidebar.convertT').\Lang::get('transfer.success');
        $text = "";
        return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,'returnUrl'=>route('transaction.convertitot', ['lang' => \App::getLocale()])]);
    }

    public function postConvertTToC (Request $request) {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $getData = $request->Data;
        $data = json_decode($getData,true); //$data["Data"][0]["getPassword1"] example
        $amount = $data["Data"][0]["amount"];
        $SPassword = $request->SPassword;

        if($member->secret_password != $SPassword) {
            return response()->json(['SPassword'=>"1"]);
        }

        if(empty($amount) || $amount <= 0){
            $tittle = \Lang::get('error.emptyAmount');
            $text = \Lang::get('error.fillAmount');
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
        }

        if($member->wallet->t_wallet < $amount) {
            $tittle = \Lang::get('sidebar.transferTitle').\Lang::get('error.notEnough');
            $text = "";
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
        }
        
        try {
            $uuid = Uuid::generate();
            $checkRequest = DB::table('Member_Transaction_Log')->where('member_id', $member->id)
                            ->where(function ($query) {
                                $query->whereNull('updated_at')
                                    ->orWhere('updated_at', '<>', \Carbon\Carbon::now());
                                })
                            ->update([ 'updated_at' => \Carbon\Carbon::now() ]);
            if(!$checkRequest){
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postConvertTToC1', 'created_at' => \Carbon\Carbon::now()]);
                $tittle = "Too Many Request...";
                $text = "";
                return response()->json(['result'=>"1","showTittle"=>$tittle]);
            }

            $updateLog = DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 0)->update(['transaction_id' => $uuid->string, 'route' => 'postConvertTToC', 'status' => 1, 'updated_at' => \Carbon\Carbon::now()]);
            if(!$updateLog){
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postConvertTToC2', 'created_at' => \Carbon\Carbon::now()]);
                $tittle = "Oops...";
                $text = "";
                $html = \Lang::get('error.helpDesk');
                return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"showHtml"=>$html]);
            }
            sleep(1);

            $checklog = DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('transaction_id', $uuid->string)->count();
            if($checklog){
                $this->TransferRepository->makeConvertTToC($member, $amount);
            }else{
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postConvertTToC3', 'created_at' => \Carbon\Carbon::now()]);
            }
            DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);
        } catch (\Exception $e) {
            DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);
            $tittle = "Oops...";
            $text = "";
            $html = \Lang::get('error.helpDesk');
            return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"showHtml"=>$html]);
        }

        \Cache::forget('member.' . $user->id);

        $tittle = \Lang::get('sidebar.transferTitle').\Lang::get('sidebar.convertC').\Lang::get('transfer.success');
        $text = "";
        return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,'returnUrl'=>route('transaction.convertttoc', ['lang' => \App::getLocale()])]);
    }

    public function postConvertWToC (Request $request) {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $getData = $request->Data;
        $data = json_decode($getData,true); //$data["Data"][0]["getPassword1"] example
        $amount = $data["Data"][0]["amount"];
        $SPassword = $request->SPassword;

        if ($member->secret_password != $SPassword) {
            return response()->json(['SPassword'=>"1"]);
        }

        if(empty($amount) || $amount <= 0){
            $tittle = \Lang::get('error.emptyAmount');
            $text = \Lang::get('error.fillAmount');
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
        }

        if ($member->wallet->w_wallet < $amount) {
            $tittle = \Lang::get('sidebar.wTitle').\Lang::get('error.notEnough');
            $text = "";
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
        }
        
        try {
            $uuid = Uuid::generate();
            $checkRequest = DB::table('Member_Transaction_Log')->where('member_id', $member->id)
                            ->where(function ($query) {
                                $query->whereNull('updated_at')
                                    ->orWhere('updated_at', '<>', \Carbon\Carbon::now());
                                })
                            ->update([ 'updated_at' => \Carbon\Carbon::now() ]);
            if(!$checkRequest){
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postConvertWToC1', 'created_at' => \Carbon\Carbon::now()]);
                $tittle = "Too Many Request...";
                $text = "";
                return response()->json(['result'=>"1","showTittle"=>$tittle]);
            }

            $updateLog = DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 0)->update(['transaction_id' => $uuid->string, 'route' => 'postConvertWToC', 'status' => 1, 'updated_at' => \Carbon\Carbon::now()]);
            if(!$updateLog){
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postConvertWToC2', 'created_at' => \Carbon\Carbon::now()]);
                $tittle = "Oops...";
                $text = "";
                $html = \Lang::get('error.helpDesk');
                return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"showHtml"=>$html]);
            }
            sleep(1);

            $checklog = DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('transaction_id', $uuid->string)->count();
            if($checklog){
                $this->TransferRepository->makeConvertWToC($member, $amount);
            }else{
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postConvertWToC3', 'created_at' => \Carbon\Carbon::now()]);
            }
            DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);
        } catch (\Exception $e) {
            DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);
            $tittle = "Oops...";
            $text = "";
            $html = \Lang::get('error.helpDesk');
            return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"showHtml"=>$html]);
        }

        \Cache::forget('member.' . $user->id);

        $tittle = \Lang::get('sidebar.wTitle').\Lang::get('sidebar.convertC').\Lang::get('transfer.success');
        $text = "";
        return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,'returnUrl'=>route('transaction.convertwtoc', ['lang' => \App::getLocale()])]);
    }

    public function postConvertWToT (Request $request) {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $getData = $request->Data;
        $data = json_decode($getData,true); //$data["Data"][0]["getPassword1"] example
        $amount = $data["Data"][0]["amount"];
        $SPassword = $request->SPassword;

        if ($member->secret_password != $SPassword) {
            return response()->json(['SPassword'=>"1"]);
        }

        if(empty($amount) || $amount <= 0){
            $tittle = \Lang::get('error.emptyAmount');
            $text = \Lang::get('error.fillAmount');
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
        }

        if($member->wallet->wp_wallet < $amount || $member->wallet->w_wallet < $amount) {
            $tittle = \Lang::get('sidebar.wTitle').\Lang::get('error.notEnough');
            $text = "";
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
        }
        
        try {
            $uuid = Uuid::generate();
            $checkRequest = DB::table('Member_Transaction_Log')->where('member_id', $member->id)
                            ->where(function ($query) {
                                $query->whereNull('updated_at')
                                    ->orWhere('updated_at', '<>', \Carbon\Carbon::now());
                                })
                            ->update([ 'updated_at' => \Carbon\Carbon::now() ]);
            if(!$checkRequest){
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postConvertWToT1', 'created_at' => \Carbon\Carbon::now()]);
                $tittle = "Too Many Request...";
                $text = "";
                return response()->json(['result'=>"1","showTittle"=>$tittle]);
            }

            $updateLog = DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 0)->update(['transaction_id' => $uuid->string, 'route' => 'postConvertWToT', 'status' => 1, 'updated_at' => \Carbon\Carbon::now()]);
            if(!$updateLog){
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postConvertWToT2', 'created_at' => \Carbon\Carbon::now()]);
                $tittle = "Oops...";
                $text = "";
                $html = \Lang::get('error.helpDesk');
                return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"showHtml"=>$html]);
            }
            sleep(1);

            $checklog = DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('transaction_id', $uuid->string)->count();
            if($checklog){
                $this->TransferRepository->makeConvertWToT($member, $amount);
            }else{
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postConvertWToT3', 'created_at' => \Carbon\Carbon::now()]);
            }
            DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);
        } catch (\Exception $e) {
            DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);
            $tittle = "Oops...";
            $text = "";
            $html = \Lang::get('error.helpDesk');
            return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"showHtml"=>$html]);
        }

        \Cache::forget('member.' . $user->id);

        $tittle = \Lang::get('sidebar.wTitle').\Lang::get('sidebar.convertT').\Lang::get('transfer.success');
        $text = "";
        return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,'returnUrl'=>route('transaction.convertwtot', ['lang' => \App::getLocale()])]);
    }
    
    public function postConvertWPoint (Request $request) {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $getData = $request->Data;
        $data = json_decode($getData,true); //$data["Data"][0]["getPassword1"] example
        $amount = $data["Data"][0]["amount"];
        $SPassword = $request->SPassword;

        if ($member->secret_password != $SPassword) {
            return response()->json(['SPassword'=>"1"]);
        }
    
        if(empty($amount) || $amount <= 0){

            $tittle = \Lang::get('error.emptyAmount');
            $text = \Lang::get('error.fillAmount');
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
        }

        if ($member->wallet->w_wallet < $amount) {
            $tittle = \Lang::get('error.wPointNotEnough');
            $text = "";
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
        }
        
        try {
            $this->TransferRepository->makeConvertW($member, $amount);
        } catch (\Exception $e) {
            $tittle = "Oops...";
            $text = "";
            $html = \Lang::get('error.helpDesk');
            return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"showHtml"=>$html]);
        }

        \Cache::forget('member.' . $user->id);
        $tittle = \Lang::get('success.wwtitle');
        $text = "";
        return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text]);
        
    }
    
    public function postConvertAPointSelf (Request $request) {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $getData = $request->Data;
        $data = json_decode($getData,true); //$data["Data"][0]["getPassword1"] example
        $amount = $data["Data"]["amount"];
        $account = $data["Data"]["account"];
        $SPassword = $request->SPassword;

        if($member->secret_password != $SPassword) {
            return response()->json(['SPassword'=>"1"]);
        }
        
        if($account == 0) {
            $tittle = \Lang::get('error.noaccount');
            $text ="";
            return response()->json(['result'=>"1","showTittle"=>$tittle]);
        }

        $ownaccount = DB::table('Member_Account_Self')->where('member_id', $member->id)->where('id', $account)->count();
        if($ownaccount == 0){
            $tittle = "Oops...";
            $text = "";
            $html = \Lang::get('error.helpDesk');
            return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"showHtml"=>$html]);
        }

        $book = DB::table('Member_Account_Self')->where('id', '=', $account)->first();
        $bookA =$book->bookA;
        $bookABalance = $this->MemberRepository->AccountAPI($bookA);
        $tradestatus = DB::connection('mysql2')->table('opentrade')->where('Login', '=', $bookA)->first();

        if(count($tradestatus) > 0) {
            $tittle = \Lang::get('error.trading');
            $text ="";
            return response()->json(['result'=>"1","showTittle"=>$tittle]);
        }

        if($amount <= 0) {
            $tittle = \Lang::get('error.bookANotEnough');
            $text ="";
            return response()->json(['result'=>"1","showTittle"=>$tittle]);
        }

        if($amount > $bookABalance->equity) {
            $tittle = \Lang::get('error.bookANotEnough');
            $text ="";
            return response()->json(['result'=>"1","showTittle"=>$tittle]);
        }

        try {
            $uuid = Uuid::generate();
            $checkRequest = DB::table('Member_Transaction_Log')->where('member_id', $member->id)
                            ->where(function ($query) {
                                $query->whereNull('updated_at')
                                    ->orWhere('updated_at', '<>', \Carbon\Carbon::now());
                                })
                            ->update([ 'updated_at' => \Carbon\Carbon::now() ]);
            if(!$checkRequest){
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postConvertAPointSelf1', 'created_at' => \Carbon\Carbon::now()]);
                $tittle = "Too Many Request...";
                $text = "";
                return response()->json(['result'=>"1","showTittle"=>$tittle]);
            }

            $updateLog = DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 0)->update(['transaction_id' => $uuid->string, 'route' => 'postConvertAPointSelf', 'status' => 1, 'updated_at' => \Carbon\Carbon::now()]);
            if(!$updateLog){
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postConvertAPointSelf2', 'created_at' => \Carbon\Carbon::now()]);
                $tittle = "Oops...";
                $text = "";
                $html = \Lang::get('error.helpDesk');
                return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"showHtml"=>$html]);
            }
            sleep(1);

            $checklog = DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('transaction_id', $uuid->string)->count();
            if($checklog){
                $this->TransferRepository->makeConvertaSelf($member, $amount, $bookA);
            }else{
                DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postConvertAPointSelf3', 'created_at' => \Carbon\Carbon::now()]);
            }
            DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);
        } catch (\Exception $e) {
            DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);
            $tittle = "Oops...";
            $text = "";
            $html = \Lang::get('error.helpDesk');
            return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"showHtml"=>$html]);
        }
        
        \Cache::forget('member.' . $user->id);
        $tittle = \Lang::get('breadcrumbs.convertSuccess');
        $text = "";
        return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,'returnUrl'=>route('misc.mt5statementself', ['lang' => \App::getLocale()])]);
    }

    public function postConvertAPoint () {
        $data = \Input::get('data');
        $user = \Sentinel::getUser();
        // $member = $user->member;
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        
        if ( $data['account'] == 0) {
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  \Lang::get('error.noaccount')
                                   ]);
        }
        
        if ($member->secret_password != trim($data['s'])) {
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  \Lang::get('error.securityPasswordError')
                                   ]);
        }
        
        $book = DB::table('Member_Account')->where('id', '=', $data['account'])->first();
        
        $bookA =$book->bookA;
        $bookB =$book->bookB;
        
        $bookABalance = $this->MemberRepository->AccountAPI($bookA);
        $bookBBalance = $this->MemberRepository->AccountAPI($bookB);
        
        //$bookABalance = $book->balanceA;
        //$bookBBalance = $book->balanceB;
        
        //$tradestatus = DB::table('temp_orders')->where('bookB', '=', $bookB)->where('status', '=', 'E')->first();
        
        
       
        
        
        if ($bookABalance->balance < 0) {
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  \Lang::get('error.processing')
                                   ]);
        }
        /*
        if (count($tradestatus) > 0) {
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  \Lang::get('error.processing2')
                                   ]);
        }
        */
        if ($data['amount'] <= 0) {
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  \Lang::get('error.bookANotEnough')
                                   ]);
        }
        
        if ($data['amount'] > $bookABalance->balance) {
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  \Lang::get('error.bookANotEnough')
                                   ]);
        }
        /*
        if (!$this->MemberRepository->getTradeStatus($bookA)) {
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  \Lang::get('error.trading')
                                   ]);
        }
        */
        
        try {
            $this->TransferRepository->makeConverta($member, $data['amount'], $bookA);
        } catch (\Exception $e) {
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  $e->getMessage()
                                   ]);
        }
        
        \Cache::forget('member.' . $user->id);
        
        
        return \Response::json([
                               'type'  => 'success',
                               'message' => \Lang::get('message.convertSuccess'),
                               'redirect' => route('transaction.convertaSuccess', ['lang' => \App::getLocale()])
                               ]);
    }

    /**
     * Transfer list - DataTable
     * @return [type] [description]
     */
    public function getList () {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        return $this->TransferRepository->getList($member);
    }
    
    public function mergeList () {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        $exceptionAry = [];
        $exception = \DB::table('Merge_List')->where('member_id', $member->id)->where('status', 0)->get();
        foreach ($exception as $item) {
            $data = explode(',', $item->merge_from);
            foreach ($data as $item) {
                $exceptionAry[] = $item;
            }
        }

        $accounts = \DB::table('Member_Account_Fund')->where('member_id', $member->id)->whereNotIn('bookA', $exceptionAry)->where('balanceB', '<>', 0)->count();
        if($accounts > 1) $mergeAccountDisplay = true;
        else $mergeAccountDisplay = false;

        $data = [ 'mergeAccountDisplay' => $mergeAccountDisplay ];

        return view('front.transaction.mergeList')->with($data);
    }

    public function getMergeList () {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        return $this->TransferRepository->getMergeList(true, $member->id);
    }

    public function mergeFund () {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        $exceptionAry = [];
        $exception = \DB::table('Merge_List')->where('member_id', $member->id)->where('status', 0)->get();
        foreach ($exception as $item) {
            $data = explode(',', $item->merge_from);
            foreach ($data as $item) {
                $exceptionAry[] = $item;
            }
        }

        $accounts = \DB::table('Member_Account_Fund')->where('member_id', $member->id)->whereNotIn('bookA', $exceptionAry)->where('balanceB', '<>', 0)->get();
        
        $temp = [];
        foreach ($accounts as $item) {
            $temp[$item->bookA] = \Lang::get('transfer.a.wallet').': '.$item->bookA.' '.\Lang::get('transfer.balance').': $'.number_format($item->rbalance,2).' ; '.\Lang::get('transfer.b.wallet').': '.$item->bookB.' '.\Lang::get('transfer.balance').': $'.number_format($item->balanceB,2);
        }

        return view('front.transaction.mergeFund')->with('model', $temp);
    }

    public function postMergeFund (Request $request) {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        $SPassword = $request->SPassword;
        $getData = $request->Data;
        $data = json_decode($getData,true); //$data["Data"]["getPassword1"] example
        
        if ($member->secret_password != $SPassword) {
            $response = array('SPassword' => 1);
            return response()->json($response);
        }

        if (empty($data['Data']['account'])) {
            $response = array('result' => 1, 'showTittle' => \Lang::get('merge.pleaseselect').''.\Lang::get('merge.mergeaccount').' ('.\Lang::get('merge.from').')', 'showText' => '');
            return response()->json($response);
        }

        if (empty($data['Data']['accountto'])) {
            $response = array('result' => 1, 'showTittle' => \Lang::get('merge.pleaseselect').''.\Lang::get('merge.mergeaccount').' ('.\Lang::get('merge.to').')', 'showText' => '');
            return response()->json($response);
        }

        $accountmerge = explode(',', $data['Data']['account']);

        $accounts = \DB::table('Member_Account_Fund')->where('member_id', $member->id)->whereIn('bookA', $accountmerge)->where('balanceB', '<>', 0)->get();

        if(count($accountmerge) != count($accounts)){
            $response = array('result' => 1, 'showTittle' => 'Please do not try to hack us :)', 'showText' => '');
            return response()->json($response);
        }

        $totalMerge = 0;
        foreach ($accounts as $item) {
            $totalMerge = $totalMerge + $item->balanceB;
        }

        if($totalMerge > 10000){
            $response = array('result' => 1, 'showTittle' => \Lang::get('merge.error1'), 'showText' => '');
            return response()->json($response);
        }

        if(isset($data['Data']['id'])) \DB::table('Merge_List')->where('id', $data['Data']['id'])->where('member_id', $member->id)->update(['merge_from' => $data['Data']['account'], 'merge_to' => $data['Data']['accountto'], 'updated_at' => \Carbon\Carbon::now()]);
        else \DB::table('Merge_List')->insert(['member_id' => $member->id, 'merge_from' => $data['Data']['account'], 'merge_to' => $data['Data']['accountto'], 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);

        // try {
        //     $this->TransferRepository->makeMergeFund($member, $data['Data']['account']);
        // } catch (\Exception $e) {
        //     $response = array('result' => 1, 'showTittle' => $e->getMessage(), 'showText' => '');
        //     return response()->json($response);
        // }

        $response = array('result' => 0, 'showTittle' => \Lang::get('message.mergeSuccess'), 'showText' => '', 'returnUrl' => route('mergelist', ['lang' => \App::getLocale()]));
        return response()->json($response);
    }

    public function editMergeFund () {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $id = \Input::get('id');

        $exceptionAry = [];
        $exception = \DB::table('Merge_List')->where('id', '<>', $id)->where('member_id', $member->id)->where('status', 0)->get();
        foreach ($exception as $item) {
            $data = explode(',', $item->merge_from);
            foreach ($data as $item) {
                $exceptionAry[] = $item;
            }
        }

        $accounts = \DB::table('Member_Account_Fund')->where('member_id', $member->id)->whereNotIn('bookA', $exceptionAry)->where('balanceB', '<>', 0)->get();
        
        $temp = [];
        foreach ($accounts as $item) {
            $temp[$item->bookA] = \Lang::get('transfer.a.wallet').': '.$item->bookA.' '.\Lang::get('transfer.balance').': $'.number_format($item->rbalance,2).' ; '.\Lang::get('transfer.b.wallet').': '.$item->bookB.' '.\Lang::get('transfer.balance').': $'.number_format($item->balanceB,2);
        }

        $selected = \DB::table('Merge_List')->where('id', $id)->where('member_id', $member->id)->whereIn('status', [0,2])->first();
        if(isset($selected)) {
            $selectedMerge = $selected->merge_from;
            $selectedMergeTo = $selected->merge_to;
        }

        $data = [ 
                    'id' => $id,
                    'selectedMerge' => $selectedMerge,
                    'selectedMergeTo' => $selectedMergeTo,
                    'model' => $temp
                ];

        return view('front.transaction.mergeFundEdit')->with($data);
    }

    public function cancelFund (Request $request) {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        \DB::table('Merge_List')->where('member_id', $member->id)->where('id', $request->id)->update(['status' => 3]);

        $response = array('status' => 0, 'msg' => \Lang::get('merge.cancelsuccess'));
        return response()->json($response);
    }
}
