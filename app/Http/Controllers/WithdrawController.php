<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\WithdrawRepository;
use App\Repositories\MemberRepository;
use App\Repositories\BankInfoRepository;
use App\Repositories\DepositRepository;
use App\Services\CheckingService;
use Carbon\Carbon;

class WithdrawController extends Controller
{
    protected $WithdrawRepository;
    protected $MemberRepository;
    protected $BankInfoRepository;
    protected $CheckingService;

    public function __construct(WithdrawRepository $WithdrawRepository,MemberRepository $MemberRepository,
        BankInfoRepository $BankInfoRepository,DepositRepository $DepositRepository,CheckingService $CheckingService) {
        $this->WithdrawRepository = $WithdrawRepository;
        $this->MemberRepository = $MemberRepository;
        $this->BankInfoRepository = $BankInfoRepository;
        $this->DepositRepository = $DepositRepository;
        $this->CheckingService = $CheckingService;
        $this->middleware('member');
    }

    /**
     * Member Withdraw Cash Point
     * @return [type] [description]
     */
    public function getWithdrawal () {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername($user->username);
        $bankList = $this->BankInfoRepository->findBankInfo($member->id,1);
        return view('front.withdrawal.withdrawal')->with('bankList',$bankList)->with('user',$user)->with('member',$member);
    }

    public function getBankData (Request $request) {
        $id = $request->id;
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername($user->username);
        $detail = $member->detail;

        if(!$bankInfo = $this->BankInfoRepository->findByNoSelf($id,$member->id)){
            $tittle = \Lang::get('withdraw.AccFail');
            $text = "";
            $returnUrl = "";
            return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"returnUrl"=>$returnUrl]);

        }

        $bankData = $bankInfo;
        if($detail->nationality == "Vietnam1" || $detail->nationality == "Thailand1"){
            
        }else{
            $language = \App::getLocale();
            if($language == 'chs'){

                $language = 'zh-cn';
            }else{
                $language = 'en-us';
            }

            $countryName = $bankInfo->countryName;
            $stateName = $bankInfo->stateName;
            $cityName = $bankInfo->cityName;

            if(!$bankData->countryName = \DB::table('country')->where('countrycode',$countryName)->where('languagecode',$language)->value('countryname')){
                $bankData->countryName = $countryName;
            }
            if(!$bankData->stateName = \DB::table('state')->where('countrycode',$countryName)->where('statecode',$stateName)->where('languagecode',$language)->value('statename')){
                if(!$bankData->stateName  = \DB::table('city')->where('statecode',$stateName)->where('citycode',$cityName)->where('languagecode',$language)->value('cityname')){
                    $bankData->stateName = \DB::table('country')->where('countrycode',$countryName)->where('languagecode',$language)->value('countryname');
                }
            }
            if(!$bankData->cityName  = \DB::table('city')->where('statecode',$stateName)->where('citycode',$cityName)->where('languagecode',$language)->value('cityname')){
                $bankData->cityName  = \DB::table('country')->where('countrycode',$countryName)->where('languagecode',$language)->value('countryname');
            }
        }
        $bankData = json_encode($bankData);
        $tittle = \Lang::get('withdraw.addBanksuc');
        $text = \Lang::get('withdraw.goWithdrawal');
        return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,"bankData"=>$bankData]);
    }
    public function getBankDataAsia (Request $request) {
        $id = $request->id;
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername($user->username);
        if(!$bankInfo = $this->BankInfoRepository->findByNoSelf($id,$member->id)){
            $tittle = \Lang::get('withdraw.AccFail');
            $text = "";
            $returnUrl = "";
            return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"returnUrl"=>$returnUrl]);

        }
        $bankData = $bankInfo;
        $bankData = json_encode($bankData);
        $tittle = \Lang::get('withdraw.addBanksuc');
        $text = \Lang::get('withdraw.goWithdrawal');
        return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,"bankData"=>$bankData]);
    }

    public function postCreate (Request $request){
        $SPassword = $request->SPassword;
        $getData = $request->Data;

        $data = json_decode($getData,true); //$data["Data"]["getPassword1"] example
        $amount = $data["Data"]["amount"];
        $bank_id = $data["Data"]["bank_id"];
        $remark = $data["Data"]["remark"];

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $detail = $member->detail;

        if ($member->secret_password != $SPassword) {
            return response()->json(['SPassword'=>"1"]);
        }
        if($amount <= 0){
            $tittle = \Lang::get('withdraw.amountless');
            $text = "";
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
        }

        if($user->allow_withdrawal != "1"){

            $GetCompleted = $this->DepositRepository->CheckWithdrawStatus($member->id,2); // Check Completed
            $GetProcess = $this->DepositRepository->CheckWithdrawStatus($member->id,8); // Check Completed
            $Total = $GetCompleted + $GetProcess;
            
            if( $Total >= 2){
                $tittle = \Lang::get('withdraw.withdrawWarning');
                $text = \Lang::get('withdraw.withdraw2time');
                return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"returnUrl"=>""]);
            }

            if($amount < 150){
                $tittle = \Lang::get('withdraw.amountless');
                $text = "";
                return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);

            }
        }

        if(!$bankInfo = $this->BankInfoRepository->findByNoSelf($bank_id,$member->id)){
            $tittle = \Lang::get('withdraw.AccFail');
            $text = "";
            $returnUrl = "";
            return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"returnUrl"=>$returnUrl]);
        }

        if(!empty($data["Data"]["swift"]) || !empty($data["Data"]["bank_account_holder"])){
            $swift = $data["Data"]["swift"];
            $bank_account_holder = $data["Data"]["bank_account_holder"];

            $bankInfo->swift = $swift;
            $bankInfo->bank_account_holder = $bank_account_holder;
            $bankInfo->save();
        }



        if ($member->wallet->cash_point < $amount) {
            $tittle = \Lang::get('error.cashPointNotEnough');
            $text = "";
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
        }

        if($checkNewApp = $this->DepositRepository->CheckNewApp($member->id)){
            $tittle = \Lang::get('withdraw.waitting');
            $text = "";
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
        }

        $bankData = $bankInfo;
        if($detail->nationality == "Vietnam1"){

            $insertData["currency_new"] = 'VND';

        }elseif($detail->nationality == "Thailand1"){

            $insertData["currency_new"] = 'THB';

        }else{

            $language = \App::getLocale();
            if($language == 'chs'){
                $language = 'zh-cn';
            }else{
                $language = 'en-us';
            }

            
            $countryName = $bankInfo->countryName;
            $stateName = $bankInfo->stateName;
            $cityName = $bankInfo->cityName;

            $bankData->countryName = \DB::table('country')->where('countrycode',$countryName)->where('languagecode',$language)->value('countryname');
            if(!$bankData->stateName = \DB::table('state')->where('countrycode',$countryName)->where('statecode',$stateName)->where('languagecode',$language)->value('statename')){
                if(!$bankData->stateName  = \DB::table('city')->where('statecode',$stateName)->where('citycode',$cityName)->where('languagecode',$language)->value('cityname')){
                    $bankData->stateName = \DB::table('country')->where('countrycode',$countryName)->where('languagecode',$language)->value('countryname');
                }
            }
            if(!$bankData->cityName  = \DB::table('city')->where('statecode',$stateName)->where('citycode',$cityName)->where('languagecode',$language)->value('cityname')){
                $bankData->cityName  = \DB::table('country')->where('countrycode',$countryName)->where('languagecode',$language)->value('countryname');
            }

        }

        $insertData["TransID"] = $member->id."_W". date('ymd').uniqid();
        $insertData["type"] = 2;
        $insertData["OrderMoney"] = 0;
        $insertData["fundmoney"] = $amount;
        $insertData["wid"] = $member->id;
        $insertData["login"] = 0;
        $insertData["created"] = Carbon::now()->addHours(8)->timestamp;
        $insertData["paybank"] = 0;
        $insertData["paybankcode"] = $bankData->bank_code;
        $insertData["memo"] = $remark;
        $insertData["status"] = 1;
        $insertData["deal_time"] = Carbon::now();
        $insertData["cname"] = $user->first_name;
        $insertData["optIp"] = $request->ip();

        if(!$GetPayId = $this->DepositRepository->storeDeposit($insertData)){

            $tittle = \Lang::get('withdraw.insertLogFail');
            $text = "";
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
        }

        $insertPayData["PayId"] = $GetPayId->id;
        $insertPayData["card_no"] = $bankData->bank_no;
        $insertPayData["bank_Name"] = $bankData->bank_name;
        $insertPayData["bank_address"] = $bankData->bank_address;
        $insertPayData["bank_swift"] = $bankData->swift;
        $insertPayData["zbank_name"] = $bankData->sub_bank;
        $insertPayData["bank_memo"] = $remark;
        $insertPayData["rec_Name"] = $user->first_name;
        $insertPayData["Countries"] = $bankData->countryName;
        $insertPayData["Province"] = $bankData->stateName;
        $insertPayData["City"] = $bankData->cityName;
        $insertPayData["WithdrawalMethod"] = $bankData->withdraval_type;
        $insertPayData["exchangeRate"] = 0;

        
        if(!$model = $this->DepositRepository->storePay($insertPayData)){
            $tittle = \Lang::get('withdraw.insertLogFail');
            $text = "";
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
        }

        $tittle = \Lang::get('withdraw.withdrawSubmitted');
        $text = \Lang::get('withdraw.pendingWithdraw');

        if($detail->nationality == 'Vietnam1' || $detail->nationality == 'Thailand1'){
            $returnUrl = route('withdraw.Asiarecords', ['lang' => \App::getLocale()]);
        }else{
            $returnUrl = route('withdraw.records', ['lang' => \App::getLocale()]);
        }
        return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,"returnUrl"=>$returnUrl]);
    }

    public function getWithdrawRecord () {
        
        return view('front.withdrawal.withdrawalRecord');
    }
    public function getWithdrawAsiaRecord () {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $detail = $member->detail;
        if($detail->nationality == 'Vietnam1' || $detail->nationality == 'Thailand1'){
            return view('front.withdrawal.withdrawAsiaSystemRecord');
        }else{
            return view('front.withdrawal.withdrawAsiaRecord');
        }
       
    }

    public function WithdrawalRecords () {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $detail = $member->detail;
        return $this->DepositRepository->getWithdrawList(2,$member->id,'CNY');
    }
    public function withdrawAsiaRecords () {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        return $this->WithdrawRepository->getWithdrawList($member->id);

    }
    public function withdrawAsiaSystemRecords () {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $detail = $member->detail;
        if($detail->nationality == 'Vietnam1'){
            return $this->WithdrawRepository->getWithdrawSystemList($member->id);
            //return $this->DepositRepository->getWithdrawList(2,$member->id,'VND');
        }else if($detail->nationality == 'Thailand1'){
            return $this->DepositRepository->getWithdrawList(2,$member->id,'THB');
        }
    }

    public function cancel (Request $request) {

        $payID = $request->id;
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        if(!$model = $this->DepositRepository->findById($payID,$member->id)){
            $result = "fail";
            return response()->json(['model'=>$model,'result'=>$result]);
        }
        if ($model->type == 2)
        {
            if ($model->status != 1 && $model->status != 5 && $model->status != 6){
                $result = "fail";
                return response()->json(['model'=>$model,'result'=>$result]);
            }
            else{
                if(!$ans = $this->DepositRepository->cancelWithdraw($payID,$member->id)){
                    $result = "fail";
                    return response()->json(['model'=>$model,'result'=>$result]);
                }
                $result = "sucess";
                $UserName = $user->username;
                $OperateLog ="Update transaction status to CancelledOrder";
                $OperateTime = Carbon::now();
                $MemberId = $member->id;
                $TransId = $model->TransID;
                $TransMemo = "";
                $RunSql ="update web_deposit_log SET web_deposit_log.status=3 WHERE web_deposit_log.TransID=".$TransId;
                \DB::table('user_optlog')->insert(
                    [
                        'adminname' => $UserName, 
                        'adminmemo' => $OperateLog,
                        'admintime' => $OperateTime,
                        'record_sql' => $RunSql,
                        'memberId' => $MemberId,
                        'TransID' => $TransId,
                        'transmemo' => $TransMemo,
                    ]
                );

            }
        }
        return response()->json(['model'=>$model,'result'=>$result]);
        
    }






    // ==================================================JACKY=================================//
    public function postCreateAsia (Request $request){
        $SPassword = $request->SPassword;
        $getData = $request->Data;

        $data = json_decode($getData,true); //$data["Data"]["getPassword1"] example
        $type = $data["Data"]["type"];
        $amount = $data["Data"]["amount"];
        $bank_id = $data["Data"]["bank_id"];
        $vm_cardname = $data["Data"]["vm_cardname"];
        $vm_cardname2 = $data["Data"]["vm_cardname2"];
        $vm_country = $data["Data"]["vm_country"];


        
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        if ($member->secret_password != $SPassword) {
            return response()->json(['SPassword'=>"1"]);
        }

        if($amount < 150){
            $tittle = \Lang::get('withdraw.amountless50');
            $text = "";
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);

        }
        if ($member->wallet->cash_point < $amount) {
                $tittle = \Lang::get('error.cashPointNotEnough');
                $text = "";
                return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
        }

        if($type =="bank"){

            

            if(!$bankInfo = $this->BankInfoRepository->findByNoSelf($bank_id,$member->id)){
                $tittle = \Lang::get('withdraw.AccFail');
                $text = "";
                $returnUrl = "";
                return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"returnUrl"=>$returnUrl]);
            }
            if(!empty($data["Data"]["swift"]) || !empty($data["Data"]["bank_account_holder"])){
                $swift = $data["Data"]["swift"];
                $bank_account_holder = $data["Data"]["bank_account_holder"];
                $bankInfo->swift = $swift;
                $bankInfo->bank_account_holder = $bank_account_holder;
                $bankInfo->save();
            }

            $this->WithdrawRepository->makeWithdraw($member,$bankInfo,$amount,$type,'');
            $tittle = \Lang::get('message.withdrawSuccess');
            $text = \Lang::get('success.wttext2');
            $returnUrl = route('withdraw.Asiarecords', ['lang' => \App::getLocale()]);
            return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,"returnUrl"=>$returnUrl]);

        }else if($type=="debit"){

            if (empty($vm_cardname) || empty($vm_country) ){

                $tittle = \Lang::get('error.debitError');
                $text = "";
                $returnUrl = "";
                return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text,"returnUrl"=>$returnUrl]);

            }else{

                $debit_data = ['card_number' => $vm_cardname,'card_country'=>$vm_country];
                $this->WithdrawRepository->makeWithdraw($member,'',$amount ,$type,$debit_data);
                $tittle = \Lang::get('message.withdrawSuccess');
                $text = \Lang::get('success.wttext2');
                $returnUrl = route('withdraw.Asiarecords', ['lang' => \App::getLocale()]);
                return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,"returnUrl"=>$returnUrl]);
               
            }

        }
    }


    public function getwithdrawPage () {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername($user->username);
        $detail = $member->detail;

        if($detail->nationality == 'Vietnam1'){
            $bankList = $this->BankInfoRepository->findBankInfo($member->id,98);
            $bankListAsia = $this->BankInfoRepository->findBankInfo($member->id,2);
            return view('front.withdrawal.withdrawAsiaSystem')->with('bankList',$bankList)->with('user',$user)->with('member',$member)->with('bankListAsia',$bankListAsia);
        }
        else if($detail->nationality == 'Thailand1'){
            $bankList = $this->BankInfoRepository->findBankInfo($member->id,97);
            return view('front.withdrawal.withdrawAsiaSystem')->with('bankList',$bankList)->with('user',$user)->with('member',$member);
        }else{
            $bankList = $this->BankInfoRepository->findBankInfo($member->id,2);
            return view('front.withdrawal.withdrawAsia')->with('bankList',$bankList)->with('user',$user)->with('member',$member);
        }
        
    }

    public function postMakeWithdraw () { //weilun

        $data = \Input::get('data');
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        if ($member->secret_password != trim($data['s'])) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  \Lang::get('error.securityPasswordError')
            ]);
        }


        if($data["payment_type"]=="bank"){

            if (empty($data["bank_name"]) || empty($data["bank_account_holder"]) || empty($data["bank_account_number"])
                || empty($data["bank_swiftcode"])|| empty($data["bank_address"])|| empty($data["bank_country"])){

                return \Response::json([
                    'type'  =>  'error',
                    'message'   =>  \Lang::get('error.bankError')
                ]);

            }else{

                $detail = $member->detail; 
                foreach ($detail->getAttributes() as $k => $d) {
                    if (isset($data[$k])) $detail->{$k} = $data[$k];
                }
                $detail->save(); //update tb_member_detail bank info

                try {   
                    $this->WithdrawRepository->makeWithdraw($member, $data['amount'] ,$data["payment_type"],'');
                } catch (\Exception $e) {
                    return \Response::json([
                        'type'  =>  'error',
                        'message'   =>  $e->getMessage()
                    ]);
                }

            }

        }else if($data["payment_type"]=="debit"){

                

            if (empty($data["v_cardname"]) || empty($data["v_country"]) ){


                return \Response::json([
                    'type'  =>  'error',
                    'message'   =>  \Lang::get('error.debitError')
                ]);

            }else{

                $debit_data = ['card_number' => $data["v_cardname"],'card_country'=>$data["v_country"]];


                try {   
                    $this->WithdrawRepository->makeWithdraw($member, $data['amount'] ,$data["payment_type"], $debit_data);
                } catch (\Exception $e) {
                    return \Response::json([
                        'type'  =>  'error',
                        'message'   =>  $e->getMessage()
                    ]);
                }
            }

        }else{

            return \Response::json([
                    'type'  =>  'error',
                    'message'   => \Lang::get('error.paymentError')
                ]);

        }

        

        

        

        \Cache::forget('member.' . $user->id);

        return \Response::json([
            'type'  =>  'success',
            'message'   =>  \Lang::get('message.withdrawSuccess'),
            'redirect' => route('transaction.withdrawSuccess', ['lang' => \App::getLocale()])
           ]);

    }

    /**
     * Withdraw list - DataTable
     * @return [type] [description]
     */
    public function getList () {
        $user = \Sentinel::getUser();
        $member = $user->member;
        return $this->WithdrawRepository->getList($member);
    }
}
