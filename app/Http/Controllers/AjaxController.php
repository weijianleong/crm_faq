<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UploadpicService;
use App\Repositories\HelpdeskRepository;
use App\Repositories\HelpdeskCommentRepository;
use App\Repositories\MemberRepository;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class AjaxController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $HelpdeskRepository;
    protected $HelpdeskCommentRepository;
    protected $MemberRepository;
    protected $UploadpicService;

    public function __construct(HelpdeskRepository $HelpdeskRepository,MemberRepository $MemberRepository,HelpdeskCommentRepository $HelpdeskCommentRepository ,UploadpicService $UploadpicService) {

        
        $this->HelpdeskRepository = $HelpdeskRepository;
        $this->HelpdeskCommentRepository = $HelpdeskCommentRepository;
        $this->MemberRepository = $MemberRepository;
        $this->UploadpicService = $UploadpicService;

        $this->middleware(function ($request, $next) {
            if (\Sentinel::check()) {
                $user = \Sentinel::getUser();
                \View::share(['user' => $user]);
          /*
                $permissions = $user->permissions;
                if (isset($permissions['member'])) {
                    if (!\Cache::has('member.' . $user->id)) {
                          $member = $user;
                        //$member = $user->member;
                        //$member = $this->MemberRepository->findById(trim($user->id));
                        //$member->load('wallet');
                        //$member->load('detail');
                        //$member->load('shares');
                        \Cache::put('member.' . $user->id, $member, 60);
                    } else {
                        $member = \Cache::get('member.'. $user->id);
                    }
                    \View::share(['member' => $member]);
                }
           */
            }
            return $next($request);
        });
    }

    public function getaes(){
        
        $money = 10000000;

        $website_data = \DB::table('admin_website')->where('max_amount','<=', $money)->get();

          foreach ($website_data as $key ) {
            $get_amount = $key->max_amount - ($key->max_amount /50*10);
            $cash_now = $key->cash_now;

            if($get_amount >= $cash_now){
              $website_id = $key->id;
              break;
            }

          }
          if(empty($website_id)){
            $website_id = 0;
          }

          $user = \Sentinel::getUser();
          $username_page = $user->email;
          $password_page = $user->password2;
          $loginip = request()->ip();
          $sign = date("Y-m-d H:i:s");

          $ekey = 'trealcappassword';
          $eiv = 'trealcappassword';
          $emethod = 'aes128';
          $epass = openssl_encrypt($password_page, $emethod, $ekey, null, $eiv);

          $topup_word = '{username^'.$username_page.'|password^'.$epass.'|loginip^'.$loginip.'|deposittype^addvalue'.'|sign^'.$sign.'|website_id^'.$website_id.'}';
          $mating_word = '{username^'.$username_page.'|password^'.$epass.'|loginip^'.$loginip.'|deposittype^mating'.'|sign^'.$sign.'|website_id^'.$website_id.'}';


          $pass = 'Trealcap_learing';
          $method = 'aes128';
          $iv = 'Trealcap_learing';
          $topup_data = openssl_encrypt($topup_word, $method, $pass, null, $iv);
          $mating_data = openssl_encrypt($mating_word, $method, $pass, null, $iv);
          return response()->json(array('top' => $topup_data , 'mat'=>$mating_data));
    }

    public function search_email(request $request){
      
          $username = $request->username;

          $user = $this->MemberRepository->findByUsername($username);

          $first_name = \DB::table('users')->where('username', '=', $username)->first(['first_name']);
     
          if (!$user) {
              return \Response::json(array("error" => "response was not JSON"));
          }

          return response()->json(array('email' => $username, 'first_name' => $first_name->first_name));
    }

    public function set_session(request $request){
      
          $get_date = $request->get_date;
          session(['personal_date' => $get_date]);
    }
}
