<?php

namespace App\Http\Controllers;

use App\Models\Faq;
use Illuminate\Http\Request;
use App\Services\UploadpicService;
use App\Repositories\HelpdeskRepository;
use App\Repositories\HelpdeskCommentRepository;
use App\Repositories\MemberRepository;
use Carbon\Carbon;
use App\Services\AmazonS3;
use Illuminate\Support\Facades\App;
use Validator;

class HelpdeskController extends Controller
{
    /**
     * The AnnouncementRepository instance.
     *
     * @var \App\Repositories\AnnouncementRepository
     */
    protected $HelpdeskRepository;
    protected $HelpdeskCommentRepository;
    protected $MemberRepository;
    protected $UploadpicService;
    protected $AmazonS3;

    /**
     * Create a new AnnouncementController instance.
     *
     * @param \App\Repositories\AnnouncementRepository $AnnouncementRepository
     *
     * @return void
     */
    public function __construct(HelpdeskRepository $HelpdeskRepository, MemberRepository $MemberRepository, UploadpicService $UploadpicService, HelpdeskCommentRepository $HelpdeskCommentRepository, AmazonS3 $AmazonS3)
    {
        parent::__construct();
        $this->HelpdeskRepository = $HelpdeskRepository;
        $this->HelpdeskCommentRepository = $HelpdeskCommentRepository;
        $this->MemberRepository = $MemberRepository;
        $this->UploadpicService = $UploadpicService;
        $this->AmazonS3 = $AmazonS3;
        $this->middleware('member');
    }

    /**
     * Datatable List
     *
     * @return [type] [description]
     */
    public function getList()
    {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername($user->username);
        return $this->HelpdeskRepository->getList($member->id);
    }

    /**
     * Announcement List
     *
     * @return html
     */
    public function getAll()
    {
        return view('front.helpdesk.list');
    }

    public function addDesk(Request $request)
    {

        $SPassword = $request->SPassword;
        $getData = $request->allData;
        $data_js = json_decode($getData, true); //$data["Data"]["getPassword1"] example
        $type = $data_js["Data"]["HType"];
        $tittle = $data_js["Data"]["QsTitle"];
        $content = $data_js["Data"]["QsContent"];
        $user = \Sentinel::getUser();
        //$member = $user->member;
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        if (empty($type) || empty($tittle) || empty($content)) {

            $tittle = \Lang::get('helpdesk.errorAdd');
            $text = \Lang::get('helpdesk.errorAddtext');
            //$returnUrl = route('helpdesk.list', ['lang' => \App::getLocale()]);
            return response()->json(['result' => "1", "showTittle" => $tittle, "showText" => $text]);

        }

        $get_DeskNum = $this->HelpdeskRepository->get_DeskNum($member->id);
        if ($get_DeskNum >= 2) {


            $tittle = \Lang::get('helpdesk.errorAdd5');
            $text = "";
            $returnUrl = route('helpdesk.list', ['lang' => \App::getLocale()]);
            return response()->json(['result' => "1", "showTittle" => $tittle, "showText" => $text, "returnUrl" => $returnUrl]);

        }


        if (!empty(\Input::file('image1'))) {
            $image1 = \Input::file('image1');

            $checking = $this->UploadpicService->CheckFileSupported($image1);
            if (empty($checking)) {
                $tittle = \Lang::get('error.uploadProfileFail');
                $text = "";
                $returnUrl = "";
                return response()->json(['result' => "1", "showTittle" => $tittle, "showText" => $text, "returnUrl" => $returnUrl]);
            }

            $idFront_path = $image1;
            $filename1 = $this->AmazonS3->uploadImage($idFront_path, $user, 'helpdesk', uniqid());
            // $filename1 = $this->UploadpicService->helpdesk_upload($idFront_path);
            $data['pic_1'] = $filename1;
        }

        if (!empty(\Input::file('image2'))) {
            $image2 = \Input::file('image2');

            $checking = $this->UploadpicService->CheckFileSupported($image2);
            if (empty($checking)) {
                $tittle = \Lang::get('error.uploadProfileFail');
                $text = "";
                $returnUrl = "";
                return response()->json(['result' => "1", "showTittle" => $tittle, "showText" => $text, "returnUrl" => $returnUrl]);
            }

            $idFront_path = $image2;
            $filename2 = $this->AmazonS3->uploadImage($idFront_path, $user, 'helpdesk', uniqid());
            // $filename2 = $this->UploadpicService->helpdesk_upload($idFront_path);
            $data['pic_2'] = $filename2;
        }

        $data['type'] = $type;
        $data['tittle'] = $tittle;
        $data['content'] = $content;
        $data['user_id'] = $member->id;
        $data['username'] = $member->username;
        $data['status'] = "process";
        $data['for_cs_type'] = $data['type'];
        $save['user_id'] = $data['user_id']; //for refrence user id


        $check_today = date('Y-m-d') . "%";
        $check_refrence = $this->HelpdeskRepository->check_refrence($check_today);

        if ($check_refrence == 0) { //if today is the 1st ticket

            $save["id"] = date('ymd' . "0001");
            $data['refrence_id'] = $this->HelpdeskRepository->get_refrence($save);

        } else {

            $data['refrence_id'] = $this->HelpdeskRepository->get_refrence($save);
        }

        $this->HelpdeskRepository->store($data);

        $now = Carbon::now();
        $check_morning = Carbon::create($now->year, $now->month, $now->day, 00, 00, 00);
        $check_eleven = Carbon::create($now->year, $now->month, $now->day, 10, 30, 00);
        $check_six = Carbon::create($now->year, $now->month, $now->day, 18, 30, 00);
        $check_night = Carbon::create($now->year, $now->month, $now->day, 20, 30, 00);
        $break_foreach = 0;
        $gotCsID = "";
        $Handle_model = $this->HelpdeskRepository->getHandleId($member->id);

        if (count($Handle_model) <= 0) {

            if ($now->greaterThan($check_morning) && $now->lessThan($check_eleven)) {
                $getDate = Carbon::now()->format('ymd');
                $shift = 1;
                $this->HelpdeskRepository->autoTicketShift($data, $getDate, $shift);

            } else if ($now->greaterThan($check_eleven) && $now->lessThan($check_six)) {
                $getDate = Carbon::now()->format('ymd');
                $this->HelpdeskRepository->autoTicket($data, $getDate);

            } else if ($now->greaterThan($check_six) && $now->lessThan($check_night)) {
                $getDate = Carbon::now()->format('ymd');
                $shift = 2;
                $this->HelpdeskRepository->autoTicketShift($data, $getDate, $shift);
            } else {
                $getDate = Carbon::now()->addDay()->format('ymd');
                $shift = 1;
                $this->HelpdeskRepository->autoTicketShift($data, $getDate, $shift);
            }

        } else {


            foreach ($Handle_model as $Handle_models) {

                if ($now->greaterThan($check_morning) && $now->lessThan($check_eleven)) {
                    $getDate = Carbon::now()->format('ymd');
                    $shift = 1;


                } else if ($now->greaterThan($check_eleven) && $now->lessThan($check_six)) {
                    $getDate = Carbon::now()->format('ymd');
                    $shift = 3;


                } else if ($now->greaterThan($check_six) && $now->lessThan($check_night)) {
                    $getDate = Carbon::now()->format('ymd');
                    $shift = 2;

                } else {
                    $getDate = Carbon::now()->addDay()->format('ymd');
                    $shift = 1;

                }

                if ($gotCsID = $this->HelpdeskRepository->checkSchule($Handle_models->cs_id, $getDate, $shift)) {
                    $this->HelpdeskRepository->moveTicket($member->id, $gotCsID);
                    $this->HelpdeskRepository->addTicketNum($gotCsID, $getDate);
                    $break_foreach = 1;
                    break;
                }
            }

            if ($break_foreach == 0) {

                if ($now->greaterThan($check_morning) && $now->lessThan($check_eleven)) {
                    $getDate = Carbon::now()->format('ymd');
                    $shift = 1;
                    $this->HelpdeskRepository->autoTicketShift($data, $getDate, $shift);

                } else if ($now->greaterThan($check_eleven) && $now->lessThan($check_six)) {
                    $getDate = Carbon::now()->format('ymd');
                    $this->HelpdeskRepository->autoTicket($data, $getDate);

                } else if ($now->greaterThan($check_six) && $now->lessThan($check_night)) {
                    $getDate = Carbon::now()->format('ymd');
                    $shift = 2;
                    $this->HelpdeskRepository->autoTicketShift($data, $getDate, $shift);
                } else {
                    $getDate = Carbon::now()->addDay()->format('ymd');
                    $shift = 1;
                    $this->HelpdeskRepository->autoTicketShift($data, $getDate, $shift);
                }
            }

        }


        $tittle = \Lang::get('helpdesk.SucessWait');
        $text = \Lang::get('helpdesk.SucessWaitcs');
        $returnUrl = route('helpdesk.list', ['lang' => \App::getLocale()]);
        return response()->json(['result' => "0", "showTittle" => $tittle, "showText" => $text, "returnUrl" => $returnUrl]);

    }

    public function postcreate(Request $request)
    {
        $getData = $request->allData;
        $data_js = json_decode($getData, true); //$data["Data"]["getPassword1"] example
        $data["Comment"] = $data_js["Data"]["comment"];
        $data["helpdesk_id"] = $data_js["Data"]["helpdesk_id"];
        $client = \Sentinel::getUser(); //weilun
        $member = $this->MemberRepository->findByUsername($client->username);

        if (!$model = $this->HelpdeskRepository->findByMemberId($data["helpdesk_id"], $member->id)) {

            $tittle = 'Data not found.';
            $text = "";
            //$returnUrl = route('helpdesk.list', ['lang' => \App::getLocale()]);
            return response()->json(['result' => "1", "showTittle" => $tittle, "showText" => $text]);
        }

        if ($model->is_reply == 0) {

            $tittle = \Lang::get('helpdesk.SucessWait');
            $text = \Lang::get('helpdesk.warning');
            //$returnUrl = route('helpdesk.list', ['lang' => \App::getLocale()]);
            return response()->json(['result' => "1", "showTittle" => $tittle, "showText" => $text]);

        }

        $data["client_id"] = $member->id;
        $data["client_username"] = $member->username;
        //$data["cs_lv"] = $admin->id;


        if (empty($data["Comment"])) {

            $tittle = \Lang::get('helpdesk.errorAdd');
            $text = \Lang::get('helpdesk.errorAddtext');
            //$returnUrl = route('helpdesk.list', ['lang' => \App::getLocale()]);
            return response()->json(['result' => "1", "showTittle" => $tittle, "showText" => $text]);

        }

        if (!empty(\Input::file('image1'))) {
            $idFront_path = \Input::file('image1');

            $checking = $this->UploadpicService->CheckFileSupported($idFront_path);
            if (empty($checking)) {
                $tittle = \Lang::get('error.uploadProfileFail');
                $text = "";
                $returnUrl = "";
                return response()->json(['result' => "1", "showTittle" => $tittle, "showText" => $text, "returnUrl" => $returnUrl]);
            }


            $filename1 = $this->AmazonS3->uploadImage($idFront_path, $client, 'helpdesk', uniqid());
            // $filename1 = $this->UploadpicService->helpdesk_upload($idFront_path);
            $data['pic_1'] = $filename1;
        }
        if (!empty(\Input::file('image2'))) {
            $idFront_path = \Input::file('image2');

            $checking = $this->UploadpicService->CheckFileSupported($idFront_path);
            if (empty($checking)) {
                $tittle = \Lang::get('error.uploadProfileFail');
                $text = "";
                $returnUrl = "";
                return response()->json(['result' => "1", "showTittle" => $tittle, "showText" => $text, "returnUrl" => $returnUrl]);
            }

            $filename2 = $this->AmazonS3->uploadImage($idFront_path, $client, 'helpdesk', uniqid());
            // $filename2 = $this->UploadpicService->helpdesk_upload($idFront_path);
            $data['pic_2'] = $filename2;
        }

        $this->HelpdeskCommentRepository->store($data);
        $this->HelpdeskRepository->update_reply($data);

        $now = Carbon::now();
        $check_morning = Carbon::create($now->year, $now->month, $now->day, 00, 00, 00);
        $check_eleven = Carbon::create($now->year, $now->month, $now->day, 10, 30, 00);
        $check_six = Carbon::create($now->year, $now->month, $now->day, 18, 30, 00);
        $check_night = Carbon::create($now->year, $now->month, $now->day, 20, 30, 00);
        $break_foreach = 0;
        $gotCsID = "";
        $Handle_model = $this->HelpdeskRepository->getHandleId($member->id);

        if (count($Handle_model) <= 0) {

            if ($now->greaterThan($check_morning) && $now->lessThan($check_eleven)) {
                $getDate = Carbon::now()->format('ymd');
                $shift = 1;
                $this->HelpdeskRepository->autoTicketReplyShift($data, $getDate, $shift);

            } else if ($now->greaterThan($check_eleven) && $now->lessThan($check_six)) {
                $getDate = Carbon::now()->format('ymd');
                $this->HelpdeskRepository->autoTicketReply($data, $getDate);

            } else if ($now->greaterThan($check_six) && $now->lessThan($check_night)) {
                $getDate = Carbon::now()->format('ymd');
                $shift = 2;
                $this->HelpdeskRepository->autoTicketReplyShift($data, $getDate, $shift);
            } else {
                $getDate = Carbon::now()->addDay()->format('ymd');
                $shift = 1;
                $this->HelpdeskRepository->autoTicketReplyShift($data, $getDate, $shift);
            }

        } else {


            foreach ($Handle_model as $Handle_models) {

                if ($now->greaterThan($check_morning) && $now->lessThan($check_eleven)) {
                    $getDate = Carbon::now()->format('ymd');
                    $shift = 1;


                } else if ($now->greaterThan($check_eleven) && $now->lessThan($check_six)) {
                    $getDate = Carbon::now()->format('ymd');
                    $shift = 3;


                } else if ($now->greaterThan($check_six) && $now->lessThan($check_night)) {
                    $getDate = Carbon::now()->format('ymd');
                    $shift = 2;

                } else {
                    $getDate = Carbon::now()->addDay()->format('ymd');
                    $shift = 1;

                }

                if ($gotCsID = $this->HelpdeskRepository->checkSchule($Handle_models->cs_id, $getDate, $shift)) {
                    $this->HelpdeskRepository->moveTicket($member->id, $gotCsID);
                    $this->HelpdeskRepository->addTicketNum($gotCsID, $getDate);
                    $break_foreach = 1;
                    break;
                }
            }

            if ($break_foreach == 0) {

                if ($now->greaterThan($check_morning) && $now->lessThan($check_eleven)) {
                    $getDate = Carbon::now()->format('ymd');
                    $shift = 1;
                    $this->HelpdeskRepository->autoTicketReplyShift($data, $getDate, $shift);

                } else if ($now->greaterThan($check_eleven) && $now->lessThan($check_six)) {
                    $getDate = Carbon::now()->format('ymd');
                    $this->HelpdeskRepository->autoTicketReply($data, $getDate);

                } else if ($now->greaterThan($check_six) && $now->lessThan($check_night)) {
                    $getDate = Carbon::now()->format('ymd');
                    $shift = 2;
                    $this->HelpdeskRepository->autoTicketReplyShift($data, $getDate, $shift);
                } else {
                    $getDate = Carbon::now()->addDay()->format('ymd');
                    $shift = 1;
                    $this->HelpdeskRepository->autoTicketReplyShift($data, $getDate, $shift);
                }
            }

        }


        $tittle = \Lang::get('helpdesk.SucessWait');
        $text = \Lang::get('helpdesk.SucessWaitcs');
        //$returnUrl = route('helpdesk.list', ['lang' => \App::getLocale()]);
        return response()->json(['result' => "0", "showTittle" => $tittle, "showText" => $text]);


    }

    /**
     * Announcement Read Page
     *
     * @param integer $id
     *
     * @return html
     */
    public function read($sa, $id)
    {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername($user->username);


        if (!$model = $this->HelpdeskRepository->findByMemberId($id, $member->id)) {

            return redirect()->back()->with('flashMessage', [
                'class' => 'danger',
                'message' => 'Data not found.'
            ]);
        }
        if (!$model2 = $this->HelpdeskCommentRepository->findById($id)) {
            return redirect()->back()->with('flashMessage', [
                'class' => 'danger',
                'message' => 'Data not found.'
            ]);
        }

        $this->HelpdeskRepository->findByReadUpdate($id);
        return view('front.helpdesk.read')->with('model', $model)->with('model2', $model2);
    }

    //rate ===========================================================================================

    public function postRate(Request $request)
    {

        $getData = $request->allData;
        $data_js = json_decode($getData, true); //$data["Data"]["getPassword1"] example
        $data["rate_star"] = $data_js["Data"]["star"];
        $data["helpdesk_id"] = $data_js["Data"]["helpdesk_id"];
        $client = \Sentinel::getUser(); //weilun
        $member = $this->MemberRepository->findByUsername($client->username);


        if (!$model = $this->HelpdeskRepository->findByMemberId($data["helpdesk_id"], $member->id)) {

            $tittle = 'Data not found.';
            $text = "";
            //$returnUrl = route('helpdesk.list', ['lang' => \App::getLocale()]);
            return response()->json(['result' => "1", "showTittle" => $tittle, "showText" => $text]);
        }

        if (empty($data["rate_star"])) {
            $tittle = \Lang::get('helpdesk.FailRate');
            $text = "";
            return response()->json(['result' => "1", "showTittle" => $tittle, "showText" => $text]);

        }

        $this->HelpdeskRepository->addRate($data);
        $tittle = \Lang::get('helpdesk.SucessRate');
        $text = \Lang::get('helpdesk.SucessRates');
        //$returnUrl = route('helpdesk.list', ['lang' => \App::getLocale()]);
        return response()->json(['result' => "0", "showTittle" => $tittle, "showText" => $text]);
    }

    //Complain ===========================================================================================
    public function postComplain(Request $request)
    {

        $getData = $request->allData;
        $data_js = json_decode($getData, true); //$data["Data"]["getPassword1"] example
        $data["complain"] = $data_js["Data"]["complain"];
        $data["helpdesk_id"] = $data_js["Data"]["helpdesk_id"];
        $client = \Sentinel::getUser(); //weilun
        $member = $this->MemberRepository->findByUsername($client->username);
        if (!$model = $this->HelpdeskRepository->findByMemberId($data["helpdesk_id"], $member->id)) {

            $tittle = 'Data not found.';
            $text = "";
            //$returnUrl = route('helpdesk.list', ['lang' => \App::getLocale()]);
            return response()->json(['result' => "1", "showTittle" => $tittle, "showText" => $text]);
        }

        if (empty($data["complain"])) {

            $tittle = \Lang::get('helpdesk.FailComplain');
            $text = "";
            //$returnUrl = route('helpdesk.list', ['lang' => \App::getLocale()]);
            return response()->json(['result' => "1", "showTittle" => $tittle, "showText" => $text]);

        }

        $this->HelpdeskRepository->addComplain($data);
        $tittle = \Lang::get('helpdesk.SucessComplain');
        $text = "";
        //$returnUrl = route('helpdesk.list', ['lang' => \App::getLocale()]);
        return response()->json(['result' => "0", "showTittle" => $tittle, "showText" => $text]);

    }

    //Settle ===========================================================================================
    public function postSettle()
    {

        $data = \Input::get('data');

        if (empty($data["helpdesk_id"])) {

            return \Response::json([
                'type' => 'error'


            ]);

        }

        $this->HelpdeskRepository->updateSettle($data);
        return \Response::json([
            'type' => 'success',
            'message' => \Lang::get('helpdesk.CaseClose'),
            'redirect' => route('helpdesk.read', ['lang' => \App::getLocale(), 'id' => $data["helpdesk_id"]])
        ]);


    }

    public function faq()
    {
        if (App::getLocale() === 'en') {
            $faqs = Faq::getEn()->get();
            $funds = Faq::getEn()->getFaq('fund');
            $withdraws = Faq::getEn()->getFaq('withdrawal');
            $advisories = Faq::getEn()->getFaq('advisory');
            $approvals = Faq::getEn()->getFaq('approval');
            $capxs = Faq::getEn()->getFaq('capx');

        } else {
            $faqs = Faq::getChs()->get();
            $funds = Faq::getChs()->getFaq('fund');
            $withdraws = Faq::getChs()->getFaq('withdrawal');
            $advisories = Faq::getChs()->getFaq('advisory');
            $approvals = Faq::getChs()->getFaq('approval');
            $capxs = Faq::getChs()->getFaq('capx');
        }

        $locale = App::getLocale();

        return view('faq.index', compact('faqs', 'locale', 'funds', 'withdraws', 'advisories', 'approvals', 'capxs'));
    }
}
