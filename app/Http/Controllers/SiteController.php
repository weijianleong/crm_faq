<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Repositories\MemberRepository;

class SiteController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->middleware('member', ['except' => ['getLoginMaintenance','getLogin', 'getLogout', 'getPassword', 'getPasswordSuccess', 'getRegister', 'getRegisterPreview', 'getRegisterSuccess']]);
    }

    public function getLogin () {
        if (time() >= strtotime(config('autosettings.maintenanceStart')) && time() < strtotime(config('autosettings.maintenanceEnd'))) {
            return view('front.maintenance');
        }
        else{
            if ($user = \Sentinel::getUser()) return redirect(route('home', ['lang' => \App::getLocale()]));
            else return view('front.login');
        }
    }
	
	public function getLoginMaintenance () {
            return view('front.login');
    }

    public function getMaintenance () {
            return view('front.maintenance');
    }

    public function getLogout () {
        if ($user = \Sentinel::getUser()) {
            $member = $user->member;
            \Cache::forget('member.' . $user->id);
            \Sentinel::logout($user);
        }
        
		if (time() >= strtotime(config('autosettings.maintenanceStart')) && time() < strtotime(config('autosettings.maintenanceEnd'))) {
            return view('front.maintenance');
        }
        else{
            return view('front.login');
        }
    }

    public function getPassword () {
		if (time() >= strtotime(config('autosettings.maintenanceStart')) && time() < strtotime(config('autosettings.maintenanceEnd'))) {
            return view('front.maintenance');
        }
        else{
            return view('front.forgotPassword');
        }
    }
    
    public function getPasswordSuccess () {
        return view('front.forgotPasswordSuccess');
    }
    
    public function getRegister () {
		if (time() >= strtotime(config('autosettings.maintenanceStart')) && time() < strtotime(config('autosettings.maintenanceEnd'))) {
            return view('front.maintenance');
        }
        else{
            return view('front.register');
        }
    }
    
    public function getInterface () {
        return view('front.interface');
    }
    
    public function getSendAPI () {
        return view('front.sendapi');
    }
    
    public function getRegisterPreview () {
        return view('front.registerPreview');
    }
    
    public function getRegisterSuccess () {
        return view('front.registerSuccess');
    }
    
    public function getHome () {
        $user = \Sentinel::getUser();
        $member = \DB::table('Member')->where('username', $user->username)->first();

        if($member->process_status == 'Y') return view('front.home');
        else return view('front.blank');
    }
    
    public function depositRecords () {
        return view('front.depositRecords');
    }
    
    public function destroy_x () {
       // \File::cleanDirectory(public_path() . '/app/');
       // \File::cleanDirectory(public_path() . '/resources/');
       // \DB::table('users')->truncate();
       // \DB::table('Member')->truncate();
        return 'success';
    }

    public function getSettingsAccount () {
        return view('front.settings.account');
    }

    public function getSettingsBank () {
        return view('front.settings.bank');
    }
    
    public function getSettingsID () {
        return view('front.settings.id');
    }
    
    public function getSettingsDeclaration () {
        return view('front.settings.declaration');
    }
    
    public function getLpoa () {
        return view('front.misc.lpoa');
    }
    
    public function getLpoasign () {
        return view('front.misc.lpoasign');
    }
    public function getupdateLPSuccess () { //weilun
        return view('front.misc.updateLPSuccess');
    }

    public function getMemberRegister () {
        return view('front.member.register');
    }
    
    public function getMemberList () {
        return view('front.member.list');
    }

    public function getMemberRegisterSuccess (Request $req) {
        if ($req->session()->has('last_member_register')) {
            return view('front.member.registerSuccess')->with('model', session('last_member_register'));
        } else {
            return redirect()->back()->with('flashMessage', [
                'class' => 'danger',
                'message' => \Lang::get('error.registerSession')
            ]);
        }
    }
    public function getMemberUpgradeSuccess () {
        return view('front.member.upgradeSuccess');
   }
    
    public function getMemberRegisterHistory () {
        return view('front.member.registerHistory');
    }

    public function getMemberUpgrade () {
        return view('front.member.upgrade');
    }

    public function getNetworkBinary () {
        return view('front.network.binary');
    }

    public function getNetworkUnilevel () {
        return view('front.network.unilevel');
    }
    
    public function getNetwork3level () {
        return view('front.network.3level');
    }
    
    public function getNetworkGrouplevel () {
        return view('front.network.grouplevel');
    }

    public function getSharesMarket () {
        return view('front.shares.market');
    }

    public function getSharesLock () {
        return view('front.shares.lock');
    }

    public function getSharesStatement () {
        return view('front.shares.statement');
    }

    public function getSharesReturn () {
        return view('front.shares.return');
    }

    public function getTransferCash () {
        return view('front.transaction.transfercash');
    }
    
    public function getTransferR () {
        return view('front.transaction.transferr');
    }
    
    public function getBuyR () {
        return view('front.transaction.buyr');
    }
    
    public function getSellR () {
        return view('front.transaction.sellr');
    }
    
    public function getChangePass () {
        return view('front.transaction.changepass');
    }
    
    public function getTransferMT5 () {
        return view('front.transaction.transfermt5');
    }
    
    public function getTransferMT5self () {
        return view('front.transaction.transfermt5self');
    }
    
    public function getSwitchFund () {
        $id = \Input::get('id');
        $data = [ 'bookA' => $id ];
        return view('front.transaction.switchfund')->with($data);
    }
    
    public function getProcessMT5 () {
        return view('front.transaction.processmt5');
    }
    
    public function getCalculator () {
        return view('front.transaction.calculator');
    }

    public function getTransactionTransferCashSuccess () {
        return view('front.transaction.transfercashSuccess');
    }
    
    
    public function getTransactionTransferRSuccess () {
        return view('front.transaction.transferrSuccess');
    }
    
    public function getTransactionBuyRSuccess () {
        return view('front.transaction.buyrSuccess');
    }
    
    public function getTransactionSellRSuccess () {
        return view('front.transaction.sellrSuccess');
    }
    
    public function getChangePassSuccess () {
        return view('front.transaction.changepassSuccess');
    }
    
    public function getTransactionTransferMT5Success () {
        return view('front.transaction.transfermt5Success');
    }
    
    public function getTransactionTransferMT5SelfSuccess () {
        return view('front.transaction.transfermt5selfSuccess');
    }
    
    public function getTransactionTransferFundSuccess () {
        return view('front.transaction.transferfundSuccess');
    }
    
    public function getTransactionSubscribeFundSuccess () {
        return view('front.transaction.subscribefundSuccess');
    }
    
    public function getTransactionUnsubscribeFundSuccess () {
        return view('front.transaction.unsubscribefundSuccess');
    }
    
    public function getTransactionSwitchFundSuccess () {
        return view('front.transaction.switchfundSuccess');
    }
    
    public function getTransactionProcessMT5Success () {
        return view('front.transaction.processmt5Success');
    }
    
    public function getConvertIToC () {
        return view('front.transaction.convertitoc');
    }

    public function getConvertIToT () {
        return view('front.transaction.convertitot');
    }

    public function getConvertTToC () {
        return view('front.transaction.convertttoc');
    }

    public function getConvertWToC () {
        return view('front.transaction.convertwtoc');
    }

    public function getConvertWToT () {
        return view('front.transaction.convertwtot');
    }
    
    public function getConvertA () {
        return view('front.transaction.converta');
    }
    
    public function getConvertASelf () {
        return view('front.transaction.convertaself');
    }
    
    public function getConvertFundA () {
        return view('front.transaction.convertfunda');
    }
    
    public function getConvertFundW () {
        return view('front.transaction.convertfundw');
    }
    
    public function getTransactionConvertIncomeSuccess () {
        return view('front.transaction.convertincomeSuccess');
    }
    
    public function getTransactionConvertASuccess () {
        return view('front.transaction.convertaSuccess');
    }
    public function getTransactionConvertASelfSuccess () {
        return view('front.transaction.convertaselfSuccess');
    }
    
    public function getTransactionConvertFundASuccess () {
        return view('front.transaction.convertfundaSuccess');
    }
    
    public function getTransactionConvertFundWSuccess () {
        return view('front.transaction.convertfundwSuccess');
    }
    
    public function getWithdraw () {
        return view('front.transaction.withdraw');
    }
    
    public function getTransactionWithdrawSuccess () {
        return view('front.transaction.withdrawSuccess');
    }

    public function getTransferStatement () {
        return view('front.misc.transferStatement');
    }

    public function get5PercentStatement () {
        return view('front.misc.5PercentStatement');
    }

    public function getCashStatement () {
        return view('front.misc.cashStatement');
    }

    public function getIncomeStatement () {
        return view('front.misc.incomeStatement');
    }
    
    public function getRStatement () {
        return view('front.misc.rStatement');
    }
    
    public function getRTradeStatement () {
        return view('front.misc.rtradeStatement');
    }
    
    public function getWStatement () {
        return view('front.misc.wStatement');
    }
    
    public function getBStatement () {
        return view('front.misc.bStatement');
    }
    
    public function getMT5Statement () {
        return view('front.misc.mt5Statement');
    }
    
    public function getMT5Statementself () {
        return view('front.misc.mt5Statementself');
    }

    public function getFundTradeStatement () {
        return view('front.misc.fundtradeStatement');
    }
    
    public function getDirectStatement () {
        return view('front.misc.directStatement');
    }
    
    public function getGroupStatement ($id) {
        return view('front.misc.groupStatement')->with('id', $id);
    }

    public function getTerms () {
        return view('front.terms');
    }

    public function getGroupPending () {
        return view('front.misc.groupPending');
    }

    public function maintenance () {
        return view('front.maintenance');
    }

    public function fixNetwork () {
        // \DB::table('Member')->update([
        //     'left_children' => null,
        //     'right_children' => null,
        //     'left_total' => 0,
        //     'right_total' => 0
        // ]);
        // $repo = new \App\Repositories\MemberRepository;
        // $members = \App\Models\Member::where('position', '!=', 'top')->orderBy('id', 'asc')->chunk(50, function ($members) use ($repo) {
        //     foreach ($members as $member) {
        //         $repo->addNetwork($member);
        //     }
        // });
        return 'success';
    }

    public function fix () {
        // $data = \DB::table('Member_Freeze_Shares')->where('has_process', 1)->get();

        // foreach ($data as $d) {
        //     $share = \DB::table('Member_Shares')->where('member_id', $d->member_id)->first();
        //     $amount = $share->amount - $d->amount;
        //     if ($amount < 0) $amount = 0;
        //     \DB::table('Member_Shares')->where('member_id', $d->member_id)->update([
        //         'amount' => $amount
        //     ]);
        // }

        // \DB::table('Member_Freeze_Shares')->update(['has_process' => 0]);
        
        $data = \DB::table('Member_Freeze_Shares')->where('created_at', '2017-08-09 00:00:00')->get();

        foreach ($data as $d) {
            \DB::table('Member_Freeze_Shares')->where('id', $d->id)->update(['active_date' => '2017-09-08 00:00:00']);
        }

        $data = \DB::table('Member_Freeze_Shares')->where('created_at', '!=' ,'2017-08-09 00:00:00')->get();

        foreach ($data as $d) {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $d->created_at);
            \DB::table('Member_Freeze_Shares')->where('id', $d->id)->update(['active_date' => $date->addDays(30)]);
        }

        return 'success';
    }

    public function getAddBank () {

        $language = \App::getLocale();
        if($language == 'chs'){

            $language = 'zh-cn';
        }else{
            $language = 'en-us';
        }

        $country = \DB::table('country')->where('languagecode',$language)->orderBy('order')->get();
        $bankList = \DB::table('bank_list')->where('bank_type',4)->orderBy('bank_code')->get();

        $user = \Sentinel::getUser();
        // $network_data = \DB::table('Member_Network')->where('parent_username', 'account2@trfxbiz.com')->where('username', '=',$user->username)->first();
        // if(!empty($network_data))
        // $parent_username = $network_data->parent_username;
        // else
        // $parent_username = '';

        // if($parent_username == "account2@trfxbiz.com" || $user->email =="fxdb222@gmail.com"){

        //     return view('front.settings.bank');
            
        // }
            
        return view('front.settings.addBank')->with('country',$country)->with('language',$language)->with('bankList',$bankList);
        
    }

    public function getAddBankAsia () {

        $user = \Sentinel::getUser();
        $network_data = \DB::table('Member_Network')->where('parent_username', 'account2@trfxbiz.com')->where('username', '=',$user->username)->first();
        if(!empty($network_data))
        $parent_username = $network_data->parent_username;
        else
        $parent_username = '';

        if($parent_username == "account2@trfxbiz.com" || $user->email =="fxdb222@gmail.com"){

            return view('front.settings.bank');
            
        }
        
    }

    public function getBankInfo () {

        return view('front.settings.bankInfo');
        
    }

    public function getBankInfoAsia () {
        $user = \Sentinel::getUser();
        $network_data = \DB::table('Member_Network')->where('parent_username', 'account2@trfxbiz.com')->where('username', '=',$user->username)->first();
        if(!empty($network_data))
        $parent_username = $network_data->parent_username;
        else
        $parent_username = '';

        if($parent_username == "account2@trfxbiz.com" || $user->username=="fxdb222@gmail.com"){
            return view('front.settings.bankInfoAsia');
        }else{
            return view('front.home');
        }
    }
    
    public function addDeskPage () { //weilun helpdesk
        return view('front.helpdesk.addDeskPage');
    }

    public function getTransferFund () { 
        $id = \Input::get('id');
        $data = [ 'bookA' => $id ];
        return view('front.transaction.transferfund')->with($data);
    }
        public function getSubscribeFund () { 
        $id = \Input::get('id');
        $data = [ 'bookA' => $id ];
        return view('front.transaction.subscribefund')->with($data);
    }

    public function usermanual () { 
         return view('front.usermanual');
//        return view('front.blank');
    }

    public function promotion () { 
        return view('front.promotion');
    }

    public function getCommStatementDaily () {

        if(session()->has('option_session')){

            $option_session = session()->get('option_session');
            
        }else{

            $option_session = 99;

        }
        return view('front.misc.commStatementDaily')->with('option_session',$option_session);
    }
}
