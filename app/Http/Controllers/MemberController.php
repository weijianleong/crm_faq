<?php

namespace App\Http\Controllers;
use Mail;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use App\Jobs\NetworkAfterRegisterJob;
use App\Jobs\SharesAfterRegisterJob;
use App\Repositories\MemberRepository;
use App\Repositories\PackageRepository;
use App\Repositories\SharesRepository;
use App\Repositories\BonusRepository;
use App\Repositories\HelpdeskRepository;
use Yajra\Datatables\Facades\Datatables;
use App\Services\AmazonS3;
use App\Services\CheckingService;
use App\Services\UploadpicService;
use Validator;

    

class MemberController extends Controller
{
    /**
     * The MemberRepository instance.
     *
     * @var \App\Repositories\MemberRepository
     */
    protected $MemberRepository;

    /**
     * The PackageRepository instance.
     *
     * @var \App\Repositories\PackageRepository
     */
    protected $PackageRepository;
    
    /**
     * The SharesRepository instance.
     *
     * @var \App\Repositories\SharesRepository
     */
    protected $SharesRepository;
    
    protected $BonusRepository;
    
    protected $HelpdeskRepository;

    protected $AmazonS3;
    protected $CheckingService;
    protected $UploadpicService;

    /**
     * Create a new MemberController instance.
     *
     * @param \App\Repositories\MemberRepository $MemberRepository
     * @return void
     */
    public function __construct(
        MemberRepository $MemberRepository,
        PackageRepository $PackageRepository,
        SharesRepository $SharesRepository,
        BonusRepository $BonusRepository,
        HelpdeskRepository $HelpdeskRepository,
        AmazonS3 $AmazonS3,
        CheckingService $CheckingService,
        UploadpicService $UploadpicService
    ) {
        $this->MemberRepository = $MemberRepository;
        $this->PackageRepository = $PackageRepository;
        $this->SharesRepository = $SharesRepository;
        $this->BonusRepository = $BonusRepository;
        $this->HelpdeskRepository = $HelpdeskRepository;
        $this->AmazonS3 = $AmazonS3;
        $this->CheckingService = $CheckingService;
        $this->UploadpicService = $UploadpicService;
        $this->middleware('member', ['except' => ['adminLogin','postLogin','postgetPassword','postRegisterPreview','postRegisterPublic','getMemberRegisterModal','amazonReadRegister','deletePic']]);
    }
    

    public function getregisterList () {
        
        $user = \Sentinel::getUser();
        //$currentMember = $user->member;
        $currentMember = $this->MemberRepository->findByUsername(trim($user->username));
       
            return $this->MemberRepository->findRegisterWalletStatementList(true, $currentMember->id);
        
    }

    public function get5PercentList () {
        
        $user = \Sentinel::getUser();
        //$currentMember = $user->member;
        $currentMember = $this->MemberRepository->findByUsername(trim($user->username));
        
        return $this->MemberRepository->find5PercentStatementList(true, $currentMember->id);
        
    }
    
    public function getwList () {
        
        $user = \Sentinel::getUser();
        //$currentMember = $user->member;
        $currentMember = $this->MemberRepository->findByUsername(trim($user->username));
        
        return $this->MemberRepository->findWWalletStatementList(true, $currentMember->id);
        
    }
    
    public function getbList () {
        
        $user = \Sentinel::getUser();
        //$currentMember = $user->member;
        $currentMember = $this->MemberRepository->findByUsername(trim($user->username));
        
        return $this->MemberRepository->findBWalletStatementList(true, $currentMember->id);
        
    }
    
    public function getcashList () {
        
        $user = \Sentinel::getUser();
        //$currentMember = $user->member;
        $currentMember = $this->MemberRepository->findByUsername(trim($user->username));
        
        return $this->MemberRepository->findCashWalletStatementList(true, $currentMember->id);
        
    }
    
    public function getbonusList () {
        
        $user = \Sentinel::getUser();
        //$currentMember = $user->member;
        $currentMember = $this->MemberRepository->findByUsername(trim($user->username));
   
        return $this->MemberRepository->findBonusWalletStatementList(true, $currentMember->id);
        
    }

    public function gettransferlist () {
        
        $user = \Sentinel::getUser();
        //$currentMember = $user->member;
        $currentMember = $this->MemberRepository->findByUsername(trim($user->username));
   
        return $this->MemberRepository->findTransferWalletStatementList(true, $currentMember->id);
        
    }
    
    public function getaccountList () {
        
        $user = \Sentinel::getUser();
        //$currentMember = $user->member;
        $currentMember = $this->MemberRepository->findByUsername(trim($user->username));
        
        return $this->MemberRepository->findAccountList(true, $currentMember->id);
        
    }
    
    public function getselfaccountList () {
        
        $user = \Sentinel::getUser();
        //$currentMember = $user->member;
        $currentMember = $this->MemberRepository->findByUsername(trim($user->username));
        
        return $this->MemberRepository->findSelfAccountList(true, $currentMember->id);
        
    }
    
    public function getdirectSales () {
        
        $user = \Sentinel::getUser();
        //$currentMember = $user->member;
        $currentMember = $this->MemberRepository->findByUsername(trim($user->username));
        
        return $this->MemberRepository->findDirectSalesList(true, $currentMember->id);
        
    }
    
    public function getgroupSales ($id) {
        
        $user = \Sentinel::getUser();
        
        $id  = \Session::get('meid');
        
        if (isset($id))
        {
            $member_id = $id;
        }
        else
        {
            $currentMember = $this->MemberRepository->findByUsername(trim($user->username));
            $member_id = $currentMember->id;
        }

        //$currentMember = $user->member;
    
        return $this->MemberRepository->findGroupSalesList(true, $member_id);
        
    }
    
    public function getfundaccountList () {
        
        $user = \Sentinel::getUser();
        $currentMember = $this->MemberRepository->findByUsername(trim($user->username));
        
        return $this->MemberRepository->findFundAccountList(true, $currentMember->id);
        
    }
    
    public function getfundtradeaccountList () {
        
        $user = \Sentinel::getUser();
        //$currentMember = $user->member;
        $currentMember = $this->MemberRepository->findByUsername(trim($user->username));
        
        return $this->MemberRepository->findFundTradeAccountList(true, $currentMember->id);
        
    }
    
    public function gettradeList () {
        
        $user = \Sentinel::getUser();
        //$currentMember = $user->member;
        $currentMember = $this->MemberRepository->findByUsername(trim($user->username));
        
        return $this->MemberRepository->findTradeList(true, $currentMember->id);
        
    }

    public function CommStatementDaily ($lang,$type) {
        session()->forget('option_session');
        $user = \Sentinel::getUser();
        //$currentMember = $user->member;
        $Member = $this->MemberRepository->findByUsername(trim($user->username));
        if($type == 99){
            return $this->MemberRepository->findAllCommStatementDailyList($Member->id);
        }else{
            return $this->MemberRepository->findCommStatementDailyList($Member->id,$type);
        }
        
    }
    
    /**
     * Login
     * @return [type] [description]
     */
    public function postgetPassword (Request $request) {
        $isHuman = captcha_check($request->captcha);
        if(!$isHuman){
            $response = array('status' => 1, 'msg' => \Lang::get('error.captchaError'));
            return response()->json($response);
        }

        try {
            $user = $this->MemberRepository->findByUsername(trim($request->email));
            if (!$user) {
                $response = array('status' => 1, 'msg' => \Lang::get('error.loginError'));
                return response()->json($response);
            }else{
                $sendPassword = $this->sendPassword($user->user->username, $user->user->first_name,$user->user->password2,$user->secret_password);
            }
        } catch (\Exception $e) {
            \Sentinel::logout();
            $response = array('status' => 1, 'msg' => $e->getMessage());
            return response()->json($response);
        }

        $response = array('status' => 0, 'msg' => \Lang::get('message.getpasswordSuccess'));
        return response()->json($response);
    }

        /**
     * Login
     * @return [type] [description]
     */
    public function postLogin (Request $request) {
        try {
            $isHuman = captcha_check($request->captcha);
            if(!$isHuman){
                throw new \Exception(\Lang::get('error.captchaError'), 1);
                return false;
            }

            $user = \Sentinel::authenticate([
                    'email'  =>  $request->email,
                    'password'  =>  $request->password
                ], (isset($request->remember)));

            if(!$checkUser = $this->MemberRepository->findByUsername($request->email)) {
                throw new \Exception(\Lang::get('error.memberNotFound'), 1);
                return false;
            }

            if(!$user) {
                throw new \Exception(\Lang::get('error.loginError'), 1);
                return false;
            }

            if($user->is_ban != 1) {
                throw new \Exception(\Lang::get('error.userBan'), 1);
                return false;
            }

            // check right for access url
            if(in_array($_SERVER['HTTP_HOST'], ['my.veko.io'])){
                if(!in_array($user->username, ['seanetlead@gmail.com'])){
                    $seaNetwork = \DB::table('Member_Network')->where('parent_username', 'seanetlead@gmail.com')->where('username', $checkUser->register_by)->count();
                    if($checkUser->register_by == 'seanetlead@gmail.com') $seaNetwork = 1;
                    if($seaNetwork > 0){
                        if($checkUser->detail->nationality <> 'Malaysia'){
                            throw new \Exception(\Lang::get('error.loginError'), 1);
                            return false;
                        }
                    }else{
                        throw new \Exception(\Lang::get('error.loginError'), 1);
                        return false;
                    }
                }
            }else{
                $seaNetwork = \DB::table('Member_Network')->where('parent_username', 'seanetlead@gmail.com')->where('username', $checkUser->register_by)->count();
                if($checkUser->register_by == 'seanetlead@gmail.com') $seaNetwork = 1;
                if($seaNetwork > 0){
                    if($checkUser->detail->nationality == 'Malaysia'){
                        throw new \Exception(\Lang::get('error.loginError'), 1);
                        return false;
                    }
                }
            }

            $permissions = $user->permissions;
            if(!isset($permissions['member'])) {
                throw new \Exception(\Lang::get('error.userError'), 1);
                return false;
            }
            else if ($permissions['member'] != 1) {
                throw new \Exception(\Lang::get('error.userError'), 1);
                return false;
            }
            else{
                if(!\Cache::has('member.' . $user->id)) {
                    $member = $this->MemberRepository->findByUsername(trim($user->username));
                    $member->load('wallet');
                    $member->load('detail');
                    //$member->load('shares');
                    $member->load('package');
                    \Cache::put('member.' . $user->id, $member, 60);
                }else {
                    $member = \Cache::get('member.'. $user->id);
                }
            }
        } catch (\Exception $e) {
            \Sentinel::logout();
            $response = array('status' => 1, 'msg' => $e->getMessage());
            return response()->json($response);
        }

        $unreadTicket = \DB::table('helpdesk')->where('user_id', $member->id)->where('is_reply', 1)->where('read', 1)->count();
        if($unreadTicket > 0){
            $resendurl =  route('helpdesk.list', ['lang' => \App::getLocale()]) ;
            $msg = \Lang::get('helpdesk.fleshmsg');
            Session::flash('message', $msg);
            Session::flash('resendurl', $resendurl);
        }

        $unreadAnnouncement = 0;
        if($member->detail->announcement) $unreadAnnouncement = 1;

        if($unreadAnnouncement > 0){
            $resendurl =  route('inbox.list', ['lang' => \App::getLocale()]) ;
            $msg = \Lang::get('inbox.fleshmsg');
            Session::flash('Notification', $msg);
            Session::flash('Notificationresendurl', $resendurl);
        }

        $memoShow = \DB::table('Member_Detail')->where('member_id', $member->id)->first();
        if($memoShow->memo > 0) Session::flash('Memo', $memoShow->memo);

        \DB::table('Member')->where('id',$member->id)->update(['last_login' => \Carbon\Carbon::now()]);

        $response = array('status' => 0, 'msg' => \Lang::get('message.loginSuccess'));
        return response()->json($response);
    }
    
    
    public function postRegisterPreview (Request $request) {

        $getData = $request->allData;
        $data_js = json_decode($getData,true); //$data["Data"]["getPassword1"] example
        $direct_id = $this->CheckingService->CheckSpace($data_js["Data"]["direct_id"]);
        $first_name = $data_js["Data"]["first_name"];
        $identification_number = $this->CheckingService->CheckSpace($data_js["Data"]["identification_number"]);
        $inputGender = $this->CheckingService->CheckSpace($data_js["Data"]["inputGender"]);
        $email = $this->CheckingService->CheckSpace($data_js["Data"]["email"]);
        $inputPhone = $this->CheckingService->CheckSpace($data_js["Data"]["inputPhone"]);
        $inputNationality = $this->CheckingService->CheckSpace($data_js["Data"]["inputNationality"]);
        $address = $data_js["Data"]["address"];
        $inputPassword = $this->CheckingService->CheckSpace($data_js["Data"]["inputPassword"]);
        $captcha = $this->CheckingService->CheckSpace($data_js["Data"]["captcha"]);

        if(empty($email)){
            $response = array('status' => 1, 'msg' => \Lang::get('error.emailEmpty'));
            return response()->json($response);
        }
        $isHuman = captcha_check($captcha);
        if(!$isHuman){
            $response = array('status' => 1, 'msg' => \Lang::get('error.captchaError'));
            return response()->json($response);
        }
        if(empty($first_name)){
            $response = array('status' => 1, 'msg' => \Lang::get('error.nameEmpty'));
            return response()->json($response);
        }
        if(empty($identification_number)){
            $response = array('status' => 1, 'msg' => \Lang::get('error.icEmpty'));
            return response()->json($response);
        }
        if(empty($inputPhone)){
            $response = array('status' => 1, 'msg' => \Lang::get('error.inputPhoneEmpty'));
            return response()->json($response);
        }
        if(empty($address)){
            $response = array('status' => 1, 'msg' => \Lang::get('error.addressEmpty'));
            return response()->json($response);
        }
        if($CheckPass = \DB::table('ban_setting')->where('content',strtolower($inputPassword))->value('id')){
            $response = array('status' => 1, 'msg' => \Lang::get('error.passwordfail'));
            return response()->json($response);
        }

        $ispass = $this->CheckingService->CheckPassword($inputPassword);
        if($ispass == false){
            $response = array('status' => 1, 'msg' => \Lang::get('register.passwordRemind'));
            return response()->json($response);
        }

        if (!$direct = $this->MemberRepository->findByUsername($direct_id)) {
            $response = array('status' => 1,'msg' => \Lang::get('error.sponsorNotFound') );
            return response()->json($response);
        }
        if($direct->process_status != 'Y'){ // ban or processing weilun
            $response = array('status' => 1,'msg' => \Lang::get('error.transferFail') );
            return response()->json($response);
        }
        if ($Checkban = $this->MemberRepository->findUser($direct_id)) { 
          if($Checkban->is_ban ==0){
                $response = array('status' => 1,'msg' => \Lang::get('error.transferFail') );
                return response()->json($response);
            }
          if($Checkban->special_group_block ==1){
                $response = array('status' => 1,'msg' => \Lang::get('error.transferFail') );
                return response()->json($response);
            }
        }
        if ($existinIC = $this->MemberRepository->findIC($identification_number)) {
            $response = array('status' => 1,'msg' => \Lang::get('error.memberICExist') );
            return response()->json($response);
        }
    
        if ($existingemail = $this->MemberRepository->findByUsername($email)) {
            $response = array('status' => 1,'msg' => \Lang::get('error.memberExist') );
            return response()->json($response);
        }  


        if(!empty( \Input::file('inputIDfront')) )
        {
            $inputIDfront = \Input::file('inputIDfront');
            $checking = $this->UploadpicService->CheckFileSupported($inputIDfront);
            if(empty($checking)){
                $tittle = \Lang::get('error.uploadProfileFail');
                $response = array('status' => 1,'msg' => $tittle );
                return response()->json($response);
            }
            $idFront_path = $inputIDfront;
            //$filename1 = $this->AmazonS3->uploadImage($idFront_path, $user, 'helpdesk', uniqid());
            $filename1 = $this->CheckingService->uploadFile($idFront_path,'/ID/',$email);
            $data['inputIDfront'] = $filename1;
        }
        if(!empty( \Input::file('inputIDback')) )
        {
            $inputIDfront = \Input::file('inputIDback');
            $checking = $this->UploadpicService->CheckFileSupported($inputIDfront);
            if(empty($checking)){
                $tittle = \Lang::get('error.uploadProfileFail');
                $response = array('status' => 1,'msg' => $tittle );
                return response()->json($response);
            }
            $idFront_path = $inputIDfront;
            //$filename1 = $this->AmazonS3->uploadImage($idFront_path, $user, 'helpdesk', uniqid());
            $filename2 = $this->CheckingService->uploadFile($idFront_path,'/ID/',$email);
            $data['inputIDback'] = $filename2;
        }

        $response = array('status' => 0,'msg' => \Lang::get('register.confirm'),'pic1'=>$data['inputIDfront'],'pic2'=>$data['inputIDback'],"memberData"=>$getData );
        return response()->json($response);



    }
    public function deletePic (Request $request) {
        $idfront = $request->idfront;
        $idback = $request->idback;
        unlink(public_path('ID/'.$idfront));
        unlink(public_path('ID/'.$idback));
    }
    /**
     * Register new member
     * @return [type] [description]
     */
    public function postRegister () {
        $data = \Input::get('data');
        $user = \Sentinel::getUser();
        //$currentMember = $user->member;
        $currentMember = $this->MemberRepository->findByUsername(trim($user->username));

        if (!isset($data['terms'])) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  \Lang::get('error.termsError')
            ]);
        }
        //print_r($data);
        if ($currentMember->secret_password != trim($data['s'])) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  \Lang::get('error.securityPasswordError')
            ]);
        }

        try {
            $member = $this->MemberRepository->register($data, $currentMember);
        } catch (\Exception $e) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  $e->getMessage()
            ]);
        }
        /*
        if (env('APP_ENV') == 'local') { // local
            $wallet = $member->wallet;
            $this->MemberRepository->addNetwork($member);
            $this->SharesRepository->repurchasePackage($member, $wallet->purchase_point, $wallet);
        } else { // production
            dispatch(new SharesAfterRegisterJob($member))->onQueue('queue-shares-register');
            dispatch(new NetworkAfterRegisterJob($member))->onQueue('queue-network-register');
        }
        */
        \Cache::forget('member.' . $user->id);
        session(['last_member_register' => $member]);

        return \Response::json([
            'type'  => 'success',
            'message' => \Lang::get('message.registerSuccess'),
            'redirect' => route('member.registerSuccess', ['lang' => \App::getLocale()])
        ]);
    }
    
     /**
     * Register new member
     * @return [type] [description]
     */
    public function postRegisterPublic (Request $request) {

        $getData = $request->allData;
        $data_js = json_decode($getData,true); //$data["Data"]["getPassword1"] example
        $direct_id = $this->CheckingService->CheckSpace($data_js["Data"]["direct_id"]);
        $first_name = $data_js["Data"]["first_name"];
        $identification_number = $this->CheckingService->CheckSpace($data_js["Data"]["identification_number"]);
        $inputGender = $this->CheckingService->CheckSpace($data_js["Data"]["inputGender"]);
        $email = $this->CheckingService->CheckSpace($data_js["Data"]["email"]);
        $inputPhone = $this->CheckingService->CheckSpace($data_js["Data"]["inputPhone"]);
        $inputNationality = $this->CheckingService->CheckSpace($data_js["Data"]["inputNationality"]);
        $address = $data_js["Data"]["address"];
        $inputPassword = $this->CheckingService->CheckSpace($data_js["Data"]["inputPassword"]);

        $data["direct_id"] = $direct_id;
        $data["first_name"] = $first_name;
        $data["identification_number"] = $identification_number;
        $data["gender"] = $inputGender;
        $data["email"] = $email;
        $data["phone1"] = $inputPhone;
        $data["nationality"] = $inputNationality;
        $data["address"] = $address;
        $data["password"] = $inputPassword;

        $user = new \stdClass();
        $user->email = $data['email'];

        if (!$direct = $this->MemberRepository->findByUsername($direct_id)) {
            $response = array('status' => 1,'msg' => \Lang::get('error.sponsorNotFound') );
            return response()->json($response);
        }
        if($direct->process_status != 'Y'){ // ban or processing weilun
            $response = array('status' => 1,'msg' => \Lang::get('error.transferFail') );
            return response()->json($response);
        }
        if ($Checkban = $this->MemberRepository->findUser($direct_id)) { 
          if($Checkban->is_ban ==0){
                $response = array('status' => 1,'msg' => \Lang::get('error.transferFail') );
                return response()->json($response);
            }
        }
        if ($existinIC = $this->MemberRepository->findIC($identification_number)) {
            $response = array('status' => 1,'msg' => \Lang::get('error.memberICExist') );
            return response()->json($response);
        }
    
        if ($existingemail = $this->MemberRepository->findByUsername($email)) {
            $response = array('status' => 1,'msg' => \Lang::get('error.memberExist') );
            return response()->json($response);
        }  

        if(!empty( \Input::file('inputIDfront')) )
        {
            $inputIDfront = \Input::file('inputIDfront');
            $idFront_path = $inputIDfront;
            $filename1 = $this->AmazonS3->uploadImage($idFront_path, $user, 'ID', 'front');
            $data['inputIDfront'] = $filename1;
        }
        if(!empty( \Input::file('inputIDback')) )
        {
            $inputIDback = \Input::file('inputIDback');
            $idBack_path = $inputIDback;
            $filename2 = $this->AmazonS3->uploadImage($idBack_path, $user, 'ID', 'back');
            $data['inputIDback'] = $filename2;
        }

        try {
            $member = $this->MemberRepository->register($data);
        } catch (\Exception $e) {
            $response = array('status' => 1,'msg' => $e->getMessage() );
            return response()->json($response);
        }


        session(['last_member_register' => $member]);
        \Session::flush();

        $response = array('status' => 0,'msg' => \Lang::get('success.title'),'showText'=>\Lang::get('success.text2'),"returnUrl"=>route('login', ['lang' => \App::getLocale()]) );
        return response()->json($response);


        

        $data = \Session::get('data');

        if (!$direct = $this->MemberRepository->findByUsername($data['direct_id'])) {

             return \Response::json([
                'type'  =>  'error',
                'message'   =>  $e->getMessage()
            ]);

        }else{
            
            if($direct->process_status != 'Y'){ // ban or processing weilun
                 return \Response::json([
                'type'  =>  'error',
                'message'   =>  $e->getMessage()
                ]);
            }
            // ban or processing weilun
            if ($Checkban = $this->MemberRepository->findUser(trim($data['direct_id']))) { 
              if($Checkban->is_ban ==0)
                 return \Response::json([
                    'type'  =>  'error',
                    'message'   =>  $e->getMessage()
                ]);
            }

        }
    

        try {
            $member = $this->MemberRepository->register($data);
        } catch (\Exception $e) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  $e->getMessage()
            ]);
        }

        //print_r($member);
        
      //  \Cache::forget('member.' . $user->id);
        session(['last_member_register' => $member]);
        \Session::flush();
        /*
        return \Response::json([
            'type'  => 'success',
            'message' => \Lang::get('message.registerSuccess'),
            'redirect' => route('registerSuccess', ['lang' => \App::getLocale()])
        ]);
         */
        return view('front.registerSuccess', ['lang' => \App::getLocale()]);
    }

    /**
     * Update Account Settings
     * @return [type] [description]
     */
    public function postUpdateBank (Request $request) {
        
        $SPassword = $request->SPassword;
        $getData = $request->Data;
        $data = json_decode($getData,true); //$data["Data"]["getPassword1"] example
        $user = \Sentinel::getUser();
        //$member = $user->member;
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        if ($member->secret_password != $SPassword) {
            return response()->json(['SPassword'=>"1"]);
        }

        $detail = $member->detail;
        foreach ($detail->getAttributes() as $k => $d) {
            if (isset($data["Data"][$k])) 
                $detail->{$k} = $data["Data"][$k];
        }
        
        $detail->save();
        $tittle = \Lang::get('message.accountUpdateSuccess');
        $text = \Lang::get('message.accountUpdateSuccess');
        return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>""]);
    }
    
    public function postUpdateAccount (Request $request) {
        
        $SPassword = $request->SPassword;
        $getData = $request->Data;
        $data = json_decode($getData,true); //$data["Data"][0]["getPassword1"] example
        $phone1 = $data["Data"][0]["phone1"];
        if(empty($phone1)){
            $tittle = \Lang::get('error.inputPhoneEmpty');
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>""]);
        }
        //$data = \Input::get('data');
        $user = \Sentinel::getUser();
        //$member = $user->member;
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        if ($member->secret_password != $SPassword) {
            return response()->json(['SPassword'=>"1"]);
        }
        
        $detail = $member->detail;
        $detail->phone1 = $phone1;
        $detail->save();
        $tittle = \Lang::get('message.accountUpdateSuccess');
        $text = \Lang::get('message.accountUpdateSuccess');
        return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>""]);
    }

    public function postUpdatePassword (Request $request) {
        
        $SPassword = $request->SPassword;
        $getData = $request->Data;
        $data = json_decode($getData,true); //$data["Data"][0]["getPassword1"] example
        $getPassword1 = $data["Data"][0]["getPassword1"];
        $getPassword2 = $data["Data"][0]["getPassword2"];

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        
        if ($member->secret_password != $SPassword) {
            return response()->json(['SPassword'=>"1"]);
        }


        if (!empty($getPassword1)) {
            if($CheckPass = \DB::table('ban_setting')->where('content',strtolower($getPassword1))->value('id')){
                $response = array('result' => 1, 'showTittle' => \Lang::get('error.passwordfail'),"showText"=>\Lang::get('error.passwordfailtext'));
                return response()->json($response);
            }
            $ispass = $this->CheckingService->CheckPassword($getPassword1);
            if($ispass == false){
                $response = array('result' => 1, 'showTittle' => \Lang::get('register.passwordRemind'),"showText"=>"");
                return response()->json($response);
            }
        }

        if (!empty($getPassword2)) {
            if($CheckPass = \DB::table('ban_setting')->where('content',strtolower($getPassword2))->value('id')){
                $response = array('result' => 1, 'showTittle' => \Lang::get('error.Spasswordfail'),"showText"=>\Lang::get('error.Spasswordfailtext'));
                return response()->json($response);
            }
            $ispass = $this->CheckingService->CheckPassword($getPassword2);
            if($ispass == false){
                $response = array('result' => 1, 'showTittle' => \Lang::get('register.passwordRemind'),"showText"=>"");
                return response()->json($response);
            }
        }


        

        $userData = [];

        if (isset($getPassword1)) {
            if ($getPassword1 != '') {
                $userData['password'] = $getPassword1;
                $userData['password2'] = $getPassword1;
                
                //$updatepass = $this->MemberRepository->updatePasswordAPI($data['password']);
            }
        }
        if (count($userData) > 0) {
            \Sentinel::update($user, $userData);
        }
        if (isset($getPassword2)) {
            if ($getPassword2 != '') {
                $member->secret_password = $getPassword2;
            }
        }

        $member->save();
        \DB::table('users')->where('id',$user->id)->update(['is_rstpass' => 1]);
        \Cache::forget('member.'. $user->id);
        $tittle = \Lang::get('message.accountUpdateSuccess');
        $text = \Lang::get('message.accountUpdateSuccess');
        return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>""]);
    }
    
    
    public function postUpdateDeclare () {
        
        
        $user = \Sentinel::getUser();
        //$member = $user->member;
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        
        $destinationPath = public_path('/declare/');
        
        $declare_path = \Input::file('inputDeclare');
        
      //  print_r($declare_path);
        
        $filename1 = md5($user->email).$declare_path->getClientOriginalName();
        $declare_path->move($destinationPath, $filename1);
        
        $declare_path2 = \Input::file('inputDeclare2');
        
        if (file_exists($declare_path2)) {
            
            $filename2 = md5($user->email).$declare_path2->getClientOriginalName();
            $declare_path2->move($destinationPath, $filename2);
            
            $member['declare_form2'] = $filename2;
            
        }
        
        $member['declare_form'] = $filename1;
        

        
       $member->save();
        
        
        //print_r($updateinfo);
        
        
        
        return view('front.settings.updateDeclareSuccess', ['lang' => \App::getLocale()]);
    }
    
    public function postUpdateLP (Request $request) {
        
        $image = \Input::file('image');
        if(empty($image)){
            return response()->json(['result'=>1]);
            exit();
        }
        $user = \Sentinel::getUser();
        
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        if(!$filename1 = $this->AmazonS3->uploadImage($image, $user, 'LP', '')){
            return response()->json(['result'=>1]);
        }     
        $member['lpoa'] = $filename1;
        $member->save();

        return response()->json(['result'=>0]);
    }

    public function postUpdateID () {
        $image1 = \Input::file('image1');
        $image2 = \Input::file('image2');
        if(empty($image1) || empty($image2)){
            return response()->json(['result'=>1]);
            exit();
        }

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        $idFront_path = $image1;
        $idBack_path = $image2;
        $checking = $this->UploadpicService->CheckFileSupported($idFront_path);
        if(empty($checking)){
            $tittle = \Lang::get('error.uploadProfileFail');
            $response = array('status' => 1,'msg' => $tittle );
            return response()->json($response);
        }
        $filename1 = $this->AmazonS3->uploadImage($idFront_path, $user, 'ID', 'front');

        $checking = $this->UploadpicService->CheckFileSupported($idBack_path);
        if(empty($checking)){
            $tittle = \Lang::get('error.uploadProfileFail');
            $response = array('status' => 1,'msg' => $tittle );
            return response()->json($response);
        }

        $filename2 = $this->AmazonS3->uploadImage($idBack_path, $user, 'ID', 'back');


        $member['id_front'] = $filename1;
        $member['id_back'] = $filename2;
        $member['reject'] = 0;
        $member->save();

        return response()->json(['result'=>0]);
    }

    public function postUpdateProfile () {
        $image1 = \Input::file('image1');

        if(empty($image1) ){
            return response()->json(['result'=>1]);
        }else{

            $fileArray = array('image' => $image1);
            $rules = array(
              'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000' // max 10000kb
            );
            $validator = Validator::make($fileArray, $rules);
            if ($validator->fails())
            {
                return response()->json(['result'=>1]);
            }
        }

        $img = Image::make(\Input::file('image1'));
        $img->resize(500, 500);
        $b64 = base64_encode($img->encode()->encoded);

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $detail = $member->detail;
        $detail->profile_pic_64 = $b64;
        $detail->save();
        return response()->json(['result'=>0]);
    }
    
    public function license_agreement (Request $request) { //weilun

        
        $getData = $request->Data;
        $data = json_decode($getData,true); //$data["Data"][0]["getPassword1"] example
        $signData =  $data["Data"]["ctlSignature_data"];
        $signDataSmooth =  $data["Data"]["ctlSignature_data_canvas"];
        $lpoa_Language =  $data["Data"]["lpoa"];
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $fName =  $user->first_name; // first name
        $identification_number = $member->detail->identification_number; // ic




        $img = imagecreate(2000, 250);
        $size=100;
        $textbgcolor = imagecolorallocate($img, 255, 255, 255);
        $textcolor = imagecolorallocate($img, 0, 192, 255);
        $font= app_path().'/fpdff/simhei.ttf';
        $black=imagecolorallocate($img,0,0,255);
      
        $txt = $fName;
        imagettftext($img,$size, 0, 150, 200,$black, $font, $txt);
        ob_start();
        imagepng($img, app_path().'/fpdff/sign/' . $identification_number,0,NULL);
            
            
        include(app_path().'/fpdff/common/license.php');

        
        $im = null; // check signdata
        if (strlen($signDataSmooth) > 0)  {
            $im = GetSignatureImageSmooth($signDataSmooth);
        }
        else if (strlen($signData) > 0) {
            $im = GetSignatureImage($signData);
        }
        if (!isset($im)) {
            
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  \Lang::get('error.Signature_data')
                                   ]);
        }


        if($lpoa_Language == "chs_lpoa") {// $lpoa_Language == chs_lpoa
            $filpath_pdf = app_path().'/fpdff/lpoa_chs.pdf';
            
            $pdf = new \setasign\Fpdi\Fpdi();

            $pdf->AddPage();
            $pageCount = $pdf->setSourceFile($filpath_pdf);
            //$tplId = $pdf->importPage(1);
            $tplIdx = $pdf->importPage(1);
            for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                // import a page
                $templateId = $pdf->importPage($pageNo);
                // get the size of the imported page
                $size = $pdf->getTemplateSize($templateId);
                
                
                // create a page (landscape or portrait depending on the imported page size)
                
                // use the imported page
                $pdf->useTemplate($templateId);
                
            }
            // use the imported page and place it at point 10,10 with a width of 100 mm
            $pdf->SetFont('Arial');
            $pdf->SetTextColor(0,0,255);
            
            $pdf->SetXY(140, 220);
            $pdf->Write(0, $identification_number);

            $pdf->SetXY(10, 230);
            $pdf->Write(0, $user->username);
            
            $dateFormat="d-M-Y";
            $timeNdate=gmdate($dateFormat);

            
            $fileName = $user->username.'_'. $identification_number.'_'.$timeNdate.'.png';
            $pdf->SetXY(140, 270);
            $pdf->Write(0, $timeNdate);
            
            imagepng($im, app_path().'/fpdff/sign/' . $fileName,0,NULL);
            
            $pdf->Image(app_path().'/fpdff/sign/'  . $fileName,35,255,20,20,'PNG');
            $pdf->Image(app_path().'/fpdff/sign/'  . $identification_number,50,213,15,10,'PNG');
        }
        
        else if ($lpoa_Language == "en_lpoa") {
            $filpath_pdf = app_path().'/fpdff/lpoa_eng.pdf';
            $pdf = new \setasign\Fpdi\Fpdi();
            $pdf->AddPage();
            $pageCount = $pdf->setSourceFile($filpath_pdf);
            //$tplId = $pdf->importPage(1);
            $tplIdx = $pdf->importPage(1);
            for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                // import a page
                $templateId = $pdf->importPage($pageNo);
                // get the size of the imported page
                $size = $pdf->getTemplateSize($templateId);
                
                
                // create a page (landscape or portrait depending on the imported page size)
                
                // use the imported page
                $pdf->useTemplate($templateId);
                
            }
            // use the imported page and place it at point 10,10 with a width of 100 mm
            $pdf->SetFont('Arial');
            $pdf->SetTextColor(0,0,255);

            
            $pdf->SetXY(145, 33);
            $pdf->Write(0, $identification_number);

            $pdf->SetXY(50, 42);
            $pdf->Write(0, $user->username);
            
            
            
            $dateFormat="d-M-Y";
            $timeNdate=gmdate($dateFormat);
            
            $fileName = $user->username.'_'.$identification_number.'_'.$timeNdate.'.png';
            $pdf->SetXY(70, 275);
            $pdf->Write(0, $timeNdate);
            
            imagepng($im, app_path().'/fpdff/sign/' . $fileName,0,NULL);
            $pdf->Image(app_path().'/fpdff/sign/'  . $fileName,150,255,20,20,'PNG');
            $pdf->Image(app_path().'/fpdff/sign/'  . $identification_number,70,255,15,10,'PNG');
            $pdf->Image(app_path().'/fpdff/sign/'  . $identification_number,65,25,15,10,'PNG');
            
            # code...
        }

        
        $filename = md5($user->email).$identification_number.'.pdf';
        $pdf->Output('LP/'.$filename,'F'); //save the file
 
        
        $this->AmazonS3->uploadFile('LP/'.$filename, public_path('LP/').$filename);
        // $filename = uniqid().$fName.'.pdf';
        // $pdf->Output(public_path('/LP/').$filename,'F'); //save the file
        unlink(app_path().'/fpdff/sign/'  . $fileName);
        unlink(app_path().'/fpdff/sign/'  . $identification_number);
        $member['lpoa'] = $filename;
        $member['lpoa_status'] = "Y";
        $member->save();
        
        // return view('front.misc.updateLPSuccess', ['lang' => \App::getLocale()]);
        //return View::make('front.misc.updateLPSuccess');
        //return redirect()->route('misc.updateLPSuccess', ['lang' => \App::getLocale()]);
        
        $tittle = \Lang::get('success.lptext');
        $text = \Lang::get('message.accountUpdateSuccess');
        return response()->json(['status'=>"0","msg"=>$tittle,"showText"=>""]);
        
        
        
    }
    
    public function postUpdateDeclareStatus () {
        

    }

    /**
     * Renew or upgrade package
     * @return [type] [description]
     */
    public function postUpgrade () {
        $data = \Input::get('data');
        $user = \Sentinel::getUser();
        //$member = $user->member;
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        
        if ($member->secret_password != trim($data['s'])) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  \Lang::get('error.securityPasswordError')
            ]);
        }

        try {
            $member = $this->MemberRepository->upgrade($member, $data);
        } catch (\Exception $e) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  $e->getMessage()
            ]);
        }
/*
        if (env('APP_ENV') == 'local') { // local
            $wallet = $member->wallet;
            $this->SharesRepository->repurchasePackage($member, $wallet->purchase_point, $wallet);
        } else { // production
            dispatch(new SharesAfterRegisterJob($member))->onQueue('queue-shares-register');
        }
*/
        \Cache::forget('member.' . $user->id);

       
        return \Response::json([
           'type'  => 'success',
           'message' => \Lang::get('message.upgradeSuccess'),
           'redirect' => route('member.upgradeSuccess', ['lang' => \App::getLocale()])
           ]);
    }
    
    public function addMT5 () {
        
        $user = \Sentinel::getUser();
        //$member = $user->member;
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        
        try {
            $mt5 = $this->MemberRepository->addMT5($user,$member);
        } catch (\Exception $e) {
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  $e->getMessage()
                                   ]);
        }
        
        //print_r($user);
        return \Response::json([
                               'type'  =>  'success',
                               'message'   =>  \Lang::get('message.MT5addSuccess'),
                               'redirect' => route('misc.mt5statement', ['lang' => \App::getLocale()])
                               ]);
    }
    
    public function addMT5self () {
        
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        try {
            $mt5 = $this->MemberRepository->addMT5self($user,$member);
        } catch (\Exception $e) {
            $response = array('status' => 1, 'msg' => $e->getMessage());
            return response()->json($response);
        }

        $response = array('status' => 0, 'msg' => \Lang::get('message.MT5addSuccess'));
        return response()->json($response);
    }
    
    public function addFund () {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        
        try {
            $checkAccount = $this->MemberRepository->addFundChecking($user,$member);
        } catch (\Exception $e) {
            $response = array('status' => 1, 'msg' => $e->getMessage());
            return response()->json($response);
        }

        try {
            $mt5 = $this->MemberRepository->addFund($user,$member);
        } catch (\Exception $e) {
            $response = array('status' => 1, 'msg' => $e->getMessage());
            return response()->json($response);
        }
        
        $response = array('status' => 0, 'msg' => \Lang::get('message.FundaddSuccess'));
        return response()->json($response);
    }

    public function addFundExtra () {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
            
        $checkAccountExtra = \DB::table('Member_Account_Fund_Extra')->where('member_id', '=', $member->id)->count();
        if($checkAccountExtra > 49){
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  \Lang::get('common.addFundExist')
                                   ]);
        }
        
        $emptyInvestingAccount = \DB::table('Member_Account_Fund')->where('member_id', '=', $member->id)->where('balanceB', 0)->count();
        if($emptyInvestingAccount > 0){
            $extraAccount = \DB::table('Member_Account_Fund')->where('member_id', '=', $member->id)->where('balanceB', 0)->orderBy('id', 'ASC')->first(['id']);
            \DB::table('Member_Account_Fund_Extra')->insert(
                                        [
                                        'member_id' => $member->id,
                                        'account_fund_id' => $extraAccount->id,
                                        'created_at' => \Carbon\Carbon::now()
                                        ]
                                        );
        }
        else{
            // try {
            //     $checkAccount = $this->MemberRepository->addFundChecking($user,$member);
            // } catch (\Exception $e) {
            //     return \Response::json([
            //                            'type'  =>  'error',
            //                            'message'   =>  $e->getMessage()
            //                            ]);
            // }

            try {
                $mt5 = $this->MemberRepository->addFund($user,$member);
            } catch (\Exception $e) {
                return \Response::json([
                                       'type'  =>  'error',
                                       'message'   =>  $e->getMessage()
                                       ]);
            }

            $extraAccount = \DB::table('Member_Account_Fund')->where('member_id', '=', $member->id)->orderBy('id', 'DESC')->first(['id']);
            \DB::table('Member_Account_Fund_Extra')->insert(
                                        [
                                        'member_id' => $member->id,
                                        'account_fund_id' => $extraAccount->id,
                                        'created_at' => \Carbon\Carbon::now()
                                        ]
                                        );
        }
        
        return \Response::json([
                               'type'  =>  'success',
                               'message'   =>  \Lang::get('message.FundaddSuccess'),
                               'redirect' => route('misc.fundstatement', ['lang' => \App::getLocale()])
                               ]);
    }
    
    /**
     * Get all direct members - DataTable
     * @return [type] [description]
     */
    public function getRegisterHistory () {
        $user = \Sentinel::getUser();
        $member = $user->member;
        return $this->MemberRepository->registerHistory($member);
    }

    /**
     * Search Binary Tree - VisJS
     * @return array [array of children]
     */
    public function getBinary (Request $req) {
        $data = \Input::get('data');
        $user = \Sentinel::getUser();
        //$member = $user->member;
        $member = $this->MemberRepository->findById($user->id);

        if (\Input::has('m')) { // from more link
            if (!$req->session()->has('binary.session')) {
                return \Lang::get('error.securityPasswordError');
            } else {
                if (!$target = $this->MemberRepository->findByUsername(trim(\Input::get('u')))) {
                    return \Lang::get('error.memberNotFound');
                }

                if ($target->level <= $member->level && $member->username != $target->username) {
                    return \Lang::get('error.memberNotFound');
                }

                session(['binary.session' => [
                    'top' => $member,
                    'current' => $target
                ]]);

                return view('front.network.binaryTree')->with('model', $target);
            }
        }

        if (trim($data['s']) != $member->secret_password) {
            return \Lang::get('error.securityPasswordError');
        }

        if (!$target = $this->MemberRepository->findByUsername(trim($data['u']))) {
            return \Lang::get('error.memberNotFound');
        }

        if ($target->level <= $member->level && $member->username != $target->username) {
            return \Lang::get('error.memberNotFound');
        }

        if ($target->id != $member->id) {
            $left = explode(',', $member->left_children);
            $right = explode(',', $member->right_children);

            if (!in_array($target->id, $left) && !in_array($target->id, $right)) {
                return \Response::json([
                    'type'  =>  'error',
                    'message'   => \Lang::get('error.memberNotFound')
                ]);
            }
        }

        session(['binary.session' => [
            'top' => $member,
            'current' => $target
        ]]);

        return view('front.network.binaryTree')->with('model', $target);
    }

    /**
     * Search Binary Tree Top - VisJS
     * @return array [array of children]
     */
    public function getBinaryTop (Request $req) {
        if (!$req->session()->has('binary.session')) {
            return \Lang::get('error.binarySessionError');
        }

        if (!\Input::has('type')) {
            return \Lang::get('error.binarySessionError');
        }

        $type = trim(\Input::get('type'));
        if ($type != 'top' && $type != 'up') {
            return \Lang::get('error.binarySessionError');
        }

        $sessions = $req->session()->get('binary.session');
        if ($type == 'top') {
            if (!$target = $this->MemberRepository->findByUsername($sessions['top']->username)) {
                return \Lang::get('error.memberNotFound');
            }
        } else {
            if (!$parent = $this->MemberRepository->findByUsername($sessions['current']->username)) {
                return \Lang::get('error.memberNotFound');
            }

            if (!$target = $parent->parent()) {
                return \Lang::get('error.memberNotFound');
            }
        }

        $user = \Sentinel::getUser();
        $member = $user->member;
        if ($target->level <= $member->level && $member->username != $target->username) {
            return \Lang::get('error.memberNotFound');
        }

        session(['binary.session' => [
            'top' => $member,
            'current' => $target
        ]]);

        return view('front.network.binaryTree')->with('model', $target);
    }

    /**
     * Get Member detail from binary - VisJS
     * @return html
     */
    public function getBinaryModal (Request $req) {
        if (!$req->session()->has('binary.session')) {
            return \Lang::get('error.binarySessionError');
        }

        if (!\Input::has('u')) {
            return \Lang::get('error.binarySessionError');
        } else {
            $username = trim(\Input::get('u'));
            if (!$target = $this->MemberRepository->findByUsername(trim($username))) {
                return \Lang::get('error.memberNotFound');
            }
        }

        $user = \Sentinel::getUser();
        $member = $user->member;
        if ($target->level <= $member->level && $member->username != $target->username) {
            return \Lang::get('error.memberNotFound');
        }

        return view('front.network.binaryModal')->with('model', $target);
    }
    
     public function getUpline () {
        $data = \Input::get('data');
        $user = \Sentinel::getUser();
        //$member = $user->member;
        $member = $this->MemberRepository->findByUsername(trim($user->username));
/*
        if (!$target = $this->MemberRepository->findByUsername(trim($data['u']))) {
            return \Response::json([
                'type'  =>  'error',
                'message'   => \Lang::get('error.memberNotFound')
            ]);
        }

        if ($target->level <= $member->level && $member->username != $target->username) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  \Lang::get('error.memberNotFound')
            ]);
*/
        return \Response::json([
            'type'  =>  'success',
            'redirect'  =>  route('network.upline', ['lang' => \App::getLocale()]) . '?rid=' . $target->username
        ]);

    }
    
    public function getGroupSalesStatement (Request $request) {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        if (!$target = $this->MemberRepository->findByUsername($request->username)) {
            $response = array('status' => 1, 'msg' => \Lang::get('error.memberNotFound'));
            return response()->json($response);
        }

        if ($target->level <= $member->level && $member->username != $target->username) {
            $response = array('status' => 1, 'msg' => \Lang::get('error.memberNotFound'));
            return response()->json($response);
        }
        
        if($member->username != $target->username)
        {
            $checknetwork = \DB::table('Member_Network')->where('username', '=', $target->username)->where('parent_username', '=', $member->username)->first();
            
            if(count($checknetwork) == 0){
                $response = array('status' => 1, 'msg' => \Lang::get('error.memberNotFound'));
                return response()->json($response);
            }
        }

        $response = array('status' => 0, 'msg' => '', 'targetId' => $target->id);
        return response()->json($response);
    }

    /**
     * Search Hierarchy by Username - JSTree
     * @param  [type] $type [description]
     * @return [type]       [description]
     */
    public function getUnilevelTree (Request $request) {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        if (!$target = $this->MemberRepository->findByUsername($request->username)) {
            $response = array('status' => 1, 'msg' => \Lang::get('error.memberNotFound'));
            return response()->json($response);
        }

        if ($target->level <= $member->level && $member->username != $target->username) {
            $response = array('status' => 1, 'msg' => \Lang::get('error.memberNotFound'));
            return response()->json($response);
        }
        
        if($member->username != $target->username){
            $checknetwork = \DB::table('Member_Network')->where('username', '=', $target->username)->where('parent_username', '=', $member->username)->first();
            
            if(count($checknetwork) == 0){
                $response = array('status' => 1, 'msg' => \Lang::get('error.memberNotFound'));
                return response()->json($response);
            }
        }

        $response = array('status' => 0, 'msg' => '', 'targetId' => $target->id);
        return response()->json($response);
    }

    /**
     * Search Unilevel Tree - JSTree
     * @return array [array of children]
     */
    public function getUnilevel (Request $request) {
        if($request->targetId){
            $member = \DB::table('Member')->where('id', $request->targetId)->first();
            $target = $member;
        }else{
            $user = \Sentinel::getUser();
            $member = $this->MemberRepository->findByUsername(trim($user->username));
            $target = $member;
        }
        $data = [];
        $temp = [];
        
        $children = $this->MemberRepository->findDirect($target);
        if (count($children) > 0) {
            foreach ($children as $child) {
                unset($temp);
                $userdetail = \DB::table('users')->join('Member', 'users.username', '=', 'Member.username')->where('users.username', '=',$child->username)->first();
               
                $totaltransfer1 = \DB::table('Member_Blacklist')->where('member_id', '=', $child->id)->where('Member_Blacklist.blacklist_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('Member_Blacklist.blacklist_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->where('b_book', '<>', 0)->sum('b_book');
                
                $totaltransfer2 = \DB::table('Member_Wallet_Statement_Fund')->where('member_id', '=',$child->id)->where('action_type', '=', 'Transfer to MT5')->where('created_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('created_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->sum('b_amount');
                
                $transferamount1 = ($totaltransfer2 * 2) - $totaltransfer1;

                $rank = \DB::table('Member_Sales')->where('member_id', '=',$child->id)->first();
                if(isset($rank->rank)) $childRank = $rank->rank;
                else $childRank = '';

                $investtrfx = 0;
                $investcoin = 0;
                $investmargin = 0;
    
                $investtrfx = \DB::table('Member_Account_Fund')->where('member_id', $child->id)->sum('balanceB');
                $coinWallet = \DB::table('Capx_Member_Coin')->where('member_id', $child->id)->first();

                $coin = \DB::table('Capx_Coin')->get();
                foreach ($coin as $coinData) {
                    $symbol = $coinData->symbol;
                    $investcoin += $coinData->price * $coinWallet->$symbol;
                }

                $margin = \DB::table('Capx_Margin')->get();
                foreach ($margin as $marginData) {
                    $symbol = $marginData->symbol;
                    $investmargin += $marginData->current_price * $coinWallet->$symbol;
                }

                $totalinvest = $investtrfx * 3 + $investcoin + $investmargin;

                $children2 = $this->MemberRepository->findDirect($child);

                $temp["id"] = $child->id;
                $temp["firstName"] = $userdetail->first_name;
                $temp["username"] = $child->username;
                $temp["transferamount"] = number_format($transferamount1,2);
                $temp["rank"] = $childRank;
                $temp["totalinvest"] = number_format($totalinvest,2);
                $temp["profile"] = $child->detail->profile_pic_64;
                if (count($children2) > 0) $temp["children"] = 1;
                else $temp["children"] = 0;

                array_push($data,$temp);
            }
        }
        $response = array('status' => 0, 'msg' => '', 'model' => $data);
        return response()->json($response);
    }
    
    public function treeView(){
        $Categorys = Member::where('direct_id', '=', 0)->get();
        $tree='<ul id="browser" class="filetree"><li class="tree-view"></li>';
        foreach ($Categorys as $Category) {
            $tree .='<li class="tree-view closed"<a class="tree-name">'.$Category->username.'</a>';
            if(count($Category->childs)) {
                $tree .=$this->childView($Category);
            }
        }
        $tree .='<ul>';
        // return $tree;
        return view('network.treeview',compact('tree'));
    }
    public function childView($Category){
        $html ='<ul>';
        foreach ($Category->childs as $arr) {
            if(count($arr->childs)){
                $html .='<li class="tree-view closed"><a class="tree-name">'.$arr->name.'</a>';
                $html.= $this->childView($arr);
            }else{
                $html .='<li class="tree-view"><a class="tree-name">'.$arr->name.'</a>';
                $html .="</li>";
            }
            
        }
        
        $html .="</ul>";
        return $html;
    }
    
    /**
     * Find member info when register
     * @return html
     */
    public function getMemberModal () {

        $model = false;
        $modelIsban = false;
        $msg = \Lang::get('register.modal.notFound');
        if (trim(\Input::get('id')) != '') {

            if(!$model = $this->MemberRepository->findByUsername(trim(\Input::get('id')))){
                $msg = \Lang::get('register.modal.notFound');
                return response()->json(['status'=>"1","msg"=>$msg]);
            }
            if(!$modelIsban = $this->MemberRepository->findUser(trim(\Input::get('id')))){
                $msg = \Lang::get('register.modal.notFound');
                return response()->json(['status'=>"1","msg"=>$msg]);
            }

            if($modelIsban->special_group_block == 1){
                $msg = \Lang::get('register.modal.notFound');
                return response()->json(['status'=>"1","msg"=>$msg]);
            }

            if($model->process_status != 'Y' || $modelIsban->is_ban ==0){
                $msg = \Lang::get('error.transferFail');
                return response()->json(['status'=>"1","msg"=>$msg]);
            }else{
                $msg = \Lang::get('register.modal.title');
                return response()->json(['status'=>"0","model"=>$model,"modelIsban"=>$modelIsban,"msg"=>$msg]);
            }
            
        }else{
            $msg = \Lang::get('register.modal.notFound');
            return response()->json(['status'=>"1","msg"=>$msg]);

        }
    }

    public function checkNetwork ($lang, $username) {
        $model = false;
        $modelIsban = false;
        $msg = \Lang::get('register.modal.notFound');
        $user = \Sentinel::getUser();

        if ($username) {

            if(!$model = $this->MemberRepository->findByUsername($username)){
                $msg = \Lang::get('register.modal.notFound');
                return response()->json(['status'=>"1","msg"=>$msg]);
            }
            if(!$modelIsban = $this->MemberRepository->findUser($username)){
                $msg = \Lang::get('register.modal.notFound');
                return response()->json(['status'=>"1","msg"=>$msg]);
            }

            if($modelIsban->special_group_block == 1){
                $msg = \Lang::get('register.modal.notFound');
                return response()->json(['status'=>"1","msg"=>$msg]);
            }

            if(!in_array($user->username, config('autosettings.transferToAll'))){
                $checknetwork = \DB::table('Member_Network')->where('username', $username)->where('parent_username', $user->username)->first();
                if(empty($checknetwork)){
                    $msg = \Lang::get('register.modal.notFound');
                    return response()->json(['status'=>"1","msg"=>$msg]);
                }
            }

            if($model->process_status != 'Y' || $modelIsban->is_ban ==0){
                $msg = \Lang::get('error.transferFail');
                return response()->json(['status'=>"1","msg"=>$msg]);
            }else{
                $rank = \DB::table('Member_Sales_Summary')->where('member_id', $model->id)->orderBy('date', 'DESC')->first(['rank']);
                if(isset($rank)) $memberRank = $rank->rank?$rank->rank:'-';
                else $memberRank = '-';
                $msg = \Lang::get('register.modal.title');
                return response()->json(['status'=>"0","model"=>$model,"modelIsban"=>$modelIsban,"msg"=>$msg, "rank" => $memberRank]);
            }
            
        }else{
            $msg = \Lang::get('register.modal.notFound');
            return response()->json(['status'=>"1","msg"=>$msg]);

        }
    }
    
    public function getMemberRegisterModal () {
        $model = false;
        $modelIsban = false;
        $msg = \Lang::get('register.modal.notFound');
        if (trim(\Input::get('id')) != '') {

            if(!$model = $this->MemberRepository->findByUsername(trim(\Input::get('id')))){
                $msg = \Lang::get('register.modal.notFound');
                return response()->json(['status'=>"1","msg"=>$msg]);
            }
            if(!$modelIsban = $this->MemberRepository->findUser(trim(\Input::get('id')))){
                $msg = \Lang::get('register.modal.notFound');
                return response()->json(['status'=>"1","msg"=>$msg]);
            }
            if($modelIsban->special_group_block == 1){
                $msg = \Lang::get('register.modal.notFound');
                return response()->json(['status'=>"1","msg"=>$msg]);
            }

            if($model->process_status != 'Y' || $modelIsban->is_ban ==0 || $modelIsban->special_group_block ==1){
                $msg = \Lang::get('error.transferFail');
                return response()->json(['status'=>"1","msg"=>$msg]);
            }else{
                $msg = \Lang::get('register.modal.title');
                return response()->json(['status'=>"0","model"=>$model,"modelIsban"=>$modelIsban,"msg"=>$msg]);
            }
            
        }else{
            $msg = \Lang::get('register.modal.notFound');
            return response()->json(['status'=>"1","msg"=>$msg]);

        }
    }
    
    public function getDepositModal () {
        $model = false;
        if (trim(\Input::get('u')) != '') {
            $model = $this->MemberRepository->findByUsername(trim(\Input::get('u')));
        }
        return view('front.misc.modalDeposit')->with('model', $model);
    }
    
    public function getSubscribeModal () {
        $model = false;
        if (trim(\Input::get('u')) != '') {
            $model = $this->MemberRepository->findByUsername(trim(\Input::get('u')));
        }
        return view('front.misc.modalSubscribe')->with('model', $model);
    }
    
    public function getUnsubscribeModal () {
        $model = false;
        if (trim(\Input::get('u')) != '') {
            $model = $this->MemberRepository->findByUsername(trim(\Input::get('u')));
        }
        return view('front.misc.modalUnsubscribe')->with('model', $model);
    }
    
    public function getDetailsModal () {

        if (\Input::has('id')) $bookA = \Input::get('id');
        else{
            $response = array('status' => 1, 'msg' => 'Something went wrong');
            return response()->json($response);
        }

        $order = \DB::table('Orders_Fund')->where('bookA', '=', $bookA)->where('status', '=', 'P')->orderBy('bookAorderid', 'desc')->get();
        $orderDetails = array();
        
        foreach($order as $orders){
            unset($temp);
            $historyorders = \DB::connection('mysql2')->table('historyposition')->where('OrderId', '=', $orders->bookAorderid)->first();
            
            $opentime = 'Open : '.$historyorders->OpenTime;
            
            $closetime = 'Close : '.$historyorders->CloseTime;
            
            //$txndate = 'Open : '.$opentime.'\n Close : '.$closetime;
            
            $Adetails = $orders->tradecommand.' - '.$historyorders->Symbol;
            
            if($orders->tradecommand == 'BUY')
                $tradecommand = 'SELL';
            else
                $tradecommand = 'BUY';
            
            $lotsize = $historyorders->Volume / 10000;
            
            if(substr($historyorders->Symbol, -1) == '+')
                $symbol = str_replace('+', '-', $historyorders->Symbol);
            else
                $symbol = str_replace('-', '+', $historyorders->Symbol);
            
            $Bdetails = $tradecommand.' - '.$symbol;
            
            $Awin = $historyorders->Profit.' ('.\Lang::get('common.cal.swap'). ' : '.$historyorders->Storage.')';
            
            $partner = \DB::connection('mysql3')->table('user')->where('UserId', '=', $historyorders->PartnerId)->first();
            $partnername = '';
            if(count($partner) > 0)
            {
                $current_lang = \App::getLocale();
            
                if($current_lang == 'en')
                    $partnername = $partner->FirstName.' '.$partner->LastName;
                else
                    $partnername = $partner->ChineseName;
            }

            $temp["opentime"] = $opentime;
            $temp["closetime"] = $closetime;
            $temp["Adetails"] = $Adetails;
            $temp["Bdetails"] = $Bdetails;
            $temp["Awin"] = $Awin;
            $temp["lotsize"] = $lotsize;
            $temp["partnername"] = $partnername;
            array_push($orderDetails, $temp);
        }

        $response = array('status' => 0, 'msg' => '', 'model' => $orderDetails);
        return response()->json($response);
    }
    
    public function sendPassword ($username, $first_name,$password,$secretpass) {
        $title = 'Hi,'.$first_name;
  
        // Mail::send('front.emails.forgotpassword', ['title' => $title, 'password' => $password, 'secretpass' => $secretpass, 'email' => $username], function ($message) use ($username)
        // {

        //     $message->subject('Your password');

        //     $message->to($username);

        // });

        return true;
    }

    public function amazonRead ($lang, $folder, $filename) {
        $specialFolder = array('incomeSummary','incomeDetails');
        if(in_array($folder, $specialFolder)){
            if($folder == 'incomeSummary') $getDate = \DB::table('Member_Commission_Report_Consolidated')->where('summary_report_name', $filename)->first();
            else if($folder == 'incomeDetails') $getDate = \DB::table('Member_Commission_Report_Consolidated')->where('detail_report_name', $filename)->first();
            $split = explode('-', $getDate->commission_date);
            $folder = preg_replace('/(?<!\ )[A-Z]/', '/$0', $folder);
            if($split[0] > 2018) $folder = $folder."/".$getDate->commission_date;
        }
        else $folder = $folder;
        $url = $this->AmazonS3->loadImage($folder.'/'.$filename);
        return redirect($url);
    }

    public function amazonReadRegister ($folder, $filename) {
        $specialFolder = array('incomeSummary','incomeDetails');
        if(in_array($folder, $specialFolder)) $folder = preg_replace('/(?<!\ )[A-Z]/', '/$0', $folder);
        else $folder = $folder;
        $url = $this->AmazonS3->loadImage($folder.'/'.$filename);
        return redirect($url);
    }

    public function getDepositRecords () {
        
        $user = \Sentinel::getUser();
        $currentMember = $this->MemberRepository->findByUsername(trim($user->username));
        
        return $this->MemberRepository->findDepositRecords(true, $currentMember->id);
        
    }

    public function getDeposit () {
        $user = \Sentinel::getUser();
        $currentMember = $this->MemberRepository->findByUsername(trim($user->username));
        if($currentMember->process_status != 'Y'){
            return redirect(route('home', ['lang' => \App::getLocale()]));
        }

        $specialId = md5(uniqid().''.$currentMember->username);
        \DB::table('Tre_Gateway_login')->insert(['specialId' => $specialId, 'username' => trim($currentMember->username), 'password2' => trim($user->password2)]);
        
        if(\App::getLocale() == 'en') $url = 'https://www.tregateway.com/en/deposit/ExternalLogin?id='.$specialId;
        else $url = 'https://www.tregateway.com/zh/deposit/ExternalLogin?id='.$specialId;
        
        return redirect($url);
    }

    public function getFundStatement () {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        $account = \DB::table('Member_Account_Fund')->where('member_id', '=', $member->id)->where('balanceB', 0)->count();
        if($account > 0) $addAccountDisplay = false;
        else $addAccountDisplay = true;

        $data = [
                    'addAccountDisplay' => $addAccountDisplay
                ];

        return view('front.misc.fundStatement')->with($data);
    }

    public function getFundChecking (Request $request) {
        
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        $account = \DB::table('Member_Account_Fund')->where('member_id', '=', $member->id)->where('status', '=', 0)->where('system_manage', '=', 'N')->where('bookA', $request->bookA)->first();
        
        if(count($account) > 0){
            if($account->balanceB > 10000){
                $response = array('status' => 2, 'msg' => \Lang::get('error.transferFund'));
                return response()->json($response);
            }
        }else{
            $response = array('status' => 1, 'msg' => "This account does not exist.");
            return response()->json($response);
        }
    
        $checkWinAccount = \DB::table('Member_Account_Fund')->where('member_id', '=', $member->id)->where('winA', 1)->where('bookA', $account->bookA)->count();
        if($checkWinAccount > 0){
            $response = array('status' => 0, 'msg' => "", 'bookA' => $account->bookA);
            return response()->json($response);
        }

        $countWinAccount = \DB::table('Member_Account_Fund')->where('member_id', '=', $member->id)->where('winA', 1)->count();
        if($countWinAccount > 0){
            $winAccount = \DB::table('Member_Account_Fund')->where('member_id', '=', $member->id)->where('winA', 1)->get();
            foreach ($winAccount as $details) {
                if($details->balanceB < $details->ini_amount){
                    $failedMessage2 = str_replace(array('%%amount%%','%%bookA%%'), array((number_format($details->ini_amount*3,2)),$details->bookA), \Lang::get('error.minFundError'));
                    $response = array('status' => 2, 'msg' => $failedMessage2);
                    return response()->json($response);
                }
            }
        }

        $response = array('status' => 0, 'msg' => "", 'bookA' => $account->bookA);
        return response()->json($response);
    }

    public function wstatementShow ($lang, $id) {
        $model = \DB::table('Member_Wallet_Statement_Fund')->where('parent_batch_id', $id)->get();
        if(count($model) > 0) {
            foreach ($model as $data) {
                if(!(string)$data->remark_display){
                    $remark = $data->remark;
                }
                else{
                    $split = explode("/", $data->remark_display);
                    if(\Lang::get('transactionHistory.'.$split[0]) == ('transactionHistory.'.$split[0])){
                        $remark = $data->remark_display;
                    }
                    else{
                        $remark = \Lang::get('transactionHistory.'.$split[0]);
                        if($split[1]){
                            $detail = json_decode($split[1]);
                            foreach ($detail as $key => $value) {
                                if($key == "amount") $remark = str_replace('%%'.$key.'%%', number_format($value,2), $remark);
                                else if($key == "date") $remark = str_replace('%%'.$key.'%%', date_format($value, "Y-m-d"), $remark);
                                else $remark = str_replace('%%'.$key.'%%', $value, $remark);
                            }
                        }
                    }
                }
                $data->remark_display = $remark;
                $data->w_amount = number_format($data->w_amount, 2);
                $data->balance = number_format($data->balance, 2);
            }
            $response = array('status' => 0, 'msg' => '', 'model' => $model);
        }
        else $response = array('status' => 1, 'msg' => 'Something went wrong');
        return response()->json($response);
    }

    public function getUnsubscribeFund () {
        $id = \Input::get('id');
        $step = \Input::get('step');

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        $data = [
                    'user' => $user,
                    'id' => $id,
                    'step' => $step
                ];

        if($step == 1) return view('front.transaction.unsubscribefund')->with($data);
        else if($step == 2) return view('front.transaction.unsubscribefundConfirmation')->with($data);
        else if($step == 3) return view('front.transaction.unsubscribefund')->with($data);
    }

    public function getCommStatement () {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $latestDate = "";
        $summary = "";
        $detail = "";

        for ($i=1; $i <= 3; $i++) { 
            $date = date('Y-m-d', strtotime(date('Y-m-01').' -'.$i.' months'));
            $temp[$date]['date'] = $date;
            $temp[$date]['summary'] = '';
            $temp[$date]['detail'] = '';
        }

        $commHistory = \DB::table('Member_Commission_Report_Consolidated')->where('member_id', '=', trim($member->id))->orderBy('commission_date', 'DESC')->limit(3)->get(['commission_date','summary_report_name','detail_report_name']);
        foreach ($commHistory as $key => $details) {
            if(isset($temp[$details->commission_date])){
                $temp[$details->commission_date]['summary'] = $details->summary_report_name;
                $temp[$details->commission_date]['detail'] = $details->detail_report_name;
            }
        }

        $data = [
                    'user' => $user,
                    'member' => $member,
                    'temp' => $temp,
                ];
        return view('front.misc.commStatement')->with($data);
    }
    public function showCommstatementModal ($lang,$symbol,$start_date,$end_date) {
        $user = \Sentinel::getUser();
        $member = \DB::table('Member')->where('username', trim($user->username))->first();

        if (!$model = \DB::table('Capx_Commission_Details')->where('member_id',$member->id)->where('symbol',$symbol)->whereBetween('date', [$start_date, $end_date])->get()) {
            $response = array('status' => 1, 'msg' => \Lang::get('error.announcementNotFound'));
        }else{

            $response = array('status' => 0, 'msg' => '', 'model' => $model,'symbol'=>$symbol);
        }

        return response()->json($response);
    }
    public function set_session(request $request){
        $option_session = $request->option_session;
        session(['option_session' => $option_session]);
    }

    public function dashboardGraphRank (Request $request) {
        // level (1 = 1 level, 3 = 3 level, 0 = all level)

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        if($request->level == 1){
            $downlineAry = \DB::table('Member_Network')->where('parent_id', $member->id)->where('my_level', $request->level)->get(['member_id']);
        }else if($request->level == 3){
            $downlineAry = \DB::table('Member_Network')->where('parent_id', $member->id)->whereIn('my_level', [1,2,3])->get(['member_id']);
        }else{
            $downlineAry = \DB::table('Member_Network')->where('parent_id', $member->id)->get(['member_id']);
        }

        foreach ($downlineAry as $item) {
            $downline[] = $item->member_id;
        }

        $rankAry = array('IB', 'MIB', 'PIB');
        $output = [];
        $temp = [];

        if(date('d') <= 7) $requestMonth = $request->month + 1;
        else $requestMonth = $request->month;

        for ($i=$requestMonth; $i > 0; $i--) {
            unset($temp);
            $selectedMonth = date('Y-m-01', strtotime(date('Y-m-01'). "-".$i." month"));
            if(\App::getLocale() == 'en') $selectedMonthDisplay = date('M', strtotime(date('Y-m-01'). "-".$i." month"));
            else $selectedMonthDisplay = date('m', strtotime(date('Y-m-01'). "-".$i." month")).''.\Lang::get('home.monthName');
            $temp['month'] = $selectedMonthDisplay;
            foreach ($rankAry as $rank) {
                $sales = \DB::table('Member_Sales_Summary')->whereIn('member_id', $downline)->where('rank', $rank)->where('date', $selectedMonth)->count();
                $temp[$rank] = $sales;
            }
            array_push($output, $temp);
        }

        if(date('d') > 7){
            unset($temp);
            if(\App::getLocale() == 'en') $temp['month'] = date('M');
            else $temp['month'] = date('m').''.\Lang::get('home.monthName');
            foreach ($rankAry as $rank) {
                $sales = \DB::table('Member_Sales')->whereIn('member_id', $downline)->where('rank', $rank)->count();
                $temp[$rank] = $sales;
            }
            array_push($output, $temp);
        }


        $response = array('status' => 0, 'msg' => '', 'model' => $output);
        return response()->json($response);
    }

    public function dashboardGraphMember (Request $request) {
        // level (1 = 1 level, 3 = 3 level, 0 = all level)

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        if($request->level == 1){
            $downlineAry = \DB::table('Member_Network')->where('parent_id', $member->id)->where('my_level', $request->level)->get(['member_id']);
        }else if($request->level == 3){
            $downlineAry = \DB::table('Member_Network')->where('parent_id', $member->id)->whereIn('my_level', [1,2,3])->get(['member_id']);
        }else{
            $downlineAry = \DB::table('Member_Network')->where('parent_id', $member->id)->get(['member_id']);
        }

        foreach ($downlineAry as $item) {
            $downline[] = $item->member_id;
        }

        $output = [];
        $temp = [];

        if(date('d') <= 7) $requestMonth = $request->month + 1;
        else $requestMonth = $request->month;

        for ($i=$requestMonth; $i > 0; $i--) {
            unset($temp);
            $k = 0;
            $selectedMonth = date('Y-m-01', strtotime(date('Y-m-01'). "-".$i." month"));
            if(\App::getLocale() == 'en') $selectedMonthDisplay = date('M', strtotime(date('Y-m-01'). "-".$i." month"));
            else $selectedMonthDisplay = date('m', strtotime(date('Y-m-01'). "-".$i." month")).''.\Lang::get('home.monthName');
            $temp['month'] = $selectedMonthDisplay;
            $totalinvest = \DB::table('Member_Sales_Summary')->whereIn('member_id', $downline)->where('totalinvest', '>', 0)->where('date', $selectedMonth)->count();
            $temp['totalinvest'] = $totalinvest;
            
            $k = $i-1;
            $selectedMonthEnd = date('Y-m-01 00:00:00', strtotime(date('Y-m-01'). "-".$k." month"));
            $totalmember = \DB::table('Member')->whereIn('id', $downline)->where('created_at', '<', $selectedMonthEnd)->count();
            $temp['totalnoinvest'] = $totalmember - $totalinvest;

            array_push($output, $temp);
        }

        if(date('d') > 7){
            unset($temp);
            if(\App::getLocale() == 'en') $temp['month'] = date('M');
            else $temp['month'] = date('m').''.\Lang::get('home.monthName');
            // $totalinvest1 = \DB::table('Member_Account_Fund')->whereIn('member_id', $downline)->where('balanceB', '>', 0)->groupBy('member_id')->get();
            // $totalinvest = count($totalinvest1);

            $coin = \DB::table('Capx_Coin')->get();
            $margin = \DB::table('Capx_Margin')->get();
            $totalinvest = 0;

            foreach ($downlineAry as $item) {
                $member_id = $item->member_id;
                $investtrfx = 0;
                $investcoin = 0;
                $investmargin = 0;
                $totalinvestamount = 0;

                $investtrfx = \DB::table('Member_Account_Fund')->where('member_id', $member_id)->sum('balanceB');
                $coinWallet = \DB::table('Capx_Member_Coin')->where('member_id', $member_id)->first();

                foreach ($coin as $coinData) {
                    $symbol = $coinData->symbol;
                    $investcoin = $coinData->price * $coinWallet->$symbol;
                }

                foreach ($margin as $marginData) {
                    $symbol = $marginData->symbol;
                    $investmargin = $marginData->current_price * $coinWallet->$symbol;
                }

                $totalinvestamount = $investtrfx * 3 + $investcoin + $investmargin;

                if($totalinvestamount >= 1500) $totalinvest++;
            }

            $temp['totalinvest'] = $totalinvest;
            
            $selectedMonthEnd = date('Y-m-01 00:00:00', strtotime(date('Y-m-01'). "+1 month"));
            $totalmember = count($downline);
            $temp['totalnoinvest'] = $totalmember - $totalinvest;
            array_push($output, $temp);
        }
        
        $response = array('status' => 0, 'msg' => '', 'model' => $output);
        return response()->json($response);
    }

    public function dashboardGraphInvest (Request $request) {
        // level (1 = 1 level, 3 = 3 level, 0 = all level)

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        if($request->level == 1){
            $downlineAry = \DB::table('Member_Network')->where('parent_id', $member->id)->where('my_level', $request->level)->get(['member_id']);
        }else if($request->level == 3){
            $downlineAry = \DB::table('Member_Network')->where('parent_id', $member->id)->whereIn('my_level', [1,2,3])->get(['member_id']);
        }else{
            $downlineAry = \DB::table('Member_Network')->where('parent_id', $member->id)->get(['member_id']);
        }

        foreach ($downlineAry as $item) {
            $downline[] = $item->member_id;
        }

        $output = [];
        $temp = [];

        if(date('d') <= 7) $requestMonth = $request->month + 1;
        else $requestMonth = $request->month;

        for ($i=$requestMonth; $i > 0; $i--) {
            unset($temp);
            $selectedMonth = date('Y-m-01', strtotime(date('Y-m-01'). "-".$i." month"));
            if(\App::getLocale() == 'en') $selectedMonthDisplay = date('M', strtotime(date('Y-m-01'). "-".$i." month"));
            else $selectedMonthDisplay = date('m', strtotime(date('Y-m-01'). "-".$i." month")).''.\Lang::get('home.monthName');
            $temp['month'] = $selectedMonthDisplay;
            $totalinvest = \DB::table('Member_Sales_Summary')->whereIn('member_id', $downline)->where('date', $selectedMonth)->sum('totalinvest');
            $temp['totalinvest'] = $totalinvest;
            array_push($output, $temp);
        }

        if(date('d') > 7){
            unset($temp);
            if(\App::getLocale() == 'en') $temp['month'] = date('M');
            else $temp['month'] = date('m').''.\Lang::get('home.monthName');

            $investtrfx = 0;
            $investcoin = 0;
            $investmargin = 0;

            $investtrfx = \DB::table('Member')->join('Member_Account_Fund', 'Member.id', '=', 'Member_Account_Fund.member_id')->whereIn('member_id', $downline)->sum('balanceb');
            $level1 = \DB::table('Member')->join('Capx_Member_Coin', 'Member.id', '=', 'Capx_Member_Coin.member_id')->whereIn('member_id', $downline)->get();

            $coin = \DB::table('Capx_Coin')->get();
            foreach ($coin as $coinData) {
                $symbol = $coinData->symbol;
                if(count($level1) > 0){
                    foreach ($level1 as $item) {
                        if($item->$symbol > 0) $investcoin += $coinData->price * $item->$symbol;
                    }
                }
            }

            $margin = \DB::table('Capx_Margin')->get();
            foreach ($margin as $marginData) {
                $symbol = $marginData->symbol;
                if(count($level1) > 0){
                    foreach ($level1 as $item) {
                        if($item->$symbol > 0) $investmargin += $marginData->current_price * $item->$symbol;
                    }
                }
            }

            $totalinvest = $investtrfx * 3 + $investcoin + $investmargin;

            $temp['totalinvest'] = $totalinvest;
            array_push($output, $temp);
        }

        $response = array('status' => 0, 'msg' => '', 'model' => $output);
        return response()->json($response);
    }

    public function dashboardGraphSales (Request $request) {
        // level (1 = 1 level, 3 = 3 level, 0 = all level)

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        if($request->level == 1){
            $downlineAry = \DB::table('Member_Network')->where('parent_id', $member->id)->where('my_level', $request->level)->get(['member_id']);
        }else if($request->level == 3){
            $downlineAry = \DB::table('Member_Network')->where('parent_id', $member->id)->whereIn('my_level', [1,2,3])->get(['member_id']);
        }else{
            $downlineAry = \DB::table('Member_Network')->where('parent_id', $member->id)->get(['member_id']);
        }

        foreach ($downlineAry as $item) {
            $downline[] = $item->member_id;
        }

        $output = [];
        $temp = [];

        if(date('d') <= 7) $requestMonth = $request->month + 1;
        else $requestMonth = $request->month;

        for ($i=$requestMonth; $i > 0; $i--) {
            unset($temp);
            $selectedMonth = date('Y-m-01', strtotime(date('Y-m-01'). "-".$i." month"));
            if(\App::getLocale() == 'en') $selectedMonthDisplay = date('M', strtotime(date('Y-m-01'). "-".$i." month"));
            else $selectedMonthDisplay = date('m', strtotime(date('Y-m-01'). "-".$i." month")).''.\Lang::get('home.monthName');
            $temp['month'] = $selectedMonthDisplay;
            $totalsales = \DB::table('Member_Sales_Summary')->whereIn('member_id', $downline)->where('date', $selectedMonth)->sum('totaltransfer');
            $temp['totalsales'] = $totalsales;
            array_push($output, $temp);
        }

        if(date('d') > 7){
            unset($temp);
            if(\App::getLocale() == 'en') $temp['month'] = date('M');
            else $temp['month'] = date('m').''.\Lang::get('home.monthName');
            $totalsales1 = \DB::table('Member_Wallet_Statement_Fund')->whereIn('member_id', $downline)->where('action_type', '=', 'Transfer to MT5')->where('created_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('created_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->sum('b_amount');
            $totalsales2 = \DB::table('Member_Blacklist')->whereIn('member_id', $downline)->where('blacklist_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('blacklist_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->where('b_book', '<>', 0)->sum('b_book');
            $totalsales = $totalsales1 - ($totalsales2/2);
            $temp['totalsales'] = $totalsales*2;
            array_push($output, $temp);
        }

        $response = array('status' => 0, 'msg' => '', 'model' => $output);
        return response()->json($response);
    }

    public function hidememo () {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        \DB::table('Member_Detail')->where('member_id',$member->id)->update(['memo' => 0]);
    }
    
    public function adminLogin ($sa,$id) {

        $AdminUser = \DB::table('admin_MemberLogin')->where('password',$id)->first();

        if($AdminUser->is_login ==1){
            return view('errors.404');
        }

        $getUser = \Sentinel::findById($AdminUser->user_id);
        $user = \Sentinel::authenticate([
            'email'  =>  $getUser->email,
            'password'  =>  $getUser->password2
        ]);

        if(!$checkUser = $this->MemberRepository->findByUsername($getUser->email)) {
            throw new \Exception(\Lang::get('error.memberNotFound'), 1);
            return false;
        }

        if(!$user) {
            throw new \Exception(\Lang::get('error.loginError'), 1);
            return false;
        }

        if($user->is_ban != 1) {
            throw new \Exception(\Lang::get('error.userBan'), 1);
            return false;
        }

        $permissions = $user->permissions;
        if(!isset($permissions['member'])) {
            throw new \Exception(\Lang::get('error.userError'), 1);
            return false;
        }
        else if ($permissions['member'] != 1) {
            throw new \Exception(\Lang::get('error.userError'), 1);
            return false;
        }
        else{
            if(!\Cache::has('member.' . $user->id)) {
                $member = $this->MemberRepository->findByUsername(trim($user->username));
                $member->load('wallet');
                $member->load('detail');
                //$member->load('shares');
                $member->load('package');
                \Cache::put('member.' . $user->id, $member, 60);
            }else {
                $member = \Cache::get('member.'. $user->id);
            }
        }
         

        $unreadNotification = \DB::table('notification_target')->where('member_id', $member->id)->where('read', 0)->count();
        if($unreadNotification > 0){
            $resendurl =  route('inbox.list', ['lang' => \App::getLocale()]) ;
            $msg = \Lang::get('inbox.fleshmsg');
            Session::flash('Notification', $msg);
            Session::flash('Notificationresendurl', $resendurl);
        }

        $unreadTicket = \DB::table('helpdesk')->where('user_id', $member->id)->where('is_reply', 1)->where('read', 1)->count();
        if($unreadTicket > 0){
            $resendurl =  route('helpdesk.list', ['lang' => \App::getLocale()]) ;
            $msg = \Lang::get('helpdesk.fleshmsg');
            Session::flash('message', $msg);
            Session::flash('resendurl', $resendurl);
        }

        $readAnnouncement = \DB::table('Notification_Log')->where('member_id', $member->id)->get(['notification_id']);
        if(count($readAnnouncement) > 0){
            foreach ($readAnnouncement as $item) {
                $announcementTemp[] = $item->notification_id;
            }
            $unreadAnnouncement = \DB::table('Notification')->where('created_at', '>=', $member->created_at)->where('type', 0)->whereNotIn('id', $announcementTemp)->count();
        }else{
            $unreadAnnouncement = \DB::table('Notification')->where('created_at', '>=', $member->created_at)->where('type', 0)->count();
        }
        if($unreadAnnouncement > 0){
            $resendurl =  route('inbox.list', ['lang' => \App::getLocale()]) ;
            $msg = \Lang::get('inbox.fleshmsg');
            Session::flash('Notification', $msg);
            Session::flash('Notificationresendurl', $resendurl);
        }

        $memoShow = \DB::table('Member_Detail')->where('member_id', $member->id)->first();
        if($memoShow->memo > 0) Session::flash('Memo', $memoShow->memo);

        \DB::table('admin_MemberLogin')->where('password',$id)->update(['is_login' => 1]);

        return redirect()->route('home','chs');
    }

    public function usermanual () {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $isSeanet = $this->MemberRepository->find_seanet($member->id);
        return view('front.usermanual')->with('isSeanet',$isSeanet);
//        if($isSeanet == 0){
//            return view('front.usermanual')->with('isSeanet',$isSeanet);
//        }else{
//            return view('front.blank');
//        }


    }
}
