<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositories\PackageRepository;

class PackageController extends Controller
{
	/**
     * The PackageRepository instance.
     *
     * @var \App\Repositories\PackageRepository
     */
    protected $PackageRepository;

    public function __construct(
        PackageRepository $PackageRepository
    ) {
        $this->middleware('admin');
        $this->PackageRepository = $PackageRepository;
    }

    /**
     * Update package settings
     * @return [type] [description]
     */
    public function postUpdate ($id) {
        if (!$model = $this->PackageRepository->findById(trim($id))) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  'Product not found.'
            ]);
        }

        $data = \Input::get('data');
        \DB::table('Package')->where('id', $id)->update([
          'title' =>  $data['title'],
          'month_percent' =>  $data['month_percent'],
          'direct_percent' =>  $data['direct_percent'],
          'group_percent' =>  $data['group_percent'],
          'group_level' =>  $data['group_level'],
          'package_amount' =>  $data['package_amount'],
          'status'    =>  isset($data['status']) ? 1 : 0
      ]);
        //$this->PackageRepository->update($model, $data);
        return \Response::json([
            'type'  =>  'success',
            'message'   =>  'Product updated.'
        ]);
    }
}
