<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Services\UploadpicService;
use App\Repositories\HelpdeskRepository;
use App\Repositories\HelpdeskCommentRepository;
use App\Repositories\MemberRepository;
use App\Repositories\AdminRightRepository;
use Carbon\Carbon;
use App\Services\AmazonS3;

class HelpdeskController extends Controller
{
    /**
     * The AnnouncementRepository instance.
     *
     * @var \App\Repositories\AnnouncementRepository
     */
    protected $HelpdeskRepository;
    protected $HelpdeskCommentRepository;
    protected $MemberRepository;
    protected $UploadpicService;
    protected $AmazonS3;
    /**
     * Create a new AnnouncementController instance.
     *
     * @param \App\Repositories\AnnouncementRepository $AnnouncementRepository
     * @return void
     */
    public function __construct(HelpdeskRepository $HelpdeskRepository,
        MemberRepository $MemberRepository,
        HelpdeskCommentRepository $HelpdeskCommentRepository ,
        UploadpicService $UploadpicService,
        AdminRightRepository $AdminRightRepository,
        AmazonS3 $AmazonS3) {
        parent::__construct();
        $this->HelpdeskRepository = $HelpdeskRepository;
        $this->HelpdeskCommentRepository = $HelpdeskCommentRepository;
        $this->MemberRepository = $MemberRepository;
        $this->UploadpicService = $UploadpicService;
        $this->AdminRightRepository = $AdminRightRepository;
        $this->AmazonS3 = $AmazonS3;
        $this->middleware('admin');
    }

    /**
     * Datatable List
     * @return [type] [description]
     */
    public function getList ($id,$type_Id) {
        $type_Id = explode(",",$type_Id);
        return $this->HelpdeskRepository->getAdminList($id,$type_Id);
    }
    public function getOpenList ($id,$type_Id) {
        $type_Id = explode(",",$type_Id);
        return $this->HelpdeskRepository->getAdminOpenList($id,$type_Id);
    }
    public function getPList () {

        return $this->HelpdeskRepository->getAdminPList();
    }
    public function getRecord () {

        return $this->HelpdeskRepository->getAdminRecord();
    }
    public function getPersonalRating ($date) {
        //session()->forget('personal_date');

        $user = \Sentinel::getUser();
        $model = $this->AdminRightRepository->findById($user->id);

        if($model->level =="superadmin"){
            return $this->HelpdeskRepository->getAllPersonalRating($date);
        }else{
            return $this->HelpdeskRepository->getPersonalRating($date,$user->id);
        }
        // $right = json_decode($model->right, true);
        // $type_Id = explode(",",$model->cs_type);   
    }

    public function getComplain ($date) {
        session()->forget('personal_date');

        $user = \Sentinel::getUser();
        $model = $this->AdminRightRepository->findById($user->id);

        if($model->level =="superadmin"){
            return $this->HelpdeskRepository->getAllPersonalComplain($date);
        }else{
            return $this->HelpdeskRepository->getPersonalComplain($date,$user->id);
        }
        // $right = json_decode($model->right, true);
        // $type_Id = explode(",",$model->cs_type);   
    }

    public function getTranModal ($id) { //weilun
        if (!$model = $this->HelpdeskRepository->findById(trim($id))) {
            return 'Helpdesk ID not found.';
        }
        if (!$admin_model = $this->HelpdeskRepository->find_admin($id)) {
            return redirect()->back()->with('flashMessage', [
                'class' => 'danger',
                'message' => 'Data not found.'
            ]);
        }
        return view('back.helpdesk.helpdesk_showTran')->with('model', $model)->with('admin_model',$admin_model);
    }

    public function getShowModal ($id) { //weilun
        if (!$model = $this->HelpdeskRepository->findLog(trim($id))) {
            return 'History not found.';
        }
        return view('back.helpdesk.helpdesk_showHistory')->with('model', $model);
    }

    public function postUpdate ($id) {

        if (!$model = $this->HelpdeskRepository->findById($id)) {
            return \Response::json([
                'type' => 'error',
                'message' => 'Data not found'
            ]);
        }

        $data = \Input::get('data');
        $admin = \Sentinel::getUser(); //weilun

        $data['close_by']  = $admin->username;


        if(empty($data["remark"])){
            return \Response::json([
                'type' => 'error',
                'message' => 'Remark is empty'
            ]);
        }

        $this->HelpdeskRepository->update($model,$data);

        if( $data["status"] == "resolved"){

            $update_data["action"] = "R";

        }else{

            $update_data["action"] = "D";
            
        }

        $update_data["helpdesk_id"] = $id;
        $update_data["helpdesk_type"] = "0";
        $update_data["cs_id"] = $admin->id;
        $update_data["cs_username"] = $admin->username;
        $update_data["cs_type"] = $admin->cs_type;
        
        $update_data["comment"] = $data['remark'];

        $this->HelpdeskRepository->update_log($update_data);
        return \Response::json([
            'type' => 'success',
            'message' => 'Case is updated.'
        ]);
    }

    public function postcreate (Request $request) {

        //$data = \Input::get('data');

        $data = $request->all();   
        $admin = \Sentinel::getUser(); //weilun

        $check_id = $this->HelpdeskRepository->findById($data["helpdesk_id"]); //get helpdesk data
        if (!$lastCommentTime = $this->HelpdeskCommentRepository->getLastcomment($data["helpdesk_id"] ,  $check_id->user_id)) {

            $lastCommentTime = $this->HelpdeskRepository->findById($data["helpdesk_id"]);

        }

        if(!is_null($check_id->cs_id) && ($check_id->cs_id != $admin->id && $admin->cs_type!=0 ) ){

            return redirect('admin/helpdesk/edit/'.$data["helpdesk_id"])->with('status', 'This Ticker is already Replied');
        }

        $now = Carbon::now(); // cs reply time
        $check_nine = Carbon::create($now->year, $now->month, $now->day, 9, 30, 00); // cr8 today 9.30
        $check_night = Carbon::create($now->year, $now->month, $now->day, 20, 30, 00); // cr8 today 9.30
        $nineTime = $check_nine->toDateTimeString();
        $eightTime = $check_night->toDateTimeString();
        $startTime = Carbon::parse($lastCommentTime->created_at); //get comment time
        $finishTime = Carbon::parse($now); // get reply time


        $data["reply_sec"] = $this->UploadpicService->helpdesk_count_sec($startTime,$nineTime,$eightTime,$finishTime);
        $data["cs_id"] = $admin->id;
        $data["cs_username"] = $admin->username;
        $data["cs_type"] = $admin->cs_type;
        //dd($data);

        if(empty($data["Comment"])){
            return back()->withErrors(['不能空白回复']);
        }

        if(!empty( \Input::file('pic_1')) )
        {
            $idFront_path = \Input::file('pic_1');
            $filename1 = $this->AmazonS3->uploadImage($idFront_path, $admin, 'helpdesk', uniqid());
            // $filename1 = $this->UploadpicService->helpdesk_upload($idFront_path);
            $data['pic_1'] = $filename1;
        }
        if(!empty( \Input::file('pic_2')) )
        {
            $idFront_path = \Input::file('pic_2');
            $filename2 = $this->AmazonS3->uploadImage($idFront_path, $admin, 'helpdesk', uniqid());
            // $filename2 = $this->UploadpicService->helpdesk_upload($idFront_path);
            $data['pic_2'] = $filename2;
        }

        $record["email"] = $admin->email;
        //$record["reply_sec"] = $data["reply_sec"];
        $record["created_at"] = $check_nine;

        if($finishTime->greaterThan($nineTime) && $finishTime->lessThan($eightTime)){ // less than 9.30 and big than 8.30 don want record
            if (!$get_record = $this->HelpdeskCommentRepository->check_record($record)) {
                $record["reply_sec"] = $data["reply_sec"];
                $reply_case = 1;
                $get_record = $this->HelpdeskCommentRepository->first_record($record , $reply_case);

            }else{

                $record["reply_sec"] = $get_record["reply_sec"] + $data["reply_sec"];
                $reply_case = $get_record["reply_case"]+1;
                $get_record = $this->HelpdeskCommentRepository->first_record($record , $reply_case);
       
            }

        }



        $this->HelpdeskCommentRepository->store($data);
        $this->HelpdeskRepository->update_reply($data); 

        $data["action"] = "C"; 
        
        $this->HelpdeskRepository->comment_log($data);

        return view('back.helpdesk.uploadSuccess');   
    }

    public function transfer () {
        $data = \Input::get('data');     
        $admin = \Sentinel::getUser(); //weilun

        $data["cs_id"] = $admin->id;
        $data["cs_username"] = $admin->username;
        $data["cs_type"] = $admin->cs_type;
        $data["action"] = "T";
    

        $get_data["get_data"] = explode("-", $data["to_cs_id"]); //get type,csid,csusernmae
        $data["cs_type"] = substr($get_data["get_data"][0],0,1); // cs type
        $data["to_cs_id"] = $get_data["get_data"][1]; //cs id
        $data["to_cs_username"] = $get_data["get_data"][2]; //cs usernmae



        try {
            $this->HelpdeskRepository->transfer($data);

        } catch (\Exception $e) {
            return \Response::json([
                'type' => 'error',
                'message' => $e->getMessage()
            ]);
        }


        return \Response::json([
            'type' => 'success',
            'message' => 'Transfer Success.'
        ]);
    }
    public function transfer_list (Request $request) {
        $data = $request->all();     
        $admin = \Sentinel::getUser(); //weilun
        
        $data["cs_id"] = $admin->id;
        $data["cs_username"] = $admin->username;
        $data["cs_type"] = $admin->cs_type;
        $data["action"] = "T";
    

        $get_data["get_data"] = explode("-", $data["to_cs_id"]); //get type,csid,csusernmae
        $data["cs_type"] = substr($get_data["get_data"][0],0,1); // cs type
        $data["to_cs_id"] = $get_data["get_data"][1]; //cs id
        $data["to_cs_username"] = $get_data["get_data"][2]; //cs usernmae

        try {
            $this->HelpdeskRepository->transfer($data);
            return response()->json(array('msg' => "Transfer Success" ));

        } catch (\Exception $e) {
            return response()->json(array('msg' => "Transfer Fail" ));
        }
    }
    public function getEdit ($id) {
        $admin = \Sentinel::getUser();


        if (!$model = $this->HelpdeskRepository->findById($id)) {
            return redirect()->back()->with('flashMessage', [
                'class' => 'danger',
                'message' => 'Data not found.'
            ]);
        }
        if (!$model2 = $this->HelpdeskCommentRepository->findById($id)) {
            return redirect()->back()->with('flashMessage', [
                'class' => 'danger',
                'message' => 'Data not found.'
            ]);
        }
        if (!$admin_model = $this->HelpdeskRepository->find_admin($admin->username)) {
            return redirect()->back()->with('flashMessage', [
                'class' => 'danger',
                'message' => 'Data not found.'
            ]);
        }

        $now = Carbon::now();
        $end_date = Carbon::parse($model->created_at);
        $lengthOfAd = $end_date->diffInDays($now);

        

        return view('back.helpdesk.helpdesk_edit')->with('model', $model)->with('model2', $model2)->with('admin_model',$admin_model)->with('lengthOfAd',$lengthOfAd);
    }

    public function multiSend () {

        //$member_model = \DB::table('member')->get();

        return view('back.helpdesk.helpdesk_multisend');
    }

    public function multipost (Request $request) {
        
        $data = $request->all();
        $admin = \Sentinel::getUser(); //weilun


        foreach ($data["email"] as $cr8_comment) {

            $member = $this->MemberRepository->findByUsername($cr8_comment);
            $data1['user_id'] = $member->id;
            $data1['username'] = $member->username;
            $data1['status'] = "process";
            $data1['for_cs_type'] = $data['type'] ;
            $data1['type'] = $data['type'] ;
            $data1['tittle'] = $data['tittle'] ;
            $data1['content'] = $data['content'] ;
            $data1['is_reply'] = 1 ;
            $data1['is_multisend'] = 1 ;
            $data1["cs_id"] = $admin->id;
            $data1["status"] = "solving";
            $save['user_id'] = $data1['user_id'];

            if(!empty( \Input::file('pic_1')) )
            {
                $idFront_path = \Input::file('pic_1');
                $filename1 = $this->AmazonS3->uploadImage($idFront_path, $admin, 'helpdesk', uniqid());
                // $filename1 = $this->UploadpicService->helpdesk_upload($idFront_path);
                $data1['pic_1'] = $filename1;
            }
            if(!empty( \Input::file('pic_2')) )
            {
                $idFront_path = \Input::file('pic_2');
                $filename2 = $this->AmazonS3->uploadImage($idFront_path, $admin, 'helpdesk', uniqid());
                // $filename2 = $this->UploadpicService->helpdesk_upload($idFront_path);
                $data1['pic_2'] = $filename2;
            }

            $check_today = date('Y-m-d')."%";
            $check_refrence = $this->HelpdeskRepository->check_refrence($check_today);

            if($check_refrence==0){ //if today is the 1st ticket

                $save["id"] = date('ymd'."0001");
                $data1['refrence_id'] = $this->HelpdeskRepository->get_refrence($save);

            }else{

                $data1['refrence_id'] = $this->HelpdeskRepository->get_refrence($save);
            }

            $this->HelpdeskRepository->store($data1);
            $this->HelpdeskRepository->update_read($member->id,$data['tittle']);
        }
        
        return view('back.helpdesk.uploadSuccess');
        return \Response::json([
            'type' => 'success',
            'message' => 'Send Success.'
        ]);
    }


    // ===========================schedule =====================================================================
    public function getHelpdeskschedule () {

        if(session()->has('personal_date')){
            $date = session('personal_date');
            $checkSchedule = $this->HelpdeskRepository->getSchedule(session('personal_date'));
            session()->forget('personal_date');
        }else{
            $date = Carbon::now()->format('ymd');
            $checkSchedule = $this->HelpdeskRepository->getSchedule(Carbon::now()->format('ymd'));
            // dd($checkSchedule);
        }

        $model = $this->HelpdeskRepository->getCS();
        $getday = collect([
            Carbon::now()->format('ymd') => Carbon::now()->format('Y-m-d'),
            Carbon::now()->addDay()->format('ymd') => Carbon::now()->addDay()->format('Y-m-d')
        ]);

        if($checkSchedule){

            $model = $model->map(function ($value) use ($checkSchedule)  {

                    $checkSchedule =  $checkSchedule->toArray(); // Converts Collection to array
                    $arr = array_column($checkSchedule,'cs_id');
                    $checkWork = array_search($value['id'], $arr);
                    if($checkWork!=false || $checkWork==0){
                        if(!empty($checkSchedule[$checkWork]['work'])){
                            $value["work"] = $checkSchedule[$checkWork]['work'];
                            $value["shift"] = $checkSchedule[$checkWork]['shift'];

                        }
                    }


                    return $value;   
            });

        }
        //dd($model);

        return view('back.helpdesk.schedule')->with('model', $model)->with('getday',$getday)->with('date',$date);
    }

    public function postSchedule () {
        $data = \Input::get('data');
        $model = $this->HelpdeskRepository->getCS();
        foreach ($model as $models ) {
            $updateornot = 0;
            if(!empty($data["am"])){
                foreach ($data["am"] as $key ) {
                    if($key == $models->id){
                        
                        $this->HelpdeskRepository->insertSchedule($key , $data["schedule_date"] , 1 , 1);
                        $updateornot = 1;
                    }
                
                }
            }

            if(!empty($data["pm"])){
                foreach ($data["pm"] as $key ) {
                    if($key == $models->id){
                        
                        $this->HelpdeskRepository->insertSchedule($key , $data["schedule_date"] , 1 , 2);
                        $updateornot = 1;
                    }
                
                }
            }

            if($updateornot == 0){
                $this->HelpdeskRepository->insertSchedule($models->id , $data["schedule_date"] , 0 , 0);
            }
            
        }

        return \Response::json([
            'type' => 'success',
            'message' => "Add/Edit Schedule Success."
        ]);
    }
    // ===========================schedule =====================================================================

    // ===========================Blacklist =====================================================================
    public function postBlackList () {
        
        $data = \Input::get('data');
        if(empty($data)){
            return \Response::json([
                'type' => 'error',
                'message' => 'no have email.'
            ]);
        }
        $admin = \Sentinel::getUser(); //weilun


        foreach ($data["email"] as $memberEmail) {

            $member = $this->MemberRepository->findByUsername($memberEmail);
            $data1['member_id'] = $member->id;
            $data1["cs_id"] = $admin->id;


            $this->HelpdeskRepository->insertBlackList($data1);
        }

        return \Response::json([
            'type' => 'success',
            'message' => 'Add to BlackList Success.'
        ]);
    }
    // ===========================Blacklist =====================================================================
}
