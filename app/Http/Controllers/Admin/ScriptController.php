<?php

namespace App\Http\Controllers\Admin;

use DB;
use File;
use Mail;
use Excel;
use Carbon\Carbon;
use App\Repositories\MemberRepository;

class ScriptController extends Controller
{
    public function __construct() {
        $this->middleware('admin', ['except' => []]);
    }

    public function patchRemarkDisplay () {
        // Manually add
        $statement = DB::table('Member_Wallet_Statement_Fund')->where('created_at', '>', '2018-08-03 12:00:00')->whereNull('remark_display')->get();
        $temp = [];
        foreach ($statement as $details) {
            unset($temp);
            switch ($details->action_type) {
                case 'Adjustment':
                    $split = explode(' ', (string)$details->remark);

                    if($split[2] == 'User'){
                        DB::table('Member_Wallet_Statement_Fund')->where('id', (string)$details->id)->update(['remark_display' => 'TH00025/']);
                    }

                    // if($split[1] == 'for' && $split[2] == 'MT5'){
                    //     $split1 = explode('account', (string)$details->remark);
                    //     $temp['account'] = $split1[1];
                    //     DB::table('Member_Wallet_Statement_Fund')->where('id', (string)$details->id)->update(['remark_display' => 'TH00059/'.json_encode($temp)]);
                    // }
                    break;
                
                case 'Adjustment 5 Percent':
                    $split = explode(' ', (string)$details->remark);

                    if($split[3] == 'compensation'){
                        $split1 = explode('Account', (string)$details->remark);
                        $split2 = explode(' (', $split1[1]);
                        $split3 = explode('until ', (string)$details->remark);
                        $split4 = explode(') ', $split3[1]);
                        $temp['account'] = $split2[0];
                        $temp['amountString'] = '('.$split2[1];
                        $temp['month'] = $split4[0];
                        DB::table('Member_Wallet_Statement_Fund')->where('id', (string)$details->id)->update(['remark_display' => 'TH00022/'.json_encode($temp)]);
                    }
                    break;

                default:
                    break;
            }
        }

        // Deposit
        // $statement = DB::table('Member_Wallet_Statement')->where('created_at', '>', '2018-08-03 12:00:00')->whereNull('remark_display')->get();
        // foreach ($statement as $details) {
        //     switch ($details->action_type) {
        //         case 'Deposit':
        //                 $temp['amount'] = $details->cash_amount;
        //                 DB::table('Member_Wallet_Statement')->where('id', (string)$details->id)->update(['remark_display' => 'TH00002/'.json_encode($temp)]);
        //             break;

        //         case 'Adjustment':
        //             $split = explode(' ', (string)$details->remark);
        //             if($split[2] == 'withdrawal'){
        //                 $temp['account'] = end($split);
        //                 DB::table('Member_Wallet_Statement')->where('id', (string)$details->id)->update(['remark_display' => 'TH00060/'.json_encode($temp)]);
        //             }

        //             if($split[2] == 'Deduct'){
        //                 $split1 = explode('for', (string)$details->remark);
        //                 $temp['percentage'] = $split[3];
        //                 $temp['monthyear'] = $split1[1];
        //                 DB::table('Member_Wallet_Statement')->where('id', (string)$details->id)->update(['remark_display' => 'TH00061/'.json_encode($temp)]);
        //             }
        //             break;
                
        //         default:
        //             break;
        //     }
        // }

        //change 5%
        // $statement = DB::table('Member_Wallet_Statement_Fund')->where('action_type', 'Adjustment')->where('remark', 'like', '%compensation%')->get();
        // foreach ($statement as $details) {
        //     DB::table('Member_Wallet_Statement_Fund')->where('id', (string)$details->id)->update(['action_type' => 'Adjustment 5 Percent']);
        // }
    }

    public function voidUsernameIC() {
        $member = \DB::table('Member_Blacklist')->where('is_due_to_ban',0)->orderBy('member_id')->get();
        $i=0;
        foreach ($member as $memberDetail){
            $i++;
            $id = $memberDetail->member_id;
            $username = $memberDetail->username;
            $new = $memberDetail->username."_".Carbon::now()->format('Ymd')."N".rand(5, 15);

            $ic = \DB::table('Member_Detail')->where('member_id',$id)->first(['identification_number']);
            $newIC = $ic->identification_number."_".Carbon::now()->format('Ymd')."N".rand(5, 15);

            DB::table('Member')->where('id',$id)->where('username',$username)->update(['username' => $new,'void'=>1]);
            DB::table('Member_Detail')->where('member_id',$id)->update(['identification_number' => $newIC]);
            DB::table('Admin_Fees')->where('username',$username)->update(['username' => $new]);
            DB::table('Bonus_Direct')->where('username',$username)->update(['username' => $new]);
            DB::table('Bonus_Direct')->where('from_username',$username)->update(['from_username' => $new]);
            DB::table('Bonus_Group')->where('username',$username)->update(['username' => $new]);
            DB::table('Bonus_Month')->where('username',$username)->update(['username' => $new]);
            DB::table('Bonus_Month')->where('from_username',$username)->update(['from_username' => $new]);
            DB::table('Bonus_Override')->where('username',$username)->update(['username' => $new]);
            DB::table('Bonus_Override')->where('from_username',$username)->update(['from_username' => $new]);
            DB::table('Bonus_Pairing')->where('username',$username)->update(['username' => $new]);
            DB::table('helpdesk')->where('username',$username)->update(['username' => $new]);
            DB::table('helpdesk_comment')->where('client_username',$username)->update(['client_username' => $new]);
            DB::table('Member_Account_Withdrawal')->where('username',$username)->update(['username' => $new]);
            DB::table('member_commission_report')->where('username',$username)->update(['username' => $new]);
            DB::table('Member_Compensate')->where('username',$username)->update(['username' => $new]);
            DB::table('Member_Inactive_Account')->where('username',$username)->update(['username' => $new]);
            DB::table('Member_Network')->where('username',$username)->update(['username' => $new]);
            DB::table('Member_Sales')->where('username',$username)->update(['username' => $new]);
            DB::table('Member_x')->where('username',$username)->update(['username' => $new]);
            DB::table('Orders')->where('member_username',$username)->update(['member_username' => $new]);
            DB::table('Orders_Fund')->where('member_username',$username)->update(['member_username' => $new]);
            DB::table('payid_info')->where('rec_name',$username)->update(['rec_name' => $new]);
            DB::table('temp_leader')->where('Email',$username)->update(['Email' => $new]);
            DB::table('temp_member')->where('Emails',$username)->update(['Emails' => $new,'Ic number' =>$newIC]);
            DB::table('temp_orders')->where('member_username',$username)->update(['member_username' => $new]);
            DB::table('temp_ranking')->where('username',$username)->update(['username' => $new]);
            DB::table('temp_rwallet')->where('member_username',$username)->update(['member_username' => $new]);
            DB::table('Transfer')->where('from_username',$username)->update(['from_username' => $new]);
            DB::table('Transfer')->where('to_username',$username)->update(['to_username' => $new]);
            DB::table('Tre_Gateway_login')->where('username',$username)->update(['username' => $new]);
            DB::table('users')->where('username',$username)->update(['username' => $new]);
            DB::table('users')->where('email',$username)->update(['email' => $new]);
            DB::table('Withdraw')->where('username',$username)->update(['username' => $new]);
            DB::table('wwallet_reimburse')->where('username',$username)->update(['username' => $new]);
            DB::table('temp_leader')->where('Email',$username)->update(['Email' => $new]);
             // DB::table('temp_account_status')->where('Email',$username)->update(['Email' => $new]);
             // DB::table('temp_allow_transfer')->where('Email',$username)->update(['Email' => $new]);
             // DB::table('temp_bonus')->where('member_username',$username)->update(['member_username' => $new]);

            DB::table('scirptlog')->insert([
                'member_id' => $id,
                'scirpt' => 'MemberBlackScirpt',
                'old_username' => $username,
                'new_username' => $new,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
                
            ]);
            
            DB::table('Member_Blacklist')->where('member_id',$id)->update(['is_due_to_ban' => 99]);
            echo $i."."."old Email = ".$username."    new Email = ".$new."<br>";
        }
        echo "Done";
    }

    public function updateMonthlyMemberSales () {
        $insertDate = '2018-10-01';
        $sales = DB::table('Member_Sales_Oct')->get();

        foreach ($sales as $item) {
            $rank = $item->rank ? $item->rank : '';
            
            $totalinvest = DB::table('Member_Account_Fund_Oct')->where('member_id', $item->member_id)->sum('balanceB');
            
            DB::table('Member_Sales_Summary')->insert([
                'member_id' => $item->member_id,
                'rank' => $rank,
                'conditions' => $item->conditions,
                'totaldeposit' => $item->totaldeposit,
                'totaltransfer' => $item->totaltransfer,
                'totallotsize' => $item->totallotsize,
                'totallotsize9' => $item->totallotsize9,
                'totalinvest' => $totalinvest*3,
                'date' => $insertDate
            ]);
        }
    }

    public function patchMemberPreviousMonthSales () {
        //initialize data
        $notInSalesMember = DB::table('Member')->whereNotIn('id', function($q){$q->select('member_id')->from('Member_Sales');})->get(['id', 'username']);
        foreach ($notInSalesMember as $detail) {
            DB::table('Member_Sales')->insert([
                                        'member_id' => $detail->id,
                                        'username' => $detail->username
                                    ]);
        }

        DB::table('Member_Sales')->update(['rank' => null, 'conditions' => 0]);

        // start process
        $members = DB::table('Member')->where('id', '<>',1)->where('id', '<>',2)->whereNotIn('id', function($q){$q->select('member_id')->from('Member_Network');})->get();
        foreach($members as $member){
            $myid = $member->id;
            $mylevel = $member->level;
            $myusername = $member->username;
            $upline = $member->register_by;
            $upline_id = $member->direct_id;
            $totallotsize = 0;
            
            $mlevel = 0;
            while($mylevel > 2)
            {
                $umember = DB::table('Member')->where('id', '=',$upline_id)->first();
                
                $upline = $umember->register_by;
                $upline_id = $umember->direct_id;
                $upid = $umember->id;
                $mylevel = $umember->level;
                $upusername = $umember->username;
                
                $mlevel++;
                
                \DB::table('Member_Network')->insert([
                            'member_id' =>  $myid,
                            'username'    =>  $myusername,
                            'parent_id'    =>  $upid,
                            'parent_username'   =>  $upusername,
                            'parent_level'   =>  $mylevel,
                            'my_level'   =>  $mlevel,
                            'created_at' => \Carbon\Carbon::now(),
                            'updated_at' => \Carbon\Carbon::now()
                         ]);
            }
        }
        echo "0";
        sleep(1);
        $this->runBonusNotification1();
    }

    public function runBonusNotification1 () {
        //Condition 1 - 个人流水 >= 10000
        $member1 = DB::table('Member_Wallet_Statement_Fund')->select('member_id')->where('action_type', '=','Transfer to MT5')->where('created_at', '>','2018-09-01 05:00:00')->where('created_at', '<=','2018-10-01 05:00:00')->where('wallet_type', '=','BW')->groupBy('member_id')->havingRaw('SUM(b_amount) * 2 >= 10000')->get();
     
        $members2 = DB::table('Member_Account_Fund_Sep')->select('member_id')->where('balanceB', '<>',0)->groupBy('member_id')->havingRaw('SUM(balanceB) * 3 >= 15000')->get();
     
        $members = $member1->merge($members2);
    
        $no = 0;

        $temp = [];

        foreach($members as $member){
            unset($temp);
            $no++;
         
            $myid = $member->member_id;
         
            //$totalmember = DB::table('Member')->where('direct_id', '=', $myid)->count();
        
            $totalmember2 = DB::table('Member_Account_Fund_Sep')->join('Member', 'Member_Account_Fund_Sep.member_id', '=', 'Member.id')->select('member_id')->where('direct_id', '=', $myid)->groupBy('member_id')->havingRaw('SUM(balanceB) * 3 > 0')->get();
         
            $rank = '';
         
            $totalmember = count($totalmember2);
             
            if($totalmember >= 5){
                $totalsales1 = DB::table('Member')->join('Member_Wallet_Statement_Fund', 'Member.id', '=', 'Member_Wallet_Statement_Fund.member_id')->where('direct_id', '=', $myid)->where('action_type', '=', 'Transfer to MT5')->where('Member_Wallet_Statement_Fund.created_at', '>', '2018-09-01 05:00:00')->where('Member_Wallet_Statement_Fund.created_at', '<=', '2018-10-01 05:00:00')->sum('b_amount');
             
                $totalsales2 = DB::table('Member')->join('Member_Blacklist', 'Member.id', '=', 'Member_Blacklist.member_id')->where('Member.direct_id', '=', $myid)->where('Member_Blacklist.blacklist_at', '>', '2018-09-01 05:00:00')->where('Member_Blacklist.blacklist_at', '<=', '2018-10-01 05:00:00')->where('b_book', '<>', 0)->sum('b_book');
             
                $totalsales = ($totalsales1 * 2) - ($totalsales2);
             
                if($totalmember >= 15 && $totalsales >= 100000) $rank = 'PIB';
                else if($totalmember >= 10 && $totalsales >= 60000) $rank = 'MIB';
                else if($totalmember >= 5 && $totalsales >= 25000) $rank = 'IB';

                if($rank <> ''){
                    $current_rank = DB::table('Member_Sales')->where('member_id', '=', $myid)->first();
             
                    if(empty($current_rank)|| is_null($current_rank->rank) || ($current_rank->rank == 'IB' && $rank <> 'IB') || ($current_rank->rank == 'MIB' && $rank == 'PIB')){
                        DB::table('Member_Sales')
                        ->where('member_id', $myid)
                        ->update(['rank' => $rank,'conditions' => 1]);
                    }

                    $temp[] = 1;
                    DB::table('Member_Sales')->where('member_id', $myid)->update(['oldconditions2' => json_encode($temp)]);
                }
            }
        }
        echo "1";
        sleep(1);
        $this->runBonusNotification2();
    }

    public function runBonusNotification2 () {
        //Condition 2 - 个人投资 >= 15000 + 散客
        $member1 = DB::table('Member_Wallet_Statement_Fund')->select('member_id')->where('action_type', '=','Transfer to MT5')->where('created_at', '>','2018-09-01 05:00:00')->where('created_at', '<=','2018-10-01 05:00:00')->where('wallet_type', '=','BW')->groupBy('member_id')->havingRaw('SUM(b_amount) * 2 >= 10000')->get();
     
        $members2 = DB::table('Member_Account_Fund_Sep')->select('member_id')->where('balanceB', '<>',0)->groupBy('member_id')->havingRaw('SUM(balanceB) * 3 >= 15000')->get();
     
        $members = $member1->merge($members2);
     
        $no = 0;

        $temp = [];
     
        foreach($members as $member){
            unset($temp);
            $no++;
         
            $myid = $member->member_id;
         
            //$totalmember = DB::table('Member')->where('direct_id', '=', $myid)->count();
         
            $totalmember2 = DB::table('Member_Account_Fund_Sep')->join('Member', 'Member_Account_Fund_Sep.member_id', '=', 'Member.id')->select('member_id')->where('direct_id', '=', $myid)->groupBy('member_id')->havingRaw('SUM(balanceB) * 3 > 0')->get();
         
            $totalmember = count($totalmember2);
         
            $rank = '';
            $current_rank = '';
         
            if($totalmember >= 5){
                $totalinvest1 = DB::table('Member')->join('Member_Account_Fund_Sep', 'Member.id', '=', 'Member_Account_Fund_Sep.member_id')->where('direct_id', '=', $myid)->sum('balanceb');
             
                $totalinvest = $totalinvest1*3;
             
                if($totalmember >= 15 && $totalinvest >= 400000) $rank = 'PIB';
                else if($totalmember >= 10 && $totalinvest >= 250000) $rank = 'MIB';
                else if($totalmember >= 5 && $totalinvest >= 100000) $rank = 'IB';
             
                if($rank <> ''){
                    $current_rank = DB::table('Member_Sales')->where('member_id', '=', $myid)->first();
             
                    if(empty($current_rank)|| is_null($current_rank->rank) || ($current_rank->rank == 'IB' && $rank <> 'IB') || ($current_rank->rank == 'MIB' && $rank == 'PIB')){
                        DB::table('Member_Sales')
                        ->where('member_id', $myid)
                        ->update(['rank' => $rank,'conditions' => 2]);
                    }

                    if(isset($current_rank->oldconditions2)) $temp = json_decode($current_rank->oldconditions2);
                    $temp[] = 2;
                    DB::table('Member_Sales')->where('member_id', $myid)->update(['oldconditions2' => json_encode($temp)]);
                }
            }
        }
        echo "2";
        sleep(1);
        $this->runBonusNotification3();
    }

    public function runBonusNotification3 () {
        //Condition 3 - 个人投资 >= 15000 + 大客
        $members = DB::table('Member_Account_Fund_Sep')->select('member_id')->where('balanceB', '<>',0)->groupBy('member_id')->havingRaw('SUM(balanceB) * 3 >= 15000')->get();
    
        $no = 0;

        $temp = [];
        
        foreach($members as $member){
            $no++;
            
            $myid = $member->member_id;
            
            $rank = '';
            $current_rank = '';
            
            $dmembers = DB::table('Member')->where('direct_id', '=', $myid)->get();

            $oldconditions2check = '';
            
            foreach($dmembers as $dmember){
                unset($temp);
                $dmyid = $dmember->id;
                
                $totalinvest1 = DB::table('Member_Account_Fund_Sep')->where('member_id', '=', $dmyid)->sum('balanceb');
                
                $totalinvest = $totalinvest1*3;
        
               // if($totalinvest >= 50000)
               //     $rank = 'IB';
        
                if($totalinvest >= 400000) $rank = 'PIB';
                else if($totalinvest >= 200000) $rank = 'MIB';
                else if($totalinvest >= 50000) $rank = 'IB';
                
                if($rank <> ''){
                    $network_data = DB::table('Member_Network')->where('parent_username', 'account2@trfxbiz.com')->where('member_id', '=',$myid)->first();
                    
                    if(!empty($network_data)) $rank = 'IB';
                    
                    $current_rank = DB::table('Member_Sales')->where('member_id', '=', $myid)->first();
                    
                    if(empty($current_rank)|| is_null($current_rank->rank) || ($current_rank->rank == 'IB' && $rank <> 'IB') || ($current_rank->rank == 'MIB' && $rank == 'PIB')){
                        DB::table('Member_Sales')
                        ->where('member_id', $myid)
                        ->update(['rank' => $rank,'conditions' => 3]);
                    }

                    $oldconditions2check = 1;
                }
            }

            if($oldconditions2check){
                if(isset($current_rank->oldconditions2)) $temp = json_decode($current_rank->oldconditions2);
                $temp[] = 3;
                DB::table('Member_Sales')->where('member_id', $myid)->update(['oldconditions2' => json_encode($temp)]);
            }
        }
        echo "3";
        sleep(1);
        $this->runBonusNotification4();
    }

    public function runBonusNotification4 () {
        //Condition 4 - 个人投资 >= 1500
        $members = DB::table('Member_Account_Fund_Sep')->select('member_id')->where('balanceB', '<>',0)->groupBy('member_id')->havingRaw('SUM(balanceB) * 3 >= 1500')->get();
     
        $no = 0;

        $temp = [];
     
        foreach($members as $member){
            unset($temp);

            $no++;
         
            $myid = $member->member_id;
         
            $rank = '';
            $current_rank = '';
         
            $totalinvest1 = DB::table('Member_Account_Fund_Sep')->join('Member', 'Member_Account_Fund_Sep.member_id', '=', 'Member.id')->select('member_id')->where('direct_id', '=', $myid)->groupBy('member_id')->havingRaw('SUM(balanceB) * 3 >= 1500')->get();
         
            if(count($totalinvest1) >= 15) $rank = 'IB';
         
            if($rank <> ''){
                $current_rank = DB::table('Member_Sales')->where('member_id', '=', $myid)->first();
             
                if(empty($current_rank)|| is_null($current_rank->rank)){
                    DB::table('Member_Sales')
                    ->where('member_id', $myid)
                    ->update(['rank' => $rank,'conditions' => 4]);
                }

                if(isset($current_rank->oldconditions2)) $temp = json_decode($current_rank->oldconditions2);
                $temp[] = 4;
                DB::table('Member_Sales')->where('member_id', $myid)->update(['oldconditions2' => json_encode($temp)]);
            }
        }
        echo "4";
        sleep(1);
        $this->runBonusNotification5();
    }

    public function runBonusNotification5 () {
        //Condition 5 - 个人投资 >= 4500
        $members = DB::table('Member_Account_Fund_Sep')->select('member_id')->where('balanceB', '<>',0)->groupBy('member_id')->havingRaw('SUM(balanceB) * 3 >= 4500')->get();
     
        $no = 0;

        $temp = [];
     
        foreach($members as $member){
            unset($temp);
            $no++;
         
            $myid = $member->member_id;
         
            //$totalmember = DB::table('Member')->where('direct_id', '=', $myid)->count();
        
            $totalmember2 = DB::table('Member_Account_Fund_Sep')->join('Member', 'Member_Account_Fund_Sep.member_id', '=', 'Member.id')->select('member_id')->where('direct_id', '=', $myid)->groupBy('member_id')->havingRaw('SUM(balanceB) * 3 > 0')->get();
        
            $totalmember = count($totalmember2);
         
            $rank = '';
            $current_rank = '';
         
            if($totalmember >= 5){
                $totalinvest1 = DB::table('Member')->join('Member_Account_Fund_Sep', 'Member.id', '=', 'Member_Account_Fund_Sep.member_id')->where('direct_id', '=', $myid)->sum('balanceb');
             
                $totalinvest = $totalinvest1*3;
             
                if($totalinvest >= 30000) $rank = 'IB';
             
                if($rank <> ''){
                    $current_rank = DB::table('Member_Sales')->where('member_id', '=', $myid)->first();
                 
                    if(empty($current_rank)|| is_null($current_rank->rank)){
                        DB::table('Member_Sales')
                        ->where('member_id', $myid)
                        ->update(['rank' => $rank,'conditions' => 5]);
                    }

                    if(isset($current_rank->oldconditions2)) $temp = json_decode($current_rank->oldconditions2);
                    $temp[] = 5;
                    DB::table('Member_Sales')->where('member_id', $myid)->update(['oldconditions2' => json_encode($temp)]);
                }
            }
        }
        echo "5";
        sleep(1);
        $this->runBonusNotification6();
    }

    public function runBonusNotification6 () {
        //Condition 6 - 5 IB 升级 MIB
        $members = DB::table('Member_Sales')->where('rank', '=', 'IB')->get();
    
        $no = 0;

        $temp = [];
    
        foreach($members as $member){
            unset($temp);
            $no++;
            
            $myid = $member->member_id;
            
            $totalmember2 = DB::table('Member_Sales')->join('Member', 'Member_Sales.member_id', '=', 'Member.id')->select('member_id')->where('direct_id', '=', $myid)->whereIn('rank', ['MIB','PIB'])->get();
        
            $totalmember = count($totalmember2);
        
            if($totalmember >= 6){
                
                DB::table('Member_Sales')
                ->where('member_id', $myid)
                ->update(['rank' => 'PIB','conditions' => 6]);

                $current_rank = DB::table('Member_Sales')->where('member_id', '=', $myid)->first();
                if(isset($current_rank->oldconditions2)) $temp = json_decode($current_rank->oldconditions2);
                $temp[] = 6;
                DB::table('Member_Sales')->where('member_id', $myid)->update(['oldconditions2' => json_encode($temp)]);
            }
            else{
                $totalmember2 = DB::table('Member_Sales')->join('Member', 'Member_Sales.member_id', '=', 'Member.id')->select('member_id')->where('direct_id', '=', $myid)->whereIn('rank', ['IB','MIB','PIB'])->get();

                $totalmember = count($totalmember2);

                if($totalmember >= 5){
                    DB::table('Member_Sales')
                    ->where('member_id', $myid)
                    ->update(['rank' => 'MIB','conditions' => 6]);

                    $current_rank = DB::table('Member_Sales')->where('member_id', '=', $myid)->first();
                    if(isset($current_rank->oldconditions2)) $temp = json_decode($current_rank->oldconditions2);
                    $temp[] = 6;
                    DB::table('Member_Sales')->where('member_id', $myid)->update(['oldconditions2' => json_encode($temp)]);
                }
            }
        }
        echo "6";
        sleep(1);
        $this->runBonusNotification7();
    }

    public function runBonusNotification7 () {
        //Condition 7 - 6 MIB 升级 PIB
        $members = DB::table('Member_Sales')->where('rank', '=', 'MIB')->get();
    
        $no = 0;

        $temp = [];
        
        foreach($members as $member){
            unset($temp);
            $no++;
            
            $myid = $member->member_id;
            
            $totalmember2 = DB::table('Member_Sales')->join('Member', 'Member_Sales.member_id', '=', 'Member.id')->select('member_id')->where('direct_id', '=', $myid)->whereIn('rank', ['MIB','PIB'])->get();
         
            $totalmember = count($totalmember2);
         
            if($totalmember >= 6){
                DB::table('Member_Sales')
                ->where('member_id', $myid)
                ->update(['rank' => 'PIB','conditions' => 7]);

                $current_rank = DB::table('Member_Sales')->where('member_id', '=', $myid)->first();
                if(isset($current_rank->oldconditions2)) $temp = json_decode($current_rank->oldconditions2);
                $temp[] = 7;
                DB::table('Member_Sales')->where('member_id', $myid)->update(['oldconditions2' => json_encode($temp)]);
            }
        }
        echo "7";
        sleep(1);
    }

    public function blockSpecialGroupUser () {
        $username = '58811388@qq.com';

        $network = DB::table('Member_Network')->where('parent_username', $username)->get(['username']);
        foreach ($network as $memberUsername) {
            DB::table('users')->where('username',$memberUsername->username)->update(['special_group_block' => 1]);
        }
    }
}
