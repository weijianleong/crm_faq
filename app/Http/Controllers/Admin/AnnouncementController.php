<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositories\AnnouncementRepository;
use Carbon\Carbon;

class AnnouncementController extends Controller
{
    /**
     * The AnnouncementRepository instance.
     *
     * @var \App\Repositories\AnnouncementRepository
     */
    protected $AnnouncementRepository;

    /**
     * Create a new AnnouncementController instance.
     *
     * @param \App\Repositories\AnnouncementRepository $AnnouncementRepository
     * @return void
     */
    public function __construct(AnnouncementRepository $AnnouncementRepository) {
        $this->AnnouncementRepository = $AnnouncementRepository;
        $this->middleware('admin');
    }

    /**
     * Datatable List
     * @return [type] [description]
     */
    public function getList () {
        return $this->AnnouncementRepository->getAdminList();
    }

    /**
     * Edit Announcement Page
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function getEdit ($id) {
        if (!$model = $this->AnnouncementRepository->findById($id)) {
            return redirect()->back()->with('flashMessage', [
                'class' => 'danger',
                'message' => 'Data not found.'
            ]);
        }

        return view('back.announcement.edit')->with('model', $model);
    }

    /**
     * Create Announcement
     * @return [type] [description]
     */
    public function postCreate () {
        $data = \Input::get('data');
        if (isset($data['files'])) unset($data['files']);

        try {

            $now = Carbon::now()->format('Y-m-d');
            $check_refrence = $this->AnnouncementRepository->check_refrence($now);

            if(count($check_refrence) < 1){

                $data['refrence_id'] = Carbon::now()->format('y').'-'.Carbon::now()->format('m').Carbon::now()->format('d').'01';   
            
            }else{

                $last = substr($check_refrence->refrence_id,7);
                if( ($last + 1) <= 9){
                    $data['refrence_id'] = Carbon::now()->format('y').'-'.Carbon::now()->format('m').Carbon::now()->format('d').'0'.($last + 1);
                }else{
                    $data['refrence_id'] = Carbon::now()->format('y').'-'.Carbon::now()->format('m').Carbon::now()->format('d').($last + 1);
                }
            }

            $announcementId = $this->AnnouncementRepository->store($data);
        } catch (\Exception $e) {
            return \Response::json([
                'type' => 'error',
                'message' => $e->getMessage()
            ]);
        }

        \DB::table('Notification')->insert(['notify_id' =>  $announcementId->id, 'type' => 0, 'created_at' => \Carbon\Carbon::now()]);

        return \Response::json([
            'type' => 'success',
            'message' => 'Announcement created.'
        ]);
    }

    /**
     * Update Announcement
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function postUpdate ($id) {
        $data = \Input::get('data');
        if (!$model = $this->AnnouncementRepository->findById($id)) {
            return \Response::json([
                'type' => 'error',
                'message' => 'Data not found'
            ]);
        }
        if (isset($data['files'])) unset($data['files']);
        $model->title_en = $data['title_en'];
        $model->title_chs = $data['title_chs'];
        $model->title_cht = $data['title_cht'];
        $model->content_en = $data['content_en'];
        $model->content_chs = $data['content_chs'];
        $model->content_cht = $data['content_cht'];
        $model->created_at = $data['created_at'];
        $model->save();

        return \Response::json([
            'type' => 'success',
            'message' => 'Announcement updated.'
        ]);
    }

    /**
     * Remove Announcement
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function remove ($id) {
        if (!$model = $this->AnnouncementRepository->findById($id)) {
            return \Response::json([
                'type' => 'error',
                'message' => 'Data not found'
            ]);
        }

        $model->delete();
        \DB::table('Notification')->where('notify_id',$id)->delete();

        return \Response::json([
            'type' => 'success',
            'message' => 'Announcement removed.'
        ]);
    }
}
