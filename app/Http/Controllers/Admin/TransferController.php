<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositories\TransferRepository;
use App\Repositories\MemberRepository;

class TransferController extends Controller
{
	/**
     * The SharesRepository instance.
     *
     * @var \App\Repositories\TransferRepository
     */
    protected $TransferRepository;

    /**
     * The MemberRepository instance.
     *
     * @var \App\Repositories\MemberRepository
     */
    protected $MemberRepository;

    public function __construct(
        TransferRepository $TransferRepository,
        MemberRepository $MemberRepository
    ) {
        $this->middleware('admin');
        $this->TransferRepository = $TransferRepository;
        $this->MemberRepository = $MemberRepository;
    }

    /**
     * Datatable member admin
     * @return [type] [description]
     */
    public function getList () {
        return $this->TransferRepository->findAll(true);
    }
    
    /**
     * Get edit Transfer
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function getEdit ($id) {
        if (!$model = $this->TransferRepository->findById(trim($id))) {
            return redirect()->back()->with('flashMessage', [
                'class' =>  'danger',
                'message' => 'Transfer not found.'
            ]);
        }
        return view('back.transfer.edit')->with('model', $model);
    }

    /**
     * Add statement
     * @return json
     */
    public function postAdd () {
        $data = \Input::get('data');
        
        $currentuser = \Sentinel::getUser();
        

        if (!$to = $this->MemberRepository->findByUsername(trim($data['to']))) {
            return \Response::json([
                'type' => 'error',
                'message' => 'Member not found.'
            ]);
        }
        else
        {
            $walletTo = $to->wallet;
        }

        try {
            $this->TransferRepository->store([
            'from_member_id' =>  $currentuser->id,
            'to_member_id' =>  $to->id,
            'from_username' => $currentuser->username,
            'to_username' => $to->username,
            'remark' => $data['remark'],
            'amount' => (float) $data['amount']
            ]);
        } catch (\Exception $e) {
            return \Response::json([
                'type' => 'error',
                'message' => $e->getMessage()
            ]);
        }
        
        
        
        if($data['wallet_type'] == 'R')
        {
            $ramount = $data['amount'];
            $bamount = 0;
            $walletTo->register_point += $data['amount'];
            $walletTo->save();
            
            $balance = $walletTo->register_point;
        }
        else
        {
            $bamount = $data['amount'];
            $ramount = 0;
            $walletTo->promotion_point += $data['amount'];
            $walletTo->save();
            
            $balance = $walletTo->promotion_point;
        }
        
        \DB::table('Member_Wallet_Statement')->insert([
                  'admin_id' => $currentuser->id,
                'member_id' => $to->id,
                'action_type' => 'Admin Transfer',
                'wallet_type' => $data['wallet_type'],
                'transaction_type' => $data['transaction_type'],
                'remark' => $data['remark'],
                'balance' => (float) $balance,
              'register_amount' => (float) $ramount,
             'promotion_amount' => (float) $bamount,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        return \Response::json([
            'type'  =>  'success',
            'message' => 'Points transferred.'
        ]);
    }

    /**
     * Update shares settings
     * @param integer $id
     * @return [type] [description]
     */
    public function postUpdate ($id) {
        $data = \Input::get('data');
        if (!$model = $this->TransferRepository->findById(trim($id))) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  'Transfer not found.'
            ]);
        }

        $this->TransferRepository->update($model, $data);

        return \Response::json([
            'type'  =>  'success',
            'message'   =>  'Transfer #' . $model->id . ' updated.'
        ]);
    }

    /**
     * Remove transfer statement
     * @param integer $id
     * @return json
     */
    public function postDelete ($id) {
        $model = new \App\Models\Transfer;
        $model->where('id', trim($id))->delete();

        return \Response::json([
            'type' => 'success',
            'message' => 'Transfer data removed.'
        ]);
    }
}
