<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Repositories\MemberRepository;
use App\Repositories\AdminRightRepository;
use App\Repositories\HelpdeskRepository;

class SiteController extends Controller
{
    protected $AdminRightRepository;
    public function __construct(AdminRightRepository  $AdminRightRepository,HelpdeskRepository  $HelpdeskRepository) {
        $this->AdminRightRepository = $AdminRightRepository;
        $this->HelpdeskRepository = $HelpdeskRepository;

        $this->middleware('admin', ['except' => [
            'getLogin',
            'postLogin',
            'getLogout'
        ]]);
    }

    public function getRight () {
        return view('back.right.editRight');
    }

    public function getLogin () {
        return view('back.login');
    }

    public function getIndex () {
        return view('back.home');
    }

    public function getAccountSettings () {
        return view('back.settings');
    }

    
    public function runBonus () {
        return view('back.runbonus');
    }
    
    public function tradeProcess () {
        return view('back.tradeprocess');
    }
    
    public function tradeSync () {
        return view('back.tradesync');
    }
    
    public function updateinvestorpass () {
        return view('back.updateinvestorpass');
    }
    
    public function bulkupdatePass () {
        return view('back.bulkupdatepass');
    }
    public function getLogout () {
        if ($user = \Sentinel::getUser()) {
            \Sentinel::logout($user);
        }
        
        return view('back.login');
    }

    public function postLogin () {
        $data = \Input::get('data');
        
        try {
            //dd(captcha_check($data['captcha']));
           
            $code = $data['captcha'];
            $isHuman = captcha_check($code);
            
           
            if(!$isHuman) {
                throw new \Exception('Captcha not match', 1);
                return false;
            }
            
            $user = \Sentinel::authenticate([
                'email'  =>  $data['email'],
                'password'  =>  $data['password']
            ], (isset($data['remember'])));

            if (!$user) {
                throw new \Exception('Email / Password do not match.', 1);
                return false;
            }

            $permissions = $user->permissions;
            if (!isset($permissions['admin'])) {
                throw new \Exception('Cannot login here.', 1);
                return false;
            } else if ($permissions['admin'] != 1) {
                throw new \Exception('Cannot login here.', 1);
                return false;
            }
        } catch (\Exception $e) {
            \Sentinel::logout();
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  $e->getMessage()
            ]);
        }

        //$model = $this->AdminRightRepository->findById($user->id);
        if (!$model = $this->AdminRightRepository->findById($user->id)) {

            //$admin = \Sentinel::getUser();
            $right_data["right"] ='{"home":true,"profile":true}';
            $right_data["user_id"] =$user->id;
            $right_data["level"] ="admin";
            $right_data["created_by"] =$user->email;

            if(!$model = $this->AdminRightRepository->autoInsert($user->id,$right_data)){
                 return redirect()->back()->with('flashMessage', [
                     'class' => 'danger',
                     'message' => 'Data not found.'
                 ]);
            }
            $model = $this->AdminRightRepository->findById($user->id);
        }

        $rights = json_decode($model->right, true);

        foreach ($rights as $right => $value){
          
          if($value == 1){
            
            $lists[] = $right;
          }
        }
        session(['right' => $lists]);

        return \Response::json([
            'type'      =>  'success',
            'message'   =>  'Redirecting to dashboard..',
            'redirect'  =>  route('admin.home'),
        ]);
    }

    public function postUpdateAccount () {
        $data = \Input::get('data');
        $user = \Sentinel::getUser();
        
        
        if ($data['newPassword'] != '') {
            if($data['newPassword'] != $data['confirmPassword']){
                return \Response::json([
                   'type'  =>  'error',
                   'message'   =>  'New and Confirm password not match'
               ]);
            }
            else{
                try {
                    \Sentinel::update($user, [
                          'password'  =>  trim($data['newPassword'])
                      ]);
                } catch (\Exception $e) {
                    return \Response::json([
                       'type'  =>  'error',
                       'message'   =>  $e->getMessage()
                   ]);
                }
            }
        }
        else{
            try {
                \Sentinel::update($user, [
                      'email'  =>  trim($data['email']),
                  ]);
            } catch (\Exception $e) {
                return \Response::json([
                   'type'  =>  'error',
                   'message'   =>  $e->getMessage()
               ]);
            }
        }

        return \Response::json([
            'type'  =>  'success',
            'message'   =>  'Account updated.'
        ]);
    }
    
    public function getMemberList () {
        return view('back.member.list');
    }
    
    public function getMemberList2 () {
        return view('back.member.list2');
    }

    public function getMemberWallet () {
        return view('back.wallet.list');    
    }

    public function getWalletStatement ($id) {
        return view('back.wallet.statementList')->with('id', $id);
    }

    public function getMemberRegister () {
        return view('back.member.register');
    }

    public function getMemberRegisterCommon () {
        return view('back.member.register2');
    }

    public function getWithdrawAddStatement () {
        return view('back.withdraw.addStatement');
    }

    public function getWithdrawList () {
        return view('back.withdraw.list');
    }
    public function getWithdrawPendingList () { //weilun
        return view('back.withdraw.plist');
    }

    public function getTransferAddStatement () {
        return view('back.transfer.addStatement');
    }

    public function getTransferList () {
        return view('back.transfer.list');
    }

    public function getBonusAddStatement () {
        return view('back.bonus.addStatement');
    }

    public function getBonusList () {
        return view('back.bonus.list');
    }

    public function getPackageSettings () {
        return view('back.package.settings');
    }
    
    public function getDeclareSettings () {
        return view('back.declare.settings');
    }
    
    public function getTradeSettings () {
        return view('back.trade.settings');
    }

    public function getSharesSettings () {
        return view('back.shares.settings');
    }

    public function getSharesSellAdmin () {
        return view('back.shares.sell');
    }

    public function getSharesSell () {
        return view('back.shares.sellList');
    }

    public function getSharesBuy () {
        return view('back.shares.buyList');
    }

    public function getSharesSplit () {
        return view('back.shares.split');
    }

    public function getSharesLock () {
        return view('back.shares.lockList');
    }

    public function getMemberEdit ($id) {
        $instance = new MemberRepository;
        if (!$model = $instance->findById(trim($id))) {
            return redirect()->route('admin.member.list')->with('flashMessage', [
                'class'  =>  'warning',
                'message'   =>  'Member not found.'
            ]);
        }
        return view('back.member.edit')->with('model', $model);
    }

    public function runCron () {
        if (\Input::get('type') == 'pairing') {
            \Artisan::call('bonus:pairing');
        } else if (\Input::get('type') == 'checkGroup') {
            \Artisan::call('member:group');
        } else if (\Input::get('type') == 'group') {
            //\Artisan::call('bonus:group');
            return view('back.bonus');
        } else if (\Input::get('type') == 'freeze') {
            \Artisan::call('shares:freeze');
        }

        return \Response::json([
            'type' => 'success',
            'message' => 'Job completed successfully'
        ]);
    }

    public function getWebsitelist(){
        return view('back.website.web_list'); //weilun
    }

    public function getWebsiteCreate(){
        return view('back.website.web_create'); //weilun
    }

    public function getPersonalRating () {
        
        if(session()->has('personal_date')){

            $get_date = session()->get('personal_date');
            
        }else{

            $date =  Carbon::now();
            $get_date =  $date->format('Y').'-'.$date->format('m');

        }

        $select_date = $this->HelpdeskRepository->getPersonalDate();

        return view('back.helpdesk.personalRating')->with('date',$get_date)->with('select_date',$select_date);
    }

    public function getHelpdeskRecord(){
        return view('back.helpdesk.record'); //weilun
    }

    public function getHelpdesklist(){
        return view('back.helpdesk.helpdesk_list'); //weilun
    }
    public function getHelpdeskPlist(){
        $user = \Sentinel::getUser();
        $model = $this->AdminRightRepository->findById($user->id);
        $right = json_decode($model->right, true);
        $type_Id = explode(",",$model->cs_type);
        if($type_Id[0]==0){
            return view('back.helpdesk.helpdesk_plist'); //weilun
        }else{
            return view('back.helpdesk.helpdesk_list'); //weilun
        }
        
    }
    public function getHelpdeskschedule(){
        return view('back.helpdesk.schedule'); //weilun
    }
    public function getHelpdeskblacklist(){
        return view('back.helpdesk.blacklist'); //weilun
    }
    public function createAnnouncement () {
        return view('back.announcement.create');
    }

    public function getAnnouncementList () {
        return view('back.announcement.list');
    }

    public function maintenance () {
        if (\App::isDownForMaintenance()) {
            \Artisan::call('up');
        } else {
            \Artisan::call('down');
        }

        return \Response::json([
            'type'  =>  'information',
            'message' =>  'MT status toggled, refresh to see changes.'
        ]);
    }

    /**
     * Upload Image
     * @param  Request $req [description]
     * @return [type]       [description]
     */
    public function uploadImage (Request $req) {
        $validator = \Validator::make($req->all(), [
            'imageFile' => 'max:2048|mimes:jpeg,png',
        ]);

        if ($validator->fails()) {
            return \Response::json([
                'type'  =>  'error',
                'message' =>  'File must be smaller than 2mb and in jpg or png format.'
            ]);
        }
        $file = $req->file('imageFile');
        $destinationPath = 'uploads/images/' . date('m') . '/';
        $filename = time() . '-' . $file->getClientOriginalName();
        $file->move(public_path() . '/' . $destinationPath, $filename);
        $image = $destinationPath . $filename;
        return \Response::json([
            'type'  =>  'success',
            'url'   =>  asset($image)
        ]);
    }

    public function getManualDeposit () {
        return view('back.manualDeposit');
    }
}
