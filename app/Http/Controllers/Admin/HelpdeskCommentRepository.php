<?php

namespace App\Repositories;

use App\Models\Helpdesk_Comment;
use App\Models\Helpdesk_record;
use Yajra\Datatables\Facades\Datatables;

class HelpdeskCommentRepository extends BaseRepository
{
    protected $model;
    protected $allowedFields = [];
    protected $booleanFields = [];

    public function __construct() {
        $this->model = new Helpdesk_Comment;
        $this->record = new Helpdesk_record;
    }

    protected function saveModel($model, $data) {
        foreach ($data as $k=>$d) {
            $model->{$k} = $d;
        }
        $model->save();
        return $model;
    }

    public function store($data) {
        $model = $this->saveModel(new $this->model, $data);
        return $model;
    }

    public function update($model, $data) {
        $model = $this->saveModel($model, $data);
        return $model;
    }

    public function getAllowedFields () {
        return $this->allowedFields;
    }

    public function getBooleanFields () {
        return $this->booleanFields;
    }

    public function findById ($id) {

        return $this->model->where('helpdesk_id', $id)->get();
    }

    public function update_record ($id,$data) {

        return $this->record->where('id',$id)->update($this->record,$data);
       
    }
    public function first_record ($data,$reply_case) {

        return $this->record->updateOrCreate(['email' => $data["email"], 'created_at' => $data["created_at"] ],['reply_case' => $reply_case,'reply_sec' => $data["reply_sec"]]);
       
    }

    public function check_record ($data) {

        return $this->record->where('email', $data["email"])->where('created_at',$data["created_at"])->first();
    }

    public function getLastcomment ($helpdesk_id,$user_id) {

        return $this->model->where('helpdesk_id', $helpdesk_id)->where('client_id', $user_id)->orderby('created_at','desc')->first();
    }

    /**
     * Datatable List - Member
     * @return [type] [description]
     */
    public function getList () {
        return Datatables::eloquent($this->model->query())
                ->addColumn('action', function ($model) {
                    return view('front.helpdesk.action')->with('model', $model);
                })
                ->editColumn('created_at', function ($model) {
                    return $model->created_at->format('Y-m-d H:i A');
                })
                ->editColumn('status', function ($model) {
                    return $model->status;
                })
                ->editColumn('is_reply', function ($model) {
                    if($model->is_reply==0){
                        return "客服处理中";
                    }else{
                        return "客服已回复";
                    }
                })
                ->rawColumns(['action'])
                ->make(true);
    }

    /**
     * Datatable List - Admin
     * @return [type] [description]
     */
    public function getAdminList () {
        return Datatables::eloquent($this->model->query())
                ->addColumn('action', function ($model) {
                    return view('back.helpdesk.action')->with('model', $model);
                })
                ->editColumn('is_reply', function ($model) {
                    if($model->is_reply==0){
                        return "客服处理中";
                    }else{
                        return "客服已回复";
                    }
                })
                ->rawColumns(['action'])
                ->make(true);
    }

}