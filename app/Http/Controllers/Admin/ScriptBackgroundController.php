<?php

namespace App\Http\Controllers\Admin;

use DB;
use File;
use Mail;
use Excel;
use Carbon\Carbon;
use App\Repositories\MemberRepository;

class ScriptBackgroundController extends Controller
{
    public function __construct() {
        $this->middleware('admin', ['except' => ['totalInvestmentNotification', 'runBonusNotification', 'updateInvestorPasswordNotification', 'updateDailyWeather', 'dailyInvestmentRecord', 'monthEndBackupRanking', 'monthEndBackupSales', 'updateMemberSalesDetails', 'updateMemberSalesSummary']]);
    }

    public function totalInvestmentNotification () {
        $path = app_path().'/dataStore.txt';
        $pathExcel = app_path().'/InvestmentReport.xlsx';
        $dataList = array();

        if(file_exists($path)) $oriTotalInvestment = File::get($path);
        else $oriTotalInvestment = 0;

        $username = '58811388@qq.com';
        $member = DB::table('Member')->where('username', $username)->first();

        $network = DB::table('Member_Network')->where('parent_id', $member->id)->get(['member_id']);
        foreach ($network as $memberID) {
            $memberIDAry[] = $memberID->member_id;
        }
        
        $totalInvestment = DB::table('Member_Account_Fund')->whereIn('member_id', $memberIDAry)->where('balanceB', '<>', 0)->count();
        File::put($path, $totalInvestment);

        if((float)$totalInvestment > (float)$oriTotalInvestment){
            $partner = DB::connection('mysql3')->table('user')->where('UserType', 2)->get(['UserId','ChineseName']);
            foreach ($partner as $item) {
                $partnerAry[$item->UserId] = $item->ChineseName;
            }

            $list = DB::table('users')->join('Member', 'users.username', '=', 'Member.username')
                                      // ->join('Member_Network', 'Member.id', '=', 'Member_Network.member_id')
                                      ->join('Member_Account_Fund', 'Member.id', '=', 'Member_Account_Fund.member_id')
                                      ->where('Member.username', $username)
                                      ->select('users.first_name as Name', 'Member.username as Email', 'Member.id as Level', 'Member_Account_Fund.bookA as BookA', 'Member_Account_Fund.balanceB as AmountA', 'Member_Account_Fund.subscribe_date as Subcribe Date', 'Member_Account_Fund.fundcompany as Fund Company ID')
                                      ->get();

            foreach ($list as $listDetail) {
                foreach ($listDetail as $key => $value) {
                    $temp[$key] = $value;
                    if($key == 'Level') $temp[$key] = 0;
                    if($key == 'AmountA') $temp[$key] = $value*2;
                    if($key == 'Fund Company ID'){
                        if($value) $temp['Fund Company Name'] = $partnerAry[$value];
                        else $temp['Fund Company Name'] = '';
                    }
                }
                array_push($dataList, $temp);
            }

            $list = DB::table('users')->join('Member', 'users.username', '=', 'Member.username')
                                      ->join('Member_Network', 'Member.id', '=', 'Member_Network.member_id')
                                      ->join('Member_Account_Fund', 'Member.id', '=', 'Member_Account_Fund.member_id')
                                      ->where('Member_Network.parent_username', $username)
                                      ->select('users.first_name as Name', 'Member.username as Email', 'Member_Network.my_level as Level', 'Member_Account_Fund.bookA as BookA', 'Member_Account_Fund.balanceB as AmountA', 'Member_Account_Fund.subscribe_date as Subcribe Date', 'Member_Account_Fund.fundcompany as Fund Company ID')
                                      ->orderBy('Member_Account_Fund.subscribe_date', 'asc')
                                      ->get();
            foreach ($list as $listDetail) {
                foreach ($listDetail as $key => $value) {
                    $temp[$key] = $value;
                    if($key == 'AmountA') $temp[$key] = $value*2;
                    if($key == 'Fund Company ID'){
                        if($value) $temp['Fund Company Name'] = $partnerAry[$value];
                        else $temp['Fund Company Name'] = '';
                    }
                }
                array_push($dataList, $temp);
            }

            //insertDB
            foreach ($dataList as $item) {
                $Special_Account = DB::table('Special_Account')->where('bookA', $item['BookA'])->count();
                if($Special_Account == 0) DB::table('Special_Account')->insert(['bookA' => $item['BookA'], 'type' => 1]);
            }

            Excel::create('InvestmentReport', function($excel) use($dataList) {
                // Set the title
                $excel->setTitle('Investment Report');
                // Chain the setters
                $excel->setCreator('Jimmy')->setCompany('Treal Technology');
                $excel->setDescription('Total Investment Under a Tree');
                $excel->sheet('Sheet 1', function ($sheet) use($dataList) {
                    $sheet->setOrientation('landscape');
                    $sheet->fromArray($dataList, NULL, 'A1');
                });
            })->store('xlsx',app_path());

            $mailTo[] = 'jimmy.pua@trealtechnology.com';
            $mailTo[] = 'siangteck.lee@trealtechnology.com';
            $mailTo[] = 'mingaik.tan@trealtechnology.com';
            $mailTo[] = 'teamo2013@qq.com';

            Mail::send('front.emails.notification', [], function ($message) use($mailTo, $pathExcel)
            {
                foreach ($mailTo as $email) {
                    $message->to($email);
                }
                $message->subject('New Investment Notification');
                $message->attach($pathExcel);
            });
        }
    }

    public function runBonusNotification () {
        //initialize data
        $notInSalesMember = DB::table('Member')->whereNotIn('id', function($q){$q->select('member_id')->from('Member_Sales');})->get(['id', 'username']);
        foreach ($notInSalesMember as $detail) {
            DB::table('Member_Sales')->insert([
                                        'member_id' => $detail->id,
                                        'username' => $detail->username
                                    ]);
        }

        DB::table('Member_Sales')->update(['rank' => null, 'conditions' => 0]);

        // start process
        $members = DB::table('Member')->where('id', '<>',1)->where('id', '<>',2)->whereNotIn('id', function($q){$q->select('member_id')->from('Member_Network');})->get();
        foreach($members as $member){
            $myid = $member->id;
            $mylevel = $member->level;
            $myusername = $member->username;
            $upline = $member->register_by;
            $upline_id = $member->direct_id;
            $totallotsize = 0;
            
         /*
            $totaltransferx1 = DB::table('Member_Blacklist')->where('member_id', '=', $myid)->where('blacklist_at', '>', '2018-09-01 05:00:00')->where('blacklist_at', '<=', '2018-10-01 05:00:00')->where('b_book', '<>', 0)->sum('b_book');
            
            $totaltransferx2 = \DB::table('Member_Wallet_Statement_Fund')->where('member_id', '=',$myid)->where('action_type', '=', 'Transfer to MT5')->where('created_at', '>', '2018-09-01 05:00:00')->where('created_at', '<=', '2018-10-01 05:00:00')->sum('b_amount');
    
            $transferamountx1 = ($totaltransferx2 * 2) - $totaltransferx1;
            
            $accounts = DB::table('Member_Account_Fund_Aug')->where('member_id', '=',$myid)->get();
            
            foreach($accounts as $account)
            {
                $bookA = $account->bookA;
                
                $volume = DB::connection('mysql2')->table('historyposition')->where('Login', '=', $bookA)->where('CloseTime', '>=', '2018-09-01')->where('CloseTime', '<', '2018-10-01')->sum('Volume');
                
           
                
                $lotsize1 = $volume / 10000;
                
                //$lotsize2 = ($volume2 / 10000) / 2;
                
                $totallotsize = $totallotsize + $lotsize1; // - $lotsize2;
            }
 
            $totaldepositx = \DB::table('Member_Account_Fund_Aug')->where('member_id', '=',$myid)->sum('balanceB');
            
            $depositamountx1 = $totaldepositx * 3;
            
            echo $myusername;
            echo "<BR>";
            echo $transferamountx1;
            echo "<BR>";
            echo $depositamountx1;
            echo "<BR>";
            echo $totallotsize;
            echo "<BR>";
  
            \DB::table('Member_Sales_Aug')
                    ->where('member_id', $myid)
                    ->update([
                             'totaltransfer'    =>  $transferamountx1,
                             'totaldeposit'    =>  $depositamountx1,
                             'totallotsize'    =>  $totallotsize,
                             'status'    =>  'Y',
                             'created_at' => \Carbon\Carbon::now(),
                             'updated_at' => \Carbon\Carbon::now()
                             ]);
*/
/*
            \DB::table('Member_Sales_Jul')->insert([
                            'member_id' =>  $myid,
                            'username'    =>  $myusername,
                            'totaltransfer'    =>  $transferamountx1,
                            'totaldeposit'    =>  $depositamountx1,
                            'totallotsize'    =>  $totallotsize,
                            'created_at' => \Carbon\Carbon::now(),
                            'updated_at' => \Carbon\Carbon::now()
                         ]);
*/
 
            $mlevel = 0;
            while($mylevel > 2)
            {
                $umember = DB::table('Member')->where('id', '=',$upline_id)->first();
                
                $upline = $umember->register_by;
                $upline_id = $umember->direct_id;
                $upid = $umember->id;
                $mylevel = $umember->level;
                $upusername = $umember->username;
                
                $mlevel++;
                
                \DB::table('Member_Network')->insert([
                            'member_id' =>  $myid,
                            'username'    =>  $myusername,
                            'parent_id'    =>  $upid,
                            'parent_username'   =>  $upusername,
                            'parent_level'   =>  $mylevel,
                            'my_level'   =>  $mlevel,
                            'created_at' => \Carbon\Carbon::now(),
                            'updated_at' => \Carbon\Carbon::now()
                         ]);
            }
        }
        echo "0";
        sleep(1);
        $this->runBonusNotification1();
    }

    public function runBonusNotification1 () {
        //Condition 1 - 个人流水
        $members = DB::table('Member_Account_Fund')->select('member_id')->where('balanceB', '<>',0)->groupBy('member_id')->havingRaw('SUM(balanceB) * 3 >= 7500')->get();
    
        $no = 0;

        foreach($members as $member){
            $no++;
     
            $myid = $member->member_id;
         
            $totalinvest = DB::table('Member_Account_Fund')->where('member_id', '=', $myid)->sum('balanceB');
             
            $totalinvest2 = $totalinvest * 3;
        
            $totalmember2 = DB::table('Member_Account_Fund')->join('Member', 'Member_Account_Fund.member_id', '=', 'Member.id')->select('member_id')->where('direct_id', '=', $myid)->groupBy('member_id')->havingRaw('SUM(balanceB) * 3 > 0')->get();
         
            $rank = '';
            $current_rank = '';
         
            $totalmember = count($totalmember2);
             
            if($totalmember >= 5){
                $totalsales1 = DB::table('Member')->join('Member_Wallet_Statement_Fund', 'Member.id', '=', 'Member_Wallet_Statement_Fund.member_id')->where('direct_id', '=', $myid)->where('action_type', '=', 'Transfer to MT5')->where('Member_Wallet_Statement_Fund.created_at', '>', '2018-10-01 05:00:00')->where('Member_Wallet_Statement_Fund.created_at', '<=', '2018-11-01 05:00:00')->sum('b_amount');
         
                $totalsales2 = DB::table('Member')->join('Member_Blacklist', 'Member.id', '=', 'Member_Blacklist.member_id')->where('Member.direct_id', '=', $myid)->where('Member_Blacklist.blacklist_at', '>', '2018-10-01 05:00:00')->where('Member_Blacklist.blacklist_at', '<=', '2018-11-01 05:00:00')->where('b_book', '<>', 0)->sum('b_book');
         
                $totalsales = ($totalsales1 * 2) - ($totalsales2);
         
                if($totalinvest2 >= 15000 && $totalmember >= 15 && $totalsales >= 100000)
                    $rank = 'PIB';
                else if($totalinvest2 >= 15000 && $totalmember >= 10 && $totalsales >= 60000)
                    $rank = 'MIB';
                else if($totalinvest2 >= 7500 && $totalmember >= 5 && $totalsales >= 25000)
                    $rank = 'IB';

                if($rank <> ''){
                    $current_rank = DB::table('Member_Sales')->where('member_id', '=', $myid)->first();
         
                    if(empty($current_rank)|| is_null($current_rank->rank) || ($current_rank->rank == 'IB' && $rank <> 'IB') || ($current_rank->rank == 'MIB' && $rank == 'PIB')){
                        DB::table('Member_Sales')
                        ->where('member_id', $myid)
                        ->update(['rank' => $rank,'conditions' => 1]);
                    }
                }
            }
        }
        echo "1";
        sleep(1);
        $this->runBonusNotification2();
    }

    public function runBonusNotification2 () {
        //Condition 2 - 个人投资
        $members = DB::table('Member_Account_Fund')->select('member_id')->where('balanceB', '<>',0)->groupBy('member_id')->havingRaw('SUM(balanceB) * 3 >= 7500')->get();
     
        $no = 0;
     
        foreach($members as $member){
            $no++;
         
            $myid = $member->member_id;
         
            $totalinvest = DB::table('Member_Account_Fund')->where('member_id', '=', $myid)->sum('balanceB');
             
            $totalinvest2 = $totalinvest * 3;
         
            $totalmember2 = DB::table('Member_Account_Fund')->join('Member', 'Member_Account_Fund.member_id', '=', 'Member.id')->select('member_id')->where('direct_id', '=', $myid)->groupBy('member_id')->havingRaw('SUM(balanceB) * 3 > 0')->get();
         
            $totalmember = count($totalmember2);
         
            $rank = '';
            $current_rank = '';
         
            if($totalmember >= 5){
                $totalinvest1 = DB::table('Member')->join('Member_Account_Fund', 'Member.id', '=', 'Member_Account_Fund.member_id')->where('direct_id', '=', $myid)->sum('balanceb');
             
                $totalinvest = $totalinvest1*3;
             
                if($totalinvest2 >= 15000 && $totalmember >= 15 && $totalinvest >= 400000)
                    $rank = 'PIB';
                else if($totalinvest2 >= 15000 && $totalmember >= 10 && $totalinvest >= 250000)
                    $rank = 'MIB';
                else if($totalinvest2 >= 7500 && $totalmember >= 5 && $totalinvest >= 60000)
                    $rank = 'IB';
             
                if($rank <> ''){
                    $current_rank = DB::table('Member_Sales')->where('member_id', '=', $myid)->first();
             
                    if(empty($current_rank)|| is_null($current_rank->rank) || ($current_rank->rank == 'IB' && $rank <> 'IB') || ($current_rank->rank == 'MIB' && $rank == 'PIB')){
                        DB::table('Member_Sales')
                        ->where('member_id', $myid)
                        ->update(['rank' => $rank,'conditions' => 2]);
                    }
                }
            }
        }
        echo "2";
        sleep(1);
        $this->runBonusNotification3();
    }

    public function runBonusNotification3 () {
        //Condition 3 - 个人投资 >= 15000 + 大客
        $members = DB::table('Member_Account_Fund')->select('member_id')->where('balanceB', '<>',0)->groupBy('member_id')->havingRaw('SUM(balanceB) * 3 >= 15000')->get();
    
        $no = 0;
        
        foreach($members as $member){
            $no++;
            
            $myid = $member->member_id;
            
            $rank = '';
            $current_rank = '';
            
            $dmembers = DB::table('Member')->where('direct_id', '=', $myid)->get();
            
            foreach($dmembers as $dmember){
                $dmyid = $dmember->id;
                
                $totalinvest1 = DB::table('Member_Account_Fund')->where('member_id', '=', $dmyid)->sum('balanceb');
                
                $totalinvest = $totalinvest1*3;
        
                // if($totalinvest >= 50000)
                //     $rank = 'IB';
        
                if($totalinvest >= 400000) $rank = 'PIB';
                else if($totalinvest >= 200000) $rank = 'MIB';
                else if($totalinvest >= 50000) $rank = 'IB';
                
                if($rank <> ''){
                    $network_data = DB::table('Member_Network')->where('parent_username', 'account2@trfxbiz.com')->where('member_id', '=',$myid)->first();
                    
                    if(!empty($network_data)) $rank = 'IB';
                    
                    $current_rank = DB::table('Member_Sales')->where('member_id', '=', $myid)->first();
                    
                    // if($current_rank->oldconditions == 3 || $current_rank->oldconditions == 6 || $current_rank->oldconditions == 7){
                    if(isset($current_rank->oldconditions2)){
                        if(in_array(3, json_decode($current_rank->oldconditions2))){
                            if(empty($current_rank)|| is_null($current_rank->rank) || ($current_rank->rank == 'IB' && $rank <> 'IB') || ($current_rank->rank == 'MIB' && $rank == 'PIB')){
                                DB::table('Member_Sales')
                                ->where('member_id', $myid)
                                ->update(['rank' => $rank,'conditions' => 3]);
                            }
                        }
                    }
                }
            }
        }
        echo "3";
        sleep(1);
        $this->runBonusNotification4();
    }

    public function runBonusNotification4 () {
        //Condition 4 - 个人投资 >= 1500
        $members = DB::table('Member_Account_Fund')->select('member_id')->where('balanceB', '<>',0)->groupBy('member_id')->havingRaw('SUM(balanceB) * 3 >= 1500')->get();
     
        $no = 0;
     
        foreach($members as $member){
            $no++;
         
            $myid = $member->member_id;
         
            $rank = '';
            $current_rank = '';
         
            $totalinvest1 = DB::table('Member_Account_Fund')->join('Member', 'Member_Account_Fund.member_id', '=', 'Member.id')->select('member_id')->where('direct_id', '=', $myid)->groupBy('member_id')->havingRaw('SUM(balanceB) * 3 >= 1500')->get();
         
            if(count($totalinvest1) >= 15) $rank = 'IB';
         
            if($rank <> ''){
                $current_rank = DB::table('Member_Sales')->where('member_id', '=', $myid)->first();
         
                // if($current_rank->oldconditions == 4 || $current_rank->oldconditions == 6 || $current_rank->oldconditions == 7){
                if(isset($current_rank->oldconditions2)){
                    if(in_array(4, json_decode($current_rank->oldconditions2))){
                        if(empty($current_rank)|| is_null($current_rank->rank)){
                            DB::table('Member_Sales')
                            ->where('member_id', $myid)
                            ->update(['rank' => $rank,'conditions' => 4]);
                        }
                    }
                }
            }
        }
        echo "4";
        sleep(1);
        $this->runBonusNotification5();
    }

    public function runBonusNotification5 () {
        //Condition 5 - 个人投资 >= 4500
        $members = DB::table('Member_Account_Fund')->select('member_id')->where('balanceB', '<>',0)->groupBy('member_id')->havingRaw('SUM(balanceB) * 3 >= 4500')->get();
     
        $no = 0;
     
        foreach($members as $member){
            $no++;
         
            $myid = $member->member_id;
         
            //$totalmember = DB::table('Member')->where('direct_id', '=', $myid)->count();
        
            $totalmember2 = DB::table('Member_Account_Fund')->join('Member', 'Member_Account_Fund.member_id', '=', 'Member.id')->select('member_id')->where('direct_id', '=', $myid)->groupBy('member_id')->havingRaw('SUM(balanceB) * 3 > 0')->get();
        
            $totalmember = count($totalmember2);
         
            $rank = '';
            $current_rank = '';
         
            if($totalmember >= 5){
                $totalinvest1 = DB::table('Member')->join('Member_Account_Fund', 'Member.id', '=', 'Member_Account_Fund.member_id')->where('direct_id', '=', $myid)->sum('balanceb');
         
                $totalinvest = $totalinvest1*3;
         
                if($totalinvest >= 30000) $rank = 'IB';
         
                if($rank <> ''){
                    $current_rank = DB::table('Member_Sales')->where('member_id', '=', $myid)->first();
         
                    // if($current_rank->oldconditions == 5 || $current_rank->oldconditions == 6 || $current_rank->oldconditions == 7){
                    if(isset($current_rank->oldconditions2)){
                        if(in_array(5, json_decode($current_rank->oldconditions2))){
                            if(empty($current_rank)|| is_null($current_rank->rank)){
                                DB::table('Member_Sales')
                                ->where('member_id', $myid)
                                ->update(['rank' => $rank,'conditions' => 5]);
                            }
                        }
                    }
                }
            }
        }
        echo "5";
        sleep(1);
        $this->runBonusNotification6();
    }

    public function runBonusNotification6 () {
        //Condition 6 - 5 IB 升级 MIB
        $members = DB::table('Member_Sales')->where('rank', '=', 'IB')->get();
    
        $no = 0;
        
        foreach($members as $member){
            $no++;
            
            $myid = $member->member_id;
            
            $totalmemberx2 = DB::table('Member_Account_Fund')->join('Member', 'Member_Account_Fund.member_id', '=', 'Member.id')->select('member_id')->where('direct_id', '=', $myid)->groupBy('member_id')->havingRaw('SUM(balanceB) * 3 > 0')->get();
            
            $totalmemberx = count($totalmemberx2);
            
            $totalinvest = DB::table('Member_Account_Fund')->where('member_id', '=', $myid)->sum('balanceB');
            
            $totalinvest2 = $totalinvest * 3;
            
            if($totalinvest2 >= 15000){
                // if($totalmemberx >= 15){
                //     $totalmember2 = DB::table('Member_Sales')->join('Member', 'Member_Sales.member_id', '=', 'Member.id')->select('member_id')->where('direct_id', '=', $myid)->whereIn('rank', ['MIB','PIB'])->get();
            
                //     $totalmember = count($totalmember2);

                //     if($totalmember >= 6){
                //         DB::table('Member_Sales')
                //         ->where('member_id', $myid)
                //         ->update(['rank' => 'PIB','conditions' => 6]);
                //     }
                // }
                // else if($totalmemberx >= 10){
                //     $totalmember2 = DB::table('Member_Sales')->join('Member', 'Member_Sales.member_id', '=', 'Member.id')->select('member_id')->where('direct_id', '=', $myid)->whereIn('rank', ['IB','MIB','PIB'])->get();
        
                //     $totalmember = count($totalmember2);
        
                //     if($totalmember >= 5){
                //         DB::table('Member_Sales')
                //         ->where('member_id', $myid)
                //         ->update(['rank' => 'MIB','conditions' => 6]);
                //     }
                // }

                $totalmember2 = DB::table('Member_Sales')->join('Member', 'Member_Sales.member_id', '=', 'Member.id')->select('member_id')->where('direct_id', '=', $myid)->whereIn('rank', ['MIB','PIB'])->get();
            
                $totalmember = count($totalmember2);

                if($totalmember >= 6){
                    DB::table('Member_Sales')
                    ->where('member_id', $myid)
                    ->update(['rank' => 'PIB','conditions' => 6]);
                }else{
                    $totalmember2 = DB::table('Member_Sales')->join('Member', 'Member_Sales.member_id', '=', 'Member.id')->select('member_id')->where('direct_id', '=', $myid)->whereIn('rank', ['IB','MIB','PIB'])->get();
        
                    $totalmember = count($totalmember2);
        
                    if($totalmember >= 5){
                        DB::table('Member_Sales')
                        ->where('member_id', $myid)
                        ->update(['rank' => 'MIB','conditions' => 6]);
                    }
                }
            }
        }
        echo "6";
        sleep(1);
        $this->runBonusNotification7();
    }

    public function runBonusNotification7 () {
        //Condition 7 - 6 MIB 升级 PIB
        $members = DB::table('Member_Sales')->where('rank', '=', 'MIB')->get();
    
        $no = 0;
        
        foreach($members as $member){
            $no++;
            
            $myid = $member->member_id;

            $totalmemberx2 = DB::table('Member_Account_Fund')->join('Member', 'Member_Account_Fund.member_id', '=', 'Member.id')->select('member_id')->where('direct_id', '=', $myid)->groupBy('member_id')->havingRaw('SUM(balanceB) * 3 > 0')->get();
            
            $totalmemberx = count($totalmemberx2);
            
            $totalinvest = DB::table('Member_Account_Fund')->where('member_id', '=', $myid)->sum('balanceB');
            
            $totalinvest2 = $totalinvest * 3;
            
            if($totalinvest2 >= 15000){
                // if($totalmemberx >= 15){
                    $totalmember2 = DB::table('Member_Sales')->join('Member', 'Member_Sales.member_id', '=', 'Member.id')->select('member_id')->where('direct_id', '=', $myid)->whereIn('rank', ['MIB','PIB'])->get();
                 
                    $totalmember = count($totalmember2);
                 
                    if($totalmember >= 6){
                        DB::table('Member_Sales')
                        ->where('member_id', $myid)
                        ->update(['rank' => 'PIB','conditions' => 7]);
                    }
                // }
            }
        }
        echo "7";
        sleep(1);
        $this->runBonusSendEmail();
    }

    public function runBonusSendEmail () {

        // SELECT count(member_id), rank FROM trealcap_crm.tb_Member_Sales where rank is not null group by rank;
        $IBCount = DB::table('Member_Sales')->where('rank', 'IB')->count();
        $PIBCount = DB::table('Member_Sales')->where('rank', 'PIB')->count();
        $MIBCount = DB::table('Member_Sales')->where('rank', 'MIB')->count();

        $mailTo[] = 'jimmy.pua@trealtechnology.com';
        $mailTo[] = 'siangteck.lee@trealtechnology.com';
        $mailTo[] = 'mingaik.tan@trealtechnology.com';

        Mail::send('front.emails.notification', [], function ($message) use($mailTo, $IBCount, $MIBCount, $PIBCount)
        {
            foreach ($mailTo as $email) {
                $message->to($email);
            }
            $message->subject('Run Bonus Notification (IB: '.$IBCount.'; MIB: '.$MIBCount.'; PIB: '.$PIBCount.')');
        });
        echo "End";

    }

    public function updateInvestorPasswordNotification () {
        $memberRepo = new MemberRepository;

        $no = 0;
        $counter = 0;
        $done = 0;
        $error = 0;
        $errorMsg = 0;
        $errorAry = array();
        $path = app_path().'/changePassErrorLog.txt';

        $accounts = DB::table('Member_Account_Fund')->where('chgpass', '=', 'Y')->get();
    
        // $accounts = DB::table('mam_users')->where('chgpass', '=', 'Y')->where('partnercode', '=', 'baeb5956a7184bdaa37bcc5ca270e5ee')->take(100)->get();

        // $accounts = DB::table('Member_Account_Fund')->where('moveout', '=', 'Y')->where('status', '=', 0)->take(300)->get();

        // $accounts = DB::table('Member_Account_Fund')->where('status', '=', 0)->where('fundcompany', '=', 227)->where('rbalance', '>=', 10000)->where('system_manage', '<>', 'N')->where('chgpass', '=', 'Y')->take(100)->get();

        // $accounts = DB::table('mam_users')->where('chgpass', '=', 'N')->take(50)->get();

        // $fundcompany = 268;

        // $accounts = DB::table('Member_Account_Fund')->where('newfundstatus', '=', 'Y')->where('status', '=', 0)->where('chggroup', '=', 'Y')->take(50)->get();

        // $accounts = DB::table('Member_Account_Fund')->where('system_manage', '<>', 'N')->where('chgpass2', '=', 'Y')->take(200)->get();

        foreach($accounts as $account){
            // $member_id = $account->member_id;
            $bookA = $account->bookA;
            // $bookA = $account->MAMAccount;
            $newpassword = 'XSw21qaz999';
            $investnewpassword = 'XSw21qaz1228';
            
           /*
             $fundcomp = DB::connection('mysql3')->table('user')->where('UserId', '=', $fundcompany)->first();
             $fundgroup = $fundcomp->MT5Group;
            */
            
            $no++;
            $counter++;
            if($counter == 100){
                sleep(5);
                $counter = 0;
            }
            
            // $movein = $memberRepo->AccountMoveGroupAPI($bookA,'live\\H\\'.$fundgroup.'\\Manage');
            
            // $updatepass = $memberRepo->UpdateMasterPasswordAPI($bookA,$newpassword);
            
            DB::table('Member_Account_Fund')->where('bookA', $bookA)->update(['chgpass' => 'N']);
            $updatepass2 = $memberRepo->UpdateInvestorPasswordAPI($bookA,$investnewpassword);

            // $moveoutAPI = $memberRepo->AccountMoveoutAPI($bookA);
          
            if($updatepass2->status == 0) $done++;
            else{
                $error++;
                $errorAry[] = $bookA;
            }
            /*
           
            if($updatepass->status == 0)
            {
                DB::table('mam_users')->where('MAMAccount', $bookA)
                ->update([
                         'chgpass' => 'N',
                         ]);
            }
            */
        }

        if(count($errorAry) > 1){
            $errorMsg = 1;
            File::put($path, json_encode($errorAry));
        }

        $mailTo[] = 'jimmy.pua@trealtechnology.com';
        $mailTo[] = 'siangteck.lee@trealtechnology.com';
        $mailTo[] = 'mingaik.tan@trealtechnology.com';

        Mail::send('front.emails.notification', [], function ($message) use($mailTo, $path, $no, $done, $error, $errorMsg)
        {
            foreach ($mailTo as $email) {
                $message->to($email);
            }
            $message->subject('Update Investor Password Notification (Total: '.$no.'; Completed: '.$done.'; Failed: '.$error.')');
            if($errorMsg) $message->attach($path);
        });
    }

    public function updateDailyWeather () {
        //初始化请求链接
        $req=curl_init();
        //设置请求链接
        curl_setopt($req, CURLOPT_URL,'https://free-api.heweather.com/s6/weather/forecast?location=CN101010100&key=1385c806f8de49b896f3671e2010e50f');
        //设置超时时长(秒)
        curl_setopt($req, CURLOPT_TIMEOUT,3);
        //设置链接时长
        curl_setopt($req, CURLOPT_CONNECTTIMEOUT,10);
        //设置头信息
        $headers=array( "Accept: application/json", "Content-Type: application/json;charset=utf-8" );
        curl_setopt($req, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($req, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($req, CURLOPT_SSL_VERIFYHOST, false);
        $data = curl_exec($req);
        curl_close($req);

        $weather = json_decode($data);
        $temp = [];
        $i = 0;
        foreach ($weather->HeWeather6[0]->daily_forecast as $item) {
            unset($temp);
            $temp['date'] = $item->date;
            $temp['weatherCode'] = $item->cond_code_d;
            $temp['highestTemp'] = $item->tmp_max;
            $temp['lowestTemp'] = $item->tmp_min;
            $temp['humidity'] = $item->hum;

            DB::table('Setting')->where('name', 'Day'.$i)->where('type', 'Weather')->update(['value' => json_encode($temp)]);
            $i++;
        }
    }

    public function dailyInvestmentRecord () {
        $investment = DB::table('Member_Account_Fund')->where('balanceB', '<>', 0)->where('system_manage', '!=', 'N')->get();
        foreach ($investment as $item) {
            DB::table('Daily_Investment')->insert(['member_id' => $item->member_id, 'bookA' => $item->bookA, 'amount' => $item->balanceB*3, 'date'=> date('Y-m-d', strtotime(date('Y-m-d'). "-1 day"))]);
        }
    }

    public function monthEndBackupRanking ($month) {
        $db = DB::connection();

        $testname = 'tb_Member_Account_Fund_'.$month;

        $sql = "CREATE TABLE IF NOT EXISTS ".$testname." (id INT(10), member_id INT(10), bookA INT(10), bookB INT(10), system_manage CHAR(2), status INT(2), lockdate DATETIME, bwallet DECIMAL(16,2), winA INT(2), winB INT(2), created_at TIMESTAMP, updated_at TIMESTAMP, updated_by VARCHAR(200), balanceA DECIMAL(16,2), balanceB DECIMAL(16,2), fundcompany INT(2), oldfundcompany INT(2), oldfunddate TIMESTAMP, newfundcompany INT(2), newfunddate TIMESTAMP, newfundstatus CHAR(1), newfundrequest CHAR(2), subscribe_date TIMESTAMP, unsubscribe_date TIMESTAMP, partner_code VARCHAR(32), manager_code VARCHAR(32), trader_code VARCHAR(32), partner_status TINYINT(2), mam_account BIGINT(20), account_type TINYINT(2), chgpass CHAR(2), chgpass2 CHAR(2), chggroup CHAR(2), changefund CHAR(2), moveout CHAR(2), rbalance DECIMAL(16,2), ini_amount DECIMAL(16,2), request_unsubscribe_date TIMESTAMP, PRIMARY KEY (id))";
        $db->statement($sql);

        $sql = "INSERT IGNORE INTO ".$testname." SELECT * FROM tb_Member_Account_Fund";
        $db->statement($sql);

        $this->runBonusNotification();
    }

    public function monthEndBackupSales ($month) {
        $db = DB::connection();

        $testname = 'tb_Member_Sales_'.$month;

        $sql = "CREATE TABLE IF NOT EXISTS ".$testname." (member_id INT(20), username VARCHAR(200), totaltransfer DECIMAL(16,2), totallotsize DECIMAL(16,2), totallotsize9 DECIMAL(16,2), totaldeposit DECIMAL(16,2), balanceb DECIMAL(16,2), rank CHAR(5), conditions INT(11), created_at DATETIME, updated_at DATETIME, status CHAR(2), adjustment CHAR(2), oldconditions INT(11), oldconditions2 TEXT, PRIMARY KEY (member_id))";
        $db->statement($sql);

        $sql = "INSERT IGNORE INTO ".$testname." SELECT * FROM tb_Member_Sales";
        $db->statement($sql);

        $this->updateMemberSalesDetails($month);
    }

    public function updateMemberSalesDetails ($month) {
        DB::table('Member_Sales_'.$month)->update(['totaltransfer' => '0.00', 'totallotsize' => '0.00', 'totaldeposit' => '0.00', 'status' => 'N']);
        
        $members = DB::table('Member_Sales_'.$month)->join('Member', 'Member_Sales_'.$month.'.member_id', '=', 'Member.id')->where('status', '=','N')->take(1000)->get();

        foreach($members as $member){
            $myid = $member->id;
            $mylevel = $member->level;
            $myusername = $member->username;
            $upline = $member->register_by;
            $upline_id = $member->direct_id;
            $totallotsize = 0;
            
            $totaltransferx1 = DB::table('Member_Blacklist')->where('member_id', '=', $myid)->where('blacklist_at', '>', '2018-10-01 05:00:00')->where('blacklist_at', '<=', '2018-11-01 05:00:00')->where('b_book', '<>', 0)->sum('b_book');
            
            $totaltransferx2 = \DB::table('Member_Wallet_Statement_Fund')->where('member_id', '=',$myid)->where('action_type', '=', 'Transfer to MT5')->where('created_at', '>', '2018-10-01 05:00:00')->where('created_at', '<=', '2018-11-01 05:00:00')->sum('b_amount');
    
            $transferamountx1 = ($totaltransferx2 * 2) - $totaltransferx1;
            
            $accounts = DB::table('Member_Account_Fund_'.$month)->where('member_id', '=',$myid)->get();
            
            foreach($accounts as $account){
                $bookA = $account->bookA;
                
                $volume = DB::connection('mysql2')->table('historyposition')->where('Login', '=', $bookA)->where('CloseTime', '>=', '2018-10-01')->where('CloseTime', '<', '2018-11-01')->sum('Volume');
                
                $lotsize1 = $volume / 10000;
                
                //$lotsize2 = ($volume2 / 10000) / 2;
                
                $totallotsize = $totallotsize + $lotsize1; // - $lotsize2;
            }
 
            $totaldepositx = \DB::table('Member_Account_Fund_'.$month)->where('member_id', '=',$myid)->sum('balanceB');
            
            $depositamountx1 = $totaldepositx * 3;
    
            \DB::table('Member_Sales_'.$month)
                    ->where('member_id', $myid)
                    ->update([
                             'totaltransfer'    =>  $transferamountx1,
                             'totaldeposit'    =>  $depositamountx1,
                             'totallotsize'    =>  $totallotsize,
                             'status'    =>  'Y',
                             'created_at' => \Carbon\Carbon::now(),
                             'updated_at' => \Carbon\Carbon::now()
                             ]);
        }

        // $this->updateMemberSalesSummary($month);
    }

    public function updateMemberSalesSummary ($month) {
        $insertDate = date('Y').'-'.date('m', strtotime($month)).'-01';
        $sales = DB::table('member_sales_'.$month)->get();

        foreach ($sales as $item) {
            $rank = $item->rank ? $item->rank : '';
            
            $totalinvest = DB::table('Member_Account_Fund_'.$month)->where('member_id', $item->member_id)->sum('balanceB');
            
            DB::table('Member_Sales_Summary')->insert([
                'member_id' => $item->member_id,
                'rank' => $rank,
                'conditions' => $item->conditions,
                'totaldeposit' => $item->totaldeposit,
                'totaltransfer' => $item->totaltransfer,
                'totallotsize' => $item->totallotsize,
                'totallotsize9' => $item->totallotsize9,
                'totalinvest' => $totalinvest*3,
                'date' => $insertDate
            ]);
        }
    }
}
