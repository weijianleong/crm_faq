<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositories\AdminRightRepository;
use Carbon\Carbon;
use DB;

class RightController extends Controller
{
    /**
     * The AnnouncementRepository instance.
     *
     * @var \App\Repositories\AnnouncementRepository
     */
    protected $AdminRightRepository;
    /**
     * Create a new AnnouncementController instance.
     *
     * @param \App\Repositories\AnnouncementRepository $AnnouncementRepository
     * @return void
     */
    public function __construct(AdminRightRepository $AdminRightRepository) {
        parent::__construct();
        $this->AdminRightRepository = $AdminRightRepository;
        $this->middleware('admin');
    }

    /**
     * Datatable List
     * @return [type] [description]
     */
    public function getUser () {
        return $this->AdminRightRepository->getUser();
    }

    public function getShowModal ($id) { //weilun
        if (!$model = $this->HelpdeskRepository->findLog(trim($id))) {
            return 'History not found.';
        }
        return view('back.helpdesk.helpdesk_showHistory')->with('model', $model);
    }

    public function getEdit ($id) {

        if (!$model = $this->AdminRightRepository->findById($id)) {

            $admin = \Sentinel::getUser();
            $data["right"] ='{"home":true,"profile":true}';
            $data["user_id"] =$id;
            $data["level"] ="admin";
            $data["created_by"] =$admin ->email;

            if(!$model = $this->AdminRightRepository->autoInsert($id,$data)){
                 return redirect()->back()->with('flashMessage', [
                     'class' => 'danger',
                     'message' => 'Data not found.'
                 ]);
            }
            $model = $this->AdminRightRepository->findById($id);
        }

        $get_right = DB::table('admin_sbar')->groupBy('right_name')->orderBy('id')->get();
        $right = json_decode($model->right, true);

        $type_Id = explode(",",$model->cs_type);

        for ($i=0; $i < 5; $i++) { 
            foreach ($type_Id as $value) {
                if($value == $i){
                    $temp[$value] = $value;
                    break;
                }
                else $temp[$i] = null;
                # code...
            }
        }

        return view('back.right.edit')->with('model', $model)->with('right',$get_right)->with('user_right',$right)->with('cs_type',$temp);
    }

    public function postUpdate ($id) {

        $data = \Input::get('data');
        $admin = \Sentinel::getUser();
        $get_right = DB::table('admin_sbar')->groupBy('right_name')->orderby('ID')->get();

        foreach($get_right as $rights){
            if(isset($data["right"][$rights->id])){
                $temp[$rights->right_name] = true;

            }



        }

        $data["helpdesk_right"] = "";

        if(isset($data["helpdesk1"])){

            foreach($data["helpdesk1"] as $key => $value){
            
                if(isset($data["helpdesk1"][$key])){

                    if($data["helpdesk_right"] == ""){

                        $data["helpdesk_right"] .= $key;

                    }else{

                        $data["helpdesk_right"] .= ",".$key;

                    }
                }
            }

        }else{

            $data["helpdesk_right"] = 99;
        }


        
        $data["right"] = json_encode($temp);
        $data["updated_by"] = $admin->email;



        if (!$model = $this->AdminRightRepository->updateRight($id,$data)) {
            return redirect()->back()->with('flashMessage', [
                'class' => 'danger',
                'message' => 'Data Error.'
            ]);
        }

        return \Response::json([
            'type' => 'success',
            'message' => 'User Right is updated.'
        ]);
        
    }


}
