<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositories\MemberRepository;
use App\Repositories\PackageRepository;
use App\Repositories\SharesRepository;
use App\Repositories\BonusRepository;
use App\Repositories\AdminLogRepository;
use Yajra\Datatables\Facades\Datatables;
use App\Services\AmazonS3;

class MemberController extends Controller
{
    /**
     * The MemberRepository instance.
     *
     * @var \App\Repositories\MemberRepository
     */
    protected $MemberRepository;

    /**
     * The PackageRepository instance.
     *
     * @var \App\Repositories\PackageRepository
     */
    protected $PackageRepository;

    /**
     * The SharesRepository instance.
     *
     * @var \App\Repositories\SharesRepository
     */
    protected $SharesRepository;

    /**
     * The PackageRepository instance.
     *
     * @var \App\Repositories\PackageRepository
     */
    protected $BonusRepository;

    protected $AmazonS3;

    /**
     * Create a new MemberController instance.
     *
     * @param \App\Repositories\MemberRepository $MemberRepository
     * @return void
     */
    public function __construct(
        MemberRepository $MemberRepository,
        PackageRepository $PackageRepository,
        SharesRepository $SharesRepository,
        BonusRepository $BonusRepository,
        AdminLogRepository $AdminLogRepository,
        AmazonS3 $AmazonS3
    ) {
        $this->middleware('admin');
        $this->MemberRepository = $MemberRepository;
        $this->PackageRepository = $PackageRepository;
        $this->SharesRepository = $SharesRepository;
        $this->BonusRepository = $BonusRepository;
        $this->AdminLogRepository = $AdminLogRepository;
        $this->AmazonS3 = $AmazonS3;
    }

    /**
     * Datatable member admin
     * @return object
     */
    public function getLpoa () {
        return $this->MemberRepository->getLpoa();
    }


    public function getList () {
        return $this->MemberRepository->findAll(true);
    }
    
    public function getList2 () {
        return $this->MemberRepository->findPending(true);
    }

    /**
     * Datatable member wallet admin
     * @return object
     */
    public function getWalletList () {
        return $this->MemberRepository->findWalletList(true);
    }

    /**
     * Datatable member wallet statement
     * @return [type] [description]
     */
    public function getWalletStatementList ($id) {
        $query = \DB::table('Member_Wallet_Statement')
            ->where('member_id', trim($id));
        return Datatables::queryBuilder($query)
                ->editColumn('register_amount', function ($model) {
                    return number_format($model->register_amount, 2);
                })
                ->editColumn('promotion_amount', function ($model) {
                    return number_format($model->promotion_amount, 2);
                })
                ->make(true);
    }

    /**
     * Register member (ROOT)
     * @return json
     */
    public function register ($type) {
        $data = \Input::get('data');

        if (!$package = $this->PackageRepository->findById($data['package_id'])) {
            return \Response::json([
                'type' => 'error',
                'message' => 'Package not found.'
            ]);
        }

        if (($type != 'root' && $type != 'common') || !isset($type)) {
            return \Response::json([
                'type' => 'error',
                'message' => 'Type not found.'
            ]);
        }
        try {
            $user = \Sentinel::registerAndActivate([
                'email'   => $data['email'],
                'username'  =>  $data['username'],
                'password'  =>  $data['password'],
                'permissions' =>  [
                    'member' => true,
                ]
            ]);

            if ($type == 'root') { // top member
                $member = $this->MemberRepository->saveModel(new \App\Models\Member, [
                    'username'  =>  $user->username,
                    'register_by' => 'admin',
                    'secret_password' =>  $data['secret_password'],
                    'user_id'   =>  $user->id,
                    'package_id'    =>  $package->id,
                    'direct_id' =>  0,
                    'parent_id' =>  0,
                    'root_id'   =>  0,
                    'level'     =>  1,
                    'direct_percent'    =>  $package->direct_percent,
                    'pairing_percent'   =>  $package->pairing_percent,
                    'group_level'   =>  $package->group_level,
                    'max_pair'  =>  $package->max_pair,
                    'max_pairing_bonus' =>  $package->max_pairing_bonus,
                    'original_amount'  =>  $package->package_amount,
                    'package_amount'    =>  $package->package_amount,
                    'position'  =>  'top'
                ]);
                $this->MemberRepository->saveModel(new \App\Models\MemberWallet, [
                    'member_id' =>  $member->id,
                    'register_point'    =>  0,
                    'purchase_point'    =>  $package->purchase_point,
                    'promotion_point'   =>  0,
                    'cash_point'    =>  0,
                    'md_point'  =>  0
                ]);
            } else { // lower member
                if (!$direct = $this->MemberRepository->findByUsername($data['direct_id'])) {
                    throw new \Exception('Member upline not found.', 1);
                    return false;
                }

                if (!$parent = $this->MemberRepository->findByUsername($data['parent_id'])) {
                    throw new \Exception('Member binary not found.', 1);
                    return false;
                }

                if (!$this->MemberRepository->checkIfPositionAvailable($parent, $data['position'])) {
                    throw new \Exception('Position no longer available.', 1);
                    return false;
                }

                $member = $this->MemberRepository->saveModel(new \App\Models\Member, [
                    'username'  =>  $user->username,
                    'register_by' => 'admin',
                    'secret_password' =>  $data['secret_password'],
                    'user_id'   =>  $user->id,
                    'package_id'    =>  $package->id,
                    'direct_id' =>  $direct->id,
                    'parent_id' =>  $parent->id,
                    'root_id'   =>  ($parent->position == $data['position']) ? $parent->root_id : $parent->id,
                    'level'     =>  $parent->level + 1,
                    'direct_percent'    =>  $package->direct_percent,
                    'pairing_percent'   =>  $package->pairing_percent,
                    'group_level'   =>  $package->group_level,
                    'max_pair'  =>  $package->max_pair,
                    'max_pairing_bonus' =>  $package->max_pairing_bonus,
                    'original_amount'  =>  $package->package_amount,
                    'package_amount' =>  $package->package_amount,
                    'position'  =>  $data['position']
                ]);

                $this->MemberRepository->saveModel(new \App\Models\MemberWallet, [
                    'member_id' =>  $member->id,
                    'register_point'    =>  0,
                    'purchase_point'    =>  $package->purchase_point,
                    'promotion_point'   =>  0,
                    'cash_point'    =>  0,
                    'md_point'  =>  0
                ]);

                if (env('APP_ENV') == 'local') { // local
                    $wallet = $member->wallet;
                    $this->MemberRepository->addNetwork($member);
                    $this->SharesRepository->repurchasePackage($member, $wallet->purchase_point, $wallet);
                } else { // production
                    dispatch(new SharesAfterRegisterJob($member))->onQueue('queue-shares-register');
                    dispatch(new NetworkAfterRegisterJob($member))->onQueue('queue-network-register');
                }

                // remove cache for network
                \Cache::forget('member.' . $parent->id . '.children');

                $this->BonusRepository->calculateDirect($member, $direct);
                $this->BonusRepository->calculateOverride($member);
            }

            $this->MemberRepository->saveModel(new \App\Models\MemberDetail, [
                'member_id' =>  $member->id
            ]);

            $this->MemberRepository->saveModel(new \App\Models\MemberShares, [
                'member_id' =>  $member->id,
                'amount'    =>  0,
                'share_limit'   =>  $package->share_limit,
                'max_share_sale'    =>  $package->max_share_sale
            ]);
        } catch (\Exception $e) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  $e->getMessage()
            ]);
        }
        return \Response::json([
            'type'  =>  'success',
            'message'   =>  'Register successful.'
        ]);
    }
    /**
     * Update member data
     * @return json
     */
    public function postUpdate ($id) {
        $data = \Input::get('data');
        $log["new_data"] = json_encode($data);
        $member = $this->MemberRepository->findById(trim($id));
        $user = $member->user;
        
        $book = \DB::table('Member_Account')->where('member_id', '=', $id)->first();
        
        
        foreach (Session('old_data_detail') as $key => $value) {
            $old_data_1  = json_encode($value);
            # code...
        }
        foreach (Session('old_data_muser') as $key => $value) {
            $old_data_2  = json_encode($value);
            # code...
        }
        $log["old_data"] = "detail:" .$old_data_1."muser:".$old_data_2;

        $username = $data['email'];
        $email = $data['email'];
        //$username = 'leest2000@gmail.com';
        //$email = 'leest2000@gmail.com';
        $first_name = $data['first_name'];
        $ID = str_replace(' ', '', $data['ID']);
        $phone1 = $data['phone1'];
        $password = $user->password2;
        $bookA =$book->bookA;
        $bookB =$book->bookB;

        if ($checkIC = $this->MemberRepository->findIC(str_replace(' ', '', $ID))) {
            if($checkIC["member_id"] != $id){
                 return \Response::json([
                    'type'  =>  'error',
                    'message'   =>  \Lang::get('error.memberICExist')
                ]);

            }
               
        }

        
        //echo $bookA;
        if(isset($data['process_status']))
        {
            $status = 'Y';
            if($member->process_status == 'N')
            {
                $groupnameA = config('misc.newbook.newbookA.groupname');
                $leverA = config('misc.newbook.newbookA.lever');
        
               // $addBookA = $this->MemberRepository->NewUserAPI($first_name, $username, $groupnameA, $leverA, $bookA, $password);
        
                $groupnameB = config('misc.newbook.newbookB.groupname');
                $leverB = config('misc.newbook.newbookB.lever');
        
               // $addBookB = $this->MemberRepository->NewUserAPI($first_name, $username, $groupnameB, $leverB, $bookB, $password);
            
                $sendRegister = $this->sendRegister($username, $email, $first_name,$password,$bookA, $bookB);
                
                \DB::table('Member')->where('id', $id)->update([
                                                               
                   'process_status'    =>  $status
               ]);
               /*
                 \DB::table('Member_Sales')->insert(
                     [
                        'member_id' =>  $myid,
                        'username'    =>  $myusername,
                         'created_at' => \Carbon\Carbon::now(),
                         'updated_at' => \Carbon\Carbon::now()
                     ]
                     );
                
                $mlevel = 0;
                    while($mylevel > 2)
                    {
                        $umember = DB::table('Member')->where('id', '=',$upline_id)->first();
                        
                        $upline = $umember->register_by;
                        $upline_id = $umember->direct_id;
                        $upid = $umember->id;
                        $mylevel = $umember->level;
                        $upusername = $umember->username;
                        
                        $mlevel++;
                        
                        echo $myusername;
                        echo "<BR>";
                        echo $mlevel;
                        echo "<BR>";
                        
                        \DB::table('Member_Network')->insert(
                         [
                            'member_id' =>  $myid,
                            'username'    =>  $myusername,
                            'parent_id'    =>  $upid,
                            'parent_username'   =>  $upusername,
                            'parent_level'   =>  $mylevel,
                            'my_level'   =>  $mlevel,
                             'created_at' => \Carbon\Carbon::now(),
                             'updated_at' => \Carbon\Carbon::now()
                         ]
                         );
                    }
            */
            }
        }
        else
        {
            $status = 'N';
            if ($checkEmail = $this->MemberRepository->findByUsername(str_replace(' ', '', $username))) {
                if($checkEmail["id"] != $id){
                     return \Response::json([
                        'type'  =>  'error',
                        'message'   =>  \Lang::get('error.memberExist')
                    ]);

                }
                   
            }

            \DB::table('users')->where('username', $user->username)->update([
                'email'    =>  $email,
                'username'    =>  $username,
            ]); 

            \DB::table('Member')->where('id', $id)->update([
                'username'    =>  $username,
            ]); 
        }


        \DB::table('Member_Detail')->where('member_id', $id)->update([
                                                       
             'phone1'    =>  $phone1,
             'identification_number'    =>  $ID
       ]);
        
        \DB::table('users')->where('username', $username)->update([
            'first_name'    =>  $first_name
        
       ]);       

        \Cache::forget('member.' . $user->id);

        $admin = \Sentinel::getUser();
        $log["cs_email"] = $admin->username;
        $log["member_id"] = $id;
        $log["action"] = "Process_status : ".$status;
        $this->AdminLogRepository->store($log);

        return \Response::json([
            'type'  =>  'success',
            'message'   =>  'Member account updated.'
        ]);
    }
    
    public function sendRegister ($username, $email, $first_name,$password,$bookA, $bookB) {
       // $title = 'Hi,'.$first_name;
        
        // \Mail::send('front.emails.register', ['username' => $username,'email' => $email, 'first_name' => $first_name, 'password' => $password, 'bookA' => $bookA, 'bookB' => $bookB], function ($message) use ($username)
        //            {
                   
        //            $message->subject('New Account Registration');
                   
        //            $message->to($username);
                    
        //             $message->bcc('admin@trealcap.com');
                   
        //            });
        
        return true;
    }
    
    public function postUpdateBackup ($id) {
        $data = \Input::get('data');
        $member = $this->MemberRepository->findById(trim($id));
        $user = $member->user;
        
        if (isset($data['is_update_basic'])) {
            $userData = [];
            
            if (isset($data['first_name'])) {
                if ($data['first_name'] != '') {
                    $userData['first_name'] = $data['first_name'];
                }
            }
            if (isset($data['email'])) {
                if ($data['email'] != '') {
                $userData['email'] = $data['email'];
                }
            }
        
            if (isset($data['is_ban'])) {
                $userData['is_ban'] = 1;
            }
            else
            {
                $userData['is_ban'] = 0;
            }
           
            \Sentinel::update($user, $userData);
        
        }
        
        
        $detail = $member->detail;
        //$wallet = $member->wallet;
        //$shares = $member->shares;
        //$original = $member->original_amount;

        foreach ($member->getAttributes() as $k => $value) {
            if ($k == 'secret_password') {
                if (isset($data[$k])){
                    if ($data[$k] != '') {
                        $member->{$k} = $data[$k];
                    }
                }
            }
            else{
               if (isset($data[$k])) $member->{$k} = $data[$k];
            }
        }

        foreach ($detail->getAttributes() as $k => $value) {
            if ($k != 'created_at') {
                if (isset($data[$k])) $detail->{$k} = $data[$k];
            }
        }
        

        $member->save();
        $detail->save();

        \Cache::forget('member.' . $user->id);

        return \Response::json([
            'type'  =>  'success',
            'message'   =>  'Member account updated.'
        ]);
    }
    
    public function getShowModal ($id) { //weilun

        if (!$model = $this->MemberRepository->getLpoa_detail(trim($id))) {
            return 'Detail not found.';
        }
        $url = $this->AmazonS3->loadImage('LP/'.$model->lpoa);
        $model->lpoaFile = $url;
        return view('back.declare.show_detail')->with('model', $model);
    }
    
    public function postUpdateDeclare () {
        
        
        
        $data = \Input::get('data');
        $id = $data["member_id"];

        if (!$model = $this->MemberRepository->findById(trim($id))) {
            return \Response::json([
                                   'type'  =>  'error',
                                   'message'   =>  $id
                                   ]);
        }
        
        if(isset($data['declare_status']))
           {
           $status = 'Y';
           }
           else
           {
           $status = 'N';
           }
        
        \DB::table('Member')->where('id', $id)->update([
                                               
                                                        'lpoa_status'    =>  $status
                                                        ]);

        $admin = \Sentinel::getUser();
        $log["cs_email"] = $admin->username;
        $log["member_id"] = $id;
        $log["action"] = "LPOA : ".$status;
        $this->AdminLogRepository->store($log);


       
        return \Response::json([
                               'type'  =>  'success',
                               'message'   =>  'Status updated.'
                               ]);
    }
    
    public function postUpdateTrade ($id) {
        
        
       
        $data = \Input::get('data');
        
        $user = \Sentinel::getUser();
        
        $updateby = $user->username;
        
        
        \DB::table('Orders')->where('orderid', $id)->update([
            
           'status'    =>  'P',
           'rwallet'    =>  $data['rwallet'],
           'checkstatus'    =>  isset($data['checkstatus']) ? 1 : 0,
           'checkby'    =>  $updateby
       ]);
        
        if($data['rwallet'] > 0)
        {
            \DB::table('Member_Account')->where('bookB', $data['bookB'])->update([
                                                           
               'rwallet'    =>  $data['rwallet'],
                'updated_by'    =>  $updateby
                         
               ]);
        }
        
        return \Response::json([
                               'type'  =>  'success',
                               'message'   =>  'Trade Process updated.'
                               ]);
    }

    /**
     * [updateSalesAmount description]
     * @param  [type] $member [description]
     * @param  [type] $amount [description]
     * @return [type]         [description]
     */
    public function updateSalesAmount ($member, $amountFrom, $amountTo) {
        $befores = '';
        $beforeChildren = '';
        $isSub = false;
        $amount = $amountTo - $amountFrom;
        if ($amount == 0) return false;
        if ($amount < 0) {
            $amount = abs($amount);
            $isSub = true;
        }
        if ($parent = $member->parent()) {
            $alwaysRemove = explode(',', $member->position == 'right' ? $parent->left_children : $parent->right_children);
        }

        while ($root = $member->root()) {
            if ($member->position == 'left') {
                $memberIds = $root->left_children;
                $position = 'left_children';
                $totalField = 'left_total';
            } else {
                $memberIds = $root->right_children;
                $position = 'right_children';
                $totalField = 'right_total';
            }

            $memberIds = rtrim($root->id . ',' . $memberIds, ',');
            $memberIds = explode(',', $memberIds);

            if ($befores != '') {
                $memberIds = array_diff($memberIds, explode(',', $befores));
            }

            if (isset($alwaysRemove)) {
                $memberIds = array_diff($memberIds, $alwaysRemove);
            }

            if ($beforeChildren != '') {
                $memberIds = array_diff($memberIds, explode(',', $beforeChildren));
            }

            $memberIds = implode(',', $memberIds);

            if ($root->position == 'top') {
                \DB::update('UPDATE tb_Member SET ' . $totalField . '=' . $totalField . ($isSub ? ' - ' : ' + ') . $amount . ' WHERE id IN (' . $memberIds . ') AND position="' . $member->position . '" AND level < ' . $member->level . ' AND root_id=' . $member->root_id);
                \DB::update('UPDATE tb_Member SET ' . $totalField . '=' . $totalField . ($isSub ? ' - ' : ' + ') . $amount . ' WHERE id IN (' . $memberIds . ') AND position="top" AND level < ' . $member->level);
            } else {
                \DB::update('UPDATE tb_Member SET ' . $totalField . '=' . $totalField . ($isSub ? ' - ' : ' + ') . $amount . ' WHERE id IN (' . $memberIds . ') AND position="' . $member->position . '" AND level < ' . $member->level . ' AND root_id=' . $member->root_id);
                \DB::update('UPDATE tb_Member SET ' . $totalField . '=' . $totalField . ($isSub ? ' - ' : ' + ') . $amount . ' WHERE id=' . $root->id . ' AND level < ' . $member->level);
            }

            $beforeChildren .= ',' . ($root->position == 'left' ? $root->left_children : $root->right_children);
            $beforeChildren = ltrim($beforeChildren, ',');
            $befores .= ',' . $memberIds;
            $member = $root;
        }

        return true;
    }

    /**
     * Find member info when register
     * @return html
     */
    public function getMemberRegisterModal () {
        $model = false;
        if (trim(\Input::get('u')) != '') {
            $model = $this->MemberRepository->findByUsername(trim(\Input::get('u')));
        }
        return view('front.member.modalRegister')->with('model', $model);
    }

    public function amazonRead ($folder, $filename) {
        $url = $this->AmazonS3->loadImage($folder.'/'.$filename);
        
        return redirect($url);
    }

    public function showMT5 ($id) {
        if (!$model = \DB::table('Member_Account_Fund')->where('member_id',$id)->get()) {
            return 'Helpdesk ID not found.';
        }

        foreach ($model as $models) {
            $cr8_at = \DB::table('Member_Wallet_Statement_Fund')->where('member_id',$id)->where('remark','like', '%' . 'for transfer to MT5 account '.$models->bookB. '%')->where('action_type','Transfer to MT5')->where('wallet_type','BW')->where('created_at','>=','2018-03-12')->value('created_at');
            $models->cr8_at = $cr8_at;
            # code...
        }

        return view('back.member.showMT5')->with('model', $model);
    }

    public function postDeposit () {
        $data = \Input::get('data');

        $member = $this->MemberRepository->findByUsername(trim($data['email']));

        if(count($member) == 0){
            return \Response::json([
                    'type'  =>  'error',
                    'message'   =>  'Invalid Email'
                ]);
        }

        if($data['amount'] <= 0){
            return \Response::json([
                    'type'  =>  'error',
                    'message'   =>  'Invalid Amount'
                ]);
        }

        $passAry = ['trfx#5689', 'ZAq12wsx!'];
        if(!in_array($data['password'], $passAry)){
            return \Response::json([
                    'type'  =>  'error',
                    'message'   =>  'Invalid Password'
                ]);
        }
        
        $balance = \DB::table('Member_Wallet')->where('member_id', '=', $member->id)->first(['cash_point']);

        $new_balance = $balance->cash_point + $data['amount'];

        \DB::table('Member_Wallet')->where('member_id', $member->id)->update(['cash_point' => $new_balance]);

        \DB::table('Member_Wallet_Statement')->insert([
                                                        'member_id'         =>  $member->id,
                                                        'username'          =>  $data['email'],
                                                        'action_type'       =>  'Deposit',
                                                        'wallet_type'       =>  'C',
                                                        'transaction_type'  =>  'C',
                                                        'cash_amount'       =>  $data['amount'],
                                                        'balance'           =>  $new_balance,
                                                        'remark'            =>  'Deposit cash '.$data['amount'].' (transfer from TR)',
                                                        'created_at'        =>  \Carbon\Carbon::now(),
                                                        'updated_at'        =>  \Carbon\Carbon::now(),
                                                        'remark_display'    =>  'TH00048/{"amount":'.$data['amount'].'}'
                                                    ]);

        return \Response::json([
            'type'  =>  'success',
            'message'   =>  'Deposit successful'
        ]);
    }
}