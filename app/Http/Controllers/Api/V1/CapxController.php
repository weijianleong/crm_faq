<?php
namespace App\Http\Controllers\API\V1;

use DB;
use Mail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Repositories\MemberRepository;

class CapxController
{
    public function APIErrorEmailALert ($api) {
        $alertEmail = DB::table('API_Services')->where('alert_email', 1)->where('name', $api)->count();
        if($alertEmail == 0){
            $mailTo[] = 'jimmy.pua@trealtechnology.com';
            // $mailTo[] = 'siangteck.lee@trealtechnology.com';
            // $mailTo[] = 'mingaik.tan@trealtechnology.com';

            // Mail::send('front.emails.notification', [], function ($message) use($mailTo, $api)
            // {
            //     foreach ($mailTo as $email) {
            //         $message->to($email);
            //     }
            //     $message->subject('API Failed: '.$api);
            // });
        }
    }

    public function getLinkedKey (Request $request) {
        $data = $request->all();
        try{
            if(!isset($data['email']) || !isset($data['id'])){
                $dataOut = ['code' => 203, 'message' => "Invalid Parameter", 'result' => array()];
                DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
                return response()->json($dataOut);
            }

            $username = $data['email'];
            $id = $data['id'];

            $user = DB::table('users')->where('username', $username)->where('cs_type', 99)->first();
            if(isset($user)){
                $member = DB::table('Member')->where('username', $username)->first(['id']);

                $memberDetail = DB::table('Member_Detail')->where('member_id', $member->id)->first(['identification_number']);
                if($id <> $memberDetail->identification_number){
                    $dataOut = ['code' => 204, 'message' => "Email & ID Not Match", 'result' => array()];
                    DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
                    return response()->json($dataOut);
                }

                if($user->linked_id){
                    $dataOut = ['code' => 200, 'message' => "Success", 'result' => array('linked_id' => $user->linked_id)];
                }else{
                    $linked_id = uniqid(8);
                    DB::table('users')->where('id', $user->id)->update(['linked_id' => $linked_id]);

                    $dataOut = ['code' => 200, 'message' => "Success", 'result' => array('linked_id' => $linked_id)];
                }
            }else{
                $dataOut = ['code' => 201, 'message' => "No Result", 'result' => array()];
            }

            DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
            return response()->json($dataOut);
        }catch (\Exception $e) {
            $this->APIErrorEmailALert('getLinkedKey');

            $dataOut = ['code' => 400, 'message' => "Bad Gateway", 'result' => array()];
            DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'Error', 'alert_email' => 1, 'updated_at' => \Carbon\Carbon::now()]);
            return response()->json($dataOut);
        }
    }

    public function validation (Request $request) {
        $data = $request->all();
        try{
            if(!isset($data['email']) || !isset($data['linked_id'])){
                $dataOut = ['code' => 203, 'message' => "Invalid Parameter", 'result' => array()];
                DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
                return response()->json($dataOut);
            }

            $username = $data['email'];
            $linked_id = $data['linked_id'];

            $user = DB::table('users')->where('username', $username)->where('linked_id', $linked_id)->where('cs_type', 99)->first();
            if(isset($user)){
                if($user->linked_id) $dataOut = ['code' => 200, 'message' => "Success", 'result' => array('linked_id' => $user->linked_id)];
                else $dataOut = ['code' => 202, 'message' => "Validation Failed", 'result' => array()];
            }
            else{
                $dataOut = ['code' => 202, 'message' => "Validation Failed", 'result' => array()];
            }

            DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
            return response()->json($dataOut);
        }catch (\Exception $e) {
            $this->APIErrorEmailALert('validation');

            $dataOut = ['code' => 400, 'message' => "Bad Gateway", 'result' => array()];
            DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'Error', 'alert_email' => 1, 'updated_at' => \Carbon\Carbon::now()]);
            return response()->json($dataOut);
        }
    }

    public function fundTransfer (Request $request) {
        $data = $request->all();
        try{
            if(!isset($data['email']) || !isset($data['linked_id']) || !isset($data['amount'])){
                $dataOut = ['code' => 203, 'message' => "Invalid Parameter", 'result' => array()];
                DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
                return response()->json($dataOut);
            }

            $username = $data['email'];
            $linked_id = $data['linked_id'];
            $amount = $data['amount'];

            $user = DB::table('users')->where('username', $username)->where('linked_id', $linked_id)->where('cs_type', 99)->first();
            if(!isset($user)){
                $dataOut = ['code' => 202, 'message' => "Validation Failed", 'result' => array()];
                DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
                return response()->json($dataOut);
            }

            if($amount <= 0){
                $dataOut = ['code' => 203, 'message' => "Invalid Parameter", 'result' => array()];
                DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
                return response()->json($dataOut);
            }

            $member = DB::table('Member')->where('username', $username)->first(['id']);

            $cashBalance = DB::table('Member_Wallet')->where('member_id', $member->id)->first();

            $newBalance = $cashBalance->cash_point + $amount;

            DB::table('Member_Wallet')->where('member_id', $member->id)->update(['cash_point' => $newBalance]);

            DB::table('Member_Wallet_Statement')->insert([
                                                              'member_id' => $member->id,
                                                              'username' => $username,
                                                              'action_type' => 'Transfer from CAPX',
                                                              'wallet_type' => 'C',
                                                              'transaction_type' => 'C',
                                                              'cash_amount' => $amount,
                                                              'balance' =>  $newBalance,
                                                              'remark' => 'Transfer from CAPX to Cash Wallet',
                                                              'created_at' => \Carbon\Carbon::now(),
                                                              'updated_at' => \Carbon\Carbon::now(),
                                                              'remark_display' => 'TH00074/'
                                                              ]);

            $dataOut = ['code' => 200, 'message' => "Success", 'result' => array()];
            DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
            return response()->json($dataOut);
        }catch (\Exception $e) {
            $this->APIErrorEmailALert('fundTransfer');

            $dataOut = ['code' => 400, 'message' => "Bad Gateway", 'result' => array()];
            DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'Error', 'alert_email' => 1, 'updated_at' => \Carbon\Carbon::now()]);
            return response()->json($dataOut);
        }
    }

    public function updateBalance (Request $request) {
        $data = $request->all();
        try{
            if(!isset($data['email']) || !isset($data['linked_id']) || !isset($data['balance']) || !isset($data['type'])){
                $dataOut = ['code' => 203, 'message' => "Invalid Parameter", 'result' => array()];
                DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
                return response()->json($dataOut);
            }

            $username = $data['email'];
            $linked_id = $data['linked_id'];
            $balance = json_decode($data['balance']);

            $user = DB::table('users')->where('username', $username)->where('linked_id', $linked_id)->where('cs_type', 99)->first();
            if(!isset($user)){
                $dataOut = ['code' => 202, 'message' => "Validation Failed", 'result' => array()];
            }else{
                $member = DB::table('Member')->where('username', $username)->first(['id']);
                foreach ($balance as $key => $value) {
                    $key = strtolower($key);
                    if($data['type'] == 0) DB::table('Capx_Member_Coin')->where('member_id', $member->id)->update([$key => $value]);
                    else if($data['type'] == 1) DB::table('Member_Wallet')->where('member_id', $member->id)->update([$key => $value]);
                }
                $dataOut = ['code' => 200, 'message' => "Success", 'result' => array()];
            }

            DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
            return response()->json($dataOut);
        }catch (\Exception $e) {
            $this->APIErrorEmailALert('updateBalance');

            $dataOut = ['code' => 400, 'message' => "Bad Gateway", 'result' => array()];
            DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'Error', 'alert_email' => 1, 'updated_at' => \Carbon\Carbon::now()]);
            return response()->json($dataOut);
        }
    }

    public function updateProfile (Request $request) {
        $data = $request->all();
        try{
            $username = $data['email'];
            $linked_id = $data['linked_id'];
            $address = $data['address'];
            $phone = $data['phone'];

            $user = DB::table('users')->where('username', $username)->where('linked_id', $linked_id)->where('cs_type', 99)->first();
            if(!isset($user)){
                $dataOut = ['code' => 202, 'message' => "Validation Failed", 'result' => array()];
                DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
                return response()->json($dataOut);
            }

            if(!$address || !$phone){
                $dataOut = ['code' => 203, 'message' => "Invalid Parameter", 'result' => array()];
                DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
                return response()->json($dataOut);
            }

            $member = DB::table('Member')->where('username', $username)->first(['id']);
            DB::table('Member_Detail')->where('member_id', $member->id)->update(['phone1' => $phone, 'address' => $address]);

            $dataOut = ['code' => 200, 'message' => "Success", 'result' => array()];
            DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
            return response()->json($dataOut);
        }catch (\Exception $e) {
            $this->APIErrorEmailALert('updateProfile');

            $dataOut = ['code' => 400, 'message' => "Bad Gateway", 'result' => array()];
            DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'Error', 'alert_email' => 1, 'updated_at' => \Carbon\Carbon::now()]);
            return response()->json($dataOut);
        }
    }

    public function checkAPIStatus (Request $request) {
        $data = $request->all();
        try{
            if(!isset($data['check_service_status'])){
                $dataOut = ['code' => 203, 'message' => "Invalid Parameter", 'result' => array()];
                DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
                return response()->json($dataOut);
            }

            $status = DB::table('API_Services')->where('service_id', $data['check_service_status'])->first();
            if(isset($status)){
                $dataOut = ['code' => 200, 'message' => "Success", 'result' => array('status' => $status->status, 'dataOut' => $status->dataOut)];
                DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
                return response()->json($dataOut);
            }else{
                $dataOut = ['code' => 201, 'message' => "No Result", 'result' => array()];
                DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
                return response()->json($dataOut);
            }
        }catch (\Exception $e) {
            $this->APIErrorEmailALert('checkAPIStatus');

            $dataOut = ['code' => 400, 'message' => "Bad Gateway", 'result' => array()];
            DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'Error', 'alert_email' => 1, 'updated_at' => \Carbon\Carbon::now()]);
            return response()->json($dataOut);
        }
    }

    public function insertSealeadDeposit (Request $request) {
            $data = $request->all();
            try{
                if(!isset($data['calculatedAmount'])  || !isset($data['email'])  || !isset($data['path']) || !isset($data['status']) || !isset($data['unit']) || !isset($data['linked_id']) || !isset($data['uniqueId']) ){
                    $dataOut = ['code' => 203, 'message' => "Invalid Parameter", 'result' => array()];
                    DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
                    return response()->json($dataOut);
                }

                $username = $data['email'];
                $calculatedAmount = $data['calculatedAmount'];
                $linked_id = $data['linked_id'];
                $path = $data['path'];
                $status = $data['status'];
                $unit = $data['unit'];
                $uniqueId = $data['uniqueId'];
                $type = 2;

                $user = DB::table('users')->where('username', $username)->where('linked_id', $linked_id)->where('cs_type', 99)->first();
                if(!isset($user)){
                    $dataOut = ['code' => 202, 'message' => "Validation Failed", 'result' => array()];
                    DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
                    return response()->json($dataOut);
                }

                if($calculatedAmount <= 0){
                    $dataOut = ['code' => 203, 'message' => "Invalid Parameter", 'result' => array()];
                    DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
                    return response()->json($dataOut);
                }

                $member = DB::table('Member')->where('username', $username)->first();
                DB::table('admin_sealead')->insert([
                                                                  'member_id' => $member->id,
                                                                  'username' => $username,
                                                                  'type' => $type,
                                                                  'status' => $status,
                                                                  'amount' => $calculatedAmount,
                                                                  'receipt' => $path,
                                                                  'uniqueId' => $uniqueId,
                                                                  'created_at' => \Carbon\Carbon::now(),
                                                                  'updated_at' => \Carbon\Carbon::now(),
                                                                  ]);

                $dataOut = ['code' => 200, 'message' => "Success", 'result' => array('uniqueId'=>$uniqueId)];
                DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
                return response()->json($dataOut);
            }catch (\Exception $e) {
                $this->APIErrorEmailALert('insertSealeadDeposit');

                $dataOut = ['code' => 400, 'message' => "Bad Gateway", 'result' => array()];
                DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'Error', 'alert_email' => 1, 'updated_at' => \Carbon\Carbon::now()]);
                return response()->json($dataOut);
            }
    }
    public function insertWithdrawal (Request $request) {
        $data = $request->all();
        try{

            if(!isset($data['paymentMethod']) || !isset($data['linked_id']) || !isset($data['email']) ){
                $dataOut = ['code' => 203, 'message' => "Invalid Parameter", 'result' => array()];
                DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
                return response()->json($dataOut);
            }

            if($data['paymentMethod'] == 1){

                if(!isset($data['email']) || !isset($data['linked_id']) || !isset($data['bankAccountholder']) || !isset($data['bankAccountnumber']) || !isset($data['bankAddress']) || !isset($data['bankName']) || !isset($data['bankSwiftcode']) || !isset($data['bankCountry']) || !isset($data['status']) || !isset($data['uniqueId']) || !isset($data['unit']) || !isset($data['calculatedAmount'])  ){
                    $dataOut = ['code' => 203, 'message' => "Invalid Parameter", 'result' => array()];
                    DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
                    return response()->json($dataOut);
                }
                // do action
                $username = $data['email'];
                $linked_id = $data['linked_id'];
                $paymentMethod = $data['paymentMethod'];
                $calculatedAmount = $data['calculatedAmount'];
                $bankAccountholder = $data['bankAccountholder'];
                $bankAccountnumber = $data['bankAccountnumber'];
                $bankAddress = $data['bankAddress'];
                $bankName = $data['bankName'];
                $bankSwiftcode = $data['bankSwiftcode'];
                $bankCountry = $data['bankCountry'];
                $uniqueId = $data['uniqueId'];
                $status = $data['status'];
                $type = 1;

                $user = DB::table('users')->where('username', $username)->where('linked_id', $linked_id)->where('cs_type', 99)->first();
                if(!isset($user)){
                    $dataOut = ['code' => 202, 'message' => "Validation Failed", 'result' => array()];
                    DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
                    return response()->json($dataOut);
                }

                if($calculatedAmount <= 0){
                    $dataOut = ['code' => 203, 'message' => "Invalid Parameter", 'result' => array()];
                    DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
                    return response()->json($dataOut);
                }

                $member = DB::table('Member')->where('username', $username)->first();

                DB::table('admin_sealead')->insert([
                                                  'member_id' => $member->id,
                                                  'username' => $username,
                                                  'type' => $type,
                                                  'status' => $status,
                                                  'amount' => $calculatedAmount,

                                                  'bank_name' => $bankName,
                                                  'bank_account_number' => $bankAccountnumber,
                                                  'bank_account_holder' => $bankAccountholder,
                                                  'bank_swiftcode' => $bankSwiftcode,
                                                  'bank_country' => $bankCountry,
                                                  'bank_address' => $bankAddress,

                                                  'uniqueId' => $uniqueId,
                                                  'created_at' => \Carbon\Carbon::now(),
                                                  'updated_at' => \Carbon\Carbon::now(),
                ]);


            }elseif($data['paymentMethod'] == 2){

                if(!isset($data['email']) || !isset($data['linked_id']) || !isset($data['creditCardNumber']) || !isset($data['creditCardCountry']) || !isset($data['calculatedAmount']) || !isset($data['status']) || !isset($data['uniqueId']) || !isset($data['unit']) ){
                    $dataOut = ['code' => 203, 'message' => "Invalid Parameter", 'result' => array()];
                    DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
                    return response()->json($dataOut);
                }
                // // do action

                $username = $data['email'];
                $linked_id = $data['linked_id'];
                $paymentMethod = $data['paymentMethod'];
                $calculatedAmount = $data['calculatedAmount'];
                $creditCardNumber = $data['creditCardNumber'];
                $creditCardCountry = $data['creditCardCountry'];
                $uniqueId = $data['uniqueId'];
                $status = $data['status'];
                $type = 1;

                $user = DB::table('users')->where('username', $username)->where('linked_id', $linked_id)->where('cs_type', 99)->first();
                if(!isset($user)){
                    $dataOut = ['code' => 202, 'message' => "Validation Failed", 'result' => array()];
                    DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
                    return response()->json($dataOut);
                }

                if($calculatedAmount <= 0){
                    $dataOut = ['code' => 203, 'message' => "Invalid Parameter", 'result' => array()];
                    DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
                    return response()->json($dataOut);
                }

                $member = DB::table('Member')->where('username', $username)->first();

                DB::table('admin_sealead')->insert([
                                                  'member_id' => $member->id,
                                                  'username' => $username,
                                                  'type' => $type,
                                                  'payment_type'=>$paymentMethod,
                                                  'status' => $status,
                                                  'amount' => $calculatedAmount,

                                                  'card_number' => $creditCardNumber,
                                                  'card_country' => $creditCardCountry,

                                                  'uniqueId' => $uniqueId,
                                                  'created_at' => \Carbon\Carbon::now(),
                                                  'updated_at' => \Carbon\Carbon::now(),
                                                  ]);
            }
            


            $dataOut = ['code' => 200, 'message' => "Success", 'result' => array('uniqueId'=>$uniqueId)];
            DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);
            return response()->json($dataOut);
        }catch (\Exception $e) {
            $this->APIErrorEmailALert('insertSealeadDeposit');

            $dataOut = ['code' => 400, 'message' => "Bad Gateway", 'result' => array()];
            DB::table('API_Services')->where('service_id', $data['service_id'])->update(['dataOut' => json_encode($dataOut), 'status' => 'Error', 'alert_email' => 1, 'updated_at' => \Carbon\Carbon::now()]);
            return response()->json($dataOut);
        }
    }
}
