<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\AnnouncementRepository;
use App\Repositories\MemberRepository;

class AnnouncementController extends Controller
{
    /**
     * The AnnouncementRepository instance.
     *
     * @var \App\Repositories\AnnouncementRepository
     */
    protected $AnnouncementRepository;

    /**
     * Create a new AnnouncementController instance.
     *
     * @param \App\Repositories\AnnouncementRepository $AnnouncementRepository
     * @return void
     */
    public function __construct(AnnouncementRepository $AnnouncementRepository) {
        parent::__construct();
        $this->AnnouncementRepository = $AnnouncementRepository;
        $this->middleware('member');
    }

    /**
     * Datatable List
     * @return [type] [description]
     */
    public function getList () {
        return $this->AnnouncementRepository->getList();
    }

    /**
     * Announcement List
     * @return html
     */
    public function getAll () {
        return view('front.announcement.list');
    }

    /**
     * Announcement Read Page
     * @param  integer $id
     * @return html
     */
    public function read ($lang, $id) {
        $memberRepo = new MemberRepository;

        $user = \Sentinel::getUser();
        $member = $memberRepo->findByUsername(trim($user->username));
        $newInsert = 0;

        if($member->detail->announcement == $id){
            $newInsert = 1;
            $member->detail->announcement = 0;
            $member->detail->save();
        }

        if (!$model = $this->AnnouncementRepository->findById($id)) {
            $response = array('status' => 1, 'msg' => \Lang::get('error.announcementNotFound'));
        }else{
            $response = array('status' => 0, 'msg' => '', 'model' => $model, 'newInsert' => $newInsert);
        }

        return response()->json($response);
    }
}
