<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\MemberRepository;
use App\Repositories\NotificationRepository;
use App\Services\CheckingService;
use Carbon\Carbon;

class NotificationsController extends Controller
{
    protected $MemberRepository;
    protected $NotificationRepository;

    public function __construct(MemberRepository $MemberRepository,NotificationRepository $NotificationRepository) {

        $this->MemberRepository = $MemberRepository;
        $this->NotificationRepository = $NotificationRepository;
        $this->middleware('member');
    }

    /**
     * Member Withdraw Cash Point
     * @return [type] [description]
     */
    public function getList () {
        
        $user = \Sentinel::getUser();
        $Member = $this->MemberRepository->findByUsername(trim($user->username));
    
        $unreadSystemNotification = \DB::table('notification_target')->where('member_id', $Member->id)->where('type',0)->where('read', 0)->count();
        $unreadCsNotification = \DB::table('notification_target')->where('member_id', $Member->id)->where('type',1)->where('read', 0)->count();
        $unreadAnnouncement = $Member->detail->announcement;

        $unRead = collect([
                            "SystemNotification" => $unreadSystemNotification,
                            "CsNotification" => $unreadCsNotification,
                            "Announcement" => $unreadAnnouncement
                        ])->all();

        return view('front.Inbox.list')->with('unRead',$unRead);
    }

    public function getCSlist () {
        
        $user = \Sentinel::getUser();
        $Member = $this->MemberRepository->findByUsername(trim($user->username));
        return $this->NotificationRepository->getCSlist($Member->id);
        
    }

    public function getSYSTEMlist () {
        
        $user = \Sentinel::getUser();
        $Member = $this->MemberRepository->findByUsername(trim($user->username));
        return $this->NotificationRepository->getSYSTEMlist($Member->id);
        
    }
    public function read ($sa,$id) {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername($user->username);

        
        if (!$model = $this->NotificationRepository->findByMemberId($id,$member->id)) {

            return redirect()->back()->with('flashMessage', [
                'class' => 'danger',
                'message' => 'Data not found.'
            ]);
        }
        $subject = "subject_".\App::getLocale();
        $content = "content_".\App::getLocale();
        $model->subject = $model->$subject;
        $model->content = $model->$content;
        
        $this->NotificationRepository->findByReadUpdate($id);
        return view('front.Inbox.read')->with('model', $model)->with('user',$user);
    }
}
