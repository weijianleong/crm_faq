<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\WithdrawRepository;
use App\Repositories\MemberRepository;
use App\Repositories\BankInfoRepository;
use App\Repositories\DepositRepository;
use App\Repositories\SealeadRepository;
use App\Services\CheckingService;
use App\Services\UploadpicService;
use App\Services\AmazonS3;
use Carbon\Carbon;

class SealeadController extends Controller
{
    protected $WithdrawRepository;
    protected $MemberRepository;
    protected $BankInfoRepository;
    protected $SealeadRepository;
    protected $CheckingService;
    protected $UploadpicService;
    protected $AmazonS3;

    public function __construct
    (
        WithdrawRepository $WithdrawRepository,
        MemberRepository $MemberRepository,
        BankInfoRepository $BankInfoRepository,
        DepositRepository $DepositRepository,
        SealeadRepository $SealeadRepository,
        CheckingService $CheckingService,
        UploadpicService $UploadpicService,
        AmazonS3 $AmazonS3
    )
    {

        $this->WithdrawRepository = $WithdrawRepository;
        $this->MemberRepository = $MemberRepository;
        $this->BankInfoRepository = $BankInfoRepository;
        $this->DepositRepository = $DepositRepository;
        $this->CheckingService = $CheckingService;
        $this->SealeadRepository = $SealeadRepository;
        $this->UploadpicService = $UploadpicService;
        $this->AmazonS3 = $AmazonS3;
        $this->middleware('member');
    }

    /**
     * Member Withdraw Cash Point
     * @return [type] [description]
     */
    public function getDeposit () {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername($user->username);

        if(empty($member->detail->currency_seanet)){
            $currency = \DB::table('seanet_currency')->where('id',1)->first();
        }else{
            $currency = \DB::table('seanet_currency')->where('id',$member->detail->currency_seanet)->first();
        }

        $seanet5 = config('settings.seanet5');



        return view('front.sealead.deposit')
            ->with('user',$user)
            ->with('member',$member)
            ->with('seanet5',$seanet5)
            ->with('currency',$currency);
    }
    public function getWithdraw () {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername($user->username);
        $bankList = $this->BankInfoRepository->findBankInfo($member->id,2);

        if(empty($member->detail->currency_seanet)){
            $currency = \DB::table('seanet_currency')->where('id',1)->first();
        }else{
            $currency = \DB::table('seanet_currency')->where('id',$member->detail->currency_seanet)->first();
        }

        $seanet5 = config('settings.seanet-5');

        return view('front.sealead.withdrawAsia')
            ->with('bankList',$bankList)
            ->with('user',$user)
            ->with('member',$member)
            ->with('seanet5',$seanet5)
            ->with('currency',$currency);
    }
    public function getAddbank () {

        return view('front.sealead.addbank');
    }
    public function setCurrency () {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        if(empty($member->detail->currency_seanet)){

            $currency = \DB::table('seanet_currency')->get();
            return view('front.sealead.setCurrency')
                ->with('user',$user)
                ->with('member',$member)
                ->with('currency',$currency);

        }else{

            $currency = \DB::table('seanet_currency')->where('id',$member->detail->currency_seanet)->first();
            return view('front.sealead.setCurrency')
                ->with('user',$user)
                ->with('member',$member)
                ->with('currency',$currency);

        }

    }
    public function getRecord () {

        return view('front.sealead.record');
    }

    public function findRecord () {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        return $this->SealeadRepository->findRecord($member->id);

    }
    public function updateCurrency (Request $request){

        $getData = $request->Data;
        $data = json_decode($getData,true); //$data["Data"]["getPassword1"] example
        $SPassword = $request->SPassword;
        $currency = $data["Data"]["currency"];


        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        if ($member->secret_password != $SPassword) {
            return response()->json(['SPassword'=>"1"]);
        }

        if(!$getCurrency = \DB::table('seanet_currency')->where('id',$currency)->first()){

            $tittle = \Lang::get('sealead.warning');
            $text = \Lang::get('sealead.currency_wrong');
            $returnUrl = "";
            return response()->json(['result' => "1", "showTittle" => $tittle, "showText" => $text, "returnUrl" => $returnUrl]);
        }

        $member->detail->currency_seanet = $getCurrency->id;
        $member->detail->save();

        $tittle = \Lang::get('sealead.success');
        $text = \Lang::get('sealead.currencySuccess');
        $returnUrl = route('sealead.setCurrency', ['lang' => \App::getLocale()]);
        return response()->json(['result' => "0", "showTittle" => $tittle, "showText" => $text, "returnUrl" => $returnUrl]);
    }
    public function getBankDataAsia (Request $request) {
        $id = $request->id;
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername($user->username);
        if(!$bankInfo = $this->BankInfoRepository->findByNoSelf($id,$member->id)){
            $tittle = \Lang::get('withdraw.AccFail');
            $text = "";
            $returnUrl = "";
            return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"returnUrl"=>$returnUrl]);

        }
        $bankData = $bankInfo;
        $bankData = json_encode($bankData);
        $tittle = \Lang::get('withdraw.addBanksuc');
        $text = \Lang::get('withdraw.goWithdrawal');
        return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,"bankData"=>$bankData]);
    }

    public function postCreateDeposit (Request $request){
        $getData = $request->allData;
        $data_js = json_decode($getData,true); //$data["Data"]["getPassword1"] example
        $amount = $data_js["Data"]["amount"];
        $seanet5 = config('settings.seanet5');
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        if( !$getCurrency = \DB::table('seanet_currency')->where('id',$member->detail->currency_seanet)->first()) {

            $tittle = \Lang::get('sealead.warning');
            $text = \Lang::get('sealead.currency_wrong');
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
        }else{

            if ($amount <= 0) {
                $tittle = \Lang::get('sealead.warning');
                $text = \Lang::get('sealead.amountminNotice');
                return response()->json(['result' => "1", "showTittle" => $tittle, "showText" => $text]);
            }
            if (!empty(\Input::file('image1'))) {
                $idFront_path = \Input::file('image1');
                $checking = $this->UploadpicService->CheckFileSupported($idFront_path);
                if (empty($checking)) {
                    $tittle = \Lang::get('sealead.warning');
                    $text = \Lang::get('error.uploadProfileFail');
                    $returnUrl = "";
                    return response()->json(['result' => "1", "showTittle" => $tittle, "showText" => $text, "returnUrl" => $returnUrl]);
                }

                $filename1 = $this->AmazonS3->uploadImageNew($idFront_path, $user, 'sealead', uniqid());
                $data['receipt'] = $filename1;
            }

            $rate = $getCurrency->exchangeRate * $seanet5;
            $data['member_id'] = $member->id;
            $data['username'] = $member->username;
            $data['type'] = 2;
            $data['status'] = 1;
            $data['amount'] = $amount;
            $data['currency'] = $getCurrency->currency;
            $data['rate'] = $rate;

            try {
                $this->SealeadRepository->store($data);

                $tittle = \Lang::get('sealead.success');
                $text = \Lang::get('sealead.depositSubmitted');
                $returnUrl = route('sealead.record', ['lang' => \App::getLocale()]);

                return response()->json(['result' => "0", "showTittle" => $tittle, "showText" => $text, "returnUrl" => $returnUrl]);

            }
            catch (\Exception $e) {
                $tittle = "Oops...";
                $text = "";
                $html = \Lang::get('error.helpDesk');
                return response()->json(['result' => "2", "showTittle" => $tittle, "showText" => $text, "showHtml" => $html]);
            }
        }
    }

    public function postCreateWithdraw (Request $request){
        $SPassword = $request->SPassword;
        $getData = $request->Data;

        $data = json_decode($getData,true); //$data["Data"]["getPassword1"] example
        $type = $data["Data"]["type"];
        $amount = $data["Data"]["amount"];
        $bank_id = $data["Data"]["bank_id"];
        $vm_cardname = $data["Data"]["vm_cardname"];
        $vm_cardname2 = $data["Data"]["vm_cardname2"];
        $vm_country = $data["Data"]["vm_country"];
//        $currency = $data["Data"]["currency"];
        $seanet5 = config('settings.seanet-5');


        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        if ($member->secret_password != $SPassword) {
            return response()->json(['SPassword'=>"1"]);
        }

        if($amount < 30 ){
            $tittle = \Lang::get('sealead.warning');
            $text = \Lang::get('sealead.amountminNotice');
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
        }

        if ($member->wallet->cash_point < $amount) {
            $tittle = \Lang::get('sealead.warning');
            $text = \Lang::get('error.cashPointNotEnough');
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
        }

        if( !$getCurrency = \DB::table('seanet_currency')->where('id',$member->detail->currency_seanet)->first()) {

            $tittle = \Lang::get('sealead.warning');
            $text = \Lang::get('sealead.currency_wrong');
            return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
        }
        else{


            $rate = $getCurrency->exchangeRate * $seanet5;

            if ($type == "bank") {

                if (!$bankInfo = $this->BankInfoRepository->findByNoSelf($bank_id, $member->id)) {
                    $tittle = \Lang::get('sealead.warning');
                    $text = \Lang::get('withdraw.AccFail');
                    $returnUrl = "";
                    return response()->json(['result' => "2", "showTittle" => $tittle, "showText" => $text, "returnUrl" => $returnUrl]);
                }
                if (!empty($data["Data"]["swift"]) || !empty($data["Data"]["bank_account_holder"])) {
                    $swift = $data["Data"]["swift"];
                    $bank_account_holder = $data["Data"]["bank_account_holder"];
                    $bankInfo->swift = $swift;
                    $bankInfo->bank_account_holder = $bank_account_holder;
                    $bankInfo->save();
                }

                $insertData["member_id"] = $member->id;
                $insertData["username"] = $member->username;
                $insertData["amount"] = $amount;
                $insertData["currency"] = $getCurrency->currency;
                $insertData["rate"] = $rate;


                $insertData["type"] = 1;
                $insertData["status"] = 1;
                $insertData["payment_type"] = 1;

                $insertData["bank_name"] = $bankInfo->bank_name;
                $insertData["bank_account_number"] = $bankInfo->bank_no;
                $insertData["bank_account_holder"] = $bankInfo->bank_account_holder;
                $insertData["bank_swiftcode"] = $bankInfo->swift;
                $insertData["bank_country"] = $bankInfo->countryName;
                $insertData["bank_address"] = $bankInfo->bank_address;


            }
            else if ($type == "debit") {

                if (empty($vm_cardname) || empty($vm_country)) {

                    $tittle = \Lang::get('sealead.warning');
                    $text = \Lang::get('error.debitError');
                    $returnUrl = "";
                    return response()->json(['result' => "1", "showTittle" => $tittle, "showText" => $text, "returnUrl" => $returnUrl]);

                }
                else {

                    $insertData["member_id"] = $member->id;
                    $insertData["username"] = $member->username;
                    $insertData["amount"] = $amount;
                    $insertData["currency"] = $getCurrency->currency;
                    $insertData["rate"] = $rate;

                    $insertData["type"] = 1;
                    $insertData["status"] = 1;
                    $insertData["payment_type"] = 2;

                    $insertData["card_number"] = $vm_cardname;
                    $insertData["card_country"] = $vm_country;
                }

            }
            try {

                $this->SealeadRepository->store($insertData);

                $member->wallet->cash_point -= $amount;
                $member->wallet->save();

                $remark = 'Debited ' . $amount . ' for withdrawal';
                $remarkAry = array('amount' => $amount);
                $remark_display = 'TH00010/' . json_encode($remarkAry);

                $this->SealeadRepository->saveWalletStatement($member->id, $member->username, $amount, 'Withdraw', 'C', 'D', $member->wallet->cash_point, $remark, $remark_display);

                $tittle = \Lang::get('sealead.success');
                $text = \Lang::get('sealead.withdrawSubmitted');
                $returnUrl = route('sealead.record', ['lang' => \App::getLocale()]);
                return response()->json(['result' => "0", "showTittle" => $tittle, "showText" => $text, "returnUrl" => $returnUrl]);

            }
            catch (\Exception $e) {
                $tittle = "Oops...";
                $text = "";
                $html = \Lang::get('error.helpDesk');
                return response()->json(['result' => "2", "showTittle" => $tittle, "showText" => $text, "showHtml" => $html]);
            }

        }
    }

    public function getWithdrawRecord () {

        return view('front.withdrawal.withdrawalRecord');
    }
    public function getWithdrawAsiaRecord () {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $detail = $member->detail;
        if($detail->nationality == 'Vietnam1' || $detail->nationality == 'Thailand1'){
            return view('front.withdrawal.withdrawAsiaSystemRecord');
        }else{
            return view('front.withdrawal.withdrawAsiaRecord');
        }

    }

    public function WithdrawalRecords () {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $detail = $member->detail;
        return $this->DepositRepository->getWithdrawList(2,$member->id,'CNY');
    }

    public function withdrawAsiaSystemRecords () {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $detail = $member->detail;
        if($detail->nationality == 'Vietnam1'){
            return $this->WithdrawRepository->getWithdrawSystemList($member->id);
            //return $this->DepositRepository->getWithdrawList(2,$member->id,'VND');
        }else if($detail->nationality == 'Thailand1'){
            return $this->DepositRepository->getWithdrawList(2,$member->id,'THB');
        }
    }

    public function cancel (Request $request) {

        $payID = $request->id;
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        if(!$model = $this->DepositRepository->findById($payID,$member->id)){
            $result = "fail";
            return response()->json(['model'=>$model,'result'=>$result]);
        }
        if ($model->type == 2)
        {
            if ($model->status != 1 && $model->status != 5 && $model->status != 6){
                $result = "fail";
                return response()->json(['model'=>$model,'result'=>$result]);
            }
            else{
                if(!$ans = $this->DepositRepository->cancelWithdraw($payID,$member->id)){
                    $result = "fail";
                    return response()->json(['model'=>$model,'result'=>$result]);
                }
                $result = "sucess";
                $UserName = $user->username;
                $OperateLog ="Update transaction status to CancelledOrder";
                $OperateTime = Carbon::now();
                $MemberId = $member->id;
                $TransId = $model->TransID;
                $TransMemo = "";
                $RunSql ="update web_deposit_log SET web_deposit_log.status=3 WHERE web_deposit_log.TransID=".$TransId;
                \DB::table('user_optlog')->insert(
                    [
                        'adminname' => $UserName,
                        'adminmemo' => $OperateLog,
                        'admintime' => $OperateTime,
                        'record_sql' => $RunSql,
                        'memberId' => $MemberId,
                        'TransID' => $TransId,
                        'transmemo' => $TransMemo,
                    ]
                );

            }
        }
        return response()->json(['model'=>$model,'result'=>$result]);

    }






    // ==================================================JACKY=================================//





    public function postMakeWithdraw () { //weilun

        $data = \Input::get('data');
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        if ($member->secret_password != trim($data['s'])) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  \Lang::get('error.securityPasswordError')
            ]);
        }


        if($data["payment_type"]=="bank"){

            if (empty($data["bank_name"]) || empty($data["bank_account_holder"]) || empty($data["bank_account_number"])
                || empty($data["bank_swiftcode"])|| empty($data["bank_address"])|| empty($data["bank_country"])){

                return \Response::json([
                    'type'  =>  'error',
                    'message'   =>  \Lang::get('error.bankError')
                ]);

            }else{

                $detail = $member->detail;
                foreach ($detail->getAttributes() as $k => $d) {
                    if (isset($data[$k])) $detail->{$k} = $data[$k];
                }
                $detail->save(); //update tb_member_detail bank info

                try {
                    $this->WithdrawRepository->makeWithdraw($member, $data['amount'] ,$data["payment_type"],'');
                } catch (\Exception $e) {
                    return \Response::json([
                        'type'  =>  'error',
                        'message'   =>  $e->getMessage()
                    ]);
                }

            }

        }else if($data["payment_type"]=="debit"){



            if (empty($data["v_cardname"]) || empty($data["v_country"]) ){


                return \Response::json([
                    'type'  =>  'error',
                    'message'   =>  \Lang::get('error.debitError')
                ]);

            }else{

                $debit_data = ['card_number' => $data["v_cardname"],'card_country'=>$data["v_country"]];


                try {
                    $this->WithdrawRepository->makeWithdraw($member, $data['amount'] ,$data["payment_type"], $debit_data);
                } catch (\Exception $e) {
                    return \Response::json([
                        'type'  =>  'error',
                        'message'   =>  $e->getMessage()
                    ]);
                }
            }

        }else{

            return \Response::json([
                'type'  =>  'error',
                'message'   => \Lang::get('error.paymentError')
            ]);

        }







        \Cache::forget('member.' . $user->id);

        return \Response::json([
            'type'  =>  'success',
            'message'   =>  \Lang::get('message.withdrawSuccess'),
            'redirect' => route('transaction.withdrawSuccess', ['lang' => \App::getLocale()])
        ]);

    }

    /**
     * Withdraw list - DataTable
     * @return [type] [description]
     */
    public function getList () {
        $user = \Sentinel::getUser();
        $member = $user->member;
        return $this->WithdrawRepository->getList($member);
    }
}
