<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\WithdrawRepository;
use App\Repositories\MemberRepository;
use App\Repositories\BankInfoRepository;
use App\Repositories\DepositRepository;
use App\Services\CheckingService;
use Carbon\Carbon;

class BankInfoController extends Controller
{

    protected $WithdrawRepository;
    protected $MemberRepository;
    protected $BankInfoRepository;
    protected $CheckingService;
    

    public function __construct(WithdrawRepository $WithdrawRepository,MemberRepository $MemberRepository,
        BankInfoRepository $BankInfoRepository,DepositRepository $DepositRepository,CheckingService $CheckingService) {
        $this->WithdrawRepository = $WithdrawRepository;
        $this->MemberRepository = $MemberRepository;
        $this->BankInfoRepository = $BankInfoRepository;
        $this->DepositRepository = $DepositRepository;
        $this->CheckingService = $CheckingService;
        $this->middleware('member');
    }

    /**
     * Member Withdraw Cash Point
     * @return [type] [description]
     */
    public function getBankInfoAsia () {
        $user = \Sentinel::getUser();
        return view('front.settings.bankInfoAsia');
    }
    public function getAddBankAsia () {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername($user->username);
        $detail = $member->detail;

            if($detail->nationality == "Vietnam1"){
                $bankList = \DB::table('bank_list')->where('bank_type',98)->where('currency','VND')->orderBy('bank_code')->get();
                return view('front.settings.addBankAsiaSystem')->with('bankList',$bankList)->with('detail',$detail);

            }else if($detail->nationality == "Thailand1"){
                $bankList = \DB::table('bank_list')->where('bank_type',97)->where('currency','THB')->orderBy('bank_code')->get();
                return view('front.settings.addBankAsiaSystem')->with('bankList',$bankList)->with('detail',$detail);;

            }else{

                return view('front.settings.bank');
            }

        
    }
    public function getBankDetail($lang,$id){
        
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername($user->username);

        if(!$model =  $this->BankInfoRepository->findById($id,$member->id)){
            return redirect()->back()->with('flashMessage', [
                'class' => 'danger',
                'message' => "Data no find"
            ]);
        }

        if($model->withdraval_type == 2 ){

            return view('front.settings.bankDetailAsia')->with('model', $model);
            
        }else if($model->withdraval_type == 98 ){

            $bankList = \DB::table('bank_list')->where('bank_type',98)->orderBy('bank_code')->get();
            return view('front.settings.bankDetailAsiaSystem')->with('model', $model)->with('bankList',$bankList);;

        }else {

            $language = \App::getLocale();
            if($language == 'chs'){

                $language = 'zh-cn';
            }else{
                $language = 'en-us';
            }
            $country = \DB::table('country')->where('languagecode',$language)->orderBy('order')->get();


            if(!$state = \DB::table('state')->where('countrycode',$model->countryName)->where('languagecode',$language)->pluck("statename","statecode")->all()){
                if(!$state  = \DB::table('city')->where('countrycode',$model->countryName)->where('languagecode',$language)->pluck("cityname","citycode")->all()){
                    $state = \DB::table('country')->where('countrycode',$model->countryName)->where('languagecode',$language)->pluck("countryname","countrycode")->all();
                }
            }

            if(!$city = \DB::table('city')->where('statecode',$model->stateName)->where('languagecode',$language)->pluck("cityname","citycode")->all()){
                $city = \DB::table('country')->where('countrycode',$model->stateName)->where('languagecode',$language)->pluck("countryname","countrycode")->all();
            }

            $bankList = \DB::table('bank_list')->where('bank_type',4)->orderBy('bank_code')->get();
            return view('front.settings.bankDetail')->with('model', $model)->with('bankList',$bankList)->with('country',$country)->with('state',$state)->with('city',$city)->with('language',$language);

        }
        //for china
    }
    public function getBankList(Request $request){

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername($user->username);
        // $network_data = \DB::table('Member_Network')->where('parent_username', 'account2@trfxbiz.com')->where('username', '=',$user->username)->first();
        // if(!empty($network_data))
        // $parent_username = $network_data->parent_username;
        // else
        // $parent_username = '';

        // if($parent_username == "account2@trfxbiz.com" || $user->username=="fxdb222@gmail.com"){
        //     return $this->BankInfoRepository->getList($member->id,2);
        // }
            return $this->BankInfoRepository->getList($member->id,1);
        
    }
    public function getBankListAsia(Request $request){

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername($user->username);
        $detail = $member->detail;
        if($detail->nationality == 'Vietnam1'){
            return $this->BankInfoRepository->getList($member->id,98); 
        }
        else if($detail->nationality == 'Thailand1'){
            return $this->BankInfoRepository->getList($member->id,97); 
        }else{
            return $this->BankInfoRepository->getList($member->id,2);
        }
        
    }
    public function addBank (Request $request){

        $SPassword = $request->SPassword;
        $getData = $request->Data;
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        if ($member->secret_password != $SPassword) {
            return response()->json(['SPassword'=>"1"]);
        }

        $data_js = json_decode($getData,true); //$data["Data"]["getPassword1"] example
        $bankData["bank_no"] = $this->CheckingService->CheckSpace($data_js["Data"]["bank_no"]);
        $bankData["sub_bank"] = $this->CheckingService->CheckSpace($data_js["Data"]["sub_bank"]);
        $bankData["bank_name"] = \DB::table('bank_list')->where('bank_value',$data_js["Data"]["bank_name"])->value('bank_name');
        $bankData["bank_code"] = $data_js["Data"]["bank_name"];
        $bankData["member_id"] = $member->id;
        $bankData["bank_address"] = $data_js["Data"]["bank_address"];
        $bankData["countryName"] = $this->CheckingService->CheckSpace($data_js["Data"]["countryName"]);
        $bankData["stateName"] = $this->CheckingService->CheckSpace($data_js["Data"]["state"]);
        $bankData["cityName"] = $this->CheckingService->CheckSpace($data_js["Data"]["city"]);

        $bankData["withdraval_type"] = 1;

        if(empty($data_js["Data"]["bank_id"])){ //Add
            if(!$this->BankInfoRepository->findByNo($bankData["bank_no"],$bankData["bank_code"])){
                $this->BankInfoRepository->store($bankData);
                $tittle = \Lang::get('withdraw.addBanksuc');
                $text = \Lang::get('withdraw.goWithdrawal');
                $returnUrl = route('withdrawal', ['lang' => \App::getLocale()]);
                return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,"returnUrl"=>$returnUrl]);
            }else{
                $tittle = \Lang::get('withdraw.hadAcc');
                $text = "";
                return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>""]);
            }
        }else{ //Edit
            $id = $data_js["Data"]["bank_id"];
            if(!$this->BankInfoRepository->findById($id,$member->id)){
                
                $tittle = "Data no Find";
                $text = "请确认是否在修改自己的银行账号";
                $returnUrl = route('withdrawal', ['lang' => \App::getLocale()]);
                return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
            }

            if($this->BankInfoRepository->findByNoChinaSelf($id,$member->id,$bankData["bank_no"],$bankData["bank_code"])){
                $this->BankInfoRepository->update($id,$bankData);
                $tittle = \Lang::get('withdraw.editBanksuc');
                $text = \Lang::get('withdraw.goWithdrawal');
                $returnUrl = route('withdrawal', ['lang' => \App::getLocale()]);
                return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,"returnUrl"=>$returnUrl]);
            }else{
                if($this->BankInfoRepository->findByNo($bankData["bank_no"],$bankData["bank_code"])){
                    $tittle = \Lang::get('withdraw.hadAcc');
                    $text = "";
                    return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>""]);
                }else{
                    $this->BankInfoRepository->update($id,$bankData);
                    $tittle = \Lang::get('withdraw.editBanksuc');
                    $text = \Lang::get('withdraw.goWithdrawal');
                    $returnUrl = route('withdrawal', ['lang' => \App::getLocale()]);
                    return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,"returnUrl"=>$returnUrl]);
                }

            }


        } 
    }
    public function addBankAsia (Request $request){

        $SPassword = $request->SPassword;
        $getData = $request->Data;
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        if ($member->secret_password != $SPassword) {
            return response()->json(['SPassword'=>"1"]);
        }

        $data_js = json_decode($getData,true); //$data["Data"]["getPassword1"] example
        $bankData["bank_no"] = $this->CheckingService->CheckSpace($data_js["Data"]["bank_no"]);
        $bankData["sub_bank"] = $data_js["Data"]["sub_bank"];
        $bankData["bank_name"] = $data_js["Data"]["bank_name"];
        $bankData["bank_account_holder"] = $data_js["Data"]["bank_account_holder"];
        $bankData["bank_address"] = $data_js["Data"]["bank_address"];
        $bankData["countryName"] = $data_js["Data"]["countryName"];
        $bankData["swift"] = $data_js["Data"]["swift"];
        $bankData["member_id"] = $member->id;
        $bankData["withdraval_type"] = 2;

        if(empty($data_js["Data"]["bank_id"])){ //Add
            if(!$this->BankInfoRepository->findByNoAsia($bankData["bank_no"],$bankData["bank_name"])){
                $this->BankInfoRepository->store($bankData);
                $tittle = \Lang::get('withdraw.addBanksuc');
                $text = \Lang::get('withdraw.goWithdrawal');
                $returnUrl = route('withdraw.withdrawAsia', ['lang' => \App::getLocale()]);
                return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,"returnUrl"=>$returnUrl]);
            }else{
                $tittle = \Lang::get('withdraw.hadAcc');
                $text = "";
                return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>""]);
            }
        }else{ //Edit
            $id = $data_js["Data"]["bank_id"];
            if(!$this->BankInfoRepository->findById($id,$member->id)){
                
                $tittle = "Data no Find";
                $text = "请确认是否在修改自己的银行账号";
                $returnUrl = route('withdrawal', ['lang' => \App::getLocale()]);
                return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
            }

            if($this->BankInfoRepository->findByNoAsiaSelf($id,$member->id,$bankData["bank_no"],$bankData["bank_name"])){
                $this->BankInfoRepository->update($id,$bankData);
                $tittle = \Lang::get('withdraw.editBanksuc');
                $text = \Lang::get('withdraw.goWithdrawal');
                $returnUrl = route('withdraw.withdrawAsia', ['lang' => \App::getLocale()]);
                return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,"returnUrl"=>$returnUrl]);
            }else{
                if($this->BankInfoRepository->findByNoAsia($bankData["bank_no"],$bankData["bank_name"])){
                    $tittle = \Lang::get('withdraw.hadAcc');
                    $text = "";
                    return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>""]);
                }else{
                    $this->BankInfoRepository->update($id,$bankData);
                    $tittle = \Lang::get('withdraw.editBanksuc');
                    $text = \Lang::get('withdraw.goWithdrawal');
                    $returnUrl = route('withdraw.withdrawAsia', ['lang' => \App::getLocale()]);
                    return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,"returnUrl"=>$returnUrl]);
                }

            }


        } 
    }
    public function addBankAsiaSystem (Request $request){

        $SPassword = $request->SPassword;
        $getData = $request->Data;
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $detail = $member->detail; 
        if ($member->secret_password != $SPassword) {
            return response()->json(['SPassword'=>"1"]);
        }

        $data_js = json_decode($getData,true); //$data["Data"]["getPassword1"] example
        $bankData["bank_no"] = $this->CheckingService->CheckSpace($data_js["Data"]["bank_no"]);
        $bankData["sub_bank"] = $this->CheckingService->CheckSpace($data_js["Data"]["sub_bank"]);
        $bankData["bank_name"] = \DB::table('bank_list')->where('bank_value',$data_js["Data"]["bank_name"])->value('bank_name');
        $bankData["bank_code"] = $data_js["Data"]["bank_name"];
        $bankData["member_id"] = $member->id;
        $bankData["bank_address"] = $data_js["Data"]["bank_address"];
        $bankData["countryName"] = $data_js["Data"]["countryName"];
        $bankData["stateName"] = $data_js["Data"]["stateName"];
        $bankData["cityName"] = $data_js["Data"]["cityName"];
        $bankData["swift"] = $data_js["Data"]["swift"];
        $bankData["bank_account_holder"] = $data_js["Data"]["bank_account_holder"];
        
        if($detail->nationality == 'Vietnam1'){
            $bankData["withdraval_type"] = 98;
        }else if($detail->nationality == 'Thailand1'){
            $bankData["withdraval_type"] = 97;
        }

        if($member->process_status == 'N'){
            $returnUrl = route('settings.BankInfoAsia', ['lang' => \App::getLocale()]);
        }else{
            $returnUrl = route('withdraw.withdrawAsia', ['lang' => \App::getLocale()]);
        }

        if(empty($data_js["Data"]["bank_id"])){ //Add
            if(!$this->BankInfoRepository->findByNo($bankData["bank_no"],$bankData["bank_code"])){
                $this->BankInfoRepository->store($bankData);
                $tittle = \Lang::get('withdraw.addBanksuc');
                $text = \Lang::get('withdraw.goWithdrawal');
                return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,"returnUrl"=>$returnUrl]);
            }else{
                $tittle = \Lang::get('withdraw.hadAcc');
                $text = "";
                return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>""]);
            }
        }else{ //Edit

            $id = $data_js["Data"]["bank_id"];
            if(!$this->BankInfoRepository->findById($id,$member->id)){
                
                $tittle = "Data no Find";
                $text = "";
                return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text]);
            }

            if($this->BankInfoRepository->findByNoChinaSelf($id,$member->id,$bankData["bank_no"],$bankData["bank_code"])){
                $this->BankInfoRepository->update($id,$bankData);
                $tittle = \Lang::get('withdraw.editBanksuc');
                $text = \Lang::get('withdraw.goWithdrawal');
                return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,"returnUrl"=>$returnUrl]);
            }else{
                if($this->BankInfoRepository->findByNo($bankData["bank_no"],$bankData["bank_code"])){
                    $tittle = \Lang::get('withdraw.hadAcc');
                    $text = "";
                    return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>""]);
                }else{
                    $this->BankInfoRepository->update($id,$bankData);
                    $tittle = \Lang::get('withdraw.editBanksuc');
                    $text = \Lang::get('withdraw.goWithdrawal');
                    return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,"returnUrl"=>$returnUrl]);
                }

            }
        } 
    }
    public function postCreate (Request $request){
        $data = \Input::get('data');
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $CashPoint = $member->wallet;
        

        if ($member->secret_password != trim($data['s'])) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  \Lang::get('error.securityPasswordError')
            ]);
        }

        if ($CashPoint->cash_point < $data['amount']) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  \Lang::get('error.cashPointNotEnough')
            ]);
        }

        $language = \App::getLocale();
        if($language == 'chs'){

            $language = 'zh-cn';
        }else{
            $language = 'en-us';
        }

        $insertData["TransID"] = $member->id."_W". date('ymd').uniqid();
        $insertData["type"] = 2;
        $insertData["OrderMoney"] = 0;
        $insertData["fundmoney"] = $data["amount"];
        $insertData["wid"] = $member->id;
        $insertData["login"] = 0;
        $insertData["created"] = Carbon::now()->timestamp;;
        $insertData["paybank"] = 0;
        $insertData["memo"] = $data["remark"];
        $insertData["status"] = 1;
        $insertData["deal_time"] = Carbon::now();
        $insertData["cname"] = $user->first_name;
        $insertData["optIp"] = $request->ip();

        if($data["bankInfo"]){ // if have acc then get data from db

            if(!$BankInfo = $this->BankInfoRepository->findById($data["bankInfo"],$member->id)){
                return \Response::json([
                            'type'  =>  'error',
                            'message'   => \Lang::get('withdraw.AccFail')
                ]);
            }
            
            if($BankInfo->withdraval_type == 1){
                $insertData["paybankcode"] = $BankInfo->bank_name;
                $bankData["bank_name"] = \DB::table('bank_list')->where('bank_value',$BankInfo->bank_name)->value('bank_name');
                
            }else{  
                $bankData["bank_name"] = $BankInfo->bank_name;
            }

            $bankData["swift"] = $BankInfo->swift;
            $bankData["sub_bank"] = $BankInfo->sub_bank;
            $bankData["bank_address"] = $BankInfo->bank_address;
            $bankData["bank_no"] = $BankInfo->bank_no;
            $bankData["withdraval_type"] = $BankInfo->withdraval_type;

            $bankData["countryName"] = \DB::table('country')->where('countrycode',$BankInfo->countryName)->where('languagecode',$language)->value('countryname');
            if(!$bankData["stateName"] = \DB::table('state')->where('countrycode',$BankInfo->countryName)->where('statecode',$BankInfo->stateName)->where('languagecode',$language)->value('statename')){
                if(!$bankData["stateName"]  = \DB::table('city')->where('statecode',$BankInfo->stateName)->where('citycode',$BankInfo->cityName)->where('languagecode',$language)->value('cityname')){
                    $bankData["stateName"] = \DB::table('country')->where('countrycode',$BankInfo->countryName)->where('languagecode',$language)->value('countryname');
                }
            }

            if(!$bankData["cityName"]  = \DB::table('city')->where('statecode',$BankInfo->stateName)->where('citycode',$BankInfo->cityName)->where('languagecode',$language)->value('cityname')){
                $bankData["cityName"] = \DB::table('country')->where('countrycode',$BankInfo->countryName)->where('languagecode',$language)->value('countryname');
            }

           

        }else{

            $bankData["bank_no"] = $this->CheckingService->CheckSpace($data["bank_no"]);
            $bankData["withdraval_type"] = $data["withdraval_type"];

            if( $data["withdraval_type"] == 1){ //银联
                $bankData["sub_bank"] = $this->CheckingService->CheckSpace($data["sub_bank"]);
                //$bankData["bank_name"] = $this->CheckingService->CheckSpace($data["bank_name"]);
                $bankData["bank_name"] = \DB::table('bank_list')->where('bank_value',$data["bank_name"])->value('bank_name');
                $bankData["swift"] = null;

                $insertData["paybankcode"] = $bankData["bank_name"]; // just select bankname have code
            }else{ //电汇
                $bankData["swift"] = $this->CheckingService->CheckSpace($data["swift"]);
                $bankData["bank_name"] = $this->CheckingService->CheckSpace($data["b_name"]);
                $bankData["sub_bank"] = null;
            }
            $bankData["member_id"] = $member->id;
            $bankData["withdraval_type"] = $this->CheckingService->CheckSpace($data["withdraval_type"]);
            $bankData["bank_address"] = $this->CheckingService->CheckSpace($data["bank_address"]);
            $bankData["countryName"] = $this->CheckingService->CheckSpace($data["countryName"]);
            $bankData["stateName"] = $this->CheckingService->CheckSpace($data["stateName"]);
            $bankData["cityName"] = $this->CheckingService->CheckSpace($data["cityName"]);
            $bankData["withdraval_type"] = $data["withdraval_type"];

            if(($data["checkSave"]==1)){ //need to save bank account

                if(!$this->BankInfoRepository->findByNo($bankData["bank_no"])){

                    $this->BankInfoRepository->store($bankData);
                }else{
                    return \Response::json([
                        'type'  =>  'error',
                        'message'   => \Lang::get('withdraw.hadAcc')
                    ]);
                }
            }

            $bankData["countryName"] = \DB::table('country')->where('countrycode',$data["countryName"])->where('languagecode',$language)->value('countryname');
            $bankData["stateName"] = \DB::table('state')->where('countrycode',$data["countryName"])->where('statecode',$data["stateName"])->where('languagecode',$language)->value('statename');
            $bankData["cityName"] = \DB::table('city')->where('statecode',$data["stateName"])->where('citycode',$data["cityName"])->where('languagecode',$language)->value('cityname');
            
            $bankData["countryName"] = \DB::table('country')->where('countrycode',$data["countryName"])->where('languagecode',$language)->value('countryname');
            if(!$bankData["stateName"] = \DB::table('state')->where('countrycode',$data["countryName"])->where('statecode',$data["stateName"])->where('languagecode',$language)->value('statename')){
                if(!$bankData["stateName"]  = \DB::table('city')->where('statecode',$data["stateName"])->where('citycode',$data["cityName"])->where('languagecode',$language)->value('cityname')){
                    $bankData["stateName"] = \DB::table('country')->where('countrycode',$data["countryName"])->where('languagecode',$language)->value('countryname');
                }
            }

            if(!$bankData["cityName"]  = \DB::table('city')->where('statecode',$data["stateName"])->where('citycode',$data["countryName"])->where('languagecode',$language)->value('cityname')){
                $bankData["cityName"] = \DB::table('country')->where('countrycode',$data["countryName"])->where('languagecode',$language)->value('countryname');
            }
        }

        if(!$GetPayId = $this->DepositRepository->storeDeposit($insertData)){
            return \Response::json([
                        'type'  =>  'error',
                        'message'   => \Lang::get('withdraw.insertLogFail')
            ]);
        }

        $insertPayData["PayId"] = $GetPayId->id;
        $insertPayData["card_no"] = $bankData["bank_no"];
        $insertPayData["bank_Name"] = $bankData["bank_name"];
        $insertPayData["bank_address"] = $bankData["bank_address"];
        $insertPayData["bank_swift"] = $bankData["swift"];
        $insertPayData["zbank_name"] = $bankData["sub_bank"];
        $insertPayData["bank_memo"] = $data["remark"];
        $insertPayData["rec_Name"] = $user->first_name;
        $insertPayData["Countries"] = $bankData["countryName"];
        $insertPayData["Province"] = $bankData["stateName"];
        $insertPayData["City"] = $bankData["cityName"];
        $insertPayData["WithdrawalMethod"] = $bankData["withdraval_type"];
        $insertPayData["exchangeRate"] = 0;

        
        if(!$model = $this->DepositRepository->storePay($insertPayData)){
            return \Response::json([
                        'type'  =>  'error',
                        'message'   => \Lang::get('withdraw.insertLogFail')
            ]);
        }
        

        return \Response::json([
            'type'  =>  'success',
            'message'   => "申请出金成功",
            'redirect' => 'withdrawRecords'
        ]);
    }
    public function showBankInfo (Request $request) {

        if(!empty($request->id)){

            $id = $request->id;
            $language = $request->language;
            $user = \Sentinel::getUser();
            $member = $this->MemberRepository->findByUsername(trim($user->username));
            $BankInfo = $this->BankInfoRepository->findById($id,$member->id);
            
            $country = \DB::table('country')->where('countrycode',$BankInfo->countryName)->where('languagecode',$language)->value('countryname');

            if(!$state = \DB::table('state')->where('countrycode',$BankInfo->countryName)->where('statecode',$BankInfo->stateName)->where('languagecode',$language)->value('statename')){
                if(!$state  = \DB::table('city')->where('statecode',$BankInfo->stateName)->where('citycode',$BankInfo->cityName)->where('languagecode',$language)->value('cityname')){
                    $state = \DB::table('country')->where('countrycode',$BankInfo->countryName)->where('languagecode',$language)->value('countryname');
                }
            }

            if(!$city  = \DB::table('city')->where('statecode',$BankInfo->stateName)->where('citycode',$BankInfo->cityName)->where('languagecode',$language)->value('cityname')){
                $city = \DB::table('country')->where('countrycode',$BankInfo->countryName)->where('languagecode',$language)->value('countryname');
            }

            $BankInfo->countryName = $country;
            $BankInfo->stateName = $state;
            $BankInfo->cityName = $city;
            
           
            return response()->json(['data'=>$BankInfo,'ans'=>1]);

        }else{

            return response()->json(['ans'=>0]);
        }   
    }
    public function getBankInfo (Request $request) {

        $withdraval_type = $request->withdraval_type;
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        

        $BankInfo = $this->BankInfoRepository->findBankInfo($member->id,$withdraval_type);
        $data = view('front.withdrawal.selectBankInfo',compact('BankInfo'))->render();
        return response()->json(['options'=>$data]);   
    }
    public function getState (Request $request) {

        $countrycode = $request->countryname;
        $language = $request->language;

        if(!$state = \DB::table('state')->where('countrycode',$countrycode)->where('languagecode',$language)->pluck("statename","statecode")->all()){
            if(!$state  = \DB::table('city')->where('countrycode',$countrycode)->where('languagecode',$language)->pluck("cityname","citycode")->all()){
                $state = \DB::table('country')->where('countrycode',$countrycode)->where('languagecode',$language)->pluck("countryname","countrycode")->all();
            }
        }
        $data = view('front.settings.selectState',compact('state'))->render();
        $city = view('front.settings.selectCity',compact('state'))->render();
        return response()->json(['options'=>$data,'city'=>$city]);   
    }
    public function getCity (Request $request) {

        $statecode = $request->statename;
        $language = $request->language;

        if(!$city = \DB::table('city')->where('statecode',$statecode)->where('languagecode',$language)->pluck("cityname","citycode")->all()){
            $city = \DB::table('country')->where('countrycode',$statecode)->where('languagecode',$language)->pluck("countryname","countrycode")->all();
        }

        $data = view('front.settings.selectCity',compact('city'))->render();
        return response()->json(['options'=>$data]);
    }
    public function cancel (Request $request) {

        $payID = $request->id;
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        if(!$model = $this->DepositRepository->findById($payID,$member->id)){
            $result = "fail";
        }
        if($model->status == "1"){
            if(!$ans = $this->DepositRepository->cancelWithdraw($payID,$member->id)){
                $result = "fail";
            }
            $result = "sucess";
        }else{
            $result = "fail";
        }


        return response()->json(['model'=>$model,'result'=>$result]); 
    }
    public function WithdrawalRecords () {


        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        return $this->DepositRepository->getWithdrawList(2,$member->id);
    }



    // =====================================================================FOr Jacky only ====================//
    public function postMakeWithdraw () { //weilun

        $data = \Input::get('data');
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        if ($member->secret_password != trim($data['s'])) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  \Lang::get('error.securityPasswordError')
            ]);
        }


        if($data["payment_type"]=="bank"){

            if (empty($data["bank_name"]) || empty($data["bank_account_holder"]) || empty($data["bank_account_number"])
                || empty($data["bank_swiftcode"])|| empty($data["bank_address"])|| empty($data["bank_country"])){

                return \Response::json([
                    'type'  =>  'error',
                    'message'   =>  \Lang::get('error.bankError')
                ]);

            }else{

                $detail = $member->detail; 
                foreach ($detail->getAttributes() as $k => $d) {
                    if (isset($data[$k])) $detail->{$k} = $data[$k];
                }
                $detail->save(); //update tb_member_detail bank info

                try {   
                    $this->WithdrawRepository->makeWithdraw($member, $data['amount'] ,$data["payment_type"],'');
                } catch (\Exception $e) {
                    return \Response::json([
                        'type'  =>  'error',
                        'message'   =>  $e->getMessage()
                    ]);
                }

            }

        }else if($data["payment_type"]=="debit"){

                

            if (empty($data["v_cardname"]) || empty($data["v_country"]) ){


                return \Response::json([
                    'type'  =>  'error',
                    'message'   =>  \Lang::get('error.debitError')
                ]);

            }else{

                $debit_data = ['card_number' => $data["v_cardname"],'card_country'=>$data["v_country"]];


                try {   
                    $this->WithdrawRepository->makeWithdraw($member, $data['amount'] ,$data["payment_type"], $debit_data);
                } catch (\Exception $e) {
                    return \Response::json([
                        'type'  =>  'error',
                        'message'   =>  $e->getMessage()
                    ]);
                }
            }

        }else{

            return \Response::json([
                    'type'  =>  'error',
                    'message'   => \Lang::get('error.paymentError')
                ]);

        }

        \Cache::forget('member.' . $user->id);

        return \Response::json([
            'type'  =>  'success',
            'message'   =>  \Lang::get('message.withdrawSuccess'),
            'redirect' => route('transaction.withdrawSuccess', ['lang' => \App::getLocale()])
           ]);

    }

    /**
     * Withdraw list - DataTable
     * @return [type] [description]
     */
    public function getList () {
        $user = \Sentinel::getUser();
        $member = $user->member;
        return $this->WithdrawRepository->getList($member);
    }
}
