<?php

namespace App\Http\Controllers;
use Mail;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use App\Jobs\NetworkAfterRegisterJob;
use App\Jobs\SharesAfterRegisterJob;
use App\Repositories\MemberRepository;
use App\Repositories\PackageRepository;
use App\Repositories\SharesRepository;
use App\Repositories\BonusRepository;
use App\Repositories\HelpdeskRepository;
use App\Repositories\TransferRepository;
use Yajra\Datatables\Facades\Datatables;
use App\Services\AmazonS3;
use App\Services\CheckingService;
use App\Services\UploadpicService;
use Validator;
use DB;
use Carbon\Carbon;
use Uuid;

    

class CapxController extends Controller
{
    /**
     * The MemberRepository instance.
     *
     * @var \App\Repositories\MemberRepository
     */
    protected $MemberRepository;

    /**
     * The PackageRepository instance.
     *
     * @var \App\Repositories\PackageRepository
     */
    protected $PackageRepository;
    
    /**
     * The SharesRepository instance.
     *
     * @var \App\Repositories\SharesRepository
     */
    protected $SharesRepository;
    
    protected $BonusRepository;
    
    protected $HelpdeskRepository;
    protected $TransferRepository;


    protected $AmazonS3;
    protected $CheckingService;
    protected $UploadpicService;

    /**
     * Create a new MemberController instance.
     *
     * @param \App\Repositories\MemberRepository $MemberRepository
     * @return void
     */
    public function __construct(
        MemberRepository $MemberRepository,
        PackageRepository $PackageRepository,
        SharesRepository $SharesRepository,
        BonusRepository $BonusRepository,
        HelpdeskRepository $HelpdeskRepository,
        AmazonS3 $AmazonS3,
        CheckingService $CheckingService,
        TransferRepository $TransferRepository,
        UploadpicService $UploadpicService
    ) {
        $this->MemberRepository = $MemberRepository;
        $this->PackageRepository = $PackageRepository;
        $this->SharesRepository = $SharesRepository;
        $this->BonusRepository = $BonusRepository;
        $this->HelpdeskRepository = $HelpdeskRepository;
        $this->AmazonS3 = $AmazonS3;
        $this->CheckingService = $CheckingService;
        $this->UploadpicService = $UploadpicService;
        $this->TransferRepository = $TransferRepository;

        $this->middleware('member', ['except' => []]);
    }
    

    public function registration () {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        if($member->detail->nationality == 'CN' || $member->detail->nationality == 'China'){
            $data = [
                        'isChina' => 1,
                    ];
        }
        else{
            $data = [
                        'isChina' => 0
                    ];
        }
        return view('front.capx.registration')->with($data);
    }

    public function loginRedirect () {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        if($member->username !="seanetlead@gmail.com"){
            if($member->register_by!="seanetlead@gmail.com"){
                $leader = \DB::table('Member_Network')->where('parent_username', 'seanetlead@gmail.com')->where('username', '=',$member->register_by)->first();
                if(!empty($leader)){
                    if($member->detail->nationality!="China"){
                        $member->customerGroup = "b";
                    }else{
                        $member->customerGroup = "c";
                    }
                }else{
                    $member->customerGroup = "a";
                }
            }else{
                if($member->detail->nationality!="China"){
                    $member->customerGroup = "b";
                }else{
                    $member->customerGroup = "c";
                } 
            }
        }
        else{
            $member->customerGroup = "b";
        }

        $rank = 'BASIC';
        $ranking = \DB::table('Member_Sales_Summary')->where('member_id', $member->id)->orderBy('date', 'DESC')->first();
        if(isset($ranking)){
            if($ranking->rank) $rank = $ranking->rank;
        }

        if(empty($user->linked_id)){
            $user->linked_id = uniqid(8);
            $user->save();
            $result = $this->CheckingService->CapxRegister($member,$user);
            if($result->statusCode == 0){
                $result = $this->CheckingService->loginRedirect($member, $user, $rank);
                return redirect($result->resultDesc);
            }else{
                $user->linked_id = null;
                $user->save();
                return view('front.capx.capxfail');
            }
        }else{
            $result = $this->CheckingService->loginRedirect($member, $user, $rank);
            if($result->statusCode == 0){
                return redirect($result->resultDesc);
            }else{
                return view('front.capx.capxfail');
            }
        }
    }

    public function transferToCapx () {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        if(empty($user->linked_id)){
            $user->linked_id = uniqid(8);
            $user->save();
            $result = $this->CheckingService->CapxRegister($member,$user);
            if($result->statusCode == 0){
                return view('front.capx.transfer')->with('member',$member);
            }else{
                $user->linked_id = null;
                $user->save();
                return view('front.capx.capxfail');
            }
        }else{
            return view('front.capx.transfer')->with('member',$member)->with('user',$user);
        }
    }

    public function postTransferToCapx (Request $request) {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $getData = $request->Data;
        $data = json_decode($getData,true); //$data["Data"][0]["getPassword1"] example
        $amount = $data["Data"]["amount"];
        $SPassword = $request->SPassword;

        if ($member->secret_password != $SPassword) {
            return response()->json(['SPassword'=>"1"]);
        }

        if ( $amount <= 0) {
            $tittle = \Lang::get('capx.invalid_amount');
            $text ="";
            return response()->json(['result'=>"1","showTittle"=>$tittle]);
        }

        if ($member->wallet->cash_point < $amount) {
                $tittle = \Lang::get('error.cashPointNotEnough');
                $text ="";
                return response()->json(['result'=>"1","showTittle"=>$tittle]);
        }

// =================================
        $uuid = Uuid::generate();
        $checkRequest = DB::table('Member_Transaction_Log')->where('member_id', $member->id)
                            ->where(function ($query) {
                                $query->whereNull('updated_at')
                                    ->orWhere('updated_at', '<>', \Carbon\Carbon::now());
                                })
                            ->update([ 'updated_at' => \Carbon\Carbon::now() ]);
        if(!$checkRequest){
            DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postTransferToCapx', 'created_at' => \Carbon\Carbon::now()]);
            $tittle = "Too Many Request...";
            $text = "";
            return response()->json(['result'=>"1","showTittle"=>$tittle]);
        }

        $updateLog = DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 0)->update(['transaction_id' => $uuid->string, 'route' => 'postTransferToCapx', 'status' => 1, 'updated_at' => \Carbon\Carbon::now()]);
        if(!$updateLog){
            DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postTransferToCapx', 'created_at' => \Carbon\Carbon::now()]);
            $tittle = "Oops...";
            $text = "";
            $html = \Lang::get('error.helpDesk');
            return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"showHtml"=>$html]);
        }
        sleep(1);

        $checklog = DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('transaction_id', $uuid->string)->count();
        if($checklog){
        
// =================================
            $walletQuery = DB::table('Member_Wallet')->where('member_id', $member->id);
            $walletQuery->decrement('cash_point',$amount);

            if($walletQuery->first(['cash_point'])->cash_point < 0){

                DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);
                
                $member->wallet->cash_point += $amount;
                $member->wallet->save();
                $tittle = \Lang::get('error.cashPointNotEnough');
                $text ="";
                return response()->json(['result'=>"1","showTittle"=>$tittle]);

            }else{
                try {
                    $result = $this->CheckingService->transferToCapx($member, $user, $amount);
                    if($result->statusCode == 0){

                        $title = 'Transfer to CAPX';
                        $remark = 'Debited '.$amount.' for transfer to CAPX ';

                        $remarkAry = array('amount'=>$amount);
                        $remark_display = 'TH00078/'.json_encode($remarkAry);

                        $this->TransferRepository->saveWalletStatement($member->id,'',$amount,$title,'C', 'D',$walletQuery->first(['cash_point'])->cash_point,$remark,$remark_display,'');

                        // update balance
                        $symbol = 'usd';
                        $result = $this->CheckingService->getBalance($member,$user);
                        if($result->statusCode == 0){
                            foreach ($result->result as $item) {
                                $coinSymbol = strtolower($item->coin);
                                if($coinSymbol == $symbol){
                                    if(floatval($member->coin->usd) <> floatval($item->amount)){
                                        DB::table('Capx_Member_Coin')->where('member_id', $member->id)->update([$symbol => $item->amount]);
                                    }
                                    break;
                                }
                            }
                        }

                        DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);

                        $tittle = \Lang::get('message.transferSuccess');
                        $text = \Lang::get('message.transferCAPXSuccess');
                        return response()->json(['result'=>"0","showTittle"=>$tittle,"showText"=>$text,'returnUrl'=>route('capx.transferToCapx', ['lang' => \App::getLocale()])]);

                    }else{

                        DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);

                        $title = 'Transfer to CAPX';
                        $remark = 'Debited '.$amount.' for transfer to CAPX ';

                        $remarkAry = array('amount'=>$amount);
                        $remark_display = 'TH00078/'.json_encode($remarkAry);

                        $this->TransferRepository->saveWalletStatement($member->id,'',$amount,$title,'C', 'D',$walletQuery->first(['cash_point'])->cash_point,$remark,$remark_display,'');

                        DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);


                        $tittle = \Lang::get('message.transferFail');
                        $text = \Lang::get('message.transferCAPXFail');
                        return response()->json(['result'=>"1","showTittle"=>$tittle,"showText"=>$text,'returnUrl'=>""]);
                    }

                } catch (\Exception $e) {
                    DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);

                    $tittle = "Oops...";
                    $text = "";
                    $html = \Lang::get('error.helpDesk');
                    return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"showHtml"=>$html]);
                }
            }
        }else{
            DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'postTransferToCapx', 'created_at' => \Carbon\Carbon::now()]);
        }
    }

    public function mytrcap () {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $coinUnit = $member->coin->trc;
        $symbol = 'trc';

        $date = date('Y-m-01 H:i:s');
        
        $oldStartDate = date('Y-m-01 00:00:00', strtotime($date. "-1 month"));
        $oldEndDate = date('Y-m-01 00:00:00', strtotime($date));
        $oldPoolAmount = DB::table('Capx_Pool')->where('created_at', '>=', $oldStartDate)->where('created_at', '<', $oldEndDate)->sum('amount');
        
        $startDate = date('Y-m-01 00:00:00', strtotime($date));
        $endDate = date('Y-m-01 00:00:00', strtotime($date. "+1 month"));
        $poolAmount = DB::table('Capx_Pool')->where('created_at', '>=', $startDate)->where('created_at', '<', $endDate)->sum('amount');

        $coin = DB::table('Capx_Coin')->where('symbol', $symbol)->first();

        if(isset($user->linked_id)){
            $result = $this->CheckingService->getBalance($member,$user);
            if($result->statusCode == 0){
                foreach ($result->result as $item) {
                    if($item->coin == $symbol){
                        if(floatval($coinUnit) <> floatval($item->amount)){
                            $coinUnit = $item->amount;
                            DB::table('Capx_Member_Coin')->where('member_id', $member->id)->update([$symbol => $item->amount]);
                        }
                        break;
                    }
                }
            }
        }

        $data = [
                    'oldPoolAmount' => $oldPoolAmount,
                    'poolAmount' => $poolAmount,
                    'coinUnit' => $coinUnit,
                    'coin' => $coin,
                ];
        return view('front.capx.mytrcap')->with($data);
    }

    public function onsalescap () {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        $coin = DB::table('Capx_Coin')->where('type', 0)->first();

        $rank = DB::table('Member_Sales_Summary')->where('member_id', $member->id)->orderBy('date', 'DESC')->first();
        
        $maxSubscribe = '';
        $ranking = '';

        $conditions2 = json_decode($rank->conditions2);
        if(in_array(7, $conditions2)){
            if(in_array($member->id, config('autosettings.bypasstrcapcheck'))){
                $maxSubscribe = \Lang::get('capx.nolimit');
                $ranking = $rank->rank.' ('.\Lang::get('capx.networkPIB').')';
            }else{
                $currentMonth = $rank->date;
                $realNetwork = DB::table('Member_Sales_Summary')->join('Member', 'Member_Sales_Summary.member_id', '=', 'Member.id')
                            ->where('Member.direct_id', $member->id)
                            ->whereIn('Member_Sales_Summary.rank', ['MIB','PIB'])
                            ->where('conditions2', 'LIKE', '%6%')
                            ->where('date', $currentMonth)
                            ->count();

                if($realNetwork >= 6){
                    $maxSubscribe = \Lang::get('capx.nolimit');
                    $ranking = $rank->rank.' ('.\Lang::get('capx.networkPIB').')';
                }else{
                    $maxSubscribe = config('capx.maxTrcap.MIB')-$member->coin->trc;
                    $maxSubscribe = $maxSubscribe <= 0 ? 0 : number_format($maxSubscribe);
                    $ranking = $rank->rank.' ('.\Lang::get('capx.networkMIB').')';
                }
            }
        }elseif(in_array(6, $conditions2)){
            $maxSubscribe = config('capx.maxTrcap.MIB')-$member->coin->trc;
            $maxSubscribe = $maxSubscribe <= 0 ? 0 : number_format($maxSubscribe);
            $ranking = $rank->rank.' ('.\Lang::get('capx.networkMIB').')';
        }else{
            $maxSubscribe = config('capx.maxTrcap.IB')-$member->coin->trc;
            $maxSubscribe = $maxSubscribe <= 0 ? 0 : number_format($maxSubscribe);
            $ranking = $rank->rank.' ('.\Lang::get('capx.networkIB').')';
        }

        $soldout = DB::table('Capx_Member_Coin')->sum('trc');

        $data = [
                    'coin' => $coin,
                    'rank' => $ranking,
                    'soldout' => $soldout,
                    'maxSubscribe' => $maxSubscribe,
                    'cashBalance' => $member->wallet->cash_point
                ];
        return view('front.capx.onsalescap')->with($data);
    }

    public function trcap3level () {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        $coin = DB::table('Capx_Coin')->where('type', 0)->first();
        $symbol = $coin->symbol;
        $temp = [];

        $level1member = 0;
        $level1amount = 0;
        $level1 = DB::table('Member_Network')->join('Capx_Member_Coin', 'Member_Network.member_id', '=', 'Capx_Member_Coin.member_id')->where('parent_id', $member->id)->where('my_level', 1)->get(['Capx_Member_Coin.'.$symbol]);
        if(count($level1) > 0){
            foreach ($level1 as $item) {
                if($item->$symbol > 0) {
                    $level1member++;
                    $level1amount += $item->$symbol;
                }
            }
        }
        $temp['level1member'] = $level1member;
        $temp['level1amount'] = $level1amount;

        $level2member = 0;
        $level2amount = 0;
        $level2 = DB::table('Member_Network')->join('Capx_Member_Coin', 'Member_Network.member_id', '=', 'Capx_Member_Coin.member_id')->where('parent_id', $member->id)->where('my_level', 2)->get(['Capx_Member_Coin.'.$symbol]);
        if(count($level2) > 0){
            foreach ($level2 as $item) {
                if($item->$symbol > 0) {
                    $level2member++;
                    $level2amount += $item->$symbol;
                }
            }
        }
        $temp['level2member'] = $level2member;
        $temp['level2amount'] = $level2amount;

        $level3member = 0;
        $level3amount = 0;
        $level3 = DB::table('Member_Network')->join('Capx_Member_Coin', 'Member_Network.member_id', '=', 'Capx_Member_Coin.member_id')->where('parent_id', $member->id)->where('my_level', 3)->get(['Capx_Member_Coin.'.$symbol]);
        if(count($level3) > 0){
            foreach ($level3 as $item) {
                if($item->$symbol > 0) {
                    $level3member++;
                    $level3amount += $item->$symbol;
                }
            }
        }
        $temp['level3member'] = $level3member;
        $temp['level3amount'] = $level3amount;

        $totalmember = $level1member + $level2member + $level3member;
        $totalamount = $level1amount + $level2amount + $level3amount;

        $data = [
                    'level1member' => $level1member,
                    'level1amount' => $level1amount,
                    'level2member' => $level2member,
                    'level2amount' => $level2amount,
                    'level3member' => $level3member,
                    'level3amount' => $level3amount,
                    'totalmember' => $totalmember,
                    'totalamount' => $totalamount
                ];
        return view('front.capx.trcap3level')->with($data);
    }

    public function subscribetrc (Request $request) {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        $SPassword = $request->SPassword;
        $getData = $request->Data;
        $data = json_decode($getData,true); //$data["Data"]["getPassword1"] example
        $amount = $data['Data']['amount'];
        
        if ($member->secret_password != $SPassword) {
            $response = array('SPassword' => 1);
            return response()->json($response);
        }

        if (empty($amount) || $amount < 0 || filter_var($amount, FILTER_VALIDATE_INT) === false) {
            $response = array('result' => 1, 'showTittle' => \Lang::get('capx.errorAmount'), 'showText' => '');
            return response()->json($response);
        }

        $subscribeAmount = $amount * 500;
        $maxSubscribe = 0;

        $rank = DB::table('Member_Sales_Summary')->where('member_id', $member->id)->orderBy('date', 'DESC')->first();

        if($rank->rank == ''){
            $response = array('result' => 1, 'showTittle' => 'Please contact customer service for help', 'showText' => '');
            return response()->json($response);
        }

        $conditions2 = json_decode($rank->conditions2);
        if(in_array(7, $conditions2)){
            if(in_array($member->id, config('autosettings.bypasstrcapcheck'))){
                $maxSubscribe = 1000000000;
            }else{
                $currentMonth = $rank->date;
                $realNetwork = DB::table('Member_Sales_Summary')->join('Member', 'Member_Sales_Summary.member_id', '=', 'Member.id')
                            ->where('Member.direct_id', $member->id)
                            ->whereIn('Member_Sales_Summary.rank', ['MIB','PIB'])
                            ->where('conditions2', 'LIKE', '%6%')
                            ->where('date', $currentMonth)
                            ->count();

                if($realNetwork >= 6){
                    $maxSubscribe = 1000000000;
                }else{
                    $maxSubscribe = config('capx.maxTrcap.MIB')-$member->coin->trc;
                }
            }
        }elseif(in_array(6, $conditions2)){
            $maxSubscribe = config('capx.maxTrcap.MIB')-$member->coin->trc;
        }else{
            $maxSubscribe = config('capx.maxTrcap.IB')-$member->coin->trc;
        }

        if($maxSubscribe <= 0 || $subscribeAmount > $maxSubscribe){
            $response = array('result' => 1, 'showTittle' => \Lang::get('capx.errorMax'), 'showText' => '');
            return response()->json($response);
        }

        $coinPrice = DB::table('Capx_Coin')->where('symbol', 'trc')->first();
        $price = $subscribeAmount * $coinPrice->price;

        if($member->wallet->cash_point < $price){
            $response = array('result' => 1, 'showTittle' => \Lang::get('error.cashPointNotEnough'), 'showText' => '');
            return response()->json($response);
        }

        $uuid = Uuid::generate();
        $checkRequest = DB::table('Member_Transaction_Log')->where('member_id', $member->id)
                        ->where(function ($query) {
                            $query->whereNull('updated_at')
                                ->orWhere('updated_at', '<>', \Carbon\Carbon::now());
                            })
                        ->update([ 'updated_at' => \Carbon\Carbon::now() ]);
        if(!$checkRequest){
            DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'subscribetrc1', 'created_at' => \Carbon\Carbon::now()]);
            $tittle = "Too Many Request...";
            $text = "";
            return response()->json(['result'=>"1","showTittle"=>$tittle]);
        }

        $updateLog = DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 0)->update(['transaction_id' => $uuid->string, 'route' => 'subscribetrc', 'status' => 1, 'updated_at' => \Carbon\Carbon::now()]);
        if(!$updateLog){
            DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'subscribetrc2', 'created_at' => \Carbon\Carbon::now()]);
            $tittle = "Oops...";
            $text = "";
            $html = \Lang::get('error.helpDesk');
            return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"showHtml"=>$html]);
        }
        sleep(1);

        $checklog = DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('transaction_id', $uuid->string)->count();
        if($checklog){
            $remarkAry = array('coin' => 'TRCAP');
            $remark_display = 'TH00075/'.json_encode($remarkAry);

            $walletQuery = DB::table('Member_Wallet')->where('member_id', $member->id);
            $walletQuery->decrement('cash_point', $price);
            $cashBalance = $walletQuery->first(['cash_point'])->cash_point;

            DB::table('Member_Wallet_Statement')->insert([
                                                            'member_id' => $member->id,
                                                            'username' => $member->username,
                                                            'cash_amount' => $price,
                                                            'action_type' => 'Buy trc',
                                                            'wallet_type' => 'C',
                                                            'transaction_type' => 'D',
                                                            'balance' =>  $cashBalance,
                                                            'remark' => 'Buy TRCAP',
                                                            'created_at' => \Carbon\Carbon::now(),
                                                            'updated_at' => \Carbon\Carbon::now(),
                                                            'remark_display' => $remark_display
                                                        ]);

            if($cashBalance >= 0){
                $member->coin->trc += $subscribeAmount;
                $member->coin->save();

                DB::table('Capx_Member_Coin_Statement')->insert([
                                                                    'member_id' => $member->id,
                                                                    'symbol' => 'trc',
                                                                    'action_type' => 'Buy trc',
                                                                    'transaction_type' => 'C',
                                                                    'unit' => $subscribeAmount,
                                                                    'price' => $coinPrice->price,
                                                                    'balance' => $member->coin->trc,
                                                                    'created_at' => \Carbon\Carbon::now(),
                                                                    'updated_at' => \Carbon\Carbon::now()
                                                                ]);
            }
        }else{
            DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'subscribetrc3', 'created_at' => \Carbon\Carbon::now()]);
        }
        DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);

        $response = array('result' => 0, 'showTittle' => \Lang::get('capx.subscribeSuccess'), 'showText' => '', 'returnUrl' => route('capx.mytrcap', ['lang' => \App::getLocale()]));
        return response()->json($response);
    }

    public function mymargin () {
        $margin = DB::table('Capx_Margin')->get();

        $data = [
                    'margin' => $margin
                ];
        return view('front.capx.mymargin')->with($data);
    }

    public function getMyMargin (Request $request) {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $symbol = $request->symbol;
        $my_unit = $member->coin->$symbol;

        if(isset($user->linked_id)){
            $result = $this->CheckingService->getBalance($member,$user);
            if($result->statusCode == 0){
                foreach ($result->result as $item) {
                    $coinSymbol = strtolower($item->coin);
                    if($coinSymbol == $symbol){
                        if(floatval($my_unit) <> floatval($item->amount)){
                            $my_unit = $item->amount;
                            DB::table('Capx_Member_Coin')->where('member_id', $member->id)->update([$symbol => $item->amount]);
                        }
                        break;
                    }
                }
            }
        }

        $margin = DB::table('Capx_Margin')->where('symbol', $symbol)->first();
        $temp = [];

        foreach ($margin as $key => $value) {
            $temp[$key] = $value;
        }
        $temp['my_unit'] = $my_unit;

        $response = array('status' => 0, 'msg' => '', 'details' => $temp);
        return response()->json($response);
    }

    public function onsales () {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        $margin = DB::table('Capx_Margin')->where('status', 1)->orderBy('id', 'DESC')->get();

        $data = [
                    'margin' => $margin,
                    'cashBalance' => $member->wallet->cash_point
                ];
        return view('front.capx.onsales')->with($data);
    }

    public function getAllMargin () {
        $margin = DB::table('Capx_Margin')->where('status', 1)->get();

        $response = array('status' => 0, 'msg' => '', 'margin' => $margin);
        return response()->json($response);
    }

    public function subscribeMargin (Request $request) {

        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));

        $SPassword = $request->SPassword;
        $getData = $request->Data;
        $data = json_decode($getData,true); //$data["Data"]["getPassword1"] example
        $symbol = $data['Data']['symbol'];
        $amount = $data['Data']['amount'];
        
        if ($member->secret_password != $SPassword) {
            $response = array('SPassword' => 1);
            return response()->json($response);
        }

        if (empty($symbol)){
            $response = array('result' => 1, 'showTittle' => "Don't hack our system. :)", 'showText' => '');
            return response()->json($response);
        }

        if (empty($amount) || $amount < 0 || filter_var($amount, FILTER_VALIDATE_INT) === false) {
            $response = array('result' => 1, 'showTittle' => \Lang::get('capx.errorAmount'), 'showText' => '');
            return response()->json($response);
        }

        $subscribeAmount = $amount * 500;

        $margin = DB::table('Capx_Margin')->where('symbol', $symbol)->where('launched_date', '<=', date('Y-m-d H:i:s'))->where('expired_date', '>=', date('Y-m-d H:i:s'))->first();

        if(!isset($margin)){
            $response = array('result' => 1, 'showTittle' => "Don't hack our system. :)", 'showText' => '');
            return response()->json($response);
        }

        if($subscribeAmount > $margin->current_unit){
            $response = array('result' => 1, 'showTittle' => \Lang::get('capx.errorBalance'), 'showText' => '');
            return response()->json($response);
        }

        $price = $subscribeAmount * $margin->current_price;

        if($member->wallet->cash_point < $price){
            $response = array('result' => 1, 'showTittle' => \Lang::get('error.cashPointNotEnough'), 'showText' => '');
            return response()->json($response);
        }

        DB::table('Capx_Margin')->where('symbol', $symbol)->decrement('current_unit', $subscribeAmount);
        
        // $marginNew = DB::table('Capx_Margin')->where('symbol', $symbol)->where('launched_date', '<=', date('Y-m-d H:i:s'))->where('expired_date', '>=', date('Y-m-d H:i:s'))->first();

        // if($marginNew->current_unit < 0){
        //     DB::table('Capx_Margin')->where('symbol', $symbol)->increment('current_unit', $subscribeAmount);
        //     $response = array('result' => 1, 'showTittle' => \Lang::get('capx.errorBalance'), 'showText' => '');
        //     return response()->json($response);
        // }

        $uuid = Uuid::generate();
        $checkRequest = DB::table('Member_Transaction_Log')->where('member_id', $member->id)
                        ->where(function ($query) {
                            $query->whereNull('updated_at')
                                ->orWhere('updated_at', '<>', \Carbon\Carbon::now());
                            })
                        ->update([ 'updated_at' => \Carbon\Carbon::now() ]);
        if(!$checkRequest){
            DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'subscribeMargin1', 'created_at' => \Carbon\Carbon::now()]);
            $tittle = "Too Many Request...";
            $text = "";
            return response()->json(['result'=>"1","showTittle"=>$tittle]);
        }

        $updateLog = DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 0)->update(['transaction_id' => $uuid->string, 'route' => 'subscribeMargin', 'status' => 1, 'updated_at' => \Carbon\Carbon::now()]);
        if(!$updateLog){
            DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'subscribeMargin2', 'created_at' => \Carbon\Carbon::now()]);
            $tittle = "Oops...";
            $text = "";
            $html = \Lang::get('error.helpDesk');
            return response()->json(['result'=>"2","showTittle"=>$tittle,"showText"=>$text,"showHtml"=>$html]);
        }
        sleep(1);

        $checklog = DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('transaction_id', $uuid->string)->count();
        if($checklog){
            $remarkAry = array('coin' => $symbol);
            $remark_display = 'TH00075/'.json_encode($remarkAry);

            $walletQuery = DB::table('Member_Wallet')->where('member_id', $member->id);
            $walletQuery->decrement('cash_point', $price);
            $cashBalance = $walletQuery->first(['cash_point'])->cash_point;

            DB::table('Member_Wallet_Statement')->insert([
                                                            'member_id' => $member->id,
                                                            'username' => $member->username,
                                                            'cash_amount' => $price,
                                                            'action_type' => 'Buy '.$symbol,
                                                            'wallet_type' => 'C',
                                                            'transaction_type' => 'D',
                                                            'balance' =>  $cashBalance,
                                                            'remark' => 'Buy'.$symbol,
                                                            'created_at' => \Carbon\Carbon::now(),
                                                            'updated_at' => \Carbon\Carbon::now(),
                                                            'remark_display' => $remark_display
                                                        ]);

            if($cashBalance >= 0){
                $member->coin->$symbol += $subscribeAmount;
                $member->coin->save();

                DB::table('Capx_Member_Coin_Statement')->insert([
                                                                    'member_id' => $member->id,
                                                                    'symbol' => $symbol,
                                                                    'action_type' => 'Buy '.$symbol,
                                                                    'transaction_type' => 'C',
                                                                    'unit' => $subscribeAmount,
                                                                    'price' => $margin->current_price,
                                                                    'balance' => $member->coin->$symbol,
                                                                    'created_at' => \Carbon\Carbon::now(),
                                                                    'updated_at' => \Carbon\Carbon::now()
                                                                ]);
            }
        }else{
            DB::table('Multitread_Log')->insert([ 'member_id' => $member->id, 'transaction_id' => $uuid->string, 'route' => 'subscribeMargin3', 'created_at' => \Carbon\Carbon::now()]);
        }
        DB::table('Member_Transaction_Log')->where('member_id', $member->id)->where('status', 1)->where('transaction_id', $uuid->string)->update(['status' => 0]);

        $response = array('result' => 0, 'showTittle' => \Lang::get('capx.subscribeSuccess'), 'showText' => '', 'returnUrl' => route('capx.mymargin', ['lang' => \App::getLocale()]));
        return response()->json($response);
    }

    public function margin3level () {
        $margin = DB::table('Capx_Margin')->get();

        $data = [
                    'margin' => $margin
                ];
        return view('front.capx.margin3level')->with($data);
    }

    public function get3levelmargin (Request $request) {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $symbol = $request->symbol;

        $margin = DB::table('Capx_Margin')->where('symbol', $symbol)->first();
        $temp = [];

        $level1member = 0;
        $level1amount = 0;
        $level1 = DB::table('Member_Network')->join('Capx_Member_Coin', 'Member_Network.member_id', '=', 'Capx_Member_Coin.member_id')->where('parent_id', $member->id)->where('my_level', 1)->get(['Capx_Member_Coin.'.$symbol]);
        if(count($level1) > 0){
            foreach ($level1 as $item) {
                if($item->$symbol > 0) {
                    $level1member++;
                    $level1amount += $item->$symbol;
                }
            }
        }
        $temp['level1member'] = $level1member;
        $temp['level1amount'] = $level1amount;

        $level2member = 0;
        $level2amount = 0;
        $level2 = DB::table('Member_Network')->join('Capx_Member_Coin', 'Member_Network.member_id', '=', 'Capx_Member_Coin.member_id')->where('parent_id', $member->id)->where('my_level', 2)->get(['Capx_Member_Coin.'.$symbol]);
        if(count($level2) > 0){
            foreach ($level2 as $item) {
                if($item->$symbol > 0) {
                    $level2member++;
                    $level2amount += $item->$symbol;
                }
            }
        }
        $temp['level2member'] = $level2member;
        $temp['level2amount'] = $level2amount;

        $level3member = 0;
        $level3amount = 0;
        $level3 = DB::table('Member_Network')->join('Capx_Member_Coin', 'Member_Network.member_id', '=', 'Capx_Member_Coin.member_id')->where('parent_id', $member->id)->where('my_level', 3)->get(['Capx_Member_Coin.'.$symbol]);
        if(count($level3) > 0){
            foreach ($level3 as $item) {
                if($item->$symbol > 0) {
                    $level3member++;
                    $level3amount += $item->$symbol;
                }
            }
        }
        $temp['level3member'] = $level3member;
        $temp['level3amount'] = $level3amount;

        $temp['totalmember'] = $level1member + $level2member + $level3member;
        $temp['totalamount'] = $level1amount + $level2amount + $level3amount;
        
        $response = array('status' => 0, 'msg' => '', 'details' => $temp);
        return response()->json($response);
    }

    public function getBalance () {
        $user = \Sentinel::getUser();
        $member = $this->MemberRepository->findByUsername(trim($user->username));
        $balance['cash'] = $member->wallet->cash_point;
        $balance['w'] = $member->wallet->w_wallet;
        $balance['income'] = floor($member->wallet->promotion_point * 100) / 100;
        $balance['t'] = $member->wallet->t_wallet;
        $balance['capx'] = $member->coin->usd;
        $symbol = 'usd';

        if(isset($user->linked_id)){
            $result = $this->CheckingService->getBalance($member,$user);
            if($result->statusCode == 0){
                foreach ($result->result as $item) {
                    $coinSymbol = strtolower($item->coin);
                    if($coinSymbol == $symbol){
                        if(floatval($balance['capx']) <> floatval($item->amount)){
                            $balance['capx'] = $item->amount;
                            DB::table('Capx_Member_Coin')->where('member_id', $member->id)->update([$symbol => $item->amount]);
                        }
                        break;
                    }
                }
            }
        }

        $response = array('status' => 0, 'msg' => '', 'balance' => $balance);
        return response()->json($response);
    }

}
