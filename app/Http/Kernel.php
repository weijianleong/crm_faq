<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
	/**
	 * The application's global HTTP middleware stack.
	 *
	 * These middleware are run during every request to your application.
	 *
	 * @var array
	 */
	protected $middleware = [
		// \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
		\App\Http\Middleware\MaintenanceMiddleware::class
	];

	/**
	 * The application's route middleware groups.
	 *
	 * @var array
	 */
	protected $middlewareGroups = [
		'web' => [
			\App\Http\Middleware\EncryptCookies::class,
			\Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
			\Illuminate\Session\Middleware\StartSession::class,
			\Illuminate\View\Middleware\ShareErrorsFromSession::class,
			\App\Http\Middleware\VerifyCsrfToken::class,
			\Illuminate\Routing\Middleware\SubstituteBindings::class,
		],

		'api' => [
			\App\Http\Middleware\EncryptCookies::class,
			\Illuminate\Session\Middleware\StartSession::class,
		],
	];

	/**
	 * The application's route middleware.
	 *
	 * These middleware may be assigned to groups or used individually.
	 *
	 * @var array
	 */
	protected $routeMiddleware = [
		'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
		'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
		'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
		'can' => \Illuminate\Auth\Middleware\Authorize::class,
		'locale'	=>	\App\Http\Middleware\LocaleMiddleware::class,
		'admin'	=>	\App\Http\Middleware\AdminMiddleware::class,
		'adminRight'	=>	\App\Http\Middleware\admin\AdminCheckRight::class,
		'adminHelpdesk'	=>	\App\Http\Middleware\admin\AdminCheckHelpdesk::class,
		'adminHome'	=>	\App\Http\Middleware\admin\AdminCheckHome::class,
		'adminProfile'	=>	\App\Http\Middleware\admin\AdminCheckProfile::class,
		'adminMember'	=>	\App\Http\Middleware\admin\AdminCheckMember::class,
		'adminMemberP'	=>	\App\Http\Middleware\admin\AdminCheckMemberP::class,
		'adminLpoa'	=>	\App\Http\Middleware\admin\AdminCheckLpoa::class,
		'adminProcess'	=>	\App\Http\Middleware\admin\AdminCheckProcess::class,
		'adminAnnouncement'	=>	\App\Http\Middleware\admin\AdminCheckAnnouncement::class,
		'adminWithdrawal'	=>	\App\Http\Middleware\admin\AdminCheckWithdrawal::class,
        'adminReply'    =>  \App\Http\Middleware\admin\AdminCheckReply::class,
		'adminDeposit'	=>	\App\Http\Middleware\admin\AdminCheckDeposit::class,
		'member'	=>	\App\Http\Middleware\MemberMiddleware::class,
		'checkMemberStatus' =>  \App\Http\Middleware\CheckMemberStatusMiddleware::class,
		'checkLpoa'	=>	\App\Http\Middleware\CheckLpoaMiddleware::class,
		'checkLeader'	=>	\App\Http\Middleware\CheckLeaderMiddleware::class,
        'checkAddHelpdesk'   =>  \App\Http\Middleware\CheckAddHelpDeskMiddleware::class,
        'checkSeaLeader'   =>  \App\Http\Middleware\CheckSeaLeaderMiddleware::class,
		'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
		'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
		'rank' => \App\Http\Middleware\RankMiddleware::class,
        'partnerAuth'    =>  \App\Http\Middleware\API\PartnerAuthenticate::class,
	];
}
