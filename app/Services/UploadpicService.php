<?php
namespace App\Services;
use Validator;

class UploadpicService
{

    public function helpdesk_upload($get_path)
    {
        $idFront_path = $get_path;
        $destinationPath = public_path('/helpdesk/');
        $filename = uniqid().$get_path->getClientOriginalName();
        $idFront_path->move($destinationPath, $filename);
        return $filename;
    }

    public function helpdesk_count_sec($startTime,$nineTime,$eightTime,$finishTime)
    {
        if($startTime->lessThan($nineTime)){ // if < 09.30
            // echo "small"."<br>";
            // echo $nineTime."<br>";
            // echo $finishTime."<br>";
            // echo $finishTime->diffInDays($nineTime)." "; 
            // echo $finishTime->diffInHours($nineTime)." "; 
            // echo $finishTime->diffInMinutes($nineTime)." ";

            $totalDuration = $finishTime->diffInSeconds($nineTime); // get 9.30 diffent day
        }else{
            // echo "big"."<br>";
            // echo $finishTime->diffInDays($startTime)."<br>"; 
            // echo $finishTime->diffInHours($startTime)."<br>"; 
            // echo $finishTime->diffInMinutes($startTime)."<br>"; 
            $totalDuration = $finishTime->diffInSeconds($startTime); // get comment time diffent day
        }

     
    
        return $totalDuration;
    }

    public function CheckFileSupported($img)
    {
        $fileArray = array('image' => $img);
        $rules = array(
          'image' => 'mimes:jpeg,jpg,png,gif,pdf|required|max:10000' // max 10000kb
        );
        $validator = Validator::make($fileArray, $rules);
        if ($validator->fails())
        {
            return false;
        }else{
            return true;
        }
    }
}