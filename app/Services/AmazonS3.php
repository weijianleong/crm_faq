<?php
namespace App\Services;
use Illuminate\Support\Facades\Storage;

class AmazonS3 {
    public function uploadImage($idFront_path, $user, $folder, $site) {
        $filename = $site.md5($user->email).$idFront_path->getClientOriginalName();
        $path = $folder."/".$filename;
        Storage::disk('s3')->put($path, fopen($idFront_path, 'r+'));
        return $filename;
    }
    public function uploadImageNew($idFront_path, $user, $folder, $site) {
        $filename = $site.md5($user->email);
        $path = $folder."/".$filename;
        Storage::disk('s3')->put($path, fopen($idFront_path, 'r+'));
        return $filename;
    }

    public function uploadProfileImage($profile_path, $user, $folder, $site) {
        //$filename = $site.md5($user->email).$idFront_path->getClientOriginalName();
        $path = $folder."/".$user->email;
        $url = Storage::disk('s3')->put($path, fopen($profile_path, 'r+'),'public');
        return $path;
    }

    public function loadImage($path) {
        $s3 = \Storage::disk('s3');
        $client = $s3->getDriver()->getAdapter()->getClient();
        $command = $client->getCommand('GetObject', [
            'Bucket' => \Config::get('filesystems.disks.s3.bucket'),
            'Key'    => $path
        ]);
        $request = $client->createPresignedRequest($command, '+30 seconds');
        $url = (string) $request->getUri();
        return $url;
    }

    public function uploadFile($savepath, $filepath) {
        $file = fopen($filepath, 'r');
        Storage::disk('s3')->put($savepath, $file);
        fclose($file);
        unlink($filepath);
    }
}
