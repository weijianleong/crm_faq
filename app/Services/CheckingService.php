<?php
namespace App\Services;
use Carbon\Carbon;
use DB;

class CheckingService
{

    public function CheckSpace($data)
    {
        $newData = str_replace(' ', '', $data );
        return $newData;
    }

    public function uploadFile($file,$filePath,$email)
    {
        $idFront_path = $file;
        $destinationPath = public_path($filePath);
        $filename = uniqid()."-ID".$email;
        $idFront_path->move($destinationPath, $filename);
        return $filename;
    }

    public function CheckPassword($password){
        if (!preg_match('/^(?=.*[A-Za-z])(?=.*[0-9])(?!.*[^a-zA-Z0-9]).{8,30}$/',$password))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public function getBalance ($member, $user) {

        $myArray = [
            'apiuserId'=>'api_user_test',
            'apikey'=>'api_key',
            'timestamp'=>Carbon::now()->timestamp,
            'memberId'=>$user->email,
            'linkedKey'=>$user->linked_id,
            // 'sign'=>'EA73307873511FAA3A095D49D22B8509',
        ];

        $getSign = $this->createSign($myArray);
        $result = $this->callCapxApi("getBalance",$getSign,$member);
        // print_r($result);
        return $result;
        //echo $result["resultDesc"];
        //echo $result;
    }

    public function transferToCapx ($member, $user, $amount) {



        $refId = uniqid()."-TToCapx-".Carbon::now()->timestamp;
 
        $myArray = [
           'apiuserId'=>'api_user_test', 
           'apikey'=>'api_key', 
           'timestamp'=>Carbon::now()->timestamp,
           'memberId'=>$user->email, 
           'linkedKey'=>$user->linked_id, 
           'amount'=>$amount,
           'refId'=>$refId,
       ];

        $getSign = $this->createSign($myArray);
        $result = $this->callCapxApi("transferToCapx",$getSign,$member);
        // print_r($result);
        return $result;
        //echo $result["resultDesc"];
        //echo $result;
    }

    public function loginRedirect ($member, $user, $getRank) {

        
        $myArray = [
           'apiuserId'=>'api_user_test', 
           'apikey'=>'api_key', 
           'timestamp'=>Carbon::now()->timestamp,
           'memberId'=>$user->email, 
           'linkedKey'=>$user->linked_id,
           'rank'=> $getRank,
           'customerGroup'=>$member->customerGroup,
           // 'sign'=>'EA73307873511FAA3A095D49D22B8509',
       ];

        $getSign = $this->createSign($myArray);

        //echo $getSign."<br>";
        $result = $this->callCapxApi("loginRedirect",$getSign,$member);
        return $result;

    }
    public function CapxRegister ($member,$memberUser) {

        $member->detail->phone1 = str_replace("+","",$member->detail->phone1);
        if(empty($member->customerGroup)){
            $member->customerGroup = 'a';
        }

        $rank = 'BASIC';
        $ranking = \DB::table('Member_Sales_Summary')->where('member_id', $member->id)->orderBy('date', 'DESC')->first();
        if(isset($ranking)){
            if($ranking->rank) $rank = $ranking->rank;
        }

        $myArray = [
            'apiuserId'=>'api_user_test',
            'apikey'=>'api_key',
            'timestamp'=>Carbon::now()->timestamp,
            'memberId'=>$member->username,
            'email'=>$member->username,
            'name'=>$memberUser->first_name,
            'address'=>$member->detail->address,
            'city'=>$member->detail->nationality,
            'country'=>$member->detail->nationality,
            'identityNo'=>$member->detail->identification_number,
            'identityType'=>1,
            'mobile'=>$member->detail->phone1,
            'isKyc'=>4,
            'linkedKey'=>$memberUser->linked_id,
            'rank'=>$rank,
            'customerGroup'=>$member->customerGroup,
        ];

        $getSign = $this->createSign($myArray);
        $result = $this->callCapxApi("register",$getSign,$member);
        return $result;

    }

    public function createSign ($data) {

           ksort($data);

           $str ='';
           foreach ($data as $key => $value) {

                if ($value === end($data))
                $str.=$key.'='.$value;
                else
                $str.=$key.'='.$value.'&';
           }

           $data["sign"] = strtoupper(md5($str));
           ksort($data);

           $str ='';
           foreach ($data as $key => $value) {

                if ($value === end($data))
                $str.=$key.'='.$value;
                else
                $str.=$key.'='.$value.'&';
           }

           return $str;
    }

    public function callCapxApi ($api,$PostStr,$member) {

        $service_id = $member->id."_callAPI_".uniqid()."_".Carbon::now()->timestamp;

        \DB::table('API_Services')->insert([
                                                    'access_id' => $member->id,
                                                    'type' => 0,
                                                    'name' => $api,
                                                    'dataIn' => $PostStr,
                                                    'dataOut' => '',
                                                    'status' => 'Pending',
                                                    'service_id' => $service_id,
                                                    'created_at' => \Carbon\Carbon::now(),
                                                    'updated_at' => \Carbon\Carbon::now()
                                            ]);


        $url = config('capx.newapiURL').$api;
        $ch=curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt ($ch, CURLOPT_POST, 1);
        curl_setopt ($ch, CURLOPT_POSTFIELDS, $PostStr);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


        $result = curl_exec($ch);
        $data = json_decode($result);

        
        if(empty($result)){
        
            \DB::table('API_Services')
            ->where('service_id', $service_id)
            ->update(['dataOut' => '', 'status' => 'error', 'updated_at' => \Carbon\Carbon::now()]);

        }else{

            \DB::table('API_Services')
            ->where('service_id', $service_id)
            ->update(['dataOut' => ($result), 'status' => 'OK', 'updated_at' => \Carbon\Carbon::now()]);

            if($data->statusCode !=0 ){
                \DB::table('Capx_Api_Fail')
                    ->insert(
                        [
                            'service_id' => $service_id,
                            'api_name' => $api,
                            'result' => $data->resultDesc,
                            'created_at' => \Carbon\Carbon::now(),
                            'updated_at' => \Carbon\Carbon::now()
                        ]
                    );
            }
        }

        return $data;
    }

}