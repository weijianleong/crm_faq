<?php
namespace App\Presenters\back\helpdesk;

class helpdesk_edit 
{
    public function cs_dashboard($cs_type)
    {
        //
        $ans = "";
        if($cs_type == "0"){

           $ans = "<li><a href=".route('admin.helpdesk.Plist').">Helpdesk List</a></li>";
        }
        else{
           $ans = "<li><a href=".route('admin.helpdesk.list').">Helpdesk List</a></li>";
        }

        return $ans;
    }

    public function cs_BackPending($cs_type)
    {
        //
        $ans = "";
        if($cs_type == "0"){

           $ans = "<li><a href=".route('admin.helpdesk.Plist').">Back Pending Page</a></li>";
        }
        else{
           $ans = "<li><a href=".route('admin.helpdesk.list').">Back Pending Page</a></li>";
        }

        return $ans;
    }

    public function check_day( $refrence_id , $day)
    {
        //
        $ans = "";
        if($day < 3)
            $ans = "<p style=color:#00FF00>ID :".$refrence_id."</p>";
                      
        elseif($day == 3)
            $ans = "<p style=color:orange>ID :".$refrence_id."</p>";
                      
        else
            $ans = "<p style=color:red>ID :".$refrence_id."</p>";
                      
    

        return $ans;
    }

    public function show_pic( $helpdeskPath, $pic_1 , $pic_2)
    {
        //
        $ans1 = "";
        $ans2 = "";

        if(!empty($pic_1))
            $ans1 = "<br><a target=_blank href=".route('admin.amazon.read', ['folder' => 'helpdesk', 'filename' => $pic_1, 'lang' => \App::getLocale()]).">".\Lang::get('helpdesk.readPic1')."</a>";
            // $ans1 = "<br><a target=_blank href=".$helpdeskPath.$pic_1.">".\Lang::get('helpdesk.readPic1')."</a>";
        if(!empty($pic_2))
            $ans2 = "<br><a target=_blank href=".route('admin.amazon.read', ['folder' => 'helpdesk', 'filename' => $pic_2, 'lang' => \App::getLocale()]).">".\Lang::get('helpdesk.readPic2')."</a>";               
            // $ans2 = "<br><a target=_blank href=".$helpdeskPath.$pic_2.">".\Lang::get('helpdesk.readPic2')."</a>";               

        return $ans1.$ans2;
    }

    public function show_type( $cs_type )
    {
        $ans = "";
        $type_Id = explode(",",$cs_type);
        for ($i=0; $i <count($type_Id) ; $i++) { 
                if($type_Id[$i] == 1)
                    $ans .= "托管-";
                elseif($type_Id[$i] ==2)
                    $ans .= "资金-"; 
                elseif($type_Id[$i] ==3)
                    $ans .= "咨询-";
                else
                    $ans .= "审核-";
        }
        //exit();

             

        return $ans;
    }


}