<?php
namespace App\Presenters\back\helpdesk;

class helpdesk_showHistory 
{
    public function action_type( $action )
    {
        //
        $ans = "";
        

        if($action == "C")
            $ans = "<label class='label label-warning'>Comment</label>";
        elseif($action == "T") 
            $ans = "<label class='label label-danger'>Transfer</label>";
        elseif($action == "R")
            $ans = "<label class='label label-primary'>Resolved</label>";  
        else 
            $ans = "<label class='label label-success'>Done</label>";  
                 

        return $ans;
    }

    public function action_msg( $cs_type , $cs_name)
    {
        //
        $ans = "";
        

      if($cs_type == '1')
        $ans = "<div class=input-group><span class=input-group-addon><a href=# title=Username data-toggle=popover data-trigger=focus data-content=".$cs_name .">拖管</a></span></div>";
      elseif($cs_type == '2')
        $ans = "<div class=input-group><span class=input-group-addon><a href=# title=Username data-toggle=popover data-trigger=focus data-content=".$cs_name .">資金</a></span></div>";
      else
        $ans = "<div class=input-group><span class=input-group-addon><a href=# title=Username data-toggle=popover data-trigger=focus data-content=".$cs_name.">諮詢</a></span></div>";

                 

        return $ans;
    }
}