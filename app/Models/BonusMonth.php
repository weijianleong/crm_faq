<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BonusMonth extends Model
{
    protected $table = 'Bonus_Month';
    protected $fillable = [];
    protected $hidden = [];

    protected static $memberModel = 'App\Models\Member';

    public function member () {
        return $this->belongsTo(static::$memberModel, 'member_id');
    }
}
