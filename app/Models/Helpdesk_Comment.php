<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class helpdesk_comment extends Model
{
    protected $table = 'helpdesk_comment';
    protected $fillable = [];
    protected $hidden = [];
}