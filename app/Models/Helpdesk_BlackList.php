<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Helpdesk_BlackList extends Model
{
    protected $table = 'helpdesk_blacklist';
    protected $fillable = ['member_id','cr8_by'];
    protected $hidden = [];
}