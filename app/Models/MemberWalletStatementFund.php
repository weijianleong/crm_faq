<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberWalletStatementFund extends Model
{
    protected $table = 'Member_Wallet_Statement_Fund';
    protected $fillable = [];
    protected $hidden = [];
    protected $primaryKey = 'member_id';

    protected static $memberModel = 'App\Models\Member';

    public function member () {
    	return $this->belongsTo(static::$memberModel, 'member_id');
    }
}
