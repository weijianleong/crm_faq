<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Helpdesklog extends Model
{
    protected $table = 'helpdesk_log';
    protected $fillable = [];
    protected $hidden = [];
}