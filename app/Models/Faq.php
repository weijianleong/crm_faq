<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Faq
 *
 * @package App\Models
 *
 * @method Faq getEn()
 * @method Faq getChs()
 * @method Faq getFaq()
 * @method Faq getWithdraw()
 * @method Faq getAdvisory()
 * @method Faq getApproval()
 * @method Faq getCapx()
 *
 */
class Faq extends Model
{
    protected $fillable =[
        'title_cn',
        'content_cn',
        'title_en',
        'content_en',
        'type'
    ];

    public function scopeGetEn($query)
    {
        return $query->select('title_en', 'content_en');
    }

    public function scopeGetChs($query)
    {
        return $query->select('title_cn', 'content_cn');
    }

    public function scopeGetFaq($query, $param)
    {
        return $query->where('type', $param)->get();
    }
}
