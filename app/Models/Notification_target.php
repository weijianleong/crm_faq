<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification_target extends Model
{
    protected $table = 'notification_target';
    protected $fillable = [];
    protected $hidden = [];
}