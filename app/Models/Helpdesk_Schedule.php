<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Helpdesk_Schedule extends Model
{
    protected $table = 'helpdesk_schedule';
    protected $fillable = ['cs_id','ticket_num','work','day','shift'];
    protected $hidden = [];
}