<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class helpdesk_record extends Model
{
    protected $table = 'helpdesk_record';
    protected $fillable = ['email','reply_case','reply_sec','created_at'];
    protected $hidden = [];
}