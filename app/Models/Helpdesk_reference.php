<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Helpdesk_reference extends Model
{
    protected $table = 'helpdesk_reference';
    protected $fillable = [];
    protected $hidden = [];
}