<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberCoin extends Model
{
    protected $table = 'Capx_Member_Coin';
    protected $fillable = [];
    protected $hidden = [];
    protected $primaryKey = 'member_id';

    protected static $memberModel = 'App\Models\Member';

    public function member () {
    	return $this->belongsTo(static::$memberModel, 'member_id');
    }
}