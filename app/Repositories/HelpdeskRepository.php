<?php

namespace App\Repositories;

use App\Models\Helpdesk;
use App\Models\Helpdesklog;
use App\Models\Helpdesk_reference;
use App\Models\Helpdesk_record;
use App\Models\Helpdesk_Schedule;
use App\Models\Helpdesk_BlackList;
use App\Models\Helpdesk_Cs_Handle;
use App\Models\Users;
use Carbon\Carbon;
use DB;
use Yajra\Datatables\Facades\Datatables;

class HelpdeskRepository extends BaseRepository
{
    protected $model;
    protected $allowedFields = [];
    protected $booleanFields = [];

    public function __construct() {
        $this->model = new Helpdesk;
        $this->model_log = new Helpdesklog;
        $this->user = new Users;
        $this->reference = new helpdesk_reference;
        $this->record = new helpdesk_record;
        $this->schedule = new Helpdesk_Schedule;
        $this->blacklist = new Helpdesk_BlackList;
        $this->cs_handle = new Helpdesk_Cs_Handle;

    }

    protected function saveModel($model, $data) {
        foreach ($data as $k=>$d) {
            $model->{$k} = $d;
        }
        $model->save();
        return $model;
    }



    public function check_refrence($data){      
        $model = $this->reference->where('created_at', 'like', $data)->get();
        return count($model);
    }

    public function get_refrence($data){      
        $model = $this->saveModel(new $this->reference, $data);
        return $model->id;
    }
    public function get_DeskNum ($id){
        return $this->reference->where('user_id', $id)->where('created_at','like',Carbon::now()->format('Y-m-d').'%')->count();
    }

    public function transfer($data) {
        $model = $this->saveModel(new $this->model_log, $data);

        $this->model->where('id', $data["helpdesk_id"])->update(['for_cs_type' => $data["cs_type"] ,'cs_id' => $data["to_cs_id"] ]);
        return $model;
    }
    public function find_admin() {
        $model = $this->user->whereIn('cs_type',[1,2,3,4] )->orderBy('cs_type','desc')->get();
        return $model;
    }

    public function comment_log($data) {
        $model = $this->saveModel(new $this->model_log, $data);
        $this->model->where('id', $data["helpdesk_id"])->update(['is_reply' => 1 ,'status' => "solving" ]);//update admmin_helpdesh is_reply
        //$this->model->where('id', $data["helpdesk_id"])->update(['status' => "solving" ]);//update admmin_helpdesh is_reply
        return $model;
    }

    public function update_reply($data) {
        
        if(empty($data["cs_id"])){ //client
            $this->model->where('id', $data["helpdesk_id"])->update(['is_reply' => 0,'read' => 1 ]);//update client_helpdesh is_reply

        }else{ //CS
            
            if($data["cs_type"] == 0){

                $this->model->where('id', $data["helpdesk_id"])->update(['is_reply' => 0, 'read' => 1 ]);//update cs is_reply

            }else{

                $this->model->where('id', $data["helpdesk_id"])->update(['is_reply' => 0, 'cs_id' => $data["cs_id"] ,'read' => 1 ]);//update cs is_reply

            }
            

    }

        
       
    }
    public function update_read($id, $tittle) {
        
        $this->model->where('user_id', $id)->where('tittle',$tittle)->update(['read' => 1 ]);//update client_helpdesh unread
       
    }
    public function update_log($data) {
        $model = $this->saveModel(new $this->model_log, $data);
       
    }


    public function findLog($id) {
        
        return $this->model_log->where('helpdesk_id', $id)->get();
       
    }

    public function store($data) {
        $model = $this->saveModel(new $this->model, $data);
        return $model;
    }

    public function update($model, $data) {
        $model = $this->saveModel($model, $data);
        return $model;
    }

    public function findById ($id) {

        return $this->model->where('id', $id)->first();
    }
    public function findByMemberId ($id,$member_id) {

        return $this->model->where('id', $id)->where('user_id',$member_id)->first();
    }
    public function findByIdRead ($id) {
        return $this->model->where('user_id', $id)->where('read',1)->first();
        //$this->model->where('id', $id)->update(['read' => 0 ]);
    }
    public function findByReadUpdate ($id) {
        //return $this->model->where('user_id', $id)->where('read',1)->first();
        $this->model->where('id', $id)->update(['read' => 0 ]);
    }

    public function autoTicket ($data , $getDate) {
        //return $this->model->where('user_id', $id)->where('read',1)->first();


        if($cs_id = $this->schedule->where('day',$getDate)->where('work',1)->orderBy('ticket_num')->value('cs_id')){
            $this->schedule->where('cs_id',$cs_id)->where('day',$getDate)->increment('ticket_num');
            $this->model->where('refrence_id', $data['refrence_id'])->update(['cs_id' => $cs_id ]);

            $email = $this->user->where('id',$cs_id)->value('email');
            $helpdesk_model = $this->model->where('refrence_id',$data['refrence_id'])->first();
            $this->moveTicket($helpdesk_model->user_id , $cs_id);

            $priority = $this->cs_handle->where('member_id',$helpdesk_model->user_id)->count();
            $priority = $priority + 1;
            $this->addHandle($helpdesk_model->user_id,$cs_id,$priority);

            $this->model_log->insert(
                ['helpdesk_id' => $helpdesk_model->id,'helpdesk_type' => $data['type'], 'cs_id' => '1','cs_type'=>'1','cs_username'=>'System','action'=>"T",'to_cs_id'=>$cs_id,'to_cs_username'=>$email,'created_at'=>Carbon::now()]
            );
        }

    }

    public function autoTicketShift ($data , $getDate , $Shift) {
        //return $this->model->where('user_id', $id)->where('read',1)->first();


        if($cs_id = $this->schedule->where('day',$getDate)->where('work',1)->where('shift',$Shift)->orderBy('ticket_num')->value('cs_id')){
            $this->schedule->where('cs_id',$cs_id)->where('day',$getDate)->increment('ticket_num');
            $this->model->where('refrence_id', $data['refrence_id'])->update(['cs_id' => $cs_id ]);

            

            $email = $this->user->where('id',$cs_id)->value('email');
            $helpdesk_model = $this->model->where('refrence_id',$data['refrence_id'])->first();
            $this->moveTicket($helpdesk_model->user_id , $cs_id);

            $priority = $this->cs_handle->where('member_id',$helpdesk_model->user_id)->count();
            $priority = $priority + 1;
            $this->addHandle($helpdesk_model->user_id,$cs_id,$priority);

            $this->model_log->insert(
                ['helpdesk_id' => $helpdesk_model->id,'helpdesk_type' => $data['type'], 'cs_id' => '1','cs_type'=>'1','cs_username'=>'System','action'=>"T",'to_cs_id'=>$cs_id,'to_cs_username'=>$email,'created_at'=>Carbon::now()]
            );
        }



    }

    public function autoTicketReply ($data , $getDate) {
        //return $this->model->where('user_id', $id)->where('read',1)->first();
        //$helpdesk_id = $this->model->where('id',$data['helpdesk_id'])->value('id');
        //$helpdesk_cs_id = $this->model->where('id',$data['helpdesk_id'])->value('cs_id');
        $helpdesk_model = $this->model->where('id',$data['helpdesk_id'])->first();
        $cs_id = $helpdesk_model->cs_id;
        
        if($is_work = $this->schedule->where('day',$getDate)->where('cs_id',$helpdesk_model->cs_id)->value('work')){
            if($is_work == 0){
                if($cs_id = $this->schedule->where('day',$getDate)->where('work',1)->orderBy('ticket_num')->value('cs_id')){
                    $this->schedule->where('cs_id',$cs_id)->where('day',$getDate)->increment('ticket_num');
                    $this->model->where('id', $data['helpdesk_id'])->update(['cs_id' => $cs_id ]);

                    $email = $this->user->where('id',$cs_id)->value('email');

                    $this->model_log->insert(
                        ['helpdesk_id' => $data['helpdesk_id'],'helpdesk_type' => "99", 'cs_id' => '1','cs_type'=>'1','cs_username'=>'System','action'=>"T",'to_cs_id'=>$cs_id,'to_cs_username'=>$email,'created_at'=>Carbon::now()]
                    );
                }
            }
        }else{
            if($cs_id = $this->schedule->where('day',$getDate)->where('work',1)->orderBy('ticket_num')->value('cs_id')){
                $this->schedule->where('cs_id',$cs_id)->where('day',$getDate)->increment('ticket_num');
                $this->model->where('id', $data['helpdesk_id'])->update(['cs_id' => $cs_id ]);

                $email = $this->user->where('id',$cs_id)->value('email');

                $this->model_log->insert(
                    ['helpdesk_id' => $data['helpdesk_id'],'helpdesk_type' => "99", 'cs_id' => '1','cs_type'=>'1','cs_username'=>'System','action'=>"T",'to_cs_id'=>$cs_id,'to_cs_username'=>$email,'created_at'=>Carbon::now()]
                );
            }
        }
        
        $this->moveTicket($helpdesk_model->user_id , $cs_id);

        $priority = $this->cs_handle->where('member_id',$helpdesk_model->user_id)->count();
        $priority = $priority + 1;
        $this->addHandle($helpdesk_model->user_id,$cs_id,$priority);

    }

    public function autoTicketReplyShift ($data , $getDate , $shift) {
        //return $this->model->where('user_id', $id)->where('read',1)->first();
        //$helpdesk_cs_id = $this->model->where('id',$data['helpdesk_id'])->value('cs_id');
        $helpdesk_model = $this->model->where('id',$data['helpdesk_id'])->first();
        $cs_id = $helpdesk_model->cs_id;
        if($cs_model = $this->schedule->where('day',$getDate)->where('cs_id',$helpdesk_model->cs_id)->first()){
            if($cs_model->shift != $shift || $cs_model->work ==0){
                if($cs_id = $this->schedule->where('day',$getDate)->where('work',1)->where('shift',$shift)->orderBy('ticket_num')->value('cs_id')){
                    $this->schedule->where('cs_id',$cs_id)->where('day',$getDate)->increment('ticket_num');
                    $this->model->where('id', $data['helpdesk_id'])->update(['cs_id' => $cs_id ]);

                    $email = $this->user->where('id',$cs_id)->value('email');

                    $this->model_log->insert(
                        ['helpdesk_id' => $data['helpdesk_id'],'helpdesk_type' => "99", 'cs_id' => '1','cs_type'=>'1','cs_username'=>'System','action'=>"T",'to_cs_id'=>$cs_id,'to_cs_username'=>$email,'created_at'=>Carbon::now()]
                    );
                }
            }
        }else{

            if($cs_id = $this->schedule->where('day',$getDate)->where('work',1)->where('shift',$shift)->orderBy('ticket_num')->value('cs_id')){
                $this->schedule->where('cs_id',$cs_id)->where('day',$getDate)->increment('ticket_num');
                $this->model->where('id', $data['helpdesk_id'])->update(['cs_id' => $cs_id ]);

                $email = $this->user->where('id',$cs_id)->value('email');

                $this->model_log->insert(
                    ['helpdesk_id' => $data['helpdesk_id'],'helpdesk_type' => "99", 'cs_id' => '1','cs_type'=>'1','cs_username'=>'System','action'=>"T",'to_cs_id'=>$cs_id,'to_cs_username'=>$email,'created_at'=>Carbon::now()]
                );
            }
        }

        $this->moveTicket($helpdesk_model->user_id , $cs_id);

        $priority = $this->cs_handle->where('member_id',$helpdesk_model->user_id)->count();
        $priority = $priority + 1;
        $this->addHandle($helpdesk_model->user_id,$cs_id,$priority);
    }

    public function moveTicket($member_id , $cs_id){

        $allTicket = $this->model->where('user_id',$member_id)->get();

        foreach ($allTicket as $allTickets ) {
            $allTickets->cs_id = $cs_id;
            $allTickets->save();
        }

        

    }

    public function addTicketNum($cs_id , $getDate){
        $this->schedule->where('cs_id',$cs_id)->where('day',$getDate)->increment('ticket_num');
    }

    public function addHandle($member_id , $cs_id , $priority){
        
        $data["member_id"] = $member_id;
        $data["cs_id"] = $cs_id;
        $data["priority"] = $priority;
        $this->saveModel(new $this->cs_handle, $data);
    }

    public function getHandleId($member_id){
        return $this->cs_handle->where('member_id',$member_id)->orderBy('priority')->get();
    }
    public function checkSchule($cs_id , $getDate ,$shift){
        if($shift == 3){
            return $this->schedule->where('cs_id',$cs_id)->where('day',$getDate)->where('work',1)->value('cs_id');
        }else{
            return $this->schedule->where('cs_id',$cs_id)->where('day',$getDate)->where('shift',$shift)->where('work',1)->value('cs_id');
        }
    }



    /**
     * Datatable List - Member
     * @return [type] [description]
     */


    public function getList ($id) {
        return Datatables::eloquent($this->model->where('user_id',$id)->orderBy('read', 'desc')->orderBy('created_at', 'desc'))
                ->addColumn('action', function ($model) {
                    return view('front.helpdesk.action')->with('model', $model);
                })
                ->editColumn('created_at', function ($model) {
                    return $model->created_at->format('Y-m-d H:i A');
                })

                ->editColumn('status', function ($model) {
                    
                    if($model->status=="done"){
                       return '<i class="fa fa-check" style="color: #00bf86;"></i>';
                    }else{
                        return '<i class="fa fa-warning" style="color: #ff8086;"></i>';
                    }
                })
                ->editColumn('is_reply', function ($model) {
                    if($model->is_reply==0){
                       return '<i class="fa fa-refresh" style="color: #ffa000;"></i>';
                    }else{
                       return '<i class="fa fa-mail-reply" style="color: #0fb0c0;"></i>';
                    }
                })
                ->editColumn('tittle', function ($model) {
                    
                        return mb_substr($model->tittle,0,10);
                    
                    
                })
                ->rawColumns(['action','is_reply','status'])
                ->make(true);
    }

    /**
     * Datatable List - Admin
     * @return [type] [description]
     */
    // =====================================================================Admin======================================//
    public function getAdminOpenList ($id,$type_id) {
            return Datatables::eloquent($this->model->whereIn('for_cs_type',$type_id)->where('status','process')->where('refrence_id','!=','')->whereNull('cs_id'))
                    ->addColumn('action', function ($model) {
                        return view('back.helpdesk.action')->with('model', $model);
                    })
                    ->editColumn('type', function ($model) {
                        if($model->for_cs_type==1){
                            return "托管";
                        }elseif($model->for_cs_type==2){
                            return "出金";
                        }elseif($model->for_cs_type==3){
                            return "咨询";
                        }else{
                            return "审核";
                        }
                         
                    })
                    ->editColumn('status', function ($model) {
                        
                        if($model->status=="done"){
                           return '<label class="label label-default">'."Close".'</label>';
                        }elseif ($model->status=="resolved"){
                            return '<label class="label label-success">'."Resolved".'</label>';
                        }elseif ($model->status=="solving"){
                            return '<label class="label label-warning">'."Solving".'</label>';
                        }else{
                            return '<label class="label label-danger">'."Open".'</label>';
                        }
                    })
                    ->editColumn('is_reply', function ($model) {
                        if($model->is_reply==0){
                           return '<label class="label label-warning">'."Pending".'</label><span class="glyphicon glyphicon-exclamation-sign"></span></p>';
                        }else{
                           return '<label class="label label-success">'."Replied".'</label>';

                        }
                    })
                    ->editColumn('tittle', function ($model) {
                        
                        return '<p >'.mb_substr($model->tittle,0,15).'...'.'</p>';       
                    })
                    ->editColumn('refrence_id', function ($model) {
                
                        $now = Carbon::now();
                        $end_date = Carbon::parse($model->created_at);
                        $lengthOfAd = $end_date->diffInDays($now);

                        if( $model->status == "process" || $model->status == "resolved" || $model->status == "solving"){

                            if( $lengthOfAd < 3 ){
                                return '<p style="color:#00FF00">'.$model->refrence_id.'</p>';
                            }elseif( $lengthOfAd == 3){
                                return '<p style="color:orange">'.$model->refrence_id.'</p>';
                            }else{
                                return '<p style="color:red">'.$model->refrence_id.'</p>';
                            }

                        }else{

                            return '<label class="label label-default">'.$model->refrence_id.'</label>';
                        }     
                        
                    })
                    ->rawColumns(['action','is_reply','status','tittle','refrence_id'])
                    ->make(true);
    }

    public function getAdminList ($id,$type_id) {
        return Datatables::eloquent($this->model->whereIn('for_cs_type',$type_id)->where('cs_id', $id))
                ->addColumn('action', function ($model) {
                    return view('back.helpdesk.action')->with('model', $model);
                })
                ->editColumn('type', function ($model) {
                    if($model->for_cs_type==1){
                        return "托管";
                    }elseif($model->for_cs_type==2){
                        return "出金";
                    }elseif($model->for_cs_type==3){
                        return "咨询";
                    }else{
                        return "审核";
                    }
                     
                })
                ->editColumn('status', function ($model) {
                    
                    if($model->status=="done"){
                       return '<label class="label label-default">'."Close".'</label>';
                    }elseif ($model->status=="resolved"){
                        return '<label class="label label-success">'."Resolved".'</label>';
                    }elseif ($model->status=="solving"){
                        return '<label class="label label-warning">'."Solving".'</label>';
                    }else{
                        return '<label class="label label-danger">'."Open".'</label>';
                    }
                })
                ->editColumn('is_reply', function ($model) {
                    if($model->is_reply==0){
                       return '<label class="label label-warning">'."Pending".'</label><span class="glyphicon glyphicon-exclamation-sign"></span></p>';
                    }else{
                       return '<label class="label label-success">'."Replied".'</label>';

                    }
                })
                ->editColumn('tittle', function ($model) {
                    
                    return '<p >'.mb_substr($model->tittle,0,15).'...'.'</p>';       
                })
                ->editColumn('refrence_id', function ($model) {
            
                    $now = Carbon::now();
                    $end_date = Carbon::parse($model->created_at);
                    $lengthOfAd = $end_date->diffInDays($now);

                    if( $model->status == "process" || $model->status == "resolved" || $model->status == "solving"){

                        if( $lengthOfAd < 3 ){
                            return '<p style="color:#00FF00">'.$model->refrence_id.'</p>';
                        }elseif( $lengthOfAd == 3){
                            return '<p style="color:orange">'.$model->refrence_id.'</p>';
                        }else{
                            return '<p style="color:red">'.$model->refrence_id.'</p>';
                        }

                    }else{

                        return '<label class="label label-default">'.$model->refrence_id.'</label>';
                    }     
                    
                })
                ->rawColumns(['action','is_reply','status','tittle','refrence_id'])
                ->make(true);
    }

    public function getAdminPList () {
        return Datatables::eloquent($this->model->query())
                ->addColumn('action', function ($model) {
                    return view('back.helpdesk.action')->with('model', $model);
                })
                ->editColumn('status', function ($model) {
                    
                    if($model->status=="done"){
                       return '<label class="label label-default">'."Close".'</label>';
                    }elseif ($model->status=="resolved"){
                        return '<label class="label label-success">'."Resolved".'</label>';
                    }elseif ($model->status=="solving"){
                        return '<label class="label label-warning">'."Solving".'</label>';
                    }else{
                        return '<label class="label label-danger">'."Open".'</label>';
                    }
                })
                ->editColumn('type', function ($model) {
                    if($model->for_cs_type==1){
                        return "托管";
                    }elseif($model->for_cs_type==2){
                        return "出金";
                    }elseif($model->for_cs_type==3){
                        return "咨询";
                    }else{
                        return "审核";
                    }
                     
                })
                ->editColumn('is_reply', function ($model) {
                    if($model->is_reply==0){
                       return '<label class="label label-warning">'."Pending".'</label><span class="glyphicon glyphicon-exclamation-sign"></span></p>';
                    }else{
                       return '<label class="label label-success">'."Replied".'</label>';

                    }
                })
                ->editColumn('tittle', function ($model) {
                    
                    return '<p >'.mb_substr($model->tittle,0,15).'...'.'</p>';
                    
                    
                })
                ->editColumn('refrence_id', function ($model) {
            
                    $now = Carbon::now();
                    $end_date = Carbon::parse($model->created_at);
                    $lengthOfAd = $end_date->diffInDays($now);
                    
                    if( $model->status == "process" || $model->status == "resolved" || $model->status == "solving"){

                        if( $lengthOfAd < 3 ){
                            return '<p style="color:#00FF00">'.$model->refrence_id.'</p>';
                        }elseif( $lengthOfAd == 3){
                            return '<p style="color:orange">'.$model->refrence_id.'</p>';
                        }else{
                            return '<p style="color:red">'.$model->refrence_id.'</p>';
                        }

                    }else{

                        return '<label class="label label-default">'.$model->refrence_id.'</label>';
                    }

                    
                    
                    
                })
                ->rawColumns(['action','is_reply','status','tittle','refrence_id'])
                ->make(true);
    }

    //record
    public function getAdminRecord(){      
        return Datatables::eloquent($this->record->query())
                ->editColumn('reply_sec', function ($model) {
                    
                    $min = ($model->reply_sec/$model->reply_case);
                    return gmdate("H",$min).'hr '.gmdate("i",$min).'min '.gmdate("s",$min).'s';

                    
                })
                ->editColumn('created_at', function ($model) {
                    
                    
                    $day = date('Y-m-d', strtotime($model->created_at));
                    return $day;

                    
                })
                ->rawColumns(['reply_sec','reply_date'])
                ->make(true);
    }
    //personalRating
    public function getPersonalRating ($date ,$cs_id) {
        $sql = DB::table('users')
        // ->join('users', 'users.id', '=', 'helpdesk.cs_id')
        ->join('helpdesk', 'users.id', '=', 'helpdesk.cs_id')
        
        ->select([
            \DB::raw('sum(rate_star) as rate_star'),
            \DB::raw('sum(is_complain) as complain'),
            \DB::raw('count(cs_id) as ticket'),

            "users.email as email"])

        ->where('rate_star', '!=','0')
        ->where('cs_id', '=',$cs_id)
        ->where('helpdesk.created_at', 'like', $date.'%')

        ->groupBy('helpdesk.cs_id');


        return Datatables::of($sql)
                ->editColumn('average', function ($sql) {
                    
                    $avg = ($sql->rate_star/$sql->ticket);
                    return $avg;

                    
                })
                ->rawColumns(['average'])

                ->make(true);
                
    }

    public function getAllPersonalRating ($date) {
        $sql = DB::table('users')
        // ->join('users', 'users.id', '=', 'helpdesk.cs_id')
        ->join('helpdesk', 'users.id', '=', 'helpdesk.cs_id')
        ->select([
            \DB::raw('sum(rate_star) as rate_star'),
            \DB::raw('sum(is_complain) as complain'),
            \DB::raw('count(cs_id) as ticket'),
            "users.email as email"])

        ->where('rate_star', '!=','0')
        ->where('helpdesk.created_at', 'like', $date.'%')

        ->groupBy('helpdesk.cs_id');


        return Datatables::of($sql)
                ->editColumn('average', function ($sql) {
                    
                    $avg = ($sql->rate_star/$sql->ticket);
                    return $avg;

                    
                })
                ->rawColumns(['average'])

                ->make(true);
                
    }
    //complain
    public function getPersonalComplain ($date ,$cs_id) {
        $sql = DB::table('users')
        // ->join('users', 'users.id', '=', 'helpdesk.cs_id')
        ->join('helpdesk', 'users.id', '=', 'helpdesk.cs_id')
        
        ->select([

            \DB::raw('sum(is_complain) as complain'),
            \DB::raw('count(cs_id) as ticket'),

            "users.email as email"])

        ->where('cs_id', '=',$cs_id)
        ->where('helpdesk.created_at', 'like', $date.'%')

        ->groupBy('helpdesk.cs_id');


        return Datatables::of($sql)
                ->make(true);
                
    }

    public function getAllPersonalComplain ($date) {
        $sql = DB::table('users')
        // ->join('users', 'users.id', '=', 'helpdesk.cs_id')
        ->join('helpdesk', 'users.id', '=', 'helpdesk.cs_id')
        
        ->select([
            \DB::raw('sum(is_complain) as complain'),
            \DB::raw('count(cs_id) as ticket'),
            "users.email as email"])

        ->where('helpdesk.created_at', 'like', $date.'%')
        ->groupBy('helpdesk.cs_id');


        return Datatables::of($sql)
                ->make(true);
                
    }
    //complain
    public function getPersonalDate () {

        $model = $this->model->groupBy(DB::raw("YEAR(created_at)"),DB::raw("MONTH(created_at)"))->get();
        return $model;
         
    }



    //rate
    public function addRate($data){

        
        $this->model->where('id', $data["helpdesk_id"])->update(['rate_star' => $data["rate_star"],'is_rate' => 1 ]);//update helpdesk is_rate

    }

    //complain
    public function addComplain($data){

        
        $this->model->where('id', $data["helpdesk_id"])->update(['complain' => $data["complain"],'is_complain' => 1 ]);//update helpdesk is_rate

    }
    //settle
    public function updateSettle($data){

        
        $this->model->where('id', $data["helpdesk_id"])->update(['status' => "done" ]);//update helpdesk is_rate

    }

    //schedule 
    public function getCS () {
        return $this->user->where('permissions','{"admin":true}')->where('cs_type','=','1,2,3,4')->get();
    }

    public function getSchedule ($date) {
        return $this->schedule->where('day',$date)->get();
    }

    public function insertSchedule ($cs_id , $cr8_at , $work_status , $shift) {
        $this->schedule->updateOrCreate(['day' => $cr8_at,'cs_id' => $cs_id],['cs_id' => $cs_id, 'work' => $work_status,'day' => $cr8_at,'shift'=>$shift]);
    }
    //

    //BlackList
    public function insertBlackList ($data) {
        $this->blacklist->updateOrCreate(['member_id' => $data["member_id"]],['member_id' => $data["member_id"],'cr8_by' => $data["cs_id"]]);
    }
    public function checkBlackList ($member_id) {
        return $this->blacklist->where('member_id',$member_id)->first();
    }


    // =====================================================================Admin======================================//


}