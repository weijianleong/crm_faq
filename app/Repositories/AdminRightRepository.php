<?php

namespace App\Repositories;

use App\Models\admin_right;
use App\Models\Users;
use Yajra\Datatables\Facades\Datatables;
use Carbon\Carbon;
use DB;

class AdminRightRepository extends BaseRepository
{
    protected $model;
    protected $allowedFields = [];
    protected $booleanFields = [];

    public function __construct() {
        $this->model = new admin_right;
        $this->user_model = new Users;
        
    }

    protected function saveModel($model, $data) {
        foreach ($data as $k=>$d) {
            $model->{$k} = $d;
        }
        $model->save();
        return $model;
    }

    public function findById ($id) {
        return DB::table('admin_right')->leftJoin('users', 'users.id', '=', 'admin_right.user_id')->where('user_id',$id)->first();
    }
    public function autoInsert ($id,$data) {
        return $this->saveModel($this->model,$data);
        
    }
    public function updateRight ($id,$data) {
        $this->user_model->where("id",$id)->update(['cs_type' => $data["helpdesk_right"]]);
        return $this->model->where("user_id",$id)->update(['right' => $data["right"],'updated_by'=>$data["updated_by"]]);
        
    }
    
    public function getUser () {
        return Datatables::eloquent($this->user_model->where('permissions','{"admin":true}'))
                ->addColumn('action', function ($model) {
                    return view('back.right.action')->with('model', $model);
                })
                ->rawColumns(['action'])
                ->make(true);
    }

}