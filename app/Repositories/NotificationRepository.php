<?php

namespace App\Repositories;

use App\Models\Notification_target;
use Yajra\Datatables\Facades\Datatables;

class NotificationRepository extends BaseRepository
{
    protected $model;
    protected $allowedFields = [];
    protected $booleanFields = [];

    public function __construct() {
        $this->model = new Notification_target;
    }

    protected function saveModel($model, $data) {
        foreach ($data as $k=>$d) {
            $model->{$k} = $d;
        }
        $model->save();
        return $model;
    }

    public function store($data) {
        $model = $this->saveModel(new $this->model, $data);
        return $model;
    }

    public function update($model, $data) {
        $model = $this->saveModel($model, $data);
        return $model;
    }

    public function findById ($id) {

        return $this->model->where('helpdesk_id', $id)->get();
    }
    public function findByMemberId ($id,$member_id) {

        return $this->model->where('id', $id)->where('member_id',$member_id)->first();

    }
    public function findByReadUpdate ($id) {

        return $this->model->where('id', $id)->update(['read' => 1]);
        
    }

    public function getCSlist ( $member_id ) {

        return Datatables::eloquent($this->model->where('member_id',$member_id)->where('type',1)->orderBy('created_at', 'DESC'))
                ->addColumn('action', function ($model) {
                    if($model->read == 0) {
                        return '<button class="btn btn-primary glow_button" onclick="getNotificationDetails('.$model->id.')"><i class="fa fa-share-square-o"></i> '.\Lang::get('common.read').'</button><i id='.$model->id.' class="fa fa-circle" style="position: absolute; color: red; font-size: 8px;"></i>';                        
                    }else{
                        return '<button class="btn btn-primary glow_button" onclick="getNotificationDetails('.$model->id.')"><i class="fa fa-share-square-o"></i> '.\Lang::get('common.read').'</button>';
                    }
                })
                ->editColumn('created_at', function ($model) {
                    return $model->created_at->format('Y-m-d h:i A');
                })
                ->rawColumns(['action'])
                ->make(true);
    }

    public function getSYSTEMlist ( $member_id ) {

        return Datatables::eloquent($this->model->where('member_id',$member_id)->where('type',0)->orderBy('created_at', 'DESC'))
                ->addColumn('action', function ($model) {
                    if($model->read == 0) {
                        return '<button class="btn btn-primary glow_button" onclick="getNotificationDetails('.$model->id.')"><i class="fa fa-share-square-o"></i> '.\Lang::get('common.read').'</button><i id='.$model->id.' class="fa fa-circle" style="position: absolute; color: red; font-size: 8px;"></i>';                        
                    }else{
                        return '<button class="btn btn-primary glow_button" onclick="getNotificationDetails('.$model->id.')"><i class="fa fa-share-square-o"></i> '.\Lang::get('common.read').'</button>';
                    }
                })
                ->editColumn('created_at', function ($model) {
                    return $model->created_at->format('Y-m-d h:i A');
                })
                ->rawColumns(['action'])
                ->make(true);
    }

}