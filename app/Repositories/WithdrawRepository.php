<?php

namespace App\Repositories;

use App\Models\Withdraw;
use Carbon\Carbon;
use Yajra\Datatables\Facades\Datatables;

class WithdrawRepository extends BaseRepository
{
    protected $model, $adminFee;
    protected $allowedFields = [];
    protected $booleanFields = [];

    public function __construct() {
        $this->model = new Withdraw;
        $this->adminFee = config('misc.withdrawAdminFee');
    }

    protected function saveModel($model, $data) {
        foreach ($data as $k=>$d) {
            $model->{$k} = $d;
        }
        $model->save();
        return $model;
    }

    public function store($data) {
        $model = $this->saveModel(new $this->model, $data);
        return $model;
    }

    public function update($model, $data) {
        $model = $this->saveModel($model, $data);
        return $model;
    }

    public function getAllowedFields () {
        return $this->allowedFields;
    }

    public function getBooleanFields () {
        return $this->booleanFields;
    }

    public function findById ($id) {
        return $this->model->where('id', $id)->first();
    }

    /**
     * All Withdraws - DataTable
     * @param  boolean $table
     * @return object
     */
    public function findAll ($table) { //weilun
        if (!$table) return $this->model->all();
        else {
            return Datatables::eloquent($this->model->query())
                ->addColumn('action', function ($model) {
                    return view('back.withdraw.action')->with('model', $model);
                })
                ->editColumn('status', function ($model) {
                    if ($model->status == 'done') return '<label class="label label-success">Approved</label>';
                    elseif ($model->status == 'reject') return '<label class="label label-danger">Rejected</label>';
                    else return '<label class="label label-default">Processing</label>';
                })

                
                
                ->editColumn('amount', function ($model) {
                    return number_format($model->amount, 2);
                })
                ->rawColumns(['action', 'status'])

                ->make(true);
        }
    }
    public function findPList ($table) {//weilun
        if (!$table) return $this->model->all();
        else {
            return Datatables::eloquent($this->model->where('status','process'))
                ->addColumn('action', function ($model) {
                    return view('back.withdraw.action')->with('model', $model);
                })
                ->editColumn('status', function ($model) {
                    if ($model->status == 'done') return '<label class="label label-success">Approved</label>';
                    elseif ($model->status == 'reject') return '<label class="label label-danger">Rejected</label>';
                    else return '<label class="label label-default">Processing</label>';
                })
                
                ->editColumn('amount', function ($model) {
                    return number_format($model->amount, 2);
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
    }

    /**
     * Make Withdraw - Member
     * @param  App\Models\Member $member [description]
     * @param  decimal $amount [description]
     * @return [type]         [description]
     */
    public function makeWithdraw ($member,$bankInfo, $amount , $payment_type , $debit) { //weilun
        if ($amount < 50) {
            throw new \Exception(\Lang::get('error.withdrawAmountError'), 1);
            return false;
        }
        $wallet = $member->wallet;
        
        /*
        if ($wallet->lock_cash) {
            throw new \Exception(\Lang::get('error.cashWalletLock'), 1);
            return false;
        }
         */
        //$adminFee = ($this->adminFee / 100) * $amount;
        //$wdAmount = $amount + $adminFee;

        if ($amount > $wallet->cash_point) {
            throw new \Exception(\Lang::get('error.bonusNotEnough'), 1);
            return false;
        } 

        if ($payment_type == "bank"){
            $detail = $member->detail; 

            $this->saveModel($this->model, [
            'member_id' =>  $member->id,
            'username'  =>  $member->username,
            'amount'    =>  $amount,
            'bank_name' =>  $bankInfo->bank_name,
            'bank_account_number'  =>  $bankInfo->bank_no,
            'bank_account_holder'    =>  $bankInfo->bank_account_holder,
            'bank_swiftcode' =>  $bankInfo->swift,
            'bank_country'  =>  $bankInfo->countryName,
            'bank_address'    =>  $bankInfo->bank_address,
            'payment_type' =>'bank',
            'status'    =>  'process'
            ]);

        }else{

            $this->saveModel($this->model, [
            'member_id' =>  $member->id,
            'username'  =>  $member->username,
            'amount'    =>  $amount,
            'card_number'    => $debit['card_number'],
            'card_country'    => $debit['card_country'],
            'payment_type' =>'debit',
            'status'    =>  'process'
            ]);
        }
        

        $wallet->cash_point -= $amount;
        $wallet->save();
        
        $remark = 'Debited '.$amount.' for withdrawal';

        $remarkAry = array('amount'=>$amount);
        $remark_display = 'TH00010/'.json_encode($remarkAry);
        
        $this->saveWalletStatement($member->id, $member->username, $amount ,'Withdraw','C','D',$wallet->cash_point,$remark,$remark_display);
        
        return true;
    }
    public function saveWalletStatement ($fromMember, $toMember, $amount, $action_type, $wallet_type, $transaction_type, $balance, $remark, $remark_display) {//weilun
        if($wallet_type == 'R')
        {
            $ramount = $amount;
            $bamount = 0;
        }
        else
        {
            $ramount = 0;
            $bamount = $amount;
        }

        \DB::table('Member_Wallet_Statement')->insert([
            'member_id' => $fromMember,
            'username' => $toMember,
            'cash_amount' => $bamount,
            'register_amount' => $ramount,
            'action_type' => $action_type,
            'wallet_type' => $wallet_type,
            'transaction_type' => $transaction_type,
            'balance' =>  $balance,
            'remark' => $remark,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
            'remark_display' => $remark_display
        ]);

        return true;
    }
    public function saveWalletStatementForJackie ($fromMember, $toMember, $amount, $action_type, $wallet_type, $transaction_type, $balance, $remark, $remark_display) {//weilun
        if($wallet_type == 'R')
        {
            $ramount = $amount;
            $bamount = 0;
        }
        else
        {
            $ramount = 0;
            $bamount = $amount;
        }

        \DB::table('Member_Wallet_Statement')->insert([
            'member_id' => $fromMember,
            'username' => $toMember,
            't_amount' => $bamount,
            'register_amount' => $ramount,
            'action_type' => $action_type,
            'wallet_type' => $wallet_type,
            'transaction_type' => $transaction_type,
            'balance' =>  $balance,
            'remark' => $remark,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
            'remark_display' => $remark_display
        ]);

        return true;
    }
    public function update_wallet ($member,$f_member, $data) {//weilun


        $wallet = $member->wallet;
        $f_wallet = $f_member->wallet;


        //echo $f_wallet->cash_point."f<br>";
        if( $data["status"] == "done"){

            $wallet->t_wallet += $data["amount"];
            $wallet->save();
            $remark = 'Credited '.$data["amount"].' for withdrawal from member ID #'.$f_member->username;
            
            $remarkAry = array('amount'=>$data["amount"],'username'=>$f_member->username);
            $remark_display = 'TH00013/'.json_encode($remarkAry);

            $this->saveWalletStatementForJackie($member->id, $member->username, $data["amount"] ,'Withdraw','T','C',$wallet->t_wallet,$remark,$remark_display);

        }else{

            $f_wallet->cash_point += $data["amount"];
            $f_wallet->save();
            $remark = 'Credited '.$data["amount"].' for withdrawal reject';

            $remarkAry = array('amount'=>$data["amount"]);
            $remark_display = 'TH00012/'.json_encode($remarkAry);

            $this->saveWalletStatement($f_member->id, $f_member->username, $data["amount"] ,'Withdraw','C','C',$f_wallet->cash_point,$remark,$remark_display);

        }
        //echo $wallet->cash_point."<br>";
        //echo $f_wallet->cash_point."f<br>";

        return true;
    }

    /**
     * Withdraw List - DataTable
     * @param  App\Models\Member $member [description]
     * @return [type]         [description]
     */
    public function getList ($member) {
        return Datatables::eloquent($this->model->where('member_id', $member->id))
            ->editColumn('status', function ($model) {
                if ($model->status == 'done') return '<label class="label label-success">' . \Lang::get('common.status.done') . '</label>';
                elseif ($model->status == 'reject') return '<label class="label label-danger">' . \Lang::get('common.status.reject') . '</label>';
                else return '<label class="label label-default">' . \Lang::get('common.status.process') . '</label>';
            })
            ->editColumn('admin', function ($model) {
                return number_format($model->admin, 2);
            })
            ->editColumn('amount', function ($model) {
                return number_format($model->amount, 2);
            })
            ->rawColumns(['action', 'status'])
            ->make(true);
    }
    public function getWithdrawList ($id) {

        return Datatables::eloquent($this->model->where('member_id', $id)->orderBy('created_at', 'desc'))
            ->editColumn('status', function ($model) {
                if ($model->status == 'done') return '<span style="color:blue;">' . \Lang::get('common.status.done') . '</label>';
                elseif ($model->status == 'reject') return '<span style="color:red;">' . \Lang::get('common.status.reject') . '</label>';
                else return '<span style="color:orange;">' . \Lang::get('common.status.process') . '</label>';
            })
            ->editColumn('payment_type', function ($model) {
                if ($model->payment_type == 'debit') return \Lang::get('withdraw.Card_Payment');
                else  return \Lang::get('withdraw.Bank_Payment');
            })
            ->editColumn('admin', function ($model) {
                return number_format($model->admin, 2);
            })
            ->editColumn('amount', function ($model) {
                return number_format($model->amount, 2);
            })
            ->rawColumns(['status','payment_type'])
            ->make(true);
    }

    public function getWithdrawSystemList ($id) {

        $sql = \DB::table('deposit_log')
        ->where('deposit_log.currency_new','VND')
        ->where('deposit_log.wid',$id)
        ->where('deposit_log.type',2)
        ->get();
        $sql2 = $this->model->where('member_id', $id)->orderBy('created_at', 'desc')->get();
        $sql3 = $sql->merge($sql2)->sortByDesc('created_at');;

        return Datatables::of($sql3)
            ->editColumn('pay_id', function ($model) {

                if(empty($model->wid)){
                    return $model->member_id."_W".Carbon::createFromDate(substr($model->created_at, 0,4),substr($model->created_at, 5,2),substr($model->created_at, 8,2))->format('ymd').$model->id;
                }else{
                    
                    return $model->TransID;
                }

            })
            ->editColumn('cr8_at', function ($model) {

                if(empty($model->wid)){

                    return $model->created_at;

                }else{

                    $gettime = date("Y-m-d H:i:s", $model->created);
                    return date("Y-m-d H:i:s",strtotime($gettime. "-8 hours"));
                }

            })
            ->editColumn('amount', function ($model) {

                if(empty($model->wid)){

                    return number_format($model->amount, 2);

                }else{
                    return number_format($model->fundmoney, 2);
                }

            })
            ->editColumn('status', function ($model) {

                if(empty($model->wid)){
                    if ($model->status == 'done') return '<span style="color:blue;">' . \Lang::get('common.status.done') . '</label>';
                    elseif ($model->status == 'reject') return '<span style="color:red;">' . \Lang::get('common.status.reject') . '</label>';
                    else return '<span style="color:orange;">' . \Lang::get('common.status.process') . '</label>';
                }else{

                    if($model->status=="1" ){
                        if($model->xPayStatus != "0" || $model->xPayWithdrawal !="0"){
                            return '<div id='.$model->payid.'><span style="color:blue;">'.\Lang::get('withdraw.pending').'</span></div>';
                        }else{
                            return '<div id='.$model->payid.'><span style="color:blue;">'.\Lang::get('withdraw.Submitted').'</span></div>';

                        }
                    }elseif ($model->status=="3"){
                        return '<div id='.$model->payid.'><span style="color:red;"><id='.$model->payid.'>'.\Lang::get('withdraw.Cancelled').'</span></div>';
                    }elseif ($model->status=="9"){
                        return '<div id='.$model->payid.'><span style="color:red;"><id='.$model->payid.'>'.\Lang::get('withdraw.refund').'</span></div>';
                    }elseif ($model->status=="8"){
                        return '<div id='.$model->payid.'><span style="color:blue;"><id='.$model->payid.'>'.\Lang::get('withdraw.pending').'</span></div>';
                    }elseif ($model->status=="7"){
                        return '<div id='.$model->payid.'><span style="color:blue;"><id='.$model->payid.'>'.\Lang::get('withdraw.Invalid').'</span></div>';
                    }
                    elseif ($model->status=="2"){
                        return '<div id='.$model->payid.'><span style="color:green;"><id='.$model->payid.'>'.\Lang::get('withdraw.success').'</span></div>';
                    }
                }

            })
            ->editColumn('remark', function ($model) {

                if(empty($model->wid)){

                    return "-";

                }else{
                    if(empty($model->note)){
                        return "-";
                    }else{
                        return $model->note;
                    }
                }

            })
            // ->editColumn('payment_type', function ($model) {
            //     if ($model->payment_type == 'debit') return \Lang::get('withdraw.Card_Payment');
            //     else  return \Lang::get('withdraw.Bank_Payment');
            // })
            // ->editColumn('admin', function ($model) {
            //     return number_format($model->admin, 2);
            // })
            // ->editColumn('amount', function ($model) {
            //     return number_format($model->amount, 2);
            // })
            ->rawColumns(['status','pay_id','cr8_at','amount'])
            ->make(true);
    }

}
