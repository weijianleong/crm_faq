<?php

namespace App\Repositories;

use App\Models\Member;
use App\Models\MemberAccount;
use App\Models\MemberFundAccount;
use App\Models\MemberSelfAccount;
use App\Models\MemberDetail;
use App\Models\MemberWallet;
use App\Models\MemberWalletStatement;
use App\Models\MemberWalletStatementFund;
use App\Models\MemberShares;
use App\Models\MemberFreezeShares;
use App\Models\Users;
use App\Models\Trade;
use App\Repositories\PackageRepository;
use App\Repositories\BonusRepository;
use Yajra\Datatables\Facades\Datatables;
use Carbon\Carbon;
use DB;

    
    

class MemberRepository extends BaseRepository
{
    protected $model, $accountModel, $selfaccountModel, $fundaccountModel, $detailModel, $walletModel, $walletstatementModel,$walletstatamentfundModel, $sharesModel, $freezeSharesModel, $tradeModel,$userModel;
    protected $allowedFields = [];
    protected $booleanFields = [];

    public function __construct() {
        $this->model = new Member;
        $this->accountModel = new MemberAccount;
        $this->selfaccountModel = new MemberSelfAccount;
        $this->fundaccountModel = new MemberFundAccount;
        $this->detailModel = new MemberDetail;
        $this->walletModel = new MemberWallet;
        $this->walletstatamentModel = new MemberWalletStatement;
        $this->walletstatamentfundModel = new MemberWalletStatementFund;
        $this->sharesModel = new MemberShares;
        $this->freezeSharesModel = new MemberFreezeShares;
        $this->tradeModel = new Trade;
        $this->userModel = new Users;
    }

    public function saveModel($model, $data) {
        foreach ($data as $k=>$d) {
            $model->{$k} = $d;
        }
        $model->save();
        return $model;
    }

    public function store($data) {
        $model = $this->saveModel(new $this->model, $data);
        return $model;
    }

    public function update($model, $data) {
        $model = $this->saveModel($model, $data);
        return $model;
    }

    public function getAllowedFields () {
        return $this->allowedFields;
    }

    public function getBooleanFields () {
        return $this->booleanFields;
    }

    public function findById ($id) {
        return $this->model->where('id', $id)->first();
    }
    
    
    public function getSign()
    {
        $user = \Sentinel::getUser();
        $username = trim($user->username);
        $password = trim($user->password2);
        $sign = md5(md5($username).md5($password));
        
        return $sign;
    }
    public function getBalanceAPI($account)
    {
        $sign = $this->getSign();
        $url = config('misc.apiURL');
        $user = \Sentinel::getUser();
        $username = trim($user->username);
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //setup request to send json via POST
        $data = array(
                      'command' => 'GetBalance',
                      'username' => $username,
                      'bankcard' => $account,
                      'sign' => $sign,
    
                      );
        $payload = json_encode($data);
        
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //execute the POST request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
  
        //return $data;
        
        if($data->status_code == 200)
            return $data->result->balance;
        else
            return 0;

        

        
    }
    
    public function getBookBCredit($account)
    {
        $sign = $this->getSign();
        $url = config('misc.apiURL');
        $user = \Sentinel::getUser();
        $username = trim($user->username);
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //setup request to send json via POST
        $data = array(
                      'command' => 'GetBalance',
                      'username' => $username,
                      'bankcard' => $account,
                      'sign' => $sign,
                      
                      );
        $payload = json_encode($data);
        
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //execute the POST request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
        
        //return $data;
        
        if($data->status_code == 200)
            return $data->result->credit;
        else
            return 0;
        
        
        
        
    }
    
    public function getTradeStatus($account)
    {
        $sign = $this->getSign();
        $url = config('misc.apiURL');
        $user = \Sentinel::getUser();
        $username = trim($user->username);
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //setup request to send json via POST
        $data = array(
                      'command' => 'isEmptyHouse',
                      'username' => $username,
                      'bankcard' => $account,
                      'sign' => $sign,
                      
                      );
        $payload = json_encode($data);
        
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //execute the POST request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        //$data = json_decode($result);
        //return $data->result->balance;
        return $result;
        
        
        
        
    }
    
    public function getactiveMT5($account)
    {
        $sign = $this->getSign();
        $url = config('misc.apiURL');
        $user = \Sentinel::getUser();
        $username = trim($user->username);
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //setup request to send json via POST
        $data = array(
                      'command' => 'activeMt5no',
                      'username' => $username,
                      'bankcard' => $account,
                      'sign' => $sign,
                      
                      );
        $payload = json_encode($data);
        
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //execute the POST request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        //$data = json_decode($result);
        //return $data->result->balance;
        return $result;
        
        
        
        
    }
    
     public function getOrders($account)
    {
        $sign = $this->getSign();
        $url = config('misc.apiURL');
        $user = \Sentinel::getUser();
        $username = trim($user->username);
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //setup request to send json via POST
        $data = array(
                      'command' => 'findOrders',
                      'username' => $username,
                      'bankcard' => $account,
                      'sign' => $sign,
                       'starttime' => '20171201000000',
                       'endtime' => '20180331000000',
                      
                      );
        $payload = json_encode($data);
        
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //execute the POST request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
        //return $data->result->balance;
        return $data;
        
        
        
        
    }
    public function getBalanceAPI2($username, $password, $account)
    {
        $sign = md5(md5($username).md5($password));
        $url = config('misc.apiURL');
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //setup request to send json via POST
        $data = array(
                      'command' => 'GetBalance',
                      'username' => $username,
                      'bankcard' => $account,
                      'sign' => $sign,
    
                      );
        $payload = json_encode($data);
        
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //execute the POST request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
  
        //return $data;
        
        if($data->status_code == 200)
            return $data->result->balance;
        else
            return 0;

        

        
    }
    
    public function getTradeStatus2($username, $password, $account)
    {
        $sign = md5(md5($username).md5($password));
        $url = config('misc.apiURL');
       
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //setup request to send json via POST
        $data = array(
                      'command' => 'isEmptyHouse',
                      'username' => $username,
                      'bankcard' => $account,
                      'sign' => $sign,
                      
                      );
        $payload = json_encode($data);
        
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //execute the POST request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        //$data = json_decode($result);
        //return $data->result->balance;
        return $result;
        
        
        
        
    }
    
    public function getBookBCredit2($username, $password,$account)
    {
        $sign = md5(md5($username).md5($password));
        $url = config('misc.apiURL');
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //setup request to send json via POST
        $data = array(
                      'command' => 'GetBalance',
                      'username' => $username,
                      'bankcard' => $account,
                      'sign' => $sign,
                      
                      );
        $payload = json_encode($data);
        
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //execute the POST request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
        
        //return $data;
        
        if($data->status_code == 200)
            return $data->result->credit;
        else
            return 0;
        
        
        
        
    }
    public function getOrders2($username, $password, $account)
    {
       $sign = md5(md5($username).md5($password));
        $url = config('misc.apiURL');
        //$user = \Sentinel::getUser();
        //$username = trim($user->username);
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //setup request to send json via POST
        $data = array(
                      'command' => 'findOrders',
                      'username' => $username,
                      'bankcard' => $account,
                      'sign' => $sign,
                      'starttime' => '20171201000000',
                      'endtime' => '20180131000000',
                      
                      );
        $payload = json_encode($data);
        
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //execute the POST request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
        //return $data->result->balance;
        return $data;
        
        
        
        
    }
    
    public function updateBalanceAPI2($username, $password,$account,$amount)
    {
        
        
       $sign = md5(md5($username).md5($password));
        $url = config('misc.apiURL');
       
        //create a new cURL resource
        $ch = curl_init($url);
        
        //setup request to send json via POST
        $data = array(
                      'command' => 'UpdateBalance',
                      'username' => $username,
                      'bankcard' => $account,
                      'amount' => $amount,
                      'sign' => $sign,
                      
                      );
        $payload = json_encode($data);
        
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //execute the POST request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
        return $data;
        
        
        
        
    }
    
    public function updateCreditAPI2($username, $password,$account,$amount)
    {
        
        
        $sign = md5(md5($username).md5($password));
        $url = config('misc.apiURL');
        $lasttime = date("YmdHis");
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //setup request to send json via POST
        $data = array(
                      'command' => 'UpdateCredit',
                      'username' => $username,
                      'bankcard' => $account,
                      'amount' => $amount,
                      'lasttime' => $lasttime,
                      'sign' => $sign,
                      
                      );
        $payload = json_encode($data);
        
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //execute the POST request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
        return $data;
        
        
        
        
    }

    public function UpdateInfo($username,$credentials_1, $credentials_2,$sign)
    {
        
        $url = config('misc.apiURL');
        //$user = \Sentinel::getUser();
        //$username = trim($user->username);
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //setup request to send json via POST
        $data = array(
                      'command' => 'UpdateInfo',
                      'username' => $username,
                      'credentials_1' => $credentials_1,
                      'credentials_2' => $credentials_2,
                      'sign' => $sign,
                      
                      );
        $payload = json_encode($data);
        
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //execute the POST request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
        return $data;
        
        
    }
    
    public function UpdateBankInfo($username,$credentials_1, $credentials_2,$sign)
    {
        
        $url = config('misc.apiURL');
        //$user = \Sentinel::getUser();
        //$username = trim($user->username);
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //setup request to send json via POST
        $data = array(
                      'command' => 'UpdateInfo',
                      'username' => $username,
                      'credentials_1' => $credentials_1,
                      'credentials_2' => $credentials_2,
                      'sign' => $sign,
                      
                      );
        $payload = json_encode($data);
        
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //execute the POST request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
        return $data;
        
        
    }
    
    public function updatePasswordAPI($userpass)
    {
        
        $url = config('misc.apiURL');
        $user = \Sentinel::getUser();
        $username = trim($user->username);
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //setup request to send json via POST
        $data = array(
                      'command' => 'UpdatePass',
                      'username' => $username,
                      'userpass' => $userpass,
                      
                      );
        $payload = json_encode($data);
        
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //execute the POST request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
        return $data;
        
        
    }

    public function updateBalanceAPI($account,$amount)
    {
        
        
        $sign = $this->getSign();
        $url = config('misc.apiURL');
        $user = \Sentinel::getUser();
        $username = trim($user->username);
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //setup request to send json via POST
        $data = array(
                      'command' => 'UpdateBalance',
                      'username' => $username,
                      'bankcard' => $account,
                      'amount' => $amount,
                      'sign' => $sign,
                      
                      );
        $payload = json_encode($data);
        
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //execute the POST request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
        return $data;
        
        
        
        
    }
    
    public function updateCreditAPI($account,$amount)
    {
        
        
        $sign = $this->getSign();
        $url = config('misc.apiURL');
        $user = \Sentinel::getUser();
        $username = trim($user->username);
        $lasttime = date("YmdHis");
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //setup request to send json via POST
        $data = array(
                      'command' => 'UpdateCredit',
                      'username' => $username,
                      'bankcard' => $account,
                      'amount' => $amount,
                      'lasttime' => $lasttime,
                      'sign' => $sign,
                      
                      );
        $payload = json_encode($data);
        
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //execute the POST request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
        return $data;
        
        
        
        
    }
    

    
    public function addMT5API($groupname, $lever, $bankcard)
    {
        $sign = $this->getSign();
        $url = config('misc.apiURL');
        $user = \Sentinel::getUser();
        $username = trim($user->username);
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //setup request to send json via POST
        $data = array(
                      'command' => 'addAccount',
                      'username' => $username,
                      'sign' => $sign,
                      'groupname' => $groupname,
                      'lever' => $lever,
                      'bankcard' => $bankcard
                      
                      );
        $payload = json_encode($data);
        
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //execute the POST request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
        return $data;
        
        
        
        
    }
    
    public function addMember($username, $password, $member_name, $sponsor_id, $ic_number, $gender, $email, $phone, $country, $address, $ac1, $ac2, $credentials_1, $credentials_2)
    {
        
        $url = config('misc.apiURL');
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //setup request to send json via POST
        $data = array(
                      'command' => 'SetLogin',
                      'username' => $username,
                      'password' => $password,
                      'member_name' => $member_name,
                      'sponsor_id' => $sponsor_id,
                      'ic_number' => $ic_number,
                      'gender' => $gender,
                      'email' => $email,
                      'phone' => $phone,
                      'country' => $country,
                      'address' => $address,
                      'ac1' => $ac1,
                      'ac2' => $ac2,
                      'credentials_1' => $credentials_1,
                      'credentials_2' => $credentials_2
                      
                      );
        $payload = json_encode($data);
        
        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //execute the POST request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
        return $data;
        
        
        
        
    }

    public function NewUserAPI($firstname, $email, $groupname, $lever, $bankcard, $password)
    {
        //$sign = $this->getSign();
        $url = config('misc.newapiURL').'NewUser';
        $header2 = array('TimeOutSeconds' => '60');
        
        $header = array();
        $header[] = 'Content-type: application/json';
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //The JSON data.
        $jsonData = array(
                          'Header' => $header2,
                          'Login' => $bankcard,
                          'Email' => $email,
                          'Name' => $firstname,
                          'Group' => $groupname,
                          'Leverage' => $lever,
                          'MasterPassword' => $password,
                          'InvestorPassword' => $password,
                          'UserRights' => 'ENABLED',
                       
                          );
        
        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);
        
        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);
        
        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        
        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //Execute the request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
        
        return $data;
        
        
        
        
    }
    
    public function AccountAPI($bankcard)
    {
        //$sign = $this->getSign();
        $url = config('misc.newapiURL').'Account';
        $header2 = array('TimeOutSeconds' => '60');
        
        $header = array();
        $header[] = 'Content-type: application/json';
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //The JSON data.
        $jsonData = array(
                          'Header' => $header2,
                          'Login' => $bankcard,
                          
                          );
        
        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);
        
        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);
        
        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        
        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //Execute the request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
        
        return $data;
        
        
        
        
    }
    
    public function HistoryAPI($bankcard)
    {
        //$sign = $this->getSign();
        $url = config('misc.newapiURL2').'HistoryPositions';
        $header2 = array('TimeOutSeconds' => '60');
        
        $header = array();
        $header[] = 'Content-type: application/json';
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //The JSON data.
        $jsonData = array(
                          'Header' => $header2,
                          'Login' => $bankcard,
                          'From' => '2019-01-01',
                          'To' => '2019-12-31',
                          
                          );
        
        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);
        
        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);
        
        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        
        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //Execute the request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
        
        return $data;
        
        
        
        
    }
    
    public function AccountMoveoutAPI($bankcard)
    {
        //$sign = $this->getSign();
        $url = config('misc.newapiURL3').'AccountMoveout';
        $header2 = array('TimeOutSeconds' => '60');
        
        $header = array();
        $header[] = 'Content-type: application/json';
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //The JSON data.
        $jsonData = array(
                          'Login' => $bankcard,
                          
                          );
        
        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);
        
        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);
        
        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        
        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //Execute the request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
        
        return $data;
        
        
        
        
    }
    
    public function AccountMoveGroupAPI($bankcard, $newgroup)
    {
        //$sign = $this->getSign();
        $url = config('misc.newapiURL4').'AccountMoveoutWithGroup';
        $header2 = array('TimeOutSeconds' => '60');
        
        $header = array();
        $header[] = 'Content-type: application/json';
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //The JSON data.
        $jsonData = array(
                          'Login' => $bankcard,
                          'MoveInGroup' => $newgroup,
                          );
        
        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);
        
        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);
        
        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        
        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //Execute the request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
        
        return $data;
        
        
        
        
    }
    
    public function BalanceAPI($account,$amount,$actiontype,$comment)
    {
        //$sign = $this->getSign();
        $url = config('misc.newapiURL').'Balance';
        $header2 = array('TimeOutSeconds' => '60');
        
        $header = array();
        $header[] = 'Content-type: application/json';
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //The JSON data.
        $jsonData = array(
                          'Header' => $header2,
                          'Login' => $account,
                          'Amount' => $amount,
                          'IsDeposit' => $actiontype,
                          'Comment' => $comment,
                          
                          );
        
        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);
        
        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);
        
        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        
        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //Execute the request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
        
        return $data;
        
        
        
        
    }
    
    public function BalanceAPI2($account,$amount,$comment)
    {
        //$sign = $this->getSign();
        $url = config('misc.newapiURL').'BalanceChange';
        $header2 = array('TimeOutSeconds' => '60');
        
        $header = array();
        $header[] = 'Content-type: application/json';
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //The JSON data.
        $jsonData = array(
                          'Header' => $header2,
                          'Login' => $account,
                          'Amount' => $amount,
                          'Comment' => $comment,
                          
                          );
        
        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);
        
        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);
        
        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        
        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //Execute the request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
        
        return $data;
        
        
        
        
    }
    
    public function CreditAPI($account,$amount,$comment)
    {
        //$sign = $this->getSign();
        $url = config('misc.newapiURL').'Credit';
        $header2 = array('TimeOutSeconds' => '60');
        
        $header = array();
        $header[] = 'Content-type: application/json';
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //The JSON data.
        $jsonData = array(
                          'Header' => $header2,
                          'Login' => $account,
                          'Amount' => $amount,
                          'Comment' => $comment,
                          
                          );
        
        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);
        
        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);
        
        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        
        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //Execute the request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
        
        return $data;
        
        
        
        
    }
    
    public function UpdateMasterPasswordAPI($bankcard, $password)
    {
        //$sign = $this->getSign();
        $url = config('misc.newapiURL').'UserPassword';
        $header2 = array('TimeOutSeconds' => '60');
        
        $header = array();
        $header[] = 'Content-type: application/json';
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //The JSON data.
        $jsonData = array(
                          'Header' => $header2,
                          'Login' => $bankcard,
                          'MainPassword' => $password,
                          
                          );
        
        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);
        
        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);
        
        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        
        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //Execute the request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
        
        return $data;
        
        
        
        
    }
    
    public function UpdateInvestorPasswordAPI($bankcard, $password)
    {
        //$sign = $this->getSign();
        $url = config('misc.newapiURL').'UserPassword';
        $header2 = array('TimeOutSeconds' => '60');
        
        $header = array();
        $header[] = 'Content-type: application/json';
        
        //create a new cURL resource
        $ch = curl_init($url);
        
        //The JSON data.
        $jsonData = array(
                          'Header' => $header2,
                          'Login' => $bankcard,
                          'InvestorPassword' => $password,
                          
                          );
        
        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);
        
        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);
        
        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        
        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //Execute the request
        $result = curl_exec($ch);
        
        //close cURL resource
        curl_close($ch);
        
        $data = json_decode($result);
        
        return $data;
        
        
        
        
    }
    /**
     * Find by username
     * @param  string $username
     * @return object
     */
    public function findByUsername ($username) {
        return $this->model->where('username', trim($username))->first();
    }

    /**
     * Find children (not direct)
     * @param  App\Models\Member $member
     * @return object
     */
    public function findChildren ($member) {
        // return $this->model->where('parent_id', $member->id)->get();
        $children = \Cache::remember('member.' . $member->id . '.children', 3600, function () use ($member) {
            return $this->model->where('parent_id', $member->id)->orderBy('position', 'asc')->get();
        });
        return $children;
    }

    /**
     * Find direct downline
     * @param  App\Models\Member $member
     * @return object
     */
    public function findDirect ($member) {
        return $this->model->where('direct_id', $member->id)->get();
    }
    
    public function findIC ($ic) {
        return $this->detailModel->where('identification_number', $ic)->first();
    }
    public function findUser ($email) {
        return $this->userModel->where('email', $email)->first();
    }

    /**
     * All Members - DataTable
     * @param  boolean $table
     * @return object
     */
    public function findAll ($table=false) {
        

        if (!$table) return $this->model->all();
        else {
            
            return Datatables::eloquent($this->model->where('void',0)->with('user','detail','package'))
                ->addColumn('action', function ($model) {
                    return view('back.member.action')->with('model', $model);
                })
            

            
                ->editColumn('direct', function ($model) {
                    if ($direct = $model->direct()) return $direct->username;
                    else return 'None';
                })
           
                ->editColumn('status', function ($model) {
                     if ($model->process_status ==  'Y') return 'Active';
                         else return 'Inactive';
                 })

                ->editColumn('lpoa', function ($model) {

                    if ($model->lpoa ==  '') return '-';

                    else{

                        if ($model->lpoa_status ==  'Y') return 'Approved';
                         else return 'Pending';

                    }

                     
                 })
                ->editColumn('is_ban', function ($model) {

                    if ($model->user->is_ban ==  '1') return '-';
                    else{
                        return 'Ban';

                    }

                     
                 })
                ->editColumn('password', function ($model) {
                         return $model->user->password2;
                })
            
    
            
                ->editColumn('package_amount', function ($model) {
                    return number_format($model->package_amount, 2);
                })
                ->make(true);
          
        }
    }
    
 public function findPending ($table=false) {
     

        if (!$table) return $this->model->all();
        else {
            
            return Datatables::eloquent($this->model->with('user','detail')->where('void',0)->Where('process_status', 'N')->orderBy('created_at', 'ASC'))
                ->addColumn('action', function ($model) {
                    return view('back.member.action')->with('model', $model);
                })
            

            
                ->make(true);
          
        }
    }

    /**
     * All Member Wallets - DataTable
     * @param  boolean $table
     * @return object
     */
    public function findWalletList ($table=false) {
        if (!$table) return $this->walletModel->all();
        else {
            return Datatables::eloquent($this->walletModel->with('member'))
                ->addColumn('username', function ($model) {
                    return $model->member->username;
                })
                ->addColumn('action', function ($model) {
                    return view('back.wallet.action')->with('model', $model);
                })
                ->editColumn('cash_point', function ($model) {
                    return number_format($model->cash_point, 2);
                })
                ->editColumn('register_point', function ($model) {
                    return number_format($model->register_point, 2);
                })
                ->editColumn('promotion_point', function ($model) {
                    return number_format($model->promotion_point, 2);
                })
                ->editColumn('purchase_point', function ($model) {
                    return number_format($model->purchase_point, 2);
                })
                ->editColumn('md_point', function ($model) {
                    return number_format($model->md_point, 2);
                })
                ->make(true);
        }
    }
    
    public function findAccountList ($table=false, $memberid) {
        
        
        
        if (!$table) return $this->accountModel->all();
        else {
            return Datatables::eloquent($this->accountModel->Where('member_id', $memberid)->orderBy('id', 'ASC'))
            
                                          
            ->editColumn('balanceA', function ($model) {
                          $balanceA = $this->AccountAPI($model->bookA);
                          return number_format($balanceA->balance, 2);
                          // return number_format($model->balanceA, 2);
                         })
            ->editColumn('balanceB', function ($model) {
                          $balanceB = $this->AccountAPI($model->bookB);
                          return number_format($balanceB->balance, 2);
                         //return number_format($model->balanceB, 2);
                         })
            ->editColumn('status', function ($model) {
                         switch ($model->status) {
                                 case -2:
                         return \Lang::get('misc.status.t');
                                 break;
                                 case -1:
                         return \Lang::get('misc.status.n');
                                 break;
                                 case 0:
                         return \Lang::get('misc.status.0');
                                 break;
                                 case 1:
                         return \Lang::get('misc.status.1');
                                 break;
                                 case 2:
                         return \Lang::get('misc.status.2');
                                 break;
                                 case 3:
                         return \Lang::get('misc.status.3');
                                 break;
                                 case 4:
                         return \Lang::get('misc.status.4');
                                 break;
                                 case 5:
                         return \Lang::get('misc.status.5');
                                 break;
                                 case 6:
                         return \Lang::get('misc.status.6');
                                 break;
                                 case 7:
                         return \Lang::get('misc.status.7');
                                 break;
                                 case 8:
                         return \Lang::get('misc.status.8');
                                 break;
                                 case 9:
                         return \Lang::get('misc.status.9');
                         break;
                         
                                 }
                         })
            
            ->editColumn('lockdate', function ($model) {
                         if($model->winB == 9)
                             return '--:--';
                         else
                             return $model->lockdate;
                         })
            
            ->make(true);
        }
    }
    
    public function findSelfAccountList ($table=false, $memberid) {
        
        
        
        if (!$table) return $this->selfaccountModel->all();
        else {
            return Datatables::eloquent($this->selfaccountModel->Where('member_id', $memberid)->orderBy('id', 'ASC'))
            
            
            ->editColumn('balanceA', function ($model) {
                          $balanceA = $this->AccountAPI($model->bookA);
                          return number_format($balanceA->balance, 2);
                          // return number_format($model->balanceA, 2);
                         })
           
            
            ->make(true);
        }
    }
    public function findFundAccountList ($table=false, $memberid) {
   
        
        if (!$table) return $this->fundaccountModel->all();
        else {
            // $model = DB::table('Member_Account_Fund')
            //         ->where('member_id', $memberid)
            //         ->where('balanceB', '>', 0)
            //         ->orWhere('id', $freeAccount->id)
            //         ->orWhere(function ($query) use($memberid) {
            //             $query->where('member_id', $memberid)
            //                   ->where('winA', '1');
            //         })
            //         ->orderBy('balanceB', 'ASC')
            //         ->orderBy('winA', 'DESC')
            //         ->orderBy('id', 'ASC');

            $accountID = '';
            $winAccount = DB::table('Member_Account_Fund')->where('member_id', '=', $memberid)->where('balanceB', 0)->where('winA', 1)->first(['id']);

            if(count($winAccount) > 0) $accountID = $winAccount->id;
            else{
                $freeAccount = DB::table('Member_Account_Fund')->where('member_id', '=', $memberid)->where('balanceB', 0)->where('winA', 0)->first(['id']);
                if(count($freeAccount) > 0) $accountID = $freeAccount->id;
            }

            if($accountID){
                    $model = DB::table('Member_Account_Fund')
                    ->where('member_id', $memberid)
                    ->where('balanceB', '>', 0)
                    ->orWhere('id', $accountID)
                    ->orderByRaw("CASE WHEN balanceB = 0 THEN 0 ELSE 1 END")
                    ->orderBy('id', 'ASC');
            }else{
                $model = DB::table('Member_Account_Fund')
                    ->where('member_id', $memberid)
                    ->where('balanceB', '>', 0)
                    ->orderByRaw("CASE WHEN balanceB = 0 THEN 0 ELSE 1 END")
                    ->orderBy('id', 'ASC');
            }

            return Datatables::of($model)
            
            ->addColumn('action', function ($model) {
                        return view('front.misc.action')->with('model', $model);
                        })
            
            ->editColumn('balanceA', function ($model) {
                         // $balanceA = $this->AccountAPI($model->bookA);
                         // return number_format($balanceA->balance, 2);
                         return number_format($model->rbalance, 2);
                         })
            ->editColumn('balanceB', function ($model) {
                         //$balanceB = $this->AccountAPI($model->bookB);
                         //return number_format($balanceB->balance, 2);
                         return number_format($model->balanceB, 2);
                         })
            ->editColumn('status', function ($model) {
                         switch ($model->status) {
                         case -2:
                         return \Lang::get('misc.status.t');
                         break;
                         case -1:
                         return \Lang::get('misc.status.n');
                         break;
                         case 0:
                         return \Lang::get('misc.status.0');
                         break;
                         case 1:
                         return \Lang::get('misc.status.1');
                         break;
                         case 2:
                         return \Lang::get('misc.status.2');
                         break;
                         case 3:
                         return \Lang::get('misc.status.3');
                         break;
                         case 4:
                         return \Lang::get('misc.status.4');
                         break;
                         case 5:
                         return \Lang::get('misc.status.5');
                         break;
                         case 6:
                         return \Lang::get('misc.status.6');
                         break;
                         case 7:
                         return \Lang::get('misc.status.7');
                         break;
                         case 8:
                         return \Lang::get('misc.status.8');
                         break;
                         case 9:
                         return \Lang::get('misc.status.9');
                         break;
                         
                         }
                         })
            
            
            ->editColumn('request_unsubscribe', function ($model) {
                            if(isset($model->cancel_request_unsubscribe)) return '';
                            else return $model->request_unsubscribe_date;
                            })


            ->editColumn('txndate', function ($model) {
                         
                         $opentime = '';
                         $opentype = '';
                         
                         $orders = DB::table('Orders_Fund')->where('bookA', '=', $model->bookA)->where('status', '<>', 'X')->orderBy('bookAorderid', 'desc')->first();
                         
                         if(count($orders) > 0)
                         {
                             if($orders->status == 'O')
                             {
                                 $tradestatus = DB::connection('mysql2')->table('opentrade')->where('Login', '=', $model->bookA)->orderBy('Order', 'desc')->first();
                         
                         
                                 if(count($tradestatus) > 0)
                                     $opentime = $tradestatus->OpenTime;
                             }
                             else
                             {
                                 if(empty($orders->opentime))
                                 {
                                     $orderAP = DB::connection('mysql2')->table('historyposition')->where('Login', '=', $model->bookA)->orderBy('OrderId', 'desc')->first();
                         
                                     if($orderAP->Cmd == 'SELL')
                                         $tradecommand = 'BUY';
                                     else
                                         $tradecommand = 'SELL';
                                     $orderid = $orderAP->OrderId;
                                     DB::table('Orders_Fund')->where('bookAorderid', $orderid)
                                     ->update([
                                              'opentime' => $orderAP->OpenTime,
                                              'openprice' => $orderAP->OpenPrice,
                                              'tradecommand' => $tradecommand,
                                              'symbol' => $orderAP->Symbol,
                                              'lotsize' => $orderAP->Volume/10000,
                                              'fundcompany' => $orderAP->PartnerId,
                                              ]);
                                     $opentime = $orderAP->OpenTime;
                                 }
                                 else
                                     $opentime = $orders->opentime;
                             }
                         }
                         
                         
                         if($model->status == -2)
                             return \Lang::get('misc.status.t');
                         else
                             return $opentime;
                         
                   
                         })
            ->editColumn('bookAtxn', function ($model) {
                         
                         $Adetails = '';
                         
                         $orders = DB::table('Orders_Fund')->where('bookA', '=', $model->bookA)->where('status', '<>', 'X')->orderBy('bookAorderid', 'desc')->first();
                         
                         if(count($orders) > 0)
                         {
                             if($orders->status == 'O')
                             {
                                // $tradestatus = DB::connection('mysql2')->table('opentrade')->where('Login', '=', $model->bookA)->orderBy('Order', 'desc')->first();
                         
                         
                                // if(count($tradestatus) > 0)
                                //     $Adetails = $tradestatus->TradeCommand;
                                 $Adetails = 'Open';
                             }
                             else
                             {
                                 $Adetails = $orders->tradecommand.' - '.$orders->symbol;
                             }
                         }
                         
                         
                         if($Adetails == 'Open' || $model->status == -2)
                             return \Lang::get('misc.status.t');
                         else
                             return $Adetails;
                         
                         /*
                         switch ($model->status) {
                         case -2:
                         return \Lang::get('misc.status.t');
                         break;
                         case -1:
                         return \Lang::get('misc.status.n');
                         break;
                         case 0:
                         return \Lang::get('misc.status.0');
                         break;
                         }
                          */
                         
                         })
            
            ->editColumn('bookBtxn', function ($model) {
                         
                         $Bdetails = '';
                         
                          $orders = DB::table('Orders_Fund')->where('bookA', '=', $model->bookA)->where('status', '<>', 'X')->orderBy('bookAorderid', 'desc')->first();
                         
                         if(count($orders) > 0)
                         {
                             if($orders->status == 'O')
                             {
                                  $tradestatus = DB::connection('mysql2')->table('opentrade')->where('Login', '=', $model->bookA)->orderBy('Order', 'desc')->first();
                       
                         
                         
                                 if(count($tradestatus) > 0)
                                 {
                                     if($tradestatus->TradeCommand == 'BUY')
                                             $tradecommand = 'SELL';
                                     else
                                         $tradecommand = 'BUY';
                         
                                     if(substr($tradestatus->Symbol, -1) == '+')
                                         $symbol = str_replace('+', '-', $tradestatus->Symbol);
                                     else
                                         $symbol = str_replace('-', '+', $tradestatus->Symbol);
                         
                                     $Bdetails = $tradecommand;
                                 }
                         
                                     $Bdetails = 'Open';
                             }
                             else
                             {
                                     if($orders->tradecommand == 'BUY')
                                             $tradecommand = 'SELL';
                                     else
                                         $tradecommand = 'BUY';
                         
                                     if(substr($orders->symbol, -1) == '+')
                                         $symbol = str_replace('+', '-', $orders->symbol);
                                     else
                                         $symbol = str_replace('-', '+', $orders->symbol);
                         
                                     $Bdetails = $tradecommand.' - '.$symbol;
                             }
                         }
                         
                         if($Bdetails == 'Open' || $model->status == -2)
                             return \Lang::get('misc.status.t');
                         else
                             return $Bdetails;
                         
                         /*
                         switch ($model->status) {
                         case -2:
                         return \Lang::get('misc.status.t');
                         break;
                         case -1:
                         return \Lang::get('misc.status.n');
                         break;
                         case 0:
                         return \Lang::get('misc.status.0');
                         break;
                         }
                          */
                         
                         })
            
            ->editColumn('bookAwin', function ($model) {

                         if($model->status == -2) return '';
                         
                         $Awin = '';
                         
                         $orders = DB::table('Orders_Fund')->where('bookA', '=', $model->bookA)->where('status', '<>', 'X')->orderBy('bookAorderid', 'desc')->first();
                         
                         if(count($orders) > 0)
                         {
                             if($orders->profitA <> 0)
                                 $Awin = $orders->profitA.' ('.\Lang::get('common.cal.swap'). ' : '.$orders->swapA.')';
                         }
                         
                         return $Awin;
                         
                         })
  
             ->editColumn('fundcompany', function ($model) {
                       
                      if($model->system_manage == 'N')
                          $partnername = '';
                      else
                      {
                       if($model->fundcompany == 0)
                           $partnername = '公司决定 (Company Decide)';
                       else
                       {
                         $partner = DB::connection('mysql3')->table('user')->where('UserId', '=', $model->fundcompany)->first();
                          
                          $current_lang = \App::getLocale();
                          
                          if($current_lang == 'en')
                              $partnername = $partner->FirstName.' '.$partner->LastName;
                          else
                              $partnername = $partner->ChineseName;
                       }
                      }
                       
                       
                         return $partnername;
                       
                         })
            ->make(true);
        }
    }
    
     public function findFundTradeAccountList ($table=false, $memberid) {
        
        
        
        if (!$table) return $this->fundaccountModel->all();
        else {
            return Datatables::eloquent($this->fundaccountModel->Where('member_id', $memberid)->Where('member_id', $memberid)->orderBy('id', 'DESC'))
            ->addColumn('action', function ($model) {
                        return '<button class="btn btn-primary glow_button" onclick="getTradeDetails('.$model->bookA.')"><i class="fa fa-eye"></i> '.\Lang::get('misc.details').'</button>';
                        })
            
            ->editColumn('balanceA', function ($model) {
                         $balanceA = $this->AccountAPI($model->bookA);
                         return number_format($balanceA->balance, 2);
                         // return number_format($model->balanceA, 2);
                         })
            ->editColumn('balanceB', function ($model) {
                         //$balanceB = $this->AccountAPI($model->bookB);
                         //return number_format($balanceB->balance, 2);
                         return number_format($model->balanceB, 2);
                         })
           
            ->editColumn('startdate', function ($model) {
                         if($model->winB == 9)
                         return '--:--';
                         else
                         return $model->subscribe_date;
                         })
            
            ->editColumn('enddate', function ($model) {
                         if($model->winB == 9)
                         return '--:--';
                         else
                         return $model->unsubscribe_date;
                         })
            
            ->make(true);
        }
    }
    public function findTradeList ($table=false, $memberid) {
        
        
            return Datatables::eloquent($this->tradeModel->Where('member_id', $memberid)->orderBy('id', 'DESC'))
            
        ->editColumn('trade_type', function ($model) {
                     if($model->trade_type == 'B')
                            return \Lang::get('transfer.br.title');
                     else
                         return \Lang::get('transfer.sr.title');
                     })
        
        ->editColumn('status', function ($model) {
                     if($model->status == 'Done')
                     return \Lang::get('misc.Done');
                     else
                     return $model->amount_complete.'/'.$model->amount;
                     })

            
            ->make(true);
       
    }

     public function findRegisterWalletStatementList ($table=false, $memberid) {

     
         
        if (!$table) return $this->walletModel->all();
        else {
            return Datatables::eloquent($this->walletstatamentModel->where('wallet_type', 'R')->Where('member_id', $memberid)->orderBy('created_at', 'ASC')->orderBy('id', 'ASC'))
            
            
            
                ->editColumn('credit', function ($model) {
                         if ($model->transaction_type ==  'C') return number_format($model->register_amount, 2);
                         else return number_format(0, 2);
                         })
            ->editColumn('debit', function ($model) {
                         if ($model->transaction_type ==  'D') return number_format($model->register_amount, 2);
                         else return number_format(0, 2);
                         })


                ->make(true);
        }
    }

    public function find5PercentStatementList ($table=false, $memberid) {

     
        
        if (!$table) return $this->walletModel->all();
        else {
            return Datatables::eloquent($this->walletstatamentfundModel->where('action_type', 'Adjustment 5 Percent')->Where('member_id', $memberid)->orderBy('created_at', 'DESC')->orderBy('id', 'DESC'))
            
                ->editColumn('credit', function ($model) {
                         if ($model->transaction_type ==  'C') return number_format($model->w_amount, 2);
                         else return number_format(0, 2);
                         })
                ->editColumn('debit', function ($model) {
                         if ($model->transaction_type ==  'D') return number_format($model->w_amount, 2);
                         else return number_format(0, 2);
                         })
                ->editColumn('remark', function ($model) use($memberid) {
                        if(!(string)$model->remark_display){
                            return $model->remark;
                        }
                        else{
                            $split = explode("/", $model->remark_display);
                            if(\Lang::get('transactionHistory.'.$split[0]) == ('transactionHistory.'.$split[0])){
                                $remark = $model->remark_display;
                            }
                            else{
                                $remark = \Lang::get('transactionHistory.'.$split[0]);
                                if($split[1]){
                                    $detail = json_decode($split[1]);
                                    foreach ($detail as $key => $value) {
                                        if($key == "amount") $remark = str_replace('%%'.$key.'%%', number_format($value,2), $remark);
                                        else if($key == "date") $remark = str_replace('%%'.$key.'%%', date_format($value, "Y-m-d"), $remark);
                                        else $remark = str_replace('%%'.$key.'%%', $value, $remark);
                                    }
                                }
                            }
                            return $remark;
                        }
                        })


                ->make(true);
        }
    }
    
    public function findWWalletStatementList ($table=false, $memberid) {
        
        
        
        if (!$table) return $this->walletModel->all();
        else {
            return Datatables::eloquent($this->walletstatamentfundModel->where('wallet_type', 'W')->Where('member_id', $memberid)->Where('parent_batch_id', 0)->orderBy('created_at', 'DESC')->orderBy('id', 'DESC'))
            ->addColumn('action', function ($model) {
                         if($model->batch_id) return '<button class="btn btn-primary glow_button" onclick="getStatementDetails('.$model->id.')"><i class="fa fa-eye"></i> '.\Lang::get('misc.view').'</button>';
                         else return "";
                         })
            ->editColumn('credit', function ($model) {
                         if ($model->transaction_type ==  'C') return number_format($model->w_amount, 2);
                         else return number_format(0, 2);
                         })
            ->editColumn('debit', function ($model) {
                         if ($model->transaction_type ==  'D') return number_format($model->w_amount, 2);
                         else return number_format(0, 2);
                         })
            ->editColumn('balance', function ($model) {
                         return number_format($model->balance, 2);
                         })
            ->editColumn('remark', function ($model) use($memberid) {
                        if(!(string)$model->remark_display){
                            return $model->remark;
                        }
                        else{
                            $split = explode("/", $model->remark_display);
                            if(\Lang::get('transactionHistory.'.$split[0]) == ('transactionHistory.'.$split[0])){
                                $remark = $model->remark_display;
                            }
                            else{
                                $remark = \Lang::get('transactionHistory.'.$split[0]);
                                if($split[1]){
                                    $detail = json_decode($split[1]);
                                    foreach ($detail as $key => $value) {
                                        if($key == "amount") $remark = str_replace('%%'.$key.'%%', number_format($value,2), $remark);
                                        else if($key == "date") $remark = str_replace('%%'.$key.'%%', date_format($value, "Y-m-d"), $remark);
                                        else $remark = str_replace('%%'.$key.'%%', $value, $remark);
                                    }
                                }
                            }
                            return $remark;
                        }
                        })
            
            ->rawColumns(['action'])
            ->make(true);
        }
    }
    
    public function findBWalletStatementList ($table=false, $memberid) {
        
        
        
        if (!$table) return $this->walletModel->all();
        else {
            return Datatables::eloquent($this->walletstatamentfundModel->where('wallet_type', 'BW')->Where('member_id', $memberid)->orderBy('created_at', 'DESC')->orderBy('id', 'DESC'))
            
            
            
            ->editColumn('credit', function ($model) {
                         if ($model->transaction_type ==  'C') return number_format($model->b_amount, 2);
                         else return number_format(0, 2);
                         })
            ->editColumn('debit', function ($model) {
                         if ($model->transaction_type ==  'D') return number_format($model->b_amount, 2);
                         else return number_format(0, 2);
                         })
            ->editColumn('balance', function ($model) {
                         return number_format($model->balance, 2);
                         })
            ->editColumn('remark', function ($model) use($memberid) {
                        if(!(string)$model->remark_display){
                            return $model->remark;
                        }
                        else{
                            $split = explode("/", $model->remark_display);
                            if(\Lang::get('transactionHistory.'.$split[0]) == ('transactionHistory.'.$split[0])){
                                $remark = $model->remark_display;
                            }
                            else{
                                $remark = \Lang::get('transactionHistory.'.$split[0]);
                                if($split[1]){
                                    $detail = json_decode($split[1]);
                                    foreach ($detail as $key => $value) {
                                        if($key == "amount") $remark = str_replace('%%'.$key.'%%', number_format($value,2), $remark);
                                        else if($key == "date") $remark = str_replace('%%'.$key.'%%', date_format($value, "Y-m-d"), $remark);
                                        else $remark = str_replace('%%'.$key.'%%', $value, $remark);
                                    }
                                }
                            }
                            return $remark;
                        }
                        })
            
            
            ->make(true);
        }
    }
    
    public function findCashWalletStatementList ($table=false, $memberid) {

     
        
        if (!$table) return $this->walletModel->all();
        else {
            return Datatables::eloquent($this->walletstatamentModel->where('wallet_type', 'C')->Where('member_id', $memberid)->orderBy('created_at', 'DESC')->orderBy('id', 'DESC'))
            
                ->editColumn('credit', function ($model) {
                         if ($model->transaction_type ==  'C') return number_format($model->cash_amount, 2);
                         else return number_format(0, 2);
                         })
                ->editColumn('debit', function ($model) {
                         if ($model->transaction_type ==  'D') return number_format($model->cash_amount, 2);
                         else return number_format(0, 2);
                         })
                ->editColumn('balance', function ($model) {
                         return number_format($model->balance, 2);
                         })
                ->editColumn('remark', function ($model) use($memberid) {
                        if(!(string)$model->remark_display){
                            return $model->remark;
                        }
                        else{
                            $split = explode("/", $model->remark_display);
                            if(\Lang::get('transactionHistory.'.$split[0]) == ('transactionHistory.'.$split[0])){
                                $remark = $model->remark_display;
                            }
                            else{
                                $remark = \Lang::get('transactionHistory.'.$split[0]);
                                if($split[1]){
                                    $detail = json_decode($split[1]);
                                    foreach ($detail as $key => $value) {
                                        if($key == "amount") $remark = str_replace('%%'.$key.'%%', number_format($value,2), $remark);
                                        else if($key == "date") $remark = str_replace('%%'.$key.'%%', date_format($value, "Y-m-d"), $remark);
                                        else if($key == "coin"){
                                            $coinName = \DB::table('Capx_Margin')->where('symbol', $value)->first();
                                            if(count($coinName) > 0){
                                                $displayName = \App::getLocale() == 'en' ? $coinName->name_en : $coinName->name_chs;
                                                $remark = str_replace('%%'.$key.'%%', $displayName, $remark);
                                            }
                                            else {
                                                $remark = str_replace('%%'.$key.'%%', \Lang::get('capx.'.$value), $remark);
                                            }
                                        }
                                        else $remark = str_replace('%%'.$key.'%%', $value, $remark);
                                    }
                                }
                            }
                            return $remark;
                        }
                        })


                ->make(true);
        }
    }
    
    public function findDirectSalesList ($table=false, $memberid) {
        
        
           return Datatables::eloquent($this->model->Where('direct_id', $memberid))
        
            ->editColumn('username', function ($model) {
                     return $model->username;
                     })
        
            ->editColumn('firstname', function ($model) {
                     return $model->user->first_name;
                     })
            
            ->editColumn('rank', function ($model) {
                    $rank = \DB::table('Member_Sales')->where('member_id', '=',$model->id)->first();
                    if(isset($rank)) return $rank->rank;
                    else return '';
                    })
        
            ->editColumn('totaldeposit', function ($model) {
                    $totaldeposit1 = \DB::table('Member_Wallet_Statement')->where('member_id', '=',$model->id)->where('action_type', '=', 'Deposit')->where('created_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('created_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->sum('cash_amount');
                            
                    $totaltransfer1 = \DB::table('Transfer')->where('to_member_id', '=',$model->id)->where('from_member_id', '<>', 'to_member_id')->where('type', '=', 'C')->where('Transfer.created_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('Transfer.created_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->sum('amount');
                            
                    $totaldeposit = $totaldeposit1 + $totaltransfer1;
                      
                    return number_format($totaldeposit, 2);
                    })
                                       
           ->editColumn('totalwithdrawal', function ($model) {
                        
                    $totalwithdrawal = \DB::table('Member_Wallet_Statement')->where('member_id', '=',$model->id)->where('action_type', '=', 'Withdraw')->where('created_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('created_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->sum('cash_amount');
                     
                    $totaltransfer2 = \DB::table('Transfer')->where('from_member_id', '=',$model->id)->where('from_member_id', '<>', 'to_member_id')->where('type', '=', 'C')->where('Transfer.created_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('Transfer.created_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->sum('amount');

                    $totalwithdrawal2 = $totalwithdrawal + $totaltransfer2;
       
                    return number_format($totalwithdrawal2, 2);
                    })

            ->editColumn('totalsales', function ($model) {
          
                    $totalsales1 = \DB::table('Member_Wallet_Statement_Fund')->where('member_id', '=', $model->id)->where('action_type', '=', 'Transfer to MT5')->where('created_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('created_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->sum('b_amount');
                
                    $totalsales2 = \DB::table('Member_Blacklist')->where('member_id', '=', $model->id)->where('blacklist_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('blacklist_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->where('b_book', '<>', 0)->sum('b_book');
            
                    $totalsales = $totalsales1 - ($totalsales2/2);
           
                    return number_format($totalsales*2, 2);
                    })

            ->editColumn('totalinvest', function ($model) {

                    $investtrfx = 0;
                    $investcoin = 0;
                    $investmargin = 0;

                    $investtrfx = \DB::table('Member_Account_Fund')->where('member_id', $model->id)->sum('balanceB');
                    $coinWallet = \DB::table('Capx_Member_Coin')->where('member_id', $model->id)->first();

                    $coin = \DB::table('Capx_Coin')->get();
                    foreach ($coin as $coinData) {
                        $symbol = $coinData->symbol;
                        $investcoin += $coinData->price * $coinWallet->$symbol;
                    }

                    $margin = \DB::table('Capx_Margin')->get();
                    foreach ($margin as $marginData) {
                        $symbol = $marginData->symbol;
                        $investmargin += $marginData->current_price * $coinWallet->$symbol;
                    }

                    $totalinvest = $investtrfx * 3 + $investcoin + $investmargin;
                    
                    return number_format($totalinvest, 2);
                    })

            ->editColumn('totallotsize', function ($model) {
                    $totallotsize = 0;
                    
                    $accounts = DB::table('Member_Account_Fund')->where('member_id', '=',$model->id)->get();
                    
                    foreach($accounts as $account)
                    {
                    $bookA = $account->bookA;
        
                    $volume = DB::connection('mysql2')->table('historyposition')->where('Login', '=', $bookA)->where('CloseTime', '>=', config('misc.selectedMonthStart'))->where('CloseTime', '<', config('misc.selectedMonthEnd'))->sum('Volume');
        
                    // $volume2 = DB::connection('mysql2')->table('historyposition')->where('Login', '=', $bookA)->where('CloseTime', '>=', '2018-04-01')->where('OpenTime', '<', '2018-04-01')->sum('Volume');
                        
                    $lotsize1 = $volume / 10000;
                    // $lotsize2 = ($volume2 / 10000) / 2;
        
                    $totallotsize = $totallotsize + $lotsize1; // - $lotsize2;
                    }
        
                    return number_format($totallotsize, 2);
                    })

            ->make(true);
    }

    public function findGroupSalesList ($table=false, $memberid) {

        $posts = DB::table('Member_Network')->join('Member', 'Member_Network.member_id', '=', 'Member.id')->Where('Member_Network.parent_id', $memberid)
        ->select(['Member.username','Member.id','level','Member.register_by','my_level'])
        ->orderBy('my_level', 'ASC');

        //return Datatables::eloquent($this->model->join('Member_Network', 'Member_Network.member_id', '=', 'Member_Wallet_Statement_Fund.member_id')->Where('direct_id', $memberid))

        return Datatables::of($posts,$memberid)

            ->editColumn('username', function ($model) {
                    return $model->username;
                    })

            ->editColumn('firstname', function ($model) {
                    $first_name = \DB::table('users')->where('username', '=',$model->username)->first();
                    return $first_name->first_name;
                    })

            ->editColumn('upline', function ($model) {
                    $upline_name = \DB::table('users')->where('username', '=',$model->register_by)->first();
                    return $model->register_by.' '.$upline_name->first_name;
                    
                    })

            ->editColumn('level', function ($model) {
                    
                    return $model->my_level;
                    
                    })

            ->editColumn('rank', function ($model) {
                    $rank = \DB::table('Member_Sales')->where('member_id', '=',$model->id)->first();
                    return $rank->rank;
                    })

            ->editColumn('totaldeposit', function ($model) {
                    $totaldeposit1 = \DB::table('Member_Wallet_Statement')->where('member_id', '=',$model->id)->where('action_type', '=', 'Deposit')->where('created_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('created_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->sum('cash_amount');
                
                    $totaltransfer1 = \DB::table('Transfer')->where('to_member_id', '=',$model->id)->where('from_member_id', '<>', 'to_member_id')->where('type', '=', 'C')->where('Transfer.created_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('Transfer.created_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->sum('amount');
                    
                    $totaldeposit = $totaldeposit1 + $totaltransfer1;
                    
                    return number_format($totaldeposit, 2);
                    })

            ->editColumn('totalwithdrawal', function ($model) {
                    
                    $totalwithdrawal = \DB::table('Member_Wallet_Statement')->where('member_id', '=',$model->id)->where('action_type', '=', 'Withdraw')->where('created_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('created_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->sum('cash_amount');
                
                    $totaltransfer2 = \DB::table('Transfer')->where('from_member_id', '=',$model->id)->where('from_member_id', '<>', 'to_member_id')->where('type', '=', 'C')->where('Transfer.created_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('Transfer.created_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->sum('amount');
                    
                    $totalwithdrawal2 = $totalwithdrawal + $totaltransfer2;
                    
                    
                    return number_format($totalwithdrawal2, 2);
                    })

            ->editColumn('totalsales', function ($model) {
                    
                    $totalsales1 = \DB::table('Member_Wallet_Statement_Fund')->where('member_id', '=', $model->id)->where('action_type', '=', 'Transfer to MT5')->where('created_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('created_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->sum('b_amount');
                
                    $totalsales2 = \DB::table('Member_Blacklist')->where('member_id', '=', $model->id)->where('blacklist_at', '>', config('misc.selectedMonthStart').config('misc.startTime'))->where('blacklist_at', '<=', config('misc.selectedMonthEnd').config('misc.endTime'))->where('b_book', '<>', 0)->sum('b_book');
                    
                    $totalsales = $totalsales1 - ($totalsales2/2);
                    
                    return number_format($totalsales*2, 2);
                    })

            ->editColumn('totalinvest', function ($model) {

                    $investtrfx = 0;
                    $investcoin = 0;
                    $investmargin = 0;

                    $investtrfx = \DB::table('Member_Account_Fund')->where('member_id', $model->id)->sum('balanceB');
                    $coinWallet = \DB::table('Capx_Member_Coin')->where('member_id', $model->id)->first();

                    $coin = \DB::table('Capx_Coin')->get();
                    foreach ($coin as $coinData) {
                        $symbol = $coinData->symbol;
                        $investcoin += $coinData->price * $coinWallet->$symbol;
                    }

                    $margin = \DB::table('Capx_Margin')->get();
                    foreach ($margin as $marginData) {
                        $symbol = $marginData->symbol;
                        $investmargin += $marginData->current_price * $coinWallet->$symbol;
                    }

                    $totalinvest = $investtrfx * 3 + $investcoin + $investmargin;
                    
                    return number_format($totalinvest, 2);
                    })

            ->editColumn('totallotsize', function ($model) {
                    $totallotsize = 0;
                    
                    $accounts = DB::table('Member_Account_Fund')->where('member_id', '=',$model->id)->get();
                    
                    foreach($accounts as $account)
                    {
                    $bookA = $account->bookA;
                    
                    $volume = DB::connection('mysql2')->table('historyposition')->where('Login', '=', $bookA)->where('CloseTime', '>=', config('misc.selectedMonthStart'))->where('CloseTime', '<', config('misc.selectedMonthEnd'))->sum('Volume');
                     
                    // $volume2 = DB::connection('mysql2')->table('historyposition')->where('Login', '=', $bookA)->where('CloseTime', '>=', '2018-04-01')->where('OpenTime', '<', '2018-04-01')->sum('Volume');
                     
                    $lotsize1 = $volume / 10000;
                    // $lotsize2 = ($volume2 / 10000) / 2;
                     
                    $totallotsize = $totallotsize + $lotsize1;// - $lotsize2;
                    }
                     
                    return number_format($totallotsize, 2);
                    })
        
        ->make(true);
    }
    
    public function findBonusWalletStatementList ($table=false, $memberid) {
        if (!$table) return $this->walletModel->all();
        else {
            return Datatables::eloquent($this->walletstatamentModel->where('wallet_type', 'B')->Where('member_id', $memberid)->orderBy('id', 'DESC'))
            
                ->editColumn('credit', function ($model) {
                         if ($model->transaction_type ==  'C') return  number_format(floor($model->promotion_amount * 100) / 100,2);
                         else return number_format(0, 2);
                         })
                ->editColumn('debit', function ($model) {
                         if ($model->transaction_type ==  'D') return  number_format(floor($model->promotion_amount * 100) / 100,2);
                         else return number_format(0, 2);
                         })
                ->editColumn('balance', function ($model) {
                         return number_format(floor($model->balance * 100) / 100,2);
                         })
                ->editColumn('remark', function ($model) use($memberid) {
                        if(!(string)$model->remark_display){
                            return $model->remark;
                        }
                        else{
                            $split = explode("/", $model->remark_display);
                            if(\Lang::get('transactionHistory.'.$split[0]) == ('transactionHistory.'.$split[0])){
                                $remark = $model->remark_display;
                            }
                            else{
                                $remark = \Lang::get('transactionHistory.'.$split[0]);
                                if($split[1]){
                                    $detail = json_decode($split[1]);
                                    foreach ($detail as $key => $value) {
                                        if($key == "amount") $remark = str_replace('%%'.$key.'%%', number_format($value,2), $remark);
                                        else if($key == "date") $remark = str_replace('%%'.$key.'%%', date_format($value, "Y-m-d"), $remark);
                                        else if($key == "coin"){
                                            $coinName = \DB::table('Capx_Margin')->where('symbol', $value)->first();
                                            if(count($coinName) > 0){
                                                $displayName = \App::getLocale() == 'en' ? $coinName->name_en : $coinName->name_chs;
                                                $remark = str_replace('%%'.$key.'%%', $displayName, $remark);
                                            }
                                            else {
                                                $remark = str_replace('%%'.$key.'%%', \Lang::get('capx.'.$value), $remark);
                                            }
                                        }
                                        else $remark = str_replace('%%'.$key.'%%', $value, $remark);
                                    }
                                }
                            }
                            return $remark;
                        }
                        })

                ->make(true);
        }
    }

    public function findTransferWalletStatementList ($table=false, $memberid) {
        if (!$table) return $this->walletModel->all();
        else {
            return Datatables::eloquent($this->walletstatamentModel->where('wallet_type', 'T')->Where('member_id', $memberid)->orderBy('id', 'DESC'))
            
                ->editColumn('credit', function ($model) {
                         if ($model->transaction_type ==  'C') return number_format($model->t_amount, 2);
                         else return number_format(0, 2);
                         })
                ->editColumn('debit', function ($model) {
                         if ($model->transaction_type ==  'D') return number_format($model->t_amount, 2);
                         else return number_format(0, 2);
                         })
                ->editColumn('balance', function ($model) {
                         return number_format($model->balance, 2);
                         })
                ->editColumn('remark', function ($model) use($memberid) {
                        if(!(string)$model->remark_display){
                            return $model->remark;
                        }
                        else{
                            $split = explode("/", $model->remark_display);
                            if(\Lang::get('transactionHistory.'.$split[0]) == ('transactionHistory.'.$split[0])){
                                $remark = $model->remark_display;
                            }
                            else{
                                $remark = \Lang::get('transactionHistory.'.$split[0]);
                                if($split[1]){
                                    $detail = json_decode($split[1]);
                                    foreach ($detail as $key => $value) {
                                        if($key == "amount") $remark = str_replace('%%'.$key.'%%', number_format($value,2), $remark);
                                        else if($key == "date") $remark = str_replace('%%'.$key.'%%', date_format($value, "Y-m-d"), $remark);
                                        else $remark = str_replace('%%'.$key.'%%', $value, $remark);
                                    }
                                }
                            }
                            return $remark;
                        }
                        })

                ->make(true);
        }
    }
    /**
     * Register Member
     * @param  array $data
     * @param  App\Models\Member $currentMember
     * @return boolean
     */
    public function register ($data) {
      
        $idfront = $data['inputIDfront'];
        $idback = $data['inputIDback'];
    
        if (!$direct = $this->findByUsername($data['direct_id'])) {
            throw new \Exception(\Lang::get('error.sponsorNotFound'), 1);
            return false;
        }
    
        $user = \Sentinel::registerAndActivate([
            'email'             => $data['email'],
            'username'          => $data['email'],
            'first_name'        => $data['first_name'],
            'password'          => $data['password'],
            'password2'         => $data['password'],
            'permissions'       => [
                                        'member' => true,
                                    ]
        ]);
        
        $member_id = \DB::table('Member')->insertGetId([
            'username'          => $user->username,
            'register_by'       => $data['direct_id'],
            'package_id'        => 6,
            'secret_password'   => $data['password'],
            'user_id'           => $user->id,
            'direct_id'         => $direct->id,
            'level'             => $direct->level + 1,
            'id_front'          => $idfront,
            'id_back'           => $idback,
            'created_at'        => \Carbon\Carbon::now(),
            'updated_at'        => \Carbon\Carbon::now()
        ]);
        
        \DB::table('Member_Wallet')->insert([
            'member_id'         => $member_id,
            'register_point'    => 0,
            'purchase_point'    => 0,
            'promotion_point'   => 0,
            'cash_point'        => 0,
            'md_point'          => 0,
            'created_at'        => \Carbon\Carbon::now(),
            'updated_at'        => \Carbon\Carbon::now()
        ]);

        \DB::table('Capx_Member_Coin')->insert([
            'member_id'         => $member_id,
            'created_at'        => \Carbon\Carbon::now(),
            'updated_at'        => \Carbon\Carbon::now()
        ]);

        \DB::table('Member_Transaction_Log')->insert([
            'member_id'         => $member_id
        ]);

        // check network
        $seaNetwork = \DB::table('Member_Network')->where('parent_username', 'seanetlead@gmail.com')->where('username', $data['direct_id'])->count();
        if($data['direct_id'] == 'seanetlead@gmail.com') $seaNetwork = 1;
        if($seaNetwork > 0){
            $networkType = 1;
        }else{
            $networkType = 0;
        }

        $access = \DB::table('Member_Access_Type')->where('name', 'registration')->where('status', 1)->where('type', $networkType)->first();

        \DB::table('Member_Detail')->insert([
            'member_id'         => $member_id,
            'gender'            => $data['gender'],
            'phone1'            => $data['phone1'],
            'identification_number' => $data['identification_number'],
            'address'           => $data['address'],
            'nationality'       => $data['nationality'],
            'access_type'       => $access->id,
            'created_at'        => \Carbon\Carbon::now(),
            'updated_at'        => \Carbon\Carbon::now()
        ]);
     
        $bookNo = \DB::table('MT5')->first();
        
        $bookA = $bookNo->bookA;
        
        $bookB = $bookNo->bookB;
     
        \DB::table('MT5')->increment('bookA');
        \DB::table('MT5')->increment('bookB');
        
        \DB::table('Member_Account')->insert([
            'member_id'         => $member_id,
            'bookA'             => $bookA,
            'bookB'             => $bookB,
            'system_manage'     => 'N',
            'created_at'        => \Carbon\Carbon::now(),
            'updated_at'        => \Carbon\Carbon::now()
        ]);
     
        $credentials_1 = config('misc.imagePath').$idfront;
        $credentials_2 = config('misc.imagePath').$idback;
        
        $country_str = 'misc.countries.'.$data['nationality'].'.code';
        $country_code = config($country_str);
        
        if($data['gender'] == 'Male'){
            $genders = 'M';
        }
        else{
            $genders = 'F';
        }

        return true;
    }

    /**
     * Register History - DataTable
     * @param  App\Models\Member $member
     * @return object
     */
    public function registerHistory ($member) {
        return Datatables::eloquent($this->model->where('direct_id', $member->id))
        ->editColumn('package_amount', function ($model) {
            return number_format($model->package_amount, 0);
        })->make(true);
    }
    
    public function addMT5 ($user, $member) {

        $accounts = \DB::table('Member_Account')->where('member_id', '=', $member->id)->count();
        
        if ($accounts == 3)
        {
            throw new \Exception(\Lang::get('error.maxaccountError'), 1);
            return false;
        }
        
        $bookNo = \DB::table('MT5')->first();
        
        $bookA = $bookNo->bookA;
        
        $bookB = $bookNo->bookB;
        
        \DB::table('MT5')->increment('bookA');
        \DB::table('MT5')->increment('bookB');
        
        /*
        $groupnameA = config('misc.book.bookA.groupname');
        $leverA = config('misc.book.bookA.lever');
        $addBookA = $this->addMT5API($groupnameA, $leverA, $bookA);
        */
        
        $groupnameA = config('misc.newbook.newbookA.groupname');
        $leverA = config('misc.newbook.newbookA.lever');
        
        $addBookA = $this->NewUserAPI($user->first_name, $user->email, $groupnameA, $leverA, $bookA, $user->password2);
        
       
        if ($addBookA->status == 1)
        {
            throw new \Exception(\Lang::get('error.mt5existError'), 1);
            return false;
        }
        
        $groupnameB = config('misc.newbook.newbookB.groupname');
        $leverB = config('misc.newbook.newbookB.lever');
        
        $addBookB = $this->NewUserAPI($user->first_name, $user->email, $groupnameB, $leverB, $bookB, $user->password2);
        
        /*
        $groupnameB = config('misc.book.bookB.groupname');
        $leverB = config('misc.book.bookB.lever');
        $addBookB = $this->addMT5API($groupnameB, $leverB, $bookB);
        */
        
        \DB::table('Member_Account')->insert(
                                        [
                                        'member_id' => $member->id,
                                        'bookA' => $bookA,
                                        'bookB' => $bookB,
                                        'system_manage' => 'N',
                                        'created_at' => \Carbon\Carbon::now(),
                                        'updated_at' => \Carbon\Carbon::now()
                                        ]
                                        );
        
         return true;
    }
    
    public function addMT5self ($user, $member) {

        $accounts = \DB::table('Member_Account_Self')->where('member_id', '=', $member->id)->count();
        
        if ($accounts == 1)
        {
            throw new \Exception(\Lang::get('error.maxaccountError'), 1);
            return false;
        }
        
        $bookNo = \DB::table('MT5_Self')->first();
        
        $bookA = $bookNo->bookA;

        \DB::table('MT5_Self')->increment('bookA');
        
        
        $groupnameA = config('misc.pbook.pbookA.groupname');
        $leverA = config('misc.pbook.pbookA.lever');
        
        $addBookA = $this->NewUserAPI($user->first_name, $user->email, $groupnameA, $leverA, $bookA, $user->password2);
        
       
        if ($addBookA->status == 1)
        {
            throw new \Exception(\Lang::get('error.mt5existError'), 1);
            return false;
        }
        
       
        
        \DB::table('Member_Account_Self')->insert(
                                        [
                                        'member_id' => $member->id,
                                        'bookA' => $bookA,
                                        'system_manage' => 'N',
                                        'created_at' => \Carbon\Carbon::now(),
                                        'updated_at' => \Carbon\Carbon::now()
                                        ]
                                        );
        
         return true;
    }
    
    public function addFund ($user, $member) {

        $accounts = \DB::table('Member_Account_Fund')->where('member_id', '=', $member->id)->count();
        
       // if ($accounts == 5)
       // {
       //     throw new \Exception(\Lang::get('error.maxfundaccountError'), 1);
       //     return false;
       // }
        
        $bookNo = \DB::table('Fund')->first();
        
        $bookA = $bookNo->bookA;
        
        $bookB = $bookNo->bookB;
        
        \DB::table('Fund')->increment('bookA');
        \DB::table('Fund')->increment('bookB');
        
        $newpassword = 'XSw21QAz';

        $groupnameA = config('misc.fundbook.fundbookA.groupname');
        $leverA = config('misc.fundbook.fundbookA.lever');
        
        $addBookA = $this->NewUserAPI($user->first_name, $user->email, $groupnameA, $leverA, $bookA, $newpassword);
        
       
        if ($addBookA->status == 1)
        {
            throw new \Exception(\Lang::get('error.fundexistError'), 1);
            return false;
        }
        
       // $groupnameB = config('misc.fundbook.fundbookB.groupname');
       // $leverB = config('misc.fundbook.fundbookB.lever');
        
        //$addBookB = $this->NewUserAPI($user->first_name, $user->email, $groupnameB, $leverB, $bookB, $newpassword);
        
        /*
        $groupnameB = config('misc.book.bookB.groupname');
        $leverB = config('misc.book.bookB.lever');
        $addBookB = $this->addMT5API($groupnameB, $leverB, $bookB);
        */
        
        \DB::table('Member_Account_Fund')->insert(
                                        [
                                        'member_id' => $member->id,
                                        'bookA' => $bookA,
                                        'bookB' => $bookB,
                                        'created_at' => \Carbon\Carbon::now(),
                                        'updated_at' => \Carbon\Carbon::now()
                                        ]
                                        );
        
         return true;
    }

    /**
     * Upgrade / Renew Package
     * @param  App\Models\Member $member
     * @param  array $data
     * @return App\Models\Member
     */
    public function upgrade ($member, $data) {
        $repo = new PackageRepository;

        if (!$package = $repo->findById($data['package_id'])) {
            throw new \Exception(\Lang::get('error.packageNotFound'), 1);
            return false;
        }
        
        if (!$beforePackage = $repo->findById($member->package_id)) {
            throw new \Exception(\Lang::get('error.packageNotFound'), 1);
            return false;
        }

        if ($member->package->package_amount > $package->package_amount) { // member has the same or bigger package
            throw new \Exception(\Lang::get('error.packageNotAvailable'), 1);
            return false;
        }

       // if ($member->package_amount == $package->package_amount) { // renew
        //    $this->renewPackage($member, $package, $data);
        //} else {
            $this->upgradePackage($member, $package, $beforePackage, $data);
       // }

        return $member;
    }

    /**
     * Upgrade Package
     * @param  App\Models\Member $member [The member]
     * @param  App\Models\Package $package [new package]
     * @param  App\Models\Package $beforePackage [old package]
     * @param  array $data [data]
     * @return App\Models\Member
     */
    public function upgradePackage ($member, $package, $beforePackage, $data) {
        $wallet = $member->wallet;
        $needAmount = $package->package_amount - $beforePackage->package_amount;
        if ($wallet->register_point < $needAmount) {
            throw new \Exception(\Lang::get('error.registerNotEnough'), 1);
            return false;
        }
        $wallet->register_point -= $needAmount;
        if ($wallet->register_point < 0) $wallet->register_point = 0; // unlikely

        //$shares = $member->shares;
        //$member->direct_percent = $package->direct_percent;
        //$member->pairing_percent = $package->pairing_percent;
        //$member->group_level = $package->group_level;
        //$member->max_pair = $package->max_pair;
        //$member->max_pairing_bonus = $package->max_pairing_bonus;
        //$member->package_amount = $package->package_amount;
        //$member->original_amount = $package->package_amount;
        $member->package_id = $package->id;
        $member->direct_percent = $package->direct_percent;
        $member->month_percent =  $package->month_percent;
        $member->group_percent =  $package->group_percent;
        $member->group_level =    $package->group_level;
        $member->package_amount =    $package->package_amount;
        //$shares->max_share_sale += $package->max_share_sale;
        //$shares->share_limit = $package->share_limit;

        //$purchasePoint = $package->purchase_point - $beforePackage->purchase_point;
        //$wallet->purchase_point += abs($purchasePoint);

        $member->save();
        //$shares->save();
        $wallet->save();
        
        $currentMember = $member;
       
        $remark = 'Debited '.$needAmount.' for upgrade package to '.$package->title;

        $remarkAry = array('amount'=>$needAmount, 'package'=>$package->title);
        $remark_display = 'TH00026/'.json_encode($remarkAry);
        
        $this->saveWalletStatement($currentMember, $member->username, $needAmount ,'Top-up package', 'R', 'D', $wallet->register_point, $remark, $remark_display);
/*
        if ($member->position != 'top') {
            $repo = new BonusRepository();
            $repo->calculateDirectUpgrade($member, $needAmount);
            $repo->calculateOverrideUpgrade($member, $needAmount);
            $this->addNetworkSales($member, $needAmount);
        }
 */
        $repo = new BonusRepository();
        $repo->calculateDirectUpgrade($member, $needAmount);
        
        return $member;
    }

    /**
     * Renew Package
     * @param  App\Models\Member $member [The member]
     * @param  App\Models\Package $package [renew package]
     * @param  array $data   [data]
     * @return App\Models\Member
     */
    public function renewPackage ($member, $package, $data) {
        $wallet = $member->wallet;
        $needAmount = $package->package_amount;
        if ($wallet->register_point < $needAmount) {
            throw new \Exception(\Lang::get('error.registerNotEnough'), 1);
            return false;
        }
        $wallet->register_point -= $needAmount;
        if ($wallet->register_point < 0) $wallet->register_point = 0; // unlikely

        $shares = $member->shares;
        $member->direct_percent = $package->direct_percent;
        $member->pairing_percent = $package->pairing_percent;
        $member->group_level = $package->group_level;
        $member->max_pair = $package->max_pair;
        $member->max_pairing_bonus = $package->max_pairing_bonus;
        $member->package_amount = $package->package_amount;
        $member->package_id = $package->id;
        $shares->max_share_sale += $package->max_share_sale;
        $shares->share_limit = $package->share_limit;
        $wallet->purchase_point += abs($package->purchase_point);

        $member->save();
        $shares->save();
        $wallet->save();

        $this->saveWalletStatement($member, $needAmount, 0, 'renew');

        if ($member->position != 'top') {
            $repo = new BonusRepository();
            $direct = $member->parent();
            $repo->calculateDirect($member, $direct);
            $repo->calculateOverride($member);
            $this->addNetworkSales($member);
        }
        return $member;
    }

    /**
     * Check if member can register
     * @param  $wallet
     * @param  $package
     * @param  $percent PROMOTION POINT percent
     * @return boolean
     */
    public function checkRegisterFunds ($wallet, $package) {
        $amount = $package->package_amount;
        // check promotion point
        //$promotion = ($percent / 100) * $amount;
       // if ($wallet->promotion_point < $promotion) return false;
        // check register point
        //$register = ((100 - $percent) / 100) * $amount;
        if ($wallet->register_point < $amount) return false;
        //$wallet->promotion_point -= $promotion;
        $wallet->register_point -= $amount;
        return $wallet;
    }

    /**
     * Save Wallet Statement
     * @param  App\Models\Member $member  
     * @param  decimal $amount 
     * @param  integer $percent 
     * @param  boolean $type [<description>]
     * @return boolean
     */
    public function saveWalletStatement ($currentMember, $username, $amount, $action_type, $wallet_type, $transaction_type, $balance, $remark, $remark_display) {
        //$promotion = ($percent / 100) * $amount;
        //$register = ((100 - $percent) / 100) * $amount;
        
        if($wallet_type == 'R')
        {
            $ramount = $amount;
            $bamount = 0;
        }
        else
        {
            $ramount = 0;
            $bamount = $amount;
        }

        \DB::table('Member_Wallet_Statement')->insert([
            'member_id' => $currentMember->id,
            'username' => $username,
            'promotion_amount' => $bamount,
            'register_amount' => $ramount,
            'action_type' => $action_type,
            'wallet_type' => $wallet_type,
            'transaction_type' => $transaction_type,
            'balance' =>  $balance,
            'remark' => $remark,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
            'remark_display' => $remark_display
        ]);

        return true;
    }

    /**
     * Check if member can be add to the network
     * @param  App\Models\Member $parent
     * @param  string $position
     * @return boolean
     */
    public function checkIfPositionAvailable ($parent, $position) {
        if ($this->model
                ->where('parent_id', $parent->id)
                ->where('position', $position)
                ->first()) { // already filled
            return false;
        }
        return true;
    }

    /**
     * Add member to network tree
     * 
     * @param App\Models\Member $member
     * @return boolean
     */
    public function addNetwork ($member) {
        $id = $member->id; // id to add
        $befores = '';
        $beforeChildren = '';
        $amount = $member->original_amount;

        if ($parent = $member->parent()) {
            $alwaysRemove = explode(',', $member->position == 'right' ? $parent->left_children : $parent->right_children);
        }

        while ($root = $member->root()) {
            if ($member->position == 'left') {
                $memberIds = $root->left_children;
                $position = 'left_children';
                $totalField = 'left_total';
            } else {
                $memberIds = $root->right_children;
                $position = 'right_children';
                $totalField = 'right_total';
            }

            $memberIds = rtrim($root->id . ',' . $memberIds, ',');
            $memberIds = explode(',', $memberIds);

            if ($befores != '') {
                $memberIds = array_diff($memberIds, explode(',', $befores));
            }

            if (isset($alwaysRemove)) {
                $memberIds = array_diff($memberIds, $alwaysRemove);
            }

            if ($beforeChildren != '') {
                $memberIds = array_diff($memberIds, explode(',', $beforeChildren));
            }

            $memberIds = implode(',', $memberIds);

            if ($root->position == 'top') {
                // \Log::info('UPDATE tb_Member SET ' . $position . ' = CONCAT(ifnull(' . $position . ', ""), CASE WHEN ' . $position . ' IS NOT NULL THEN ",' . $id . '" ELSE "' . $id . '" END), ' . $totalField . '=' . $totalField . ' + ' . $amount . ' WHERE id IN (' . $memberIds . ') AND position="' . $member->position . '" AND level < ' . $member->level . ' AND root_id=' . $member->root_id);
                // \Log::info('UPDATE tb_Member SET ' . $position . ' = CONCAT(ifnull(' . $position . ', ""), CASE WHEN ' . $position . ' IS NOT NULL THEN ",' . $id . '" ELSE "' . $id . '" END), ' . $totalField . '=' . $totalField . ' + ' . $amount . ' WHERE id IN (' . $memberIds . ') AND position="top" AND level < ' . $member->level . ' AND root_id=' . $member->root_id);
                \DB::update('UPDATE tb_Member SET ' . $position . ' = CONCAT(ifnull(' . $position . ', ""), CASE WHEN ' . $position . ' IS NOT NULL THEN ",' . $id . '" ELSE "' . $id . '" END), ' . $totalField . '=' . $totalField . ' + ' . $amount . ' WHERE id IN (' . $memberIds . ') AND position="' . $member->position . '" AND level < ' . $member->level . ' AND root_id=' . $member->root_id);
                \DB::update('UPDATE tb_Member SET ' . $position . ' = CONCAT(ifnull(' . $position . ', ""), CASE WHEN ' . $position . ' IS NOT NULL THEN ",' . $id . '" ELSE "' . $id . '" END), ' . $totalField . '=' . $totalField . ' + ' . $amount . ' WHERE id IN (' . $memberIds . ') AND position="top" AND level < ' . $member->level);
            } else {
                // \Log::info('UPDATE tb_Member SET ' . $position . ' = CONCAT(ifnull(' . $position . ', ""), CASE WHEN ' . $position . ' IS NOT NULL THEN ",' . $id . '" ELSE "' . $id . '" END), ' . $totalField . '=' . $totalField . ' + ' . $amount . ' WHERE id IN (' . $memberIds . ') AND position="' . $member->position . '" AND level < ' . $member->level . ' AND root_id=' . $member->root_id);
                // \Log::info('UPDATE tb_Member SET ' . $position . ' = CONCAT(ifnull(' . $position . ', ""), CASE WHEN ' . $position . ' IS NOT NULL THEN ",' . $id . '" ELSE "' . $id . '" END), ' . $totalField . '=' . $totalField . ' + ' . $amount . ' WHERE id=' . $root->id . ' AND level < ' . $member->level . ' AND root_id=' . $member->root_id);
                \DB::update('UPDATE tb_Member SET ' . $position . ' = CONCAT(ifnull(' . $position . ', ""), CASE WHEN ' . $position . ' IS NOT NULL THEN ",' . $id . '" ELSE "' . $id . '" END), ' . $totalField . '=' . $totalField . ' + ' . $amount . ' WHERE id IN (' . $memberIds . ') AND position="' . $member->position . '" AND level < ' . $member->level . ' AND root_id=' . $member->root_id);
                \DB::update('UPDATE tb_Member SET ' . $position . ' = CONCAT(ifnull(' . $position . ', ""), CASE WHEN ' . $position . ' IS NOT NULL THEN ",' . $id . '" ELSE "' . $id . '" END), ' . $totalField . '=' . $totalField . ' + ' . $amount . ' WHERE id=' . $root->id . ' AND level < ' . $member->level);
            }

            $beforeChildren .= ',' . ($root->position == 'left' ? $root->left_children : $root->right_children);
            $beforeChildren = ltrim($beforeChildren, ',');
            $befores .= ',' . $memberIds;
            $member = $root;
        }

        return true;
    }

    /**
     * Add sales only to network
     * @param App\Models\Member $member
     * @param decimal $amount
     */
    public function addNetworkSales ($member, $amount=null) {
        $id = $member->id; // id to add
        $befores = '';
        $beforeChildren = '';
        if (is_null($amount)) {
            $amount = $member->package_amount;
        }

        if ($parent = $member->parent()) {
            $alwaysRemove = explode(',', $member->position == 'right' ? $parent->left_children : $parent->right_children);
        }

        while ($root = $member->root()) {
            if ($member->position == 'left') {
                $memberIds = $root->left_children;
                $position = 'left_children';
                $totalField = 'left_total';
            } else {
                $memberIds = $root->right_children;
                $position = 'right_children';
                $totalField = 'right_total';
            }

            $memberIds = rtrim($root->id . ',' . $memberIds, ',');
            $memberIds = explode(',', $memberIds);

            if ($befores != '') {
                $memberIds = array_diff($memberIds, explode(',', $befores));
            }

            if (isset($alwaysRemove)) {
                $memberIds = array_diff($memberIds, $alwaysRemove);
            }

            if ($beforeChildren != '') {
                $memberIds = array_diff($memberIds, explode(',', $beforeChildren));
            }

            $memberIds = implode(',', $memberIds);

            if ($root->position == 'top') {
                // \Log::info('UPDATE tb_Member SET ' . $totalField . '=' . $totalField . ' + ' . $amount . ' WHERE id IN (' . $memberIds . ') AND position="' . $member->position . '" AND level < ' . $member->level);
                // \Log::info('UPDATE tb_Member SET ' . $totalField . '=' . $totalField . ' + ' . $amount . ' WHERE id IN (' . $memberIds . ') AND position="' . $member->position . '" AND level < ' . $member->level);
                \DB::update('UPDATE tb_Member SET ' . $totalField . '=' . $totalField . ' + ' . $amount . ' WHERE id IN (' . $memberIds . ') AND position="' . $member->position . '" AND level < ' . $member->level . ' AND root_id=' . $member->root_id);
                \DB::update('UPDATE tb_Member SET ' . $totalField . '=' . $totalField . ' + ' . $amount . ' WHERE id IN (' . $memberIds . ') AND position="top" AND level < ' . $member->level);
            } else {
                // \Log::info($totalField . '=' . $totalField . ' + ' . $amount . ' WHERE id IN (' . $memberIds . ') AND position="' . $member->position . '" AND level < ' . $member->level);
                // \Log::info($totalField . '=' . $totalField . ' + ' . $amount . ' WHERE id=' . $root->id . ' AND level < ' . $member->level);
                \DB::update('UPDATE tb_Member SET ' . $totalField . '=' . $totalField . ' + ' . $amount . ' WHERE id IN (' . $memberIds . ') AND position="' . $member->position . '" AND level < ' . $member->level . ' AND root_id=' . $member->root_id);
                \DB::update('UPDATE tb_Member SET ' . $totalField . '=' . $totalField . ' + ' . $amount . ' WHERE id=' . $root->id . ' AND level < ' . $member->level);
            }

            $beforeChildren .= ',' . ($root->position == 'left' ? $root->left_children : $root->right_children);
            $beforeChildren = ltrim($beforeChildren, ',');
            $befores .= ',' . $memberIds;
            $member = $root;
        }

        return true;
    }

    /**
     * Update member shares when split
     * @param  integer $mult [multiplier]
     * @return boolean
     */
    public function updateSharesSplit ($mult) {
        $this->model->chunk(100, function ($members) use ($mult) {
            foreach ($members as $member) {
                $shares = $member->shares;
                $add = $shares->amount * $mult;
                $amount = $add - $shares->amount;
                $shares->amount += $amount;

                if ($amount > 0) {
                    \DB::table('Shares_Split_Statement')->insert([
                        'amount' => $amount,
                        'username' => $member->username,
                        'member_id' => $member->id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ]);

                    $shares->save();
                }
            }
        });

        return true;
    }

    public function findDepositRecords ($table=false, $memberid) {

        $posts = DB::table('deposit_log')->where('wid', $memberid)->where('type', 1)
        ->orderBy('deal_time', 'DESC');
        
        return Datatables::of($posts,$memberid)
        
        ->editColumn('fundmoney', function ($model) {
                     return "$ ".$model->fundmoney;
                     })
        
        ->editColumn('created', function ($model) {
                     return date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s", $model->created)."-8 hours"));
                     })
        
        ->editColumn('status', function ($model) {
                     if($model->status == 2) return '<span style="color:green;">'.\Lang::get('common.success').'<span>';
                     else return '<span style="color:red;">'.\Lang::get('common.pending').'<span>';
                     })
        ->addColumn('download', function ($model) {
                    if($model->invFileName) return '<a class="btn btn-primary glow_button" href="'.route("amazon.read", ["folder" => "invoices", "filename" => $model->invFileName, 'lang' => \App::getLocale()]).'" target="blank"><i class="fa fa-download"></i> '.\Lang::get('common.download').'</a>';
                    else return "";
                     
                })
        
        ->rawColumns(['status','download'])
        ->make(true);
    }

    public function addFundChecking ($user, $member) {

        // $availableAccount = 2;
        // $accountList = 0;

        // $cashWalletBalance = \DB::table('Member_Wallet')->where('member_id', '=', $member->id)->first()->cash_point;
        // $investingBalance = \DB::table('Member_Account_Fund')->where('member_id', '=', $member->id)->sum('balanceB');
        // $totalAvailableAmount = $cashWalletBalance + ($investingBalance * 3);

        // $totalAvailableAccount = floor($totalAvailableAmount/15000)*2;
        // if($totalAvailableAccount%2 != 0) $totalAvailableAccount = $totalAvailableAccount - 1;
        // if($totalAvailableAccount < $availableAccount) $totalAvailableAccount = $availableAccount;

        // $totalInvestingAccount = \DB::table('Member_Account_Fund')->where('member_id', '=', $member->id)->where('balanceB', '>', 0)->count();
        
        // if($totalInvestingAccount >= $totalAvailableAccount){
        //     throw new \Exception(\Lang::get('error.maxAccount'), 1);
        //     return false;
        // }

        $investingAccount = \DB::table('Member_Account_Fund')->where('member_id', '=', $member->id)->orderBy('id', 'ASC')->get();
        foreach ($investingAccount as $details) {
            if($details->fundcompany <= 0 && $details->system_manage == 'N'){
                throw new \Exception(str_replace('%%bookA%%', $details->bookA, \Lang::get('error.noFundCompany')), 1);
                return false;
            }
            else if($details->balanceB <= 0){
                throw new \Exception(str_replace('%%bookA%%', $details->bookA, \Lang::get('error.addFund')), 1);
                return false;
            }
            else if($details->balanceB < $details->ini_amount){
                throw new \Exception(str_replace(array('%%amount%%','%%bookA%%'), array($details->ini_amount,$details->bookA), \Lang::get('error.requiredFund')), 1);
                return false;
            }
        }
        return true;
    }

    public function getLpoa(){

        $posts = DB::table('users')
        ->join('Member', 'users.username', '=', 'Member.username')
        ->join('Member_Detail', 'Member.id', '=', 'Member_Detail.member_id')
        
        ->where('Member.process_status','Y')
        ->whereNotNull('Member.lpoa')
        ->select('Member.id', 'Member.username',  'users.first_name', 'Member.lpoa', 'Member.lpoa_status','Member_Detail.identification_number')
        ->where('Member.lpoa_status', '<>','Y')
        ->orderBy('Member.updated_at', 'DESC');

        return Datatables::of($posts)
        ->addColumn('action', function ($model) {
                    return view('back.declare.action')->with('model', $model);
        })



        ->editColumn('Member_Detail.identification_number', function ($model) {

            return $model->identification_number;

        })
        ->editColumn('Member.username', function ($model) {

            return $model->username;

        })





        ->rawColumns(['lpoa','status','action'])
        ->make(true);
    }

    public function getLpoa_detail($id){

        $model = DB::table('users')
        ->join('Member', 'users.id', '=', 'Member.user_id')
        ->join('Member_Detail', 'Member.id', '=', 'Member_Detail.member_id')
        ->where('Member.id',$id)
        ->first(['identification_number','first_name','lpoa','Member.id','lpoa_status']);

        return $model;
    }

    public function findCommStatementDailyList($id,$type){

        $posts = DB::table('Capx_Commission')
        ->where('member_id',$id)
        ->where('type',$type)
        ->orderBy('id', 'DESC')
        ->orderBy('end_date', 'DESC');

        return Datatables::of($posts)
        ->addColumn('action', function ($model) {
            return view('front.misc.DailyAction')->with('model', $model);          
        })
        ->editColumn('type', function ($model) {

            if($model->type=="Margin"){
                return '<span style="color:green;">'.\Lang::get('misc.margin').' - '.\Lang::get('misc.'.$model->symbol.'').'<span>';
            }else{
                return '<span style="color:green;">'.\Lang::get('misc.capx').'<span>';
            }
            

        })
        ->editColumn('status', function ($model) {

            if($model->status==0){
                return '<span style="color:orange;">'.\Lang::get('misc.pending').'<span>';
            }else{
                return '<span style="color:green;">'.\Lang::get('misc.paid').'<span>';
            }
            
        })

        ->rawColumns(['action','type','status'])
        ->make(true);
    }

    public function findAllCommStatementDailyList($id){

        $posts = DB::table('Capx_Commission')
        ->where('member_id',$id)
        ->orderBy('id', 'DESC')
        ->orderBy('end_date', 'DESC');

        return Datatables::of($posts)
        ->addColumn('action', function ($model) {
            return view('front.misc.DailyAction')->with('model', $model);     
        })

        ->editColumn('type', function ($model) {

            if($model->type=="Margin"){
                return '<span style="color:green;">'.\Lang::get('misc.margin').' - '.\Lang::get('misc.'.$model->symbol.'').'<span>';
            }else{
                return '<span style="color:green;">'.\Lang::get('misc.capx').'<span>';
            }

        })

        ->editColumn('status', function ($model) {

            if($model->status==0){
                return '<span style="color:orange;">'.\Lang::get('misc.pending').'<span>';
            }else{
                return '<span style="color:green;">'.\Lang::get('misc.paid').'<span>';
            }
            
        })



        ->rawColumns(['action','type','status'])
        ->make(true);
    }

    public function find_seanet($id) {
        $model = $this->findById(trim($id));
        if($model->username != "seanetlead@gmail.com"){
            if($model->register_by!="seanetlead@gmail.com"){
                $leader = \DB::table('Member_Network')->where('parent_username', 'seanetlead@gmail.com')->where('username', '=',$model->register_by)->first();
                if(!empty($leader)){
                    $model->leader = 1;
                }else{
                    $model->leader = 0;
                }
            }else{
                $model->leader = 1;
            }
        }else{
            $model->leader = 1;
        }

        return $model->leader;
    }

}
