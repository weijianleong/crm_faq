<?php

namespace App\Repositories;

use App\Models\Announcement;
use App\Repositories\MemberRepository;
use Yajra\Datatables\Facades\Datatables;

class AnnouncementRepository extends BaseRepository
{
    protected $model;
    protected $allowedFields = [];
    protected $booleanFields = [];

    public function __construct() {
        $this->model = new Announcement;
    }

    protected function saveModel($model, $data) {
        foreach ($data as $k=>$d) {
            $model->{$k} = $d;
        }
        $model->save();
        return $model;
    }

    public function store($data) {
        $model = $this->saveModel(new $this->model, $data);
        return $model;
    }

    public function update($model, $data) {
        $model = $this->saveModel($model, $data);
        return $model;
    }

    public function getAllowedFields () {
        return $this->allowedFields;
    }

    public function getBooleanFields () {
        return $this->booleanFields;
    }

    public function findById ($id) {
        return $this->model->where('id', $id)->first();
    }

    /**
     * Datatable List - Member
     * @return [type] [description]
     */
    public function getList () {
        $memberRepo = new MemberRepository;

        $user = \Sentinel::getUser();
        $member = $memberRepo->findByUsername(trim($user->username));

        $seaNetwork = \DB::table('Member_Network')->where('parent_username', 'seanetlead@gmail.com')->where('username', $member->register_by)->count();
        if(in_array($user->username, ['seanetlead@gmail.com']) || $member->register_by == 'seanetlead@gmail.com') $seaNetwork = 1;

        $unread = $member->detail->announcement;

        if($seaNetwork){
            return Datatables::eloquent($this->model->whereIn('access_type', [0,2])->orderBy('created_at', 'DESC'))
                ->addColumn('action', function ($model) use($unread) {
                    if($unread == $model->id) return '<button class="btn btn-primary glow_button" onclick="getAnnouncementDetails('.$model->id.')"><i class="fa fa-share-square-o"></i> '.\Lang::get('common.read').'</button><i id='.$model->id.' class="fa fa-circle" style="position: absolute; color: red; font-size: 8px;"></i>';
                    else return '<button class="btn btn-primary glow_button" onclick="getAnnouncementDetails('.$model->id.')"><i class="fa fa-share-square-o"></i> '.\Lang::get('common.read').'</button>';
                })
                ->editColumn('created_at', function ($model) {
                    return $model->created_at->format('Y-m-d h:i A');
                })
                ->rawColumns(['action'])
                ->make(true);
        }else{
            return Datatables::eloquent($this->model->whereIn('access_type', [0,1])->orderBy('created_at', 'DESC'))
                ->addColumn('action', function ($model) use($unread) {
                    if($unread == $model->id) return '<button class="btn btn-primary glow_button" onclick="getAnnouncementDetails('.$model->id.')"><i class="fa fa-share-square-o"></i> '.\Lang::get('common.read').'</button><i id='.$model->id.' class="fa fa-circle" style="position: absolute; color: red; font-size: 8px;"></i>';
                    else return '<button class="btn btn-primary glow_button" onclick="getAnnouncementDetails('.$model->id.')"><i class="fa fa-share-square-o"></i> '.\Lang::get('common.read').'</button>';
                })
                ->editColumn('created_at', function ($model) {
                    return $model->created_at->format('Y-m-d h:i A');
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        
    }

    /**
     * Datatable List - Admin
     * @return [type] [description]
     */
    public function getAdminList () {
        return Datatables::eloquent($this->model->query())
                ->addColumn('action', function ($model) {
                    return view('back.announcement.action')->with('model', $model);
                })
                ->rawColumns(['action'])
                ->make(true);
    }
    public function check_refrence($date){      
        $model = $this->model->where('created_at', 'like', $date.'%')->orderBy('created_at','DESC')->first();
        return $model;
    }

}