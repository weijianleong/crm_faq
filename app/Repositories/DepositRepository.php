<?php

namespace App\Repositories;
use App\Models\deposit_log;
use App\Models\payid_info;
use Carbon\Carbon;
use Yajra\Datatables\Facades\Datatables;

class DepositRepository
{
    protected $model;
    protected $allowedFields = [];
    protected $booleanFields = [];

    public function __construct() {
        $this->depositModel = new deposit_log;
        $this->payidModel = new payid_info;
        
    }

    protected function saveModel($model, $data) {
        foreach ($data as $k=>$d) {
            $model->{$k} = $d;
        }
        $model->save();
        return $model;
    }

    public function storeDeposit($data) {
        $model = $this->saveModel(new $this->depositModel, $data);
        return $model;
    }

    public function storePay($data) {
        $model = $this->saveModel(new $this->payidModel, $data);
        return $model;
    }

    public function update($model, $data) {
        $model = $this->saveModel($model, $data);
        return $model;
    }

    public function findById ($id,$member_id) {
        return $this->depositModel->where('payid', $id)->where('wid',$member_id)->first();
    }
    public function CheckNewApp ($member_id) {
        return $this->depositModel->where('wid',$member_id)->where('type',2)->where('status',1)->first();
    }
    public function CheckWithdrawStatus ($member_id , $status) {
        return $this->depositModel->where('wid',$member_id)->where('type',2)->where('status',$status)->where('created_at','like', Carbon::now()->format('Y-m').'%')->count();

        //return $this->depositModel->where('wid',$member_id)->where('type',2)->where('status',$status)->where('created_at','>=', Carbon::create(2018, 12, 6, 00, 00, 00))->count();
    }


    public function destroy($id)
    {
        $this->getById($id)->delete();
    }

    public function cancelWithdraw($payID,$member_id)
    {
        $model = $this->depositModel->where('wid', $member_id)->where('payid', $payID)->update(['status' => 3]);
        return $model;
    }

    public function getWithdrawList ($id,$member_id) {
        return Datatables::eloquent($this->depositModel->where('type',$id)->where('wid', $member_id)->orderby('payid','DESC'))
                ->addColumn('action', function ($model) {
                    return view('front.withdrawal.action')->with('model', $model);
                })
                ->addColumn('created', function ($model) {
                    
                    $gettime = date("Y-m-d H:i:s", $model->created);
                    return date("Y-m-d H:i:s",strtotime($gettime. "-8 hours"));
                })
                ->editColumn('status', function ($model) {
                    
                    if($model->status=="1" ){
                        if($model->xPayStatus != "0" || $model->xPayWithdrawal !="0"){
                            return '<div id='.$model->payid.'><span style="color:blue;">'.\Lang::get('withdraw.pending').'</span></div>';
                        }else{
                            return '<div id='.$model->payid.'><span style="color:blue;">'.\Lang::get('withdraw.Submitted').'</span></div>';

                        }
                    }elseif ($model->status=="3"){
                        return '<div id='.$model->payid.'><span style="color:red;"><id='.$model->payid.'>'.\Lang::get('withdraw.Cancelled').'</span></div>';
                    }elseif ($model->status=="9"){
                        return '<div id='.$model->payid.'><span style="color:red;"><id='.$model->payid.'>'.\Lang::get('withdraw.refund').'</span></div>';
                    }elseif ($model->status=="8"){
                        return '<div id='.$model->payid.'><span style="color:blue;"><id='.$model->payid.'>'.\Lang::get('withdraw.pending').'</span></div>';
                    }elseif ($model->status=="7"){
                        return '<div id='.$model->payid.'><span style="color:blue;"><id='.$model->payid.'>'.\Lang::get('withdraw.Invalid').'</span></div>';
                    }
                    elseif ($model->status=="2"){
                        return '<div id='.$model->payid.'><span style="color:green;"><id='.$model->payid.'>'.\Lang::get('withdraw.success').'</span></div>';
                    }
                })

                ->rawColumns(['action','status'])
                ->make(true);
                
    }

    /**
     * Get Model by id.
     *
     * @param  int  $id
     * @return \App\Models\Model
     */

}