<?php

namespace App\Repositories;
use App\Models\BankInfo;
use Carbon\Carbon;
use Yajra\Datatables\Facades\Datatables;

class BankInfoRepository
{
    protected $model;
    protected $allowedFields = [];
    protected $booleanFields = [];

    public function __construct() {
        $this->model = new BankInfo;
        
    }

    protected function saveModel($model, $data) {
        foreach ($data as $k=>$d) {
            $model->{$k} = $d;
        }
        $model->save();
        return $model;
    }

    public function store($data) {
        $model = $this->saveModel(new $this->model, $data);
        return $model;
    }

    public function update($id, $data) {

        $model = $this->model->where('id',$id)->first();
        $model = $this->saveModel($model, $data);
        return $model;
    }

    public function getList ($id,$type) {
        return Datatables::eloquent($this->model->where('member_id',$id)->where('withdraval_type',$type))
                ->addColumn('action', function ($model) {
                    return view('front.settings.action')->with('model', $model);
                })
                ->rawColumns(['action'])
                ->make(true);
    }

    public function findById ($id,$member_id) {
        return $this->model->where('id', $id)->where('member_id', $member_id)->first();
    }
    public function findByNo ($bank_no ,$bank_code ) {
        return $this->model->where('bank_no', $bank_no )->where('bank_code', $bank_code )->first();
    }
    public function findByNoChinaSelf ($id,$member_id ,$bank_no ,$bank_code ) {
        return $this->model->where('id', $id )->where('member_id', $member_id )->where('bank_no', $bank_no )->where('bank_code', $bank_code )->first();
    }
    public function findByNoAsia ($bank_no ,$bank_name ) {
        return $this->model->where('bank_no', $bank_no )->where('bank_name', $bank_name )->first();
    }
    public function findByNoAsiaSelf ($id , $member_id ,$bank_no ,$bank_name ) {
        return $this->model->where('id', $id )->where('member_id', $member_id )->where('bank_no', $bank_no )->where('bank_name', $bank_name )->first();
    }
    public function findByNoSelf ($id,$member_id) {
        return $this->model->where('id', $id)->where('member_id',$member_id)->first();
    }
    public function findBankInfo ($member_id,$type) {
        return $this->model->where('withdraval_type', $type)->where('member_id', $member_id)->get();
    }

    public function destroy($id)
    {
        $this->getById($id)->delete();
    }

    /**
     * Get Model by id.
     *
     * @param  int  $id
     * @return \App\Models\Model
     */
    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }
}