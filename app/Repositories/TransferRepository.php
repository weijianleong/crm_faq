<?php
    
    namespace App\Repositories;
    
    use App\Models\Transfer;
    use App\Models\Trade;
    use Yajra\Datatables\Facades\Datatables;
    use App\Repositories\MemberRepository;
    use Carbon\Carbon;
    use Illuminate\Support\Str;
    use DB;
    
    class TransferRepository extends BaseRepository
    {
        protected $model;
        protected $allowedFields = [];
        protected $booleanFields = [];
        
        public function __construct() {
            $this->model = new Transfer;
        }
        
        protected function saveModel($model, $data) {
            foreach ($data as $k=>$d) {
                $model->{$k} = $d;
            }
            $model->save();
            return $model;
        }
        
        public function store($data) {
            $model = $this->saveModel(new $this->model, $data);
            return $model;
        }
        
        public function update($model, $data) {
            $model = $this->saveModel($model, $data);
            return $model;
        }
        
        public function getAllowedFields () {
            return $this->allowedFields;
        }
        
        public function getBooleanFields () {
            return $this->booleanFields;
        }
        
        public function findById ($id) {
            return $this->model->where('id', $id)->first();
        }
        
        /**
         * All Transfers - DataTable admin
         * @param  boolean $table
         * @return object
         */
        public function findAll ($table=false) {
            if (!$table) return $this->model->all();
            else {
                return Datatables::eloquent($this->model->query())
                ->addColumn('action', function ($model) {
                            return view('back.transfer.action')->with('model', $model);
                            })
                ->editColumn('amount', function ($model) {
                             return number_format($model->amount, 2);
                             })
                ->rawColumns(['action', 'status'])
                ->make(true);
            }
        }
        
        /**
         * Transfer List - DataTable member
         * @param  App\Models\Member $member [description]
         * @return object
         */
        public function getList ($member) {
            return Datatables::queryBuilder(DB::table('Member_Wallet_Statement'))->make(true);
        }
        
        /**
         * Make the point transfer
         * @param  App\Models\Member $from
         * @param  App\Models\Member $to
         * @param  float $amount
         * @param  string $type
         * @return boolean
         */
        public function makeTransfer ($from, $to, $amount, $remark, $wallettype) {
            /*
             if (!$this->checkCanTransfer($from, $to)) {
             throw new \Exception(\Lang::get('error.transferMemberError'), 1);
             return false;
             }
             
             if ($amount % 10 != 0) {
             throw new \Exception(\Lang::get('error.transferAmountError'), 1);
             return false;
             }
             */
            $walletFrom = $from->wallet;
            $walletTo = $to->wallet;
            $amount = (float) $amount;
            
            if($wallettype == 'C')
            {
                if ($walletFrom->cash_point < $amount || $amount < 0) {
                    throw new \Exception(\Lang::get('error.cashPointNotEnough'), 1);
                    return false;
                }
            }
            elseif($wallettype == 'R')
            {
                if ($walletFrom->register_point < $amount|| $amount < 0) {
                    throw new \Exception(\Lang::get('error.registerNotEnough'), 1);
                    return false;
                }
            }
            elseif($wallettype == 'T')
            {
                if ($walletFrom->t_wallet < $amount|| $amount < 0) {
                    throw new \Exception(\Lang::get('error.tNotEnough'), 1);
                    return false;
                }
            }
            
            if($wallettype == 'C')
            {
                $remark1 = 'Debited '.$amount.' for transfer cash to member ID #'.$to->username;
                $remark2 = 'Credit '.$amount.' for transfer cash from member ID #'.$from->username;

                $remark1Ary = array('amount'=>$amount,'username'=>$to->username);
                $remark2Ary = array('amount'=>$amount,'username'=>$from->username);
                $remark_display1 = 'TH00007/'.json_encode($remark1Ary);
                $remark_display2 = 'TH00004/'.json_encode($remark2Ary);
                
                if ($from->id != $to->id) {
                    $walletFrom->cash_point -= $amount;
                    $walletTo->cash_point += $amount;
                    $walletFrom->save();
                    $walletTo->save();
                }
                
                $title = 'Transfer to Cash Wallet';
                $title2 = 'Transfer from Cash Wallet';
                
                $this->saveWalletStatement($from->id, $to->username, $amount ,$title,$wallettype, 'D',$walletFrom->cash_point,$remark1,$remark_display1,'');
                
                $this->saveWalletStatement($to->id, $from->username, $amount ,$title2,$wallettype, 'C',$walletTo->cash_point,$remark2,$remark_display2,'');
            }
            elseif($wallettype == 'R')
            {
                $remark1 = 'Debited '.$amount.' for transfer R wallet to member ID #'.$to->username;
                $remark2 = 'Credit '.$amount.' for transfer R wallet from member ID #'.$from->username;

                $remark1Ary = array('amount'=>$amount,'username'=>$to->username);
                $remark2Ary = array('amount'=>$amount,'username'=>$from->username);
                $remark_display1 = 'TH00037/'.json_encode($remark1Ary);
                $remark_display2 = 'TH00038/'.json_encode($remark2Ary);
                
                if ($from->id != $to->id) {
                    $walletFrom->register_point -= $amount;
                    $walletTo->register_point += $amount;
                    $walletFrom->save();
                    $walletTo->save();
                }
                $title = 'Transfer to R Wallet';
                $title2 = 'Transfer from R Wallet';
                
                $this->saveWalletStatement($from->id, $to->username, $amount ,$title,$wallettype, 'D',$walletFrom->register_point,$remark1,$remark_display1,'');
                
                $this->saveWalletStatement($to->id, $from->username, $amount ,$title2,$wallettype, 'C',$walletTo->register_point,$remark2,$remark_display2,'');
            }
            elseif($wallettype == 'T')
            {

                $ranking = DB::table('Member_Sales_Summary')->where('member_id', $to->id)->orderBy('date', 'DESC')->first();
                
                $remark1 = 'Debited '.$amount.' for transfer t wallet to member ID #'.$to->username;
                $remark2 = 'Credit '.$amount.' for transfer t wallet from member ID #'.$from->username;

                $remark1Ary = array('amount'=>$amount,'username'=>$to->username);
                $remark2Ary = array('amount'=>$amount,'username'=>$from->username);
                $remark_display1 = 'TH00068/'.json_encode($remark1Ary);
                $remark_display2 = 'TH00069/'.json_encode($remark2Ary);
                $remark_display3 = 'TH00072/'.json_encode($remark2Ary);

                $title = 'Transfer to T Wallet';
                $title2 = 'Transfer from T Wallet';

                if ($from->id != $to->id) {
                    $walletQuery = DB::table('Member_Wallet')->where('member_id', $from->id);
                    $walletQuery->decrement('t_wallet', $amount);
                    $tBalance = $walletQuery->first(['t_wallet'])->t_wallet;
                    $this->saveWalletStatement($from->id, $to->username, $amount ,$title, $wallettype, 'D', $tBalance, $remark1, $remark_display1, '');

                    if($tBalance >= 0){
                        if(isset($ranking)){
                            if(!$ranking->rank) {
                                $walletTo->cash_point += $amount;
                                $walletTo->save();
                                $this->saveWalletStatement($to->id, $from->username, $amount ,$title2, 'C', 'C', $walletTo->cash_point, $remark2, $remark_display3, '');
                            }
                            else {
                                $walletTo->t_wallet += $amount;
                                $walletTo->save();
                                $this->saveWalletStatement($to->id, $from->username, $amount ,$title2, $wallettype, 'C', $walletTo->t_wallet, $remark2, $remark_display2, '');
                            }
                        }else{
                            $walletTo->cash_point += $amount;
                            $walletTo->save();
                            $this->saveWalletStatement($to->id, $from->username, $amount ,$title2, 'C', 'C', $walletTo->cash_point, $remark2, $remark_display3, '');
                        }
                    }
                }
            }
            
            $this->saveModel(new $this->model, [
                             'from_member_id' =>  $from->id,
                             'to_member_id' =>  $to->id,
                             'from_username' => $from->username,
                             'to_username' => $to->username,
                             'remark'  =>  $remark,
                             'amount'    =>  $amount,
                             'type'    =>  $wallettype
                             ]);
            
            return true;
        }
        
        public function makeMT5Transfer ($from, $account, $amountA, $amountB) {
            
            $book = DB::table('Member_Account')->where('id', '=', $account)->first();
            
            $bookA =$book->bookA;
            $bookB =$book->bookB;
            
            $walletFrom = $from->wallet;
            
            $amountA = (float) $amountA;
            $aamountB =(float) $amountB;
            $amountB = (float) $amountB/2;
            
            
            $buyR = 0;
            
            if($walletFrom->register_point == 0)
            {
                $cashneeded = $amountA + $aamountB;
                $buyR = $amountB;
            }
            else
            {
                $rdeduct = $walletFrom->register_point - $amountB;
                
                if($rdeduct >= 0)
                {
                    $cashneeded = $amountA+$amountB;
                    $buyR = 0;
                }
                else
                {
                    $cashneeded = $amountA + $amountB + ($amountB-$walletFrom->register_point);
                    $buyR = $amountB-$walletFrom->register_point;
                }
                
            }
            $total = $walletFrom->cash_point+$cashneeded;
            if ($walletFrom->cash_point < $cashneeded) {
                throw new \Exception(\Lang::get('error.cashPointNotEnough'), 1);
                //throw new \Exception($total, 1);
                return false;
            }
            
            
            $admin_wallet = DB::table('Member_Wallet')->where('member_id', '=', 1)->first();
            
            $purchaseamount = $admin_wallet->purchase_point + $amountB;
            
            
            DB::table('Member_Wallet')
            ->where('member_id', 1)
            ->update(['purchase_point' => $purchaseamount, 'updated_at' => \Carbon\Carbon::now()]);
            
            
            if($buyR > 0)
            {
                $buyRPriority = $this->makeBuyPriority($from,$buyR);
                $title = 'Buy R-Wallet';
                
                
                $remark3 = 'Credited '.$buyR.' for buy R Wallet';
                $remark5 = 'Debited '.$buyR.' for buy R Wallet';

                $remark3Ary = array('amount'=>$buyR);
                $remark5Ary = array('amount'=>$buyR);
                $remark_display3 = 'TH00039/'.json_encode($remark3Ary);
                $remark_display5 = 'TH00040/'.json_encode($remark5Ary);
                
                $walletFrom->cash_point -= $buyR;
                $walletFrom->register_point += $buyR;
                $walletFrom->save();
                
                
                $this->saveWalletStatement($from->id,'', $buyR,$title,'C', 'D',$walletFrom->cash_point,$remark5,$remark_display5,'');
                $this->saveWalletStatement($from->id,'', $buyR,$title,'R', 'C',$walletFrom->register_point,$remark3,$remark_display3,'');
                
            }
            
            $memberRepo = new MemberRepository;
            $newamount =$amountB+$buyR;
            $remark = 'Debited '.$amountA.' for transfer to MT5 account '.$bookA;
            $remark2 = 'Debited '.$amountB.' for transfer to MT5 account '.$bookB;
            //$remark4 = 'Debited '.$amountB.' for transfer to MT5 account '.$bookB;

            $remarkAry = array('amount'=>$amountA,'account'=>$bookA);
            $remark2Ary = array('amount'=>$amountB,'account'=>$bookB);
            $remark_display = 'TH00008/'.json_encode($remarkAry);
            $remark_display2 = 'TH00008/'.json_encode($remark2Ary);
            
            $title = 'Transfer to MT5';
            
            
            if($amountA > 0)
            {
                $walletFrom->cash_point -= $amountA;
                
                $walletFrom->save();
                
                $this->saveWalletStatement($from->id,'', $amountA,$title,'C', 'D',$walletFrom->cash_point,$remark,$remark_display,$bookA);
                $updatebalA = $memberRepo->BalanceAPI($bookA,$amountA, 'true', $title);
            }
            
            if($amountB > 0)
            {
                
                
                $walletFrom->register_point -= $amountB;
                $walletFrom->cash_point -= $amountB;
                $walletFrom->save();
                $this->saveWalletStatement($from->id,'', $amountB,$title,'R', 'D',$walletFrom->register_point,$remark2,$remark_display2,'');
                $this->saveWalletStatement($from->id,'', $amountB,$title,'C', 'D',$walletFrom->cash_point,$remark2,$remark_display2,'');
                
                $updatebalB = $memberRepo->BalanceAPI($bookB,$aamountB,'true',$title);
                $updatecredit = $memberRepo->CreditAPI($bookB,$aamountB,$title);
            }
            
            
            
            
            
            return true;
        }
        
        public function makeMT5TransferSelf ($from, $account, $amountA) {
            
            $book = DB::table('Member_Account_Self')->where('id', '=', $account)->first();
            
            $bookA =$book->bookA;
            
            $walletFrom = $from->wallet;
            
            if ($walletFrom->cash_point < $amountA) {
                throw new \Exception(\Lang::get('error.cashPointNotEnough'), 1);
                return false;
            }
            
            $title = 'Transfer to MT5';
            $remark = 'Debited '.$amountA.' for transfer to MT5 account '.$bookA;

            $remarkAry = array('amount'=>$amountA,'account'=>$bookA);
            $remark_display = 'TH00008/'.json_encode($remarkAry);
            
            if($amountA > 0) {
                $walletQuery = DB::table('Member_Wallet')->where('member_id', $from->id);
                $walletQuery->decrement('cash_point', $amountA);
                $cashBalance = $walletQuery->first(['cash_point'])->cash_point;
                $this->saveWalletStatement($from->id,'', $amountA,$title,'C', 'D',$cashBalance,$remark,$remark_display,$bookA);
                if($cashBalance >= 0){
                    $memberRepo = new MemberRepository;
                    $updatebalA = $memberRepo->BalanceAPI($bookA,$amountA, 'true', $title);
                }
            }
            
            return true;
        }
        
        public function makeMT5TransferFund ($from, $account, $package) {
            
            $book = DB::table('Member_Account_Fund')->where('id', '=', $account)->first();
            
            $bookA =$book->bookA;
            $bookB =$book->bookB;
            $bookBbal =$book->balanceB;
            
            $walletFrom = $from->wallet;
            
            $package = DB::table('Package')->where('id', '=', $package)->first();
            
            $package_amount = $package->package_amount;
            
            if ($walletFrom->cash_point < $package_amount) {
                throw new \Exception(\Lang::get('error.cashPointNotEnough'), 1);
                return false;
            }
            
            $amountA = (float) $package->cash_amount;
            $aamountB =(float) $package->b_amount;
            $amountB = (float) $aamountB/2;

            $totalCheck = $aamountB + $bookBbal;
            if($totalCheck > 10000){
                throw new \Exception(\Lang::get('error.transferFund'),1);
                return false;
            }

            $buyB = 0;
            
            $admin_wallet = DB::table('Member_Wallet')->where('member_id', '=', 1)->first();
            
            $purchaseamount = $admin_wallet->purchase_point + $amountB;
            
          /*
            DB::table('Member_Wallet')
            ->where('member_id', 1)
            ->update(['purchase_point' => $purchaseamount, 'updated_at' => \Carbon\Carbon::now()]);
          */
            
            $title = 'Buy B-Wallet';
            
            $remark3 = 'Credited '.$amountB.' for buy B Wallet';
            $remark5 = 'Debited '.$amountB.' for buy B Wallet';

            $remark3Ary = array('amount'=>$amountB);
            $remark5Ary = array('amount'=>$amountB);
            $remark_display3 = 'TH00016/'.json_encode($remark3Ary);
            $remark_display5 = 'TH00001/'.json_encode($remark5Ary);
            
            // $walletFrom->cash_point -= $amountB;
            $walletFrom->b_wallet += $amountB;
            $walletFrom->save();
            
            
            // $this->saveWalletStatement($from->id,'', $amountB,$title,'C', 'D',$walletFrom->cash_point,$remark5,$remark_display5,'');
            $this->saveWalletStatement2($from->id,'', $amountB,$title,'BW', 'C',$walletFrom->b_wallet,$remark3,$remark_display3,'',0);
            
            
            
            $memberRepo = new MemberRepository;
            //$newamount =$amountB+$buyR;
            $remark = 'Debited '.$amountA.' for transfer to MT5 account '.$bookA;
            $remark2 = 'Debited '.$amountB.' for transfer to MT5 account '.$bookB;
            //$remark6 = 'Credited '.$deposit_amount.' package deposit on MT5 account '.$bookA;
            //$remark7 = 'Debited '.$deposit_amount.' package deposit on MT5 account '.$bookA;
            //$remark4 = 'Debited '.$amountB.' for transfer to MT5 account '.$bookB;

            $remarkAry = array('amount'=>$amountA,'account'=>$bookA);
            $remark2Ary = array('amount'=>$amountB,'account'=>$bookB);
            $remark_display = 'TH00008/'.json_encode($remarkAry);
            $remark_display2 = 'TH00008/'.json_encode($remark2Ary);

            $remark2a = 'Debited '.$aamountB.' for transfer to MT5 account '.$bookB;
            $remark2aAry = array('amount'=>$aamountB,'account'=>$bookB);
            $remark_display2a = 'TH00008/'.json_encode($remark2aAry);
            
            $title = 'Transfer to MT5';
            
            
            
            $walletFrom->cash_point -= $amountA;
            
            $walletFrom->save();
            
            $this->saveWalletStatement($from->id,'', $amountA,$title,'C', 'D',$walletFrom->cash_point,$remark,$remark_display,$bookA);
            
            $updatebalA = $memberRepo->BalanceAPI($bookA,$amountA, 'true', $title);
            
            
            $walletFrom->b_wallet -= $amountB;
            $walletFrom->cash_point -= $aamountB;
            //$walletFrom->wd_wallet += $deposit_amount;
            $walletFrom->save();
            $this->saveWalletStatement2($from->id,'', $amountB,$title,'BW', 'D',$walletFrom->b_wallet,$remark2,$remark_display2,'',0);
            // $this->saveWalletStatement($from->id,'', $aamountB,$title,'C', 'D',$walletFrom->cash_point,$remark2,$remark_display2,'');
            $this->saveWalletStatement($from->id,'', $aamountB,$title,'C', 'D',$walletFrom->cash_point,$remark2a,$remark_display2a,'');
            
            //$walletFrom->cash_point -= $deposit_amount;
           // $walletFrom->save();
            
           // $this->saveWalletStatement2($from->id,'', $deposit_amount,$title,'C', 'D',$walletFrom->cash_point,$remark7);
           // $this->saveWalletStatement2($from->id,'', $deposit_amount,$title,'WD', 'C',$walletFrom->wd_wallet,$remark6);
            
            DB::table('Member_Account_Fund')
            ->where('bookB', $bookB)
            ->update(['balanceB' => $bookBbal + $aamountB, 'updated_at' => \Carbon\Carbon::now()]);
            
            //$updatebalB = $memberRepo->BalanceAPI($bookB,$aamountB,'true',$title);
            //$updatecredit = $memberRepo->CreditAPI($bookB,$aamountB,$title);
            
            
            
            
            
            
            
            return true;
        }
        
         public function makeMT5TransferBFund ($from, $bookB, $amount, $bookA, $batch_id) {
             
            $walletFrom = $from->wallet;
            

            $aamountB =(float) $amount;
            $amountB = (float) $aamountB/2;
            
            
            $buyB = 0;
            
            
            $admin_wallet = DB::table('Member_Wallet')->where('member_id', '=', 1)->first();
            
            $purchaseamount = $admin_wallet->purchase_point + $amountB;

            
            $title = 'Buy B-Wallet';
            
            $remark3 = 'Credited '.$amountB.' for buy B Wallet';
            $remark5 = 'Debited '.$amountB.' for buy B Wallet';

            $remark3Ary = array('amount'=>$amountB);
            $remark5Ary = array('amount'=>$amountB);
            $remark_display3 = 'TH00016/'.json_encode($remark3Ary);
            $remark_display5 = 'TH00001/'.json_encode($remark5Ary);
            
            // $walletFrom->w_wallet -= $amountB;
            $walletFrom->b_wallet += $amountB;
            $walletFrom->save();
            
            
            // $this->saveWalletStatement2($from->id,'', $amountB,$title,'W', 'D',$walletFrom->w_wallet,$remark5,$remark_display5,$bookA,$batch_id);
            $this->saveWalletStatement2($from->id,'', $amountB,$title,'BW', 'C',$walletFrom->b_wallet,$remark3,$remark_display3,$bookA,$batch_id);
            
            
            
            $memberRepo = new MemberRepository;
            //$newamount =$amountB+$buyR;
            $remark2 = 'Debited '.$amountB.' for transfer to MT5 account '.$bookB;
            $remark2Ary = array('amount'=>$amountB,'account'=>$bookB);
            $remark_display2 = 'TH00008/'.json_encode($remark2Ary);

            $remark2a = 'Debited '.$aamountB.' for transfer to MT5 account '.$bookB;
            $remark2aAry = array('amount'=>$aamountB,'account'=>$bookB);
            $remark_display2a = 'TH00008/'.json_encode($remark2aAry);

            
            $title = 'Transfer to MT5';
            
            
            
            $walletFrom->b_wallet -= $amountB;
            $walletFrom->w_wallet -= $aamountB;
         
            $walletFrom->save();
            $this->saveWalletStatement2($from->id,'', $amountB,$title,'BW', 'D',$walletFrom->b_wallet,$remark2,$remark_display2,$bookA,$batch_id);
            // $this->saveWalletStatement2($from->id,'', $amountB,$title,'W', 'D',$walletFrom->w_wallet,$remark2,$remark_display2,$bookA,$batch_id);
            $this->saveWalletStatement2($from->id,'', $aamountB,$title,'W', 'D',$walletFrom->w_wallet,$remark2a,$remark_display2a,$bookA,$batch_id);
            
            
            
            
            return true;
        }
        
        public function makeMT5TransferComm ($from, $amount, $ramount, $bookA, $batch_id, $slippage) {
            
            $walletFrom = $from->wallet;
            
            $amount2 =(float) $amount;
            
            $ramount2 =(float) $ramount;
            
            $title = '33% LPOA';
        
            $remark5 = 'Debited '.$amount2.' for 33.33% LPOA fee';

            $remark5Ary = array('amount'=>$amount2);
            $remark_display5 = 'TH00020/'.json_encode($remark5Ary);
            
            $walletFrom->w_wallet -= $amount2;
            $walletFrom->save();
            
            
            $this->saveWalletStatement2($from->id,'', $amount2,$title,'W', 'D',$walletFrom->w_wallet,$remark5,$remark_display5,$bookA,$batch_id);
            
            $title = '2% Reserve';

            if($slippage > 0){
                $remark5 = 'Debited 2/3 slippage '.$ramount2.' for 20% of investment profit for reserved fund (R Wallet)';
                
                $remark5Ary = array('amount'=>$ramount2);
                $remark_display5 = 'TH00064/'.json_encode($remark5Ary);
            }else{
                $remark5 = 'Debited '.$ramount2.' for 20% of investment profit for reserved fund (R Wallet)';
                
                $remark5Ary = array('amount'=>$ramount2);
                $remark_display5 = 'TH00019/'.json_encode($remark5Ary);
            }

            $walletFrom->w_wallet -= $ramount2;
            $walletFrom->save();
            
            
            $this->saveWalletStatement2($from->id,'', $ramount2,$title,'W', 'D',$walletFrom->w_wallet,$remark5,$remark_display5,$bookA,$batch_id);

            
            
            
            
            return true;
        }
        
        public function makeMT5SubscribeFund ($from, $account, $fundcompany) {
            
            $memberRepo = new MemberRepository;
            
            $book = DB::table('Member_Account_Fund')->where('id', '=', $account)->first();
            $bookA = $book->bookA;
            $newpassword = 'XSw21qaz';
            
            if($fundcompany == 0)
            {
                $fundcompany = 268;
            }
            
            $fundcomp = DB::connection('mysql3')->table('user')->where('UserId', '=', $fundcompany)->first();
            $fundgroup = $fundcomp->MT5Group;
            
            
            
            //$movein = $memberRepo->AccountMoveGroupAPI($bookA,'live\\H\\'.$fundgroup.'\\Manage');
            
            
            $updatepass = $memberRepo->UpdateMasterPasswordAPI($bookA,$newpassword);
            
            DB::table('Member_Account_Fund')
            ->where('id', '=', $account)
            ->where('member_id', '=', $from->id)
            ->update(['system_manage' => 'S','fundcompany' => $fundcompany, 'subscribe_date' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'ini_amount' => $book->balanceB]);
            
            
            return true;
        }
        
        public function makeMT5UnsubscribeFund ($from, $account) {
            
            DB::table('Member_Account_Fund')
            ->where('bookA', '=', $account)
            ->where('member_id', '=', $from->id)
            ->update(['system_manage' => 'U', 'updated_at' => \Carbon\Carbon::now(), 'request_unsubscribe_date' => \Carbon\Carbon::now(), 'cancel_request_unsubscribe' => NULL]);
            
            
            return true;
        }

        public function makeMT5CancelUnsubscribeFund ($from, $account) {
            
            DB::table('Member_Account_Fund')
            ->where('bookA', '=', $account)
            ->where('member_id', '=', $from->id)
            ->update(['system_manage' => 'S', 'updated_at' => \Carbon\Carbon::now(), 'cancel_request_unsubscribe' => \Carbon\Carbon::now()]);
            
            
            return true;
        }
        
        public function makeMT5SwitchFund ($from, $bookA, $fundcompany) {
            
            DB::table('Member_Account_Fund')
            ->where('bookA', '=', $bookA)
            ->where('member_id', '=', $from->id)
            ->update(['newfundrequest' => 'Y','newfundcompany' => $fundcompany, 'newfunddate' => \Carbon\Carbon::now()]);
            
            
            return true;
        }
        
        public function makeMT5Process ($from, $bookid, $bookA, $bookB, $register_balance) {
            
            $error = 0;
            $memberRepo = new MemberRepository;
            
            $book = DB::table('Member_Account')->where('id', '=', $bookid)->first();
            
            $bookAbalAPI = $memberRepo->AccountAPI($bookA);
            $bookBbalAPI = $memberRepo->AccountAPI($bookB);
            
            $balanceA = $bookAbalAPI->balance;
            $balanceB = $bookBbalAPI->balance;
            $creditB = $bookBbalAPI->credit;
            
            
            
            $orderAP = DB::connection('mysql2')->table('historyposition')->where('Login', '=', $bookA)->orderBy('OrderId', 'desc')->first();
            
            if(count($orderAP) == 0)
                $history = $memberRepo->HistoryAPI($bookA);
            
            //$orderAP = end($orderA);
            $orderBP = DB::connection('mysql2')->table('historyposition')->where('Login', '=', $bookB)->orderBy('OrderId', 'desc')->first();
            //$orderB = $memberRepo->getOrders($bookB);
            //$orderBP = end($orderB);
            if(count($orderBP) == 0)
                $history = $memberRepo->HistoryAPI($bookB);
            
            //print_r($orderBP);
            $openAtime = $orderAP->OpenTime;
            $openAtime1 = new \DateTime($openAtime);
            //$openAtime2 = explode("(", $openAtime1[0]);
            $openAtime3 = $openAtime1->format('U');
            //$openAtime4 = date("Y-m-d H:i:s", $openAtime3);
            
            
            $openBtime = $orderBP->OpenTime;
            $openBtime1 = new \DateTime($openBtime);
            //$openBtime2 = explode("(", $openBtime1[0]);
            $openBtime3 = $openBtime1->format('U');
            //$openBtime4 = date("Y-m-d H:i:s", $openBtime3);
            
            
            $closeAtime = $orderAP->CloseTime;
            $closeAtime1 = new \DateTime($closeAtime);
            //$closeAtime2 = explode("(", $closeAtime1[0]);
            $closeAtime3 = $closeAtime1->format('U');
            //$closeAtime4 = date("Y-m-d H:i:s", $closeAtime3);
            
            
            $openBtime = $orderBP->CloseTime;
            $closeBtime1 = new \DateTime($openBtime);
            //$closeBtime2 = explode("(", $closeBtime1[0]);
            $closeBtime3 = $closeBtime1->format('U');
            //$closeBtime4 = date("Y-m-d H:i:s", $closeBtime3);
            
            
            
            $order_idA = $orderAP->OrderId;
            $opentimeA = $openAtime3;
            $openpriceA = $orderAP->OpenPrice;
            $currencypairA = $orderAP->Symbol;
            $lotsizeA =  $orderAP->Volume;
            $closetimeA = $closeAtime3;
            $closepriceA = $orderAP->ClosePrice;
            $amountA = $orderAP->Profit;
            $swapA = $orderAP->Storage;
            $profitA = $amountA + $swapA;
            
            $order_idB = $orderBP->OrderId;
            $opentimeB = $openBtime3;
            $openpriceB = $orderBP->OpenPrice;
            $currencypairB = $orderBP->Symbol;
            $lotsizeB = $orderBP->Volume;
            $closetimeB = $closeBtime3;
            $closepriceB = $orderBP->ClosePrice;
            $amountB = $orderBP->Profit;
            $swapB = $orderBP->Storage;
            $profitB = $amountB + $swapB;
            
            
            $lotsizeAA = (ceil($lotsizeB*10)/10)/10000;
            $lotsizeBB = $lotsizeB/10000;
            
            //Condition - 1 - check lot size
            if($lotsizeAA <> $lotsizeBB)
                $error = 1;
            //Condition - 2 check open time
            if($opentimeA-$opentimeB > 120 || $opentimeB-$opentimeA > 120)
                $error = 1;
            //Condition 3 - profit
            if($profitA <= 0 && $profitB <= 0)
                $error = 1;
            //Condition - 4 - check SL/TP
            
            $priceAdifferent = $closepriceA - $openpriceA;
            
            if($profitA > 0)
            {
                $status = 'A';
                
                
                
                if($profitA >= $creditB)
                {
                    if($balanceB < 0)
                    {
                        $updateBalanceB = abs($balanceB);
                        
                    }
                    elseif($balanceB > 0)
                    {
                        $updateBalanceB = -$balanceB;
                        
                    }
                    
                    $updateCreditB = -$creditB;
                    
                }
                else
                {
                    
                    $updateBalanceB = -($profitA + $profitB);
                    
                    $updateCreditB = -$profitA;
                    
                    
                }
                
                
                $updatebalB = $memberRepo->BalanceAPI2($bookB,$updateBalanceB,'Win A Zerorise');
                $updatecredit = $memberRepo->CreditAPI($bookB,$updateCreditB,'Win A Zerorise');
                
                DB::table('Orders')->insert([
                                            'member_username' => $from->username,
                                            'bookB' => $bookB,
                                            'bookAorderid' => $order_idA,
                                            'status' => 'P',
                                            'balancea' => $balanceA,
                                            'balanceb' => $balanceB,
                                            'profitA' => $profitA,
                                            'profitB' => $profitB,
                                            'updatebalanceb' => $updateBalanceB,
                                            'updatecreditb' => $updateCreditB,
                                            'rwallet' => 0,
                                            'created_at' => \Carbon\Carbon::now(),
                                            'updated_at' => \Carbon\Carbon::now()
                                            ]);
                
                DB::table('Member_Account')
                ->where('id', $bookid)
                ->update(['status' => '0','rwallet' => '0','winB' => '0','lockdate' => null, 'updated_at' => \Carbon\Carbon::now()]);
            }
            else
            {
                
                
                $status = 'B';
                
                $priceAdifferent = abs($closepriceA - $openpriceA);
                
                if($profitA > 0)
                {
                    if($currencypairA == 'XAUUSD+')
                    {
                        if($priceAdifferent <= 9.2 || $priceAdifferent >= 10.1)
                            $error = 1;
                        
                    }
                    elseif($currencypairA == 'GBPUSD+' || $currencypairA == 'EURUSD+' ||$currencypairA == 'AUDUSD+' || $currencypairA == 'NZDUSD+')
                    {
                        if($priceAdifferent <= 0.0096 || $priceAdifferent >= 0.0106)
                            $error = 1;
                    }
                    else
                    {
                        if($priceAdifferent <= 1.005 || $priceAdifferent >= 1.105)
                            $error = 1;
                    }
                    
                }
                else
                {
                    if($currencypairA == 'XAUUSD+')
                    {
                        if($priceAdifferent <= 6.82 || $priceAdifferent >= 7.82)
                            $error = 1;
                        
                    }
                    elseif($currencypairA == 'GBPUSD+' || $currencypairA == 'EURUSD+' ||$currencypairA == 'AUDUSD+' || $currencypairA == 'NZDUSD+')
                    {
                        if($priceAdifferent <= 0.00675 || $priceAdifferent >= 0.00775)
                            $error = 1;
                    }
                    else
                    {
                        if($priceAdifferent <= 0.726 || $priceAdifferent >= 0.826)
                            $error = 1;
                    }
                }
                
                $priceBdifferent = abs($closepriceB - $openpriceB);
                
                if($profitB > 0)
                {
                    if($currencypairB == 'XAUUSD-')
                    {
                        if($priceBdifferent <= 5.63 || $priceBdifferent >= 6.63)
                            $error = 1;
                        
                    }
                    elseif($currencypairB == 'GBPUSD-' || $currencypairB == 'EURUSD-' || $currencypairB == 'AUDUSD-' || $currencypairB == 'NZDUSD-')
                    {
                        if($priceBdifferent <= 0.00596 || $priceBdifferent >= 0.00696)
                            $error = 1;
                    }
                    else
                    {
                        if($priceBdifferent <= 0.63 || $priceBdifferent >= 0.73)
                            $error = 1;
                    }
                    
                }
                else
                {
                    if($currencypairB == 'XAUUSD-')
                    {
                        if($priceBdifferent <= 10.36 || $priceBdifferent >= 11.36)
                            $error = 1;
                        
                    }
                    elseif($currencypairB == 'GBPUSD-' || $currencypairB == 'EURUSD-' || $currencypairB == 'AUDUSD-' || $currencypairB == 'NZDUSD-')
                    {
                        if($priceBdifferent <= 0.1038 || $priceBdifferent >= 0.1138)
                            $error = 1;
                    }
                    else
                    {
                        if($priceBdifferent <= 1.1 || $priceBdifferent >= 1.2)
                            $error = 1;
                    }
                }
                
                $updateBalanceB = -$profitB;
                
                // if($profitB < abs($profitA))
                // {
                //     $updateRWallet = $profitB;
                // }
                // else
                // {
                $updateRWallet = abs($amountA);
                // }
                
                $winbookB = DB::table('Member_Account')->where('id', '=', $bookid)->first();
                $winbookB2 = $winbookB->winB + 1;
                
                if($winbookB2 == 1)
                {
                    $currentdate = \Carbon\Carbon::now();
                    $datetolock = $currentdate->addHours(24);
                }
                elseif($winbookB2 == 2)
                {
                    $currentdate = \Carbon\Carbon::now();
                    $datetolock = $currentdate->addHours(72);
                }
                elseif($winbookB2 == 3)
                {
                    $currentdate = \Carbon\Carbon::now();
                    $datetolock = $currentdate->addHours(168);
                }
                else
                {
                    $currentdate = \Carbon\Carbon::now();
                    $datetolock = $currentdate->addHours(336);
                }
                
                
                if($error == 1)
                {
                    DB::table('Orders')->insert([
                                                'member_username' => $from->username,
                                                'bookB' => $bookB,
                                                'bookAorderid' => $order_idA,
                                                'status' => 'E',
                                                'balancea' => $balanceA,
                                                'balanceb' => $balanceB,
                                                'profitA' => $profitA,
                                                'profitB' => $profitB,
                                                'updatebalanceb' => abs($balanceB),
                                                'rwallet' => $updateRWallet,
                                                'created_at' => \Carbon\Carbon::now(),
                                                'updated_at' => \Carbon\Carbon::now()
                                                ]);
                    
                    
                    DB::table('Member_Account')
                    ->where('id', $bookid)
                    ->update(['status' => $winbookB2,'rwallet' => '0','winB' => $winbookB2,'lockdate' => $datetolock, 'updated_at' => \Carbon\Carbon::now()]);
                    
                    
                }
                else
                {
                    DB::table('Orders')->insert([
                                                'member_username' => $from->username,
                                                'bookB' => $bookB,
                                                'bookAorderid' => $order_idA,
                                                'status' => 'P',
                                                'balancea' => $balanceA,
                                                'balanceb' => $balanceB,
                                                'profitA' => $profitA,
                                                'profitB' => $profitB,
                                                'updatebalanceb' => $updateBalanceB,
                                                'rwallet' => $updateRWallet,
                                                'created_at' => \Carbon\Carbon::now(),
                                                'updated_at' => \Carbon\Carbon::now()
                                                ]);
                    
                    //$updatebalB = $memberRepo->updateBalanceAPI2($username,$password,$bookB,$updateBalanceB);
                    //$updatebalB = $memberRepo->updateBalanceAPI($bookB,$updateBalanceB);
                    $updatebalB = $memberRepo->BalanceAPI2($bookB,$updateBalanceB,'Win B');
                    
                    
                    DB::table('Member_Account')
                    ->where('id', $bookid)
                    ->update(['status' => $winbookB2,'rwallet' => $updateRWallet,'winB' => $winbookB2,'lockdate' => $datetolock, 'updated_at' => \Carbon\Carbon::now()]);
                }
                
                
                
                
            }
            return $status;
        }
        
        public function makeMT5ProcessFund ($from, $bookA) {
            $batch_id = DB::table('Statement_BatchID')->insertGetId(['created_at' => \Carbon\Carbon::now()]);
            
            $error = 0;
            $memberRepo = new MemberRepository;
            
            $book = DB::table('Member_Account_Fund')->where('bookA', '=', $bookA)->first();
           
            $bookAbalAPI = $memberRepo->AccountAPI($bookA);
            $balanceA = $bookAbalAPI->balance;
            
            $bookB = $book->bookB;
            $balanceB = $book->balanceB;
            //$partnerid = $book->fundcompany;
            $system_manage = $book->system_manage;
          
            $currentpackage = DB::table('Package')->where('b_amount', '=', $balanceB)->first();
            
            $insurance_amount = config('misc.insurance');
            $max_insurance = $currentpackage->insurance_percent * $insurance_amount;
            $current_lotsize = $currentpackage->lotsize;
            $current_bookA = $currentpackage->cash_amount;
            
            $max_profit = $current_lotsize * 1000;
            
            $max_slipage = $max_profit * 20 / 100;
            
            $max_slipage_positive = $max_profit + $max_slipage;
            
            $max_slipage_negative = $max_slipage;
           
            $openOrders = DB::table('Orders_Fund')->where('status', '=', 'C')->where('bookA', '=', $bookA)->orderBy('bookAorderid', 'asc')->get();
         
            $profitA = 0;
            $swapA = 0;
            $lotsizeA = 0;
            $rprofit = 0;
      
            foreach($openOrders as $openOrder)
            {
                $bookAorderid = $openOrder->bookAorderid;
                
                $orderAP = DB::connection('mysql2')->table('historyposition')->where('OrderId', '=', $bookAorderid)->first();
                
                // print_r($orderAP);
                $order_idA = $orderAP->OrderId;
                $openpriceA = $orderAP->OpenPrice;
                $currencypairA = $orderAP->Symbol;
                $lotsizex =  $orderAP->Volume;
                $stoplossA =  $orderAP->StopLoss;
                $takeprofitA =  $orderAP->TakeProfit;
                $closepriceA = $orderAP->ClosePrice;
                $closetimeA = $orderAP->CloseTime;
                $profitx = $orderAP->Profit;
                $swapx = $orderAP->Storage;
                
                $partnerid = $orderAP->PartnerId;
                
                $profitA = $profitA + $profitx;
                $swapA = $swapA + $swapx;
                $lotsizeA = $lotsizeA + $lotsizex;
            }
            
            //$netprofitA = $profitA + $swapA;
            $netprofitA = $profitA;
            $lotsizeAA = $lotsizeA/10000;
            
            $mwalletx = 0;
            $swalletx = 0;
            $wwalletx = 0;
            $bwalletx = 0;
            $rwalletx = 0;

            if($netprofitA > 0)
            {
                $status = 'A';
                
                if($swapA <> 0)
                {
                    $bbalance = DB::table('Member_Wallet')->where('member_id', '=', $from->id)->first();
                    $wwallet_balance = $bbalance->w_wallet;
                    
                    if($swapA > 0)
                    {
                        $remark = 'Credited '.$swapA.' swap from trade '.$order_idA;

                        $remarkAry = array('amount'=>$swapA,'trade'=>$order_idA);
                        $remark_display = 'TH00027/'.json_encode($remarkAry);

                        $transaction_type = 'C';
                        $title = 'MT5 to W-Wallet';
                        
                        $user_swap = $swapA;
                        
                        $updatebalA = $memberRepo->BalanceAPI($bookA,$swapA, 'false', 'Swap Adjustment');
                    }
                    else
                    {
                        //$remark = 'Debited '.$swapA.' swap from trade '.$order_idA;
                        //$transaction_type = 'D';
                        
                        //$title = 'Transfer to MT5';
                        
                        $max_swap = ($balanceB * 3) * 2 / 100;
                        
                        $transaction_type = 'D';
                        
                        $title = 'MT5 to W-Wallet';
                        
                        if(abs($swapA) > $max_swap)
                        {
                            $user_swap = -($max_swap);
                            $r_swap = abs($swapA) - $max_swap;
                            
                            $remark = 'Debited '.$user_swap.' swap (Swap : '.$swapA.', R Wallet absorbed : '.$r_swap.') from trade '.$order_idA.' for MT5 account '.$bookA;
                            
                            $remarkAry = array('amount'=>$user_swap,'amountSwap'=>$swapA,'amountAbsorbed'=>$r_swap,'trade'=>$order_idA,'account'=>$bookA);
                            $remark_display = 'TH00041/'.json_encode($remarkAry);

                            DB::table('Rwallet')->decrement('r_amount',$r_swap);
                            
                            DB::table('Rwallet_log')
                            ->insert([
                                     'member_id' => $from->id,
                                     'orderid' => $order_idA,
                                     'bookA' => $bookA,
                                     'r_amount' => -($r_swap),
                                     'type' => 'SW',
                                     'created_at' => \Carbon\Carbon::now()
                                     ]);
                        }
                        else
                        {
                            //$user_swap = -($swapA);
                            $user_swap = $swapA;
                            
                            $remark = 'Debited '.$user_swap.' swap from trade '.$order_idA.' for MT5 account '.$bookA;
                            
                            $remarkAry = array('amount'=>$user_swap,'trade'=>$order_idA,'account'=>$bookA);
                            $remark_display = 'TH00028/'.json_encode($remarkAry);
                        }
                        
                        $updatebalA = $memberRepo->BalanceAPI($bookA, abs($swapA), 'true', 'Swap Adjustment');
                   
                    }
                    
                    $rprofit = $rprofit + $user_swap;
                    $wwallet_newbalance = $wwallet_balance + $user_swap;

                    DB::table('Member_Wallet')
                    ->where('member_id', $from->id)
                    ->update(['w_wallet' => $wwallet_newbalance]);
                    
                    DB::table('Member_Wallet_Statement_Fund')->insert([
                                                                      'member_id' => $from->id,
                                                                      'username' => $from->username,
                                                                      'w_amount' => abs($user_swap),
                                                                      'action_type' => $title,
                                                                      'wallet_type' => 'W',
                                                                      'transaction_type' => $transaction_type,
                                                                      'balance' =>  $wwallet_newbalance,
                                                                      'remark' => $remark,
                                                                      'bookA' => $bookA,
                                                                      'created_at' => \Carbon\Carbon::now(),
                                                                      'updated_at' => \Carbon\Carbon::now(),
                                                                      'remark_display' => $remark_display,
                                                                      'batch_id' => $batch_id
                                                                      ]);
                }
                
                if($netprofitA > $balanceB)
                {
                    if($system_manage <> 'U')
                    {
                        $rprofit = $rprofit + $netprofitA;
                        $rprofit = $rprofit - $balanceB;
                        $this->makeConvertaFund ($from, $netprofitA, $bookA, $batch_id);
                        $this->makeMT5TransferBFund ($from, $bookB, $balanceB, $bookA, $batch_id);
                    }
                    else
                    {
                        $balanceA2 = 0;
                        $bookAbalAPI2 = $memberRepo->AccountAPI($bookA);
                        $balanceA2 = $bookAbalAPI2->balance;
                        
                        if($balanceA2 == 0)
                        {
                            $bookAbalAPI2 = $memberRepo->AccountAPI($bookA);
                            $balanceA2 = $bookAbalAPI2->balance;
                            
                            if($balanceA2 == 0)
                            {
                                $bookAbalAPI2 = $memberRepo->AccountAPI($bookA);
                                $balanceA2 = $bookAbalAPI2->balance;
                            }
                        }
                        
                        $rprofit = $rprofit + $balanceA2;
                        $this->makeConvertaFund ($from, $balanceA2, $bookA, $batch_id);
                    }
                    
                    $newbalance = $netprofitA - $balanceB;

                    // slippage
                    $slippage = 0;

                    $slippage = $netprofitA - $max_slipage_positive;

                    if($slippage > 0){
                        $mwalletx = ($newbalance * 30) / 100;
                        $swalletx = ($newbalance * 3.33) / 100;
                        $newbalance = $newbalance - $slippage;
                        $wwalletx = ($newbalance * 53.34) / 100;
                        $rwalletx = ($newbalance * 13.33) / 100;
                    }else{
                        $mwalletx = ($newbalance * 30) / 100;
                        $swalletx = ($newbalance * 3.33) / 100;
                        $wwalletx = ($newbalance * 53.34) / 100;
                        $rwalletx = ($newbalance * 13.33) / 100;
                    }
                    
                    $txcomm = $mwalletx + $swalletx;
                    
                    $rprofit = $rprofit - $txcomm;
                    $rprofit = $rprofit - $rwalletx;
                    $this-> makeMT5TransferComm ($from, $txcomm, $rwalletx, $bookA, $batch_id, $slippage);
                    
                    DB::table('Swallet')->increment('s_amount',$swalletx);
                    
                    DB::table('Swallet_log')
                    ->insert([
                             'member_id' => $from->id,
                             'orderid' => $order_idA,
                             's_amount' => $swalletx,
                             'created_at' => \Carbon\Carbon::now()
                             ]);
                    
                    DB::table('Rwallet')->increment('r_amount',$rwalletx);
                    
                    DB::table('Rwallet_log')
                    ->insert([
                             'member_id' => $from->id,
                             'orderid' => $order_idA,
                             'bookA' => $bookA,
                             'type' => 'CP',
                             'r_amount' => $rwalletx,
                             'created_at' => \Carbon\Carbon::now()
                             ]);
                    
                    
                    DB::connection('mysql3')->table('user')->where('UserId', $partnerid)->increment('mwallet',$mwalletx);
                    
                    DB::table('Mwallet_log')
                    ->insert([
                             'member_id' => $from->id,
                             'orderid' => $order_idA,
                             'partnerid' => $partnerid,
                             'mwallet' => $mwalletx,
                             'created_at' => \Carbon\Carbon::now()
                             ]);

                    if($slippage > 0){
                        $rwalletx = ($slippage * 33.33) / 100;

                        DB::table('Rwallet')->increment('r_amount',$rwalletx);
                    
                        DB::table('Rwallet_log')
                        ->insert([
                                 'member_id' => $from->id,
                                 'orderid' => $order_idA,
                                 'bookA' => $bookA,
                                 'type' => 'SPP',
                                 'r_amount' => $rwalletx,
                                 'created_at' => \Carbon\Carbon::now()
                                 ]);

                        $walletFrom = $from->wallet;
                        $title = '2% Reserve';
            
                        $remark5 = 'Debited 1/3 slippage '.$rwalletx.' for reserved fund (R Wallet)';
                        
                        $remark5Ary = array('amount'=>$rwalletx);
                        $remark_display5 = 'TH00062/'.json_encode($remark5Ary);

                        $rprofit = $rprofit - $rwalletx;
                        $walletFrom->w_wallet -= $rwalletx;
                        $walletFrom->save();
                        
                        $this->saveWalletStatement2($from->id,'', $rwalletx,$title,'W', 'D',$walletFrom->w_wallet,$remark5,$remark_display5,$bookA,$batch_id);
                    }

                    $parent_remark = 'Account A profit (Account: '.$bookA.'; Trade: '.$order_idA.')';
                    $parent_remarkAry = array('account'=>$bookA, 'trade'=>$order_idA);
                    $parent_remark_display = 'TH00031/'.json_encode($parent_remarkAry);
                    
                }
                else
                {
                    $rprofit = $rprofit + $netprofitA;
                    $rprofit = $rprofit - $netprofitA;
                    $this->makeConvertaFund ($from, $netprofitA, $bookA, $batch_id);
                    
                    $this-> makeMT5TransferBFund ($from, $bookB, $netprofitA, $bookA, $batch_id);

                    $parent_remark = 'Account A profit - Less than deposit capital (Account: '.$bookA.'; Trade: '.$order_idA.')';
                    $parent_remarkAry = array('account'=>$bookA, 'trade'=>$order_idA);
                    $parent_remark_display = 'TH00032/'.json_encode($parent_remarkAry);
                    
                }
                
                DB::table('Orders_Fund')->where('bookAorderid', $order_idA)
                ->update([
                         'profitA' => $profitA,
                         'swapA' => $swapA,
                         'closetime' => $closetimeA,
                         'status' => 'P',
                         'closeprice' => $closepriceA,
                         'stoploss' => $stoplossA,
                         'takeprofit' => $takeprofitA,
                         'wwallet' => $wwalletx,
                         'mwallet' => $mwalletx,
                         'swallet' => $swalletx,
                         'rwallet' => $rwalletx,
                         'updated_at' => \Carbon\Carbon::now()
                         ]);
                
                DB::table('Orders_Fund')->where('status', '=', 'C')->where('bookA', $bookA)
                ->update([
                         'status' => 'P',
                         'updated_at' => \Carbon\Carbon::now()
                         ]);
                
                if($system_manage == 'U' && $netprofitA > $balanceB)
                {
                    DB::table('Member_Account_Fund')
                    ->where('bookA', $bookA)
                    ->update(['status' => '0','balanceB' => '0','winA' => '1','system_manage' => 'N','unsubscribe_date' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'newfundrequest' => 'N']);
                    
                    $newpassword = 'XSw21qaz';
                    
                    $moveoutAPI = $memberRepo->AccountMoveoutAPI($bookA);
                    $updatepass = $memberRepo->UpdateMasterPasswordAPI($bookA,$newpassword);
                    $rprofit = $rprofit - ($balanceB*3);

                }
                else
                {
                    DB::table('Member_Account_Fund')
                    ->where('bookA', $bookA)
                    ->update(['status' => '0','winB' => '0','lockdate' => null, 'updated_at' => \Carbon\Carbon::now()]);
                }
                
            }
            else
            {
                $status = 'B';
                
                $slippage = 0;
                $bbalance = DB::table('Member_Wallet')->where('member_id', '=', $from->id)->first();
                $bwallet_balance = $bbalance->b_wallet;
                $wwallet_balance = $bbalance->w_wallet;
                // $wwallet_newbalance = $wwallet_balance;
                
                
                $blost = abs($profitA);
                
                
                if($blost > $max_insurance)
                {
                    $bwallet = $max_insurance;
                    $slippage = $blost - $max_insurance;
                }
                else
                    $bwallet = $blost;
                
                
                if($bwallet <> 0)
                {
                    $bwalletx = $bwallet;
                    $rprofit = $rprofit + $bwallet;
                    $wwallet_newbalance = $wwallet_balance + $bwallet;
                    $bwallet_newbalance = $bwallet_balance + $bwallet;
                    
                    DB::table('Member_Wallet')
                    ->where('member_id', $from->id)
                    ->update(['w_wallet' => $wwallet_newbalance]);
                    
                    $remark = 'Credited '.$bwallet.' from MT5 for Account '.$bookB;

                    $remarkAry = array('amount'=>$bwallet, 'account'=>$bookB);
                    $remark_display = 'TH00017/'.json_encode($remarkAry);
                    
                    DB::table('Member_Wallet_Statement_Fund')->insert([
                                                                      'member_id' => $from->id,
                                                                      'username' => $from->username,
                                                                      'b_amount' => $bwallet,
                                                                      'action_type' => 'MT5 to B-Wallet',
                                                                      'wallet_type' => 'BW',
                                                                      'transaction_type' => 'C',
                                                                      'balance' =>  $bwallet_newbalance,
                                                                      'remark' => $remark,
                                                                      'bookA' => $bookA,
                                                                      'created_at' => \Carbon\Carbon::now(),
                                                                      'updated_at' => \Carbon\Carbon::now(),
                                                                      'remark_display' => $remark_display,
                                                                      'batch_id' => $batch_id
                                                                      ]);
                    sleep(1);
                    
                    $remark1 = 'Debited '.$bwallet.' for selling B wallet';
                    $remark2 = 'Credited '.$bwallet.' for selling B wallet';

                    $remark1Ary = array('amount'=>$bwallet);
                    $remark2Ary = array('amount'=>$bwallet);
                    $remark_display1 = 'TH00018/'.json_encode($remark1Ary);
                    $remark_display2 = 'TH00029/'.json_encode($remark2Ary);
                    
                    $bwallet_newbalance = $bwallet_newbalance - $bwallet;
                    
                    DB::table('Member_Wallet_Statement_Fund')->insert([
                                                                      'member_id' => $from->id,
                                                                      'username' => $from->username,
                                                                      'b_amount' => $bwallet,
                                                                      'action_type' => 'Sell B-Wallet',
                                                                      'wallet_type' => 'BW',
                                                                      'transaction_type' => 'D',
                                                                      'balance' =>  $bwallet_newbalance,
                                                                      'remark' => $remark1,
                                                                      'bookA' => $bookA,
                                                                      'created_at' => \Carbon\Carbon::now(),
                                                                      'updated_at' => \Carbon\Carbon::now(),
                                                                      'remark_display' => $remark_display1,
                                                                      'batch_id' => $batch_id
                                                                      ]);
                    
                    sleep(1);
                    
                    DB::table('Member_Wallet_Statement_Fund')->insert([
                                                                      'member_id' => $from->id,
                                                                      'username' => $from->username,
                                                                      'w_amount' => $bwallet,
                                                                      'action_type' => 'Sell B-Wallet',
                                                                      'wallet_type' => 'W',
                                                                      'transaction_type' => 'C',
                                                                      'balance' =>  $wwallet_newbalance,
                                                                      'remark' => $remark2,
                                                                      'bookA' => $bookA,
                                                                      'created_at' => \Carbon\Carbon::now(),
                                                                      'updated_at' => \Carbon\Carbon::now(),
                                                                      'remark_display' => $remark_display2,
                                                                      'batch_id' => $batch_id
                                                                      ]);
                }
                
                
                if($swapA <> 0)
                {
                    if($swapA > 0)
                    {
                        $remark = 'Credited '.$swapA.' swap from trade '.$order_idA;

                        $remarkAry = array('amount'=>$swapA,'trade'=>$order_idA);
                        $remark_display = 'TH00027/'.json_encode($remarkAry);

                        $transaction_type = 'C';
                        $title = 'MT5 to W-Wallet';
                        
                        $user_swap = $swapA;
                    }
                    else
                    {
                        $max_swap = ($balanceB * 3) * 2 / 100;
                        
                        $transaction_type = 'D';
                        
                        $title = 'MT5 to W-Wallet';
                        
                        if(abs($swapA) > $max_swap)
                        {
                            $user_swap = -($max_swap);
                            $r_swap = abs($swapA) - $max_swap;
                            
                            $remark = 'Debited '.$user_swap.' swap (Swap : '.$swapA.', R Wallet absorbed : '.$r_swap.') from trade '.$order_idA.' for MT5 account '.$bookA;
                            
                            $remarkAry = array('amount'=>$user_swap,'amountSwap'=>$swapA,'amountAbsorbed'=>$r_swap,'trade'=>$order_idA,'account'=>$bookA);
                            $remark_display = 'TH00041/'.json_encode($remarkAry);

                            DB::table('Rwallet')->decrement('r_amount',$r_swap);
                            
                            DB::table('Rwallet_log')
                            ->insert([
                                     'member_id' => $from->id,
                                     'orderid' => $order_idA,
                                     'bookA' => $bookA,
                                     'r_amount' => -($r_swap),
                                     'type' => 'SW',
                                     'created_at' => \Carbon\Carbon::now()
                                     ]);
                        }
                        else
                        {
                            $user_swap = $swapA;
                            
                            $remark = 'Debited '.$user_swap.' swap from trade '.$order_idA.' for MT5 account '.$bookA;
                            
                            $remarkAry = array('amount'=>$user_swap,'trade'=>$order_idA,'account'=>$bookA);
                            $remark_display = 'TH00028/'.json_encode($remarkAry);
                        }
                        
                        
                        
                        
                    }
                    
                    $rprofit = $rprofit + $user_swap;
                    $wwallet_newbalance = $wwallet_newbalance + $user_swap;
                    
                    DB::table('Member_Wallet')
                    ->where('member_id', $from->id)
                    ->update(['w_wallet' => $wwallet_newbalance]);
                    
                    DB::table('Member_Wallet_Statement_Fund')->insert([
                                                                      'member_id' => $from->id,
                                                                      'username' => $from->username,
                                                                      'w_amount' => abs($user_swap),
                                                                      'action_type' => $title,
                                                                      'wallet_type' => 'W',
                                                                      'transaction_type' => $transaction_type,
                                                                      'balance' =>  $wwallet_newbalance,
                                                                      'remark' => $remark,
                                                                      'bookA' => $bookA,
                                                                      'created_at' => \Carbon\Carbon::now(),
                                                                      'updated_at' => \Carbon\Carbon::now(),
                                                                      'remark_display' => $remark_display,
                                                                      'batch_id' => $batch_id
                                                                      ]);
                }
                
                if($slippage > 0)
                {
                    if($slippage > $max_slipage_negative)
                    {
                        $todeductfromS = round((($max_slipage_negative * 1) / 3),2);
                        $todeductfromS2 = round((($slippage - $max_slipage_negative * 1) / 3),2);
                        $todeductfromR = round(((($slippage - $max_slipage_negative) * 1) / 3),2);
                        
                        $sbalance = DB::table('Swallet')->first();
                        $swallet_balance = $sbalance->s_amount;
                        
                        $swalletx = $todeductfromS2;
                        
                        DB::table('Swallet')->decrement('s_amount',$swalletx);
                        
                        DB::table('Swallet_log')
                        ->insert([
                                 'member_id' => $from->id,
                                 'orderid' => $order_idA,
                                 's_amount' => -($swalletx),
                                 'created_at' => \Carbon\Carbon::now()
                                 ]);
                        
                        $rbalance = DB::table('Rwallet')->first();
                        $rwallet_balance = $rbalance->r_amount;
                        
                        $rwalletx = -($todeductfromR);
                        
                        DB::table('Rwallet')->decrement('r_amount',$rwalletx);
                        
                        DB::table('Rwallet_log')
                        ->insert([
                                 'member_id' => $from->id,
                                 'orderid' => $order_idA,
                                 'bookA' => $bookA,
                                 'type' => 'SPN',
                                 'r_amount' => $rwalletx,
                                 'created_at' => \Carbon\Carbon::now()
                                 ]);
                    }
                    else
                    {
                        $todeductfromS = round((($slippage * 1) / 3),2);
                    }
                    
                    $sbalance = DB::table('Swallet')->first();
                    $swallet_balance = $sbalance->s_amount;
      
                    $swalletx = $todeductfromS;
                    
                    DB::table('Swallet')->decrement('s_amount',$swalletx);
                    
                    DB::table('Swallet_log')
                    ->insert([
                             'member_id' => $from->id,
                             'orderid' => $order_idA,
                             's_amount' => -($swalletx),
                             'created_at' => \Carbon\Carbon::now()
                             ]);
                    //}
                    
                    
                    if($slippage > $max_slipage_negative)
                    {
                        $todeductfromW = $max_slipage_negative - $swalletx;
                        $rprofit = $rprofit - $todeductfromW;
                        $wwallet_newbalance = $wwallet_newbalance - $todeductfromW;
                        
                        DB::table('Member_Wallet')
                        ->where('member_id', $from->id)
                        ->decrement('w_wallet' , $todeductfromW);
                        
                        $remark = 'Debited 2/3 slippage '.$todeductfromW.' from trade '.$order_idA.' for transfer to MT5 account '.$bookA;
                        
                        $remarkAry = array('amount'=>$todeductfromW,'trade'=>$order_idA,'account'=>$bookA);
                        $remark_display = 'TH00042/'.json_encode($remarkAry);

                        DB::table('Member_Wallet_Statement_Fund')->insert([
                                                                          'member_id' => $from->id,
                                                                          'username' => $from->username,
                                                                          'w_amount' => $todeductfromW,
                                                                          'action_type' => 'Transfer to MT5',
                                                                          'wallet_type' => 'W',
                                                                          'transaction_type' => 'D',
                                                                          'balance' =>  $wwallet_newbalance,
                                                                          'remark' => $remark,
                                                                          'bookA' => $bookA,
                                                                          'created_at' => \Carbon\Carbon::now(),
                                                                          'updated_at' => \Carbon\Carbon::now(),
                                                                          'remark_display' => $remark_display,
                                                                          'batch_id' => $batch_id
                                                                          ]);
                        
                        $todeductfromW = $todeductfromR;
                        $rprofit = $rprofit - $todeductfromW;
                        $wwallet_newbalance = $wwallet_newbalance - $todeductfromW;
                        
                        DB::table('Member_Wallet')
                        ->where('member_id', $from->id)
                        ->decrement('w_wallet' , $todeductfromW);
                        
                        $remark = 'Debited 1/3 slippage '.$todeductfromW.' beyond 20 pips, from trade '.$order_idA.' for transfer to MT5 account '.$bookA;
                        
                        $remarkAry = array('amount'=>$todeductfromW,'trade'=>$order_idA,'account'=>$bookA);
                        $remark_display = 'TH00043/'.json_encode($remarkAry);

                        DB::table('Member_Wallet_Statement_Fund')->insert([
                                                                          'member_id' => $from->id,
                                                                          'username' => $from->username,
                                                                          'w_amount' => $todeductfromW,
                                                                          'action_type' => 'Transfer to MT5',
                                                                          'wallet_type' => 'W',
                                                                          'transaction_type' => 'D',
                                                                          'balance' =>  $wwallet_newbalance,
                                                                          'remark' => $remark,
                                                                          'bookA' => $bookA,
                                                                          'created_at' => \Carbon\Carbon::now(),
                                                                          'updated_at' => \Carbon\Carbon::now(),
                                                                          'remark_display' => $remark_display,
                                                                          'batch_id' => $batch_id
                                                                          ]);
                    }
                    else
                    {
                        $todeductfromW = $slippage - $swalletx;
                        $rprofit = $rprofit - $todeductfromW;
                        $wwallet_newbalance = $wwallet_newbalance - $todeductfromW;
                        
                        DB::table('Member_Wallet')
                        ->where('member_id', $from->id)
                        ->decrement('w_wallet' , $todeductfromW);
                        
                        $remark = 'Debited 2/3 slippage '.$todeductfromW.' from trade '.$order_idA.' for transfer to MT5 account '.$bookA;
                        
                        $remarkAry = array('amount'=>$todeductfromW,'trade'=>$order_idA,'account'=>$bookA);
                        $remark_display = 'TH00042/'.json_encode($remarkAry);

                        DB::table('Member_Wallet_Statement_Fund')->insert([
                                                                          'member_id' => $from->id,
                                                                          'username' => $from->username,
                                                                          'w_amount' => $todeductfromW,
                                                                          'action_type' => 'Transfer to MT5',
                                                                          'wallet_type' => 'W',
                                                                          'transaction_type' => 'D',
                                                                          'balance' =>  $wwallet_newbalance,
                                                                          'remark' => $remark,
                                                                          'bookA' => $bookA,
                                                                          'created_at' => \Carbon\Carbon::now(),
                                                                          'updated_at' => \Carbon\Carbon::now(),
                                                                          'remark_display' => $remark_display,
                                                                          'batch_id' => $batch_id
                                                                          ]);
                    }
                }
                
                $title = 'Transfer to MT5';
                $amounttotransfer = abs($netprofitA+$swapA);
                //$amounttotransferdeduct = abs($profitA);
                
                $wwallet_newbalance = $wwallet_newbalance - $bwalletx;
                
                $remark = 'Debited '.$bwalletx.' for transfer to MT5 account '.$bookA;

                $remarkAry = array('amount'=>$bwalletx,'account'=>$bookA);
                $remark_display = 'TH00008/'.json_encode($remarkAry);
                
                DB::table('Member_Wallet_Statement_Fund')->insert([
                                                                  'member_id' => $from->id,
                                                                  'username' => $from->username,
                                                                  'w_amount' => $bwalletx,
                                                                  'action_type' => $title,
                                                                  'wallet_type' => 'W',
                                                                  'transaction_type' => 'D',
                                                                  'balance' =>  $wwallet_newbalance,
                                                                  'remark' => $remark,
                                                                  'bookA' => $bookA,
                                                                  'created_at' => \Carbon\Carbon::now(),
                                                                  'updated_at' => \Carbon\Carbon::now(),
                                                                  'remark_display' => $remark_display,
                                                                  'batch_id' => $batch_id
                                                                  ]);
                
                DB::table('Orders_Fund')->where('bookAorderid', $order_idA)
                ->update([
                         'profitA' => $profitA,
                         'swapA' => $swapA,
                         'closetime' => $closetimeA,
                         'status' => 'P',
                         'closeprice' => $closepriceA,
                         'stoploss' => $stoplossA,
                         'takeprofit' => $takeprofitA,
                         'wwallet' => $amounttotransfer,
                         'bwallet' => $bwalletx,
                         'swallet' => $swalletx,
                         'updated_at' => \Carbon\Carbon::now()
                         ]);
                
                DB::table('Orders_Fund')->where('status', '=', 'C')->where('bookA', $bookA)
                ->update([
                         'status' => 'P',
                         'updated_at' => \Carbon\Carbon::now()
                         ]);
                
                DB::table('Member_Account_Fund')
                ->where('bookA', $bookA)
                ->update(['status' => '0','winB' => '0','lockdate' => null, 'updated_at' => \Carbon\Carbon::now()]);
                
                $rprofit = $rprofit - $bwalletx;
                DB::table('Member_Wallet')
                ->where('member_id', $from->id)
                ->decrement('w_wallet' , $bwalletx);
                
                $updatebalA = $memberRepo->BalanceAPI($bookA,$amounttotransfer, 'true', $title);

                $parent_remark = 'Account B profit (Account: '.$bookA.'; Trade: '.$order_idA.')';
                $parent_remarkAry = array('account'=>$bookA, 'trade'=>$order_idA);
                $parent_remark_display = 'TH00033/'.json_encode($parent_remarkAry);
            }
            
            if($rprofit > 0) DB::table('Member_Wallet')->where('member_id', $from->id)->increment('wp_wallet' , $rprofit);

            $this-> updateParentBatchID ($batch_id, $from->id, $parent_remark, $parent_remark_display, $bookA, $rprofit);
            return $status;
        }
        
        public function makeBuy ($from, $amount) {
            
            $walletFrom = $from->wallet;
            $amount = (float) $amount;
            $current_amount = 0;
            $initial_amount = $amount;
            
            
            
            if ($walletFrom->cash_point < $amount) {
                throw new \Exception(\Lang::get('error.cashPointNotEnough'), 1);
                return false;
            }
            
            $buyid = DB::table('trade')->insertGetId(
                                                     [
                                                     'member_id' => $from->id,
                                                     'trade_type' => 'B',
                                                     'amount' => $amount,
                                                     'status' => 'Open',
                                                     'created_at' => \Carbon\Carbon::now(),
                                                     'updated_at' => \Carbon\Carbon::now()
                                                     ]
                                                     );
            
            $remark1 = 'Debited '.$amount.' for buying R wallet';

            $remark1Ary = array('amount'=>$amount);
            $remark_display1 = 'TH00040/'.json_encode($remark1Ary);
            
            $walletFrom->cash_point -= $amount;
            $walletFrom->save();
            
            $title = 'Buy R Wallet';
            $title2 = 'Sell R Wallet';
            
            
            $this->saveWalletStatement($from->id, '', $amount ,$title,'C', 'D',$walletFrom->cash_point,$remark1,$remark_display1,'');
            
            
            
            $trades = DB::table('trade')->where('trade_type', '=', 'S')->where('status', '=', 'Open')->lockForUpdate()->orderBy('created_at', 'asc')->get();
            
            if ($trades->count()) {
                foreach($trades as $trade)
                {
                    $amount_deduct = $trade->amount - $trade->amount_complete;
                    
                    
                    
                    if($amount >= $amount_deduct)
                    {
                        $current_amount = $current_amount + $amount_deduct;
                        
                        DB::table('trade')
                        ->where('id', $trade->id)
                        ->update(['amount_complete' => $trade->amount,'status' => 'Done', 'updated_at' => \Carbon\Carbon::now()]);
                        
                        $memberRepo = new MemberRepository;
                        $toMember = $memberRepo->findById($trade->member_id);
                        $walletTo = $toMember->wallet;

                        $remark2 = 'Credited '.$trade->amount.' for selling R wallet';

                        $remark2Ary = array('amount'=>$trade->amount);
                        $remark_display2 = 'TH00044/'.json_encode($remark2Ary);
                        
                        $walletTo->cash_point += $trade->amount;
                        $walletTo->save();
                        
                        $this->saveWalletStatement($trade->member_id, '', $trade->amount ,$title2, 'C', 'C',$walletTo->cash_point,$remark2,$remark_display2,'');
                        
                        
                        DB::table('trade')
                        ->where('id', $buyid)
                        ->update(['amount_complete' =>  $current_amount, 'updated_at' => \Carbon\Carbon::now()]);
                        
                        $amount = $amount - $amount_deduct;
                    }
                    else
                    {
                        if($amount == 0)
                        {
                            break;
                        }
                        
                        $current_amount = $current_amount + $amount;
                        
                        DB::table('trade')
                        ->where('id', $trade->id)
                        ->update(['amount_complete' => $amount + $trade->amount_complete, 'updated_at' => \Carbon\Carbon::now()]);
                        
                        DB::table('trade')
                        ->where('id', $buyid)
                        ->update(['amount_complete' => $initial_amount,'status' => 'Done', 'updated_at' => \Carbon\Carbon::now()]);
                        
                        $remark2 = 'Credited '.$initial_amount.' for buying R wallet';

                        $remark2Ary = array('amount'=>$initial_amount);
                        $remark_display2 = 'TH00039/'.json_encode($remark2Ary);

                        $walletFrom->register_point += $initial_amount;
                        $walletFrom->save();
                        
                        $this->saveWalletStatement($from->id, '', $initial_amount ,$title, 'R', 'C',$walletFrom->register_point,$remark2,$remark_display2,'');
                        
                        $amount = 0;
                        
                        break;
                        
                    }
                }
                
                
                
            }
            
            
            
            
            
            return true;
        }
        
        public function makeBuyPriority ($from, $amount) {
            
            $walletFrom = $from->wallet;
            $initial_amount = (float) $amount;
            $amount = (float) $amount;
            $current_amount = 0;
            $initial_amount = $amount;
            
            $title = 'Buy R Wallet';
            $title2 = 'Sell R Wallet';
            
            $remark1 = 'Debited '.$amount.' for buying R wallet';

            $remark1Ary = array('amount'=>$amount);
            $remark_display1 = 'TH00040/'.json_encode($remark1Ary);
            
            $trades = DB::table('trade')->where('trade_type', '=', 'S')->where('status', '=', 'Open')->lockForUpdate()->orderBy('created_at', 'asc')->get();
            
            if (count($trades) > 0) {
                foreach($trades as $trade)
                {
                    $amount_deduct = $trade->amount - $trade->amount_complete;
                    
                    if($amount >= $amount_deduct)
                    {
                        $current_amount = $current_amount + $amount_deduct;
                        
                        DB::table('trade')
                        ->where('id', $trade->id)
                        ->update(['amount_complete' => $trade->amount,'status' => 'Done', 'updated_at' => \Carbon\Carbon::now()]);
                        
                        $memberRepo = new MemberRepository;
                        $toMember = $memberRepo->findById($trade->member_id);
                        $walletTo = $toMember->wallet;

                        $remark1 = 'Credited '.$trade->amount.' for selling R wallet';

                        $remark1Ary = array('amount'=>$trade->amount);
                        $remark_display1 = 'TH00044/'.json_encode($remark1Ary);
                        
                        $walletTo->cash_point += $trade->amount;
                        $walletTo->save();
                        
                        $this->saveWalletStatement($trade->member_id, '', $trade->amount ,$title2, 'C', 'C',$walletTo->cash_point,$remark1,$remark_display1,'');
                        
                        
                        $amount = $amount - $amount_deduct;
                    }
                    else
                    {
                        if($amount == 0)
                        {
                            break;
                        }
                        
                        $current_amount = $current_amount + $amount;
                        
                        DB::table('trade')
                        ->where('id', $trade->id)
                        ->update(['amount_complete' => $amount + $trade->amount_complete, 'updated_at' => \Carbon\Carbon::now()]);
                        
                        
                        
                        $amount = 0;
                        
                        break;
                        
                    }
                }
                
                
                
            }
            
            if($amount <> 0)
            {
                //Deduct from admin
                
                
                
                $admin_wallet = DB::table('Member_Wallet')->where('member_id', '=', 1)->first();
                
                $registeramount = $admin_wallet->register_point - $amount;
                
                $cashamount =  $admin_wallet->cash_point + $amount;
                
                DB::table('Member_Wallet')
                ->where('member_id', 1)
                ->update(['register_point' => $registeramount,'cash_point' => $cashamount, 'updated_at' => \Carbon\Carbon::now()]);
            }
            
            
            
            return true;
        }
        
        public function makeSell ($from, $amount) {
            
            $walletFrom = $from->wallet;
            $amount = (float) $amount;
            $current_amount = 0;
            
            
            if ($walletFrom->register_point < $amount) {
                throw new \Exception(\Lang::get('error.registerPointNotEnough'), 1);
                return false;
            }
            
            $sellid = DB::table('trade')->insertGetId(
                                                      [
                                                      'member_id' => $from->id,
                                                      'trade_type' => 'S',
                                                      'amount' => $amount,
                                                      'status' => 'Open',
                                                      'created_at' => \Carbon\Carbon::now(),
                                                      'updated_at' => \Carbon\Carbon::now()
                                                      ]
                                                      );
            
            $remark1 = 'Debited '.$amount.' for selling R wallet';

            $remark1Ary = array('amount'=>$amount);
            $remark_display1 = 'TH00045/'.json_encode($remark1Ary);
            
            $walletFrom->register_point -= $amount;
            $walletFrom->save();
            
            $title = 'Sell R Wallet';
            $title2 = 'Buy R Wallet';
            
            
            $this->saveWalletStatement($from->id, '', $amount ,$title,'R', 'D',$walletFrom->register_point,$remark1,$remark_display1,'');
            
            
            
            $trades = DB::table('trade')->where('trade_type', '=', 'B')->where('status', '=', 'Open')->lockForUpdate()->orderBy('created_at', 'asc')->get();
            
            
            if (count($trades)>0) {
                foreach($trades as $trade)
                {
                    $amount_deduct = $trade->amount - $trade->amount_complete;
                    
                    
                    if($amount >= $amount_deduct)
                    {
                        $current_amount = $current_amount + $amount_deduct;
                        
                        DB::table('trade')
                        ->where('id', $trade->id)
                        ->update(['amount_complete' => $trade->amount,'status' => 'Done', 'updated_at' => \Carbon\Carbon::now()]);
                        
                        $memberRepo = new MemberRepository;
                        $toMember = $memberRepo->findById($trade->member_id);
                        $walletTo = $toMember->wallet;

                        $remark2 = 'Credited '.$trade->amount.' for buying R wallet';

                        $remark2Ary = array('amount'=>$trade->amount);
                        $remark_display2 = 'TH00039/'.json_encode($remark2Ary);
                        
                        $walletTo->register_point += $trade->amount;
                        $walletTo->save();
                        
                        $this->saveWalletStatement($trade->member_id, '', $trade->amount ,$title2, 'R', 'C',$walletTo->register_point,$remark2,$remark_display2,'');
                        
                        
                        DB::table('trade')
                        ->where('id', $sellid)
                        ->update(['amount_complete' =>  $current_amount, 'updated_at' => \Carbon\Carbon::now()]);
                        
                        $amount = $amount - $amount_deduct;
                    }
                    else
                    {
                        $current_amount = $current_amount + $amount;
                        
                        DB::table('trade')
                        ->where('id', $trade->id)
                        ->update(['amount_complete' => $trade->amount_complete+ $amount, 'updated_at' => \Carbon\Carbon::now()]);
                        
                        DB::table('trade')
                        ->where('id', $sellid)
                        ->update(['amount_complete' => $current_amount,'status' => 'Done', 'updated_at' => \Carbon\Carbon::now()]);
                        
                        $remark2 = 'Credited '.$current_amount.' for selling R wallet';

                        $remark2Ary = array('amount'=>$trade->amount);
                        $remark_display2 = 'TH00044/'.json_encode($remark2Ary);

                        $walletFrom->cash_point += $current_amount;
                        $walletFrom->save();
                        
                        $this->saveWalletStatement($from->id, '', $current_amount ,$title, 'C', 'C',$walletFrom->cash_point,$remark2,$remark_display2,'');
                        
                        $amount = 0;
                        break;
                        
                    }
                }
                
                
                
                
            }
            
            
            
            
            
            return true;
        }
        
        public function makeConvertIToC ($member, $amount) {
            
            $amount = (float) $amount;

            $remark = 'Convert '.$amount.' from income wallet to cash wallet';

            $remarkAry = array('amount'=>$amount);
            $remark_display = 'TH00005/'.json_encode($remarkAry);

            $walletQuery = DB::table('Member_Wallet')->where('member_id', $member->id);
            $walletQuery->decrement('promotion_point', $amount);
            $incomeBalance = $walletQuery->first(['promotion_point'])->promotion_point;
            $this->saveWalletStatement($member->id, $member->username, $amount ,'Transfer to Income Wallet','B','D',$incomeBalance,$remark,$remark_display,'');
            
            if($incomeBalance >= 0){
                $member->wallet->cash_point += $amount;
                $member->wallet->save();
                
                $this->saveWalletStatement($member->id, $member->username, $amount ,'Transfer from Income Wallet','C','C',$member->wallet->cash_point,$remark,$remark_display,'');
            }
            
            return true;
        }

        public function makeConvertIToT ($member, $amount) {
            
            $amount = (float) $amount;

            $remark = 'Convert '.$amount.' from income wallet to t wallet';

            $remarkAry = array('amount'=>$amount);
            $remark_display = 'TH00066/'.json_encode($remarkAry);

            $walletQuery = DB::table('Member_Wallet')->where('member_id', $member->id);
            $walletQuery->decrement('promotion_point', $amount);
            $incomeBalance = $walletQuery->first(['promotion_point'])->promotion_point;
            $this->saveWalletStatement($member->id, $member->username, $amount ,'Transfer to Income Wallet','B','D',$incomeBalance,$remark,$remark_display,'');
            
            if($incomeBalance >= 0){
                $member->wallet->t_wallet += $amount;
                $member->wallet->save();
            
                $this->saveWalletStatement($member->id, $member->username, $amount ,'Transfer from Income Wallet','T','C',$member->wallet->t_wallet,$remark,$remark_display,'');
            }
            
            return true;
        }

        public function makeConvertTToC ($member, $amount) {
            
            $amount = (float) $amount;

            $remark = 'Convert '.$amount.' from t wallet to cash wallet';

            $remarkAry = array('amount'=>$amount);
            $remark_display = 'TH00067/'.json_encode($remarkAry);

            $walletQuery = DB::table('Member_Wallet')->where('member_id', $member->id);
            $walletQuery->decrement('t_wallet', $amount);
            $tBalance = $walletQuery->first(['t_wallet'])->t_wallet;
            $this->saveWalletStatement($member->id, $member->username, $amount ,'Transfer to T Wallet','T','D',$tBalance,$remark,$remark_display,'');

            if($tBalance >= 0){
                $member->wallet->cash_point += $amount;
                $member->wallet->save();
    
                $this->saveWalletStatement($member->id, $member->username, $amount ,'Transfer from T Wallet','C','C',$member->wallet->cash_point,$remark,$remark_display,'');
            }
            
            return true;
        }

        public function makeConvertWToC ($member, $amount) {
            
            $amount = (float) $amount;
            $amount_wp = (float) $member->wallet->wp_wallet;

            if($amount_wp > $amount) $amount_wp = $amount;

            $remark = 'Convert '.$amount.' from w wallet to cash wallet';

            $remarkAry = array('amount'=>$amount);
            $remark_display = 'TH00071/'.json_encode($remarkAry);

            $walletQuery = DB::table('Member_Wallet')->where('member_id', $member->id);
            $walletQuery->decrement('wp_wallet', $amount_wp);
            $walletQuery->decrement('w_wallet', $amount);
            $wBalance = $walletQuery->first(['w_wallet'])->w_wallet;

            \DB::table('Member_Wallet_Statement_Fund')->insert([
                                               'member_id' => $member->id,
                                               'username' => $member->username,
                                               'w_amount' => $amount,
                                               'wp_amount' => $amount_wp,
                                               'action_type' => 'Transfer to W Wallet',
                                               'wallet_type' => 'W',
                                               'transaction_type' => 'D',
                                               'balance' =>  $wBalance,
                                               'remark' => $remark,
                                               'created_at' => \Carbon\Carbon::now(),
                                               'updated_at' => \Carbon\Carbon::now(),
                                               'remark_display' => $remark_display,
                                               'batch_id' => 0
                                               ]);

            if($wBalance >= 0){
                $member->wallet->cash_point += $amount;
                $member->wallet->save();

                $this->saveWalletStatement($member->id, $member->username, $amount ,'Transfer from W Wallet','C','C',$member->wallet->cash_point,$remark,$remark_display,'');
            }
            return true;
        }

        public function makeConvertWToT ($member, $amount) {
            
            $amount = (float) $amount;

            $remark = 'Convert '.$amount.' from w wallet to t wallet';

            $remarkAry = array('amount'=>$amount);
            $remark_display = 'TH00070/'.json_encode($remarkAry);

            $walletQuery = DB::table('Member_Wallet')->where('member_id', $member->id);
            $walletQuery->decrement('wp_wallet', $amount);
            $walletQuery->decrement('w_wallet', $amount);
            $wpBalance = $walletQuery->first(['wp_wallet'])->wp_wallet;
            $wBalance = $walletQuery->first(['w_wallet'])->w_wallet;

            \DB::table('Member_Wallet_Statement_Fund')->insert([
                                               'member_id' => $member->id,
                                               'username' => $member->username,
                                               'w_amount' => $amount,
                                               'wp_amount' => $amount,
                                               'action_type' => 'Transfer from W Wallet',
                                               'wallet_type' => 'W',
                                               'transaction_type' => 'D',
                                               'balance' =>  $wBalance,
                                               'remark' => $remark,
                                               'created_at' => \Carbon\Carbon::now(),
                                               'updated_at' => \Carbon\Carbon::now(),
                                               'remark_display' => $remark_display,
                                               'batch_id' => 0
                                               ]);
            
            if($wpBalance >= 0 && $wBalance >=0){
                $member->wallet->t_wallet += $amount;
                $member->wallet->save();
                
                $this->saveWalletStatement($member->id, $member->username, $amount ,'Transfer from W Wallet','T','C',$member->wallet->t_wallet,$remark,$remark_display,'');
            }
            
            return true;
        }
        
        public function makeConvertW ($member, $amount) {
            
            $amount = (float) $amount;
            
            // if ($member->wallet->w_wallet < $amount || $amount < 0) {
            //     throw new \Exception(\Lang::get('error.wPointNotEnough'), 1);
            //     return false;
            // }
            
            $member->wallet->w_wallet -= $amount;
            $member->wallet->cash_point += $amount;
            $member->wallet->save();
            
            $remark = 'Convert '.$amount.' from w wallet to cash wallet';

            $remarkAry = array('amount'=>$amount);
            $remark_display = 'TH00006/'.json_encode($remarkAry);
            
            $this->saveWalletStatement($member->id, $member->username, $amount ,'Transfer from W Wallet','C','C',$member->wallet->cash_point,$remark,$remark_display,'');
            
            $this->saveWalletStatement2($member->id, $member->username, $amount ,'Transfer to W Wallet','W','D',$member->wallet->w_wallet,$remark,$remark_display,'',0);
            
            return true;
        }
        
        public function makeConverta ($member, $amount, $account) {
            $amount = (float) $amount;
            $memberRepo = new MemberRepository;
            
            $remark = 'Convert '.$amount.' from account A '.$account.' to cash wallet';

            $remarkAry = array('amount'=>$amount,'account'=>$account);
            $remark_display = 'TH00003/'.json_encode($remarkAry);
            
            $updatebal = $memberRepo->BalanceAPI($account,$amount, 'false', 'Transfer from account A');
            
            if($updatebal->status == 0)
            {
                // $member->wallet->promotion_point -= $amount;
                $member->wallet->cash_point += $amount;
                $member->wallet->save();
                
                
                
                $this->saveWalletStatement($member->id, $member->username, $amount ,'Transfer from account A'.$account,'C','C',$member->wallet->cash_point,$remark,$remark_display,'');
                
            }
            
            
            
            // $this->saveWalletStatement($member->id, $member->username, $amount ,'Transfer to Income Wallet','B','D',$member->wallet->promotion_point,$remark);
            
            return true;
        }
        
        public function makeConvertaSelf ($member, $amount, $account) {
            $amount = (float) $amount;
            $memberRepo = new MemberRepository;
            
            $remark = 'Convert '.$amount.' from account A '.$account.' to cash wallet';

            $remarkAry = array('amount'=>$amount,'account'=>$account);
            $remark_display = 'TH00003/'.json_encode($remarkAry);
            
            $updatebal = $memberRepo->BalanceAPI($account,$amount, 'false', 'Transfer from account A');
            
            if($updatebal->status == 0)
            {
                $member->wallet->cash_point += $amount;
                $member->wallet->save();
                $this->saveWalletStatement($member->id, $member->username, $amount ,'Transfer from account A'.$account,'C','C',$member->wallet->cash_point,$remark,$remark_display,'');
            }
            
            return true;
        }
        
        public function changePass ($member, $password2) {
         
            $memberRepo = new MemberRepository;
            
            $accounts = DB::table('Member_Account_Fund')->where('member_id', '=', $member->id)->get();
            
            // $accounts = DB::table('Member_Account_Fund')
            // ->where('member_id', '=', $member->id)
            // ->where(function ($query) {
            //         $query->whereNull('mam_account')
            //         ->orWhere('mam_account', '<>', 601038);
            //         })
            // ->get();
            
            foreach($accounts as $account)
            {
                $member_id = $account->member_id;
                $bookA = $account->bookA;
                //$newpassword = 'XSw21qaz';
              
                //if($account->chgpass == 'N')
               // {
                 //   if($account->system_manage == 'S' || $account->system_manage == 'U')
                 //   {
                        if($updatepass = $memberRepo->UpdateInvestorPasswordAPI($bookA,$password2))
                        {
                            if($updatepass->status == 0)
                            {
                                DB::table('Member_Account_Fund')->where('bookA', $bookA)
                                ->update([
                                         'chgpass' => 'Y',
                                         ]);
                            }
                            else
                                return false;
                            
                        }
                        else
                            return false;
                  //  }
               // }
                
            }
            
            return true;
        }
        
        public function makeConvertaFund ($member, $amount, $account, $batch_id) {
            $amount = (float) $amount;
            $memberRepo = new MemberRepository;
            
            $remark = 'Convert '.$amount.' from account A '.$account.' to W wallet';

            $remarkAry = array('amount'=>$amount,'account'=>$account);
            $remark_display = 'TH00030/'.json_encode($remarkAry);
            
            $updatebal = $memberRepo->BalanceAPI($account,$amount, 'false', 'Transfer from account A');
            
            if($updatebal->status == 0)
            {
                // $member->wallet->promotion_point -= $amount;
                $member->wallet->w_wallet += $amount;
                $member->wallet->save();
                
                
                
                $this->saveWalletStatement2($member->id, $member->username, $amount ,'Transfer from account A'.$account,'W','C',$member->wallet->w_wallet,$remark,$remark_display,$account,$batch_id);
                
            }
            
            
            
            // $this->saveWalletStatement($member->id, $member->username, $amount ,'Transfer to Income Wallet','B','D',$member->wallet->promotion_point,$remark);
            
            return true;
        }
        
        public function saveWalletStatement ($fromMember, $toMember, $amount, $action_type, $wallet_type, $transaction_type, $balance, $remark, $remark_display, $bookA) {
            
            if($wallet_type == 'C')
            {
                $camount = $amount;
                $bamount = 0;
                $ramount = 0;
                $tamount = 0;
            }
            elseif($wallet_type == 'R')
            {
                $camount = 0;
                $bamount = 0;
                $ramount = $amount;
                $tamount = 0;
            }
            elseif($wallet_type == 'B')
            {
                $camount = 0;
                $bamount = $amount;
                $ramount = 0;
                $tamount = 0;
            }
            elseif($wallet_type == 'T')
            {
                $camount = 0;
                $bamount = 0;
                $ramount = 0;
                $tamount = $amount;
            }
            
            \DB::table('Member_Wallet_Statement')->insert([
                                                          'member_id' => $fromMember,
                                                          'username' => $toMember,
                                                          'cash_amount' => $camount,
                                                          'promotion_amount' => $bamount,
                                                          'register_amount' => $ramount,
                                                          't_amount' => $tamount,
                                                          'action_type' => $action_type,
                                                          'wallet_type' => $wallet_type,
                                                          'transaction_type' => $transaction_type,
                                                          'balance' =>  $balance,
                                                          'remark' => $remark,
                                                          'bookA' => $bookA?$bookA:null,
                                                          'created_at' => \Carbon\Carbon::now(),
                                                          'updated_at' => \Carbon\Carbon::now(),
                                                          'remark_display' => $remark_display
                                                          ]);
            
            return true;
        }
        
        public function saveWalletStatement2 ($fromMember, $toMember, $amount, $action_type, $wallet_type, $transaction_type, $balance, $remark, $remark_display, $bookA, $batch_id) {
            
            if($wallet_type == 'C')
            {
                $camount = $amount;
                $bamount = 0;
                $ramount = 0;
                $wamount = 0;
                $wdamount = 0;
                $bwamount = 0;
                $wpamount = 0;
            }
            elseif($wallet_type == 'R')
            {
                $camount = 0;
                $bamount = 0;
                $ramount = $amount;
                $wamount = 0;
                $wdamount = 0;
                $bwamount = 0;
                $wpamount = 0;
            }
            elseif($wallet_type == 'B')
            {
                $camount = 0;
                $bamount = $amount;
                $ramount = 0;
                $wamount = 0;
                $wdamount = 0;
                $bwamount = 0;
                $wpamount = 0;
            }
            elseif($wallet_type == 'W')
            {
                $camount = 0;
                $bamount = 0;
                $ramount = 0;
                $wamount = $amount;
                $wdamount = 0;
                $bwamount = 0;
                $wpamount = 0;
            }
            elseif($wallet_type == 'BW')
            {
                $camount = 0;
                $bamount = 0;
                $ramount = 0;
                $wamount = 0;
                $wdamount = 0;
                $bwamount = $amount;
                $wpamount = 0;
            }
            elseif($wallet_type == 'WD')
            {
                $camount = 0;
                $bamount = 0;
                $ramount = 0;
                $wamount = 0;
                $wdamount = $amount;
                $bwamount = 0;
                $wpamount = 0;
            }
            elseif($wallet_type == 'WP')
            {
                $camount = 0;
                $bamount = 0;
                $ramount = 0;
                $wamount = 0;
                $wdamount = 0;
                $bwamount = 0;
                $wpamount = $amount;
            }
            
            \DB::table('Member_Wallet_Statement_Fund')->insert([
                                                               'member_id' => $fromMember,
                                                               'username' => $toMember,
                                                               'cash_amount' => $camount,
                                                               'promotion_amount' => $bamount,
                                                               'b_amount' => $bwamount,
                                                               'w_amount' => $wamount,
                                                               'wd_amount' => $wdamount,
                                                               'wp_amount' => $wpamount,
                                                               'action_type' => $action_type,
                                                               'wallet_type' => $wallet_type,
                                                               'transaction_type' => $transaction_type,
                                                               'balance' =>  $balance,
                                                               'remark' => $remark,
                                                               'bookA' => $bookA?$bookA:null,
                                                               'created_at' => \Carbon\Carbon::now(),
                                                               'updated_at' => \Carbon\Carbon::now(),
                                                               'remark_display' => $remark_display,
                                                               'batch_id' => $batch_id
                                                               ]);
            
            return true;
        }
        
        
        
        
        
        /**
         * Check if can transfer to target member
         * @param  App\Models\Member $from
         * @param  App\Models\Member $to
         * @return boolean
         */
        public function checkCanTransfer ($from, $to) {
            if ($from->id == $to->id) return true;
            // if ($from->level >= $to->level) return false;
            
            $left = explode(',', $from->left_children);
            $right = explode(',', $from->right_children);
            if (in_array($to->id, $left) || in_array($to->id, $right)) {
                return true;
            }
            
            $left = explode(',', $to->left_children);
            $right = explode(',', $to->right_children);
            
            if (in_array($from->id, $left) || in_array($from->id, $right)) {
                return true;
            }
            
            return false;
        }

        public function updateParentBatchID ($batch_id, $member_id, $remark, $remark_display, $bookA, $rprofit) {
            $amount = 0;

            $statementList = \DB::table('Member_Wallet_Statement_Fund')->where('wallet_type', 'W')->where('batch_id', $batch_id)->orderBy('id','ASC')->get();
            
            foreach ($statementList as $details) {
                if($details->transaction_type == 'C') $amount = $amount + $details->w_amount;
                else $amount = $amount - $details->w_amount;
                $balance = $details->balance;
            }

            if($amount > 0) $transaction_type = 'C';
            else $transaction_type = 'D';

            if($rprofit > 0){
                $parent_batch_id = \DB::table('Member_Wallet_Statement_Fund')->insertGetId([
                                    'member_id' => $member_id,
                                    'action_type' => 'Parent Statement',
                                    'wallet_type' => 'W',
                                    'transaction_type' => $transaction_type,
                                    'w_amount' => $amount,
                                    'wp_amount' => $rprofit,
                                    'balance' => $balance,
                                    'remark' => $remark,
                                    'bookA' => $bookA,
                                    'created_at' => \Carbon\Carbon::now(),
                                    'updated_at' => \Carbon\Carbon::now(),
                                    'remark_display' => $remark_display,
                                    'batch_id' => $batch_id
                                ]);
            }else{
                $parent_batch_id = \DB::table('Member_Wallet_Statement_Fund')->insertGetId([
                                    'member_id' => $member_id,
                                    'action_type' => 'Parent Statement',
                                    'wallet_type' => 'W',
                                    'transaction_type' => $transaction_type,
                                    'w_amount' => $amount,
                                    'balance' => $balance,
                                    'remark' => $remark,
                                    'bookA' => $bookA,
                                    'created_at' => \Carbon\Carbon::now(),
                                    'updated_at' => \Carbon\Carbon::now(),
                                    'remark_display' => $remark_display,
                                    'batch_id' => $batch_id
                                ]);
            }

            foreach ($statementList as $details) {
                \DB::table('Member_Wallet_Statement_Fund')->where('id', $details->id)->update(['parent_batch_id' => $parent_batch_id]);
            }
            
            return false;
        }

        public function getMergeList ($table=false, $memberid) {

            $model = DB::table('Merge_List')->where('member_id', $memberid)->where('status', '<>', 3)->orderBy('id', 'DESC');

            return Datatables::of($model)
            ->editColumn('merge_from', function ($model) {
                        $temp = [];

                        if($model->status == 0){
                            $accounts = \DB::table('Member_Account_Fund')->whereIn('bookA', explode(',', $model->merge_from))->get();
                            foreach ($accounts as $item) {
                                $temp[] = $item->bookA.' - $'.number_format($item->balanceB*3,2);
                            }
                        }else{
                            $from = json_decode($model->merge_from);
                            foreach ($from as $key => $value) {
                                $temp[] = $key.' - $'.number_format($value,2);
                            }
                        }

                        return implode('<br>', $temp);
                    })

            ->editColumn('merge_to', function ($model) {
                        if($model->status == 0){
                            $sumaccounts = \DB::table('Member_Account_Fund')->whereIn('bookA', explode(',', $model->merge_from))->sum('balanceB');
                            $account = \DB::table('Member_Account_Fund')->where('bookA', $model->merge_to)->first();
                            if($sumaccounts > 10000) return '<span style="color:red;">'.$account->bookA.' - $'.number_format($sumaccounts*3,2).'</span>';
                            else return $account->bookA.' - $'.number_format($sumaccounts*3,2);
                        }else{
                            $to = json_decode($model->merge_to);
                            foreach ($to as $key => $value) {
                                $temp = $key.' - $'.number_format($value,2);
                            }
                            return $temp;
                        }
                    })

            ->editColumn('status', function ($model) {
                        if($model->status == 1) return \Lang::get('merge.settled');
                        else if($model->status == 2) return \Lang::get('merge.failed');
                        else if($model->status == 3) return \Lang::get('merge.canceled');
                        else return \Lang::get('merge.new');
                    })

            ->editColumn('remark', function ($model) {
                        if($model->status == 1){
                            $temp = [];
                            $success = json_decode($model->merge_success);
                            foreach ($success as $key => $value) {
                                $temp[] = $key.' - $'.number_format($value,2);
                            }
                            return implode('<br>', $temp);
                        }else if($model->status == 2) return \Lang::get('merge.'.$model->remark);
                        else return '';
                    })

            ->addColumn('edit', function ($model) {
                        if($model->status == 0) return '<a class="btn btn-primary glow_button" href="'.route('transaction.editmergefund', ['lang' => \App::getLocale(), 'id' => $model->id]).'"><i class="fa fa-pencil"></i> '.\Lang::get('merge.edit').'</a>';
                        else return '';
                    })

            ->addColumn('cancel', function ($model) {
                        if($model->status == 0) return '<button class="btn btn-primary glow_button" onclick="cancelMerge('.$model->id.')"><i class="fa fa-close"></i> '.\Lang::get('merge.cancel').'</button>';
                        else return '';
                    })

            ->rawColumns(['merge_from', 'merge_to', 'edit', 'cancel', 'remark'])
            ->make(true);
        }
        
        public function makeMergeFund ($from, $account) {
            
            $accountAry = explode(',', $account);

            if(count($accountAry) < 2){
                throw new \Exception(\Lang::get('transfer.mergetitle'),1);
                return false;
            }

            $books = DB::table('Member_Account_Fund')->whereIn('bookA', $accountAry)->get();
            $i = 0;
            $totalCheck = 0;

            foreach ($books as $item) {
                
                if($item->balanceB == 0 || $item->system_manage <> 'N' || $item->status <> 0){
                    throw new \Exception("Does not qualified merge's requirement",1);
                    return false;
                }

                $totalCheck = $totalCheck + $item->balanceB;
                if($totalCheck > 10000){
                    throw new \Exception(\Lang::get('error.transferFund'),1);
                    return false;
                }

                if($i == 0) $mainBook = $item;
                else $subBook[] = $item;
                $i++;
            }

            $memberRepo = new MemberRepository;

            $totalSubBalance = 0;
            foreach ($subBook as $data) {
                $bookABal = 0;
                $balance = $memberRepo->AccountAPI($data->bookA);
                $bookABal = $balance->balance;
                if($bookABal > 0){
                    $updatebal = $memberRepo->BalanceAPI($data->bookA, $bookABal, 'false', 'Transfer from account A');
                    if($updatebal->status == 0){
                        $totalSubBalance = $totalSubBalance + $bookABal;
                        DB::table('Member_Account_Fund')->where('bookA', $data->bookA)->update(['balanceB' => '0.00', 'updated_at' => \Carbon\Carbon::now()]);
                    }
                }
            }
            
            if($totalSubBalance > 0){
                $updatebalA = $memberRepo->BalanceAPI($mainBook->bookA, $totalSubBalance, 'true', 'Transfer to MT5');

                $totalSubBalance = $totalSubBalance/2;
                $newFund = $mainBook->balanceB + $totalSubBalance;
                DB::table('Member_Account_Fund')->where('bookA', $mainBook->bookA)->update(['balanceB' => $newFund, 'updated_at' => \Carbon\Carbon::now()]);
            }
            return true;
        }
    }