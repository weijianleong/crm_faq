<?php

namespace App\Repositories;

use App\Models\admin_log;
use App\Models\Users;
use App\Models\Member;
use Carbon\Carbon;



class AdminLogRepository extends BaseRepository
{
    protected $model;
    protected $allowedFields = [];
    protected $booleanFields = [];

    public function __construct() {
        $this->model = new admin_log;
        $this->usermodel = new Users;
        $this->membermodel = new Member;
    }

    protected function saveModel($model, $data) {
        foreach ($data as $k=>$d) {
            $model->{$k} = $d;
        }
        $model->save();
        return $model;
    }

    public function store($data) {
        $model = $this->saveModel(new $this->model, $data);
        return $model;
    }


}